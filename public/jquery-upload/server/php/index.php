<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]";
$options = array(
                  'upload_dir'=> $_GET['page'].'/',
                  'upload_url'=> $actual_link.'/'.$_GET['page'].'/',
                  'id_user' => $_GET['id']
                );
//then add the array to the constructor declaration
$upload_handler = new UploadHandler($options);
// $upload_handler = new UploadHandler();
