<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ingenio LMS | Documentation</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.8.2/js/all.js" integrity="sha384-DJ25uNYET2XCl5ZF++U8eNxPWqcKohUUBUpKGlNLMchM7q4Wjg2CUpjHLaL8yYPH" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="/lmsdoc/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="/lmsdoc/assets/plugins/prism/prism.css">
    <link rel="stylesheet" href="/lmsdoc/assets/plugins/lightbox/dist/ekko-lightbox.css">
    <link rel="stylesheet" href="/lmsdoc/assets/plugins/elegant_font/css/style.css">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="/lmsdoc/assets/css/styles.css">
    <style>
    .desc-hastag{
      margin-left: 15px;
    }
    </style>

</head>

<?php

    //children hanya sampai 3 dimensi array.

    $data = [
              [
                "id" => 'panduan-admin',
                "name" => 'Panduan Admin',
                "children" => [
                  [
                    "id" => 'login-admin',
                    "name" => 'Login Admin',
                    "children" => [],
                    "paragraf" => [
                      'Untuk mengakses halaman login admin tambahkan /admin pada domain anda',
                      'Example : https://demo.ingeniolms.com/admin'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Login Admin Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-login.png',
                      ],
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'home',
                    "name" => 'Home',
                    "children" => [],
                    "paragraf" => [
                      'Halaman home pada admin terdiri dari keterangan singkat informasi performasi website yang berjalan.',
                      'Informasi yang dapat diketahui diantaranya :'
                    ],
                    "html" => [
                      '<div class="row">
                        <div class="col-md-6 col-12">
                          <ul class="list list-unstyled">
                              <li><a href="#"><i class="fas fa-check"></i> Jumlah Task Baru</a></li>
                              <li><a href="#"><i class="fas fa-check"></i> Jumlah Tickets Baru</a></li>
                              <li><a href="#"><i class="fas fa-check"></i> Jumlah User Comment</a></li>
                              <li><a href="#"><i class="fas fa-check"></i> Jumlah Visitor</a></li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-12">
                          <ul class="list list-unstyled">
                            <li><a href="#"><i class="fas fa-check"></i> Preforma CPU USAGE</a></li>
                            <li><a href="#"><i class="fas fa-check"></i> Info Task</a></li>
                            <li><a href="#"><i class="fas fa-check"></i> Keterangan Browser User</a></li>
                          </ul>
                        </div>
                      </div>',
                    ],
                    "image" => [
                      [
                        "name" => 'Home Admin Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-home.png',
                      ],
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'kelas',
                    "name" => 'Kelas',
                    "children" => [],
                    "paragraf" => [
                      'Halaman kelas pada admin berfungsu untuk memanage data kelas/course yang akan ditampilkan pada website.'
                    ],
                    "html" => [],
                    "image" => [],
                    "sub_child" => [
                      [
                        "title" => 'List Course',
                        "paragraf" => [
                          'Daftar Kelas/Course akan ditampilkan pada sebua tabel yang tediri dari "Title", "Image", "Author", "Code", "Created At" dan "Action".',
                          'Pada Bagian Action, Admin dapat melakukan pengolahan data kelas yang ada.',
                        ],
                        "html" => [],
                        "image" => [
                          [
                            "name" => 'Kelas List Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-list-courses.png',
                          ],
                        ],
                      ],
                      [
                        "title" => 'Create Course',
                        "paragraf" => [
                          'Create Kelas/Course adalah halaman untuk membuat kelas baru.',
                          'Pada bagian atas tabel list course klik tombol <button class="btn btn-blue">Create Course</button> dan akan menuju halaman untuk membuat kelas baru. ',
                        ],
                        "html" => [],
                        "image" => [
                          [
                            "name" => 'Button Create Course Overview',
                            "src" => '/lmsdoc/assets/images/admin/create-course-button.png',
                          ],
                          [
                            "name" => 'Create Course Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-create.png',
                          ],
                        ],
                      ],
                      [
                        "title" => 'Manage Course',
                        "paragraf" => [
                          'Setelah admin membuat kelas, admin bisa memanage kelas dengan menglik tombol <button class="btn btn-orange"><i class="fa fa-edit"></i>Manage</button> pada kolom Action. Kemudian anda akan menuju halaman Manage Course.',
                          'Dalam Manage Course admin dapat mengatur fitur-fitur yang ada pada course tersebut, diantaranya: Ikhtisar, Kontent, Webinar, Diskusi, Pengumuman dan Progress Perserta',
                        ],
                        "html" => [
                          '<p>> Ikhtisar</p>
                          <div class="desc-hastag">
                            <p>Pada bagian ini admin dapat melakukan pengelolaan data kelas dan aktifitas pada kelas.</p>
                          </div>
                          <p>> Kontent</p>
                          <div class="desc-hastag">
                            <p>Pada bagian ini admin dapat melakukan pengelolaan data kontent kelas dan topik yang akan jadi bahan ajar.</p>
                            <p>Untuk membuat topik admin bisa mengklik tombol Tambah Topik, maka akan muncul inputan untuk data topik.</p>
                            <p>Setelah admin membuat Topik, admin dapat membuat kontent-kontent pada topik tersebut, makan akan muncul form untuk menambah kontent.</p>
                            <p>Admin dapat menentukan jenis kontent yang akan dibuat, jenis yang sudah disediakan diantaranya: Kontent Video, Kontent Teks, Kontent File, Kuis dan Tugas.</p>
                          </div>
                          <p>> Web Binar</p>
                          <div class="desc-hastag">
                            <p>Pada bagian ini admin dapat melakukan pengelolaan data web binar yang akan dijalankan.</p>
                            <p>Admin dapat menentukan waktu mulai dan durasi yang akan dilaksanakan.</p>
                          </div>
                          <p>> Pengumuman</p>
                          <div class="desc-hastag">
                            <p>Pada bagian ini admin dapat melakukan pengelolaan data pengumuman untuk semua user pada kelas yang bersangkutan.</p>
                          </div>
                          <p>> Progress Peserta</p>
                          <div class="desc-hastag">
                            <p>Pada bagian ini admin dapat melakukan pengelolaan data progress peserta untuk semua user pada kelas yang bersangkutan.</p>
                            <p>Admin dapat mengklik tombol Penyelesaian Peserta untuk melihat hasil yang setiap peserta kerjakan, atau Grade Book yaitu buku nilai untuk setiap peserta.</p>
                          </div>',

                        ],
                        "image" => [
                          [
                            "name" => 'Manage Course Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage.png',
                          ],
                          [
                            "name" => 'Manage Course Progress Peserta Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-progress-peserta.png',
                          ],
                          [
                            "name" => 'Manage Course Jenis Kontent Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-kontent-jenis.png',
                          ],
                          [
                            "name" => 'Manage Course Web Binar Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-web-binar.png',
                          ],
                          [
                            "name" => 'Manage Course Kontent Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-kontent.png',
                          ],
                          [
                            "name" => 'Manage Course Pengumuman Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-pengumuman.png',
                          ],
                          [
                            "name" => 'Manage Course Penyelesaian Peserta Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-course-completion.png',
                          ],
                          [
                            "name" => 'Manage Course Book Grades Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-course-grades.png',
                          ],
                          [
                            "name" => 'Manage Course Ikhtisar Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-courses-manage-ikhtisar.png',
                          ]
                        ],
                      ]
                    ]
                  ],
                  [
                    "id" => 'kategori',
                    "name" => 'Kategori',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat mengolah data katgori, untuk menambah kategori klik tombol <button class="btn btn-blue">Create</button> maka akan menuju halaman create kategori.',
                      'Pada halaman create kategori admin mengisi data kategori yang baru.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Kategori Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-categories.png'
                      ],
                      [
                        "name" => 'Kategori Button Create Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-categories-btn-create.png'
                      ],
                      [
                        "name" => 'Kategori Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-categories-create.png'
                      ]
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'users',
                    "name" => 'Users',
                    "children" => [
                      [
                        "id" => 'user',
                        "name" => 'User',
                        "paragraf" => [
                          'Pada halaman ini admin dapat mengolah data user.',
                          'Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah user kemudian akan menuju halaman create user dan admin dapat klik tombol <button class="btn btn-danger">Import</button> untuk melakukan import data user kemudian akan menuju halaman import user'
                        ],
                        "html" => [],
                        "image" => [],
                        "sub_child" => [
                          [
                            "title" => 'Create User',
                            "paragraf" => [
                              'Setelah admin berada pada halaman create user.',
                              'Admin mengisi data-data user baru, diantaranya : Username/NIM/NIP, Name, Email, Password dan User Level.',
                              'Untuk Keterangan User Level yaitu :',
                            ],
                            "html" => [
                              '<div class="row">
                                <div class="col-md-6 col-12">
                                  <ul class="list list-unstyled">
                                      <li><a href="#"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check"></i> --> Administrator : Memiliki role sebagai admin</a></li>
                                      <li><a href="#"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check"></i> --> Instructor : Memiliki role sebagai Pengajar</a></li>
                                      <li><a href="#"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check"></i> --> User : Memiliki role sebagai Pelajar</a></li>
                                      <li><a href="#"><svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check"></i> --> Affiliate : Memiliki role sebagai Pengunjung</a></li>
                                  </ul>
                                </div>
                              </div>',
                            ],
                            "image" => [
                              [
                                "name" => 'User Page Overview',
                                "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-users.png'
                              ],
                              [
                                "name" => 'User Create Page Overview',
                                "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-users-create.png'
                              ],
                            ]
                          ],
                          [
                            "title" => 'Import User',
                            "paragraf" => [
                              'Setelah admin berada pada halaman import user.',
                              'Admin dapat melakukan upload file data user yang sudah dimiliki, untuk format sample data bisa didownload dengang klik tombol Download Sample pada bagian atas upload file.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'User Import Page Overview',
                                "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-users-import.png'
                              ]
                            ]
                          ],
                        ]
                      ],
                      [
                        "id" => 'pengajar',
                        "name" => 'Pengajar',
                        "paragraf" => [
                          'Admin dapat klik tombol <button class="btn btn-blue">Create Instructor</button> untuk menambah pengajar kemudian akan menuju halaman create pengajar',
                          'Setelah admin berada pada halaman create pengajar.',
                          'Admin mengisi data-data pengajar baru, diantaranya : Username/NIM/NIP, Name, Email dan Password.'
                        ],
                        "html" => [],
                        "image" => [
                          [
                            "name" => 'Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-instructor.png'
                          ],
                          [
                            "name" => 'Pengajar Create Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-instructor-create.png'
                          ]
                        ],
                        "sub_child" => []
                      ],
                      [
                        "id" => 'manager',
                        "name" => 'Manager',
                        "paragraf" => [
                          'Pada halaman ini admin dapat mengolah data Manager',
                          'Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah manager kemudian akan menuju halaman create manager',
                          'Setelah admin berada pada halaman create manager, admin mengisi data-data manager baru, diantaranya : Username/NIM/NIP, Name, Email dan Password.'
                        ],
                        "html" => [],
                        "image" => [
                          [
                            "name" => 'Manager Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-manager.png'
                          ],
                          [
                            "name" => 'Manager Create Page Overview',
                            "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-manager-create.png'
                          ]
                        ],
                        "sub_child" => []
                      ]
                    ],
                    "paragraf" => [],
                    "html" => [],
                    "image" => [],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'slider',
                    "name" => 'Slider',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat mengolah data Slider',
                      'Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah data Slider kemudian akan menuju halaman create slider',
                      'Setelah admin berada pada halaman create slider. admin mengisi data-data slider baru, diantaranya : Title, Image, Description dan Status.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Slider Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-sliders.png'
                      ],
                      [
                        "name" => 'Slider Button Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-sliders-btn-create.png'
                      ],
                      [
                        "name" => 'Slider Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-sliders-create.png'
                      ]
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'video',
                    "name" => 'Video',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat mengolah data Video',
                      'Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah data Video kemudian akan menuju halaman create Video',
                      'Setelah admin berada pada halaman create Video, admin mengisi data-data slider baru, diantaranya : Title, Url, Description dan Status.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Video Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-videos.png'
                      ],
                      [
                        "name" => 'Video Button Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-videos-btn-create.png'
                      ],
                      [
                        "name" => 'Video Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-videos-create.png'
                      ]
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'blog',
                    "name" => 'Blog',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat mengolah data Blog',
                      'Admin dapat klik tombol <button class="btn btn-blue"><i class="fa fa-plus"></i> Add New</button> untuk menambah data Blog kemudian akan menuju halaman create Blog',
                      'Setelah admin berada pada halaman create Blog. admin mengisi data-data slider baru, diantaranya : Title, Password dan Status.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Blog Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-blogs.png'
                      ],
                      [
                        "name" => 'Blog Button Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-blogs-btn-create.png'
                      ],
                      [
                        "name" => 'Blog Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-blogs-create.png'
                      ]
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'vicon',
                    "name" => 'Vicon',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat mengolah data Vcon',
                      'Admin dapat klik tombol <button class="btn btn-blue"><i class="fa fa-plus"></i> Add New</button> untuk menambah data Vcon kemudian akan menuju halaman create Vcon',
                      'Setelah admin berada pada halaman create Vcon. admin mengisi data-data slider baru, diantaranya : Title, Url, Description dan Status.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Vcon Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-vcons.png'
                      ],
                      [
                        "name" => 'Vcon Button Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-vcons-btn-create.png'
                      ],
                      [
                        "name" => 'Vcon Create Page Overview',
                        "src" => '/lmsdoc/assets/images/admin/ingeniolms-admin-vcons-create.png'
                      ]
                    ],
                    "sub_child" => []
                  ],
                  [
                    "id" => 'setting',
                    "name" => 'Setting',
                    "children" => [],
                    "paragraf" => [
                      'Pada halaman ini admin dapat merubah pengaturan website, berikut pengaturan website yang dapat dirubah admin',
                    ],
                    "html" => [
                      '<div class="desc-hastag">
                        <p>> Title</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah Title website dengan mengkilk Edit pada Bagian Title.</p>
                        </div>
                        <p>> Icon</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah Icon website dengan mengkilk Edit pada Bagian Icon.</p>
                        </div>
                        <p>> Footer</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah Footer website dengan mengkilk Edit pada Bagian Footer.</p>
                        </div>
                        <p>> Menu Category</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah nama Menu Category website dengan mengkilk Edit pada Bagian Menu Category.</p>
                        </div>
                        <p>> Menu MyCourse</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah nama Menu MyCourse website dengan mengkilk Edit pada Bagian Menu MyCourse.</p>
                        </div>
                        <p>> Color Primary</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah warna Color Primary website dengan mengkilk Edit pada Bagian Color Primary.</p>
                        </div>
                        <p>> Color Secondary</p>
                        <div class="desc-hastag">
                          <p>Admin dapat merubah warna Color Secondary website dengan mengkilk Edit pada Bagian Color Secondary.</p>
                        </div>
                        <p>> Courses</p>
                        <div class="desc-hastag">
                          <p>Admin dapat melakukan Aktif / Nonaktif fitur Courses dengan mengkilk Edit pada Bagian Courses.</p>
                          <p>Keterangan : true untuk aktif dan false non-acticve</p>
                        </div>
                        <p>> Video</p>
                        <div class="desc-hastag">
                          <p>Admin dapat melakukan Aktif / Nonaktif fitur Video dengan mengkilk Edit pada Bagian Video.</p>
                          <p>Keterangan : true untuk aktif dan false non-acticve</p>
                        </div>
                        <p>> Degree</p>
                        <div class="desc-hastag">
                          <p>Admin dapat melakukan Aktif / Nonaktif fitur Degree dengan mengkilk Edit pada Bagian Degree.</p>
                          <p>Keterangan : true untuk aktif dan false non-acticve</p>
                        </div>
                        <p>> Program</p>
                        <div class="desc-hastag">
                          <p>Admin dapat melakukan Aktif / Nonaktif fitur Program dengan mengkilk Edit pada Bagian Program.</p>
                          <p>Keterangan : true untuk aktif dan false non-acticve</p>
                        </div>
                        <p>> Vicon</p>
                        <div class="desc-hastag">
                          <p>Admin dapat melakukan Aktif / Nonaktif fitur Vicon dengan mengkilk Edit pada Bagian Vicon.</p>
                          <p>Keterangan : true untuk aktif dan false non-acticve</p>
                        </div>
                      </div>'
                    ],
                    "image" => [],
                    "sub_child" => []
                  ]
                ],
                "paragraf" => []
              ],
              [
                "id" => 'panduan-dosen',
                "name" => 'Panduan Pengajar',
                "children" =>[
                  [
                    "id" => 'pengajar-login',
                    "name" => 'Login Pengajar',
                    "children" => [],
                    "paragraf" => [
                      'Sebelum anda masuk ke menu mengajar, pastikan bahwa sudah mempunyai akun sebagai pengajar. Pengajar mempunyai wewenang untuk melakukan pembuatan kelas dan hal lain yang berhubungan dengan pengajar. Jika anda sudah memiliki akun sebagai pengajar, maka login terlebih dulu.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Login Pengajar Overview',
                        "src" => '/lmsdoc/assets/images/pengajar/login-pengajar.png'
                      ]
                    ],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pengajar-halaman-depan',
                    "name" => 'Halaman Depan',
                    "children" => [],
                    "paragraf" => [
                      'Ketika kita berhasil login, maka sistem akan langsung menyajikan halaman Utama, pada Header user akan menemui Menu Kategori, Vicon, Kelola Kelas, Kelas saya dan Profil'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Halaman Utama Overview',
                        "src" => '/lmsdoc/assets/images/pengajar/home.png'
                      ],
                      [
                        "name" => 'Halaman Utama di baris “Kelas Tersedia” Overview',
                        "src" => '/lmsdoc/assets/images/pengajar/home2.png'
                      ]
                    ],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pengajar-manajemen-menu',
                    "name" => 'Manajemen Menu',
                    "children" => [
                      [
                        "id" => 'pengajar-kelola-kelas',
                        "name" => 'Kelola Kelas',
                        "children" => [],
                        "paragraf" => [
                          'Pada halaman depan Kelola kelas, bisa menambahkan kelas dan juga mencari modul dengan filter status dan jenis kelas. Untuk menambahkan aktifitas anda perlu masuk ke halaman “beranda situs”.',
                          'Fitur penambahan aktifitas hanya akan muncul apabila anda sedang dalam mode “edit halaman” yang telah anda lakukan sebelumnya. Berikut adalah gambar cara menambahkan aktifitas pada halaman depan.'
                        ],
                        "html" => [],
                        "image" => [
                          [
                            "name" => 'Kelola Kelas Page Overview',
                            "src" => '/lmsdoc/assets/images/pengajar/kelola-kelas.png'
                          ]
                        ],
                        "sub_child" => [
                          [
                            "title" => 'Membuat Kelas Baru',
                            "paragraf" => [
                              'Apabila ingin membuat kelas baru, bisa lakukan dengan klik tombol “Buat kelas sekarang” kemudian pilih jenis kelas (Kelas terbuka atau Kelas tertutup), Contoh akan membuat Kelas Terbuka, maka klik jenis kelas terbuka. Lalu akan menampilkan form yang harus di isi.',
                              'Setelah masuk ke halaman buat kelas, pada form tedapat isian yang telah disediakan seperti judul, sub judul, terjadwal, tujuan pembelajaran, deskripsi, gambar, harga, kategori dan tingkatan. Sesuaikan dengan yang akan di inginkan, setelah di isi semua terakhir klik tombol “create kelas”.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Tombol Buat Kelas',
                                "src" => '/lmsdoc/assets/images/pengajar/kelola-kelas-btn.png'
                              ],
                              [
                                "name" => 'Memilih Jenis Kelas Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kelola-kelas-btn-list.png'
                              ],
                              [
                                "name" => 'Form Membuat Kelas Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/buat kelas.png'
                              ],
                              [
                                "name" => 'Form Membuat Kelas Lanjutan Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/buat kelas 2.png'
                              ],
                              [
                                "name" => 'Form Membuat Kelas Lanjutan 2 Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/buat kelas 3.png'
                              ],
                              [
                                "name" => 'Button Create Kelas Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/btn create kelas.png'
                              ],
                              [
                                "name" => 'Kelas Berhasil Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/buat kelas sukses.png'
                              ],
                            ]
                          ],
                          [
                            "title" => 'Ubah Header',
                            "paragraf" => [
                              'Setelah beres membuat kelas baru, apabila ada yg salah pada nama judul, sub judul dan gambar bisa di edit dengan cara klik icon pensil yang telah ditandai warna merah, seperti pada gambar  15.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Ubah Header Kelas Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kelas edit.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Membuat Topik',
                            "paragraf" => [
                              'Sudah membuat kelas, selanjutnya membuat topik di dalam kelas itu sendiri, dengan cara klik pada tombol “Tambah topik” , lalu isi kolom nama topik yang sudah di sediakan dan klik tombol simpan untuk megakhiri, apabila ingin membatalkan klik tombol “Batal”.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Tambah Topik Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kontent topik.png'
                              ],
                              [
                                "name" => 'Simpan Topik Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kontent topik simpan.png'
                              ],
                              [
                                "name" => 'Hasil Penambahan Topik Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kontent topik simpan sukses.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Membuat Konten / Kuis',
                            "paragraf" => [
                              'Setelah buat Topik, lalu topik tersebut di isi Konten / kuis. Berikut beberapa pilihan untuk membuat konten/kuis.',
                            ],
                            "html" => [
                              '<div class="row">
                                <div class="col-md-12 col-12">
                                  <ul class="list list-unstyled">
                                      <li><a href="#"><i class="fas fa-check"></i> Konten Video (Ambil dari library, Upload video dan Tambah URL Youtube)</a></li>
                                      <li><a href="#"><i class="fas fa-check"></i> Konten Teks (Tambahkan Konten Teks dan Ambil dari library)</a></li>
                                      <li><a href="#"><i class="fas fa-check"></i> Konten File (Tambahkan konten file, Ambil dari library dan Tambah dari URL)</a></li>
                                      <li><a href="#"><i class="fas fa-check"></i> Kuis (Tambah Kuis dan Ambil dari library)</a></li>
                                      <li><a href="#"><i class="fas fa-check"></i> Tugas (Tambah tugas)</a></li>
                                  </ul>
                                </div>
                              </div>'
                            ],
                            "image" => []
                          ],
                          [
                            "title" => 'Konten Video – Upload Video',
                            "paragraf" => [
                              'Membuat konten video dengan upload file video yang telah ada di local disk. Pertama, klik menu Konten video lalu klik tombol “upload video”.',
                              'Kedua, klik pilih pada colom yang disediakan lalu akan menuju ke local disk. Ketiga, pilih file yang akan diupload.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Konten Video – Upload Video Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/video.png'
                              ],
                              [
                                "name" => 'Pilih FIle Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kontent.png'
                              ],
                              [
                                "name" => 'Upload FIle Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/open video.png'
                              ],
                              [
                                "name" => 'Progress Upload Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/progress.png'
                              ],
                              [
                                "name" => 'Simpan Konten Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kontent save.png'
                              ],
                              [
                                "name" => 'Konten Sukses Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/video save.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Konten Video – Tambah URL Youtube',
                            "paragraf" => [
                              'Membuat konten video dengan url youtube. Pertama, klik menu Konten video lalu klik tombol “Tambah URL youtube”.',
                              'Kedua, isi form yang telah disediakan dan isi sesuai yang di inginkan.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Konten Video – Tambah Url Youtube Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/video.png'
                              ],
                              [
                                "name" => 'Mengisi Form Page Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube.png'
                              ],
                              [
                                "name" => 'Simpan Dan Terbitkan Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube save.png'
                              ],
                              [
                                "name" => 'Konten Yotube Sukses Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube success.png'
                              ],
                              [
                                "name" => 'Preview Kontent Yotube Button Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube preview.png'
                              ],
                              [
                                "name" => 'Set Preview Kontent Yotube Success Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube berhasil.png'
                              ],
                              [
                                "name" => 'Preview Kontent Yotube Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/youtube preview play.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Konten Teks – Tambah Konten Teks',
                            "paragraf" => [
                              'Membuat konten teks dengan tombol tambah konten teks. Pertama, klik menu Konten teks lalu klik tombol “Tambah konten teks”.',
                              'Kedua, isi form yang telah disediakan dan isi sesuai yang di inginkan.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Konten Tekas – Tambah Konten Teks',
                                "src" => '/lmsdoc/assets/images/pengajar/teks.png'
                              ],
                              [
                                "name" => 'Mengisi Form Page Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks create.png'
                              ],
                              [
                                "name" => 'Simpan Dan Terbitkan Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks save.png'
                              ],
                              [
                                "name" => 'Kontent Teks Sukses Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks success.png'
                              ],
                              [
                                "name" => 'Preview Kontent Teks Button Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks eyes.png'
                              ],
                              [
                                "name" => 'Set Preview Kontent Teks Success Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks berhasil.png'
                              ],
                              [
                                "name" => 'Preview Kontent Teks Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks show.png'
                              ],
                              [
                                "name" => 'Edit Kontent Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/teks crud.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Konten File – Tambah Konten File',
                            "paragraf" => [
                              'Membuat konten file dengan tombol tambah konten file. Pertama, klik menu Konten file lalu klik tombol “Tambah konten file.',
                              'Kedua, isi form yang telah disediakan dan isi sesuai yang di inginkan.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Konten File – Tambah Konten File',
                                "src" => '/lmsdoc/assets/images/pengajar/file.png'
                              ],
                              [
                                "name" => 'Upload FIle Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file upload.png'
                              ],
                              [
                                "name" => 'Mengisi Form Page Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file save.png'
                              ],
                              [
                                "name" => 'Simpan Dan Terbitkan Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/simpan dan terbitkan.png'
                              ],
                              [
                                "name" => 'Kontent File Sukses Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file sukses.png'
                              ],
                              [
                                "name" => 'Preview Kontent File Button Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file eyes.png'
                              ],
                              [
                                "name" => 'Set Preview Kontent File Success Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file berhasil.png'
                              ],
                              [
                                "name" => 'Preview Kontent File Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/file show.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Konten Kuis – Tambah Konten Kuis',
                            "paragraf" => [
                              'Membuat konten file dengan tombol tambah kuis. Pertama, klik menu Kuis lalu klik tombol “Tambah Kuis.',
                              'Kedua, isi form yang telah disediakan dan isi sesuai yang di inginkan.',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Konten Kuis – Tambah Konten Kuis',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis.png'
                              ],
                              [
                                "name" => 'Mengisi Form Page Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis create 1.png'
                              ],
                              [
                                "name" => 'Mengisi Form Page Lanjutan Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis create 2.png'
                              ],
                              [
                                "name" => 'Kuis Create Button Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis create btn.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Sukses Dibuat Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis success create.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Download Sample Import Format Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis import download.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Sample Foemat Import Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis sample format.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Sample Foemat Import Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis sample format 2.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Upload Import File Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis upload.png'
                              ],
                              [
                                "name" => 'Kontent Kuis Upload Import File Success Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kuis import upload.png'
                              ]
                            ]
                          ],
                          [
                            "title" => 'Kelola Kelas',
                            "paragraf" => [
                              'Apabila ingin mengkelola kelas yang sudan ada, bisa lakukan dengan klik (tiga titik) di pojok kanan atas pada kolom kelas, kemudian klik tombol kelola.',
                              '(contoh menggunakan kolom kelas react JS.)',
                            ],
                            "html" => [],
                            "image" => [
                              [
                                "name" => 'Tombol Kelola Pada Kelas Saya Overview',
                                "src" => '/lmsdoc/assets/images/pengajar/kelola kelas saya.png'
                              ]
                            ]
                          ]
                        ],
                      ]
                    ],
                    "paragraf" => [
                      'Pada menu  Vicon memilik fungsi untuk join video conference yang sudah dibut, pada menu Kelola Kelas memiliki fungsi untuk mengelola kelas yang dibuat oleh user tersebut dan pada menu Kelas Saya memiliki fungsi untuk berinteraksi dengan kelas saya user tersebut ikuti'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Menu Header Overview',
                        "src" => '/lmsdoc/assets/images/pengajar/home4.png'
                      ]
                    ],
                    "sub_child" => [],
                  ]
                ],
                "paragraf" => [],
                "html" => [],
                "image" => [],
                "sub_child" => [],
              ],
              [
                "id" => 'panduan-pelajar',
                "name" => 'Panduan Pelajar',
                "children" => [
                  [
                    "id" => 'pelajar-login',
                    "name" => 'Login Pelajar',
                    "children" => [],
                    "paragraf" => [
                      'Sebelum anda masuk ke menu pelajar, pastikan bahwa sudah mempunyai akun sebagai pelajar. Seorang pelajar  bisa melakukan hampir fungsi sebagai pelajar yang ada di web ini. Jika anda sudah memiliki akun sebagai pelajar, maka login terlebih dulu.'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Login Pelajar Overview',
                        "src" => '/lmsdoc/assets/images/pelajar/login-pelajar.png'
                      ]
                    ],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pelajar-halaman-depan',
                    "name" => 'Halaman Depan',
                    "children" => [],
                    "paragraf" => [
                      'Ketika kita berhasil login, maka sistem akan langsung menyajikan halaman Utama, Di Header akan menemui Menu <b>Kategori</b>, <b>Vicon</b>, <b>Kelas Saya</b> dan <b>Profil</b>'
                    ],
                    "html" => [],
                    "image" => [
                      [
                        "name" => 'Halaman Depan Overview',
                        "src" => '/lmsdoc/assets/images/pelajar/home-1.png'
                      ],
                      [
                        "name" => 'Halaman Depan di baris “Kelas Tersedia” Overview',
                        "src" => '/lmsdoc/assets/images/pengajar/home2.png'
                      ],
                    ],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pelajar-halaman-kelas',
                    "name" => 'Halaman Kelas',
                    "children" => [],
                    "paragraf" => [
                      'Untuk melihat daftar kelas yang tersedia, peserta dapat masuk ke halaman kelas pada tombol lihat semua kelas di halaman depan'
                    ],
                    "html" => [],
                    "image" => [],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pelajar-halaman-detail-kelas',
                    "name" => 'Halaman Detail Kelas',
                    "children" => [],
                    "paragraf" => [
                      'Disini terdapat informasi detail mengenai kelas yang dipilih. seperti konten belajar list vidio, tugas, ataupun kuis'
                    ],
                    "html" => [],
                    "image" => [],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pelajar-enroll-kelas',
                    "name" => 'Cara Enroll Kelas',
                    "children" => [],
                    "paragraf" => [
                      'Di halaman detail kelas selain ada informasi kelas juga terdapat tombol untuk memasuki kelas tersebut tombol dengan nama Enroll Sekarang'
                    ],
                    "html" => [],
                    "image" => [],
                    "sub_child" => [],
                  ],
                  [
                    "id" => 'pelajar-kelas-saya',
                    "name" => 'Kelas Saya',
                    "children" => [],
                    "paragraf" => [
                      'Halaman ini adalah untuk menampilkan list kelas yang sudah terdaftar oleh pelajar'
                    ],
                    "html" => [],
                    "image" => [],
                    "sub_child" => [],
                  ],
                ],
                "paragraf" => [],
                "html" => [],
                "image" => [],
                "sub_child" => [],
              ]
            ];

 ?>

<body class="body-blue">
    <div class="page-wrapper">
        <!-- ******Header****** -->
        <header id="header" class="header">
            <div class="container">
                <div class="branding">
                    <h1 class="logo">
                        <a href="">
                            <span aria-hidden="true" class="icon_documents_alt icon"></span>
                            <span class="text-highlight">INGENIOLMS</span><span class="text-bold">Docs</span>
                        </a>
                    </h1>
                </div><!--//branding-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="">Home</a></li>
                    <!-- <li class="breadcrumb-item active">Components</li> -->
                </ol>
                <!-- <div class="top-search-box">
	                 <form class="form-inline search-form justify-content-center" action="" method="get">

    			            <input type="text" placeholder="Search..." name="search" class="form-control search-input">

    			            <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>

    			        </form>
                </div> -->
            </div><!--//container-->
        </header><!--//header-->
        <div class="doc-wrapper">
            <div class="container">
                <div id="doc-header" class="doc-header text-center">
                    <h1 class="doc-title"><span aria-hidden="true" class="icon icon_puzzle_alt"></span> INGENIOLMS</h1>
                    <div class="meta"><i class="far fa-clock"></i> Last updated: <?php echo date('F d, Y'); ?></div>
                </div><!--//doc-header-->
                <div class="doc-body row" >
                    <div class="doc-content col-md-9 col-12 order-1">
                        <div class="content-inner">
                            <section id="introduction" class="doc-section">
                                <h2 class="section-title">Introduction</h2>
                                <div class="section-block">
                                    <p>Selamat Datang! Halaman ini adalah dokumentasi penggunaan Ingenio LMS.</p>
                                    <p>Domain : https://demo.ingeniolms.com</p>
                                    <ul class="list list-inline">
                                        <li class="list-inline-item"><a class="btn btn-cta btn-blue" href="https://demo.ingeniolms.com" target="_blank"><i class="fas fa-external-link-alt"></i> Halaman Ingenio LMS</a></li>
                                    </ul>

                                </div><!--//section-block-->
                                <div class="section-block">
                                    <!-- <div class="row">
                                        <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 1</h6>
                                            <p>Project management</p>
                                            <div class="screenshot-holder">
                                                <a href="/lmsdoc/assets/images/demo/appify-dashboard-1.jpg" data-title="Dashboard - Project Management" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-1-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="/lmsdoc/assets/images/demo/appify-dashboard-1.jpg" data-title="Dashboard - Project Management" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 2</h6>
                                            <p>Web analytics</p>
                                            <div class="screenshot-holder">
                                                <a href="/lmsdoc/assets/images/demo/appify-dashboard-2.jpg" data-title="Dashboard - Web Analytics" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-2-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="/lmsdoc/assets/images/demo/appify-dashboard-2.jpg" data-title="Dashboard - Web Analytics" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 3</h6>
                                            <p>Health/Fitness app</p>
                                            <div class="screenshot-holder">
                                                <a href="/lmsdoc/assets/images/demo/appify-dashboard-3.jpg" data-title="Dashboard - Health/Fitness" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-3-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="/lmsdoc/assets/images/demo/appify-dashboard-3.jpg" data-title="Dashboard - Health/Fitness" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 4</h6>
                                            <p>Web hosting</p>
                                            <div class="screenshot-holder">
                                                <a href="/lmsdoc/assets/images/demo/appify-dashboard-4.jpg" data-title="Dashboard - Web Hosting" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-4-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="/lmsdoc/assets/images/demo/appify-dashboard-4.jpg" data-title="Dashboard - Web Hosting" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!--//row-->
                                    <div class="callout-block callout-info">
                                        <div class="icon-holder">
                                            <i class="fas fa-bullhorn"></i>
                                        </div><!--//icon-holder-->
                                        <div class="content">
                                            <h4 class="callout-title">Pemberitahuan Mendatang</h4>
                                            <p>Update Dokumen Manual.</p>
                                        </div><!--//content-->
                                    </div><!--//callout-->
                                </div><!--//section-block-->

                            </section><!--//doc-section-->

                            <?php foreach ($data as $key => $value) { ?>
                            <section id="<?php echo $value["id"]; ?>" class="doc-section">
                              <h2 class="section-title"><?php echo $value["name"]; ?></h2>

                              <!-- children -->
                              <?php foreach ($value["children"] as $key_1 => $value_1) { ?>
                                <div id="<?php echo $value_1["id"]; ?>" class="section-block">
                                  <h3 class="block-title"><?php echo $value_1["name"]; ?></h3>

                                  <!-- paragraf -->
                                    <?php foreach ($value_1["paragraf"] as $key_p => $value_p) { ?>
                                      <p><?php echo $value_p; ?></p>
                                    <?php } ?>

                                  <!-- html custom -->
                                    <?php foreach ($value_1["html"] as $key_html => $value_html) { ?>
                                      <?php echo $value_html; ?>
                                    <?php } ?>

                                  <!-- img -->
                                    <div class="row">
                                      <?php foreach ($value_1["image"] as $key_img => $value_img) { ?>
                                        <div class="col-md-6 col-12">
                                          <h6><?php echo $value_img["name"]; ?></h6>
                                            <div class="screenshot-holder">
                                                <a href="<?php echo $value_img["src"]; ?>" data-title="<?php echo $value_img["name"]; ?>" data-toggle="lightbox"><img class="img-fluid" src="<?php echo $value_img["src"]; ?>" alt="screenshot" /></a>
                                                <a class="mask" href="<?php echo $value_img["src"]; ?>" data-title="<?php echo $value_img["name"]; ?>" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                      <?php } ?>
                                    </div>

                                  <!-- sub child -->
                                  <?php foreach ($value_1["sub_child"] as $key_sub => $value_sub) { ?>
                                    <h5>#<?php echo $value_sub["title"]; ?></h5>

                                    <!-- paragraf -->
                                    <div class="desc-hastag">
                                        <?php foreach ($value_sub["paragraf"] as $key_sub_p => $value_sub_p) { ?>
                                          <p><?php echo $value_sub_p; ?></p>
                                        <?php } ?>

                                      <!-- html custom -->
                                        <?php foreach ($value_sub["html"] as $key_sub_html => $value_sub_html) { ?>
                                          <?php echo $value_sub_html; ?>
                                        <?php } ?>

                                      <!-- img -->
                                        <div class="row">
                                          <?php foreach ($value_sub["image"] as $key_sub_img => $value_sub_img) { ?>
                                            <div class="col-md-6 col-12">
                                              <h6><?php echo $value_sub_img["name"]; ?></h6>
                                                <div class="screenshot-holder">
                                                    <a href="<?php echo $value_sub_img["src"]; ?>" data-title="<?php echo $value_sub_img["name"]; ?>" data-toggle="lightbox"><img class="img-fluid" src="<?php echo $value_sub_img["src"]; ?>" alt="screenshot" /></a>
                                                    <a class="mask" href="<?php echo $value_sub_img["src"]; ?>" data-title="<?php echo $value_sub_img["name"]; ?>" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                          <?php } ?>
                                        </div>
                                    </div>
                                  <?php } ?>

                                  <!-- children 2 -->

                                  <?php foreach ($value_1["children"] as $key_child => $value_child) { ?>
                                    <div style="padding-left:20px;">
                                      <div id="<?php echo $value_child["id"]; ?>" class="section-block">
                                          <h4 class="block-title"><?php echo $value_child["name"]; ?></h4>

                                          <!-- paragraf -->
                                            <?php foreach ($value_child["paragraf"] as $key_p => $value_p) { ?>
                                              <p><?php echo $value_p; ?></p>
                                            <?php } ?>

                                          <!-- html custom -->
                                            <?php foreach ($value_child["html"] as $key_html => $value_html) { ?>
                                              <?php echo $value_html; ?>
                                            <?php } ?>

                                          <!-- img -->
                                            <div class="row">
                                              <?php foreach ($value_child["image"] as $key_img => $value_img) { ?>
                                                <div class="col-md-6 col-12">
                                                  <h6><?php echo $value_img["name"]; ?></h6>
                                                    <div class="screenshot-holder">
                                                        <a href="<?php echo $value_img["src"]; ?>" data-title="<?php echo $value_img["name"]; ?>" data-toggle="lightbox"><img class="img-fluid" src="<?php echo $value_img["src"]; ?>" alt="screenshot" /></a>
                                                        <a class="mask" href="<?php echo $value_img["src"]; ?>" data-title="<?php echo $value_img["name"]; ?>" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                    </div>
                                                </div>
                                              <?php } ?>
                                            </div>
                                          <br>
                                          <!-- sub child -->
                                          <?php foreach ($value_child["sub_child"] as $key_sub => $value_sub) { ?>
                                            <h5># <?php echo $value_sub["title"]; ?></h5>

                                            <!-- paragraf -->
                                            <div class="desc-hastag">
                                                <?php foreach ($value_sub["paragraf"] as $key_sub_p => $value_sub_p) { ?>
                                                  <p><?php echo $value_sub_p; ?></p>
                                                <?php } ?>

                                              <!-- html custom -->
                                                <?php foreach ($value_sub["html"] as $key_sub_html => $value_sub_html) { ?>
                                                  <?php echo $value_sub_html; ?>
                                                <?php } ?>

                                              <!-- img -->
                                                <div class="row">
                                                  <?php foreach ($value_sub["image"] as $key_sub_img => $value_sub_img) { ?>
                                                    <div class="col-md-6 col-12">
                                                      <h6><?php echo $value_sub_img["name"]; ?></h6>
                                                        <div class="screenshot-holder">
                                                            <a href="<?php echo $value_sub_img["src"]; ?>" data-title="<?php echo $value_sub_img["name"]; ?>" data-toggle="lightbox"><img class="img-fluid" src="<?php echo $value_sub_img["src"]; ?>" alt="screenshot" /></a>
                                                            <a class="mask" href="<?php echo $value_sub_img["src"]; ?>" data-title="<?php echo $value_sub_img["name"]; ?>" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                        </div>
                                                    </div>
                                                  <?php } ?>
                                                </div>
                                            </div>
                                          <?php } ?>
                                      </div>
                                    </div>
                                  <?php } ?>
                                </div>
                              <?php } ?>
                            </section>
                            <?php } ?>
                        </div><!--//content-inner-->
                    </div><!--//doc-content-->
                    <div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
                        <div id="doc-nav" class="doc-nav">
                            <nav id="doc-menu" class="nav doc-menu flex-column sticky">
                              <a class="nav-link scrollto" href="#introduction">Introduction</a>
                              <?php foreach ($data as $key => $value) { ?>
                              <a class="nav-link scrollto" href="#<?php echo $value["id"]; ?>"><?php echo $value["name"]; ?></a>
                                  <nav class="doc-sub-menu nav flex-column">
                                    <?php foreach ($value["children"] as $key_1 => $value_1) { ?>
                                      <a class="nav-link scrollto" href="#<?php echo $value_1["id"]; ?>"><?php echo $value_1["name"]; ?></a>
                                        <nav class="doc-sub-menu nav flex-column"  style="padding-left: 20px;">
                                          <?php foreach ($value_1["children"] as $key_2 => $value_2) { ?>
                                            <a class="nav-link scrollto" href="#<?php echo $value_2["id"]; ?>"><?php echo $value_2["name"]; ?></a>
                                          <?php } ?>
                                        </nav>
                                    <?php } ?>
                                  </nav>
                              <?php } ?>
                            </nav><!--//doc-menu-->
                        </div><!--//doc-nav-->
                    </div><!--//doc-sidebar-->
                </div><!--//doc-body-->
            </div><!--//container-->
        </div><!--//doc-wrapper-->

        <div id="promo-block" class="promo-block">
            <div class="container">
                <div class="promo-block-inner">
                    <h3 class="promo-title text-center"><i class="fas fa-heart"></i> <a href="https://ingenio.co.id/" target="_blank">Ingenio, Platform belajar online Indonesia.</a></h3>
                    <!-- <div class="row">
                        <div class="figure-holder col-lg-5 col-md-6 col-12">
                            <div class="figure-holder-inner">
                                <a href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank"><img class="img-fluid" src="assets/images/demo/instance-promo.jpg" alt="Instance Theme" /></a>
                                <a class="mask" href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/"><i class="icon fa fa-heart pink"></i></a>

                            </div>
                        </div>
                        <div class="content-holder col-lg-7 col-md-6 col-12">
                            <div class="content-holder-inner">
                                <div class="desc">
                                    <h4 class="content-title"><strong> Instance - Bootstrap 4 Portfolio Theme for Aspiring Developers</strong></h4>
                                    <p>Check out <a href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank">Instance</a> - a Bootstrap personal portfolio theme I created for developers. The UX design is focused on selling a developer’s skills and experience to potential employers or clients, and has <strong class="highlight">all the winning ingredients to get you hired</strong>. It’s not only a HTML site template but also a marketing framework for you to <strong class="highlight">build an impressive online presence with a high conversion rate</strong>. </p>
                                    <p><strong class="highlight">[Tip for developers]:</strong> If your project is Open Source, you can use this area to promote your other projects or hold third party adverts like Bootstrap and FontAwesome do!</p>
                                    <a class="btn btn-cta" href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank"><i class="fas fa-external-link-alt"></i> View Demo</a>

                                </div>


                                <div class="author"><a href="https://themes.3rdwavemedia.com">Xiaoying Riley</a></div>
                            </div>
                        </div>
                    </div> -->
                </div><!--//promo-block-inner-->
            </div><!--//container-->
        </div><!--//promo-block-->

    </div><!--//page-wrapper-->

    <footer id="footer" class="footer text-center">
        <div class="container">
            <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can buy the commercial license via our website: themes.3rdwavemedia.com */-->
            <small class="copyright"><i class="fa fa-heart"></i> <a href="https://ingenio.co.id/" target="_blank">Ingenio</a>  © 2020. PT. Dataquest Leverage Indonesia. </small>

        </div><!--//container-->
    </footer><!--//footer-->


    <!-- Main Javascript -->
    <script type="text/javascript" src="assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/prism/prism.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="assets/plugins/lightbox/dist/ekko-lightbox.min.js"></script>
    <script type="text/javascript" src="assets/plugins/stickyfill/dist/stickyfill.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>
