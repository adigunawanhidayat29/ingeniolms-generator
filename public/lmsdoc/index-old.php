<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ingenio UNINUS | Documentation</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.8.2/js/all.js" integrity="sha384-DJ25uNYET2XCl5ZF++U8eNxPWqcKohUUBUpKGlNLMchM7q4Wjg2CUpjHLaL8yYPH" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/plugins/prism/prism.css">
    <link rel="stylesheet" href="assets/plugins/lightbox/dist/ekko-lightbox.css">
    <link rel="stylesheet" href="assets/plugins/elegant_font/css/style.css">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
    <style>
    .desc-hastag{
      margin-left: 15px;
    }
    </style>

</head>

<body class="body-blue">
    <div class="page-wrapper">
        <!-- ******Header****** -->
        <header id="header" class="header">
            <div class="container">
                <div class="branding">
                    <h1 class="logo">
                        <a href="index.html">
                            <span aria-hidden="true" class="icon_documents_alt icon"></span>
                            <span class="text-highlight">INGENIOLMS</span><span class="text-bold">Docs</span>
                        </a>
                    </h1>
                </div><!--//branding-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <!-- <li class="breadcrumb-item active">Components</li> -->
                </ol>
                <!-- <div class="top-search-box">
	                 <form class="form-inline search-form justify-content-center" action="" method="get">

    			            <input type="text" placeholder="Search..." name="search" class="form-control search-input">

    			            <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>

    			        </form>
                </div> -->
            </div><!--//container-->
        </header><!--//header-->
        <div class="doc-wrapper">
            <div class="container">
                <div id="doc-header" class="doc-header text-center">
                    <h1 class="doc-title"><span aria-hidden="true" class="icon icon_puzzle_alt"></span> INGENIOLMS</h1>
                    <div class="meta"><i class="far fa-clock"></i> Last updated: <?php echo date('F d, Y'); ?></div>
                </div><!--//doc-header-->
                <div class="doc-body row" >
                    <div class="doc-content col-md-9 col-12 order-1">
                        <div class="content-inner">
                            <section id="introduction" class="doc-section">
                                <h2 class="section-title">Introduction</h2>
                                <div class="section-block">
                                    <p>Selamat Datang! Halaman ini adalah dokumentasi penggunaan UNINUS Ingenio LMS.</p>
                                    <p>Domain : https://uninus.ingeniolms.com</p>
                                    <ul class="list list-inline">
                                        <li class="list-inline-item"><a class="btn btn-cta btn-blue" href="https://uninus.ingeniolms.com" target="_blank"><i class="fas fa-external-link-alt"></i> Halaman UNINUS IngenioLMS</a></li>
                                    </ul>

                                </div><!--//section-block-->
                                <div class="section-block">
                                    <!-- <div class="row">
                                        <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 1</h6>
                                            <p>Project management</p>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/demo/appify-dashboard-1.jpg" data-title="Dashboard - Project Management" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-1-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/demo/appify-dashboard-1.jpg" data-title="Dashboard - Project Management" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 2</h6>
                                            <p>Web analytics</p>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/demo/appify-dashboard-2.jpg" data-title="Dashboard - Web Analytics" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-2-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/demo/appify-dashboard-2.jpg" data-title="Dashboard - Web Analytics" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 3</h6>
                                            <p>Health/Fitness app</p>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/demo/appify-dashboard-3.jpg" data-title="Dashboard - Health/Fitness" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-3-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/demo/appify-dashboard-3.jpg" data-title="Dashboard - Health/Fitness" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-12 col-sm-12">
                                            <h6>Use Case 4</h6>
                                            <p>Web hosting</p>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/demo/appify-dashboard-4.jpg" data-title="Dashboard - Web Hosting" data-toggle="lightbox"><img class="img-fluid" src="assets/images/demo/appify-dashboard-4-thumb.jpg" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/demo/appify-dashboard-4.jpg" data-title="Dashboard - Web Hosting" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!--//row-->
                                    <div class="callout-block callout-info">
                                        <div class="icon-holder">
                                            <i class="fas fa-bullhorn"></i>
                                        </div><!--//icon-holder-->
                                        <div class="content">
                                            <h4 class="callout-title">Pemberitahuan Mendatang</h4>
                                            <p>Update Dokumen Manual.</p>
                                        </div><!--//content-->
                                    </div><!--//callout-->
                                </div><!--//section-block-->

                            </section><!--//doc-section-->

                            <section id="panduan-admin" class="doc-section">
                                <h2 class="section-title">Panduan Admin</h2>
                                <div id="login-admin" class="section-block">
                                    <h3 class="block-title">Login Admin</h3>
                                    <p>Untuk mengakses halaman login admin tambahkan /admin pada domain anda</p>
                                    <p>Example : https://uninus.ingeniolms.com/admin</p>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                          <h6>Login Admin Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-login.png" data-title="Login Admin Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-login.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-login.png" data-title="Login Admin Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="home" class="section-block">
                                    <h3 class="block-title">Home</h3>
                                    <p>Halaman home pada admin terdiri dari keterangan singkat informasi performasi website yang berjalan.</p>
                                    <p>Informasi yang dapat diketahui diantaranya : </p>
                                    <div class="row">
                                      <div class="col-md-6 col-12">
                                        <ul class="list list-unstyled">
                                            <li><a href="#"><i class="fas fa-check"></i> Jumlah Task Baru</a></li>
                                            <li><a href="#"><i class="fas fa-check"></i> Jumlah Tickets Baru</a></li>
                                            <li><a href="#"><i class="fas fa-check"></i> Jumlah User Comment</a></li>
                                            <li><a href="#"><i class="fas fa-check"></i> Jumlah Visitor</a></li>
                                        </ul>
                                      </div>
                                      <div class="col-md-6 col-12">
                                        <ul class="list list-unstyled">
                                          <li><a href="#"><i class="fas fa-check"></i> Preforma CPU USAGE</a></li>
                                          <li><a href="#"><i class="fas fa-check"></i> Info Task</a></li>
                                          <li><a href="#"><i class="fas fa-check"></i> Keterangan Browser User</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                          <h6>Home Admin Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-home.png" data-title="Home Admin Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-home.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-home.png" data-title="Home Admin Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="kelas" class="section-block">
                                    <h3 class="block-title">Kelas</h3>
                                    <p>Halaman kelas pada admin berfungsu untuk memanage data kelas/course yang akan ditampilkan pada website.</p>
                                    <h5># List Course</h5>
                                    <div class="desc-hastag">
                                      <p>Daftar Kelas/Course akan ditampilkan pada sebua tabel yang tediri dari "Title", "Image", "Author", "Code", "Created At" dan "Action". </p>
                                      <p>Pada Bagian Action, Admin dapat melakukan pengolahan data kelas yang ada. </p>
                                      <div class="row">
                                          <div class="col-md-6 col-12">
                                            <h6>Kelas List Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-list-courses.png" data-title="Kelas List Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-list-courses.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-list-courses.png" data-title="Kelas List Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <h5># Create Course</h5>
                                    <div class="desc-hastag">
                                      <p>Create Kelas/Course adalah halaman untuk membuat kelas baru. </p>
                                      <p>Pada bagian atas tabel list course klik tombol <button class="btn btn-blue">Create Course</button> dan akan menuju halaman untuk membuat kelas baru. </p>
                                      <div class="row">
                                          <div class="col-md-6 col-12">
                                            <h6>Button Create Course Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/create-course-button.png" data-title="Button Create Course Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/create-course-button.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/create-course-button.png" data-title="Button Create Course Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Create Course Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-create.png" data-title="Create Course Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-create.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-create.png" data-title="Create Course Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <h5># Manage Course</h5>
                                    <div class="desc-hastag">
                                      <p>Setelah admin membuat kelas, admin bisa memanage kelas dengan menglik tombol <button class="btn btn-orange"><i class="fa fa-edit"></i>Manage</button> pada kolom Action. Kemudian anda akan menuju halaman Manage Course.</p>
                                      <p>Dalam Manage Course admin dapat mengatur fitur-fitur yang ada pada course tersebut, diantaranya: Ikhtisar, Kontent, Webinar, Diskusi, Pengumuman dan Progress Perserta</p>
                                      <p>> Ikhtisar</p>
                                      <div class="desc-hastag">
                                        <p>Pada bagian ini admin dapat melakukan pengelolaan data kelas dan aktifitas pada kelas.</p>
                                      </div>
                                      <p>> Kontent</p>
                                      <div class="desc-hastag">
                                        <p>Pada bagian ini admin dapat melakukan pengelolaan data kontent kelas dan topik yang akan jadi bahan ajar.</p>
                                        <p>Untuk membuat topik admin bisa mengklik tombol Tambah Topik, maka akan muncul inputan untuk data topik.</p>
                                        <p>Setelah admin membuat Topik, admin dapat membuat kontent-kontent pada topik tersebut, makan akan muncul form untuk menambah kontent.</p>
                                        <p>Admin dapat menentukan jenis kontent yang akan dibuat, jenis yang sudah disediakan diantaranya: Kontent Video, Kontent Teks, Kontent File, Kuis dan Tugas.</p>
                                      </div>
                                      <p>> Web Binar</p>
                                      <div class="desc-hastag">
                                        <p>Pada bagian ini admin dapat melakukan pengelolaan data web binar yang akan dijalankan.</p>
                                        <p>Admin dapat menentukan waktu mulai dan durasi yang akan dilaksanakan.</p>
                                      </div>
                                      <p>> Pengumuman</p>
                                      <div class="desc-hastag">
                                        <p>Pada bagian ini admin dapat melakukan pengelolaan data pengumuman untuk semua user pada kelas yang bersangkutan.</p>
                                      </div>
                                      <p>> Progress Peserta</p>
                                      <div class="desc-hastag">
                                        <p>Pada bagian ini admin dapat melakukan pengelolaan data progress peserta untuk semua user pada kelas yang bersangkutan.</p>
                                        <p>Admin dapat mengklik tombol Penyelesaian Peserta untuk melihat hasil yang setiap peserta kerjakan, atau Grade Book yaitu buku nilai untuk setiap peserta.</p>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage.png" data-title="Manage Course Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage.png" data-title="Manage Course Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Progress Peserta Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-progress-peserta.png" data-title="Manage Course Progress Peserta Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-progress-peserta.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-progress-peserta.png" data-title="Manage Course Progress Peserta Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Jenis Kontent Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-kontent-jenis.png" data-title="Manage Course Jenis Kontent Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-kontent-jenis.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-kontent-jenis.png" data-title="Manage Kontent Jenis Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Web Binar Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-web-binar.png" data-title="Manage Course Web Binar Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-web-binar.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-web-binar.png" data-title="Manage Kontent Web Binar Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Kontent Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-kontent.png" data-title="Manage Course Kontent Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-kontent.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-kontent.png" data-title="Manage Kontent Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Pengumuman Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-pengumuman.png" data-title="Manage Course Pengumuman Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-pengumuman.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-pengumuman.png" data-title="Manage Course Pengumuman Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Penyelesaian Peserta Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-course-completion.png" data-title="Manage Course Penyelesaian Peserta Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-course-completion.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-course-completion.png" data-title="Create Course Penyelesaian Peserta Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Book Grades Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-course-grades.png" data-title="Manage Course Book Grades Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-course-grades.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-course-grades.png" data-title="Manage Course Book Grades Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Manage Course Ikhtisar Page Overview</h6>
                                              <div class="screenshot-holder">
                                                  <a href="assets/images/admin/ingeniolms-admin-courses-manage-ikhtisar.png" data-title="Manage Course Ikhtisar Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-courses-manage-ikhtisar.png" alt="screenshot" /></a>
                                                  <a class="mask" href="assets/images/admin/ingeniolms-admin-courses-manage-ikhtisar.png" data-title="Manage Course Ikhtisar Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                </div><!--//section-block-->

                                <div id="kategori" class="section-block">
                                    <h3 class="block-title">Kategori</h3>
                                    <p>Pada halaman ini admin dapat mengolah data katgori, untuk menambah kategori klik tombol <button class="btn btn-blue">Create</button> maka akan menuju halaman create kategori.</p>
                                    <p>Pada halaman create kategori admin mengisi data kategori yang baru.</p>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                          <h6>Kategori Page Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-categories.png" data-title="Kategori Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-categories.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-categories.png" data-title="Kategori Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <h6>Kategori Button Create Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-categories-btn-create.png" data-title="Kategori Button Create Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-categories-btn-create.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-categories-btn-create.png" data-title="Kategori Button Create Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <h6>Kategori Create Page Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-categories-create.png" data-title="Kategori Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-categories-create.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-categories-create.png" data-title="Kategori Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--//section-block-->

                                <div id="users" class="section-block">
                                  <h3 class="block-title">Users</h3>
                                  <div style="padding-left:20px;">

                                    <div id="user" class="section-block">
                                        <h4 class="block-title">User</h4>
                                        <p>Pada halaman ini admin dapat mengolah data user</p>
                                        <p>Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah user kemudian akan menuju halaman create user dan admin dapat klik tombol <button class="btn btn-danger">Import</button> untuk melakukan import data user kemudian akan menuju halaman import user</p>
                                        <h5># Create User</h5>
                                        <div class="desc-hastag">
                                          <p>Setelah admin berada pada halaman create user.</p>
                                          <p>Admin mengisi data-data user baru, diantaranya : Username/NIM/NIP, Name, Email, Password dan User Level.</p>
                                          <p>Untuk Keterangan User Level yaitu :</p>
                                          <div class="row">
                                            <div class="col-md-6 col-12">
                                              <ul class="list list-unstyled">
                                                  <li><a href="#"><i class="fas fa-check"></i> Administrator : Memiliki role sebagai admin</a></li>
                                                  <li><a href="#"><i class="fas fa-check"></i> Instructor : Memiliki role sebagai Pengajar</a></li>
                                                  <li><a href="#"><i class="fas fa-check"></i> User : Memiliki role sebagai Pelajar</a></li>
                                                  <li><a href="#"><i class="fas fa-check"></i> Affiliate : Memiliki role sebagai Pengunjung</a></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                        <h5># Import User</h5>
                                        <div class="desc-hastag">
                                          <p>Setelah admin berada pada halaman import user.</p>
                                          <p>Admin dapat melakukan upload file data user yang sudah dimiliki, untuk format sample data bisa didownload dengang klik tombol Download Sample pada bagian atas upload file.</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                              <h6>User Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-users.png" data-title="User Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-users.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-users.png" data-title="User Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <h6>User Create Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-users-create.png" data-title="User Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-users-create.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-users-create.png" data-title="User Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <h6>User Import Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-users-import.png" data-title="User Import Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-users-import.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-users-import.png" data-title="User Import Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--//section-block-->

                                    <div id="pengajar" class="section-block">
                                        <h4 class="block-title">Pengajar</h4>
                                        <p>Pada halaman ini admin dapat mengolah data pengajar/Instructor</p>
                                        <p>Admin dapat klik tombol <button class="btn btn-blue">Create Instructor</button> untuk menambah pengajar kemudian akan menuju halaman create pengajar</p>
                                        <p>Setelah admin berada pada halaman create pengajar.</p>
                                        <p>Admin mengisi data-data pengajar baru, diantaranya : Username/NIM/NIP, Name, Email dan Password.</p>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                              <h6>Page Overview</h6>
                                                <div class="Pengajar Page Overview">
                                                    <a href="assets/images/admin/ingeniolms-admin-instructor.png" data-title="Pengajar Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-instructor.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-instructor.png" data-title="Pengajar Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <h6>Pengajar Create Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-instructor-create.png" data-title="Pengajar Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-instructor-create.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-instructor-create.png" data-title="Pengajar Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--//section-block-->

                                    <div id="manager" class="section-block">
                                        <h4 class="block-title">Manager</h4>
                                        <p>Pada halaman ini admin dapat mengolah data Manager</p>
                                        <p>Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah manager kemudian akan menuju halaman create manager</p>
                                        <p>Setelah admin berada pada halaman create manager.</p>
                                        <p>Admin mengisi data-data manager baru, diantaranya : Username/NIM/NIP, Name, Email dan Password.</p>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                              <h6>Manager Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-manager.png" data-title="Manager Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-manager.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-manager.png" data-title="Manager Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <h6>Manager Create Page Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/admin/ingeniolms-admin-manager-create.png" data-title="Manager Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-manager-create.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/admin/ingeniolms-admin-manager-create.png" data-title="Manager Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--//section-block-->
                                  </div>
                                </div><!--//section-block-->

                                 <div id="slider" class="section-block">
                                    <h3 class="block-title">Slider</h3>
                                    <p>Pada halaman ini admin dapat mengolah data Slider</p>
                                    <p>Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah data Slider kemudian akan menuju halaman create slider</p>
                                    <p>Setelah admin berada pada halaman create slider.</p>
                                    <p>Admin mengisi data-data slider baru, diantaranya : Title, Image, Description dan Status.</p>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                          <h6>Slider Page Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-sliders.png" data-title="Slider Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-sliders.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-sliders.png" data-title="Slider Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <h6>Slider Button Create Page Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-instructor-sliders-btn-create.png" data-title="Slider Button Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-sliders-btn-create.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-sliders-btn-create.png" data-title="Slider Button Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <h6>Slider Create Page Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/admin/ingeniolms-admin-instructor-sliders-create.png" data-title="Slider Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-sliders-create.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/admin/ingeniolms-admin-sliders-create.png" data-title="Slider Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                 </div>

                                 <div id="video" class="section-block">
                                   <h3 class="block-title">Video</h3>
                                   <p>Pada halaman ini admin dapat mengolah data Video</p>
                                   <p>Admin dapat klik tombol <button class="btn btn-blue">Create</button> untuk menambah data Video kemudian akan menuju halaman create Video</p>
                                   <p>Setelah admin berada pada halaman create Video.</p>
                                   <p>Admin mengisi data-data slider baru, diantaranya : Title, Url, Description dan Status.</p>
                                   <div class="row">
                                       <div class="col-md-6 col-12">
                                         <h6>Video Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-videos.png" data-title="Video Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-videos.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-videos.png" data-title="Video Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Video Button Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-videos-btn-create.png" data-title="Video Button Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-videos-btn-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-videos-btn-create.png" data-title="Video Button Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Video Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-videos-create.png" data-title="Video Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-videos-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-videos-create.png" data-title="Video Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                   </div>
                                 </div>

                                 <div id="blog" class="section-block">
                                   <h3 class="block-title">Blog</h3>
                                   <p>Pada halaman ini admin dapat mengolah data Blog</p>
                                   <p>Admin dapat klik tombol <button class="btn btn-blue"><i class="fa fa-plus"></i> Add New</button> untuk menambah data Blog kemudian akan menuju halaman create Blog</p>
                                   <p>Setelah admin berada pada halaman create Blog.</p>
                                   <p>Admin mengisi data-data slider baru, diantaranya : Title, Password dan Status.</p>
                                   <div class="row">
                                       <div class="col-md-6 col-12">
                                         <h6>Blog Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-blogs.png" data-title="Blog Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-blogs.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-blogs.png" data-title="Blog Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Blog Button Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-blogs-btn-create.png" data-title="Blog Button Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-blogs-btn-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-blogs-btn-create.png" data-title="Blog Button Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Blog Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-blogs-create.png" data-title="Blog Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-blogs-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-blogs-create.png" data-title="Blog Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                   </div>
                                 </div>

                                 <div id="vcon" class="section-block">
                                   <h3 class="block-title">Vcon</h3>
                                   <p>Pada halaman ini admin dapat mengolah data Vcon</p>
                                   <p>Admin dapat klik tombol <button class="btn btn-blue"><i class="fa fa-plus"></i> Add New</button> untuk menambah data Vcon kemudian akan menuju halaman create Vcon</p>
                                   <p>Setelah admin berada pada halaman create Vcon.</p>
                                   <p>Admin mengisi data-data slider baru, diantaranya : Title, Url, Description dan Status.</p>
                                   <div class="row">
                                       <div class="col-md-6 col-12">
                                         <h6>Vcon Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-vcons.png" data-title="Vcon Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-vcons.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-vcons.png" data-title="Vcon Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Vcon Button Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-vcons-btn-create.png" data-title="Vcon Button Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-vcons-btn-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-vcons-btn-create.png" data-title="Vcon Button Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-12">
                                         <h6>Vcon Create Page Overview</h6>
                                           <div class="screenshot-holder">
                                               <a href="assets/images/admin/ingeniolms-admin-instructor-vcons-create.png" data-title="Vcon Create Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/admin/ingeniolms-admin-vcons-create.png" alt="screenshot" /></a>
                                               <a class="mask" href="assets/images/admin/ingeniolms-admin-vcons-create.png" data-title="Vcon Create Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                           </div>
                                       </div>
                                   </div>
                                 </div>

                                 <div id="setting" class="section-block">
                                    <h3 class="block-title">Setting</h3>
                                    <p>Pada halaman ini admin dapat merubah pengaturan website, berikut pengaturan website yang dapat dirubah admin</p>
                                    <div class="desc-hastag">
                                      <p>> Title</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah Title website dengan mengkilk Edit pada Bagian Title.</p>
                                      </div>
                                      <p>> Icon</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah Icon website dengan mengkilk Edit pada Bagian Icon.</p>
                                      </div>
                                      <p>> Footer</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah Footer website dengan mengkilk Edit pada Bagian Footer.</p>
                                      </div>
                                      <p>> Menu Category</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah nama Menu Category website dengan mengkilk Edit pada Bagian Menu Category.</p>
                                      </div>
                                      <p>> Menu MyCourse</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah nama Menu MyCourse website dengan mengkilk Edit pada Bagian Menu MyCourse.</p>
                                      </div>
                                      <p>> Color Primary</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah warna Color Primary website dengan mengkilk Edit pada Bagian Color Primary.</p>
                                      </div>
                                      <p>> Color Secondary</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat merubah warna Color Secondary website dengan mengkilk Edit pada Bagian Color Secondary.</p>
                                      </div>
                                      <p>> Courses</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat melakukan Aktif / Nonaktif fitur Courses dengan mengkilk Edit pada Bagian Courses.</p>
                                        <p>Keterangan : true untuk aktif dan false non-acticve</p>
                                      </div>
                                      <p>> Video</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat melakukan Aktif / Nonaktif fitur Video dengan mengkilk Edit pada Bagian Video.</p>
                                        <p>Keterangan : true untuk aktif dan false non-acticve</p>
                                      </div>
                                      <p>> Degree</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat melakukan Aktif / Nonaktif fitur Degree dengan mengkilk Edit pada Bagian Degree.</p>
                                        <p>Keterangan : true untuk aktif dan false non-acticve</p>
                                      </div>
                                      <p>> Program</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat melakukan Aktif / Nonaktif fitur Program dengan mengkilk Edit pada Bagian Program.</p>
                                        <p>Keterangan : true untuk aktif dan false non-acticve</p>
                                      </div>
                                      <p>> Vicon</p>
                                      <div class="desc-hastag">
                                        <p>Admin dapat melakukan Aktif / Nonaktif fitur Vicon dengan mengkilk Edit pada Bagian Vicon.</p>
                                        <p>Keterangan : true untuk aktif dan false non-acticve</p>
                                      </div>
                                    </div>
                                 </div>

                            </section><!--//doc-section-->
                            <section id="panduan-dosen" class="doc-section">
                                <h2 class="section-title">Panduan Dosen / Pengajar</h2>
                                <div id="pengajar-login" class="section-block">
                                  <h3 class="block-title">Login Pengajar</h3>
                                  <p>Sebelum anda masuk ke menu mengajar, pastikan bahwa sudah mempunyai akun sebagai pengajar. Pengajar mempunyai wewenang untuk melakukan pembuatan kelas dan hal lain yang berhubungan dengan pengajar. Jika anda sudah memiliki akun sebagai pengajar, maka login terlebih dulu.</p>
                                  <div class="row">
                                    <div class="col-md-6 col-12">
                                      <h6>Login Pengajar Overview</h6>
                                      <div class="screenshot-holder">
                                          <a href="assets/images/pengajar/login-pengajar.png" data-title="Login Pengajar Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/login-pengajar.png" alt="screenshot" /></a>
                                          <a class="mask" href="assets/images/pengajar/login-pengajar.png" data-title="Login Pengajar Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div id="pengajar-halaman-depan" class="section-block">
                                  <h3 class="block-title">Halaman Depan Pengajar</h3>
                                  <p>Ketika kita berhasil login, maka sistem akan langsung menyajikan halaman Utama, pada Header user akan menemui Menu Kategori, Vicon, Kelola Kelas, Kelas saya dan Profil</p>
                                  <div class="row">
                                      <div class="col-md-6 col-12">
                                        <h6>Halaman Utama Overview</h6>
                                          <div class="screenshot-holder">
                                              <a href="assets/images/pengajar/home.png" data-title="Halaman Utama Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/home.png" alt="screenshot" /></a>
                                              <a class="mask" href="assets/images/pengajar/home.png" data-title="Halaman Utama Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-12">
                                        <h6>Halaman Utama di baris “Kelas Tersedia” Overview</h6>
                                          <div class="screenshot-holder">
                                              <a href="assets/images/pengajar/home2.png" data-title="Halaman Utama di baris “Kelas Tersedia” Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/home2.png" alt="screenshot" /></a>
                                              <a class="mask" href="assets/images/pengajar/home2.png" data-title="Halaman Utama di baris “Kelas Tersedia” Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-12">
                                        <h6>Halaman Utama di baris “Keunggulan” Overview</h6>
                                          <div class="screenshot-holder">
                                              <a href="assets/images/pengajar/home3.png" data-title="Halaman Utama di baris “Keunggulan” Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/home3.png" alt="screenshot" /></a>
                                              <a class="mask" href="assets/images/pengajar/home3.png" data-title="Halaman Utama di baris “Keunggulan” Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                                </div>

                                <div id="pengajar-manajemen-menu" class="section-block">
                                <h3 class="block-title">Manajemen Menu</h3>
                                <p>Pada menu  Vicon memilik fungsi untuk join video conference yang sudah dibut, pada menu Kelola Kelas memiliki fungsi untuk mengelola kelas yang dibuat oleh user tersebut dan pada menu Kelas Saya memiliki fungsi untuk berinteraksi dengan kelas saya user tersebut ikuti</p>
                                <div class="row">
                                  <div class="col-md-6 col-12">
                                    <h6>Menu Header Overview</h6>
                                    <div class="screenshot-holder">
                                        <a href="assets/images/pengajar/home4.png" data-title="Menu Header Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/home4.png" alt="screenshot" /></a>
                                        <a class="mask" href="assets/images/pengajar/home4.png" data-title="Menu Header Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                    </div>
                                  </div>
                                </div>
                                <div style="padding-left:20px;">
                                  <div id="pengajar-kelola-kelas" class="section-block">
                                      <h4 class="block-title">Kelola Kelas</h4>
                                      <p>Pada halaman depan Kelola kelas, bisa menambahkan kelas dan juga mencari modul dengan filter status dan jenis kelas. Untuk menambahkan aktifitas anda perlu masuk ke halaman “beranda situs”.</p>
                                      <p>Fitur penambahan aktifitas hanya akan muncul apabila anda sedang dalam mode “edit halaman” yang telah anda lakukan sebelumnya. Berikut adalah gambar cara menambahkan aktifitas pada halaman depan.</p>
                                      <div class="row">
                                        <div class="col-md-6 col-12">
                                          <h6>Kelola Kelas Page Overview</h6>
                                          <div class="screenshot-holder">
                                              <a href="assets/images/pengajar/kelola-kelas.png" data-title="Kelola Kelas Page Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/kelola-kelas.png" alt="screenshot" /></a>
                                              <a class="mask" href="assets/images/pengajar/kelola-kelas.png" data-title="Kelola Kelas Page Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                          </div>
                                        </div>
                                      </div>
                                      <h5># Membuat Kelas Baru</h5>
                                      <div class="desc-hastag">
                                        <p>Apabila ingin membuat kelas baru, bisa lakukan dengan klik tombol “Buat kelas sekarang” kemudian pilih jenis kelas (Kelas terbuka atau Kelas tertutup), Contoh akan membuat Kelas Terbuka, maka klik jenis kelas terbuka. Lalu akan menampilkan form yang harus di isi.</p>
                                        <p>Setelah masuk ke halaman buat kelas, pada form tedapat isian yang telah disediakan seperti judul, sub judul, terjadwal, tujuan pembelajaran, deskripsi, gambar, harga, kategori dan tingkatan. Sesuaikan dengan yang akan di inginkan, setelah di isi semua terakhir klik tombol “create kelas”.</p>
                                        <div class="row">
                                          <div class="col-md-6 col-12">
                                            <h6>Tombol Buat Kelas Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/kelola-kelas-btn.png" data-title="Tombol Buat Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/kelola-kelas-btn.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/kelola-kelas-btn.png" data-title="Tombol Buat Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Memilih Jenis Kelas Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/kelola-kelas-btn-list.png" data-title="Memilih Jenis Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/kelola-kelas-btn-list.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/kelola-kelas-list.png" data-title="Memilih Jenis Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Form Membuat Kelas Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/buat kelas.png" data-title="Form Membuat Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/buat kelas.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/buat kelas.png" data-title="Form Membuat Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Form Membuat Kelas Lanjutan Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/buat kelas 2.png" data-title="Form Membuat Kelas Lanjutan Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/buat kelas 2.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/buat kelas 2.png" data-title="Form Membuat Kelas Lanjutan Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Form Membuat Kelas Lanjutan 2 Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/buat kelas 3.png" data-title="Form Membuat Kelas Lanjutan 2 Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/buat kelas 3.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/buat kelas 3.png" data-title="Form Membuat Kelas Lanjutan 2 Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Button Create Kelas Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/btn create kelas.png" data-title="Button Create Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/btn create kelas.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/btn create kelas.png" data-title="Button Create Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-12">
                                            <h6>Kelas Berhasil Dibuat Overview</h6>
                                            <div class="screenshot-holder">
                                                <a href="assets/images/pengajar/buat kelas sukses.png" data-title="Kelas Berhasil Dibuat Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/buat kelas sukses.png" alt="screenshot" /></a>
                                                <a class="mask" href="assets/images/pengajar/buat kelas sukses.png" data-title="Kelas Berhasil Dibuat Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <h5># Ubah Header</h5>
                                      <div class="desc-hastag">
                                        <p>Setelah beres membuat kelas baru, apabila ada yg salah pada nama judul, sub judul dan gambar bisa di edit dengan cara klik icon pensil yang telah ditandai warna merah, seperti pada gambar  15.</p>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                              <h6>Ubah Header Kelas Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/pengajar/kelas edit" data-title="Ubah Header Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/kelas edit.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/pengajar/kelas edit.png" data-title="Ubah Header Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                      <h5># Membuat Topik</h5>
                                      <div class="desc-hastag">
                                        <p>Sudah membuat kelas, selanjutnya membuat topik di dalam kelas itu sendiri, dengan cara klik pada tombol “Tambah topik” , lalu isi kolom nama topik yang sudah di sediakan dan klik tombol simpan untuk megakhiri, apabila ingin membatalkan klik tombol “Batal”.</p>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                              <h6>Ubah Header Kelas Overview</h6>
                                                <div class="screenshot-holder">
                                                    <a href="assets/images/pengajar/kelas edit" data-title="Ubah Header Kelas Overview" data-toggle="lightbox"><img class="img-fluid" src="assets/images/pengajar/kelas edit.png" alt="screenshot" /></a>
                                                    <a class="mask" href="assets/images/pengajar/kelas edit.png" data-title="Ubah Header Kelas Overview" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                            </section>

                            <section id="panduan-mahasiswa" class="doc-section">
                                <h2 class="section-title">Panduan Mahasiswa</h2>
                                <div class="section-block">
                                    <p>Halaman pada bagian mahasiswa adalah....</p>
                                </div>
                            </section>

                        </div><!--//content-inner-->
                    </div><!--//doc-content-->
                    <div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
                        <div id="doc-nav" class="doc-nav">
                            <nav id="doc-menu" class="nav doc-menu flex-column sticky">
                                <a class="nav-link scrollto" href="#introduction">Introduction</a>
                                <a class="nav-link scrollto" href="#panduan-admin">Panduan Admin</a>
                                  <nav class="doc-sub-menu nav flex-column">
                                      <a class="nav-link scrollto" href="#login-admin">Login Admin</a>
                                      <a class="nav-link scrollto" href="#home">Home</a>
                                      <a class="nav-link scrollto" href="#kelas">Kelas</a>
                                      <a class="nav-link scrollto" href="#kategori">Kategori</a>
                                      <a class="nav-link scrollto" href="#users">Users</a>
                                        <nav class="doc-sub-menu nav flex-column" style="padding-left: 20px;">
                                            <a class="nav-link scrollto" href="#user">User</a>
                                            <a class="nav-link scrollto" href="#pengajar">Pengajar</a>
                                            <a class="nav-link scrollto" href="#manager">Manager</a>
                                        </nav><!--//nav-->
                                      <a class="nav-link scrollto" href="#slider">Slider</a>
                                      <a class="nav-link scrollto" href="#video">Video</a>
                                      <a class="nav-link scrollto" href="#blog">Blog</a>
                                      <a class="nav-link scrollto" href="#vcon">Vcon</a>
                                      <a class="nav-link scrollto" href="#setting">Setting</a>
                                  </nav><!--//nav-->
                                <a class="nav-link scrollto" href="#panduan-dosen">Panduan Dosen/Pengajar</a>
                                  <nav class="doc-sub-menu nav flex-column">
                                      <a class="nav-link scrollto" href="#pengajar-login">Login Pengajar</a>
                                      <a class="nav-link scrollto" href="#pengajar-halaman-depan">Halaman Depan</a>
                                      <a class="nav-link scrollto" href="#pengajar-manajemen-menu">Manajemen menu</a>
                                        <nav class="doc-sub-menu nav flex-column" style="padding-left: 20px;">
                                            <a class="nav-link scrollto" href="#pengajar-kelola-kelas">Kelola Kelas</a>
                                        </nav><!--//nav-->
                                  </nav><!--//nav-->
                                <a class="nav-link scrollto" href="#panduan-mahasiswa">Panduan Mahasiswa</a>
                            </nav><!--//doc-menu-->
                        </div><!--//doc-nav-->
                    </div><!--//doc-sidebar-->
                </div><!--//doc-body-->
            </div><!--//container-->
        </div><!--//doc-wrapper-->

        <div id="promo-block" class="promo-block">
            <div class="container">
                <div class="promo-block-inner">
                    <h3 class="promo-title text-center"><i class="fas fa-heart"></i> <a href="https://ingenio.co.id/" target="_blank">Ingenio, Platform belajar online Indonesia.</a></h3>
                    <!-- <div class="row">
                        <div class="figure-holder col-lg-5 col-md-6 col-12">
                            <div class="figure-holder-inner">
                                <a href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank"><img class="img-fluid" src="assets/images/demo/instance-promo.jpg" alt="Instance Theme" /></a>
                                <a class="mask" href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/"><i class="icon fa fa-heart pink"></i></a>

                            </div>
                        </div>
                        <div class="content-holder col-lg-7 col-md-6 col-12">
                            <div class="content-holder-inner">
                                <div class="desc">
                                    <h4 class="content-title"><strong> Instance - Bootstrap 4 Portfolio Theme for Aspiring Developers</strong></h4>
                                    <p>Check out <a href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank">Instance</a> - a Bootstrap personal portfolio theme I created for developers. The UX design is focused on selling a developer’s skills and experience to potential employers or clients, and has <strong class="highlight">all the winning ingredients to get you hired</strong>. It’s not only a HTML site template but also a marketing framework for you to <strong class="highlight">build an impressive online presence with a high conversion rate</strong>. </p>
                                    <p><strong class="highlight">[Tip for developers]:</strong> If your project is Open Source, you can use this area to promote your other projects or hold third party adverts like Bootstrap and FontAwesome do!</p>
                                    <a class="btn btn-cta" href="https://themes.3rdwavemedia.com/bootstrap-templates/portfolio/instance-bootstrap-portfolio-theme-for-developers/" target="_blank"><i class="fas fa-external-link-alt"></i> View Demo</a>

                                </div>


                                <div class="author"><a href="https://themes.3rdwavemedia.com">Xiaoying Riley</a></div>
                            </div>
                        </div>
                    </div> -->
                </div><!--//promo-block-inner-->
            </div><!--//container-->
        </div><!--//promo-block-->

    </div><!--//page-wrapper-->

    <footer id="footer" class="footer text-center">
        <div class="container">
            <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can buy the commercial license via our website: themes.3rdwavemedia.com */-->
            <small class="copyright"><i class="fa fa-heart"></i> <a href="https://ingenio.co.id/" target="_blank">Ingenio</a>  © 2020. PT. Dataquest Leverage Indonesia. </small>

        </div><!--//container-->
    </footer><!--//footer-->


    <!-- Main Javascript -->
    <script type="text/javascript" src="assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/prism/prism.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="assets/plugins/lightbox/dist/ekko-lightbox.min.js"></script>
    <script type="text/javascript" src="assets/plugins/stickyfill/dist/stickyfill.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>
