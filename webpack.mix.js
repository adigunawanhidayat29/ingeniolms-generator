let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
  'public/css/preload.min.css',
  'public/css/plugins.min.css',
  'public/css/style.dq-light-blue.min.css',
  'public/css/dq-style.css',
  'public/css/dq-style-2.css'
], 'public/css/all.css').options ({
  processCssUrls : false,
  autoprefixer: false
});

mix.scripts([
  'public/js/plugins.min.js',
  'public/js/app.min.js',
  'public/js/index.js',
], 'public/js/all.js');
