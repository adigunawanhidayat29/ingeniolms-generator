<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingProviderTrainee extends Model
{
    protected $table = 'training_provider_trainees';
}
