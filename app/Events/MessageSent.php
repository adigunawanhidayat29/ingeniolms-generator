<?php

namespace App\Events;

use App\User;
use App\CourseChat;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $course_chat;

    public function __construct(User $User, CourseChat $CourseChat)
    {
      $this->course_chat = $CourseChat;
      $this->user = $User;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('chat-'.$this->course_chat->course_id);
    }

    public function broadcastAs()
    {
        return 'get-chat';
    }
}
