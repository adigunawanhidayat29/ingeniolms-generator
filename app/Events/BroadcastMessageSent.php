<?php

namespace App\Events;

use App\User;
use App\CourseBroadcastChat;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BroadcastMessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $course_broadcast_chats;

    public function __construct(User $User, CourseBroadcastChat $CourseBroadcastChat)
    {
      $this->course_broadcast_chats = $CourseBroadcastChat;
      $this->user = $User;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('broadcast-chat-'.$this->course_broadcast_chats->broadcast_id);
    }

    public function broadcastAs()
    {
        return 'get-broadcast-chat';
    }
}
