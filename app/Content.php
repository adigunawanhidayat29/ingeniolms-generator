<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
  protected $table = 'contents';

  public function section()
  {
      return $this->belongsTo('App\Section', 'id_section');
  }

  public function discussion(){

    return $this->hasMany('App\DiscussionAnswers', 'content_id');
  }

  public function files(){

    return $this->hasMany('App\ContentFile', 'content_id');
  }

  public function setting(){

    return $this->hasOne('App\ActivityCompletion', 'content_id');
  }

  public function progress(){
    return $this->hasOne('App\Progress', 'content_id');
  }

  public function progress_users(){
    return $this->hasMany('App\Progress', 'content_id')->with('user');
  }


  public function activity_order(){

    return $this->hasOne('App\ActivityOrder', 'content_id');
  }

  public function restrict(){

    return $this->hasMany('App\RestrictAccess', 'content_from_id');
  }

  // public function group_has_content(){
  //   return $this->belongsTo('App\GroupHasContent', 'id', 'id')->where('table', 'contents');
  // }
}
