<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseUpdate extends Model
{
    //
    public function user(){

      return $this->belongsTo('App\User');
    }

    public function reply_1(){

      return $this->hasMany('App\CourseUpdate', 'level_1');
    }
}
