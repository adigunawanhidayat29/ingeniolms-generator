<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePublish extends Model
{
    protected $table = 'site_publishes';
}
