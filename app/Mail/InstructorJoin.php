<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use app\Instructor;

class InstructorJoin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $Instructor;

    public function __construct(Instructor $Instructor)
    {
      $Instructor = Instructor::where(['user_id'=>$Instructor->user_id])->join('users','users.id','=','instructors.user_id');
      $this->Instructor = $Instructor->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.instructor.join')->with(['Instructor'=>$this->Instructor]);
    }
}
