<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\CourseUser;
use App\User;

class Course extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $CourseUser;
    public function __construct(CourseUser $CourseUser)
    {
      $CourseUser = CourseUser::where(['user_id'=>$CourseUser->user_id, 'course_id'=>$CourseUser->course_id])
        ->join('users','users.id','=','courses_users.user_id')
        ->join('courses','courses.id','=','courses_users.course_id');
      $this->CourseUser = $CourseUser->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.course.enroll')
          ->with([
            'course' => $this->CourseUser->title,
            'name' => $this->CourseUser->name,
          ]);
    }
}
