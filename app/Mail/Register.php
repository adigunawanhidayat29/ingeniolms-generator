<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class Register extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user;
    public function __construct(User $user)
    {
      $this->user = $user;
    }

    public function build()
    {
      return $this->from(Setting('MAIL_FROM')->value, Setting('MAIL_SENDER')->value)
        ->subject("Register " . Setting('title')->value)
        ->markdown('emails.user.register')
        ->with([
          'name' => $this->user->name,
          'email' => $this->user->email,
          'activation_token' => $this->user->activation_token,
        ]);
    }
}
