<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transaction;
use App\TransactionDetail;

class TransactionInformation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $Transaction;
    public function __construct(Transaction $Transaction)
    {
      $Transaction = Transaction::where(['invoice'=>$Transaction->invoice])->join('users','users.id','=','transactions.user_id');
      $this->Transaction = $Transaction->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      $TransactionDetails = TransactionDetail::where('invoice', $this->Transaction->invoice)
        ->join('courses', 'courses.id', '=', 'transactions_details.course_id' )
        ->get();

        return $this->markdown('emails.transaction.information')
          ->with([
            'Transaction' => $this->Transaction,
            'TransactionDetails' => $TransactionDetails,
          ]);
    }
}
