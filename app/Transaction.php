<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //

    public function detail_transactions(){
        return $this->hasMany('App\TransactionDetail', 'invoice', 'invoice');
    }
}
