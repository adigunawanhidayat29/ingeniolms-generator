<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentVideoQuiz extends Model
{

  public $table = 'content_video_quizes';

  public function content_video_quiz_answers(){
    return $this->hasMany('App\ContentVideoUserQuizAnswer', 'content_video_quiz_id', 'id');
  }
}
