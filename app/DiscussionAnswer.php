<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionAnswer extends Model
{
  protected $table = "discussions_answers";
}
