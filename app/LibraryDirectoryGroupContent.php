<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectoryGroupContent extends Model
{
    public function library(){

      return $this->belongsTo('App\LibraryDirectoryContent', 'group_content_id', 'id');
    }
}
