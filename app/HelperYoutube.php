<?php
namespace App;

use Google_Client;
use Google_Service_YouTube;
use Google_Service_YouTube_PlaylistSnippet;
use Google_Service_YouTube_PlaylistStatus;
use Google_Service_YouTube_Playlist;
use Google_Service_YouTube_ResourceId;
use Google_Service_YouTube_PlaylistItemSnippet;
use Google_Service_YouTube_PlaylistItem;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;
use Google_Http_MediaFileUpload;
use Google_Service_YouTube_LiveBroadcastSnippet;
use Google_Service_YouTube_LiveBroadcastStatus;
use Google_Service_YouTube_LiveBroadcast;
use Google_Service_YouTube_LiveStreamSnippet;
use Google_Service_YouTube_CdnSettings;
use Google_Service_YouTube_LiveStream;
use Session;

class HelperYoutube {

  public static function playlistCreate($playlistTitle, $playlistDescription, $redirectUrl){
    session_start();
    $OAUTH2_CLIENT_ID = env('YOUTUBE_CLIENT_ID');
    $OAUTH2_CLIENT_SECRET = env('YOUTUBE_CLIENT_SECRET');

    $client = new Google_Client();
    $client->setClientId($OAUTH2_CLIENT_ID);
    $client->setClientSecret($OAUTH2_CLIENT_SECRET);
    $client->setScopes('https://www.googleapis.com/auth/youtube');
    $redirect = filter_var($redirectUrl, FILTER_SANITIZE_URL);
    $client->setRedirectUri($redirect);

    // Define an object that will be used to make all API requests.
    $youtube = new Google_Service_YouTube($client);

    // Check if an auth token exists for the required scopes
    $tokenSessionKey = 'token-' . $client->prepareScopes();
    if (isset($_GET['code'])) {
      if (strval($_SESSION['state']) !== strval($_GET['state'])) {
        die('The session state did not match.');
      }

      $client->authenticate($_GET['code']);
      $_SESSION[$tokenSessionKey] = $client->getAccessToken();
      header('Location: ' . $redirect);
    }

    if (isset($_SESSION[$tokenSessionKey])) {
      $client->setAccessToken($_SESSION[$tokenSessionKey]);
    }


    // Check to ensure that the access token was successfully acquired.
    if ($client->getAccessToken()) {

        // This code creates a new, private playlist in the authorized user's
        // channel and adds a video to the playlist.

        // 1. Create the snippet for the playlist. Set its title and description.
        $playlistSnippet = new Google_Service_YouTube_PlaylistSnippet();
        $playlistSnippet->setTitle($playlistTitle);
        $playlistSnippet->setDescription($playlistDescription);

        // 2. Define the playlist's status.
        $playlistStatus = new Google_Service_YouTube_PlaylistStatus();
        $playlistStatus->setPrivacyStatus('unlisted');

        // 3. Define a playlist resource and associate the snippet and status
        // defined above with that resource.
        $youTubePlaylist = new Google_Service_YouTube_Playlist();
        $youTubePlaylist->setSnippet($playlistSnippet);
        $youTubePlaylist->setStatus($playlistStatus);

        // 4. Call the playlists.insert method to create the playlist. The API
        // response will contain information about the new playlist.
        $playlistResponse = $youtube->playlists->insert('snippet,status',
        $youTubePlaylist, array());
        $playlistId = $playlistResponse;
      }

      return $playlistId;
    }

    public static function videoCreate($videoPath, $videoTitle, $videoDescripion, $videoTag, $videoCategoryId, $videoStatus, $redirectUrl){
      session_start();

      $OAUTH2_CLIENT_ID = env('YOUTUBE_CLIENT_ID');
      $OAUTH2_CLIENT_SECRET = env('YOUTUBE_CLIENT_SECRET');

      $client = new Google_Client();
      $client->setClientId($OAUTH2_CLIENT_ID);
      $client->setClientSecret($OAUTH2_CLIENT_SECRET);
      $client->setScopes('https://www.googleapis.com/auth/youtube');
      $redirect = filter_var($redirectUrl, FILTER_SANITIZE_URL);
      $client->setRedirectUri($redirect);
      $htmlBody = "";

      // Define an object that will be used to make all API requests.
      $youtube = new Google_Service_YouTube($client);

      // Check if an auth token exists for the required scopes
      $tokenSessionKey = 'token-' . $client->prepareScopes();
      if (isset($_GET['code'])) {
        if (strval($_SESSION['state']) !== strval($_GET['state'])) {
          die('The session state did not match.');
        }

        $client->authenticate($_GET['code']);
        $_SESSION[$tokenSessionKey] = $client->getAccessToken();
        header('Location: ' . $redirect);
      }

      if (isset($_SESSION[$tokenSessionKey])) {
        $client->setAccessToken($_SESSION[$tokenSessionKey]);
      }

      // Check to ensure that the access token was successfully acquired.
      if ($client->getAccessToken()) {
        $htmlBody = '';
        try{
          // REPLACE this value with the path to the file you are uploading.

          // Create a snippet with title, description, tags and category ID
          // Create an asset resource and set its snippet metadata and type.
          // This example sets the video's title, description, keyword tags, and
          // video category.
          $snippet = new Google_Service_YouTube_VideoSnippet();
          $snippet->setTitle($videoTitle);
          $snippet->setDescription($videoDescripion);
          $snippet->setTags(array($videoTag));

          // Numeric video category. See
          // https://developers.google.com/youtube/v3/docs/videoCategories/list
          $snippet->setCategoryId($videoCategoryId);

          // Set the video's status to "public". Valid statuses are "public",
          // "private" and "unlisted".
          $status = new Google_Service_YouTube_VideoStatus();
          $status->privacyStatus = $videoStatus;

          // Associate the snippet and status objects with a new video resource.
          $video = new Google_Service_YouTube_Video();
          $video->setSnippet($snippet);
          $video->setStatus($status);

          // Specify the size of each chunk of data, in bytes. Set a higher value for
          // reliable connection as fewer chunks lead to faster uploads. Set a lower
          // value for better recovery on less reliable connections.
          $chunkSizeBytes = 1 * 1024 * 1024;

          // Setting the defer flag to true tells the client to return a request which can be called
          // with ->execute(); instead of making the API call immediately.
          $client->setDefer(true);

          // Create a request for the API's videos.insert method to create and upload the video.
          $insertRequest = $youtube->videos->insert("status,snippet", $video);

          // Create a MediaFileUpload object for resumable uploads.
          $media = new Google_Http_MediaFileUpload(
            $client,
            $insertRequest,
            'video/*',
            null,
            true,
            $chunkSizeBytes
          );
          $media->setFileSize(filesize($videoPath));


          // Read the media file and upload it chunk by chunk.
          $status = false;
          $handle = fopen($videoPath, "rb");
          while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
          }

          fclose($handle);

          // If you want to make other calls after the file upload, set setDefer back to false
          $client->setDefer(false);


          $htmlBody .= "<h3>Video Uploaded</h3><ul>";
          $htmlBody .= sprintf('<li>%s (%s)</li>',
          $status['snippet']['title'],
          $status['id']);

          $htmlBody .= '</ul>';

        } catch (Google_Service_Exception $e) {
          $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
          htmlspecialchars($e->getMessage()));
        } catch (Google_Exception $e) {
          $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
          htmlspecialchars($e->getMessage()));
        }

        $_SESSION[$tokenSessionKey] = $client->getAccessToken();
      } elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {
        $htmlBody = "
        <h3>Client Credentials Required</h3>
        <p>
        You need to set <code>\$OAUTH2_CLIENT_ID</code> and
        <code>\$OAUTH2_CLIENT_ID</code> before proceeding.
        <p>";
      } else {
        // If the user hasn't authorized the app, initiate the OAuth flow
        $state = mt_rand();
        $client->setState($state);
        $_SESSION['state'] = $state;

        $authUrl = $client->createAuthUrl();
        $htmlBody = "
        <h3>Authorization Required</h3>
        <p>You need to <a href=".$authUrl.">authorize access</a> before proceeding.<p>";
      }
      return $status;
    }

    public static function playlistItemCreate($playlistId, $videoId, $redirectUrl){
      session_start();

      $OAUTH2_CLIENT_ID = env('YOUTUBE_CLIENT_ID');
      $OAUTH2_CLIENT_SECRET = env('YOUTUBE_CLIENT_SECRET');

      $client = new Google_Client();
      $client->setClientId($OAUTH2_CLIENT_ID);
      $client->setClientSecret($OAUTH2_CLIENT_SECRET);
      $client->setScopes('https://www.googleapis.com/auth/youtube');
      $redirect = filter_var($redirectUrl,
      FILTER_SANITIZE_URL);
      $client->setRedirectUri($redirect);
      $htmlBody = '';

      // Define an object that will be used to make all API requests.
      $youtube = new Google_Service_YouTube($client);

      // Check if an auth token exists for the required scopes
      $tokenSessionKey = 'token-' . $client->prepareScopes();
      if (isset($_GET['code'])) {
        if (strval($_SESSION['state']) !== strval($_GET['state'])) {
          die('The session state did not match.');
        }

        $client->authenticate($_GET['code']);
        $_SESSION[$tokenSessionKey] = $client->getAccessToken();
        header('Location: ' . $redirect);
      }

      if (isset($_SESSION[$tokenSessionKey])) {
        $client->setAccessToken($_SESSION[$tokenSessionKey]);
      }

      // Check to ensure that the access token was successfully acquired.
      if ($client->getAccessToken()) {


          $resourceId = new Google_Service_YouTube_ResourceId();
          $resourceId->setVideoId($videoId);
          $resourceId->setKind('youtube#video');

          // Then define a snippet for the playlist item. Set the playlist item's
          // title if you want to display a different value than the title of the
          // video being added. Add the resource ID and the playlist ID retrieved
          // in step 4 to the snippet as well.
          $playlistItemSnippet = new Google_Service_YouTube_PlaylistItemSnippet();
          $playlistItemSnippet->setPlaylistId($playlistId);
          $playlistItemSnippet->setResourceId($resourceId);

          // Finally, create a playlistItem resource and add the snippet to the
          // resource, then call the playlistItems.insert method to add the playlist
          // item.
          $playlistItem = new Google_Service_YouTube_PlaylistItem();
          $playlistItem->setSnippet($playlistItemSnippet);
          $playlistItemResponse = $youtube->playlistItems->insert('snippet,contentDetails', $playlistItem, array());
        }

        return $playlistItemResponse;
      }

      public static function streaming(){

        session_start();
        $OAUTH2_CLIENT_ID = env('YOUTUBE_CLIENT_ID');
        $OAUTH2_CLIENT_SECRET = env('YOUTUBE_CLIENT_SECRET');

        $client = new Google_Client();
        $client->setClientId($OAUTH2_CLIENT_ID);
        $client->setClientSecret($OAUTH2_CLIENT_SECRET);
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $redirect = filter_var(url('/').'/youtube/streaming/', FILTER_SANITIZE_URL);
        $client->setRedirectUri($redirect);

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);

        // Check if an auth token exists for the required scopes
        $tokenSessionKey = 'token-' . $client->prepareScopes();
        if (isset($_GET['code'])) {
          if (strval($_SESSION['state']) !== strval($_GET['state'])) {
            die('The session state did not match.');
          }

          $client->authenticate($_GET['code']);
          $_SESSION[$tokenSessionKey] = $client->getAccessToken();
          header('Location: ' . $redirect);
          exit();
        }

        if (isset($_SESSION[$tokenSessionKey])) {
          $client->setAccessToken($_SESSION[$tokenSessionKey]);
        }

        // Check to ensure that the access token was successfully acquired.
        if ($client->getAccessToken()) {
          try {
            // Create an object for the liveBroadcast resource's snippet. Specify values
            // for the snippet's title, scheduled start time, and scheduled end time.
            $broadcastSnippet = new Google_Service_YouTube_LiveBroadcastSnippet();
            $broadcastSnippet->setTitle(Session::get('title'));
            $broadcastSnippet->setDescription(Session::get('description'));
            $broadcastSnippet->setScheduledStartTime(date("Y-m-d").'T19:00:00.000Z');
            $broadcastSnippet->setScheduledEndTime(date("Y-m-d").'T20:00:00.000Z');

            // Create an object for the liveBroadcast resource's status, and set the
            // broadcast's status to "private".
            $status = new Google_Service_YouTube_LiveBroadcastStatus();
            $status->setPrivacyStatus('unlisted');

            // Create the API request that inserts the liveBroadcast resource.
            $broadcastInsert = new Google_Service_YouTube_LiveBroadcast();
            $broadcastInsert->setSnippet($broadcastSnippet);
            $broadcastInsert->setStatus($status);
            $broadcastInsert->setKind('youtube#liveBroadcast');

            // Execute the request and return an object that contains information
            // about the new broadcast.
            $broadcastsResponse = $youtube->liveBroadcasts->insert('snippet,status',
                $broadcastInsert, array());

            // Create an object for the liveStream resource's snippet. Specify a value
            // for the snippet's title.
            $streamSnippet = new Google_Service_YouTube_LiveStreamSnippet();
            $streamSnippet->setTitle(Session::get('title') . ' Stream');

            // Create an object for content distribution network details for the live
            // stream and specify the stream's format and ingestion type.
            $cdn = new Google_Service_YouTube_CdnSettings();
            $cdn->setFormat("1080p");
            $cdn->setIngestionType('rtmp');

            // Create the API request that inserts the liveStream resource.
            $streamInsert = new Google_Service_YouTube_LiveStream();
            $streamInsert->setSnippet($streamSnippet);
            $streamInsert->setCdn($cdn);
            $streamInsert->setKind('youtube#liveStream');

            // Execute the request and return an object that contains information
            // about the new stream.
            $streamsResponse = $youtube->liveStreams->insert('snippet,cdn',
                $streamInsert, array());

            // Bind the broadcast to the live stream.
            $bindBroadcastResponse = $youtube->liveBroadcasts->bind(
                $broadcastsResponse['id'],'id,contentDetails',
                array(
                    'streamId' => $streamsResponse['id'],
                ));

            $broadcastsResponseList = $youtube->liveBroadcasts->listLiveBroadcasts(
                'id,snippet,contentDetails',
                array(
                    'broadcastType' => 'persistent',
                    'mine' => 'true',
                ));

            $boundStreamId = $broadcastsResponseList['items']['0']['contentDetails']['boundStreamId'];

            $streamsResponseList = $youtube->liveStreams->listLiveStreams('id,snippet,cdn', array(
                'id' => $boundStreamId
            ));

            $responses = [
              'status' => '200',
              'streamsResponse' => $streamsResponse,
              'broadcastsResponse' => $broadcastsResponse,
              'streamsResponse' => $streamsResponse,
              'bindBroadcastResponse' => $bindBroadcastResponse
            ];

          $session_access_token = $_SESSION[$tokenSessionKey] = $client->getAccessToken();
          Session::put('access_token', $session_access_token);
          return $responses;

        } catch (Google_Service_Exception $e) {
          return $e->getMessage();
        } catch (Google_Exception $e) {
          return $e->getMessage();
        }

        }else {
          // If the user hasn't authorized the app, initiate the OAuth flow
          $state = mt_rand();
          $client->setState($state);
          $_SESSION['state'] = $state;

          $authUrl = $client->createAuthUrl();
          $responses = [
            'status' => '401',
            'authUrl' => $authUrl
          ];
          return $responses;
          // return redirect($authUrl);
        }
      }
}
