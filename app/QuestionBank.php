<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    //
    public function type(){

      return $this->belongsTo('App\QuizType', 'quiz_library_type_id');
    }
}
