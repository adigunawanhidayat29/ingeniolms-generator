<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;

use Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

use App\Course;
use App\Observers\CourseObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      if(env('APP_ENV') === 'production'){
        URL::forceScheme('https');
      }
      Schema::defaultStringLength(191);

      Storage::extend('sftp', function ($app, $config) {
        return new Filesystem(new SftpAdapter($config));
      });

      Course::observe(CourseObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
