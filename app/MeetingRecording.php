<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRecording extends Model
{
  protected $table = 'meeting_recordings';
}
