<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateSetting extends Model
{
  public $table = 'affiliate_setting';
}
