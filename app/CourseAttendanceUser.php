<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAttendanceUser extends Model
{
    protected $table = 'course_attendance_users';

    public function user () {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function course_attendance () {
        return $this->hasOne('App\CourseAttendance', 'id', 'course_attendances_id');
    }
}
