<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteGlobal extends Model
{
    protected $table = 'site_globals';
}
