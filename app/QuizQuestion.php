<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model
{
    protected $table = 'quiz_questions';

    public function quiz_question_answers()
    {
        return $this->hasMany('App\QuizQuestionAnswer', 'quiz_question_id', 'id');
    }

    public function quiz_participants_answers()
    {
        return $this->hasMany('App\QuizParticipantAnswer', 'quiz_question_id', 'id');
    }

    public function participants_answers($id_participant)
    {
        return $this->hasOne('App\QuizParticipantAnswer', 'quiz_question_id', 'id')->where('quiz_participant_id', $id_participant)->first();
    }

    public function quiz_type() {
        return $this->hasOne('App\QuizType', 'id', 'quiz_type_id');
    }
}
