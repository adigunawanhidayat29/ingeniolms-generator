<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelingRulesContent extends Model
{
    protected $table = 'leveling_rules_content';
}
