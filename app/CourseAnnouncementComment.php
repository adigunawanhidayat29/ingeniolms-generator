<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAnnouncementComment extends Model
{
  protected $table = 'course_announcement_comments';
}
