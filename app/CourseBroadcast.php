<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseBroadcast extends Model
{
  public $table = 'course_broadcasts';
}
