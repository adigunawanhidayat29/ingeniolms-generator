<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseUser extends Model
{
  protected $table = 'courses_users';

  protected $fillable = ['course_id', 'user_id'];

  public function courses(){
    return $this->belongsTo('App\Course', 'course_id', 'id');
  }

  public function registered_courses(){
    return $this->belongsTo('App\Course', 'course_id', 'id')->where('courses.status', '=', '1');
  }

  public function user(){
    return $this->belongsTo('App\User', 'user_id');
  }

  public function student() {
    return $this->hasOne('App\User', 'id', 'user_id');
}

  public function grade($content_id){

    return $this->hasOne('App\DiscussionGrade', 'user_id', 'user_id')->where('content_id', $content_id)->first();
  }
}
