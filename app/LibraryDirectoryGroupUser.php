<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectoryGroupUser extends Model
{
    public function group(){

      return $this->belongsTo('App\LibraryDirectoryGroup', 'group_id', 'id');
    }
}
