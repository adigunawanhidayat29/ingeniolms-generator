<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDiscussion extends Model
{
    //
    public function user(){

      return $this->belongsTo('App\User', 'user_id');
    }

    public function answer(){

      return $this->hasMany('App\GroupDiscussionAnswer', 'group_discussion_id');
    }
}
