<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingProviderTrainer extends Model
{
    protected $table = 'training_provider_trainers';
}
