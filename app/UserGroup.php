<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
  protected $table = 'users_groups';
  public $timestamps = false;
  protected $fillable = ['level_id', 'user_id'];
}
