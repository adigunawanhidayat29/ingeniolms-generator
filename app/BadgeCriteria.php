<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeCriteria extends Model
{
    protected $table = 'badges_criteria';
}
