<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramUser extends Model
{
    public function program() {
        return $this->hasMany('App\Program', 'id', 'program_id');
    }
}
