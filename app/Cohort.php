<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cohort extends Model
{
    protected $fillable = ['name', 'created_by', 'status', 'code'];

    public function cohort_user () {
        return $this->hasMany('App\CohortUser')->join('users', 'users.id', '=', 'cohort_user.user_id');
    }
}
