<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorBalance extends Model
{
  protected $table = 'instructor_balances';
}
