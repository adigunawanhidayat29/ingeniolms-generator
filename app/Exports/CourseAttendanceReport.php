<?php
namespace App\Exports;
use App\CourseAttendance;
use App\Course;
use App\CourseUser;
use App\CourseAttendanceUser;
use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class CourseAttendanceReport implements FromView
{
    use Exportable;

    public function __construct(string $course_id)
    {
        $this->course_id = $course_id;
    }

    public function view(): View {

        $Course = Course::where('id', $this->course_id)->first();

        $CoursesUsers = CourseUser::where('course_id', $this->course_id)->where('user_id', '!=', $Course->id_author)
            ->join('users','users.id','=','courses_users.user_id')->get();

        $Reports = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), DB::raw('count(course_attendances_id) as count_attendances'), 'user_id')
          ->with('user')
          ->where('course_id', $this->course_id)->groupBy('user_id')->get();

        $CourseAttendancesSummary = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), 'course_attendances.description', 'course_attendance_users.course_attendances_id')
          ->where('course_attendance_users.course_id', $this->course_id)->groupBy('course_attendances_id')
          ->join('course_attendances', 'course_attendances.id', '=', 'course_attendance_users.course_attendances_id')
          ->get();

        return view('course.exports.course_attendances', [
            'courses_users' => $CoursesUsers,
            'reports' => $Reports,
            'CourseAttendancesSummary' => $CourseAttendancesSummary,
        ]);

    }

}