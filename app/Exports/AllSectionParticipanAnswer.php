<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class AllSectionParticipanAnswer implements FromView, WithTitle
{

    public function __construct($data){
      $this->data = $data;
    }

    public function view(): View
    {
        // return view('exports.invoices', [
        //     'invoices' => Invoice::all()
        // ]);
        return view('course.quiz_all_participant_answer_excel', $this->data);
    }

    public function title(): string
    {
        return $this->data['quiz']->name;
    }
}
