<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\QuizParticipantAnswer;
use App\QuizParticipant;
use App\Course;
use App\User;

class AllParticipanAnswer implements WithMultipleSheets
{
    use Exportable;


    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new AllSectionParticipanAnswer($this->data);

        foreach ($this->data["quiz_participants"] as $key => $participant) {
          $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*', 'quiz_participant_answers.id as quiz_participant_answer_id','quiz_questions.*','quiz_question_answers.*')
            ->where('quiz_participant_id', $participant->id)
            ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
            ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
            ->get();

            $Course = Course::select('courses.*')->join('sections', 'sections.id_course', '=', 'courses.id')->where('sections.id', $this->data['quiz']->id_section)->first();

            $QuizParticipantUserId = QuizParticipant::where('id', $participant->id)->first();
            $UserName = User::where('id', $QuizParticipantUserId->user_id)->select('name', 'email', 'id')->first();

            $newData = [
              'quiz_participants' => $QuizParticipants,
              'quiz' => $this->data['quiz'],
              'course' => $Course,
              'user' => $UserName,
            ];

          $sheets[] = new ParticipanAnswer($newData);
          // code...
        }

        return $sheets;
    }
}
