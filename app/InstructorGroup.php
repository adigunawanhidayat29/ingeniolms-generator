<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorGroup extends Model
{
    public function library(){

      return $this->hasMany('App\InstructorGroupLibrary', 'instructor_group_id');
    }

    public function group_update(){

      return $this->hasMany('App\GroupUpdate', 'group_id');
    }
}
