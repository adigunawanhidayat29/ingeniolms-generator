<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Program extends Model
{
  use Sluggable;

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  public function getImageAttribute()
  {
    // return asset_url() . $this->attributes['image'];
    return $this->attributes['image'] ? asset_url() . $this->attributes['image'] : asset('default.jpg');
  }
}
