<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeFix extends Model
{
    protected $table = 'badges_fix';
}
