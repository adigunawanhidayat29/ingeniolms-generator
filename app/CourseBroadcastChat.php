<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseBroadcastChat extends Model
{
  public $table = 'course_broadcast_chats';
}
