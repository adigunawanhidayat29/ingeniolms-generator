<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeCourseAssign extends Model
{
    protected $table = 'badges_course_assign';
}
