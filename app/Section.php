<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
  protected $table = 'sections';

  public function course(){
    return $this->hasOne('App\Course', 'id', 'id_course');
  }

  public function contents(){
    return $this->hasMany('App\Content', 'id_section', 'id');
  }

  public function quizzes(){
    return $this->hasMany('App\Quiz', 'id_section', 'id');
  }
  public function assignments(){
    return $this->hasMany('App\Assignment', 'id_section', 'id');
  }
}
