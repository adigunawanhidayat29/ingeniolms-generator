<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionAnswer extends Model
{
  protected $table = 'quiz_question_answers';
}
