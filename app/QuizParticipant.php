<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizParticipant extends Model
{
  protected $table = 'quiz_participants';

  public function user(){

    return $this->belongsTo('App\User', 'user_id');
  }

  public function answer(){

    return $this->hasMany('App\QuizParticipantAnswer', 'quiz_participant_id');
  }
}
