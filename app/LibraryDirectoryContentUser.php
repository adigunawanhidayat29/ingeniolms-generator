<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectoryContentUser extends Model
{
    public function library(){

      return $this->hasOne('App\LibraryDirectoryContent', 'id', 'library_directory_content_id');
    }
}
