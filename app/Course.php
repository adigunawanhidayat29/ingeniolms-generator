<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Course extends Model
{
  use Sluggable;

  protected $table = 'courses';

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  public function participants()
  {
      return $this->hasMany('App\CourseUser', 'course_id', 'id');
  }

  public function sections()
  {
      return $this->hasMany('App\Section', 'id_course', 'id')->with('contents', 'quizzes', 'assignments');
  }

  public function contents()
  {
      return $this->hasManyThrough('App\Content', 'App\Section', 'id_course', 'id_section');
  }

  public function assigment()
  {
      return $this->hasManyThrough('App\Assignment', 'App\Section', 'id_course', 'id_section');
  }

  public function quizz()
  {
      return $this->hasManyThrough('App\Quiz', 'App\Section', 'id_course', 'id_section');
  }

  public function user(){
		return $this->belongsTo('App\User','id_author');
  }

  public function getImageAttribute()
  {
      // return asset_url() . $this->attributes['image'];
      return $this->attributes['image'] ? asset_url() . $this->attributes['image'] : asset('default.jpg');
  }

  public function course_user () {
    return $this->hasMany('App\CourseUser')->join('users', 'users.id', '=', 'courses_users.user_id');
  }

  public function course_cohort () {
    return $this->hasMany('App\CourseCohort');
  }

  public function setting () {
    return $this->hasOne('App\CourseSetting');
  }

  public function default_activity_completion () {
    return $this->hasMany('App\DefaultActivityCompletion');
  }

  public function author() {
    return $this->hasOne('App\User', 'id', 'id_author')->select('id', 'name', 'photo', 'email')->withCount('total_courses');
  }

  public function category() {
    return $this->hasOne('App\Category', 'id', 'id_category');
  }

  public function level() {
    return $this->hasOne('App\CourseLevel', 'id', 'id_level_course');
  }

}
