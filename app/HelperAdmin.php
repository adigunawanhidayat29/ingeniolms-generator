<?php

use Illuminate\Support\Facades\DB;

function status_transaction($status){
  if($status == '0'){
    $status = "<span class='label label-warning'>Pending</span>";
  }else{
    $status = "<span class='label label-success'>Success</span>";
  }
  return $status;
}

function status_discussion($status){
  if($status == '0'){
    $status = "<span class='label label-danger'>Block</span>";
  }else{
    $status = "<span class='label label-success'>Publish</span>";
  }
  return $status;
}

function status_instructor($status){
  if($status == '0'){
    $status = "<span class='label label-danger'>". \Lang::get('back.user_teacher.status_block') ."</span>";
  }else{
    $status = "<span class='label label-success'>". \Lang::get('back.user_teacher.status_approve') ."</span>";
  }
  return $status;
}

function status_manager($status){
  if($status == '0'){
    $status = "<span class='label label-danger'>Block</span>";
  }else{
    $status = "<span class='label label-success'>Approved</span>";
  }
  return $status;
}
