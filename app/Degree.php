<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    public function getImageAttribute()
    {
        // return asset_url() . $this->attributes['image'];
        return $this->attributes['image'] ? asset_url() . $this->attributes['image'] : asset('default.jpg');
    }
}
