<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupHasContent extends Model
{
    protected $table = 'group_has_contents';
}
