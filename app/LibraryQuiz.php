<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryQuiz extends Model
{
    protected $table = "library_quizzes";

    public function pagebreak(){

      return $this->hasMany('App\LibraryQuizPageBreak', 'library_quiz_id');
    }

    public function question(){

      return $this->hasMany('App\LibraryQuizQuestion', 'quiz_library_id');
    }
}
