<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RulesCriteria extends Model
{
    protected $table = 'rules_criteria';
}
