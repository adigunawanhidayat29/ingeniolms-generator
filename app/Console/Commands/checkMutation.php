<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class checkMutation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:mutation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check transaction mutation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $transactions = DB::table('transactions')->where(['status' => '0'])->get();
      foreach($transactions as $index => $transaction){
        // echo $transaction->subtotal;
        ${'moota'.$index}[] = json_decode(moota('bank/vq7WPqAkADe/mutation/search/'.$transaction->subtotal), TRUE);
        foreach(${'moota' . $index}[0]['mutation'] as $data){
          if($data['type'] == 'CR'){
            DB::table('transactions')->where('subtotal', $transaction->subtotal)->update(['status' => '1']);
          }
        }
      }
    }
}
