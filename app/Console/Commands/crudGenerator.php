<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class crudGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:crud {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate CRUD';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected function getStub($type){
        return file_get_contents(resource_path("stubs/$type.stub"));
    }

    protected function controller($name){

        $count_columns = count(\Schema::getColumnListing(strtolower(str_plural($name))));
        $columns = \Schema::getColumnListing(strtolower(str_plural($name)));

        $column_array_old = "";
        $column_array_store = "";
        $column_array_old_value = "";

        for($i=0; $i < $count_columns; $i++){
            if($columns[$i] != 'created_at' && $columns[$i] != 'updated_at'){
                $column_array_old .= "'".$columns[$i]."' => old('".$columns[$i]."'), \n";
                $column_array_store .= "$".$name."->".$columns[$i]."; \n ";           
                $column_array_old_value .= "'".$columns[$i]."' => old('".$columns[$i]."', $".$name."->".$columns[$i]."), \n";           
            }
        }
        
        $controllerTemplate = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePlural}}',
                '{{modelNameSingular}}',
                '{{column_array_old}}',
                '{{column_array_store}}',
                '{{column_array_old_value}}',
            ],
            [
                $name,
                strtolower(str_plural($name)),
                strtolower($name),
                $column_array_old,
                $column_array_store,
                $column_array_old_value,
            ],
            $this->getStub('Controller')
        );
        file_put_contents(app_path("/Http/Controllers/Admin/{$name}Controller.php"), $controllerTemplate);
    }

    protected function model($name){
        $modelTemplate = str_replace(
            ['{{modelName}}', '{{modelNamePlural}}'],
            [$name, strtolower(str_plural($name))],
            $this->getStub('Model')
        );

        file_put_contents(app_path("/{$name}.php"), $modelTemplate);
    }

    protected function view($name){
        $viewIndexTemplate = str_replace(
            ['{{modelName}}', '{{modelNamePlural}}', '{{modelNameSingular}}'],
            [$name, strtolower(str_plural($name)), strtolower($name)],
            $this->getStub('views/index')
        );

        $viewFormTemplate = str_replace(
            ['{{modelName}}', '{{modelNamePlural}}', '{{modelNameSingular}}'],
            [$name, strtolower(str_plural($name)), strtolower($name)],
            $this->getStub('views/form')
        );
        
        File::makeDirectory(resource_path("views/admin/{$name}"));
        file_put_contents(resource_path("views/admin/{$name}/index.blade.php"), $viewIndexTemplate);
        file_put_contents(resource_path("views/admin/{$name}/form.blade.php"), $viewFormTemplate);
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        $this->controller($name);
        $this->model($name);
        $this->view($name);

        //create api route
        File::append(base_path('routes/web.php'),
            "
                Route::get('" . str_plural(strtolower($name)) . "/', '{$name}Controller@index');
                Route::get('" . str_plural(strtolower($name)) . "/serverside', '{$name}Controller@serverside');
                Route::get('" . str_plural(strtolower($name)) . "/create', '{$name}Controller@create');
                Route::post('" . str_plural(strtolower($name)) . "/store', '{$name}Controller@store');
                Route::get('" . str_plural(strtolower($name)) . "/edit/{id}', '{$name}Controller@edit');
                Route::post('" . str_plural(strtolower($name)) . "/update', '{$name}Controller@update');
                Route::get('" . str_plural(strtolower($name)) . "/delete/{id}', '{$name}Controller@delete');
            "
        );
    }
}
