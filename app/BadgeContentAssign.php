<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeContentAssign extends Model
{
    protected $table = 'badges_content_assign';
}
