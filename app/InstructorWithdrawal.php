<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorWithdrawal extends Model
{
  protected $table = 'instructor_withdrawals';
}
