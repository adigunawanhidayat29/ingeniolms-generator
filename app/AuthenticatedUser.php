<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthenticatedUser extends Model
{
    //
    protected $table = 'authenticated_users';

    protected $fillable = [
        'user_id', 'token', 'expired_at', 'session_id'
    ];
}
