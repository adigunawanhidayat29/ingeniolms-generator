<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizPageBreak extends Model
{
    protected $table = 'quiz_page_breaks';

    public function quiz_questions(){
      return $this->hasMany('App\QuizQuestion', 'quiz_page_break_id', 'id')->with('quiz_question_answers')->orderBy('sequence', 'asc');
    }
}
