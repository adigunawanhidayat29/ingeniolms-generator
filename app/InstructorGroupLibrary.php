<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorGroupLibrary extends Model
{
    public function content(){

      return $this->belongsTo('App\LibraryDirectoryContent', 'content_id');
    }

    public function folder(){

      return $this->belongsTo('App\LibraryDirectoryGroup', 'folder_id');
    }
}
