<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    public function getImageAttribute()
    {
        return $this->attributes['image'];
    }

    public function certificate_attributes() {
        return $this->hasMany('App\CeritificateAttribute', 'ceritificate_id', 'id');
    }
}
