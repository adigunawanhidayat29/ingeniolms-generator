<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  public function courses()
  {
    return $this->hasMany('App\Course', 'id_category', 'id')
      ->select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->join('users','users.id','=','courses.id_author')
      ->where('courses.public','1')
      ->where('courses.status','1')
      ->where('courses.archive','0')
      ->whereNotIn('courses.id',function($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->orderBy('courses.id', 'desc');
  }

  public function getIconAttribute()
  {
      return $this->attributes['icon'] ? asset_url() . $this->attributes['icon'] : asset('default.jpg');
  }

  public function parent() {
      return $this->belongsTo('App\Category', 'parent_category'); //get parent category
  }

  public function children() {
      return $this->hasMany('App\Category', 'parent_category')->with('courses'); //get all subs. NOT RECURSIVE
  }
}
