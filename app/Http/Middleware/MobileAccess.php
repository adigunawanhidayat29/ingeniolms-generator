<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;

class MobileAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->provider!='ingenio_access_id' || $request->access_id!=env('MOBILE_LOGIN_ACCESS_ID')) {
            return response()->json([
              'message' => 'error',
              'data' => 'Invalid Access ID or Access ID is Missing, The end point not valid'
            ], 406);
        }

        return $next($request);
    }
}
