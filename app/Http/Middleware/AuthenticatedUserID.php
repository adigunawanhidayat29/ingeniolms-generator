<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;

use App\Http\Controllers\Api\App\AuthUser;

class AuthenticatedUserID
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('auth')) {
            $User = AuthUser::User($request->get('auth'));
            if($User!=null){
                return $next($request);
            }
            return response()->json([
                'error' => 403,
                'message' => 'Your Session expired'
            ]);
        }
        return response()->json([
            'error' => 403,
            'message' => 'Authentication Required'
        ]);
    }
}
