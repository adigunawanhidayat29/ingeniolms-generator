<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;
use App\Application;
class ThirdPartyAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$hostname =  $request->headers->has('Origin') ? $request->header('Origin') : $request->header('hostname');
        //$authorization = $request->header('Authorization');
        $code = 0;
        $message = '';
        if ($request->headers->has('Authorization')) {
           $code = isApiRegistered($request->header('Authorization'));
        }else {
            $code = 4;
        }

        if ($code==0) {
            return $next($request);
        }else if($code==1){
            $message = 'API KEY Not Registered';
        }else if($code==2){
            $message = 'API KEY Expired';
        }else if($code==3){
            $message = 'Your API KEY has blocked by Administrator';
        }else {
            $message = 'API KEY Required';
        }

        
        return response()->json([
            'error' => 403,
            'message' => $message
        ]);
        
    }
}
