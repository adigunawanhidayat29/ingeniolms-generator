<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Manager as ManagerModel;

class Manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $User = Auth::user();
      $Manager = ManagerModel::where(['user_id' => $User->id, 'status' => '1']);
      if($Manager->first()){
        return $next($request);
      }else{
        return redirect('become-a-manager');
      }
    }
}
