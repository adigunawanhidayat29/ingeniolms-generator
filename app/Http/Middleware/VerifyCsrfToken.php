<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/landingpage/save',
        '/landingpage/user/new',
        '/landingpage/user/save',
        'login',
        'upload/file/store-data',
        'instructor/library',
        'instructor/library/delete',
        'course/content/library/store',
        'library/folder/create',
        'share/content/add',
        'course/quiz/library/store',
        'library/question/quiz/submit',
        'ckeditor/uploader',
        'html/save',
        '/landingpage/sites/create/new_action',
        '/landingpage/change/permalink/*',
        '/landingpage/set/publish',
        '/landingpage/site/logo',
        '/testing/register/domain',
        '/users/sites/by/email',
        '/sites/get/favicon',
        '/sites/custom/domain',
        '/sites/info/by/domain',
        '/sites/info/by/domain/value',
        '/get/course/by/domain',
        '/api/builder/theme/*/*',
        '/api/builder/site/info',
        '/api/builder/asset/upload',
        '/api/builder/sites/edit',
        '/api/builder/pages/store',
        '/api/builder/generate',
        '/api/application/create/new',
        '/api/builder/customdomain/update',
        '/api/builder/application/update',
        '/course/content',
        'admin/users/all/import-action',
        '/api/client/api-key'
    ];
}
