<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Instructor as InstructorModel;

class Instructor
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    $User = Auth::user();
    $Instructor = InstructorModel::where(['user_id' => $User->id, 'status' => '1']);
    if ($Instructor->first()) {
      return $next($request);
    } else {
      return redirect('course/dashboard');
    }
  }
}
