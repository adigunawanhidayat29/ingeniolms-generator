<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->get('token'))) {
            return response()->json(array(
                'message' => 'Unauthorized',
                'error' => 'User token is missing',
            ), 200);
        }

        $token = $request->get('token');
  
        //Process Token (Complicated like a charm)
        $isLengthTokenValid = strlen($token)==64 ? true : false;
        $isTokenValid = false;
        if ($isLengthTokenValid) {
          $isTokenValid = (substr($token,5,6)=='Dfbh-c' && explode('.', $token)[1]=='121a') ? true : false;
        }
        //=======================================
  
        if ($isTokenValid==false) {
              return response()->json(array(
                  'message' => 'Unauthorize',
                  'error' => 'Invalid user token or session expired',
              ), 200);
        }
        return $next($request);
    }
}
