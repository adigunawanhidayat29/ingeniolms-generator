<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::user()){
        $User = Auth::user();
        $UserGroup = DB::table('users_groups')->where(['user_id' => $User->id, 'level_id' => '1']);
        if($UserGroup->first()){
          return $next($request);
        }
      }      
      return redirect('admin/login');
      
    }
}
