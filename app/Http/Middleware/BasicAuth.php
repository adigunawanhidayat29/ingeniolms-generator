<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
            This is a basic authentication middleware procedure
            Secondary credentials for API access required Authentication
            Simply to input username and password as key access to API
            Thanks to https://medium.com/oceanize-geeks/laravel-middleware-basic-auth-implementation-88b777361b5c
            For helping me
        */
        
        // User is Logged In
        if (Auth::check()) {
            return $next($request);
        }

        // 

        // Using Basic Authentication
        $isAccoutValid = false;
        // header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));

        // Login Procedure
        if ($has_supplied_credentials) {
            if (Auth::attempt(['email' => $_SERVER['PHP_AUTH_USER'], 'password' => $_SERVER['PHP_AUTH_PW']])) {
                $isAccoutValid = true;
            }
        }

        // Checking Authentication
        $is_not_authenticated = (
            !$has_supplied_credentials || !$isAccoutValid
        );
        if ($is_not_authenticated) {
            // header('HTTP/1.1 401 Authorization Required');
            // header('WWW-Authenticate: Basic realm="Access denied"');
            return response()->json([
                'error' => 403,
                'message' => 'Authentication Required'
            ], 403);
        }
        return $next($request);
        
    }
}
