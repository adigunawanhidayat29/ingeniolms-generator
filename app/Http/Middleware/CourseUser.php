<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;

class CourseUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $assignment_id = $request->route('id');
      $Assignment = AssignmentModel::where('assignments.id', $assignment_id)->join('sections','sections.id' , '=','assignments.id_section')->join('courses','courses.id','=','sections.id_course')->first();
      $User = Auth::user();
      $CourseUser = CourseUserModel::where(['user_id' => $User->id, 'course_id' => $Assignment->id_course]);
      if($CourseUser->first()){
        return $next($request);
      }else{
        return redirect('course/'.$Assignment->slug);
      }
    }
}
