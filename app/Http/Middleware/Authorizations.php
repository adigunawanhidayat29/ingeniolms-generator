<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\CourseUser as CourseUserModel;
use App\Course as CourseModel;
use App\Assignment as AssignmentModel;

class Authorizations
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');
        $parse_header = explode(" ", $header);
        if (count($parse_header)<2) {
              return response()->json(array(
                  'message' => 'Unauthorize',
                  'data' => 'User token is missing',
                  'code' => 401
              ), 401);
        }
  
        //Process Token (Complicated like a charm)
        $isLengthTokenValid = strlen($parse_header[1])==64 ? true : false;
        $isTokenValid = false;
        if ($isLengthTokenValid) {
          $isTokenValid = (substr($parse_header[1],5,6)=='Dfbh-c' && explode('.', $parse_header[1])[1]=='121a') ? true : false;
        }
        //
  
        if ($parse_header[0]!='Bearer' || $isTokenValid==false) {
  
              return response()->json(array(
                  'message' => 'Unauthorize',
                  'data' => 'Invalid user token or session expired, please re-login',
                  'code' => 401
              ), 401);
        }
        return $next($request);
    }
}
