<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index () {
        return view('auth.passwords.email');
    }

    public function store (Request $request) {

        $request->validate([
            'email' => 'required|email|exists:users',
        ]);
    
        $token = str_random(64);
    
        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
        );
        
        try{
            \Mail::send('emails.user.forgot-password', ['token' => $token], function($message) use ($request) {
                $message->to($request->email);
                $message->subject(Setting('title')->value . ' - Reset Password Notification');
                $message->from(Setting('MAIL_FROM')->value, Setting('MAIL_SENDER')->value);
            });
            \Session::flash('status', 'We have e-mailed your password reset link!');
        }
        catch(\Exception $e){
            \Session::flash('warning', 'Hubungi Administrator untuk mendapatkan email lupa password');
        }

        return back();
    }
}
