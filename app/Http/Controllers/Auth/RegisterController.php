<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserGroup;
use App\Instructor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use Illuminate\Http\Request;
use App\Mail\InstructorJoin;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      // 'phone' => 'required|string|min:8|max:20',
      'password' => 'required|string|min:6|confirmed',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  protected function create(array $data)
  {
    $language = \App\Models\Language::where('status', '1')->first();
    $Register = User::create([
      'name' => $data['name'],
      'email' => $data['email'],
      'username' => $data['email'],
      // 'phone' => $data['phone'],
      'is_active' => isset(Setting('user_verify')->value) && Setting('user_verify')->value == '1' ? '0' : '1',
      'slug' => str_slug($data['name']),
      'activation_token' => str_random(40),
      'auth_token' => str_random(60),
      'password' => bcrypt($data['password']),
      'language' => $language->alias,
    ]);

    $UserGroup = new UserGroup;
    $UserGroup->user_id = $Register['id'];
    $UserGroup->level_id = '3';
    $UserGroup->save();

    // check if register instruktor
    if (isset($data['instructor'])) {
      if ($data['instructor'] == 'true') {

        // insert instructor
        $Instructor = new Instructor;
        $Instructor->user_id = $Register['id'];
        $Instructor->status = '0';
        $Instructor->save();
        // insert instructor

        $UserGroup = new UserGroup;
        $UserGroup->user_id = $Register['id'];
        $UserGroup->level_id = '2';
        $UserGroup->save();

        // Mail::to(['tsauri@dataquest.co.id'])->send(new InstructorJoin($Instructor));
      }
    }
    // check if register instruktor

    if (isset(Setting('user_verify')->value) && Setting('user_verify')->value == '1') {
      try {
        Mail::to($data['email'])->send(new Register($Register));
        // \Mail::send('emails.user.register', $data, function($message) use ($data) {
        //   $message->to($data['email'], $data['name'])
        //   ->subject("Register " . Setting('title')->value);
        //   $message->from(Setting('MAIL_FROM')->value, Setting('MAIL_SENDER')->value);
        // });
        // \Mail::to($data['email'], $data['name'])
        //   ->from(Setting('MAIL_FROM')->value, Setting('MAIL_SENDER')->value)
        //   ->subject("Register " . Setting('title')->value)
        //   ->send(new Register($Register));
      } catch (\Exception $e) {
        \Session::flash('warning', 'Hubungi Administrator untuk mendapatkan email informasi');
      }
    }

    return $Register;
  }

  // protected function registered(Request $request, $user)
  // {
  //     $this->guard()->logout();
  //     return redirect('/login')->with('status', 'Kami mengirim link aktivasi untuk mengaktifkan akun Anda, Silakan periksa email Anda.');
  // }
}
