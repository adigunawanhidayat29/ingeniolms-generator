<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\DB;
use App\SsoAuth;
use App\User;
use App\UserGroup;
use App;

use App\Http\Controllers\Api\App\AuthUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
      // if (!$user->is_active) {
      //   // dd($user);
      //   Auth::logout();
      //   // \Session::flash('warning','Kamu harus mengkonfirmasi akun kamu. Kami sudah mengirim kode aktivasi, silakan periksa email Kamu.');
      //   return redirect('login')->with('warning','Kamu harus mengkonfirmasi akun kamu. Kami sudah mengirim kode aktivasi, silakan periksa email Kamu.');
      // }

      if(Auth::user()->auth_token==null){
        User::where('id', Auth::user()->id)->update(['auth_token' => str_random(60)]);
      }

      // set user language
      $request->session()->put(['locale' => Auth::user()->language]);
      App::setLocale(Auth::user()->language);
      // set user language

      return redirect()->intended(url()->previous());
    }

    public function username()
    {
      $login = request()->input('email');
      $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
      request()->merge([$field => $login]);
      return $field;
    }

    public function logout(Request $request) {

      $user_id = Auth::user()->id;

      $check_user_employeer = UserGroup::where('user_id', $user_id)->where('level_id', 6)->count();
      $check_user_provider = UserGroup::where('user_id', $user_id)->where('level_id', 7)->count();

      if($check_user_employeer > 0 || $check_user_provider > 0){
        Auth::logout();
        return redirect('/login');
      }
      else{
        Auth::logout();
        return redirect()->intended(url()->previous());
      }
    }
}
