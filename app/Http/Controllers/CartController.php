<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Course;
use App\Program;
use App\Promotion;
use App\PromotionUser;
use Session;
use Auth;

class CartController extends Controller
{
  public function index()
  {
    $data = [
      'carts' => Cart::content(),
    ];
    return view('cart.index', $data);
  }

  public function get($rowId)
  {
    $cart = Cart::get($rowId);
    dd($cart);
  }

  public function add($id, Request $request)
  {
    if ($request->get('aff')) {
      $affiliate = '?aff=' . $request->get('aff');
    } else {
      $affiliate = '';
    }
    $Course = Course::find($id);
    Cart::add([
      'id' => $Course->id,
      'name' => $Course->title,
      'qty' => 1,
      'price' => $Course->price,
      'options' => [
        'image' => $Course->image,
        'affiliate' => $affiliate,
        'type' => 'course',
      ]
    ]);
    return redirect('cart');
  }

  public function buy($id, Request $request)
  {
    if ($request->get('aff')) {
      $affiliate = '?aff=' . $request->get('aff');
    } else {
      $affiliate = '';
    }
    $Course = Course::find($id);
    Cart::add([
      'id' => $Course->id,
      'name' => $Course->title,
      'qty' => 1,
      'price' => $Course->price,
      'options' => [
        'image' => $Course->image,
        'affiliate' => $affiliate,
        'type' => 'course',
      ]
    ]);

    return redirect('cart/checkout/v2' . $affiliate);
  }

  public function add_program($id, Request $request)
  {
    if ($request->get('aff')) {
      $affiliate = '?aff=' . $request->get('aff');
    } else {
      $affiliate = '';
    }
    $Program = Program::find($id);
    Cart::add([
      'id' => $Program->id,
      'name' => $Program->title,
      'qty' => 1,
      'price' => $Program->price,
      'options' => [
        'image' => $Program->image,
        'affiliate' => $affiliate,
        'type' => 'program',
      ]
    ]);
    return redirect('cart');
  }

  public function buy_program($id, Request $request)
  {
    if ($request->get('aff')) {
      $affiliate = '?aff=' . $request->get('aff');
    } else {
      $affiliate = '';
    }
    $Program = Program::find($id);
    Cart::add([
      'id' => $Program->id,
      'name' => $Program->title,
      'qty' => 1,
      'price' => $Program->price,
      'options' => [
        'image' => $Program->image,
        'affiliate' => $affiliate,
        'type' => 'program',
      ]
    ]);

    return redirect('cart/checkout' . $affiliate);
  }

  public function remove($rowId)
  {
    Cart::remove($rowId);
    return redirect('cart');
  }

  public function checkout()
  {
    $data = [
      'carts' => Cart::content(),
    ];
    return view('cart.checkout', $data);
  }

  public function checkout_v2()
  {
    $data = [
      'carts' => Cart::content(),
    ];
    return view('cart.checkout_v2', $data);
  }

  public function promotion(Request $request)
  {
    $Promotion = Promotion::where([
        'discount_code' => $request->discount_code,
        'status' => '1',
      ])
      ->where('start_date', '<=', date("Y-m-d H:i:s"))
      ->where('end_date', '>=', date("Y-m-d H:i:s"))
      ->first();

    if ($Promotion) {
      $PromotionUser = PromotionUser::where(['promotion_users.user_id' => Auth::user()->id, 'promotion_users.promotion_id' => $Promotion->id, 'transactions.status' => '1'])
        ->join('transactions', 'transactions.invoice', '=', 'promotion_users.invoice')
        ->first();
      if ($PromotionUser) {
        Session::flash('error', 'Kode sudah dipakai');
      } else {
        Session::put('promotion', $Promotion);
        Session::flash('success', 'Berhasil');
      }
    } else {
      Session::flash('error', 'Kode tidak ditemukan');
    }

    return redirect('cart/checkout/v2');
  }
}
