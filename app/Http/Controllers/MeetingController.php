<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\MeetingSchedule;

class MeetingController extends Controller
{
  public function publish(Request $request){
    $Meeting = Meeting::where('course_id', $request->id)->first();
    $Meeting->publish = $request->publish;
    $Meeting->save();
  }

  public function store(Request $request){
    $MeetingSchedule = new MeetingSchedule;
    $MeetingSchedule->course_id = $request->course_id;
    $MeetingSchedule->name = $request->name;
    $MeetingSchedule->description = $request->description;
    $MeetingSchedule->date = $request->date;
    $MeetingSchedule->time = $request->time;
    $MeetingSchedule->recording = $request->recording;
    $MeetingSchedule->save();

    return redirect()->back();
  }

  public function update(Request $request){
    $MeetingSchedule = MeetingSchedule::where('id', $request->id)->first();
    $MeetingSchedule->name = $request->name;
    $MeetingSchedule->description = $request->description;
    $MeetingSchedule->date = $request->date;
    $MeetingSchedule->time = $request->time;
    $MeetingSchedule->recording = $request->recording;
    $MeetingSchedule->save();

    return redirect()->back();
  }

  public function destroy($id){
    $MeetingSchedule = MeetingSchedule::where('id', $id)->delete();
    return redirect()->back();
  }
}
