<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseLeveling;
use App\CourseUser;
use App\Content;
use App\LevelingRule;
use App\LevelingRulesContent;
use App\UserLevelingLog;
use App\RulesCriteria;
use App\Course;
use File;

class CourseLevelingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function settings($course_id)
    {
        $course_leveling = CourseLeveling::where('course_id', $course_id)->get();

        $course = Course::where('id', $course_id)->first();

        $course_user = CourseUser::join('users', 'courses_users.user_id', '=', 'users.id')
        ->where('course_id', $course_id)
        ->select('users.name as user_name', 'courses_users.level_point', 'courses_users.course_id', 'courses_users.level', 'users.id as user_id')
        ->orderBy('courses_users.level_point', 'desc')
        ->get();

        $course_content = Content::join('sections', 'contents.id_section', '=', 'sections.id')
        ->join('courses', 'sections.id_course', '=', 'courses.id')
        ->where('courses.id', $course_id)
        ->select('contents.title as content_title', 'contents.id as content_id')
        ->get();

        $users_leveling_logs = UserLevelingLog::join('users', 'users_leveling_logs.user_id', '=', 'users.id')
        ->select('users_leveling_logs.leveling_rules_id as leveling_rules_id', 'users.name as user_name', 'users_leveling_logs.created_at as log_created_at', 'users_leveling_logs.reward as log_reward')
        ->where('course_id', $course_id)->get();

        $rules_criteria = RulesCriteria::all();

        $data = [
            'rules_criteria' => $rules_criteria,
            'users_leveling_logs' => $users_leveling_logs,
            'course_content' => $course_content,
            'course_leveling' => $course_leveling,
            'course_user' => $course_user,
            'course_id' => $course_id,
            'course' => $course
        ];

        return view('course_leveling.settings', $data);
    }

    public function update_levels(Request $request, $course_id)
    {
        $level_name_array = $request->level_name;
        $points_required_array = $request->points_required;
        $description_array = $request->description;
        $level_array = $request->level;

        $leveling_count = CourseLeveling::where('course_id', $course_id)->count();
        $course_leveling = CourseLeveling::where('course_id', $course_id)->where('level', '!=', '1')->where('level', '!=', '2')->delete();     

        for($i = 0; $i < count($level_name_array); $i++)
        {
            if($level_array[$i] > 2)
            {
                $leveling = new CourseLeveling;
                $leveling->level_name = $level_name_array[$i];
                $leveling->points_required = $points_required_array[$i];
                $leveling->level_description = $description_array[$i];
                $leveling->course_id = $course_id;
                $leveling->level = $level_array[$i];
                $leveling->save();
            }

            else
            {
                $leveling = CourseLeveling::where('level', $i+1)->first();
                $leveling->level_name = $level_name_array[$i];
                $leveling->points_required = $points_required_array[$i];
                $leveling->level_description = $description_array[$i];
                $leveling->save();
            }
        }

        return redirect($course_id.'/level_settings');
    }

    public function update_rules(Request $request)
    {
        $rules_array = $request->rule;
        $rules_count = count($rules_array);
        
        for($j = 0; $j < $rules_count; $j++)
        {
            $leveling_rules = new LevelingRule;
            $leveling_rules->points_earned = $rules_array[$j]["points_earned"];
            $leveling_rules->complete_type = $rules_array[$j]["complete_type"];
            $leveling_rules->save();

            $criteria_data = $rules_array[$j]["rules_criteria_id"];
            $content_data = $rules_array[$j]["content_id"];
            $content_count = count($content_data);
        
            for($i = 0; $i < $content_count; $i++)
            {
                $rules_content = new LevelingRulesContent;
                $rules_content->content_id = $content_data[$i];
                $rules_content->leveling_rules_id = $leveling_rules->id;
                $rules_content->rules_criteria_id = $criteria_data[$i];
                $rules_content->save();
            }
        }

        return 'sukses';
    }

    public function update_settings(Request $request, $course_id)
    {
        $course = Course::find($course_id);
        $course->level_notif_status = $request->notification ? '1' : '0';
        $course->save();

        return 'sukses';
    }

    public function update_visuals(Request $request, $course_id)
    {
        $course = Course::find($course_id);

        if($request->img != null) {
            if($course->levelup_img == null)
            {
                $fileName = time().'_'.$request->img->getClientOriginalName();
                $filePath = $request->file('img')->storeAs('levelup_img', $fileName, 'public');
                $course->levelup_img = $fileName;
            }

            else
            {
                if(file_exists(public_path('storage/levelup_img/'. $course->levelup_img)))
                {
                    File::delete(public_path('storage/levelup_img/' . $course->levelup_img));
                }

                $fileName = time().'_'.$request->img->getClientOriginalName();
                $filePath = $request->file('img')->storeAs('levelup_img', $fileName, 'public');
                $course->levelup_img = $fileName;
            }

            $course->save();

            return 'sukses';
        }
    }

    public function edit_reports($course_id, $user_id)
    {
        $course_user = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->first();
        $course = Course::where('id', $course_id)->first();

        $data = [
            'course_user' => $course_user,
            'course' => $course
        ];
        
        return view('course_leveling.edit_reports', $data);
    }

    public function update_reports(Request $request, $course_id, $user_id)
    {
        $course_user = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->first();

        $course_level = CourseLeveling::where('course_id', $course_id)->get();

        $course_user->level_point = $request->points;

        foreach($course_level as $data_course_level)
        {
            //checking data course leveling
            if(!empty($data_course_level))
            {
            //checking point to get next level
                if($course_user->level_point >= $data_course_level->points_required)
                {
                    $course_user->level = $data_course_level->level;
                }
            }     
        }

        $course_user->save();

        return redirect('/' . $course_id . '/level_settings');
    }
}
