<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\QuizParticipant;
use App\QuizParticipantAnswer;
use App\Assignment;
use App\AssignmentAnswer;
use App\AssignmentGrade;
use App\GradeSetting;
use Auth;

class GradingController extends Controller
{
    public function index(){
		$quiz = Quiz::all();
		$assignment = Assignment::all();

		$data = [
			'quiz' => $quiz,
			'assignment' => $assignment,
		];

		return  view('grading.status-grading', $data);
	}

	public function get_quiz($id){
		$quiz = Quiz::where('id', $id)->first();
		$assignment = Assignment::where('id', $id)->first();

		$a = QuizParticipant::select('users.name', 'quiz_participants.user_id', 'quiz_participants.quiz_id', 'quiz_participants.time_start', 'quiz_participants.time_end', 'quiz_participants.grade')
		->where('quiz_id', $id)
		->join('quizzes', 'quizzes.id', '=', 'quiz_participants.quiz_id')
		->join('users','users.id','=','quiz_participants.user_id')->get();

		// $b = AssignmentGrade::('users.name', 'assignments_grades.assignment_id', 'assignments_grades.assignment_answer_id', 'assignments_grades.grade',

		$data = [
		  'kuis'=> $quiz,
		  'a'=>$a,
		  'ass'=>$assignment,
		];

		return $data;
	}

  public function setting($course_id){
    $quizzes = Quiz::select('quizzes.*')
      ->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->where('sections.id_course', $course_id)
      ->where('quizzes.status', '1')
      ->get();

    $assignments = Assignment::select('assignments.*')
      ->join('sections', 'sections.id', '=', 'assignments.id_section')
      ->where('sections.id_course', $course_id)
      ->where('assignments.status', '1')
      ->get();

    $data = [
      'quizzes' => $quizzes,
      'assignments' => $assignments,
    ];
    return view('grading.setting', $data);
  }

  public function setting_save($course_id, Request $request){

    foreach($request->table_id as $key => $value){
      $_checkGradeSetting = GradeSetting::where(['course_id' => $course_id, 'table_name' => $request->table_name[$key], 'table_id' => $value])->first();
      if(!$_checkGradeSetting){
        $GradeSetting = new GradeSetting;
        $GradeSetting->course_id = $course_id;
        $GradeSetting->table_name = $request->table_name[$key];
        $GradeSetting->table_id = $value;
        $GradeSetting->percentage = $request->percentage[$key];
        $GradeSetting->save();
      }else{
        $_checkGradeSetting->course_id = $course_id;
        $_checkGradeSetting->table_name = $request->table_name[$key];
        $_checkGradeSetting->table_id = $value;
        $_checkGradeSetting->percentage = $request->percentage[$key];
        $_checkGradeSetting->save();
      }
    }

    // dd($request);

    return redirect('course/grades/' . $course_id);
  }
}
