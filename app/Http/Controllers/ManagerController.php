<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use Auth;

class ManagerController extends Controller
{

  public function welcome(){
    return view('manager.welcome');
  }

  public function join(){
    $CheckManager = Manager::where('user_id', Auth::user()->id);
    if($CheckManager->first()){
      return view('manager.join_success');
    }else{
      $Manager = new Manager;
      $Manager->user_id = Auth::user()->id;
      $Manager->status = '0';
      $Manager->save();

      return redirect('become-a-manager/join/success');
    }
  }

  public function join_success(){
    return view('manager.join_success');
  }

}
