<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Site;
use App\SiteElement;
use App\Template;
use App\SitePublish;
use App\SiteGlobal;
use App\SitePage;
use App\User;
use App\SiteValue;
use App\SiteUserPage;
use App\Application;
use Auth;
use Validator;

class SiteController extends Controller{

    public function index($subdomain){
        $SitePage = DB::table('site_globals')->select('site_globals.domain', 'site_pages.*')
            ->join('site_pages', 'site_pages.id_site_global', '=', 'site_globals.id')
            ->where(['site_globals.domain' => $subdomain, 'url_prefix' => '/'])
            ->first();
        if (empty($SitePage)) {
            return view('errors.404');
        }

        $data = [
            'html' => $SitePage->html_compiled,
            'title' => $SitePage->title,
            'description' => $SitePage->description,
            'font' => $SitePage->font,
        ];

        return view('site.landingpage_user', $data);
    }

    public function pages($subdomain, $any){
        $SitePage = DB::table('site_globals')->select('site_globals.domain', 'site_pages.*')
            ->join('site_pages', 'site_pages.id_site_global', '=', 'site_globals.id')
            ->where(['site_globals.domain' => $subdomain, 'url_prefix' => $any])
            ->first();
        if (empty($SitePage)) {
            return view('errors.404');
        }

        $data = [
            'html' => $SitePage->html_compiled,
            'title' => $SitePage->title,
            'description' => $SitePage->description,
            'font' => $SitePage->font,
        ];

        return view('site.landingpage_user', $data);
    }

    public function index__($subdomain){
        $MySite = DB::table('site_publishes')->select('site_publishes.data_html', 'sites.site_title as title', 'sites.description', 'sites.font')->where(['site_publishes.url' => $subdomain, 'site_publishes.status' => 1])
        ->join('sites', 'sites.id', '=', 'site_publishes.id_site')
        ->first();
        if (empty($MySite)) {
            return view('errors.404');
        }

        $data = [
            'html' => $MySite->data_html,
            'title' => $MySite->title,
            'description' => $MySite->description,
            'font' => $MySite->font,
        ];

        return view('site.landingpage_user', $data);

    }

    public function getStatus($id_site){
        $SitePublish = SitePublish::where('id_site', $id_site)->first();
        return response()->json([
            'error' => 0,
            'message' => 'Success',
            'data' => $SitePublish->status,
        ]);
    }

    public function setStatus($id_site, Request $request){
        SitePublish::where('id_site', $id_site)->update(['status' => $request->get('status')]);
        return response()->json([
            'error' => 0,
            'message' => 'Success',
        ]);
    }

    public function saveCompiledHtml_(Request $request) {
        SitePublish::where('id_site', $request->id_site)->update(['data_html' => $request->data_html]);
        return response()->json([
            'error' => 0,
            'message' => 'Success',
        ]);
    }

    public function saveCompiledHtml(Request $request) {
        SitePage::where('id', $request->id_site)->update(['html_compiled' => $request->data_html]);
        return response()->json([
            'error' => 0,
            'message' => 'Success',
        ]);
    }

    public function index_($slug){
        $MySite = Site::select('sites.*', 'users.slug as user_slug', 'users.name as instructor', 'users.photo', 'users.facebook as fb', 'users.instagram as ig', 'users.twitter as twt')->where(['sites.slug' => $slug])
            ->join('users', 'users.id', '=', 'sites.user_id')
            ->first();
        if (empty($MySite)) {
            return view('errors.404');
        }
        $data = [
            'title' => $MySite->site_title,
            'subtitle' => $MySite->site_title,
            'favicon' => '',
            'body' => $MySite->data_html,
            'font' => 'Montserrat'
        ];
        return view('site.template_b', $data);
    }

    public function preview_template($id){
        $Template = Template::where('id', $id)->first();
        $data = [
            'title' => 'Preview',
            'subtitle' => 'Preview',
            'favicon' => '',
            'body' => $Template->data_html,
            'font' => 'Montserrat'
        ];
        return view('site.template_b', $data);
    }

    public function save_template(Request $request){
        try {
            $Template = new Template;
            $Template->data_json = $request->data_json;
            $Template->data_html = $request->data_html;
            $Template->save();
            return response()->json([
                'message' => 'Success'
            ], 200);
        }catch(\Exception $ex){
            return response()->json([
                'message' => 'Failed',
                'error' => $ex,
            ], 200);
        }
    }

    public function save_project(Request $request){
        try {
            $userSite = [
                'data_json' => $request->data_json, 
                'data_html' => $request->data_html,
                'title' => $request->title,
                'description' => $request->description,
                'submission_form' => $request->submission_form,
                'font' => $request->font,
            ];
            SitePage::where('id', $request->id)->update($userSite);
            return response()->json([
                'message' => 'Success'
            ], 200);
        }catch(\Exception $ex){
            return response()->json([
                'message' => 'Failed',
                'error' => $ex,
            ], 200);
        }
    }

    public function get_template($id){
        $Template = Template::where('id', $id)->first();
        return response()->json([
            'message' => 'Success',
            'data' => $Template->data_json,
        ], 200);
    }

    public function get_all_template(){
        $data = [];
        $Templates = Template::all();
        foreach($Templates as $Template){
            array_push($data, [
                'id' => $Template->id,
                'thumbnail' => $Template->thumbnail
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'data' => $data,
        ]);
    }

    public function get_site($id){
        $MySite = SitePage::where('id', $id)->first();
        return response()->json([
            'message' => 'Success',
            'data' => [
                'json' => $MySite->data_json,
                'html' => $MySite->data_html,
                'title' => $MySite->title,
                'description' => $MySite->description,
                'font' => $MySite->font,
            ],
        ], 200);
    }

    public function site_creator(){
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first();
        if (!$SiteGlobal) {
            return redirect('landingpage/sites/create/new');
        }
        return view ('site.dashboard');
    }

    public function get_user_site(){
        $User = Auth::user();
        $Sites = [];
        $MySites = SiteGlobal::select('site_pages.url_prefix', 'site_pages.title as site_title','site_pages.id as page_id', 'site_globals.*', 'users.name as author', 'site_pages.created_at as created')
        ->join('users', 'users.id', '=', 'site_globals.user_id')
        ->join('site_pages', 'site_pages.id_site_global', '=', 'site_globals.id')
        ->where('user_id', '=', $User->id)
        ->get();
        foreach($MySites as $MySite){
            array_push($Sites, [
                'site_id' => $MySite->page_id,
                'site_title' => $MySite->site_title,
                'author' => $MySite->author,
                'domain' => $MySite->url_prefix=='/' ? 'https://'.$MySite->domain : 'https://'.$MySite->domain.'/'.$MySite->url_prefix,
                'created_at' => $MySite->created,
                'isRoot' => $MySite->url_prefix=='/' ? true : false,
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'data' => $Sites,
        ], 200);
    }

    public function get_site_domain_by_email(Request $request) {
        $User = User::where('email', $request->email)->first();
        if(!$User){
            return response()->json([
                'error' => 406,
                'message' => 'User not found',
            ], 200);
        }
        $SiteInfo = SiteGlobal::where('user_id', $User->id)->first();
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $SiteInfo,
        ]);
    }

    public function get_favicon_site_by_email(Request $request){
        $User = User::where('email', $request->email)->first();
        if(!$User){
            return response()->json([
                'error' => 406,
                'message' => 'User not found',
            ], 200);
        }
        $SiteInfo = SiteGlobal::where('user_id', $User->id)->first();
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $SiteInfo,
        ]);
    }

    // Testing function for client page
    public function get_course_from_domain(Request $request){
        $domain = $request->domain;
        $data = [];
        if ($domain=='adigunawanhidayat222.ingenio.co.id') {
            $data = [
                [
                    'course_name' => 'Be one of the world'
                ],
                [
                    'course_name' => 'Changes everything you want'
                ],
                [
                    'course_name' => 'The miracle of `I can do it`'
                ],
                [
                    'course_name' => 'Great must espiron'
                ],
            ];
        }else if($domain=='ingenio.okebdg.com'){
            $data = [
                [
                    'course_name' => 'Good to be better'
                ],
                [
                    'course_name' => 'Changes everything you want'
                ],
                [
                    'course_name' => 'Great undertaker'
                ],
                [
                    'course_name' => 'Feel you can changes'
                ],
                [
                    'course_name' => 'True friend'
                ],
            ];
        }
        
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function get_site_info_by_domain(Request $request){
        $domain = $request->domain;
        $Sites = SiteGlobal::where('domain', $domain)->orWhere('custom_domain', $domain)->first();
        if ($Sites) {
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $Sites
            ]);
        }
        return response()->json([
            'error' => 404,
            'message' => 'Site Not Found, or is Empty',
        ]);
    }

    public function get_site_info_by_domain_with_value(Request $request){
        $domain = $request->domain;
        $Sites = SiteGlobal::where('domain', $domain)->orWhere('custom_domain', $domain)->first();
        $Sites->site_logo_small = ClientAssetUrl($Sites->user_id . '/landingpage_' . basename($Sites->site_logo));
        $SiteValue = SiteValue::where('user_id', $Sites->user_id)->first();
        if ($Sites) {
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $Sites,
                'value' => $SiteValue,
            ]);
        }
        return response()->json([
            'error' => 404,
            'message' => 'Site Not Found, or is Empty',
        ]);
    }

    public function get_site_info_by_user_id_with_value(Request $request){
        $user_id = $request->ingenio_user_id;
        $Sites = SiteGlobal::where('user_id', $user_id)->first();
        $SiteValue = SiteValue::where('user_id', $user_id)->first();
        $InstructorGroups = DB::table('instructor_groups')->where('created_by', $user_id)->get();
        $isGroupActiveOnThisSite = $Sites->application_token;
        if($isGroupActiveOnThisSite==null || $isGroupActiveOnThisSite==''){
            $activeGroup = false;
        }else{
            $Application = Application::where('api_key', $isGroupActiveOnThisSite)->first();
            if(empty($Application)){
                $activeGroup = false;
            }else{
                $activeGroup = DB::table('instructor_groups')->where('id', $Application->id_instructor_group)->first();
            }
        }
        if($Sites){
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $Sites,
                'value' => $SiteValue,
                'list_instructor_groups' => $InstructorGroups,
                'active_group' => $activeGroup,
            ]);
        }
        return response()->json([
            'error' => 404,
            'message' => 'Site Not Found, or is Empty',
        ]);
    }

    public function get_site_info(){
        $User = Auth::user();
        $Sites = SiteGlobal::where('user_id', $User->id)->first();
        if ($Sites) {
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $Sites
            ]);
        }
        return response()->json([
            'error' => 404,
            'message' => 'Site Not Found, or is Empty',
        ]);
    }

    public function open_builder(){
        $User = Auth::user();
        $Sites = SiteGlobal::where('user_id', $User->id)->first();
        if($Sites){
            return redirect('https://pagebuilder.ingenio.co.id/auth/alogin?token='.$Sites->builder_token.'&user_id='.$User->id);
        }
        return redirect('/landingpage/sites/create/new');
    }

    public function create_site(Request $request){
        $validator = Validator::make($request->all(), [
            'site_name' => 'required',
            'site_domain' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Invalid Input',
                'data' => $validator->errors()
            ], 200);
        }

        // Temporary prevent some forbidden character
        if (!CharacterValidation($request->site_domain)) {
            return response()->json([
                'error' => 406,
                'message' => 'error',
                'data' => 'Invalid Character, please use a-z, A-Z, 0-9, and _, then try again',
            ]);
        }
        // Just temporary

        $SiteGlobalCheck = SiteGlobal::where('domain', $request->site_domain)->first();
        if ($SiteGlobalCheck) {
            return response()->json([
                'error' => 406,
                'message' => 'Error',
                'data' => 'Domain Sudah Ada'
            ]);
        }

        // Registering ssl domain to another API
        $headers = array(
			'Content-type: application/json',
			'Accept: application/json',
		);
		$fields = array(
			'domain' => $request->site_domain.'.ingenio.co.id',
		);
		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://api-domain.ingenio.co.id/api/register/domain' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        // If Something wrong when registering ssl domain
        $responseResult = json_decode($result, true);
        if ($responseResult['error'] != 0) {
            return response()->json($responseResult);
        }

        // Add user to pagestead ingenio, for creating page builder
        $User = Auth::user();
        $curl = curl_init();
        $url = "https://pagebuilder.ingenio.co.id/api/users";
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "admin@ingenio.co.id:Ingenio10m!"); // Using Basic Authorization, Thanks to Medium Blog for the solution
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-API-KEY: this_secret_public_key'));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        $data = array();
        $data['first_name'] = $User->name;
        $data['last_name'] = "_";
        $data['email'] = $User->email;
        $data['password'] = str_random(8);
        $data['package_id'] = 1;
        $data['type'] = "User";

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $pagestead_result = curl_exec($curl);
        curl_close($curl);

        // If something wrong when registering user to pagestead ingenio
        $pagestead_result_json = json_decode($pagestead_result, true);
        if ($pagestead_result_json['status'] != 'success') {
            return response()->json($pagestead_result_json);
        }

        $SiteGlobal = new SiteGlobal;
        $SiteGlobal->site_name = $request->site_name;
        $SiteGlobal->domain = $request->site_domain.'.ingenio.co.id'; // This temporary
        $SiteGlobal->user_id = $User->id;
        $SiteGlobal->builder_token = $pagestead_result_json['data']['user']['auto_login_token'];
        $SiteGlobal->save();

        return response()->json([
            'error' => 0,
            'message' => 'Success',
        ]);

    }

    public function create_custom_domain(Request $request){
        $validator = Validator::make($request->all(), [
            'site_domain' => 'required',
        ]);
        $User = Auth::user();

        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Invalid Input',
                'data' => $validator->errors()
            ], 200);
        }

        // Validating Character
        if (!DomainValidation($request->site_domain)) {
            return response()->json([
                'error' => 406,
                'message' => 'error',
                'data' => 'Invalid Character',
            ]);
        }

        $SiteGlobalCheck = SiteGlobal::where('domain', $request->site_domain)->first();
        if ($SiteGlobalCheck) {
            return response()->json([
                'error' => 406,
                'message' => 'Error',
                'data' => 'Domain Sudah Ada'
            ]);
        }

        // Registering ssl domain to another API
        $headers = array(
			'Content-type: application/json',
			'Accept: application/json',
		);
		$fields = array(
			'domain' => $request->site_domain,
		);
		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://api-domain.ingenio.co.id/api/register/domain' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        // If Something wrong when registering ssl domain
        $responseResult = json_decode($result, true);
        if ($responseResult['error'] != 0) {
            return response()->json($responseResult);
        }
        
        SiteGlobal::where('user_id', $User->id)->update(['domain' => $request->site_domain]);
        return response()->json([
            'error' => 0,
            'message' => 'Success',
        ]);
    }

    public function get_user_custom_domain(){
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first();
        $data = [
            'custom_domain' => $SiteGlobal->custom_domain,
        ];
        return response()->json([
            'error' => 0,
            'message' => 'Success',
            'data' => $data,
        ]);
    }

    public function testing_registered_domain(Request $request){
        $validator = Validator::make($request->all(), [
            'site_domain' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Invalid Input',
                'data' => $validator->errors()
            ], 406);
        }

        // // Temporary prevent some forbidden character
        // if (!CharacterValidation($request->site_domain)) {
        //     return response()->json([
        //         'error' => 406,
        //         'message' => 'error',
        //         'data' => 'Invalid Character, please use a-z, A-Z, 0-9, and _, then try again',
        //     ]);
        // }
        // // Just temporary

        // $SiteGlobalCheck = SiteGlobal::where('domain', $request->site_domain)->first();
        // if ($SiteGlobalCheck) {
        //     return response()->json([
        //         'error' => 406,
        //         'message' => 'Error',
        //         'data' => 'Domain Sudah Ada'
        //     ]);
        // }

        $headers = array(
			'Content-type: application/json',
			'Accept: application/json',
		);

		$fields = array(
			'domain' => $request->site_domain,
		);

		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'http://209.58.164.227:5000/api/register/domain' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        $responseResult = json_decode($result, true);
        if ($responseResult['error'] != 0) {
            return response()->json([
                'error' => 406,
                'message' => 'Some error not known'
            ]);
        }

        return response()->json($responseResult);
    }

    public function change_permalink($id, Request $request){
        $validator = Validator::make($request->all(), [
            'permalink' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Invalid Input',
                'data' => $validator->errors()
            ], 200);
        }
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first(); // Getting Site ID

        // Checking if permalink already exist
        $SitePage = SitePage::where(['id_site_global' => $SiteGlobal->id, 'url_prefix' => $request->permalink])->first();
        if ($SitePage) {
            return response()->json([
                'error' => 406,
                'message' => 'Permalink is Already Exist',
            ]);
        }

        // Update permalink
        SitePage::where('id', $id)->update(['url_prefix' => $request->permalink]);
        return response()->json([
            'error' => 0,
            'message' => 'Permalink has changed successful',
            'data' => [
                'permalink' => $request->permalink=='/' ? 'https://'.$SiteGlobal->domain.'.ingenio.co.id' : 'https://'.$SiteGlobal->domain.'.ingenio.co.id/'.$request->permalink,
            ]
        ]);
    }

    public function set_home_page($id){
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first(); // Getting Site ID

        // Checking if permalink already exist
        $CurrentSitePage = SitePage::where(['id_site_global' => $SiteGlobal->id, 'url_prefix' => '/'])->first();
        if ($CurrentSitePage) {
            // Set previous home page to random permalink
            SitePage::where('id', $CurrentSitePage->id)->update(['url_prefix' => str_random(10)]);

            // Update the target page to home page
            SitePage::where('id', $id)->update(['url_prefix' => '/']);
        }else{

            // Update the target page to home page
            SitePage::where('id', $id)->update(['url_prefix' => '/']);
        }
        return redirect('landingpage/sites');
    }

    public function delete_page($id){
        SitePage::where('id', $id)->delete();
        return redirect('landingpage/sites');
    }

    public function publish_site(Request $request){
        $User = Auth::user();
        SiteGlobal::where('user_id', $User->id)->update(['isPublished' => $request->status ? '1' : '0']);
        return response()->json([
            'error' => 0,
            'message' => 'Success',
            'data' => $request->status ? 'Your site set to published' : 'Your site set to unpublished',
        ]);
    }

    public function information_site(){
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first();

        return response()->json([
            'error' => 0,
            'message' => 'Success',
            'data' => $SiteGlobal,
        ]);

    }

    public function set_site_logo(Request $request){
        $User = Auth::user();
        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first();

        $destinationPath = './site_assets/logo/'; // upload path
        $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
        $file = rand(11111,999999).'.'.$extension; // renameing image
        $request->file('file')->move($destinationPath, $file); // uploading file to given path

        SiteGlobal::where('user_id', $User->id)->update(['site_logo' => url('site_assets/logo/'.$file)]);

        return response()->json([
            'message' => 'Success',
            'data' => [
              'path' => url('site_assets/logo/'.$file),
            ]
          ], 200);
    }

    public function new_user_site(Request $request){
        $template_id = $request->template_id;
        $Template = Template::where('id', $template_id)->first();
        $User = Auth::user();

        $SiteGlobal = SiteGlobal::where('user_id', $User->id)->first();

        // Save Site Progress
        // $MySite = new Site;
        // $MySite->user_id = $request->user_id;
        // $MySite->data_json = $Template->data_json;
        // $MySite->data_html = $Template->data_html;
        // $MySite->site_title = $request->site_title;
        // $MySite->description = '';
        // $MySite->font = 'Montserrat';
        // $MySite->slug = str_slug($request->site_title);
        // $MySite->save();

        // Save Site General Information for Publishing
        $SitePage = new SitePage;
        $SitePage->id_site_global = $SiteGlobal->id;
        $SitePage->url_prefix = str_slug($request->site_title);
        $SitePage->data_json = $Template->data_json;
        $SitePage->data_html = $Template->data_html;
        $SitePage->title = $request->site_title;
        $SitePage->description = '';
        $SitePage->font = 'Montserrat';
        $SitePage->save();

        return response()->json([
            'message' => 'Success',
        ], 200);
    }

    public function get_all_site_values($user_builder_id, $site_builder_id){
        $whereQuery = [
            'user_builder_id' => $user_builder_id, 'site_builder_id' => $site_builder_id
        ];
        $SiteValue = SiteValue::where($whereQuery)->first();
        if(empty($SiteValue)){
            return response()->json([
                'error' => 404,
                'message' => 'Not Found',
            ]);
        }
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $SiteValue,
        ]);
    }

    public function set_color_site_values($user_builder_id, $site_builder_id, Request $request){
        // First will checking data, if null, will create data
        // But will take a longer time to response
        
        $whereQuery = [
            'user_builder_id' => $user_builder_id, 'site_builder_id' => $site_builder_id
        ];
        $SiteValue = SiteValue::where($whereQuery)->first();
        if(empty($SiteValue)){
            // Getting user data from pagestead.ingenio.co.id to matching data with local user data
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "admin@ingenio.co.id:Ingenio10m!"); // Using Basic Authorization, Thanks to Medium Blog for the solution
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-API-KEY: this_secret_public_key'));
            curl_setopt($curl,CURLOPT_URL, 'https://pagebuilder.ingenio.co.id/api/users/'.$user_builder_id );
            curl_setopt($curl,CURLOPT_RETURNTRANSFER, true );
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false );
            $resp = curl_exec($curl );
            curl_close( $curl );
            
            // Getting some users data
            $email = json_decode($resp, false)->email;
            $User = User::where('email', $email)->first();
            $saveSiteValue = new SiteValue;
            $saveSiteValue->user_builder_id = $user_builder_id;
            $saveSiteValue->site_builder_id = $site_builder_id;
            $saveSiteValue->user_id = $User->id;
            $saveSiteValue->nav_bg_color = '#ffffff';
            $saveSiteValue->nav_font_color = '#000000';
            $saveSiteValue->save();
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $saveSiteValue,
            ]);
        }

        $bg_color = $request->bg_color;
        $font_color = $request->font_color;

        SiteValue::where($whereQuery)->update(['nav_bg_color' => $bg_color, 'nav_font_color' => $font_color]);
        return response()->json([
            'error' => 0,
            'message' => 'success'
        ]);
    }

    public function set_color_site_values_by_user_id(Request $request){
        $fields = [];
        $user_id = $request->user_id;
        $datas = $request->datas;
        if(isset($datas)){
            foreach($datas as $key => $data){
                SiteGlobal::where('user_id', $user_id)->update([$key => $datas[$key]]);
                // array_push($fields, [
                //     $key => $datas[$key],
                // ]);
            }
        }
        
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => SiteGlobal::where('user_id', $user_id)->first(),
        ]);
    }

    public function store_page_by_user_id(Request $request){
        $SiteGlobal = SiteGlobal::where('user_id', $request->ingenio_user_id)->first();
        $SiteUserPages = SiteUserPage::where(['site_global_id' => $SiteGlobal->id]);
        if(!empty($SiteUserPages->first())){
            $SiteUserPages->delete();
        }
        $SiteUserPage = new SiteUserPage;
        $SiteUserPage->site_global_id = $SiteGlobal->id;
        $SiteUserPage->route = $request->route;
        $SiteUserPage->html_value = $request->html_value;
        $SiteUserPage->save();

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $SiteUserPage,
        ]);
    }

    public function generate_page(Request $request){
        $domain = $request->domain;
        $Sites = SiteGlobal::where('domain', $domain)->orWhere('custom_domain', $domain)->first();
        if(empty($Sites)){
            return response()->json([
                'error' => 404,
                'message' => 'Not Found'
            ]);
        }
        $SiteValue = SiteValue::where('user_id', $Sites->user_id)->first();
        if(empty($SiteValue)){
            return response()->json([
                'error' => 404,
                'message' => 'Not Found'
            ]);
        }
        $SiteUserPage = SiteUserPage::where(['site_global_id' => $Sites->id, 'route' => $request->route_page])->first();
        if(empty($SiteUserPage)){
            return response()->json([
                'error' => 404,
                'message' => 'Not Found'
            ]);
        }

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => [
                'site' => $Sites,
                'value' => $SiteValue,
                'page' => $SiteUserPage,
            ]
        ]);
    }
}

?>