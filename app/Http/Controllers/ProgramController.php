<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\ProgramCourse;
use App\ProgramUser;
use App\Course;
use App\CourseUser;
use DB;
use Storage;
use Auth;
use Cart;

class ProgramController extends Controller
{
  public function dashboard()
  {
    $Program = Program::where('author', Auth::user()->id)->paginate(10);

    $data = [
      'Programs' => $Program,
    ];

    return view('program.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $data = [
      'action' => 'program/store',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'price' => old('price'),
      'password' => old('password'),
      'description' => old('description'),
      'image' => old('image'),
      'introduction' => old('introduction'),
      'introduction_description' => old('introduction_description'),
      'benefits' => old('benefits'),
      'developer_team' => old('developer_team'),
      'precondition' => old('precondition'),
    ];

    return view('program.form', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if($request->file('image')){
      // image
      $destinationPath = asset_path('uploads/programs/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $name_file = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $request->file('image')->move($destinationPath, $name_file); // uploading file to given path
      // image

      $image = '/uploads/programs/'.$name_file;
    }else{
      $image = '/uploads/programs/default.jpg';
    }

    // introduction
    // if($request->file('introduction')){
    //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
    //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
    //   Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
    //   $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
    // }else{
    //   $introduction = '';
    // }
    // introduction

    $Program = new Program;
    $Program->title = $request->title;
    $Program->image = $image;
    $Program->price = $request->price;
    // $Program->slug = str_slug($request->title);
    $Program->description = $request->description;
    $Program->author = Auth::user()->id;
    // $Program->introduction = $introduction;
    // $Program->introduction_description = $request->introduction_description;
    // $Program->benefits = $request->benefits;
    // $Program->developer_team = $request->developer_team;
    // $Program->precondition = $request->precondition;

    if($Program->save()){
      \Session::flash('success', 'Create record success');
    }else{
      \Session::flash('error', 'Create record failed');
    }
    return redirect('program/dashboard');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $Program = Program::where('id', $id)->first();
    $data = ['Program' => $Program];
    return view('program.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $Program = Program::where('id', $id)->first();
    $data = [
      'action' => 'program/update',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Program->id),
      'title' => old('title', $Program->title),
      'image' => old('image', $Program->image),
      'introduction' => old('introduction', $Program->introduction),
      'price' => old('price', $Program->price),
      'password' => old('password', $Program->password),
      'description' => old('description', $Program->description),
      'introduction_description' => old('introduction_description', $Program->introduction_description),
      'benefits' => old('benefits', $Program->benefits),
      'developer_team' => old('developer_team', $Program->developer_team),
      'precondition' => old('precondition', $Program->precondition),
    ];

    return view('program.form', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $Program = Program::where('id', $request->id)->first();
    $Program->title = $request->title;
    $Program->price = $request->price;
    // $Program->slug = str_slug($request->title);
    $Program->description = $request->description;
    // $Program->password = $request->password;
    // $Program->introduction_description = $request->introduction_description;
    // $Program->benefits = $request->benefits;
    // $Program->developer_team = $request->developer_team;
    // $Program->precondition = $request->precondition;


    // image
    if($request->file('image')){
      $destinationPath = asset_path('uploads/programs/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $request->file('image')->move($destinationPath, $image); // uploading file to given path
      $Program->image = '/uploads/programs/'.$image;
    }
    // image

    // introduction
    // if($request->file('introduction')){
    //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
    //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
    //   Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
    //   $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
    //   $Program->introduction = $introduction;
    // }
    // introduction

    if($Program->save()){
      \Session::flash('success', 'Update record success');
    }else{
      \Session::flash('error', 'Update record failed');
    }

    return redirect('program/dashboard');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $Program = Program::where('id', $id);
    if($Program){
      $Program->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('program/dashboard');
  }

  public function module_add($id){
    $ProgramCourses = ProgramCourse::where('program_id', $id)->get();
    $data_course_id = [];
    foreach($ProgramCourses as $ProgramCourse){
      array_push($data_course_id, $ProgramCourse->course_id);
    }
    // dd($data_course_id);

    $courses  = Course::whereNotIn('id', $data_course_id)
      ->where([
        'id_author' => Auth::user()->id,
        'archive' => '0'
      ])
      ->groupBy('courses.id')
      ->get();

    $data = [
      'courses' => $courses,
      'ProgramCourses' => Course::whereIn('id', $data_course_id)->get(),
    ];

    return view('program.module_form', $data);
  }

  public function module_store(Request $request, $id){

    // insert Program course
    foreach ($request->course_id as $key => $value) {
      $ProgramCourse = new ProgramCourse;
      $ProgramCourse->program_id = $id;
      $ProgramCourse->course_id = $value;
      $ProgramCourse->save();

      // auto enrolled Program user
      $ProgramUsers = ProgramUser::where('program_id', $id)->get();
      foreach($ProgramUsers as $ProgramUser){
        $CourseUser = new CourseUser;
        $CourseUser->course_id = $value;
        $CourseUser->user_id = $ProgramUser->user_id;
        $CourseUser->save();
      }
      // auto enrolled Program user

    }
    // insert Program course

    \Session::flash('success', 'Save record success');
    return redirect()->back();
  }

  public function module_delete($id){
    $ProgramCourse = ProgramCourse::where('course_id', $id)->delete();

    \Session::flash('success', 'Delete module success');
    return redirect()->back();
  }

  public function index(){
    $programs = Program::select('programs.*', 'users.name', 'users.slug as user_slug')
      ->join('users', 'users.id', '=', 'programs.author');

    $data = [
      'programs' => $programs->paginate(12),
    ];

    return view('program.list', $data);
  }

  public function detail($slug){

    $programs = Program::where('slug', $slug)->first();
    $programs_courses = ProgramCourse::where('program_id', $programs->id)
      ->join('courses', 'courses.id', 'program_courses.course_id')
      ->get();

    if(Auth::check()){
      $ProgramUser = ProgramUser::where(['program_id'=>$programs->id, 'user_id'=>Auth::user()->id])->first();
      if($ProgramUser){ // jika user sudah enroll Program
        return redirect('program/learn/'.$slug);
      }
    }

    // check course on cart
    $ProgramOnCart = false;
    foreach(Cart::content() as $cart){
      if($cart->id == $programs->id){
        $ProgramOnCart = true;
      }
    }
    // check course on cart

    $data = [
      'programs' => $programs,
      'programs_courses' => $programs_courses,
      'ProgramOnCart' => $ProgramOnCart,
    ];
    return view('program.detail', $data);
  }

  public function learn($slug){
    $Program = Program::where('slug', $slug)->first();
    $ProgramUser = ProgramUser::where(['program_id'=>$Program->id, 'user_id'=>Auth::user()->id])->first();
    if($ProgramUser){ // jika user sudah enroll Program

      // get course on degree
      $ProgramCourses = ProgramCourse::where(['program_id'=>$Program->id])->get();
      $data_course_id = [];
      foreach($ProgramCourses as $ProgramCourse){
        array_push($data_course_id,$ProgramCourse->course_id) ;
      }
      $Courses = Course::whereIn('courses.id', $data_course_id)
        ->where('status','1')
        ->with('contents')
        ->get();
      // get course on degree

      $data = [
        'programs' => $Program,
        'courses' => $Courses,
      ];

      return view('program.learn', $data);
    }else{
      return redirect('program/'.$slug);
    }
  }

  public function _checkPassword($id, Request $request){
    $Program = Program::where(['id' => $id, 'password' => $request->password])->first();
    if($Program){
      return 'true';
    }else{
      return 'false';
    }
  }

  public function enroll($id, Request $request){
    $Program = Program::where('id', $id)->first();

    $ProgramUser = new ProgramUser;
    $ProgramUser->program_id = $id;
    $ProgramUser->user_id = Auth::user()->id;
    $ProgramUser->save();

    $ProgramCourses = ProgramCourse::where('program_id', $id)->get();
    foreach($ProgramCourses as $ProgramCourse){
      $CourseUser = new CourseUser;
      $CourseUser->user_id = Auth::user()->id;
      $CourseUser->course_id = $ProgramCourse->course_id;
      $CourseUser->save();
    }

    return redirect('program/learn/'. $Program->slug);
  }

  public function enroll_ajax($id, Request $request){
    $Program = Program::where('id', $id)->first();

    $ProgramUser = new ProgramUser;
    $ProgramUser->program_id = $id;
    $ProgramUser->user_id = Auth::user()->id;
    $ProgramUser->save();

    $ProgramCourses = ProgramCourse::where('program_id', $id)->get();
    foreach($ProgramCourses as $ProgramCourse){
      $CourseUser = new CourseUser;
      $CourseUser->user_id = Auth::user()->id;
      $CourseUser->course_id = $ProgramCourse->course_id;
      $CourseUser->save();
    }

    return response()->json(['status' => '201']);
  }

}
