<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\LibraryAssignment;
use App\AssignmentAnswer;
use App\AssignmentGrade;
use App\LibraryDirectoryContent;
use App\Course;
use Auth;
use Storage;

class AssignmentController extends Controller
{
  public function create(){
    $data = [
      'action' => 'library/course/assignment/create_action/',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'description' => old('description'),
      'type' => old('type'),
      'attempt' => old('attempt', '3'),
      'time_start' => old('time_start', date("Y-m-d")),
      'time_end' => old('time_end', date("Y-m-d")),
    ];

    return view('library.assignment_form', $data);
  }

  public function create_action(Request $request){

    // insert Assignment
    $Assignment = new LibraryAssignment;
    $Assignment->title = $request->title;
    $Assignment->description = $request->description;
    $Assignment->type = $request->type;
    $Assignment->attempt = $request->attempt;
    $Assignment->time_start = $request->time_start;
    $Assignment->time_end = $request->time_end;
    // $Assignment->id_section = $section_id;
    $Assignment->save();
    // insert Assignment

    $library_dir_content = new LibraryDirectoryContent;
    $library_dir_content->user_id = Auth::user()->id;
    $library_dir_content->directory_id = 1;
    $library_dir_content->assignment_id = $Assignment->id;
    $library_dir_content->save();
    // insert content

    \Session::flash('success', 'Create Assignment success');
    return redirect('instructor/library');
  }

  public function update($id){
    $Assignment = Assignment::where('id', $id)->first();
    $section = Section::where('id', $Assignment->id_section)->with('course')->first();

    $data = [
      'action' => 'course/assignment/update_action/',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Assignment->id),
      'title' => old('title', $Assignment->title),
      'description' => old('description', $Assignment->description),
      'type' => old('type', $Assignment->type),
      'attempt' => old('attempt', $Assignment->attempt),
      'time_start' => old('time_start', $Assignment->time_start),
      'time_end' => old('time_end', $Assignment->time_end),
      'section' => $section,
    ];
    return view('assignment.form', $data);
  }

  public function update_action(Request $request){
    $Assignment = Assignment::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Assignment->id_section)->first();

    $Assignment->title = $request->title;
    $Assignment->description = $request->description;
    $Assignment->type = $request->type;
    $Assignment->attempt = $request->attempt;
    $Assignment->time_start = $request->time_start;
    $Assignment->time_end = $request->time_end;
    $Assignment->save();

    \Session::flash('success', 'Create Assignment success');
    return redirect('course/preview/'.$Section->id_course);
  }

  public function publish($Assignment_id){
    $Assignment = Assignment::where('id', $Assignment_id)->first();
    $Section = Section::where('id', $Assignment->id_section)->first();
    $Assignment->status = '1';
    $Assignment->save();

    \Session::flash('success', 'Tugas berhasil di publish');
    return redirect('course/preview/'.$Section->id_course);
  }

  public function delete($course_id, $id){
    $Assignment = Assignment::where('id', $id);
    if($Assignment->first()){
      $AssignmentAnswer = AssignmentAnswer::where('assignment_id', $Assignment->first()->id);
      if($AssignmentAnswer->first()){
        $AssignmentAnswer->delete();
      }
      $Assignment->delete();
      \Session::flash('success', 'Delete Assignment success');
    }else{
      \Session::flash('error', 'Delete Assignment failed');
    }
    return redirect('course/preview/'.$course_id);
  }

  public function detail($slug, $id){
    $AssignmentAnswer = AssignmentAnswer::where(['assignments_answers.assignment_id'=>$id, 'user_id' => Auth::user()->id])->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');
    $Assignment = Assignment::where('id', $id)->first();
    if(date("Y-m-d H:i:s") <= $Assignment->time_end){

      $courses = Course::where('slug', $slug)->with('sections')->first();
      $data = [
        'assignment' => $Assignment,
        'assignments_answers' => $AssignmentAnswer->get(),
        'course' => $courses,
      ];

      if($AssignmentAnswer->count() < $Assignment->attempt){ // check answer attempt
        return view('assignment.detail', $data);
      }else{
        \Session::flash('success', 'You already answer this assignment :)');
        return view('assignment.my_answer', $data);
      }
    }else{
      return redirect('/');
    }
  }

  public function post($assignment_id, Request $request){
    $User = Auth::user();
    $Assignment = Assignment::where('id', $assignment_id)->first();
    if($Assignment->type == '0'){
      $answer = $request->answer;
    }else{

      // GOOGLE UPLOAD
      // $destinationPath = asset_path('uploads/assignments/'); // upload path
      // $extension = $request->file('answer_file')->getClientOriginalExtension(); // getting image extension
      // $file = 'assignment-'.str_slug($User->name).rand(111,9999).'.'.$extension; // renameing image
      // $folder = get_folder_course($Assignment->id_section); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$file, fopen($request->file('answer_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $file);
      // $answer = "https://drive.google.com/file/d/".$path_file;

      // DIGITALOCEAN UPLOAD
      $destinationPath = asset_path('uploads/assignments/');
      $file = $request->file('answer_file');
      $extension = $request->file('answer_file')->getClientOriginalExtension();
      $name_file = 'assignment-'.str_slug($User->name).rand(111,9999).'.'.$extension;
      $folder = get_folderName_course($Assignment->id_section); // get folder course by section id
      Storage::disk('do_spaces')->put('assets/' . $folder.'/'.$name_file, fopen($file, 'r+'), 'public');//upload google drive
      $path_file = get_asset_newpath('assets/'.$folder, $name_file);
      $answer = "https://ingeniolms.sgp1.digitaloceanspaces.com/" . $path_file;

    }
    $AssignmentAnswer = new AssignmentAnswer;
    $AssignmentAnswer->assignment_id = $assignment_id;
    $AssignmentAnswer->answer = $answer;
    $AssignmentAnswer->user_id = $User->id;
    $AssignmentAnswer->save();

    return redirect('/assignment/success/' . $Assignment->id);
    // return redirect()->back();
  }

  public function success($id){

    $AssignmentAnswer = AssignmentAnswer::where(['assignments_answers.assignment_id'=>$id, 'user_id' => Auth::user()->id])->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');
    $Assignment = Assignment::where('id', $id)->first();


      $section = Section::where('id', $Assignment->id_section)->first();
      $courses = Course::where('id', $section->id_course)->with('sections')->first();
      $data = [
        'assignment' => $Assignment,
        'assignments_answers' => $AssignmentAnswer->get(),
        'course' => $courses,
      ];

    \Session::flash('success', 'Tugas berhasil terkirim');
    // return view('assignment.success');
    return view('assignment.my_answer', $data);
  }

  public function answer($assignment_id){
    $AssignmentsAnswers = AssignmentAnswer::select('assignments_answers.id as id','assignments_answers.answer','assignments_answers.assignment_id as assignment_id','users.name','assignments.type','assignments_grades.grade')
    ->where('assignments_answers.assignment_id', $assignment_id)
    ->join('users','users.id','=','assignments_answers.user_id')
    ->join('assignments','assignments.id','=','assignments_answers.assignment_id')
    ->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');
    $data = [
      'assignments_answers' => $AssignmentsAnswers->get(),
    ];
    return view('assignment.answer', $data);
  }

  public function previewAnswer($id){
    $AssignmentAnswer = AssignmentAnswer::select('assignments_answers.id as id','assignments_answers.answer','assignments_answers.assignment_id','assignments.type')
    ->where('assignments_answers.id', $id)
    ->join('assignments','assignments.id','=','assignments_answers.assignment_id')->first();
    $data = [
      'AssignmentAnswer' => $AssignmentAnswer,
    ];
    return view('assignment.preview_answer', $data);
  }

  public function saveGrade(Request $request){
    $AssignmentGrade = new AssignmentGrade;
    $AssignmentGrade->assignment_id = $request->assignment_id;
    $AssignmentGrade->assignment_answer_id = $request->assignment_answer_id;
    $AssignmentGrade->grade = $request->grade;
    $AssignmentGrade->save();

    \Session::flash('success', 'Grade Success Saved');
  }
}
