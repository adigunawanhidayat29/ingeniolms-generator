<?php

namespace App\Http\Controllers\Library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LibraryDirectoryGroup;
use App\LibraryAssignment;
use App\LibraryDirectoryContent;
use App\LibraryDirectoryContentUser;
use App\LibraryDirectoryGroupUser;
use App\LibraryDirectoryGroupContent;
use App\LibraryDirectory;
use App\LibraryQuizPageBreak;
use App\LibraryQuizQuestion;
use App\LibraryQuizQuestionAnswer;
use App\QuizType;
use App\LibraryQuiz;
use App\QuestionBank;
use App\QuestionBankAnswer;
use Auth;

class BankSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $group = new LibraryDirectoryGroup;
        $group->user_id = Auth::user()->id;
        $group->group_name = $request->group_name;
        $group->question_bank = 1;
        $group->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //

        $bank_soal = LibraryDirectoryGroup::find($id);
        // dd($directory);

        $quiz_types = QuizType::get();

        return view('library.bank_soal', compact('bank_soal', 'quiz_types'));
    }

    public function create_question($id){
      $bank_folder = LibraryDirectoryGroup::find($id);
      $quiz_questions = QuestionBank::
                        select('question_banks.*', 'quiz_types.type')
                        ->where('group_id', $id)
                        ->join('quiz_types','quiz_types.id' ,'=','question_banks.quiz_library_type_id')
                        ->get();
      $quiz_questions_answers = [];
      foreach($quiz_questions as $quiz_question){
        array_push($quiz_questions_answers, array(
          'question_answers' => QuestionBankAnswer::where('question_bank_id', $quiz_question->id)->get(),
          'question' => $quiz_question->question,
          'weight' => $quiz_question->weight,
          'type' => $quiz_question->type,
          'id' => $quiz_question->id,
        ));
      }

      $data = [
        'action' => 'library/course/quiz/bank/question/create_action/'.$id,
        'method' => 'post',
        'button' => 'Create',
        'id' => old('id'),
        'quiz_type_id' => old('quiz_type_id'),
        'question' => old('question'),
        'weight' => old('weight','1'),
        'quiz' => $bank_folder,
        'quiz_types' => QuizType::get(),
        'quiz_questions_answers' => $quiz_questions_answers,
      ];

      return view('library.quiz_question_bank_form', $data);
    }

    public function create_action($group_id, Request $request){
      // insert Quiz Question
      $QuizQuestion = new QuestionBank;

      $QuizQuestion->quiz_library_type_id = $request->quiz_type_id;
      $QuizQuestion->question = $request->question;
      $QuizQuestion->weight = $request->weight ? $request->weight : 0;
      $QuizQuestion->group_id = $group_id;
      $QuizQuestion->save();
      $QuizQuestion_id = $QuizQuestion->id;
      // insert Quiz Question

      // inser quiz question answer
      $answers = $request->answer;
      if($answers){
        foreach($answers as $index => $value){
          if($value != NULL){
            $QuizQuestionAnswer = new QuestionBankAnswer;
            $QuizQuestionAnswer->answer = $value;
            if($request->quiz_type_id == '3'){ // ordering type
              $QuizQuestionAnswer->answer_ordering = $request->answer_ordering[$index];
            }else{
              $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
            }
            $QuizQuestionAnswer->question_bank_id = $QuizQuestion_id;
            $QuizQuestionAnswer->save();
          }
        }
      }
      // inser quiz question answer

      \Session::flash('success', 'Create record success');
      return redirect('library/bank/soal/show/'.$group_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_question($quiz_id, $id)
    {
        $quiz = LibraryQuiz::find($quiz_id);
        $bank_soal = LibraryDirectoryGroup::find($id);

        $quiz_types = QuizType::get();

        return view('library.bank_soal_get', compact('bank_soal', 'quiz_types', 'quiz'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_question_create(Request $request, $quiz_id, $id)
    {
        $quiz = LibraryQuiz::find($quiz_id);
        $QuizPageBreak = LibraryQuizPageBreak::select('id')->where('library_quiz_id', $quiz_id)->orderBy('id','desc')->first();

        foreach ($request->bank_question_id as $key => $value) {
          $bank_soal = QuestionBank::find($value);
          $QuizQuestion = new LibraryQuizQuestion;

          $QuizQuestion->quiz_library_type_id = $bank_soal->quiz_library_type_id;
          $QuizQuestion->question = $bank_soal->question;
          $QuizQuestion->weight = $bank_soal->weight ? $bank_soal->weight : 0;
          $QuizQuestion->quiz_library_id = $quiz_id;
          $QuizQuestion->quiz_page_break_id = $QuizPageBreak->id;
          $QuizQuestion->save();
          $QuizQuestion_id = $QuizQuestion->id;
          // insert Quiz Question

          // inser quiz question answer
          $answers = QuestionBankAnswer::where('question_bank_id', $bank_soal->id)->get();
          if($answers){
            foreach($answers as $index => $value){
              if($value->answer != NULL){
                $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                $QuizQuestionAnswer->answer = $value->answer;
                if($bank_soal->quiz_library_type_id == '3'){ // ordering type
                  $QuizQuestionAnswer->answer_ordering = $value->answer_ordering;
                }else{
                  $QuizQuestionAnswer->answer_correct = $value->answer_correct;
                }
                $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                $QuizQuestionAnswer->save();
              }
            }
          }
        }


        return redirect('library/course/quiz/manage/'.$quiz_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($quiz_id, $id)
    {
        //
        $bank_soal = QuestionBank::find($id);
        $answers = QuestionBankAnswer::where('question_bank_id', $bank_soal->id)->get();
        foreach ($answers as $key => $value) {
          $answers_destroy = QuestionBankAnswer::find($bank_soal->id);
          $answers_destroy->delete();
        }
        $bank_soal->delete();

        return redirect()->back();
    }
}
