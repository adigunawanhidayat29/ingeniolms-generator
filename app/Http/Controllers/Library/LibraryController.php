<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Quiz;
use App\InstructorGroupLibrary;
use App\LibraryAssignment;
use App\LibraryDirectoryContent;
use App\LibraryDirectoryContentUser;
use App\LibraryDirectoryGroup;
use App\LibraryDirectoryGroupUser;
use App\LibraryDirectoryGroupContent;
use App\LibraryQuizPageBreak;
use App\LibraryQuizQuestion;
use App\LibraryQuizQuestionAnswer;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\QuizType;
use App\User;
use App\Content;
use App\Course;
use App\CourseLive;
use App\UserLibrary;
use App\ShareContent;
use App\LibraryQuiz;
use App\LibraryDirectory;
use App\Assignment;
use Validator;
use Storage;
use Plupload;
use FFMpeg;
use Excel;
use Auth;


class LibraryController extends Controller {

    public function index(Request $request){

      $directory = LibraryDirectory::with(['library' => function($query) use ($request){
                                              if($request->directory){
                                                if($request->directory == '2'){
                                                  $query->with('content', 'quiz', 'assignment')->where('directory_id', $request->directory);
                                                }else {
                                                  $query->with('content', 'quiz', 'assignment')->where('directory_id', $request->directory)->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
                                                    $query->where('user_id', Auth::user()->id);
                                                  });;
                                                }
                                              }else {
                                                $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
                                                  $query->where('user_id', Auth::user()->id);
                                                });
                                              }
                                            }
                                          ])->get();
      // dd($directory);

      $directory_user = LibraryDirectory::with(['library' => function($query) use ($request){
                                              $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id);
                                            }
                                          ])->get();

      $group = LibraryDirectoryGroup::where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
        $query->where('user_id', Auth::user()->id);
      })->get();

      if($request->directory){
        $directory_search = LibraryDirectory::find($request->directory);
        if(!$directory_search){

          return redirect('instructor/library');
        }

        if($request->directory == '2'){
          $group = LibraryDirectoryGroup::where('group_code', null)->where('status', '1')->get();
        }else {
          $group = LibraryDirectoryGroup::where('group_code', '!=', null)->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
            $query->where('user_id', Auth::user()->id);
          })->get();
        }

      }

      return view('library.index', compact('directory','directory_user', 'group'));
    }

    public function add(Request $request, $course_id, $section_id){
      $directory = LibraryDirectory::with(['library' => function($query) use ($request){
                                              if($request->directory){
                                                if($request->directory == '2'){
                                                  $query->with('content', 'quiz', 'assignment')->where('directory_id', $request->directory);
                                                }else {
                                                  $query->with('content', 'quiz', 'assignment')->where('directory_id', $request->directory)->where('user_id', Auth::user()->id);
                                                }
                                              }else {
                                                $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id);
                                              }
                                            }
                                          ])->get();

      $share_user = LibraryDirectoryContentUser::with(['library' => function($query) use ($request){
                                                  $query->with('content', 'quiz', 'assignment');
                                                  }
                                                ])->where('user_id', Auth::user()->id)->get();

      $group = LibraryDirectoryGroup::with(['contents.library' => function($query) use ($request){
                                        $query->with('content', 'quiz', 'assignment');
                                        }
                                      ])->where('user_id', Auth::user()->id)->get();
      $group_user = LibraryDirectoryGroupUser::with(['group.contents.library' => function($query) use ($request){
                                        $query->with('content', 'quiz', 'assignment');
                                        }
                                      ])->where('user_id', Auth::user()->id)->get();

      if($request->directory){
        $directory_search = LibraryDirectory::find($request->directory);
        if(!$directory_search){

          return redirect()->back();
        }
      }

      return view('library.library_react_add', compact('directory', 'group', 'course_id', 'section_id', 'share_user', 'group_user'));
    }

    public function share_add(Request $request){
      $content = LibraryDirectoryContent::where('shared_code', $request->shared_code)->where('directory_id', '3')->first();
      if($content){
        $content_user = LibraryDirectoryContentUser::where('user_id', Auth::user()->id)->where('library_directory_content_id', $content->id)->first();
        if(!$content_user){
          $content_user = new LibraryDirectoryContentUser;
          $content_user->user_id = Auth::user()->id;
          $content_user->library_directory_content_id = $content->id;
          $content_user->save();
        }
      }

      return redirect()->back();
    }

    public function share_add_public($id){
      $content = LibraryDirectoryContent::find($id);
      if($content){
        $content_user = LibraryDirectoryContentUser::where('user_id', Auth::user()->id)->where('library_directory_content_id', $content->id)->first();
        if(!$content_user){
          $content_user = new LibraryDirectoryContentUser;
          $content_user->user_id = Auth::user()->id;
          $content_user->library_directory_content_id = $content->id;
          $content_user->save();
        }
      }

      return redirect()->back();
    }

    public function share_folder_add(Request $request){
      $group = LibraryDirectoryGroup::where('group_code', $request->shared_code)->first();
      if($group){
        $group_user = LibraryDirectoryGroupUser::where('user_id', Auth::user()->id)->where('group_id', $group->id)->first();
        if(!$group_user){
          $group_user = new LibraryDirectoryGroupUser;
          $group_user->user_id = Auth::user()->id;
          $group_user->group_id = $group->id;
          $group_user->save();
        }
      }

      return redirect()->back();
    }

    // group

    public function group_shared_destroy($id){
      $group = LibraryDirectoryGroupUser::where('group_id', $id)->where('user_id', Auth::user()->id)->first();
      $group_folder = InstructorGroupLibrary::where('folder_id', $id)->get();
      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }
      $group->delete();

      return redirect()->back();
    }

    public function group_shared($id, $directory_id){
      $group = LibraryDirectoryGroup::find($id);
      if($directory_id == 2){
        $group->status = '1';
        $group->group_code = null;
      }else {
        $code = generateCode();

        $group_cek = LibraryDirectoryGroup::where('group_code', $code)->first();
        while($group_cek){
          $code = generateCode();
          $group_cek = LibraryDirectoryGroup::where('group_code', $code)->first();
        }
        $group->group_code = $code;
        $group->status = '0';
        // code...
      }
      $group->save();

      return redirect()->back();
    }

    public function group_destroy($id){
      $group = LibraryDirectoryGroup::find($id);
      $group_user = LibraryDirectoryGroupUser::where('group_id', $id)->get();
      $group_folder = InstructorGroupLibrary::where('folder_id', $id)->get();
      $group_content = LibraryDirectoryGroupContent::where('group_id', $id)->get();
      foreach ($group_user as $key => $value) {
        $group_user_destroy = LibraryDirectoryGroupUser::find($value->id);
        $group_user_destroy->delete();
      }

      foreach ($group_content as $key => $value) {
        $group_content_destroy = LibraryDirectoryGroupContent::find($value->id);
        $group_content_destroy->delete();
      }

      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }
      $group->delete();

      return redirect()->back();
    }

    public function group_add(Request $request){
      $group = LibraryDirectoryGroup::where('group_code', $request->shared_code)->first();
      if($group){
        $group_user = LibraryDirectoryGroupUser::where('user_id', Auth::user()->id)->where('group_id', $group->id)->first();
        if(!$group_user){
          $group_user = new LibraryDirectoryGroupUser;
          $group_user->user_id = Auth::user()->id;
          $group_user->group_id = $group->id;
          $group_user->save();
        }
      }

      return redirect()->back();
    }

    public function create(){
      $data = [
        'action' => 'library/course/content/create_action',
        'method' => 'post',
        'button' => 'Create',
        'id' => old('id'),
        'title' => old('title'),
        'description' => old('description'),
        'is_description_show' => old('is_description_show'),
        'type_content' => old('type_content'),
        'name_file' => old('name_file'),
        'path_file' => old('path_file'),
        'file_size' => old('file_size'),
        'video_duration' => old('video_duration'),
        'full_path_file' => old('full_path_file'),
        'sequence' => old('sequence'),
        'section' => null,
      ];

      return view('library.content_form', $data);
    }

    public function create_action(Request $request){
      dd($request);
      // insert content
      $content_library = new UserLibrary;
      // $destinationPath = asset_path('uploads/contents/'); // upload path
      // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
      // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
      // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
      // $folder = get_folder_course($section_id); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $name_file);

      $content_library->id_user = Auth::user()->id;
      $content_library->title = $request->title;
      $content_library->description = $request->description;
      $content_library->type_content = $request->type_content;
      // $Content->sequence = $request->sequence;

      $filesize = null;
      $filenameOrigin = null;
      $filename = null;
      $path_file = null;
      $full_path_original = null;
      $filesize = 0;

      $content_library->name_file = $filenameOrigin;
      $content_library->path_file = $path_file;
      $content_library->file_size = $filesize;
      $content_library->video_duration = $request->video_duration;
      $content_library->full_path_file = $request->full_path_file;
      $content_library->full_path_original = $full_path_original;
      $content_library->full_path_file_resize = null;
      $content_library->file_size = $filesize;

      if($request->type_content == 'video'){
        if($request->video_file){
          $filesize = $request->video_file->getSize();
          $filenameOrigin = $request->video_file->getClientOriginalName();
          $filename = time() . '.' . $request->video_file->getClientOriginalExtension();
          $request->video_file->move(public_path('content/video'), $filename);
          $path_file = 'content/video/'.$filename;
          $full_path_original = asset('content/video/'.$filename);
        }
        $content_library->name_file = $filenameOrigin;
        $content_library->path_file = $path_file;
        $content_library->file_size = $filesize;
        $content_library->video_duration = $request->video_duration;
        $content_library->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
        $content_library->full_path_original = $full_path_original;
        $content_library->file_size = $filesize;
      }

      if($request->type_content == 'file'){
        if($request->file){
          $filesize = $request->file->getSize();
          $filenameOrigin = $request->file->getClientOriginalName();
          $filename = time() . '.' . $request->file->getClientOriginalExtension();
          $request->file->move(public_path('content/video'), $filename);
          $path_file = 'content/file/'.$filename;
          $full_path_original = asset('content/video/'.$filename);
        }
        $content_library->name_file = $filenameOrigin;
        $content_library->path_file = $path_file;
        $content_library->file_size = $filesize;
        $content_library->video_duration = $request->video_duration;
        $content_library->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
        $content_library->full_path_original = $full_path_original;
        $content_library->file_size = $filesize;
      }
        $content_library->save();

      $library_dir_content = new LibraryDirectoryContent;
      $library_dir_content->user_id = Auth::user()->id;
      $library_dir_content->directory_id = 1;
      $library_dir_content->content_id = $content_library->id;
      $library_dir_content->save();
      // insert content

      \Session::flash('success', 'Create library content success');

      return redirect('instructor/library');
    }


    // save to draft function
    public function store_draft(Request $request){
      $field = $request->field;
      $value = $request->value;

      $Content = UserLibrary::find($request->id); // checking record content

      if($Content){ // jika konten telah ada maka hanya melakaukan update konten
        // $folder = get_folder_course($Content->id_section); // get folder id
        $Content->type_content = $request->type_content;
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        $Content->full_path_original = $request->full_path_original;
        $Content->full_path_file = asset_url($request->full_path_original);
        if($Content->type_content == 'video'){ // jika inputan file berupa video
          $Content->video_duration = $request->video_duration;
          $Content->file_size = $request->file_size;
        }

        $title = '';
        if($request->title){
          $title = $request->title;
        }
        $Content->title = $request->title;
        $Content->description = $request->description;

        $Content->save();
      }else{ // jika belum ada konten maka buat konten baru
        // $folder = get_folder_course($request->section_id); // get folder id
        $Content = new UserLibrary;
        $Content->id_user = Auth::user()->id;
        $Content->type_content = $request->type_content;
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        $Content->full_path_original = $request->full_path_original;
        $Content->full_path_file = asset_url($request->path_file);
        if($Content->type_content == 'video'){ // jika inputan file berupa video
          $Content->file_size = $request->file_size;
          $Content->video_duration = $request->video_duration;
        }

        $title = '';
        if($request->title){
          $title = $request->title;
        }
        $Content->title = $request->title;
        $Content->description = $request->description;

        $Content->save();

        $library_dir_content = new LibraryDirectoryContent;
        $library_dir_content->user_id = Auth::user()->id;
        $library_dir_content->directory_id = 1;
        $library_dir_content->content_id = $Content->id;
        $library_dir_content->save();
      }

      return response()->json($Content);
    }

    public function upload(){
      return Plupload::receive('file', function ($file)
      {
          $extension = $file->getClientOriginalExtension(); // getting image extension
          $size_file = $file->getSize(); // getting image extension
          $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image

          $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt', 'avi', 'webm'];
          if(in_array(strtolower($extension), $video_extensions)){

              Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/library/contents/videos/' .$name_file, fopen($file, 'r+'), 'public');//upload google drive
              $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/library/contents/videos/', $name_file);
              $video_duration = 0;

              $ffprobe = FFMpeg\FFProbe::create();
              $video_duration = $ffprobe
                ->format($file) // extracts file informations
                ->get('duration');

            return response()->json([
              'path' => $path_file,
              'name' => $name_file,
              'size' => $size_file,
              'video_duration' => $video_duration,
              'full_path_original' => asset_url($path_file),
              'full_path_file' => asset_url($path_file),
              'resize_path' => '',
            ]);
          }else{

            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/library/contents/files/' .$name_file, fopen($file, 'r+'), 'public');//upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/library/contents/files/' , $name_file);

            return response()->json([
              'path' => $path_file,
              'name' => $name_file,
            ]);
          }
      });
    }

    public function create_action_course($content_id, $course_id){
      $content = Content::find($content_id);
      // insert content
      $content_library = new UserLibrary;
      // $destinationPath = asset_path('uploads/contents/'); // upload path
      // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
      // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
      // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
      // $folder = get_folder_course($section_id); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $name_file);

      $content_library->id_user = Auth::user()->id;
      $content_library->title = $content->title;
      $content_library->description = $content->description;
      $content_library->type_content = $content->type_content;
      $content_library->name_file = $content->name_file;
      $content_library->path_file = $content->path_file;
      $content_library->full_path_file = $content->full_path_file;
      $content_library->full_path_file_resize = $content->full_path_file_resize;
      $content_library->full_path_file_resize_720 = $content->full_path_file_resize_720;
      $content_library->full_path_original = $content->full_path_original;
      $content_library->full_path_original_resize = $content->full_path_original_resize;
      $content_library->full_path_original_resize_720 = $content->full_path_original_resize_720;
      $content_library->file_status = $content->file_status;
      $content_library->file_size = $content->file_size;
      $content_library->video_duration = $content->video_duration;
      $content_library->status = $content->status;
      $content_library->save();

      $library_dir_content = new LibraryDirectoryContent;
      $library_dir_content->user_id = Auth::user()->id;
      $library_dir_content->directory_id = 1;
      $library_dir_content->content_id = $content_library->id;
      $library_dir_content->save();
      // insert content

      \Session::flash('success', 'Create library content success');

      return redirect()->back();
    }

    public function create_action_assignment($assignment_id, $course_id){
      $assigm = Assignment::find($assignment_id);
      if($assigm){
        // insert Assignment
        $Assignment = new LibraryAssignment;
        $Assignment->title = $assigm->title;
        $Assignment->description = $assigm->description;
        $Assignment->type = $assigm->type;
        $Assignment->attempt = $assigm->attempt;
        $Assignment->time_start = $assigm->time_start;
        $Assignment->time_end = $assigm->time_end;
        // $Assignment->id_section = $section_id;
        $Assignment->save();
        // insert Assignment

        $library_dir_content = new LibraryDirectoryContent;
        $library_dir_content->user_id = Auth::user()->id;
        $library_dir_content->directory_id = 1;
        $library_dir_content->assignment_id = $Assignment->id;
        $library_dir_content->save();
        // insert content

        \Session::flash('success', 'Create Assignment success');
      }

      return redirect()->back();
    }

    public function create_action_quiz($quiz_id, $course_id){
      $Quiz = Quiz::find($quiz_id);
      if($Quiz){
            // insert Quiz
            $quiz_library = new LibraryQuiz;

            $quiz_library->user_id = Auth::user()->id;
            $quiz_library->name = $Quiz->name;
            $quiz_library->description = $Quiz->description;
            $quiz_library->slug = $Quiz->slug;
            // $Quiz->shuffle = $request->shuffle;
            $quiz_library->time_start = $Quiz->time_start;
            $quiz_library->time_end = $Quiz->time_end;
            $quiz_library->duration = $Quiz->duration;
            $quiz_library->attempt = $Quiz->attempt;
            $quiz_library->image = $Quiz->image;
            $quiz_library->status = $Quiz->status; // 1= publish, 2 =draft
            $quiz_library->save();
            // insert Quiz

            if($Quiz->pagebreak){
              foreach ($Quiz->pagebreak as $key => $value) {
                $QuizPageBreakNew = new LibraryQuizPageBreak;
                $QuizPageBreakNew->library_quiz_id = $quiz_library->id;
                $QuizPageBreakNew->title = $value->title;
                $QuizPageBreakNew->description = $value->description;
                $QuizPageBreakNew->save();
                // code...
              }
            }

            if($Quiz->quiz_questions){
              $QuizPageBreak = LibraryQuizPageBreak::select('id')->where('library_quiz_id', $quiz_library->id)->orderBy('id','desc')->first();
              foreach ($Quiz->quiz_questions as $key => $question) {
                // code...
                $QuizQuestion = new LibraryQuizQuestion;

                $QuizQuestion->quiz_library_id = $quiz_library->id;
                $QuizQuestion->quiz_library_type_id = $question->quiz_type_id;
                $QuizQuestion->question = $question->question;
                $QuizQuestion->weight = $question->weight;
                $QuizQuestion->quiz_page_break_id = $QuizPageBreak ? $QuizPageBreak->id : null;
                $QuizQuestion->save();
                $QuizQuestion_id = $QuizQuestion->id;
                $answers = $question->quiz_question_answers;
                if($answers){
                  foreach($answers as $index => $answer){
                    if($value != NULL){
                      $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                      $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                      $QuizQuestionAnswer->answer = $answer->answer;
                      $QuizQuestionAnswer->answer_ordering = $answer->answer_ordering;
                      $QuizQuestionAnswer->answer_correct = $answer->answer_correct;
                      $QuizQuestionAnswer->save();
                    }
                  }
                }
              }

            }

            $library_dir_content = new LibraryDirectoryContent;
            $library_dir_content->user_id = Auth::user()->id;
            $library_dir_content->directory_id = 1;
            $library_dir_content->quiz_id = $quiz_library->id;
            $library_dir_content->save();
            // insert Quiz Question

            // insert Quiz Question

            // inser quiz question answer
      }

      \Session::flash('success', 'Create content success');
      // return redirect('course/preview/'.$course_id);
      return redirect()->back();
    }

    public function shared_group(Request $request){
      $code = generateCode();

      $group_cek = LibraryDirectoryGroup::where('group_code', $code)->first();
      while($group_cek){
        $code = generateCode();
        $group_cek = LibraryDirectoryGroup::where('group_code', $code)->first();
      }

      $group = new LibraryDirectoryGroup;
      $group->user_id = Auth::user()->id;
      $group->group_name = $request->group_name;
      // $group->group_code = $code;
      $group->save();

      if($request->library_id){
        for ($i=0; $i < count($request->library_id); $i++) {
          $group_content = new LibraryDirectoryGroupContent;
          $group_content->group_id = $group->id;
          $group_content->group_content_id = $request->library_id[$i];
          $group_content->save();
        }
      }

      return redirect()->back();
    }

    public function add_group(Request $request, $id){
      $group = LibraryDirectoryGroup::find($id);

      if($group){
        if($request->library_id){
          for ($i=0; $i < count($request->library_id); $i++) {
            $group_content = LibraryDirectoryGroupContent::where('group_id', $group->id)->where('group_content_id', $request->library_id[$i])->first();
            if(!$group_content){
              $group_content = new LibraryDirectoryGroupContent;
              $group_content->group_id = $group->id;
              $group_content->group_content_id = $request->library_id[$i];
              $group_content->save();
            }
          }
        }
      }

      return redirect()->back();
    }

    public function edit_group(Request $request, $id){

      $group = LibraryDirectoryGroup::find($id);
      $group->group_name = $request->group_name;
      $group->save();

      return redirect()->back();
    }

    public function group_rollback($id){
      $group = LibraryDirectoryGroup::find($id);
      $group_user = LibraryDirectoryGroupUser::where('group_id', $id)->get();
      foreach ($group_user as $key => $value) {
        $group_user_destroy = LibraryDirectoryGroupUser::find($value->id);
        $group_user_destroy->delete();
      }

      $group_folder = InstructorGroupLibrary::where('folder_id', $id)->get();
      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }

      $group->group_code = null;
      $group->status = '0';
      $group->save();

      return redirect()->back();
    }

    //content

    public function library_shared_destroy($id){
      $content = LibraryDirectoryContentUser::where('library_directory_content_id', $id)->where('user_id', Auth::user()->id)->first();
      $group_folder = InstructorGroupLibrary::where('content_id', $id)->get();
      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }
      $content->delete();

      return redirect()->back();
    }

    public function library_destroy($id){
      $content = LibraryDirectoryContent::find($id);
      $content_user = LibraryDirectoryContentUser::where('library_directory_content_id', $id)->get();
      $group_content = LibraryDirectoryGroupContent::where('group_content_id', $id)->get();
      foreach ($content_user as $key => $value) {
        $content_user_destroy = LibraryDirectoryContentUser::find($value->id);
        $content_user_destroy->delete();
      }
      foreach ($group_content as $key => $value) {
        $group_content_destroy = LibraryDirectoryGroupContent::find($value->id);
        $group_content_destroy->delete();
      }

      $group_folder = InstructorGroupLibrary::where('content_id', $id)->get();
      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }

      if($content->quiz){
        $Quiz = LibraryQuiz::find($content->quiz->id);
        if($Quiz->question){
          foreach ($Quiz->question as $key => $question) {
            $QuizQuestion = LibraryQuizQuestion::find($question->id);
            foreach ($question->quiz_question_answers as $key => $answer) {
              $QuizQuestionAnswer = LibraryQuizQuestionAnswer::find($answer->id);
              $QuizQuestionAnswer->delete();
              // code...
            }
            $QuizQuestion->delete();
            // code...
          }
        }

        if($Quiz->pagebreak){
          foreach ($Quiz->pagebreak as $key => $quiz_page_breaks) {
            $QuizPageBreak = LibraryQuizPageBreak::find($quiz_page_breaks->id)->delete();
          }
        }

        $Quiz->delete();
      }

      $content->delete();

      return redirect()->back();
    }

    public function rollback($library_id){

      $library = LibraryDirectoryContent::find($library_id);
      $library_user = LibraryDirectoryContentUser::where('library_directory_content_id', $library_id)->get();
      foreach ($library_user as $key => $value) {
        $library_user_destroy = LibraryDirectoryContentUser::find($value->id);
        $library_user_destroy->delete();
      }

      $group_folder = InstructorGroupLibrary::where('content_id', $library_id)->get();
      foreach ($group_folder as $key => $value) {
        $group_folder_destroy = InstructorGroupLibrary::find($value->id);
        $group_folder_destroy->delete();
      }
      $library->directory_id = 1;
      $library->shared_code = null;
      $library->save();

      return redirect()->back();
    }

    // quiz

    public function create_quiz(){

      $data = [
        'action' => 'library/course/quiz/create_action/',
        'method' => 'post',
        'button' => 'Create',
        'id' => old('id'),
        'name' => old('name'),
        'description' => old('description'),
        'shuffle' => old('shuffle'),
        'time_start' => old('time_start'),
        'time_end' => old('time_end'),
        'duration' => old('duration','120'),
        'attempt' => old('attempt','1'),
      ];

      return view('library.quiz_form', $data);
    }

    public function create_quiz_action(Request $request){
      // insert Quiz
      $Quiz = new LibraryQuiz;

      $Quiz->user_id = Auth::user()->id;
      $Quiz->name = $request->name;
      $Quiz->description = $request->description;
      $Quiz->slug = str_slug($request->name);
      // $Quiz->shuffle = $request->shuffle;
      $Quiz->time_start = $request->date_start ." ". $request->time_start;
      $Quiz->time_end = $request->date_end ." ". $request->time_end;
      $Quiz->duration = $request->duration;
      $Quiz->attempt = $request->attempt;
      $Quiz->status = 1; // 1= publish, 2 =draft
      $Quiz->save();
      // insert Quiz

      // default page brak
      $QuizPageBreak = new LibraryQuizPageBreak;
      $QuizPageBreak->library_quiz_id = $Quiz->id;
      $QuizPageBreak->title = 'Page 1';
      $QuizPageBreak->save();

      $library_dir_content = new LibraryDirectoryContent;
      $library_dir_content->user_id = Auth::user()->id;
      $library_dir_content->directory_id = 1;
      $library_dir_content->quiz_id = $Quiz->id;
      $library_dir_content->save();
      // default page brak

      \Session::flash('success', 'Create Quiz success');
      return redirect('library/course/quiz/manage/'.$Quiz->id);
      // return redirect()->back();
    }

    public function create_survey(){

      $data = [
        'action' => 'library/course/survey/create_action/',
        'method' => 'post',
        'button' => 'Create',
        'id' => old('id'),
        'name' => old('name'),
        'description' => old('description'),
        'shuffle' => old('shuffle'),
        'time_start' => old('time_start'),
        'time_end' => old('time_end'),
        'duration' => old('duration','120'),
        'attempt' => old('attempt','1'),
      ];

      return view('library.survey_form', $data);
    }

    public function create_survey_action(Request $request){
      // insert Quiz
      $Quiz = new LibraryQuiz;

      $Quiz->user_id = Auth::user()->id;
      $Quiz->name = $request->name;
      $Quiz->description = $request->description;
      $Quiz->slug = str_slug($request->name);
      // $Quiz->shuffle = $request->shuffle;
      $Quiz->time_start = $request->date_start ." ". $request->time_start;
      $Quiz->time_end = $request->date_end ." ". $request->time_end;
      $Quiz->quizz_type = 'survey';
      $Quiz->duration = $request->duration;
      $Quiz->attempt = $request->attempt;
      $Quiz->status = 1; // 1= publish, 2 =draft
      $Quiz->save();
      // insert Quiz

      // default page brak
      $QuizPageBreak = new LibraryQuizPageBreak;
      $QuizPageBreak->library_quiz_id = $Quiz->id;
      $QuizPageBreak->title = 'Page 1';
      $QuizPageBreak->save();

      $library_dir_content = new LibraryDirectoryContent;
      $library_dir_content->user_id = Auth::user()->id;
      $library_dir_content->directory_id = 1;
      $library_dir_content->quiz_id = $Quiz->id;
      $library_dir_content->save();
      // default page brak

      \Session::flash('success', 'Create Quiz success');
      return redirect('library/course/survey/manage/'.$Quiz->id);
      // return redirect()->back();
    }


    public function shared($library_id, $directory_id){

      $library = LibraryDirectoryContent::find($library_id);
      $library->directory_id = $directory_id;
      if($directory_id == 3){
        $code = generateCode();

        $library_cek = LibraryDirectoryContent::where('shared_code', $code)->first();
        while($library_cek){
          $code = generateCode();
          $library_cek = LibraryDirectoryContent::where('shared_code', $code)->first();
        }
        $library->shared_code = $code;
      }
      $library->save();

      return redirect()->back();
    }

    public function edit($id){
      $content = UserLibrary::find($id);

      $data = [
        'action' => 'library/course/content/update/'.$id,
        'method' => 'post',
        'button' => 'Update',
        'section' => null,
        'content' => $content,
      ];

      return view('library.content_edit_form', $data);
    }

    public function edit_quiz($id){
      $quiz = LibraryQuiz::find($id);

      $data = [
        'action' => 'library/course/quiz/update/'.$id,
        'method' => 'post',
        'button' => 'Update',
        'quiz' => $quiz,
      ];

      return view('library.quiz_edit_form', $data);
    }

    public function edit_assignment($id){
      $assignment = LibraryAssignment::find($id);

      $data = [
        'action' => 'library/course/assignment/update/'.$id,
        'method' => 'post',
        'button' => 'Update',
        'assignment' => $assignment,
      ];

      return view('library.assignment_edit_form', $data);
    }

    public function update_assignment(Request $request, $id){
        // insert Assignment
        $Assignment = LibraryAssignment::find($id);
        $Assignment->title = $request->title;
        $Assignment->description = $request->description;
        $Assignment->type = $request->type;
        $Assignment->attempt = $request->attempt;
        $Assignment->time_start = $request->time_start;
        $Assignment->time_end = $request->time_end;
        $Assignment->save();

        \Session::flash('success', 'Create Assignment success');
        return redirect('instructor/library');
    }

    public function update_quiz(Request $request, $id){
        // insert Quiz
        $Quiz = LibraryQuiz::find($id);

        $Quiz->name = $request->name;
        $Quiz->description = $request->description;
        $Quiz->slug = str_slug($request->name);
        // $Quiz->shuffle = $request->shuffle;
        $Quiz->time_start = $request->date_start ." ". $request->time_start;
        $Quiz->time_end = $request->date_end ." ". $request->time_end;
        $Quiz->duration = $request->duration;
        $Quiz->status = 1; // 1= publish, 2 =draft
        $Quiz->save();

        return redirect('instructor/library');
    }

    public function update(Request $request, $id){
        $content = UserLibrary::find($id);
        $content->title = $request->title;
        $content->description = $request->description;

        if($content->type_content == 'video'){
          if($request->video_file){
            $filesize = $request->video_file->getSize();
            $filenameOrigin = $request->video_file->getClientOriginalName();
            $filename = time() . '.' . $request->video_file->getClientOriginalExtension();
            $request->video_file->move(public_path('content/video'), $filename);
            $path_file = 'content/video/'.$filename;
            $full_path_original = asset('content/video/'.$filename);
            $content->name_file = $filenameOrigin;
            $content->path_file = $path_file;
            $content->file_size = $filesize;
            $content->video_duration = $request->video_duration;
            $content->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
            $content->full_path_original = $full_path_original;
          }
        }

        if($content->type_content == 'file'){
          if($request->file){
            $filesize = $request->file->getSize();
            $filenameOrigin = $request->file->getClientOriginalName();
            $filename = time() . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('content/video'), $filename);
            $path_file = 'content/file/'.$filename;
            $full_path_original = asset('content/video/'.$filename);

            $content->name_file = $filenameOrigin;
            $content->path_file = $path_file;
            $content->file_size = $filesize;
            $content->video_duration = $request->video_duration;
            $content->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
            $content->full_path_original = $full_path_original;
            $content->file_size = $filesize;
          }
        }

        $content->save();

        return redirect('instructor/library');
    }
}

?>
