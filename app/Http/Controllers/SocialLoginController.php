<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\UserGroup;
use App\Instructor;
use App\SocialLoginUser;
use App\SsoAuth;
use Auth;
use Session;

use App\Http\Controllers\Api\App\AuthUser;

class SocialLoginController extends Controller
{

  public function Facebook()
  {
    return Socialite::driver('facebook')->redirect();
  }

  public function Google()
  {
    \Session::put('backUrl', \URL::previous());
    return Socialite::driver('google')->redirect();
  }

  public function FacebookCallback()
  {
    $response = Socialite::driver('facebook')->user(); // get data facebook
    // dd($response);

    if($response->email != null){ // check email facebook not null
      if(!User::where('email', $response->email)->first()){
        // masukan user baru jika user belum terdaftar
        $User = new User;
        $User->email = $response->email;
        $User->username = $response->email;
        $User->name = $response->name;
        $User->slug = str_slug($response->name);
        $User->photo = $response->avatar;
        $User->password = bcrypt('password_default');
        $User->auth_token = str_random(60);
        $User->is_active = 1;
        $User->save();
        // masukan user baru jika user belum terdaftar

        // masukan grup user
        $UserGroup = new UserGroup;
        $UserGroup->user_id = $User->id;
        $UserGroup->level_id = '3';
        $UserGroup->save();
        // masukan grup user

        // masukan data login social
        $SocialLoginUser = new SocialLoginUser;
        $SocialLoginUser->social_id = $response->id;
        $SocialLoginUser->email = $response->email;
        $SocialLoginUser->name = $response->name;
        $SocialLoginUser->avatar = $response->avatar;
        $SocialLoginUser->provider = 'facebook';
        $SocialLoginUser->save();
        // masukan data login social

        // jika daftar instruktur
        if(Session::get('register_instructor')){
          // masukan grup instruktur
          $UserGroup = new UserGroup;
          $UserGroup->user_id = $User->id;
          $UserGroup->level_id = '2';
          $UserGroup->save();
          // masukan grup instruktur

          // insert instructor
          $Instructor = new Instructor;
          $Instructor->user_id = $User->id  ;
          $Instructor->status = '1';
          $Instructor->save();
          // insert instructor
        }
        // jika daftar instruktur

        // auto login setelah daftar
        Auth::attempt(['email' => $response->email, 'password' => 'password_default']);
        // auto login setelah daftar

      }else{

        // jika user belum menggunkan social login
        if(!SocialLoginUser::where(['email' => $response->email, 'provider' => 'facebook'])->first()){
          $SocialLoginUser = new SocialLoginUser;
          $SocialLoginUser->social_id = $response->id;
          $SocialLoginUser->email = $response->email;
          $SocialLoginUser->name = $response->name;
          $SocialLoginUser->avatar = $response->avatar;
          $SocialLoginUser->provider = 'facebook';
          $SocialLoginUser->save();
        }
        // jika user belum menggunkan social login

        $User = User::where('email', $response->email)->first(); // get data user
        if($User->auth_token==null){
          User::where('email', $response->email)->update(['auth_token' => str_random(60)]);
        }

        // jika daftar instruktur
        if(Session::get('register_instructor')){
          if(!Instructor::where('user_id', $User->id)->first()){
            // masukan grup instruktur
            $UserGroup = new UserGroup;
            $UserGroup->user_id = $User->id;
            $UserGroup->level_id = '2';
            $UserGroup->save();
            // masukan grup instruktur

            // insert instructor
            $Instructor = new Instructor;
            $Instructor->user_id = $User->id  ;
            $Instructor->status = '1';
            $Instructor->save();
            // insert instructor
          }
        }
        // jika daftar instruktur

        // auto login
        Auth::login($User);
        // auto login
      }

    }else{
      // jika data facebook tidak memiliki email
      Session::put('name', $response->name);
      return redirect('register');
      // jika data facebook tidak memiliki email
    }

    return redirect()->intended(url()->previous());

  }

  public function GoogleCallback(Request $request)
  {
    $response = Socialite::driver('google')->stateless()->user();

    // dd($response);

    if(!User::where('email', $response->email)->first()){
      $User = new User;
      $User->email = $response->email;
      $User->username = $response->email;
      $User->name = $response->name;
      $User->slug = str_slug($response->name);
      $User->photo = $response->avatar;
      $User->auth_token = str_random(60);
      $User->password = bcrypt('password_default');
      $User->is_active = 1;
      $User->save();

      $UserGroup = new UserGroup;
      $UserGroup->user_id = $User->id;
      $UserGroup->level_id = '3';
      $UserGroup->save();

      $SocialLoginUser = new SocialLoginUser;
      $SocialLoginUser->social_id = $response->id;
      $SocialLoginUser->email = $response->email;
      $SocialLoginUser->name = $response->name;
      $SocialLoginUser->avatar = $response->avatar;
      $SocialLoginUser->provider = 'google';
      $SocialLoginUser->save();

      // jika daftar instruktur
      if(Session::get('register_instructor')){
        // masukan grup instruktur
        $UserGroup = new UserGroup;
        $UserGroup->user_id = $User->id;
        $UserGroup->level_id = '2';
        $UserGroup->save();
        // masukan grup instruktur

        // insert instructor
        $Instructor = new Instructor;
        $Instructor->user_id = $User->id  ;
        $Instructor->status = '1';
        $Instructor->save();
        // insert instructor
      }
      // jika daftar instruktur

      Auth::attempt(['email' => $response->email, 'password' => 'password_default']);
    }else{
      if(!SocialLoginUser::where(['email' => $response->email, 'provider' => 'facebook'])->first()){
        $SocialLoginUser = new SocialLoginUser;
        $SocialLoginUser->social_id = $response->id;
        $SocialLoginUser->email = $response->email;
        $SocialLoginUser->name = $response->name;
        $SocialLoginUser->avatar = $response->avatar;
        $SocialLoginUser->provider = 'facebook';
        $SocialLoginUser->save();
      }

      $User = User::where('email', $response->email)->first();
      if($User->auth_token==null){
        User::where('email', $response->email)->update(['auth_token' => str_random(60)]);
      }

      // jika daftar instruktur
      if(Session::get('register_instructor')){
        if(!Instructor::where('user_id', $User->id)->first()){
          // masukan grup instruktur
          $UserGroup = new UserGroup;
          $UserGroup->user_id = $User->id;
          $UserGroup->level_id = '2';
          $UserGroup->save();
          // masukan grup instruktur

          // insert instructor
          $Instructor = new Instructor;
          $Instructor->user_id = $User->id  ;
          $Instructor->status = '1';
          $Instructor->save();
          // insert instructor
        }
      }
      // jika daftar instruktur

      Auth::login($User);
    }
    return redirect()->intended(\Session::get('backUrl'));
  }
}
