<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class MobileProfileController extends Controller
{

  public function index($id){
    $User = User::where(['id' => $id])->first();
    return view('profile.mindex', compact('User'));
  }

  public function update($id, Request $request){
    $User = User::where('id', $id)->first();
    $User->name = $request->name;
    $User->save();

    return redirect('m/profile/'.$id);
  }

  public function update_password($id, Request $request){
    $User = User::where('id', $id)->first();
    $User->password = bcrypt($request->password);
    $User->save();

    return redirect('m/profile/'.$id);
  }

  public function update_avatar($id, Request $request){
    $User = User::where('id', $id)->first();
    $default_photo = "/uploads/users/default.png";
    $import_photo = explode('/', $User->photo);
    if($default_photo != $User->photo){
      if($import_photo[1] == 'uploads'){        
        unlink(asset_path($User->photo));
      }
    }

    $destinationPath = asset_path('uploads/users/'); // upload path
    $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
    $image = str_slug($User->name).rand(1111,9999).'.'.$extension; // renameing image
    $request->file('image')->move($destinationPath, $image); // uploading file to given path

    $User = User::where('id', $id)->first();
    $User->photo = '/uploads/users/'.$image;
    $User->save();

    return redirect('m/profile/'.$id);
  }
}
