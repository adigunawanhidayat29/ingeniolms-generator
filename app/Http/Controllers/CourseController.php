<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Validator;
use Storage;
use App\Program;
use App\Course;
use App\CourseSetting;
use App\DefaultActivityCompletion;
use App\ActivityCompletion;
use App\CourseLive;
use App\CourseBatch;
use App\Section;
use App\Content;
use App\CourseUser;
use App\Meeting;
use App\Progress;
use App\Discussion;
use App\Quiz;
use App\QuizQuestion;
use App\QuizParticipant;
use App\QuizQuestionAnswer;
use App\Assignment;
use App\CourseAnnouncement;
use App\Rating;
use App\CourseProject;
use App\Category;
use App\CourseBroadcast;
use App\ContentVideoQuiz;
use App\PlayerIdUser;
use App\MeetingSchedule;
use App\UserLibrary;
use App\User;
use App\ProgramCourse;
use App\AssignmentAnswer;
use App\CourseUpdate;
use App\Theme;
use App\InstructorGroup;
use App\LibraryDirectory;
use App\LibraryDirectoryGroup;
use App\InstructorGroupLibrary;
use App\Certificate;
use App\CeritificateCourseUser;
use App\ProgramUser;
use App\ActivityOrder;
use App\RestrictAccess;
use App\CourseStudentGroup;
use App\CourseStudentGroupUser;
use App\GroupHasContent;
use App\CourseAttendance;
use App\CourseAttendanceUser;
use App\TrainingProviderTrainee;
use App\UserLevel;
use App\UserGroup;
use App\NotificationUser;
use App\BadgeContentAssign;
use App\BadgeFix;
use App\BadgeUsersFix;
use App\BadgeCriteriaAssign;
use App\BadgeCourseAssign;
use App\CourseLeveling;
use App\LevelingRule;
use App\LevelingRulesContent;
use App\UserLevelingLog;
use Auth;
use File;
use Cart;
use Illuminate\Support\Facades\Mail;
use App\Mail\Course as MailCourse;
use Image;
use Plupload;
use FFMpeg;
use Cookie;
use Lang;

use Illuminate\Support\Debug\Dumper;

class CourseController extends Controller
{
  public function index()
  {

    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id', 'categories.title as category')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
      ->where('status', '1')
      ->where('archive', '0')
      ->whereNotIn('courses.id', $data_course_id);

    $parentCategories = [];
    $newCategories = [];
    if (\Request::get('parent')) {

      if (!\Request::get('child')) {
        $parentCategories = explode('-', \Request::get('parent'));
        $getChildCategories = Category::select('id')->whereIn('parent_category', $parentCategories)->orWhereIn('id', $parentCategories)->get();
        foreach ($getChildCategories as $getChildCategory) {
          $newCategories[] = $getChildCategory->id;
        }
        $courses = $courses->whereIn('courses.id_category', $newCategories);
      } else {
        $childCategories = explode('-', \Request::get('child'));
        $getChildCategories = Category::select('id')->whereIn('id', $childCategories)->get();
        foreach ($getChildCategories as $getChildCategory) {
          $newCategories[] = $getChildCategory->id;
        }
        $courses = $courses->whereIn('courses.id_category', $newCategories);
      }
    }

    $courses = $courses->where('public', '1')
      ->whereNotIn('courses.id', function ($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->orderBy('courses.id', 'desc')
      ->paginate(16);

    $data = [
      'courses' => $courses,
    ];
    return view('course.index', $data);
  }

  public function update_order(Request $request)
  {
    if ($request->datas) {
      foreach ($request->datas as $key => $value) {
        $Content = ActivityOrder::find($value);
        $Content->order = $key;
        $Content->save();
      }
    }

    return $request;
  }

  public function restrict_order_create($course_id)
  {
    $courses = Course::find($course_id);

    $section_all = [];
    $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence', 'asc')->get();

    $num_contents = 0;
    foreach ($sections as $section) {
      $contents = Content::with('activity_order')->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder')->where('id_section', $section->id)->get();
      $quizzes = Quiz::with('activity_order')->where('id_section', $section->id)->get();
      $assignments = Assignment::with('activity_order')->where('id_section', $section->id)->get();

      foreach ($contents as $content) {
        $content->section_type = 'content';
        $section_all[] = $content;
      }

      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }

      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
    }

    $new_content = collect($section_all)->sortBy('activity_order.order');
    $new_content = $new_content->values();
    for ($i = 0; $i < count($new_content); $i++) {
      if ($i != 0) {
        $from = $new_content[$i]->section_type . '_from_id';
        $to = $new_content[$i - 1]->section_type . '_to_id';
        $restrict_access = RestrictAccess::where($from, $new_content[$i]->id)->where($to, $new_content[$i - 1]->id)->first();

        if (!$restrict_access) {
          $restrict = new RestrictAccess;
          $restrict->$from = $new_content[$i]->id;
          $restrict->$to = $new_content[$i - 1]->id;
          $restrict->required_status = 0;
          $restrict->save();
        }
      }
    }

    return $new_content;
  }

  public function live()
  {
    $course_lives = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->where('courses.status', '1')
      ->where('courses.public', '1')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('course_lives', 'course_lives.course_id', '=', 'courses.id')
      ->orderBy('courses.id', 'desc')
      ->paginate(6);
    $data = [
      'courses' => $course_lives,
    ];
    return view('course.live', $data);
  }

  public function setting($course_id)
  {
    $User = Auth::user();

    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author', 'users.photo as photo', 'course_levels.id as level_id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
      ->leftJoin('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.id', $course_id)->first();

    $data = [
      'User' => $User,
      'course' => $courses,
      'bank_name' => 'BNI',
      'bank_branch' => 'test',
      'account_name' => 'Jihad',
      'account_number' => 'asas',
      'categories' => Category::get()
    ];
    return view('course.advanced_setting', $data);
  }

  public function activity_completion_create($content_id, $content_type)
  {

    $activity_completion = new ActivityCompletion;
    if ($content_type == "assigment") {
      $activity_completion->assigment_id = $content_id;
    } elseif ($content_type == "quizz") {
      $activity_completion->quizz_id = $content_id;
    } else {
      $activity_completion->content_id = $content_id;
    }

    $activity_completion->type_content = $content_type;
    $activity_completion->save();

    return $activity_completion;
  }

  public function activity_completion_destroy($content_id)
  {

    $activity_completion = ActivityCompletion::find($content_id);
    $activity_completion->delete();

    return "success";
  }

  public function update_submit(Request $request)
  {

    $group_update = new CourseUpdate;
    $group_update->course_id = $request->course_id;
    $group_update->user_id = Auth::user()->id;
    $group_update->description = $request->body;
    if ($request->parents) {
      $group_update->level_1 = $request->parents;
    }

    $new_file = [];
    if ($request->fileinput) {
      $new_file = $request->fileinput;
    }
    $file = json_encode(array_values($new_file));

    $group_update->file = $file;
    $group_update->save();

    return redirect()->back();
  }

  public function detail($slug)
  {
    $AuthorCourses = [];
    $totalStudents = 0;
    $authorTotalRating = 0;
    $authorCountRating = 0;
    $authorAverageRating = 0;

    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author', 'users.photo as photo', 'users.slug as author_slug', 'users.short_description as author_short_description', 'users.description as author_description', 'courses.id as id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.public', '=', '1')
      ->where('courses.status', '=', '1')
      ->where('courses.slug', $slug)
      ->first();

    if ($courses) {

      // set cookies
      if (!Cookie::get('affiliate')) {
        if (\Request::get('aff')) {
          // set cookie
          Cookie::queue(Cookie::make('affiliate', \Request::get('aff'), 129600));
        }
      }
      // set cookies

      $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))
        ->where('course_id', $courses->id)
        ->first();

      if ($Rating->count_rating != 0) {
        $Rate = $Rating->rating / $Rating->count_rating;
      } else {
        $Rate = 0;
      }

      $GetAuthorCourses = DB::table('courses')->select('courses.*', 'courses.id as course_id', 'users.*', 'users.slug as user_slug', 'courses.slug as slug')
        ->where(['status' => '1', 'id_author' => $courses->id_author])
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->orderBy('courses.id', 'desc')
        ->get();

      $iter = 0;
      foreach ($GetAuthorCourses as $getCourse) {
        $RatingA = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $getCourse->course_id)->first();
        if ($RatingA->count_rating != 0) {
          $RateA = $RatingA->rating / $RatingA->count_rating;
        } else {
          $RateA = 0;
        }
        $array_ratingA = array('course_rating' => $RateA, 'course_count_rating' => $RatingA->count_rating);
        $contents_mergeA = array_merge((array)$GetAuthorCourses[$iter], $array_ratingA);
        array_push($AuthorCourses, $contents_mergeA);
        $iter++;
      }

      $totalCourse = count($GetAuthorCourses);
      foreach ($GetAuthorCourses as $AuthorCourse) {
        $totalStudents = $totalStudents + count(DB::table('courses_users')->where(['course_id' => $AuthorCourse->course_id])->get());
        $AuthorRatings = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $AuthorCourse->course_id)->first();
        $authorTotalRating = $authorTotalRating + $AuthorRatings->rating;
        $authorCountRating = $authorCountRating + $AuthorRatings->count_rating;
      }
      if ($authorCountRating != 0) {
        $authorAverageRating = $authorTotalRating / $authorCountRating;
      }

      // check course on cart
      $CourseOnCart = false;
      foreach (Cart::content() as $cart) {
        if ($cart->id == $courses->id) {
          $CourseOnCart = true;
        }
      }
      // check course on cart

      // check logged in or not
      $user = Auth::user();
      if ($user) {
        $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id])->first();
      } else {
        $course_user = [];
      }
      // check logged in or not

      // check user joined course or not
      if ($course_user) {
        return redirect('course/learn/' . $slug);
      } else {
        $content_previews_data = [];
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence', 'asc')->get();

        foreach ($sections as $section) {
          $contents = DB::table('contents')->where('id_section', $section->id)->where('status', '1')->orderBy('sequence', 'asc')->get();
          $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();

          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
          ]);

          $content_previews = DB::table('contents')->where('id_section', $section->id)->where('preview', '1')->orderBy('sequence', 'asc')->get();
          foreach ($content_previews as $index => $content_preview) {
            array_push($content_previews_data, [
              'id' => $content_preview->id,
              'title' => $content_preview->title,
              'type_content' => $content_preview->type_content,
            ]);
          }
        }

        // dd($content_previews_data);

        $data = [
          'course' => $courses,
          'CourseOnCart' => $CourseOnCart,
          'sections' => $section_data,
          'content_previews' => $content_previews_data,
          'average_rating' => round($authorAverageRating, 2),
          'total_courses' => $totalCourse,
          'total_students' => $totalStudents,
          'rate' => $Rate,
          'count' => ($Rating->count_rating != 0 ? $Rating->count_rating : 0),
        ];

        $checkTheme = Theme::where(['status' => '1'])->first();
        if ($checkTheme) {
          $theme = $checkTheme->name . '/';
        } else {
          $theme = '';
        }

        return view($theme . 'course.detail', $data);
      }
      // check user joined course or not
    } else {
      return view('errors.404');
    }
  }

  public function preview($course_id)
  {
    $user = Auth::user();
    // {{ For checking the author of the course, please pull snippets below on server }}
    $UserAuthorId = Auth::user()->id; // {{ Getting signed in instructor }}

    // $checkingAuthor = Course::whereRaw("find_in_set($user->id, id_author)")
    //   ->orWhere('courses.id_instructor_group',function($query) use($user) {
    //      $query->select('instructor_groups.id')
    //        ->whereRaw("find_in_set($user->id, user_id)")
    //        ->from('instructor_groups')
    //        ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
    //   })->first();
    // if (empty($checkingAuthor)) { // {{ If the course not authored by the signed in instructor, redirect to back request }}
    //   return back();
    // }
    // {{ For checking the author of the course, please pull snippets above on server }}

    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author', 'users.photo as photo', 'course_levels.id as level_id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
      ->leftJoin('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.id', $course_id)->first();

    $courses_all = Course::where('id_author', 'like', '%' . Auth::user()->id . '%')->where('id', '!=', $courses->id)->get();

    // dd($courses);

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence', 'asc')->get();

    $num_contents = 0;

    foreach ($sections as $index => $section) {

      $section_all = [];

      $contents = Content::with('activity_order')->where('id_section', $section->id)
        ->orderBy('contents.sequence', 'asc')
        ->get();
      $quizzes = Quiz::with('activity_order')->where(['id_section' => $section->id])->get();
      $assignments = Assignment::with('activity_order')->where(['id_section' => $section->id])->get();
      $num_contents += count($contents);

      foreach ($contents as $content) {
        $content->section_type = 'content';
        $section_all[] = $content;
      }

      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }

      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'status' => $section->status,
        'sequence' => $section->sequence,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_contents' => $contents,
        'section_quizzes' => $quizzes,
        'section_assignments' => $assignments,
        'section_all' => $section_all,
      ]);
    }

    // print("<pre>".print_r($section_data,true)."</pre>");

    $meeting = Meeting::where('course_id', $course_id)->first();
    $update = CourseUpdate::where('course_id', $course_id)->get();
    $discussions = Discussion::select('discussions.id as id', 'discussions.course_id', 'discussions.body', 'discussions.created_at', 'users.name', 'users.photo')
      ->where(['course_id' => $course_id, 'status' => '1'])
      ->join('users', 'users.id', '=', 'discussions.user_id')
      ->orderby('discussions.created_at', 'desc')
      ->limit(5)
      ->get();

    $announcements = CourseAnnouncement::select('course_announcements.*', 'users.photo as photo', 'users.name')
      ->where('course_id', $course_id)
      ->join('courses', 'course_announcements.course_id', '=', 'courses.id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->orderby('course_announcements.created_at', 'desc')->get();

    $announcements_data = [];
    foreach ($announcements as $announcement) {
      $announcement_comments = DB::table('course_announcement_comments')
        ->join('users', 'users.id', '=', 'course_announcement_comments.user_id')
        ->where('course_announcement_id', $announcement->id)->get();
      array_push($announcements_data, [
        'id' => $announcement->id,
        'course_id' => $announcement->course_id,
        'title' => $announcement->title,
        'description' => $announcement->description,
        'status' => $announcement->status,
        'created_at' => $announcement->created_at,
        'photo' => $announcement->photo,
        'name' => $announcement->name,
        'announcement_comments' => $announcement_comments,
      ]);
    }

    $meeting_schedules = MeetingSchedule::where('course_id', $course_id)->orderBy('date', 'asc')->get();

    // library

    $folder = LibraryDirectoryGroup::where('question_bank', '0')->where('user_id', Auth::user()->id)->orWhereHas('shared', function ($query) {
      $query->where('user_id', Auth::user()->id);
    })->get();
    $myLibrary = LibraryDirectory::with(['library' => function ($query) {
      $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id)->orWhereHas('shared', function ($query) {
        $query->where('user_id', Auth::user()->id);
      });
    }])->get();
    $publicLibrary = LibraryDirectory::get();
    $publicLibrary = $publicLibrary[1];
    $group = InstructorGroup::with('library')->whereIn('user_id', [Auth::user()->id])->get();

    $data = [
      'course' => $courses,
      'course_all' => $courses_all,
      'sections' => $section_data,
      'meeting' => $meeting,
      'meeting_schedules' => $meeting_schedules,
      'discussions' => $discussions,
      'announcements' => $announcements_data,
      'num_contents' => $num_contents,
      'num_progress' => $num_contents,
      'percentage' => 100,
      'rate' => 5,
      'authors' => DB::table('users')->select('users.id', 'users.name', 'users.email')->join('instructors', 'instructors.user_id', '=', 'users.id')->whereNotIn('users.id', explode(',', $courses->id_author))->get(),
      'instructor_groups' => DB::table('instructor_groups')->whereRaw("find_in_set($user->id, user_id)")->whereNotIn('id', explode(',', $courses->id_instructor_group))->get(),
      'folder' => $folder,
      'myLibrary' => $myLibrary,
      'publicLibrary' => $publicLibrary,
      'group' => $group,
      'update' => $update,
    ];
    return view('course.preview', $data);
  }

  public function learn($slug)
  {
    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author', 'users.photo as photo', 'users.short_description as author_short_description', 'users.slug as author_slug', 'courses.id as id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.slug', $slug)->first();

    $user = Auth::user();
    $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id]);
    if ($course_user->first()) {
      $section_data = [];
      $sections = DB::table('sections')->where('id_course', $courses->id)->where('status', '1')->orderBy('sequence', 'asc')->get();

      $num_contents = 0;

      foreach ($sections as $index => $section) {

        $section_all = [];

        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
        $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->orderBy('contents.sequence', 'asc')
          ->get();

        $group_content_id = [];
        foreach ($group_has_contents as $data) {
          array_push($group_content_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->where(function ($query) use ($group_content_id) {
            $query->whereNotIn('id', $group_content_id);
          })
          ->orderBy('contents.sequence', 'asc')
          ->get();
        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->orderBy('contents.sequence', 'asc')
          ->get();
        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
        $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->get();

        $group_quiz_id = [];
        foreach ($group_has_quizzes as $data) {
          array_push($group_quiz_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
          $query->whereNotIn('id', $group_quiz_id);
        })->get();
        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
        $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->get();

        $group_assignment_id = [];
        foreach ($group_has_assignments as $data) {
          array_push($group_assignment_id, $data->table_id);
        }
        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
          $query->whereNotIn('id', $group_assignment_id);
        })->get();
        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
        $num_contents += count($quizzes);
        $num_contents += count($assignments);

        // MERGING KONTEN
        foreach ($contents as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        foreach ($group_has_contents_users as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        // MERGING KONTEN

        // MERGING KUIS
        foreach ($quizzes as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        foreach ($group_has_quizzes_users as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        // MERGING KUIS

        // MERGING TUGAS
        foreach ($assignments as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        foreach ($group_has_assignments_users as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        // MERGING TUGAS

        array_push($section_data, [
          'id' => $section->id,
          'title' => $section->title,
          'description' => $section->description,
          'id_course' => $section->id_course,
          'created_at' => $section->created_at,
          'updated_at' => $section->updated_at,
          'section_contents' => $contents,
          'section_quizzes' => $quizzes,
          'section_assignments' => $assignments,
          'section_all' => $section_all,
        ]);
      }

      // dd($section_data);

      $meeting = Meeting::where('course_id', $courses->id)->first();
      $discussions = Discussion::select('discussions.id as id', 'discussions.course_id', 'discussions.body', 'discussions.created_at', 'users.name', 'users.photo')
        ->where(['course_id' => $courses->id, 'status' => '1'])
        ->join('users', 'users.id', '=', 'discussions.user_id')
        ->orderby('discussions.created_at', 'desc')
        ->paginate(5);

      $announcements = CourseAnnouncement::select('course_announcements.*', 'users.photo as photo', 'users.name')
        ->where('course_id', $courses->id)
        ->join('courses', 'course_announcements.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->orderby('course_announcements.created_at', 'desc')->get();

      $announcements_data = [];
      foreach ($announcements as $announcement) {
        $announcement_comments = DB::table('course_announcement_comments')
          ->join('users', 'users.id', '=', 'course_announcement_comments.user_id')
          ->where('course_announcement_id', $announcement->id)->get();
        array_push($announcements_data, [
          'id' => $announcement->id,
          'course_id' => $announcement->course_id,
          'title' => $announcement->title,
          'description' => $announcement->description,
          'status' => $announcement->status,
          'created_at' => $announcement->created_at,
          'photo' => $announcement->photo,
          'name' => $announcement->name,
          'announcement_comments' => $announcement_comments,
        ]);
      }
      // dd($announcements_data);

      $Progress = Progress::where(['course_id' => $courses->id, 'user_id' => $user->id, 'status' => '1']);

      //count percentage
      $num_progress = 0;
      $num_progress = $Progress->count();
      $percentage = 0;
      $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
      $percentage = $percentage == 0 ? 1 : $percentage;
      $percentage = 100 / $percentage;
      //count percentage

      //count rating
      $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $courses->id)->first();
      if ($Rating->count_rating != 0) {
        $Rate = $Rating->rating / $Rating->count_rating;
      } else {
        $Rate = 0;
      }
      //count rating

      // get projects
      $Projects = CourseProject::where('course_id', $courses->id)->get();
      // get projects

      // get Broadcast
      $Broadcast = CourseBroadcast::where(['course_id' => $courses->id, 'status' => '1'])->orderBy('id', 'desc')->first();
      // get Broadcast

      // get meeting_schedules
      $meeting_schedules = MeetingSchedule::where('course_id', $courses->id)->orderBy('date', 'asc')->get();
      // get meeting_schedules

      // get content next learn
      $NextContent = Content::select('contents.id as id')->whereNotIn('contents.id', function ($query) use ($courses, $user) {
        $query->select('progresses.content_id')
          ->from('progresses')
          ->where(['progresses.status' => '1', 'progresses.course_id' => $courses->id, 'progresses.user_id' => $user->id]);
      })
        ->join('sections', 'sections.id', '=', 'contents.id_section')
        ->where('sections.id_course', $courses->id)
        ->where('contents.status', '1')
        ->orderBy('sections.sequence', 'asc')
        ->orderBy('sections.updated_at', 'desc')
        ->orderBy('contents.id', 'asc')
        ->first();
      // dd($NextContent);

      $FirstContent = $contents = Content::select('contents.id as id')
        ->join('sections', 'sections.id', '=', 'contents.id_section')
        ->where('sections.id_course', '=', $courses->id)
        ->where('contents.status', '1')
        ->orderBy('sections.sequence', 'asc')
        ->orderBy('sections.updated_at', 'desc')
        ->orderBy('contents.id', 'asc')
        ->first();
      // dd($FirstContent);
      // get content next learn

      $ceritificate = [];

      if ($courses->get_ceritificate != '0') {

        // get ceritificate
        $ceritificate = Certificate::where(['status' => '1'])
          ->whereRaw('FIND_IN_SET(' . $courses->id . ', course_id)')
          ->with('certificate_attributes')
          ->first();

        // kondisi jika sertifkat tidak sesuai dengan course_id maa default nya adlah admin creator
        if (!$ceritificate) {
          $ceritificate = Certificate::where(['status' => '1', 'course_id' => null])
            ->with('certificate_attributes')
            ->first();
        }
      }

      // $course_attendance = CourseAttendance::with('course_attendance_user_hasone')->where('course_id', $courses->id)->whereDate('datetime', date("Y-m-d"))->first();

      $CourseAttendances = CourseAttendance::with('course_attendance_user_hasone')->where('course_id', $courses->id)->get();
      // dd($CourseAttendances );

      $badge_course_assign = BadgeCourseAssign::where('course_id', $courses->id)->get();
      
      if($badge_course_assign)
      {
        foreach($badge_course_assign as $data_assign){
          $badge_check = BadgeUsersFix::where('user_id', $user->id)->where('badge_id', $data_assign->badge_id)->count();
        
          if($badge_check == 0)
          {
            $badge_publish = BadgeFix::where('id', $data_assign->badge_id)->value('publish');
            
            if($badge_publish == 1 && $num_contents > 0 && $percentage >= 100)
            {
              $badge = new BadgeUsersFix;
              $badge->user_id = $user->id;
              $badge->badge_id = $data_assign->badge_id;
              $badge->save();
            }
          }
        }
        
      }

      $users_leveling_logs = UserLevelingLog::join('users', 'users_leveling_logs.user_id', '=', 'users.id')
        ->select('users_leveling_logs.leveling_rules_id as leveling_rules_id', 'users.name as user_name', 'users_leveling_logs.created_at as log_created_at', 'users_leveling_logs.reward as log_reward')
        ->where('course_id', $courses->id)
        ->where('user_id', $user->id)
        ->orderBy('users_leveling_logs.updated_at', 'desc')
        ->take(3)->get();

      $data = [
        'users_leveling_logs' => $users_leveling_logs,
        'course' => $courses,
        'course_user' => $course_user->first(),
        'sections' => $section_data,
        'meeting' => $meeting,
        'meeting_schedules' => $meeting_schedules,
        'discussions' => $discussions,
        'announcements' => $announcements_data,
        'projects' => $Projects,
        'num_contents' => $num_contents,
        'num_progress' => $num_progress,
        'percentage' => $percentage,
        'rate' => $Rate,
        'Broadcast' => $Broadcast,
        'NextContent' => $NextContent == null ? $FirstContent : $NextContent,
        'certificate' => $ceritificate,
        'CourseAttendances' => $CourseAttendances,
      ];
      
      return view('course.learn', $data);
    } else {
      return redirect('course/' . $slug);
    }
  }

  public function learn_content_discussion($slug, $content_id)
  {
    $AssignmentAnswer = AssignmentAnswer::where(['assignments_answers.assignment_id' => $id, 'user_id' => Auth::user()->id])->leftJoin('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id');
    $Assignment = Assignment::where('id', $id)->first();
    if (date("Y-m-d H:i:s") <= $Assignment->time_end) {

      $courses = Course::where('slug', $slug)->with('sections')->first();
      $data = [
        'assignment' => $Assignment,
        'assignments_answers' => $AssignmentAnswer->get(),
        'course' => $courses,
      ];

      if ($AssignmentAnswer->count() < $Assignment->attempt) { // check answer attempt
        return view('assignment.detail', $data);
      } else {
        \Session::flash('success', Lang::get('front.page_manage_courses.assignment_has_answered'));
        return view('assignment.my_answer', $data);
      }
    } else {
      return redirect('/');
    }
  }

  public function learn_content($slug, $content_id)
  {

    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.slug', $slug)->first();

    $user = Auth::user();
    $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id]); // check user course
    if ($course_user->first()) {

      $section_all = [];
      $section_data = [];
      $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence', 'asc')->get();

      $num_contents = 0;
      foreach ($sections as $index => $section) {

        $section_all = [];

        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
        $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->orderBy('contents.sequence', 'asc')
          ->get();

        $group_content_id = [];
        foreach ($group_has_contents as $data) {
          array_push($group_content_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->where(function ($query) use ($group_content_id) {
            $query->whereNotIn('id', $group_content_id);
          })
          ->orderBy('contents.sequence', 'asc')
          ->get();
        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->orderBy('contents.sequence', 'asc')
          ->get();
        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
        $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->get();

        $group_quiz_id = [];
        foreach ($group_has_quizzes as $data) {
          array_push($group_quiz_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
          $query->whereNotIn('id', $group_quiz_id);
        })->get();
        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
        $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->get();

        $group_assignment_id = [];
        foreach ($group_has_assignments as $data) {
          array_push($group_assignment_id, $data->table_id);
        }
        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
          $query->whereNotIn('id', $group_assignment_id);
        })->get();
        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->where('course_student_group_users.user_id', '=', Auth::user()->id)
          ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
        $num_contents += count($quizzes);
        $num_contents += count($assignments);

        // MERGING KONTEN
        foreach ($contents as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        foreach ($group_has_contents_users as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        // MERGING KONTEN

        // MERGING KUIS
        foreach ($quizzes as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        foreach ($group_has_quizzes_users as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        // MERGING KUIS

        // MERGING TUGAS
        foreach ($assignments as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        foreach ($group_has_assignments_users as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        // MERGING TUGAS

        array_push($section_data, [
          'id' => $section->id,
          'title' => $section->title,
          'description' => $section->description,
          'id_course' => $section->id_course,
          'created_at' => $section->created_at,
          'updated_at' => $section->updated_at,
          'section_contents' => $contents,
          'section_quizzes' => $quizzes,
          'section_assignments' => $assignments,
          'section_all' => $section_all,
        ]);
      }

      $Content = Content::where('id', $content_id)->first();

      // check content on progress
      $default_type = $Content->type_content;
      if (strpos($default_type, '-') !== false) {
        $default_type = explode('-', $default_type);
        $default_type = $default_type[1];
      }

      $content_default_setting = $courses->default_activity_completion->where('type_content', $default_type)->first();
      if ($Content->type_content != 'folder' && $Content->type_content != 'label' && $Content->type_content != 'scorm') { // jika konten bukan video
        if ($content_default_setting->required_view == 1 && $content_default_setting->required_grade == 0 && $content_default_setting->required_submit == 0) {
          if (!Progress::where(['content_id' => $content_id, 'user_id' => $user->id])->first()) {
            // insert progress
            $Progress = new Progress;
            $Progress->content_id = $Content->id;
            $Progress->course_id = $courses->id;
            $Progress->user_id = $user->id;
            $Progress->status = '1';
            $Progress->save();

            //badges fix
            $badge_content_count = BadgeContentAssign::where('content_id', $content_id)->count();
            $badge_content = BadgeContentAssign::where('content_id', $content_id)->get();
            $badge_status = null;

            //checking if content for badge been created
            if($badge_content_count)
            {
              foreach($badge_content as $badge_content_value)
              {
                $badge_data = BadgeFix::where('id', $badge_content_value->badge_id)->first();
                //checking if badge published
                if($badge_data->publish == 1)
                {
                  $badge_content_check = BadgeContentAssign::where('badge_id', $badge_content_value->badge_id)->count();
                  $content_progress = Progress::where('content_id', $badge_content_value->content_id)->first();
                  //checking if content for badge only 1
                  if($badge_content_check == 1)
                  {
                    //checking if the content completed
                    if($content_progress && $content_progress->status == 1)
                    {
                      //checking if only need this content to complete and get the badge
                      if($badge_data->complete_criteria == 'any')
                      {
                        $BadgeUsersFix = new BadgeUsersFix;
                        $BadgeUsersFix->user_id = $user->id;
                        $BadgeUsersFix->badge_id = $badge_content_value->badge_id;
                        $BadgeUsersFix->save();

                        $notif = new NotificationUser;
                        $notif->user_id = $user->id;
                        $notif->description = "Congratulations, you earned ". $badge_data->name;
                        $notif->save();

                        $badge_status = true;
                      }

                      //adding condition for complete criteria 'all'
                      // else
                      // {
                      //   return 'a';
                      // }
                    }
                  }
  
                  //checking if content for badge more than 1
                  else
                  {
                    $badge_criteria_assign = BadgeCriteriaAssign::where('badge_id', $badge_content_value->badge_id)->first();
                    $badge_content_data = BadgeContentAssign::where('badge_id', $badge_content_value->badge_id)->get();
                    $badge_content_data_count = BadgeContentAssign::where('badge_id', $badge_content_value->badge_id)->count();

                    //if complete type all
                    if($badge_criteria_assign->complete_type == 'all')
                    {
                      $temp_status = 0;
                      foreach($badge_content_data as $data_content)
                      {
                        $content_check = Progress::where('content_id', $data_content->content_id)->first();
    
                        if($content_check && $content_progress->status == 1)
                        {
                          $temp_status = $temp_status + 1;
                          if($temp_status == $badge_content_data_count)
                          {
                            if($badge_data->complete_criteria == 'any')
                            {
                              $BadgeUsersFix = new BadgeUsersFix;
                              $BadgeUsersFix->user_id = $user->id;
                              $BadgeUsersFix->badge_id = $badge_content_value->badge_id;
                              $BadgeUsersFix->save();
    
                              $notif = new NotificationUser;
                              $notif->user_id = $user->id;
                              $notif->description = "Congratulations, you earned ". $badge_data->name;
                              $notif->save();
    
                              $badge_status = true;
                            }

                            //adding condition for complete criteria 'all'
                            // else
                            // {
                            //   return 'a';
                            // }
                          }
                        }
                      }
                    }

                    //if complete type any
                    else
                    {
                      foreach($badge_content_data as $data_content){
                        $content_check = Progress::where('content_id', $data_content->content_id)->first();
    
                        if($content_check && $content_progress->status == 1){

                          if($badge_data->complete_criteria == 'any')
                          {
                            $badge_users_fix_check = BadgeUsersFix::where('user_id', $user->id)->where('badge_id', $badge_content_value->badge_id)->count();
                            if($badge_users_fix_check == 0){
                              $BadgeUsersFix = new BadgeUsersFix;
                              $BadgeUsersFix->user_id = $user->id;
                              $BadgeUsersFix->badge_id = $badge_content_value->badge_id;
                              $BadgeUsersFix->save();

                              $notif = new NotificationUser;
                              $notif->user_id = $user->id;
                              $notif->description = "Congratulations, you earned ". $badge_data->name;
                              $notif->save();

                              $badge_status = true;
                            }

                            //adding condition for complete criteria 'all'
                            // else
                            // {
                            //   return 'a';
                            // }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            $course_level_up = Course::where('id', $courses->id)->value('level_up');
            //checking the level up status setting
            if($course_level_up == 1)
            {
              $rules_content = LevelingRulesContent::where('content_id', $content_id)->get();

              //checking the existing of rule of contents
              if(!empty($rules_content))
              {
                foreach($rules_content as $data_rules_content)
                {
                  $user_leveling_log = UserLevelingLog::where('user_id', $user->id)
                  ->where('leveling_rules_id', $data_rules_content->leveling_rules_id)->first();

                  //checking log
                  if(empty($user_leveling_log))
                  {
                    //checking rule type and criteria
                    if($data_rules_content->rules_criteria_id == 1)
                    {
                      $rules = LevelingRule::where('id', $data_rules_content->leveling_rules_id)->first();

                      //if complete type = any
                      if($rules->complete_type == 'any')
                      {
                        //checking point gain status active on settings
                        if($courses->points_gain_status == '1')
                        {
                          $course_level = CourseLeveling::where('course_id', $courses->id)->get();
                          $course_user = CourseUser::where('course_id', $courses->id)
                          ->where('user_id', $user->id)->first();

                          $course_user->level_point = $course_user->level_point + $rules->points_earned;

                          $current_user_level = $course_user->level;

                          foreach($course_level as $data_course_level)
                          {
                            //checking data course leveling
                            if(!empty($data_course_level))
                            {
                              //checking point to get next level
                              if($course_user->level_point >= $data_course_level->points_required)
                              {
                                $course_user->level = $data_course_level->level;
                              }
                            }     
                          }
                          
                          $course_user->save();

                          //creating status of level up or not
                          if($current_user_level != $course_user->level)
                          {
                            if($courses->level_notif_status == '1')
                            {
                              $notif = new NotificationUser;
                              $notif->user_id = $user->id;
                              $notif->description = "Congratulations, you are now Level ". $course_user->level . " in course " . $courses->title;
                              $notif->save();

                              $level_up_status = true;
                            }

                            else
                            {
                              $notif = new NotificationUser;
                              $notif->user_id = $user->id;
                              $notif->description = "Congratulations, you are now Level ". $course_user->level . " in course " . $courses->title;
                              $notif->save();

                              $level_up_status = false;
                            }
                          }
                          else
                          {
                            $level_up_status = false;
                          }

                          //create log of user action to get point
                          $user_log = new UserLevelingLog;
                          $user_log->user_id = $user->id;
                          $user_log->leveling_rules_id = $rules->id;
                          $user_log->reward = $rules->points_earned;
                          $user_log->course_id = $courses->id ;
                          $user_log->save();
                        }
                      }

                      //complete type = all
                      else
                      {
                        //checking point gain status active on settings
                        if($courses->points_gain_status == '1')
                        {
                          $leveling_rules_data = LevelingRulesContent::where('leveling_rules_id', $data_rules_content->leveling_rules_id)->get();

                          $temp_status = 0;
                          foreach($leveling_rules_data as $data_content_rules)
                          {
                            //checking rules criteria
                            if($data_content_rules->rules_criteria_id == 1)
                            {
                              $content_check = Progress::where('content_id', $data_content_rules->content_id)->first();

                              //checking content completion
                              if($content_check && $content_check->status == 1)
                              {
                                $temp_status = $temp_status + 1;
                              }
                            }
                          }

                          //checking contents completion are matches
                          if($temp_status == count($leveling_rules_data))
                          {
                            $course_level = CourseLeveling::where('course_id', $courses->id)->get();
                            $course_user = CourseUser::where('course_id', $courses->id)
                            ->where('user_id', $user->id)->first();

                            $course_user->level_point = $course_user->level_point + $rules->points_earned;

                            $current_user_level = $course_user->level;

                            foreach($course_level as $data_course_level)
                            {
                              //checking data course leveling
                              if(!empty($data_course_level))
                              {
                                //checking point to get next level
                                if($course_user->level_point >= $data_course_level->points_required)
                                {
                                  $course_user->level = $data_course_level->level;
                                }
                              }
                            }
                            
                            $course_user->save();
                            //creating status of level up or not
                            if($current_user_level != $course_user->level)
                            {
                              //checking status of notifications
                              if($courses->level_notif_status == '1')
                              {
                                $notif = new NotificationUser;
                                $notif->user_id = $user->id;
                                $notif->description = "Congratulations, you are now Level ". $course_user->level . " in course " . $courses->title;
                                $notif->save();

                                $level_up_status = true;
                              }

                              else
                              {
                                $notif = new NotificationUser;
                                $notif->user_id = $user->id;
                                $notif->description = "Congratulations, you are now Level ". $course_user->level . " in course " . $courses->title;
                                $notif->save();

                                $level_up_status = false;
                              }
                            }
                            else
                            {
                              $level_up_status = false;
                            }

                            //create log of user action to get point
                            $user_log = new UserLevelingLog;
                            $user_log->user_id = $user->id;
                            $user_log->leveling_rules_id = $rules->id;
                            $user_log->reward = $rules->points_earned;
                            $user_log->course_id = $courses->id ;
                            $user_log->save();
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            if(!isset($level_up_status))
            {
              $level_up_status = false;
            }
            
            $data = [
              'level_up_status' => $level_up_status,
              'course' => $courses,
              'sections' => $section_data,
              'content' => $Content,
              'content_video_quizes' => ContentVideoQuiz::where('content_id', $content_id)->with('content_video_quiz_answers')->get(),
            ];
            // return $data;
            return view('course.learn_content', $data);

            // insert progress
          } else {
            $Progress = Progress::where(['content_id' => $content_id, 'user_id' => $user->id])->first();
            $Progress->status = '1';
            $Progress->save();
          }
        }
      }
      // check content on progress

      $data = [
        'level_up_status' => false,
        'course' => $courses,
        'sections' => $section_data,
        'content' => $Content,
        'content_video_quizes' => ContentVideoQuiz::where('content_id', $content_id)->with('content_video_quiz_answers')->get(),
      ];
      
      return view('course.learn_content', $data);
    } else {
      return redirect('course/' . $slug);
    }
  }

  public function serverside(Request $request)
  {
    if ($request->ajax()) {
      $user = Auth::user();
      $data = Course::select('courses.id as id', 'title', 'image', 'status', 'meetings.publish as publish_meeting')
        ->where('id_author', $user->id)
        ->join('meetings', 'meetings.course_id', '=', 'courses.id')
        ->get();
      return Datatables::of($data)
        ->addColumn('action', function ($data) {
          return '
        <div class="btn-group">
            <button type="button" class="btn btn btn-default btn-raised dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Options <i class="zmdi zmdi-chevron-down right"></i>
            </button>
            <ul class="dropdown-menu">
              <form name="delete" method="POST" action="' . url('course/delete/' . $data->id) . '">
              <li>
                <a title="Manage" href="' . url('course/manage/' . $data->id) . '" class="dropdown-item">
                  <span class="glyphicon glyphicon-pencil"></span> Manage
                </a>
              </li>
              <li>
                <a target="_blank" title="Start Meeting" href="' . url('bigbluebutton/meeting/instructor/join/' . $data->id) . '" class="dropdown-item">
                  Start Meeting
                </a>
              </li>
              <li>
                <a title="Detail" href="' . url('course/show/' . $data->id) . '" class="dropdown-item">
                  Detail
                </a>
              </li>
              <li>
                <a title="Announcements" href="' . url('course/announcement/' . $data->id) . '" class="dropdown-item">
                  Announcements
                </a>
              </li>
              <li>
                <a title="Completion" href="' . url('course/completion/' . $data->id) . '" class="dropdown-item">
                  Completion
                </a>
              </li>
              <li>
                <a title="Completion" href="' . url('report/summary/' . $data->id) . '" class="dropdown-item">
                  Grade Participant
                </a>
              </li>
              ' . csrf_field() . '
              <input type="hidden" name="_method" value="DELETE">
              <li>
                <button title="Delete" class="dropdown-item" type="submit" name="submit" onclick="return confirm(' . "'" . 'Anda yakin ingin menghapus data ini?' . "'" . ');">
                  <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
              </li>
              </form>
            </ul>
          </div>
        ';
        })
        ->editColumn('status', function ($data) {
          $status_checked = $data->status == '1' ? 'checked' : '';
          return '
          <input type="checkbox" name="status' . $data->id . '" ' . $status_checked . '>

          <script type="text/javascript">
        		$("[name=status' . $data->id . ']").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
              var token = "' . csrf_token() . '";
              if(state === true){
                $.ajax({
                  url : "/course/publish",
                  type : "POST",
                  data : {
      							id : ' . $data->id . ',
      							status : 1,
      							_token: token
                  },
                  success : function(result){

                  },
                });
              }else{
                $.ajax({
                  url : "/course/publish",
                  type : "POST",
                  data : {
      							id : ' . $data->id . ',
      							status : 0,
      							_token: token
                  },
                  success : function(result){

                  },
                });
              }
            });
        	</script>
        ';
        })
        ->editColumn('publish_meeting', function ($data) {
          $publish_checked = $data->publish_meeting == '1' ? 'checked' : '';
          return '
          <input type="checkbox" name="publish' . $data->id . '" ' . $publish_checked . '>

          <script type="text/javascript">
        		$("[name=publish' . $data->id . ']").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
              var token = "' . csrf_token() . '";
              if(state === true){
                $.ajax({
                  url : "/meeting/publish",
                  type : "POST",
                  data : {
      							id : ' . $data->id . ',
      							publish : 1,
      							_token: token
                  },
                  success : function(result){

                  },
                });
              }else{
                $.ajax({
                  url : "/meeting/publish",
                  type : "POST",
                  data : {
      							id : ' . $data->id . ',
      							publish : 0,
      							_token: token
                  },
                  success : function(result){

                  },
                });
              }
            });
        	</script>
        ';
        })
        ->rawColumns(['status', 'publish_meeting', 'action'])
        ->make(true);
    } else {
      exit("Not an AJAX request");
    }
  }

  public function my()
  {
    $user = Auth::user();
    $q = \Request::get('q');
    $filter = \Request::get('filter') ? \Request::get('filter') : 'id';
    $sort = \Request::get('sort') ? \Request::get('sort') : 'desc';

    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    $courses = Course::select('courses.*', 'users.name as author')
      ->where('courses.title', 'like', '%' . $q . '%')
      ->join('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where('courses.status', '1')
      ->where('courses.archive', '0')
      ->whereNotIn('courses.id', $data_course_id)
      ->where(['courses_users.user_id' => $user->id])
      // ->whereNotIn('courses.id',function($query) {
      //    $query->select('course_id')->from('course_lives');
      // })
      ->orderBy('courses.' . $filter, $sort)
      ->groupBy('courses.id');
    $courses_count = $courses->get()->count();
    $courses_query = $courses->offset(0)
      ->limit(12)
      ->get();

    // COURSE ARCHIVED
    $Course_archived = Course::select('courses.*', 'users.slug as user_slug', 'courses.model as model', 'courses.public', 'courses.slug as slug', 'courses.id as id')
      ->join('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where(['courses_users.user_id' => $user->id])
      ->where('archive', '1')
      ->orderBy('courses.id', 'desc')
      ->get();
    // COURSE ARCHIVED

    $courses_data = [];
    foreach ($courses_query as $course) {
      $sections = Section::where('id_course', $course->id)->get();
      $num_contents = 0;
      foreach ($sections as $section) {
        $num_contents += count(Content::where('id_section', $section->id)->get());
      }

      array_push($courses_data, [
        'num_contents' => $num_contents,
        'slug' => $course->slug,
        'title' => $course->title,
        'image' => $course->image,
        'name' => $course->author,
        'id' => $course->id,
        'public' => $course->public,
        'model' => $course->model,
      ]);
    }

    $data = [
      'courses_datas' => $courses_data,
      'courses_datas_count' => $courses_count,
      'courses' => $courses,
      'course_archives' => $Course_archived,
      // 'course_privates' => $courses_privates,
      // 'courses_publicis' => $courses_public,
    ];
    return view('course.my', $data);
  }

  public function my_loadmore()
  {
    $user = Auth::user();
    $q = \Request::get('q');
    $filter = \Request::get('filter') ? \Request::get('filter') : 'id';
    $sort = \Request::get('sort') ? \Request::get('sort') : 'desc';

    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    $courses = Course::select('courses.*', 'users.name as author')
      ->join('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where('courses.status', '1')
      ->whereNotIn('courses.id', $data_course_id)
      ->where(['courses_users.user_id' => $user->id])
      ->where('title', 'like', '%' . $q . '%')
      ->whereNotIn('courses.id', function ($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->orderBy('courses.' . $filter, $sort)
      ->groupBy('courses.id')
      ->offset(\Request::get('offset'))
      ->limit(4)
      ->get();

    $data = [
      'courses' => $courses
    ];

    return view('course.mycourse_loadmore', $data);
  }

  public function dashboard_()
  {
    $user = Auth::user();

    $courses = Course::select('courses.*', 'users.name as author')
      ->join('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->where('courses.status', '1')
      ->where('courses.archive', '0')
      ->where(['courses_users.user_id' => $user->id])
      ->where(['courses_users.is_archive' => 0])
      ->with('contents')
      ->groupBy('courses.id')
      ->paginate(24);
    // dd($courses);

    $data = [
      'courses' => $courses
    ];
    return view('course.list_v2', $data);
  }

  public function dashboard()
  {
    $user = Auth::user();
    $q = \Request::get('q');
    $filter = \Request::get('filter') ? \Request::get('filter') : 'id';
    $sort = \Request::get('sort') ? \Request::get('sort') : 'desc';

    //checking if included in training provider
    $training_provider_trainees = TrainingProviderTrainee::where('user_id', $user->id)->count();

    if ($training_provider_trainees > 0) {
      // teacher courses
      $teacher_courses = Course::select('courses.id as id', 'courses.title', 'courses.image', 'courses.status', 'courses.model', 'courses.public', 'meetings.publish as publish_meeting', 'courses.slug', 'courses.password as password')
        ->where('public', '1')
        ->where('archive', '0')
        // ->where('is_approved', 1)
        // ->where('title', 'like', '%' . $q . '%')
        // ->where(function ($query) use ($user) {
        // jika user admin maka bisa menampilkan semua kelas
        // if ($user->id != 1) {
        //   $query->whereRaw("find_in_set($user->id, id_author)");
        // }
        // })
        // ->orWhere('courses.id_instructor_group', function ($query) use ($user) {
        //   $query->select('instructor_groups.id')
        //     ->whereRaw("find_in_set($user->id, user_id)")
        //     ->from('instructor_groups')
        //     ->where('courses.public', '1')
        //     ->where('courses.archive', '0')
        //     ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
        // })
        ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
        ->leftToin('meetings', 'meetings.course_id', '=', 'courses.id')
        ->orderBy('courses.' . $filter, $sort)
        ->where('courses.archive', '0')
        ->groupBy('courses.id')
        ->paginate(20, ['*'], 'teachercourses');
    } else {
      // teacher courses
      $teacher_courses = Course::select('courses.id as id', 'courses.title', 'courses.image', 'courses.status', 'courses.model', 'courses.public', 'meetings.publish as publish_meeting', 'courses.slug', 'courses.password as password')
        ->where('public', '1')
        ->where('archive', '0')
        ->where('title', 'like', '%' . $q . '%')
        ->where(function ($query) use ($user) {
          // jika user admin maka bisa menampilkan semua kelas
          if ($user->id != 1) {
            $query->whereRaw("find_in_set($user->id, id_author)");
          }
        })
        ->orWhere('courses.id_instructor_group', function ($query) use ($user) {
          $query->select('instructor_groups.id')
            ->whereRaw("find_in_set($user->id, user_id)")
            ->from('instructor_groups')
            ->where('courses.public', '1')
            ->where('courses.archive', '0')
            ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
        })
        ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
        ->join('meetings', 'meetings.course_id', '=', 'courses.id')
        ->orderBy('courses.' . $filter, $sort)
        ->where('courses.archive', '0')
        ->groupBy('courses.id')
        ->paginate(20, ['*'], 'teachercourses');
    }
    //checking if included in training provider

    // COURSE ARCHIVED TEACHER
    $teacher_course_archived = Course::select('courses.*', 'users.slug as user_slug', 'courses.model as model', 'courses.public', 'courses.slug as slug', 'courses.id as id')
      ->whereRaw("find_in_set($user->id, id_author)")
      ->where('archive', '1')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->where(['courses_users.user_id' => $user->id])
      ->where('archive', '1')
      ->orderBy('courses.id', 'desc')
      ->groupBy('courses.id')
      ->get();
    // COURSE ARCHIVED TEACHER

    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    // get all courses teacher joined courses
    $all_courses = Course::select('courses.*', 'users.name as author')
      ->where('courses.archive', '0')
      // ->where('courses.title', 'like', '%' . $q . '%')
      ->leftJoin('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->whereNotIn('courses.id', $data_course_id)
      ->where(['courses_users.user_id' => $user->id])
      // ->orWhere(function ($query) use ($user) {
      // jika user admin maka bisa menampilkan semua kelas
      // if ($user->id != 1) {
      //   $query->whereRaw("find_in_set($user->id, id_author)")->where('courses.archive', '0');
      // }
      // })
      // ->orWhere('courses.id_instructor_group', function ($query) use ($user) {
      //   $query->select('instructor_groups.id')
      //     ->whereRaw("find_in_set($user->id, user_id)")
      //     ->from('instructor_groups')
      //     ->where('courses.public', '1')
      //     ->where('courses.archive', '0')
      //     ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
      // })
      ->where('courses.archive', '0')
      ->orderBy('courses.' . $filter, $sort)
      ->groupBy('courses.id')
      ->paginate(12);
    // get all courses teacher joined courses

    // get student courses
    $student_courses = Course::select('courses.*', 'users.name as author')
      // ->where('courses.title', 'like', '%' . $q . '%')
      ->join('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->where('courses.status', '1')
      ->where('courses.archive', '0')
      ->where(['courses_users.user_id' => $user->id])
      ->where(['courses_users.is_archive' => 0])
      ->with('contents')
      ->orderBy('courses.' . $filter, $sort)
      ->groupBy('courses.id')
      ->paginate(12);
    // dd($student_courses);

    // COURSE ARCHIVED student
    $student_course_archived = Course::select('courses.*', 'users.slug as user_slug', 'courses.model as model', 'courses.public', 'courses.slug as slug', 'courses.id as id')
      ->where('archive', '1')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('courses_users', 'courses_users.course_id', '=', 'courses.id')
      ->where(['courses_users.user_id' => $user->id])
      ->orWhere(['courses_users.is_archive' => '1'])
      ->orderBy('courses.id', 'desc')
      ->groupBy('courses.id')
      ->get();
    // COURSE ARCHIVED student

    // GET PROGRAMS
    $programs = Program::join('program_users', 'program_users.program_id', '=', 'programs.id')->where('program_users.user_id', Auth::user()->id)->get();
    // GET PROGRAMS

    // dd($programs);

    $data = [
      'all_courses' => $all_courses,
      'teacher_courses' => $teacher_courses,
      'student_courses' => $student_courses,
      'teacher_courses_archives' => $teacher_course_archived,
      'student_courses_archives' => $student_course_archived,
      'programs' => $programs,
    ];

    return view('course.list', $data);
  }

  public function activity_order_generate($id)
  {
    $section = Section::where('id_course', $id)->get();
    $order = 0;
    foreach ($section as $key => $value) {
      $contents = Content::where('id_section', $value->id)->get();
      $assignments = Assignment::where('id_section', $value->id)->get();
      $quizzs = Quiz::where('id_section', $value->id)->get();
      foreach ($contents as $content) {
        $activity_order = new ActivityOrder;
        $activity_order->content_id = $content->id;
        $activity_order->order = $order;
        $activity_order->save();

        $order++;
      }

      foreach ($assignments as $assignment) {
        $activity_order = new ActivityOrder;
        $activity_order->assignment_id = $assignment->id;
        $activity_order->order = $order;
        $activity_order->save();

        $order++;
      }

      foreach ($quizzs as $quizz) {
        $activity_order = new ActivityOrder;
        $activity_order->quizz_id = $quizz->id;
        $activity_order->order = $order;
        $activity_order->save();

        $order++;
      }
      // code...
    }

    return $section;
  }

  public function show($id)
  {
    $Course = DB::table('courses')
      ->select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.id', $id)->first();

    $CourseUser = CourseUser::where('course_id', $id)->count();

    $data = [
      'courses' => $Course,
      'course_user' => $CourseUser,
    ];
    return view('course.show', $data);
  }

  public function create(Request $request)
  {
    // if(InstructorCompleteProfile() == true){ // check profile instructor
    $user = Auth::user();
    $data = [
      'action' => 'course/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title', $request->title),
      'subtitle' => old('subtitle', $request->subtitle),
      'goal' => old('goal', $request->goal),
      'description' => old('description', $request->description),
      'image' => old('image'),
      // 'introduction' => old('introduction'),
      'price' => old('price',  $request->price ? $request->price : 0),
      'model' => old('model', $request->model),
      'password' => old('password', $request->password),
      'id_category' => old('id_category', $request->id_category),
      'id_level_course' => old('id_level_course', $request->id_level_course),
      'id_author' => old('id_author'),
      'id_instructor_group' => old('id_instructor_group'),
      'categories' => DB::table('categories')->get(),
      'course_levels' => DB::table('course_levels')->get(),
      'authors' => DB::table('users')->select('users.id as id', 'users.name as name')->join('instructors', 'instructors.user_id', '=', 'users.id')->where('users.id', '!=', Auth::user()->id)->get(),
      'instructor_groups' => DB::table('instructor_groups')->whereRaw("find_in_set($user->id, user_id)")->get(),
    ];
    return view('course.form', $data);
    // }else{
    //   \Session::flash('message', 'Lengkapi profile [phone] dan atau akun bank Anda');
    //   return redirect('profile');
    // }
  }

  public function create_action(Request $request)
  {
    $rules = [
      'title' => 'required',
    ];
    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      \Session::flash('error', $validator->errors());
      return $this->create($request);
    }

    $user = Auth::user();

    //create folder by title
    $folder = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100, 999);
    Storage::disk(env('APP_DISK'))->makeDirectory(env('UPLOAD_PATH') . '/courses/' . $folder);
    //create folder by title

    // insert course
    $Course = new Course;

    // image
    if ($request->file('image')) {
      // $destinationPath = asset_path('uploads/courses/'); // upload path
      $file = $request->file('image');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title) . rand(111, 9999) . '.' . $extension; // renameing image

      // $imageSmall = Image::make($file)->resize(240, 135)->save($destinationPath. "small_".$image);
      // $imageMedium = Image::make($file)->resize(480, 270)->save($destinationPath. "medium_".$image);
      // $request->file('image')->move($destinationPath, $image); // uploading file to given path

      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/', $image);
      $Course->image = $path_file;
    } else {
      $Course->image = '';
    }
    // image

    // check add author
    if ($request->id_author) {
      // multiple author / teacher
      $author = '';
      $author = implode(',', $request->id_author) . ',' . $user->id;
      // dd($author);
      // multiple author / teacher

      // $author = $request->id_author;
    } else {
      $author = $user->id;
    }
    // check add author

    // check add group
    // if($request->id_instructor_group){
    //   // multiple author / teacher
    //   $instructor_group = '';
    //   $instructor_group = implode(',', $request->id_instructor_group);
    //   // multiple author / teacher
    // }else{
    //   $instructor_group = NULL;
    // }
    // check add group

    $Course->title = $request->title;
    $Course->subtitle = $request->subtitle;
    // $Course->goal = $request->goal;
    $Course->description = $request->description;
    $Course->folder = $folder;
    // $Course->folder_path = null;
    $Course->price = $request->price;
    $Course->model = $request->model;
    $Course->id_category = $request->id_category;
    $Course->id_level_course = $request->id_level_course;
    $Course->id_author = $author;
    // $Course->id_instructor_group = $instructor_group;
    $Course->status = $request->model == 'subscription' ? '0' : '1';
    $Course->public = $request->public;

    $training_provider_trainees = TrainingProviderTrainee::where('user_id', $user->id)->count();

    if ($training_provider_trainees > 0) {
      $Course->status = '0';
    } else {
      $Course->status = $request->status ? '1' : '0';
    }
    $Course->password = generateRandomString(8);
    $Course->enrollment_type = isset(Setting('enrollment')->value) ? Setting('enrollment')->value : 'manual,self';

    // if($request->public == '0'){
    //   $Course->password = generateRandomString(8);
    // }

    $Course->save();
    $Course_id = $Course->id;
    // insert course

    // insert user course
    $CourseUser = new CourseUser;
    $CourseUser->user_id = $user->id;
    $CourseUser->course_id = $Course_id;
    $CourseUser->save();
    // insert user course

    // insert section
    if ($request->model == 'batch') {
      for ($i = 1; $i <= $request->meeting_total; $i++) {
        $Section = new Section;
        $Section->title = 'Week ' . $i;
        $Section->id_course = $Course_id;
        $Section->save();
      }
    } else {
      $Section = new Section;
      $Section->title = 'Section 1';
      $Section->id_course = $Course_id;
      $Section->save();
    }
    // insert section

    // insert meeting
    $Meeting = new Meeting;
    $Meeting->course_id = $Course_id;
    $Meeting->name = $request->title;
    $Meeting->password = rand(1000, 9990);
    $Meeting->password_instructor = "instructor" . rand(1000, 9990);
    $Meeting->save();
    // insert meeting

    if ($request->model == 'live') {
      // inser course live
      $CourseLive = new CourseLive;
      $CourseLive->course_id = $Course_id;
      $CourseLive->participant_total = $request->participant_total;
      $CourseLive->meeting_total = $request->meeting_total;
      $CourseLive->start_date = $request->start_date;
      $CourseLive->end_date = $request->end_date;
      $CourseLive->save();
      // inser course live

      // insert meeting schedule
      for ($i = 1; $i <= $request->meeting_total; $i++) {
        $MeetingSchedule = new MeetingSchedule;
        $MeetingSchedule->course_id = $Course_id;
        $MeetingSchedule->name = 'Live ' . $i;
        $MeetingSchedule->description = 'Live ' . $i;
        $MeetingSchedule->date = $request->end_date;
        $MeetingSchedule->save();
      }
      // insert meeting schedule

    } elseif ($request->model == 'batch') {
      // inser course batch
      $CourseBatch = new CourseBatch;
      $CourseBatch->course_id = $Course_id;
      $CourseBatch->participant_total = $request->participant_total;
      $CourseBatch->meeting_total = $request->meeting_total;
      $CourseBatch->start_date = $request->start_date;
      $CourseBatch->end_date = $request->end_date;
      $CourseBatch->save();
      // inser course batch
    }

    \Session::flash('success', Lang::get('front.page_manage_courses.create_success'));

    $training_provider_trainees = TrainingProviderTrainee::where('user_id', $user->id)->count();

    if ($training_provider_trainees > 0) {
      return redirect('course/dashboard');
    } else {
      return redirect('course/preview/' . $Course_id);
    }
  }

  public function manage($id)
  {
    $Course = Course::where('id', $id)->first();

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'desc')->get();

    foreach ($sections as $section) {
      $contents = DB::table('contents')->where('id_section', $section->id)->get();
      $quizzes = DB::table('quizzes')->where('id_section', $section->id)->get();
      $assignments = DB::table('assignments')->where('id_section', $section->id)->get();
      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'sequence' => $section->sequence,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_contents' => $contents,
        'section_quizzes' => $quizzes,
        'section_assignments' => $assignments,
      ]);
    }

    $data = [
      'action' => 'course/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Course->id),
      'title' => old('title', $Course->title),
      'goal' => old('goal', $Course->goal),
      'description' => old('description', $Course->description),
      'image' => old('image', $Course->image),
      'introduction' => old('introduction', $Course->introduction),
      'price' => old('price', $Course->price),
      'model' => old('model', $Course->model),
      'id_category' => old('id_category', $Course->id_category),
      'id_level_course' => old('id_level_course', $Course->id_level_course),
      'categories' => DB::table('categories')->get(),
      'course_levels' => DB::table('course_levels')->get(),
      'sections' => $section_data,
    ];
    return view('course.manage', $data);
  }

  public function update_action(Request $request)
  {
    $Course = Course::where('id', $request->id)->first();
    $Course_setting = CourseSetting::where('course_id', $request->id)->first();
    $activity_completion = [
      $request->condition_activity_diskusi != null ? "1" : "0",
      $request->condition_activity_assigment != null ? "1" : "0",
      $request->condition_activity
    ];

    $manual_other_completion = [
      $request->condition_manual_other_completion_admin != null ? "1" : "0",
      $request->condition_manual_other_completion_teacher != null ? "1" : "0",
      $request->condition_manual_other_completion
    ];

    $Course_setting->course_completion = $request->completion_requirements;
    $Course_setting->activity_completion = implode(',', $activity_completion);
    $Course_setting->course_grade_enable = $request->condition_grade_enable != null ? '1' : '0';
    $Course_setting->course_grade = $request->condition_grade_value;
    $Course_setting->manual_self_completion = $request->manual_self_completion != null ? '1' : '0';
    $Course_setting->manual_other_completion = implode(',', $manual_other_completion);
    $Course_setting->save();

    if ($request->default_activity_completion) {
      foreach ($request->default_activity_completion as $key => $item) {
        $default_activity = DefaultActivityCompletion::find($item['id']);
        $default_activity->completion_tracking = $item['completion_tracking'];
        $default_activity->required_view = $item['required_view'];
        $default_activity->required_grade = isset($item['required_grade']) ? $item['required_grade'] : 0;
        $default_activity->required_submit = isset($item['required_submit']) ? $item['required_submit'] : 0;
        if (isset($item['end_date_default_activity']) && isset($item['end_time_default_activity'])) {
          $default_activity->expected_date = $item['end_date_default_activity'] . ' ' . $item['end_time_default_activity'];
        } else {
          $default_activity->expected_date = null;
        }
        $default_activity->save();
      }
    }

    if ($request->activity_completion) {
      foreach ($request->activity_completion as $key => $value) {
        $activity_completion = ActivityCompletion::find($key);
        $activity_completion->completion_tracking = $value['completion_tracking'];
        $activity_completion->required_view = $value['required_view'];
        $activity_completion->required_grade = isset($value['required_grade']) ? $value['required_grade'] : 0;
        $activity_completion->required_submit = isset($value['required_submit']) ? $value['required_submit'] : 0;
        if (isset($value['end_date_default_activity']) && isset($value['end_time_default_activity'])) {
          $activity_completion->expected_date = $value['end_date_default_activity'] . ' ' . $value['end_time_default_activity'];
        } else {
          $activity_completion->expected_date = null;
        }
        $activity_completion->save();
      }
    }

    //image
    if ($request->file('image')) {
      // unlink(asset_path($request->old_image));
      $destinationPath = public_path('uploads\courses\\'); // upload path
      // $destinationPath = asset_path('uploads/courses/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title) . rand(111, 9999) . '.' . $extension; // renameing image
      $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath . "small_" . $image);
      $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath . "medium_" . $image);
      $request->file('image')->move($destinationPath, $image); // uploading file to given path
      $update_image = '/uploads/courses/' . $image;
    } else {
      $update_image = $request->image;
    }
    //image

    //introduction
    // if($request->file('introduction')){
    //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
    //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
    //   Storage::disk('google')->put($Course->folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
    //   $introduction = get_asset_path($Course->folder_path, $nameIntroduction);
    //   $Course->introduction = "https://drive.google.com/file/d/".$introduction;
    // }
    //introduction

    if ($request->end_date_enable) {
      $Course->end_date = $request->end_date . ' ' . $request->end_time;
    } else {
      $Course->end_date = null;
    }

    if ($request->restrict_item) {
      foreach ($request->restrict_item as $key => $restrict) {
        $clear = [];
        $restrict_access = RestrictAccess::where($restrict['type'] . '_from_id', $restrict['id'])->get();
        $restrict_access_type = $restrict['type'] . '_from_id';
        if (isset($restrict['restrict'])) {
          foreach ($restrict['restrict'] as $key_restrict => $restrict_item) {
            $array_restrict = explode('-', $restrict_item);
            $array_restrict_type = $array_restrict[1] . '_to_id';
            $cek_restrict = $restrict_access->where($array_restrict[1] . '_to_id', $array_restrict[0])->first();
            $clear[] = $cek_restrict;
            if (!$cek_restrict) {
              $restrict_new = new RestrictAccess;
              $restrict_new->$restrict_access_type = $restrict['id'];
              $restrict_new->$array_restrict_type = $array_restrict[0];
              $restrict_new->required_status = 0;
              $restrict_new->save();
              $clear[] = $restrict_new;
            }

            // code...
          }
        } else {
          if ($restrict_access->count() > 0) {
            foreach ($restrict_access as $remove_restrict) {
              // code...
              $remove_restrict->delete();
            }
          }
        }

        $clear_data = collect($clear)->pluck('id');
        if ($clear_data->count() > 0) {
          $remove = $restrict_access->whereNotIn('id', $clear_data);
          foreach ($remove as $remove_data) {
            $remove_data->delete();
            // code...
          }
        }
        // code...
      }
    }

    $Course->start_date = $request->start_date . ' ' . $request->start_time;
    $Course->title = $request->title;
    $Course->slug = str_slug($request->title);
    $Course->goal = $request->goal;
    $Course->description = $request->description;
    $Course->image = $update_image;
    $Course->price = $request->price;
    $Course->model = $request->model;
    $Course->public = $request->public;
    $Course->level_up = $request->level_up ? '1' : '0';
    $Course->status = $request->status ? '1' : '0';
    $Course->id_category = $request->id_category;
    $Course->id_level_course = $request->id_level_course;
    $Course->save();

    $course_leveling_count = CourseLeveling::where('course_id', $request->id)->count();

    if($Course->level_up == '1' && $course_leveling_count == 0){
      $number = 10;
      
      for($i = 0; $i < $number; $i++){
        $course_leveling = new CourseLeveling;
        $course_leveling->course_id = $request->id;
        $course_leveling->level = $i + 1;
        $course_leveling->save();
      }

    }



    \Session::flash('success', Lang::get('front.page_manage_courses.update_success'));
    // return redirect('course/manage/'.$Course->id);
    return redirect()->back();
  }

  public function save_section(Request $request)
  {
    $SectionLast = Section::where('id_course', $request->id_course)->orderBy('sequence', 'desc')->first();

    $Section = new Section;
    $Section->title = $request->title;
    $Section->id_course = $request->id_course;
    $Section->sequence = ($SectionLast) ? $SectionLast->sequence + 1 : 0;
    $Section->save();
  }

  public function delete($id)
  {
    $Course = Course::where('id', $id);
    if ($Course) {
      // delete section and contents
      $Sections = Section::where('id_course', $id);
      foreach ($Sections->get() as $section) {
        $Contents = Content::where('id_section', $section->id);
        // foreach($Contents->get() as $content){
        //   // unlink(asset_path($content->name_file));
        //   Storage::disk('google')->delete($content->path_file); // delete file google drive
        // }
        $Contents->delete();
      }
      // Storage::disk('google')->deleteDirectory($Course->first()->folder_path); // delete directory course
      $Sections->delete();
      // delete section and contents

      // delete course
      // unlink(asset_path($Course->first()->image));
      $Course->delete();
      // delete course

      \Session::flash('success', Lang::get('front.page_manage_courses.delete_record_success'));
    } else {
      \Session::flash('error', 'Delete record failed');
    }
    return redirect('course/dashboard');
  }

  public function publish(Request $request)
  {
    // $Course = Course::where('id', $request->id)->first();
    // $course_price = $Course->price;

    // $Sections = Section::where('id_course', $request->id)->count();

    // $Contents = Course::where('courses.id', $request->id)
    //   ->join('sections', 'sections.id_course', '=', 'courses.id')
    //   ->join('contents', 'contents.id_section', '=', 'sections.id')
    //   ->where('contents.status', '1')
    //   ->where('contents.type_content', '!=', 'video')
    //   ->get();

    // $ContentsVideos = Course::where('courses.id', $request->id)
    //   ->join('sections', 'sections.id_course', '=', 'courses.id')
    //   ->join('contents', 'contents.id_section', '=', 'sections.id')
    //   ->where('contents.status', '1')
    //   ->where('contents.type_content', 'video')
    //   ->get();

    // $Quizzes = Course::where('courses.id', $request->id)
    //   ->join('sections', 'sections.id_course', '=', 'courses.id')
    //   ->join('quizzes', 'quizzes.id_section', '=', 'sections.id')
    //   ->where('quizzes.status', '1')
    //   ->get();

    // $all_contents = count($Contents) + count($Quizzes) + count($ContentsVideos);

    // SETTING
    // if($course_price == 0){
    //   $course_price_duration = 20;
    // }else{
    //   $course_price_duration = 30;
    // }
    // $setting_sections_min = 3;
    // $setting_total_videos = 5;
    // $setting_total_contents = 10;
    // SETTING

    // if($request->live == 'false'){
    //   if($request->status == '1'){

    //     // get contents videos duration
    //     $CheckContents = Content::join('sections', 'sections.id', '=', 'contents.id_section')
    //     ->where('sections.id_course', $Course->id)
    //     ->get();
    //     $video_duration = 0; // minutes format
    //     foreach($CheckContents as $data){
    //       $video_duration += ceil($data->video_duration / 60);
    //     }
    //     // get contents videos duration

    //     if( ($video_duration >= $course_price_duration) && ($Sections >= $setting_sections_min) && (count($ContentsVideos) >= $setting_total_videos) && ($all_contents >= $setting_total_contents) ){
    //       // update publish / unpublish
    //       $Course->status = $request->status;
    //       $Course->save();

    //       return response()->json([
    //         'status' => '200',
    //         'message' => 'success'
    //       ]);
    //       // update publish / unpublish
    //     }else{

    //       $messages = '';
    //       if($video_duration < $course_price_duration){
    //         $messages .= '<li>Total Video durasi harus lebih dari '.$course_price_duration.' menit. Total video Anda baru ' . $video_duration . ' menit </li>';
    //       }
    //       if($Sections < $setting_sections_min){
    //         $messages .= '<li>Kelas Anda harus memiliki minimal '.$setting_sections_min.' section / topik. Kelas Anda baru memiliki '.$Sections.' section / topik</li>';
    //       }
    //       if(count($ContentsVideos) < $setting_total_videos){
    //         $messages .= '<li>Total konten video harus lebih dari '.$setting_total_videos.' Anda baru membuat '.count($ContentsVideos).' video </li>';
    //       }
    //       if($all_contents < $setting_total_contents){
    //         $messages .= '<li>Total semua konten harus lebih dari '.$setting_total_contents.' Anda baru membuat '.$all_contents.' konten </li>';
    //       }

    //       return response()->json([
    //         'status' => '400',
    //         'message' => $messages,
    //       ]);
    //     }
    //   }else{
    //     // update publish / unpublish
    //     $Course->status = $request->status;
    //     $Course->save();

    //     return response()->json([
    //       'status' => '200',
    //       'message' => 'success'
    //     ]);
    //     // update publish / unpublish
    //   }
    // }else{
    //   // update publish / unpublish
    //   $Course->status = $request->status;
    //   $Course->save();

    //   return response()->json([
    //     'status' => '200',
    //     'message' => 'success'
    //   ]);
    //   // update publish / unpublish
    // }

    $Course = Course::where('id', $request->id)->first();
    $Course->status = $request->status;
    $Course->save();

    return response()->json([
      'status' => '200',
      'message' => 'success'
    ]);
  }

  public function publish_unpublish($content_id)
  {

    $content = Content::find($content_id);
    if ($content->status == "0") {
      $content->status = "1";
    } else {
      $content->status = "0";
    }
    $content->save();

    return redirect()->back();
  }

  public function publish_meeting(Request $request)
  {
    $Course = Course::where('id', $request->id)->first();
    $Course->status = $request->status;
    $Course->save();
  }

  public function rules($request)
  {
    return $validator = Validator::make($request->all(), [
      'title' => 'required',
    ]);
  }

  public function enroll(Request $request)
  {
    $user = Auth::user();

    $CourseUser = new CourseUser;
    $CourseUser->user_id = $user->id;
    $CourseUser->course_id = $request->id;
    $CourseUser->save();

    $player_id_users = PlayerIdUser::where(['user_id' => $user->id])->get();

    $headers = array(
      'Content-type: application/json',
      'Accept: application/json',
      'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'
    );

    $player_ids = [];

    foreach ($player_id_users as $player_id_user) {
      array_push($player_ids, $player_id_user->player_id);
    }

    $fields = array(
      'app_id' => '611e29ae-92d3-4571-be63-062b0f10b000',
      'include_player_ids' => $player_ids,
      'data' => array('CourseId' => $request->id),
      'contents' => array('en' => $user->name . ' Berhasil terdaftar di ' . $request->slug),
      'headings' => array('en' => 'Pendaftaran Berhasil'),
      'android_group' => 'ingenio_enroll'
    );


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);

    // Mail::to($user->email)->send(new MailCourse($CourseUser));
    if ($request->ajax()) {
      return "TRUE";
    } else {
      return redirect('course/learn/' . $request->slug);
    }
  }

  public function _checkPassword($id, Request $request)
  {
    $Course = Course::where(['id' => $id, 'password' => $request->password])->first();
    if ($Course) {
      return 'true';
    } else {
      return 'false';
    }
  }

  public function participant($course_id)
  {
    $CourseUsers = CourseUser::where('course_id', $course_id)->join('users', 'users.id', '=', 'courses_users.user_id')->get();
    $Course = Course::where('id', $course_id)->first();
    $data = [
      'courses' => $Course,
      'course_users' => $CourseUsers
    ];
    return view('course.participant', $data);
  }

  public function duplicate($id)
  {
    $user = Auth::user();
    $Course = Course::where('id', $id)->first();

    // duplicate course
    $newCourse = $Course->replicate();
    $newCourse->title = $Course->title . " - (copy)";
    $newCourse->slug = $Course->slug . "-" . rand(100, 999);
    if ($Course->password != null) {
      $newCourse->password = generateRandomString(8);
    }
    $newCourse->save();
    // duplicate course

    // insert user course
    $CourseUser = new CourseUser;
    $CourseUser->user_id = $user->id;
    $CourseUser->course_id = $newCourse->id;
    $CourseUser->save();
    // insert user course

    // duplicate section
    $Sections = Section::where('id_course', $Course->id)->get();
    foreach ($Sections as $Section) {
      $newSection = $Section->replicate();
      $newSection->id_course = $newCourse->id;
      $newSection->save();

      // duplicate content
      $Contents = Content::where('id_section', $Section->id)->get();
      foreach ($Contents as $Content) {
        $newContent = $Content->replicate();
        $newContent->id_section = $newSection->id;
        $newContent->save();
      }
      // duplicate content

      // duplicate assignment
      $Assignments = Assignment::where('id_section', $Section->id)->get();
      foreach ($Assignments as $Assignment) {
        $newAssignment = $Assignment->replicate();
        $newAssignment->id_section = $newSection->id;
        $newAssignment->save();
      }
      // duplicate assignment

      // duplicate quiz
      $Quizzes = Quiz::where('id_section', $Section->id)->get();
      foreach ($Quizzes as $Quiz) {
        $newQuiz = $Quiz->replicate();
        $newQuiz->id_section = $newSection->id;
        $newQuiz->save();

        // duplicate quiz question
        $QuizQuestions = QuizQuestion::where('quiz_id', $Quiz->id)->get();
        foreach ($QuizQuestions as $QuizQuestion) {
          $newQuizQuestions = $QuizQuestion->replicate();
          $newQuizQuestions->quiz_id = $newQuiz->id;
          $newQuizQuestions->save();

          $QuizQuestionAnswers = QuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->id)->get();
          foreach ($QuizQuestionAnswers as $QuizQuestionAnswer) {
            $newQuizQuestionAnswer = $QuizQuestionAnswer->replicate();
            $newQuizQuestionAnswer->quiz_question_id = $newQuizQuestions->id;
            $newQuizQuestionAnswer->save();
          }
        }
        // duplicate quiz question
      }
      // duplicate quiz
    }
    // duplicate section

    // duplicate meeting
    $Meetings = Meeting::where('course_id', $Course->id)->get();
    foreach ($Meetings as $Meeting) {
      $newMeeting = $Meeting->replicate();
      $newMeeting->course_id = $newCourse->id;
      $newMeeting->save();
    }
    // duplicate meeting

    \Session::flash('message', \Lang::get('front.page_manage_courses.messages.course_duplicated'));
    return redirect('course/dashboard');
  }

  public function completion($course_id)
  {
    $Course = Course::where('id', $course_id)->first();
    $CoursesUsers = CourseUser::where('course_id', $course_id)
      ->join('users', 'users.id', '=', 'courses_users.user_id')->get();

    $sections = DB::table('sections')->where('id_course', $course_id)->get();
    $num_contents = 0;
    foreach ($sections as $section) {
      $contents = DB::table('contents')
        ->where('id_section', $section->id)
        ->get();
      $num_contents += count($contents);
    }

    $completions = [];
    foreach ($CoursesUsers as $CourseUser) {
      //count percentage
      $num_progress = 0;
      $num_progress = count(Progress::where(['course_id' => $course_id, 'user_id' => $CourseUser->user_id, 'status' => '1'])->get());
      $percentage = 0;
      $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
      if($percentage == 0){
        $percentage = 0;
      }else {
        $percentage = 100 / $percentage;
      }
      //count percentage

      array_push($completions, [
        'name' => $CourseUser->name,
        'user_id' => $CourseUser->user_id,
        'percentage' => $percentage
      ]);
    }

    $data = [
      'completions' => $completions,
      'Course' =>  $Course,
    ];

    return view('course.completion', $data);
  }

  public function completionDetail($course_id, $user_id)
  {
    $Course = Course::where('id', $course_id)->first();

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'desc')->get();
    foreach ($sections as $section) {
      $contents = DB::table('contents')
        ->where('id_section', $section->id)
        ->get();

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'section_contents' => $contents,
      ]);
    }
    // $Progresses = Progress::select('contents.title as content_title', 'sections.title as section_title')
    //   ->where(['course_id' => $course_id, 'user_id' => $user_id, 'status' => '1'])
    //   ->join('contents','contents.id','=','progresses.content_id')
    //   ->join('sections','sections.id','=','contents.id')
    //   ->get();

    $data = [
      'Course' =>  $Course,
      'sections' => $section_data,
      'user_id' => $user_id,
    ];
    return view('course.completion_detail', $data);
  }

  public function grades_($course_id)
  {
    $Progresses = Progress::where('course_id', $course_id)
      ->join('contents', 'contents.id', '=', 'progresses.content_id')
      ->join('users', 'users.id', '=', 'progresses.user_id')->get();

    $data = [
      'progresses' => $Progresses,
    ];

    return view('course.grades', $data);
  }

  public function grades($course_id)
  {
    $quizzes = Quiz::select('quizzes.*')->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->where('sections.id_course', $course_id)
      ->where('quizzes.status', '1')
      ->get();

    $quiz_participants = Quiz::select('quizzes.name as name', 'users.name as user_name', 'quiz_participants.grade as grade')
      ->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->join('quiz_participants', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'quiz_participants.user_id')
      ->where('sections.id_course', $course_id)
      ->get();

    $assignments = Assignment::select('assignments.*')->join('sections', 'sections.id', '=', 'assignments.id_section')
      ->where('sections.id_course', $course_id)
      ->where('assignments.status', '1')
      ->get();

    $course_users = CourseUser::where('course_id', $course_id)
      ->join('quiz_participants', 'quiz_participants.user_id', '=', 'courses_users.user_id')
      ->join('quizzes', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'courses_users.user_id')
      ->groupBy('courses_users.user_id')
      ->get();

    $sections = DB::table('sections')->where('id_course', $course_id)->get();
    $num_contents = 0;
    foreach ($sections as $section) {
      $contents = DB::table('contents')
        ->where('id_section', $section->id)
        ->get();
      $num_contents += count($contents);
    }

    $data = [
      'quizzes' => $quizzes,
      'assignments' => $assignments,
      'quiz_participants' => $quiz_participants,
      'course_users' => $course_users,
      'num_contents' => $num_contents,
      'course' => Course::where('id', $course_id)->first(),
    ];

    return view('course.grades', $data);
  }

  public function learn_content_finish(Request $request)
  {
    $Progress = Progress::where(['content_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    $type = $request->type;
    if (!$Progress) {
      $Progress = Progress::where(['assignment_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    }

    if (!$Progress) {
      $Progress = Progress::where(['quiz_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    }

    if ($Progress) {
      $Progress->status = '1';
      $Progress->save();
    } else {
      $NewProgress = new Progress;
      $NewProgress->course_id = $request->course_id;
      if ($type == 'assignment') {
        $NewProgress->assignment_id = $request->content_id;
      } elseif ($type == 'quizz') {
        $NewProgress->quiz_id = $request->content_id;
      } else {
        $NewProgress->content_id = $request->content_id;
      }
      $NewProgress->status = '1';
      $NewProgress->user_id = Auth::user()->id;
      $NewProgress->save();
    }
  }

  public function course_finish($id)
  {
    $course_user = CourseUser::where('course_id', $id)->where('user_id', Auth::user()->id)->first();
    $course_user->progress = 1;
    $course_user->save();

    return redirect()->back();
  }
  public function learn_content_unfinish(Request $request)
  {
    $Progress = Progress::where(['content_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    if (!$Progress) {
      $Progress = Progress::where(['assignment_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    }

    if (!$Progress) {
      $Progress = Progress::where(['quiz_id' => $request->content_id, 'user_id' => Auth::user()->id])->first();
    }
    if ($Progress) {
      $Progress->status = '0';
      $Progress->save();
    }
  }

  public function rating($course_id, Request $request)
  {
    $Course = Course::where('id', $course_id)->first();

    $CheckRating = Rating::where(['course_id' => $course_id, 'user_id' => Auth::user()->id])->first();

    if (!$CheckRating) {
      $Rating = new Rating;
      $Rating->course_id = $course_id;
      $Rating->user_id = Auth::user()->id;
      $Rating->rating = $request->rating;
      $Rating->comment = $request->comment;
      $Rating->save();

      return redirect('course/learn/' . $Course->slug);
    } else {
      \Session::flash('error', 'You have given value');
      return redirect('course/learn/' . $Course->slug);
    }
  }

  public function editable(Request $request, $id)
  {
    $Course = Course::where('id', $id)->first();
    $folder = $Course->folder;
    $field = $request->get('name');
    $value = $request->get('value');

    if ($field == 'image') {
      // $destinationPath = asset_path('uploads/courses/'); // upload path
      // $extension = $request->file('value')->getClientOriginalExtension(); // getting image extension
      // $image = str_slug($Course->title).rand(111,9999).'.'.$extension; // renameing image
      // $imageSmall = Image::make($request->file('value'))->resize(240, 135)->save($destinationPath. "small_".$image);
      // $imageMedium = Image::make($request->file('value'))->resize(480, 270)->save($destinationPath. "medium_".$image);
      // $request->file('value')->move($destinationPath, $image); // uploading file to given path
      // $update_image = '/uploads/courses/'.$image;
      $file = $request->file('value');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title) . rand(111, 9999) . '.' . $extension; // renameing image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/', $image);
      $courseImage = $path_file;
      $Course->image = $courseImage;
    } else {
      $Course->$field = $value;
    }

    \Session::flash('success', Lang::get('front.page_manage_courses.update_success'));

    $Course->save();
  }

  public function broadcast_start($slug)
  {
    $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.slug', $slug)->first();

    $data = [
      'course' => $courses,
      'course_id' => $courses->id
    ];

    return view('broadcast.start', $data);
  }

  public function broadcast_streaming($id)
  {
    $CourseBroadcast = CourseBroadcast::where('id', $id)->first();
    $data = [
      'CourseBroadcast' => $CourseBroadcast,
    ];
    return view('broadcast.stream', $data);
  }

  public function library_put($section_id, Request $request)
  {
  }

  public function move_file($data_id, Request $request)
  {
    return response()->json([
      'message' => $data_id
    ], 200);
    // $UserLibrary = DB::table('userlibraries')->where(['id' => $request->id_folder_name])->first();
    // DB::table('userlibraries')->where('id', $user->id)->update(['user_folder_name' => $folder_name, 'user_folder_path' => $folder_path]);
  }



  public function library(Request $request)
  {
    $user = Auth::user();

    if ($user->user_folder_path == null || $user->user_folder_name == null) {
      $folder_name = $user->slug . '-' . date("Y-m-d") . '-' . rand(100, 999);
      Storage::disk('google')->makeDirectory($folder_name);
      Storage::disk('sftp')->makeDirectory(server_assets_path($folder_name), 0777, true, true);
      $folder_path = get_folder_path_course($folder_name);
      DB::table('users')->where('id', $user->id)->update(['user_folder_name' => $folder_name, 'user_folder_path' => $folder_path]);
    }

    $parent = '.';
    if (!empty($request->get('parents'))) {
      $parent = $request->get('parents');
    }
    $contents = [];
    $file_icons = [];
    //$file_icon = 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/folder-icon.png';

    $courses = Course::where(['id_author' => $user->id])->get();
    if (!empty($courses)) {
      foreach ($courses as $course) {
        $sections = Section::where(['id_course' => $course->id])->get();
        if (!empty($sections)) {
          foreach ($sections as $section) {
            $contentes = Content::where(['id_section' => $section->id])->get();
            foreach ($contentes as $content) {
              if ($content == null || $content->name_file == null) {
                continue;
              }
              $userLib = UserLibrary::where(['name_file' => $content->name_file])->first();
              if (empty($userLib)) {
                $UserLibrary = new UserLibrary;
                $UserLibrary->user_id = $user->id;
                $UserLibrary->title = $content->title;
                $UserLibrary->description = $content->description;
                $UserLibrary->type_content = $content->type_content;
                $UserLibrary->name_file = $content->name_file;
                $UserLibrary->path_file = $content->path_file;
                $UserLibrary->full_path_file = $content->full_path_file;
                $UserLibrary->full_path_file_resize = $content->full_path_file_resize;
                $UserLibrary->full_path_file_resize_720 = $content->full_path_file_resize_720;
                $UserLibrary->full_path_original = $content->full_path_original;
                $UserLibrary->full_path_original_resize = $content->full_path_original_resize;
                $UserLibrary->full_path_original_resize_720 = $content->full_path_original_resize_720;
                $UserLibrary->file_status = $content->file_status;
                $UserLibrary->file_size   = $content->file_size;
                $UserLibrary->video_duration = $content->video_duration;
                $UserLibrary->status = $content->status;
                $UserLibrary->is_directory = 0;
                $UserLibrary->save();
              }
            }
          }
        }
      }
    }

    $UserLibraryAddeds = UserLibrary::where(['user_id' => $user->id, 'parent' => $parent])->get();
    foreach ($UserLibraryAddeds as $UserLibraryAdded) {
      array_push($contents, [
        'id' => $UserLibraryAdded->id,
        'title' => $UserLibraryAdded->title,
        'name_file' => $UserLibraryAdded->name_file,
        'path_file' => $UserLibraryAdded->path_file,
        'type_content' => $UserLibraryAdded->type_content,
      ]);
    }
    $hasParents = [];
    array_push($hasParents, [
      'parent' => $parent == '.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $parent])->first()->title,
      'link_parent' => $parent == '.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $parent])->first()->name_file
    ]);
    $hasParent = $parent;
    while ($hasParent != '.') {
      $hasParent = DB::table('userlibraries')->select('parent')->where(['name_file' => $hasParent])->first()->parent;
      array_push($hasParents, [
        'parent' => $hasParent == '.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $hasParent])->first()->title,
        'link_parent' => $hasParent == '.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $hasParent])->first()->name_file
      ]);
    }

    $data = [
      'id_folder_name' => old('id_folder_name'),
      'data_id' => 1,
      'resources' => $contents,
      'parents' => $hasParents,
      'user_id' => $user->id,
    ];
    // return response()->json([
    //   'resources' => $data,
    // ]);
    return view('course.library', $data);
  }

  public function library_create_folder(Request $request)
  {

    $user = Auth::user();

    $UserLibrary = new UserLibrary;
    $UserLibrary->user_id = $user->id;
    $UserLibrary->title = $request->title;
    $UserLibrary->name_file = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100, 999);
    $UserLibrary->parent = $request->parent;
    $UserLibrary->is_directory = '1';
    $UserLibrary->save();

    if ($request->parent == '.') {
      return redirect('instructor/library');
    }
    return redirect('instructor/library?parents=' . $request->parent);
  }

  public function library_upload_file($user_id, Request $request)
  {
    return Plupload::receive('file', function ($file) use ($user_id) {
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $size_file = $file->getSize(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()) . '-' . rand(111, 9999) . '.' . $extension; // renameing image

      $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt'];
      if (in_array(strtolower($extension), $video_extensions)) {

        $folder = get_folderName_user($user_id); // get folder course by user id
        Storage::disk('sftp')->put(server_assets_path($folder) . '/' . $name_file, fopen($file, 'r+')); //upload google drive
        $video_duration = 0;

        $ffprobe = FFMpeg\FFProbe::create();
        $video_duration = $ffprobe
          ->format($file) // extracts file informations
          ->get('duration');

        return response()->json([
          'original_file_name' => $file->getClientOriginalName(),
          'path' => '',
          'name' => $name_file,
          'size' => $size_file,
          'video_duration' => $video_duration,
          'full_path_original' => '/uploads/videos/' . $folder . '/' . $name_file,
          'resize_path' => '',
          'type_content' => 'video'
        ]);
      } else {
        $folder = get_user_folder_path($user_id); // get folder course by section id
        Storage::disk('google')->put($folder . '/' . $name_file, fopen($file, 'r+')); //upload google drive
        $path_file = get_asset_path($folder, $name_file);

        return response()->json([
          'original_file_name' => $file->getClientOriginalName(),
          'path' => $path_file,
          'name' => $name_file,
          'type_content' => 'file'
        ]);
      }
    });
  }

  public function add_to_section($id)
  {
  }

  public function delete_lib($id)
  {
    $UserLibrary = DB::table('userlibraries')->where(['id' => $id])->first();
    if (!empty($UserLibrary)) {
      DB::table('contents')->where(['name_file' => $UserLibrary->name_file])->delete();
      if ($UserLibrary->path_file != null) {
        Storage::disk('google')->delete($UserLibrary->path_file);
      }
      DB::table('userlibraries')->where(['id' => $id])->delete();
      \Session::flash('success', Lang::get('front.page_manage_courses.delete_file_success'));
    } else {
      \Session::flash('failed', Lang::get('front.page_manage_courses.delete_file_failed'));
    }
    return back();
  }

  public function store_draft(Request $request)
  {
    // $field = $request->field;
    // $value = $request->value;

    $file = false;
    $UserLibrary = new UserLibrary;
    if ($request->file) {
      $file = true;
      if ($request->type_content == 'video') {
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->title = $request->original_file_name;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->full_path_file = $request->path_file;
        $UserLibrary->file_size   = $request->file_size;
        $UserLibrary->video_duration = $request->video_duration;
        $UserLibrary->full_path_original = $request->full_path_original;
        $UserLibrary->type_content = $request->type_content;
        $UserLibrary->is_directory = 0;
        $UserLibrary->parent = $request->parent;
        $UserLibrary->save();
      } elseif ($request->type_content == 'file') {
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->title = $request->original_file_name;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->type_content = $request->type_content;
        $UserLibrary->full_path_file = 'https://drive.google.com/file/d/' . $request->path_file;
        $UserLibrary->parent = $request->parent;
        $UserLibrary->is_directory = 0;
        $UserLibrary->save();
      }
    }


    // $Section = Section::where('id', $request->section_id)->first();
    // $Content = Content::where('id', $request->id)->first(); // checking record content
    // if($Content){ // jika konten telah ada maka hanya melakaukan update konten
    //   // $folder = get_folder_course($Content->id_section); // get folder id
    //   $Content->type_content = $request->type_content;
    //   if($request->file){ // jika ada inputan file
    //     $Content->name_file = $request->name_file;
    //     $Content->path_file = $request->path_file;
    //     if($Content->type_content == 'video'){ // jika inputan file berupa video
    //       $Content->file_size = $request->file_size;
    //       $Content->video_duration = $request->video_duration;
    //       $Content->full_path_original = $request->full_path_original;
    //       $Content->save();
    //       // exec resize video
    //       file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
    //       // exec resize video

    //     }elseif($Content->type_content == 'file'){
    //       $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
    //       $Content->save();
    //     }
    //   }else{
    //     $Content->$field = $value;
    //     $Content->save();
    //   }

    // }else{ // jika belum ada konten maka buat konten baru
    //   // $folder = get_folder_course($request->section_id); // get folder id
    //   $Content = new Content;
    //   $Content->type_content = $request->type_content;
    //   $Content->id_section = $request->section_id;
    //   if($request->file){  // jika ada inputan file
    //     $Content->name_file = $request->name_file;
    //     $Content->path_file = $request->path_file;
    //     if($Content->type_content == 'video'){ // jika inputan file berupa video
    //       $Content->file_size = $request->file_size;
    //       $Content->video_duration = $request->video_duration;
    //       $Content->full_path_original = $request->full_path_original;
    //       $Content->save();

    //       // exec resize video
    //       file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
    //       // exec resize video

    //     }elseif($Content->type_content == 'file'){
    //       $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
    //       $Content->save();
    //     }
    //   }else{
    //     $Content->$field = $value;
    //     $Content->save();
    //   }
    // }

    return response()->json([
      'message' => 'success',
      'file' => $file
    ]);
  }

  public function library_list($folder_name)
  {
    $contents = [];
    $course = DB::table('courses')->where(['folder' => $folder_name])->first();
    if (!empty($course)) {
      $sections = Section::where(['id_course' => $course->id])->get();
      if (!empty($sections)) {
        foreach ($sections as $section) {
          $content = Content::where(['id_section' => $section->id])->first();
          if ($content == null) {
            continue;
          }
          $UserLibrary = new UserLibrary;


          array_push($contents, $content);
        }
      }
    }
    $data = [
      'courses' => $contents
    ];
    return view('course.library', $data);
  }

  public function joinPrivateCourse(Request $request)
  {
    $Course = Course::where(['password' => $request->password])->first();
    if ($Course) {
      if ($Course->archive == '0') {
        $CourseUser = CourseUser::where('course_id', $Course->id)->get()->count();
        if ($CourseUser < 100000) { // unilimited
          return response()->json([
            'code' => 200,
            'message' => 'Berhasil. selamat menikmati fitur belajar yang tersedia di ingenio',
            'data' => $Course,
          ]);
        } else {
          return response()->json([
            'code' => 400,
            'message' => 'Kelas sudah penuh',
          ]);
        }
      } else {
        return response()->json([
          'code' => 400,
          'message' => 'Kelas sudah tidak aktif',
        ]);
      }
    } else {
      return response()->json([
        'code' => 400,
        'message' => 'Password atau kode yang Anda masukan salah',
      ]);
    }
  }

  public function archive($id)
  {
    $Course = Course::find($id);
    $Course->archive = '1';
    $Course->save();

    return redirect()->back()->with('message', 'Kelas sudah di arsipkan');
  }

  public function restore($id)
  {
    $Course = Course::find($id);
    $Course->archive = '0';
    $Course->save();

    return redirect()->back()->with('message', 'Kelas sudah di pulihkan');
  }

  public function user_archive($id)
  {
    $CourseUser = CourseUser::where(['course_id' => $id, 'user_id' => \Auth::user()->id])->first();
    $CourseUser->is_archive = '1';
    $CourseUser->save();

    return redirect()->back()->with('message', 'Kelas sudah di arsipkan');
  }

  public function user_restore($id)
  {
    $CourseUser = CourseUser::where(['course_id' => $id, 'user_id' => \Auth::user()->id])->first();
    $CourseUser->is_archive = '0';
    $CourseUser->save();

    return redirect()->back()->with('message', 'Kelas sudah di pulihkan');
  }

  public function archived()
  {
    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where('archive', '1')
      ->whereNotIn('courses.id', $data_course_id)
      ->whereNotIn('courses.id', function ($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->orderBy('courses.id', 'desc')
      ->paginate(6);

    $data = [
      'courses' => $courses,
    ];
    return view('course.archived', $data);
  }

  public function update_authors(Request $request)
  {
    if ($request->authors) {
      $Course = Course::find($request->course_id);
      $Course->id_author = $Course->id_author . ',' . implode(',', $request->authors);
      $Course->save();
    }
  }

  public function update_instructor_groups(Request $request)
  {
    if ($request->id_instructor_group) {
      $Course = Course::find($request->course_id);
      $Course->id_instructor_group = $Course->id_instructor_group != NULL ? $Course->id_instructor_group . ',' . implode(',', $request->id_instructor_group) : implode(',', $request->id_instructor_group);
      $Course->save();
    }
  }

  public function grades_user($course_id, $user_id)
  {
    $quizzes = Quiz::select('quizzes.*')->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->where('sections.id_course', $course_id)
      ->where('quizzes.status', '1')
      ->get();

    $quiz_participants = Quiz::select('quizzes.name as name', 'users.name as user_name', 'quiz_participants.grade as grade')
      ->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->join('quiz_participants', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'quiz_participants.user_id')
      ->where('sections.id_course', $course_id)
      ->get();

    $assignments = Assignment::select('assignments.*')->join('sections', 'sections.id', '=', 'assignments.id_section')
      ->where('sections.id_course', $course_id)
      ->where('assignments.status', '1')
      ->get();

    $course_users = CourseUser::where('course_id', $course_id)
      ->join('quiz_participants', 'quiz_participants.user_id', '=', 'courses_users.user_id')
      ->join('quizzes', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'courses_users.user_id')
      ->where('courses_users.user_id', $user_id)
      ->first();

    $sections = DB::table('sections')->where('id_course', $course_id)->get();
    $num_contents = 0;
    foreach ($sections as $section) {
      $contents = DB::table('contents')
        ->where('id_section', $section->id)
        ->get();
      $num_contents += count($contents);
    }

    $data = [
      'quizzes' => $quizzes,
      'assignments' => $assignments,
      'quiz_participants' => $quiz_participants,
      'course_users' => $course_users,
      'num_contents' => $num_contents,
    ];

    return view('course.grades_user', $data);
  }

  public function grades_quiz($course_id, $quiz_id)
  {
    $quiz_questions = QuizQuestion::where('quiz_id', $quiz_id)
      ->get();

    $course_users = CourseUser::where('course_id', $course_id)
      ->join('quiz_participants', 'quiz_participants.user_id', '=', 'courses_users.user_id')
      ->join('quizzes', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'courses_users.user_id')
      ->groupBy('courses_users.user_id')
      ->get();

    $data = [
      'quiz_questions' => $quiz_questions,
      'course_users' => $course_users,
    ];

    // dd($quizzes);
    return view('course.grades_quiz', $data);
  }

  public function grades_assignment($course_id, $assignment_id)
  {
    $AssignmentsAnswers = AssignmentAnswer::select('assignments_answers.id as id', 'assignments_answers.answer', 'assignments_answers.assignment_id as assignment_id', 'users.name', 'assignments.type', 'assignments_grades.grade')
      ->where('assignments_answers.assignment_id', $assignment_id)
      ->join('users', 'users.id', '=', 'assignments_answers.user_id')
      ->join('assignments', 'assignments.id', '=', 'assignments_answers.assignment_id')
      ->leftJoin('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id');
    $data = [
      'assignments_answers' => $AssignmentsAnswers->get(),
    ];
    return view('course.grades_assignment', $data);
  }

  public function atendee($course_id)
  {
    $Course = Course::where('id', $course_id)->first();
    $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
      ->join('users', 'users.id', '=', 'courses_users.user_id')->get();

    $sections = DB::table('sections')->where('id_course', $course_id)->get();
    $num_contents = 0;
    foreach ($sections as $section) {
      $contents = DB::table('contents')
        ->where('id_section', $section->id)
        ->get();
      $num_contents += count($contents);
    }

    $completions = [];
    foreach ($CoursesUsers as $CourseUser) {
      //count percentage
      $num_progress = 0;
      $num_progress = count(Progress::where(['course_id' => $course_id, 'user_id' => $CourseUser->user_id, 'status' => '1'])->get());
      $percentage = 0;
      $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
      if($percentage == 0){
        $percentage = 0;
      }else {
        $percentage = 100 / $percentage;
      }
      //count percentage

      $section_data = [];
      $sectionscontents = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'desc')->get();
      foreach ($sectionscontents as $section) {
        $contents = DB::table('contents')
          ->where('id_section', $section->id)
          ->get();

        array_push($section_data, [
          'id' => $section->id,
          'title' => $section->title,
          'section_contents' => $contents,
        ]);
      }

      array_push($completions, [
        'name' => $CourseUser->name,
        'user_id' => $CourseUser->user_id,
        'percentage' => $percentage,
        'section_data' => $section_data
      ]);
    }

    $CourseStudentGroupUsers = CourseStudentGroupUser::where('course_id', $course_id)->get();

    $course_users = [];
    foreach ($CourseStudentGroupUsers as $value) {
      array_push($course_users, $value->user_id);
    }

    $course_student_group_users_by_course = CourseUser::with('student')->where('course_id', $course_id)->whereNotIn('user_id', $course_users)->get();

    // dd($course_student_group_users_by_course);

    $data = [
      'completions' => $completions,
      'course_student_groups' => CourseStudentGroup::where('course_id', $course_id)->with('course_student_group_user')->get(),
      'course_student_group_users_by_course' => $course_student_group_users_by_course,
      'Course' =>  $Course,
    ];

    // dd($data);

    return view('course.atendee', $data);
  }

  public function setCeritifcated(Request $request)
  {
    $Course = Course::where('id', $request->id)->first();
    $Course->get_ceritificate = $request->get_ceritificate;
    $Course->save();

    return redirect()->back()->with('success', Lang::get('front.page_manage_courses.setting_ceritificate_success'));
  }

  public function getLadder($course_id)
  {
    $course_user = CourseUser::join('users', 'courses_users.user_id', '=', 'users.id')
      ->where('course_id', $course_id)
      ->select('users.name as user_name', 'courses_users.level_point', 'courses_users.course_id', 'courses_users.level', 'users.id as user_id')
      ->orderBy('courses_users.level_point', 'desc')
      ->get();

    $course_leveling = CourseLeveling::where('course_id', $course_id)->get();

    $course_levelup_img = Course::where('id', $course_id)->value('levelup_img');

    $data = [
      'course_user' => $course_user,
      'course_leveling' => $course_leveling,
      'course_levelup_img' => $course_levelup_img
    ];
    
    return view('course.level_ladder', $data);
  }
}
