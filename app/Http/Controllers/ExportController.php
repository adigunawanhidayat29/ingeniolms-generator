<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\CourseUser;
use App\Progress;
use App\Quiz;
use App\Assignment;
use DB;
use Excel;

class ExportController extends Controller
{
    public function completion($course_id)
    {

        $type = 'xls';
        $Course = Course::where('id', $course_id)->first();
        $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
          ->join('users','users.id','=','courses_users.user_id')->get();

          $sections = DB::table('sections')->where('id_course', $course_id)->get();
          $num_contents = 0;
          $data_contents = [];
          foreach($sections as $section){
            $contents = DB::table('contents')
              ->where('id_section', $section->id)
              ->get();
              foreach($contents as $content) {
                array_push($data_contents, [
                  'id' => $content->id,
                  'title' => $content->title
                ]);
              }
            $num_contents += count($contents);
          }

          $completions = [];
          foreach($CoursesUsers as $CourseUser){
            //count percentage
            $num_progress = 0;
            $num_progress = count(Progress::where(['course_id'=>$course_id, 'user_id' => $CourseUser->user_id, 'status' => '1'])->get());
            $percentage = 0;
            $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
            $percentage = 100 / $percentage;
            //count percentage

            array_push($completions, [
              'name' => $CourseUser->name,
              'user_id' => $CourseUser->user_id,
              'percentage' => $percentage
            ]);
          }

          // dd($data_contents);
          $data = [
            'completions' => $completions,
            'Course' =>  $Course,
            'contents' => $data_contents,
          ];
      
        return Excel::create('completion-' . $Course->title , function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('No');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Nama');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Persentase'); });
                $current_column = 'C';
                foreach($data['contents'] as $index => $value) {
                    $sheet->cell($current_column++ . '1', function($cell) use ($value) {
                      $cell->setValue($value['title']); 
                    });
                }

                if (!empty($data['completions'])) {
                    foreach ($data['completions'] as $key => $value) {
                        $i= $key+2;
                        $sheet->cell('A'.$i, $key+1); 
                        $sheet->cell('B'.$i, $value['name']); 
                        $sheet->cell('C'.$i, round($value['percentage'], 2) . '%'); 
                        $current_column = 'C';
                        foreach($data['contents'] as $index => $content) {
                            $content_progress = DB::table('progresses')->where(['content_id'=>$content['id'], 'user_id' => $value['user_id'], 'status' => '1'])->first();
                            $sheet->cell($current_column++ . $i, function($cell) use ($content_progress) {
                              $cell->setValue($content_progress ? 'Sudah' : 'Belum'); 
                            });
                        }
                    }
                }
            });
        })->download($type);
    }

    public function grades($course_id){
      $Course = Course::where('id', $course_id)->first();

      $quizzes = Quiz::select('quizzes.*')->join('sections', 'sections.id', '=', 'quizzes.id_section')
        ->where('sections.id_course', $course_id)
        ->where('quizzes.status', '1')
        ->get();

      $quiz_participants = Quiz::select('quizzes.name as name', 'users.name as user_name', 'quiz_participants.grade as grade')
      ->join('sections', 'sections.id', '=', 'quizzes.id_section')
      ->join('quiz_participants', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'quiz_participants.user_id')
      ->where('sections.id_course', $course_id)
      ->get();

      $assignments = Assignment::select('assignments.*')->join('sections', 'sections.id', '=', 'assignments.id_section')
        ->where('sections.id_course', $course_id)
        ->where('assignments.status', '1')
        ->get();

      $course_users = CourseUser::where('course_id', $course_id)
      ->join('quiz_participants', 'quiz_participants.user_id', '=', 'courses_users.user_id')
      ->join('quizzes', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->join('users', 'users.id', '=', 'courses_users.user_id')
      ->groupBy('courses_users.user_id')
      ->get();

      $sections = DB::table('sections')->where('id_course', $course_id)->get();
      $num_contents = 0;
      foreach($sections as $section){
        $contents = DB::table('contents')
          ->where('id_section', $section->id)
          ->get();
        $num_contents += count($contents);
      }

      $data = [
        'Course' => $Course,
        'quizzes' => $quizzes,
        'assignments' => $assignments,
        'quiz_participants' => $quiz_participants,
        'course_users' => $course_users,
        'num_contents' => $num_contents,
      ];

      return view('course.exports.grade', $data);
  }

}
