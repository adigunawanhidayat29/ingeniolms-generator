<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AffiliateUser;
use App\AffiliateTransaction;
use App\AffiliateBalance;
use App\AffiliateWithdrawals;
use App\Promotion;
use App\Course;
use App\Program;
use Auth;
use Session;
use DB;
use Validator;

class AffiliateController extends Controller
{

  public function index(){
    if(Auth::check()){
      if(is_affiliate(Auth::user()->id)){
        if(is_affiliate(Auth::user()->id)->status == '1'){

          $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();
          $AffiliateBalances = AffiliateBalance::where('affiliate_id', $affiliate_user->id)
            ->orderBy('id', 'asc');

          $debit = 0;
          $credit = 0;
          foreach($AffiliateBalances->get() as $AffiliateBalance){
            $debit += $AffiliateBalance->debit ;
            $credit += $AffiliateBalance->credit ;
          }

          $data = [
            'affiliate_user' => $affiliate_user,
            'affiliate_setting' => DB::table('affiliate_setting')->where('status', '1')->orderBy('id', 'desc')->first(),
            'program' => Program::where('status', '1')->first(),
            'balance' => $debit - $credit,
          ];
          return view('affiliate.index', $data);
        }else{
          Session::flash('message', 'Affiliate Anda dalam proses verifikasi');
          return view('affiliate.register');
        }
      }else{
        return view('affiliate.register');
      }
    }else{
      return view('affiliate.register');
    }
  }

  public function join(Request $request){
    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
      'phone' => 'required',
    ]);

    $User = User::create([
      'name' => $request->name,
      'email' => $request->email,
      'password' => bcrypt($request->password),
      'slug' => str_slug($request->slug),
      'phone' => $request->phone,
      'is_active' => '1',
    ]);

    $code = rand(123456,999999);

    $AffiliateUser = new AffiliateUser;
    $AffiliateUser->user_id = $User['id'];
    $AffiliateUser->status = '1';
    $AffiliateUser->url = '?aff='.rand(123456,999999);
    $AffiliateUser->code = $code;
    $AffiliateUser->bank = $request->bank;
    $AffiliateUser->bank_account_number = $request->bank_account_number;
    $AffiliateUser->bank_account_name = $request->bank_account_name;
    $AffiliateUser->save();

    $AffiliateSetting = DB::table('affiliate_setting')->where('status', '1')->first();

    $Promotion = new Promotion;
    $Promotion->name = "Affiliate " . $request->name;
    $Promotion->description = "Discount Affiliate " . $request->name;
    $Promotion->discount_code = $code;
    $Promotion->discount = $AffiliateSetting->discount;
    $Promotion->start_date = $AffiliateSetting->start_date;
    $Promotion->end_date = $AffiliateSetting->end_date;
    $Promotion->status = '1';
    $Promotion->save();

    $UserGroup = DB::table('users_groups')->insert([
      'user_id' => $User['id'],
      'level_id' => 4,
    ]);

    Session::flash('message', 'Daftar berhasil, Silakan login');
    return redirect('affiliate');

  }

  public function join_user(Request $request){
    if(Auth::check()){
      if(!is_affiliate(Auth::user()->id)){

        $code = rand(123456,999999);

        $AffiliateUser = new AffiliateUser;
        $AffiliateUser->user_id = Auth::user()->id;
        $AffiliateUser->status = '1';
        $AffiliateUser->url = '?aff='.rand(123456,999999);
        $AffiliateUser->code = $code;
        $AffiliateUser->save();

        $UserGroup = DB::table('users_groups')->insert([
          'user_id' => Auth::user()->id,
          'level_id' => 4,
        ]);

        $AffiliateSetting = DB::table('affiliate_setting')->where('status', '1')->first();

        $Promotion = new Promotion;
        $Promotion->name = "Affiliate " . Auth::user()->name;
        $Promotion->description = "Discount Affiliate " . Auth::user()->name;
        $Promotion->discount_code = $code;
        $Promotion->discount = $AffiliateSetting->discount;
        $Promotion->start_date = $AffiliateSetting->start_date;
        $Promotion->end_date = $AffiliateSetting->end_date;
        $Promotion->status = '1';
        $Promotion->save();
      }

      if(is_affiliate(Auth::user()->id)->status == '0'){
        Session::flash('message', 'Affiliate Anda dalam proses verifikasi');
      }
      return redirect('affiliate');
    }else{
      return redirect('affiliate');
    }
  }

  public function login(Request $request){
    $Login = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
    if($Login){
      return redirect('affiliate');
    }else{
      Session::flash('message', 'Login Gagal, Email atau Password salah');
      return redirect('affiliate');
    }
  }

  public function profile_edit(){
    $data = [
      'affiliate_user' => AffiliateUser::where('user_id', Auth::user()->id)->first(),
    ];
    return view('affiliate.profile_edit', $data);
  }

  public function profile_update(Request $request){
    $AffiliateUser = AffiliateUser::where('user_id', Auth::user()->id)->first();
    if($request->file('banner')){
      $destinationPath = asset_path('uploads/affiliate/'); // upload path
      $extension = $request->file('banner')->getClientOriginalExtension(); // getting image extension
      $banner = rand(1111000000,9999999999).'.'.$extension; // renameing image
      $request->file('banner')->move($destinationPath, $banner);
      $AffiliateUser->banner = '/uploads/affiliate/'.$banner;
    }
    $AffiliateUser->bank = $request->bank;
    $AffiliateUser->bank_account_name = $request->bank_account_name;
    $AffiliateUser->bank_account_number = $request->bank_account_number;
    $AffiliateUser->save();

    Session::flash('message', 'Profile berhasil disimpan');
    return redirect('affiliate');
  }

  public function edit_code(Request $request){
    $AffiliateUser = AffiliateUser::where('user_id', Auth::user()->id)->first();

    $Promotion = Promotion::where('discount_code', $AffiliateUser->code)->first();
    $Promotion->discount_code = $request->code;
    $Promotion->save();

    $AffiliateUser->code = $request->code;
    $AffiliateUser->save();


    Session::flash('message', 'Code Affiliate berhasil diubah');
    return redirect('affiliate');
  }

  public function transaction(){
    $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();
    $AffiliateTransactions = AffiliateTransaction::where('affiliate_transactions.affiliate_id', $affiliate_user->id)
      ->join('transactions','transactions.invoice','=','affiliate_transactions.invoice')
      ->join('users','users.id','=','affiliate_transactions.user_id')
      ->orderBy('affiliate_transactions.id', 'desc')
      ->paginate(10);
    $data = [
      'affiliate_transactions' => $AffiliateTransactions,
    ];
    return view('affiliate.transaction', $data);
  }

  public function balance(){
    $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();
    $AffiliateBalances = AffiliateBalance::where('affiliate_id', $affiliate_user->id)
      ->orderBy('id', 'asc');

    $debit = 0;
    $credit = 0;
    foreach($AffiliateBalances->get() as $AffiliateBalance){
      $debit += $AffiliateBalance->debit ;
      $credit += $AffiliateBalance->credit ;
    }

    $data = [
      'affiliate_balances' => $AffiliateBalances->paginate(10),
      'balance' => $debit - $credit,
    ];
    return view('affiliate.balance', $data);
  }

  public function withdrawal(){
    $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();

    $AffiliateBalances = AffiliateBalance::where('affiliate_id', $affiliate_user->id)
      ->orderBy('id', 'asc');

    $debit = 0;
    $credit = 0;
    foreach($AffiliateBalances->get() as $AffiliateBalance){
      $debit += $AffiliateBalance->debit ;
      $credit += $AffiliateBalance->credit ;
    }

    $data = [
      'affiliate_withdrawals' => AffiliateWithdrawals::where('affiliate_id', $affiliate_user->id)->paginate(10),
      'balance' => $debit - $credit,
    ];
    return view('affiliate.withdrawal', $data);
  }

  public function withdrawal_request(Request $request){

    $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();
    $AffiliateBalances = AffiliateBalance::where('affiliate_id', $affiliate_user->id)
      ->orderBy('id', 'asc');

    $debit = 0;
    $credit = 0;
    foreach($AffiliateBalances->get() as $AffiliateBalance){
      $debit += $AffiliateBalance->debit ;
      $credit += $AffiliateBalance->credit ;
    }
    $balance = $debit - $credit;

    //validation
    $validatedData = $request->validate([
        'amount' => 'required|numeric|min:20000|max:'.$balance,
    ]);
    //validation

    $AffiliateWithdrawals  = new AffiliateWithdrawals;
    $AffiliateWithdrawals->affiliate_id = $affiliate_user->id;
    $AffiliateWithdrawals->request = $request->amount;
    $AffiliateWithdrawals->save();

    Session::flash('message', 'Permintaan Anda sedang di proses');
    return redirect('affiliate/withdrawal');
  }

  public function links(){
    $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')->where('status','1')->join('users','users.id','=','courses.id_author')->orderBy('courses.id', 'desc')->paginate(10);
    $affiliate_user = AffiliateUser::where('user_id', Auth::user()->id)->first();
    $data = [
      'courses' => $courses,
      'affiliate_user' => $affiliate_user
    ];
    return view('affiliate.links', $data);
  }

}
