<?php

namespace App\Http\Controllers\TrainingProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainingProviderTrainee;
use App\TrainingProviderTrainer;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        
        $training_provider_id = TrainingProviderTrainer::where('user_id', $user_id)->value('training_provider_id');

        $users = TrainingProviderTrainee::join('users', 'training_provider_trainees.user_id', '=', 'users.id')
        ->select('users.name', 'users.email', 'users.username')
        ->where('training_provider_trainees.training_provider_id', $training_provider_id)
        ->orderBy('users.name')
        ->get();

        $data = [
            'users' => $users
        ];
        // return $data;
        return view('training_provider.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
