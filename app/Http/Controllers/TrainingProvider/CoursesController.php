<?php

namespace App\Http\Controllers\TrainingProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;
use App\TrainingProviderTrainer;
use App\TrainingProviderTrainee;
use Auth;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        
        $training_provider_id = TrainingProviderTrainer::where('user_id', $user_id)->value('training_provider_id');

        $training_provider_id_user = TrainingProviderTrainee::where('training_provider_id', $training_provider_id)->get();

        // return $training_provider_id_user;
        $data = [
            'id_users' => $training_provider_id_user
        ];

        return view('training_provider.courses.index', $data);
    }

    public function index_approved()
    {
        $user_id = Auth::user()->id;
        
        $training_provider_id = TrainingProviderTrainer::where('user_id', $user_id)->value('training_provider_id');

        $training_provider_id_user = TrainingProviderTrainee::where('training_provider_id', $training_provider_id)->get();

        // return $training_provider_id_user;
        $data = [
            'id_users' => $training_provider_id_user
        ];

        return view('training_provider.courses.index_approved', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve($id)
    {

        $course = Course::find($id);
        $course->is_approved = 1;
        $course->status = '1';
        $course->save();

        return redirect('/training_provider/courses');
    }
}
