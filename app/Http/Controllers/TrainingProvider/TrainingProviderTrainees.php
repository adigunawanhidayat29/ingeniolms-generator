<?php

namespace App\Http\Controllers\TrainingProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\TrainingProviderTrainee;
use App\TrainingProviderTrainer;
use Auth;

class TrainingProviderTrainees extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = TrainingProviderTrainee::join('users', 'training_provider_trainees.user_id', '=', 'users.id')
        ->orderBy('users.name')
        ->get();

        $data = [
            'users' => $users
        ];
        
        return view('training_provider.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::join('users_groups', 'users.id', '=', 'users_groups.user_id')
        ->select('users.name', 'users.id')
        ->where('users_groups.level_id', 5)
        ->where('users.is_provided', null)
        ->orderBy('users.name')
        ->get();

        $data = [
            'users' => $users
        ];
        
        return view('training_provider.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        
        $training_provider_id = TrainingProviderTrainer::where('user_id', $user_id)->value('training_provider_id');

        $user = new TrainingProviderTrainee;
        $user->training_provider_id = $training_provider_id;
        $user->user_id = $request->user_id;
        $user->save();

        return redirect('/training_provider/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
