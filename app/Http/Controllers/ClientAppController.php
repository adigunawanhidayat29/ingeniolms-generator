<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Site;
use App\SiteElement;
use App\Template;
use App\SitePublish;
use App\SiteGlobal;
use App\SitePage;
use App\User;
use App\SiteValue;
use App\SiteUserPage;
use App\Application;
use Auth;
use Validator;
use DateTime, DateInterval;

class ClientAppController extends Controller {

    public function get_api_key(Request $request){
        $domain = $request->domain;
        $SiteGlobal = SiteGlobal::where('domain', $domain)->orWhere('custom_domain', $domain)->first();

        if(empty($SiteGlobal)){
            return response()->json([
                'error' => 500,
                'message' => 'Your site is not working, please contact technical support'
            ]);
        }

        if($SiteGlobal->application_token==null || $SiteGlobal->application_token==''){
            return response()->json([
                'error' => 404,
                'message' => 'This site isn`t integrate with any ingenio group, please integrate first site settings'
            ]);
        }

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => [
                'api_key' => $SiteGlobal->application_token,
            ]
        ]);
    }

    public function insert_api_key(Request $request){
        $user_id = $request->ingenio_user_id;
        $group_id = $request->group_id;

        $Application = Application::where('id_instructor_group', $group_id)->first();
        if(empty($Application)){
            return response()->json([
                'error' => 404,
                'message' => 'Invalid id group',
            ]);
        }

        SiteGlobal::where('user_id', $user_id)->update(['application_token' => $Application->api_key]);
        return response()->json([
            'error' => 0,
            'message' => 'success'
        ]);
    }

    public function create_application(Request $request){
        $User = Auth::user();
        $id_instructor_group = $request->id_instructor_group;
        $date_now = new DateTime('now');

        $date = new DateTime('now');
        $expired_date = $date->add(new DateInterval('P1Y'));

        $InstructorGroup = DB::table('instructor_groups')->where('id', $id_instructor_group)->first();
        if(empty($InstructorGroup)){
            return response()->json([
                'error' => 404,
                'message' => 'Group Not Found'
            ]);
        }

        $Application = new Application;
        $Application->name = $InstructorGroup->title;
        $Application->id_instructor_group = $id_instructor_group;
        $Application->created_by = $User->id;
        $Application->api_key = str_random(20);
        $Application->registered_at = $date_now->format('Y-m-d H:i:s');
        $Application->expired_at = $expired_date->format('Y-m-d H:i:s');
        $Application->save();

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $Application,
        ]);

    }

    public function get_available_group(){
        $User = Auth::user();
        $Groups = DB::table('instructor_groups')->where('created_by', $User->id)->get();

        if(empty($Groups)){
            return response()->json([
                'error' => 404,
                'message' => 'Group Not found'
            ]);
        }

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $Groups,
        ]);

    }

    public function create_custom_domain(Request $request){
        $default_domain = $request->default_domain;
        $custom_domain = $request->custom_domain;

        // Checking the custom domain, if someone has used
        $isCustomDomainExist = SiteGlobal::where('custom_domain', $custom_domain)->first();
        if(!empty($isCustomDomainExist)){
            return response()->json([
                'error' => 406,
                'message' => 'Your domain has used by someone, please use different custom domain'
            ]);
        }

        $SiteGlobal = SiteGlobal::where('domain', $default_domain);
        if(empty($SiteGlobal->first())){

            // if site information not found
            return response()->json([
                'error' => 404,
                'message' => 'Site global not found'
            ]);
        }

        // Creating custom domain
        $headers = array(
			'Content-type: application/json',
			'Accept: application/json',
		);
		$fields = array(
            'domain' => $default_domain,
            'custom_domain' => $custom_domain,
		);
		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://api-domain.ingenio.co.id/api/customdomain/create' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        // if got something wrong when creating custom domain
        $responseResult = json_decode($result, true);
        if ($responseResult['error'] != 0) {
            return response()->json($responseResult);
        }

        // Update custom domain
        $SiteGlobal->update(['custom_domain' => $custom_domain]);
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $SiteGlobal,
        ]);

    }

    
}


?>