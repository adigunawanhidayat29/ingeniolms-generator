<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Degree;
use App\DegreeUser;
use App\DegreeCourse;
use App\Course;
use App\CourseUser;
use App\Section;
use App\Content;
use Auth;
use DB;
use Session;

class DegreeController extends Controller
{
  public function index(){
    $data = [
      'degrees' => Degree::get(),
    ];
    return view('degree.index', $data);
  }

  public function detail($slug){
    $degree = Degree::where('slug', $slug)->first();
    if($degree){

      if(Auth::check()){
        $DegreeUser = DegreeUser::where(['degree_id'=>$degree->id, 'user_id'=>Auth::user()->id])->first();
        if($DegreeUser){ // jika user sudah enroll degree
          return redirect('degree/learn/'.$slug);
        }
      }

      // get course on degree
      $DegreeCourses = DegreeCourse::where(['degree_id'=>$degree->id])->get();
      $data_course_id = [];
      foreach($DegreeCourses as $DegreeCourse){
        array_push($data_course_id,$DegreeCourse->course_id) ;
      }
      $Courses = Course::whereIn('courses.id', $data_course_id)
        ->where('status','1')
        ->join('users','users.id','=','courses.id_author')
        ->get();
      // get course on degree

      $data = [
        'degree' => $degree,
        'courses' => $Courses,
      ];
      return view('degree.detail', $data);
    }else{
      abort('404');
    }

  }

  public function learn($slug){
    $degree = Degree::where('slug', $slug)->first();
    $DegreeUser = DegreeUser::where(['degree_id'=>$degree->id, 'user_id'=>Auth::user()->id])->first();
    if($DegreeUser){ // jika user sudah enroll degree

      // get course on degree
      $DegreeCourses = DegreeCourse::where(['degree_id'=>$degree->id])->get();
      $data_course_id = [];
      foreach($DegreeCourses as $DegreeCourse){
        array_push($data_course_id,$DegreeCourse->course_id) ;
      }

      $courses = Course::select('courses.*','users.name as author')
        ->join('users','users.id','=','courses.id_author')
        ->whereIn('courses.id', $data_course_id)
        ->where('status','1')
        ->get();

        $courses_data = [];
        foreach($courses as $course){
          $sections = Section::where('id_course', $course->id)->get();
          $num_contents = 0;
          foreach($sections as $section){
            $num_contents += count(Content::where('id_section', $section->id)->get());
          }

          array_push($courses_data, [
            'num_contents' => $num_contents,
            'slug' => $course->slug,
            'title' => $course->title,
            'image' => $course->image,
            'name' => $course->author,
            'id' => $course->id,
          ]);
        }
      // get course on degree

      $data = [
        'degree' => $degree,
        'courses_data' => $courses_data,
      ];

      return view('degree.learn', $data);
    }else{
      return redirect('degree/'.$slug);
    }
  }

  public function _checkPassword($id, Request $request){
    $Degree = Degree::where(['id' => $id, 'password' => $request->password])->first();
    if($Degree){
      return 'true';
    }else{
      return 'false';
    }
  }

  public function enroll($id, Request $request){
    $Degree = Degree::where('id', $id)->first();

    $DegreeUser = new DegreeUser;
    $DegreeUser->degree_id = $id;
    $DegreeUser->user_id = Auth::user()->id;
    $DegreeUser->save();

    $DegreeCourses = DegreeCourse::where('degree_id', $id)->get();
    foreach($DegreeCourses as $DegreeCourse){
      $CourseUser = new CourseUser;
      $CourseUser->user_id = Auth::user()->id;
      $CourseUser->course_id = $DegreeCourse->course_id;
      $CourseUser->save();
    }

    Session::flash('success', 'Daftar Degree Berhasil');
    return $DegreeUser;
  }
}
