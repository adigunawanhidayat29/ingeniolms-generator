<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use Auth;

class ActivationController extends Controller
{
  public function activate_api($token){
    if(isset($token) ){
      $User = User::where('activation_token', $token)->first();
      if(!$User->is_active) {
        $User->is_active = '1';        
        $User->save();
        $status = "Akun anda berhasil aktif, silakan login untuk mulai menikmati fitur kami.";
      }else{
        $status = "Akun Anda sudah aktif, silakan login kembali";
      }
    }else{
        return redirect('/login')->with('warning', "Maaf email anda tidak teridentifikasi.");
    }

    //return redirect('/login')->with('status', $status);
    return response()->json([
      'error' => 0,
      'message' => 'Success',
      'data' => 'Activation Success'
    ]);
  }
  public function activate($token){
    if(isset($token) ){
      $User = User::where('activation_token', $token)->first();
      if(!$User->is_active) {
        $User->is_active = '1';        
        $User->save();
        $status = "Akun anda berhasil aktif, silakan login untuk mulai menikmati fitur kami.";
      }else{
        $status = "Akun Anda sudah aktif, silakan login kembali";
      }
    }else{
        return redirect('/login')->with('warning', "Maaf email anda tidak teridentifikasi.");
    }

    return redirect('/login')->with('status', $status);
    // return redirect('user/activation/success');
  }

  public function success(){
    return view('profile.activation_success');
  }

  public function resend() {
    $User = Auth::user();
    Mail::to($User->email)->send(new Register($User));

    return view('profile.resend_activation_success');
  }
}
