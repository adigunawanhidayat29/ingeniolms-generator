<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessageSent;
use App\CourseChat;
use Auth;

class MessageController extends Controller
{
  public function save(Request $request){
    $User = Auth::user();
    $CourseChat = new CourseChat;
    $CourseChat->course_id = $request->course_id;
    $CourseChat->user_id = $User->id;
    $CourseChat->body = $request->body;
    $CourseChat->save();

    broadcast(new MessageSent($User, $CourseChat));
  }

  public function get($course_id){
    $CoursesChats = CourseChat::where('course_id', $course_id)->join('users','users.id','=','courses_chats.user_id')->orderby('courses_chats.id','asc')->get();
    $data = [
      'courses_chats' => $CoursesChats,
    ];
    return view('meeting.message', $data);
  }
}
