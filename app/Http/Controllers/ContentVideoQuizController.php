<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentVideoQuiz;
use App\ContentVideoQuizAnswer;
use App\Content;
use App\Course;
use GuzzleHttp\Client;

class ContentVideoQuizController extends Controller
{
  public function index($content_id)
  {
    $Content = Content::where('id', $content_id)->first();

    // $headers_a = array(
    // 	'Content-type: application/x-www-form-urlencoded',
    // 	'Accept: application/x-www-form-urlencoded',
    // );
    // $fields_a = 'client_secret='.env('GOOGLE_DRIVE_CLIENT_SECRET').'&grant_type=refresh_token&refresh_token='.env('GOOGLE_DRIVE_REFRESH_TOKEN').'&client_id='.env('GOOGLE_DRIVE_CLIENT_ID');

    // $ch = curl_init();
    //   curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token' );
    //   curl_setopt( $ch,CURLOPT_POST, true );
    //   curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_a);
    //   curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    //   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    //   curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields_a  );
    //   $result = curl_exec($ch );
    //   curl_close( $ch );

    //   $client_info = json_decode($result);
    //   // dd($client_info);

    // $curl = curl_init();
    // curl_setopt_array($curl, array(
    //   CURLOPT_URL => "https://www.googleapis.com/drive/v2/files/" . $Content->path_file,
    //   CURLOPT_RETURNTRANSFER => true,
    //   CURLOPT_TIMEOUT => 30,
    //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //   CURLOPT_CUSTOMREQUEST => "GET",
    //   CURLOPT_HTTPHEADER => array(
    //     "Authorization: Bearer " . $client_info->access_token
    //   ),
    // ));

    // $response = curl_exec($curl);
    // $err = curl_error($curl);
    // curl_close($curl);
    // $data = json_decode($response);
    // dd($data);
    // echo $data->downloadUrl;

    $course = Course::select('courses.id')
      ->join('sections', 'sections.id_course', '=', 'courses.id')
      ->join('contents', 'contents.id_section', '=', 'sections.id')
      ->where('contents.id', $content_id)
      ->first();

    $data = [
      'content' => $Content,
      'content_video_quizes' => ContentVideoQuiz::where('content_id', $content_id)->with('content_video_quiz_answers')->get(),
      'course' => $course,
      // 'videoProperties' => $data,
      // 'access_token' => $client_info->access_token,
    ];
    return view('content.video-manager', $data);
  }

  public function delete($id)
  {
    $ContentVideoQuiz = ContentVideoQuiz::find($id)->delete();
    return redirect()->back();
  }

  public function store(Request $request)
  {
    $ContentVideoQuiz = new ContentVideoQuiz;
    $ContentVideoQuiz->content_id = $request->content_id;
    $ContentVideoQuiz->question = $request->question;
    $ContentVideoQuiz->seconds = $request->seconds;
    $ContentVideoQuiz->status = '1';
    $ContentVideoQuiz->save();

    foreach ($request->answer as $index => $value) {
      $ContentVideoQuizAnswer = new ContentVideoQuizAnswer;
      $ContentVideoQuizAnswer->content_video_quiz_id = $ContentVideoQuiz->id;
      $ContentVideoQuizAnswer->answer = $value;
      $ContentVideoQuizAnswer->answer_description = $request->answer_description[$index];
      $ContentVideoQuizAnswer->is_correct = $request->is_correct[$index];
      $ContentVideoQuizAnswer->save();
    }

    return redirect()->back();
  }

  public function check_answer(Request $request)
  {
    $ContentVideoQuizAnswer = ContentVideoQuizAnswer::where('id', $request->content_video_quiz_answer_id)->first();
    // dd($ContentVideoQuizAnswer);
    return response()->json([
      'is_correct' => $ContentVideoQuizAnswer->is_correct,
      'answer_description' => $ContentVideoQuizAnswer->answer_description,
    ]);
  }
}
