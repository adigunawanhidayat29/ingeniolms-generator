<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Site;
use App\SiteElement;
use App\Template;
use App\SitePublish;
use App\SiteGlobal;
use App\SitePage;
use App\User;
use App\SiteValue;
use Auth;
use Validator;
use Image;


class ClientAssetsController extends Controller{
    public function upload_assets(Request $request){
        $destinationPath = '../../../client-assets/'. 'upload/' . $request->user_id . '/'; // upload path
        if ( ! file_exists($destinationPath))
        {
            mkdir($destinationPath, 0777, TRUE);
        }
        $originalExtension = $request->file('file')->getClientOriginalExtension(); // getting image extension
        $originalName = $request->file('file')->getClientOriginalName();
        $file = rand(11111,999999) . '_' . rand(11111,999999) . '_' . str_slug($originalName) . '.' . $originalExtension;
        // $file = $originalName .'_'. rand(11111,999999).'_'.rand(11111,999999).'.'.$extension; // renameing image
        $ImageLogo = Image::make($request->file('file'))->resize(164, 90)->save($destinationPath . 'landingpage_' .$file);
        $request->file('file')->move($destinationPath, $file); // uploading file to given path
        $finalAssetsPath = ClientAssetUrl($request->user_id . '/' . $file);
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => [
                'asset_url' => $finalAssetsPath,
            ],
        ]);
    }
}

?>