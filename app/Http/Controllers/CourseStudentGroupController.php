<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseStudentGroup;
use App\CourseStudentGroupUser;

class CourseStudentGroupController extends Controller
{
    public function store($id, Request $request)
    {
        $CourseStudentGroup = new CourseStudentGroup;
        $CourseStudentGroup->course_id = $id;
        $CourseStudentGroup->name = $request->name;
        $CourseStudentGroup->description = $request->description;
        $CourseStudentGroup->created_by = \Auth::user()->id;
        $CourseStudentGroup->save();

        return redirect()->back()->with('message', \Lang::get('front.page_manage_atendee.message_group_created'));
    }

    public function addUser($id, Request $request)
    {
        $user_id = isset($request->user_id) ? array_unique($request->user_id) : null;
        if ($user_id) {
            $CourseStudentGroup = CourseStudentGroup::where('id', $id)->first();
            CourseStudentGroupUser::where('course_student_group_id', $id)->delete();

            if ($request->user_id) {
                foreach ($user_id as $index => $value) {
                    $CourseStudentGroupUser = new CourseStudentGroupUser;
                    $CourseStudentGroupUser->course_student_group_id = $id;
                    $CourseStudentGroupUser->course_id = $CourseStudentGroup->course_id;
                    $CourseStudentGroupUser->user_id = $value;
                    $CourseStudentGroupUser->save();
                }
            }
        } else {
            CourseStudentGroupUser::where('course_student_group_id', $id)->delete();
        }

        return redirect()->back()->with('flash_message', \Lang::get('front.page_manage_atendee.message_usergroup_created'));
    }
}
