<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Content;
use Storage;

class SectionController extends Controller
{
  public function update_action($id, Request $request){
    $Section = Section::where('id', $id)->first();
    $Section->title = $request->section_title;
    $Section->description = $request->section_description;
    $Section->sequence = $request->section_sequence;
    $Section->save();

    \Session::flash('success', 'Update section success');
    return redirect('course/manage/'.$Section->id_course);
  }

  public function delete($id_course, $id){
    $Section = Section::where('id', $id);
    if($Section){
      //delete content
      $Contents = Content::where('id_section', $id);
      foreach($Contents->get() as $content){
        // unlink(asset_path($content->name_file));
        Storage::disk('google')->delete($content->path_file); // delete file google drive
      }
      $Contents->delete();
      //delete content

      $Section->delete();

      \Session::flash('success', 'Delete section success');
    }else{
      \Session::flash('error', 'Delete section failed');
    }

    return redirect('course/preview/'.$id_course);
  }

  public function update_sequence(Request $request){
    foreach($request->datas as $key => $value){
      $Section = Section::find($value);
      $Section->id_course = $request->course_id;
      $Section->sequence = $key;
      $Section->save();
    }
  }

  public function edit_section(Request $request){
    $Section = Section::where('id', $request->id)->first();
    $Section->title = $request->title;
    $Section->description = $request->description;
    $Section->status = $request->status;
    $Section->save();
  }

  public function change_status(Request $request){
    $Section = Section::where('id', $request->section_id)->first();
    $Section->status = $request->status == 'true' ? '1' : '0' ;
    $Section->save();
  }

}
