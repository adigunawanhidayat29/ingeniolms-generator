<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseAnnouncement;
use App\CourseAnnouncementComment;
use App\Course;
use Auth;
use Lang;

class CourseAnnouncementController extends Controller
{
  public function index($course_id){
    $CourseAnnouncement = CourseAnnouncement::where('course_id', $course_id)->orderBy('created_at','desc')->paginate(5);
    $data = [
      'course_announcements' => $CourseAnnouncement
    ];
    return view('course.announcement', $data);
  }

  public function create($course_id){
    $data = [
      'url' => '/course/announcement/store/'.$course_id,
      'method' => 'post',
      'title' => old('title'),
      'description' => old('description'),
      'course' => Course::where('id', $course_id)->first(),
    ];
    return view('course.announcement_form', $data);
  }

  public function store($course_id, Request $request){
    $CourseAnnouncement = new CourseAnnouncement;
    $CourseAnnouncement->course_id = $course_id;
    $CourseAnnouncement->title = $request->title;
    $CourseAnnouncement->description = $request->description == null ? '' : $request->description;
    $CourseAnnouncement->save();

    return redirect('course/preview/'.$course_id);
  }

  public function edit($id){
    $CourseAnnouncement = CourseAnnouncement::where('id', $id)->first();

    $data = [
      'url' => '/course/announcement/update/'.$id,
      'method' => 'post',
      'title' => old('title', $CourseAnnouncement->title),
      'description' => old('description', $CourseAnnouncement->description),
      'course' => Course::where('id', $CourseAnnouncement->course_id)->first(),
    ];
    return view('course.announcement_form', $data);
  }

  public function update($id, Request $request){
    $CourseAnnouncement = CourseAnnouncement::where('id', $id)->first();
    $CourseAnnouncement->title = $request->title;
    $CourseAnnouncement->description = $request->description;
    $CourseAnnouncement->save();

    return redirect('course/preview/'.$CourseAnnouncement->course_id);
  }

  public function delete($id){
    $CourseAnnouncement = CourseAnnouncement::where('id', $id)->first();
    $CourseAnnouncement->delete();

    return back();
  }

  public function comment(Request $request){
    $CourseAnnouncementComment = new CourseAnnouncementComment;
    $CourseAnnouncementComment->course_announcement_id = $request->course_announcement_id;
    $CourseAnnouncementComment->comment = $request->comment;
    $CourseAnnouncementComment->user_id = Auth::user()->id;
    $CourseAnnouncementComment->status = '1';
    $CourseAnnouncementComment->save();

    return back()->with('success', Lang::get('front.page_manage_courses.announcement_comment_success'));
  }
}
