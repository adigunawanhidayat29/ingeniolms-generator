<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\AffiliateTransaction;
use App\AffiliateBalance;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\PlayerIdUser;
use App\Course;
use App\InstructorBalance;
use App\Instructor;
use App\ProgramCourse;
use App\ProgramUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionInformation;

use App\Veritrans\Veritrans;

class PaymentController extends Controller
{

  public function __construct()
  {
      Veritrans::$serverKey = '<your server key>';
      Veritrans::$isProduction = false;
  }

  public function transaction()
  {
      return view('transaction');
  }

  public function transaction_process(Request $request)
  {
      $vt = new Veritrans;
      $order_id = $request->input('order_id');
      $action = $request->input('action');
      switch ($action) {
          case 'status':
              $this->status($order_id);
              break;
          case 'approve':
              $this->approve($order_id);
              break;
          case 'expire':
              $this->expire($order_id);
              break;
          case 'cancel':
              $this->cancel($order_id);
              break;
      }
  }

  public function status($order_id)
  {
      $vt = new Veritrans;
      echo 'test get status </br>';
      print_r ($vt->status($order_id) );
  }

  public function cancel($order_id)
  {
      $vt = new Veritrans;
      echo 'test cancel trx </br>';
      echo $vt->cancel($order_id);
  }

  public function approve($order_id)
  {
      $vt = new Veritrans;
      echo 'test get approve </br>';
      print_r ($vt->approve($order_id) );
  }

  public function expire($order_id)
  {
      $vt = new Veritrans;
      echo 'test get expire </br>';
      print_r ($vt->expire($order_id) );
  }

  public function approve_transaction($invoice){
    // Update status to success
    $Transaction = Transaction::where('invoice', $invoice)->first();
    $Transaction->status = '1';
    $Transaction->save();
    // Update status to success

    // check affiliate transaction
    $AffiliateTransaction = AffiliateTransaction::where('invoice', $invoice)->first();
    if($AffiliateTransaction){
      $AffiliateTransaction->status = '1'; // set status affiliate TRANSACTION to success
      $AffiliateTransaction->save();

      $AffiliateBalance = new AffiliateBalance;
      $AffiliateBalance->affiliate_id = $AffiliateTransaction->affiliate_id;
      $AffiliateBalance->debit = $AffiliateTransaction->commission;
      $AffiliateBalance->credit = 0;
      $AffiliateBalance->information = 'Comission transaction '.$invoice;
      $AffiliateBalance->save();
    }
    // check affiliate transaction

    // add user to course
    $user_id = $Transaction->user_id;
    $TransactionsDetails = TransactionDetail::where('invoice', $invoice)->get();
    foreach($TransactionsDetails as $transaction_detail){
      if($transaction_detail->course_id != null){
        $CourseUser = new CourseUser;
        $CourseUser->user_id = $Transaction->user_id;
        $CourseUser->course_id = $transaction_detail->course_id;
        $CourseUser->save();

        // add to balance instructor
        $course = Course::where('id', $transaction_detail->course_id)->first();
        $instructor = Instructor::where('user_id', $course->id_author)->first();
        $InstructorBalance = new InstructorBalance;
        $InstructorBalance->instructor_id = $instructor->id;
        $InstructorBalance->debit = $transaction_detail->total;
        $InstructorBalance->credit = 0;
        $InstructorBalance->information = 'transaction course ' . $course->title . ' - ' . $transaction_detail->invoice;
        $InstructorBalance->save();
        // add to balance instructor
      }else{
        $ProgramUser = new ProgramUser;
        $ProgramUser->user_id = $Transaction->user_id;
        $ProgramUser->program_id = $transaction_detail->program_id;
        $ProgramUser->save();

        $ProgramCourses = ProgramCourse::where('program_id', $transaction_detail->program_id)->get();
        foreach($ProgramCourses as $ProgramCourse){
          $CourseUser = new CourseUser;
          $CourseUser->user_id = $Transaction->user_id;
          $CourseUser->course_id = $ProgramCourse->course_id;
          $CourseUser->save();
        }
      }
    }

    // add user to course

    // NOTIFICATION PUSH
    $player_id_users = PlayerIdUser::where(['user_id' => $user_id])->get();
  	$headers = array(
  		'Content-type: application/json',
  		'Accept: application/json',
  		'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'
  	);
  	$player_ids = [];
  	foreach ($player_id_users as $player_id_user) {
  		 array_push($player_ids, $player_id_user->player_id);
  	}
  	$fields = array(
  		'app_id' => '611e29ae-92d3-4571-be63-062b0f10b000',
  		'include_player_ids' => $player_ids,
  		'data' => array('user_id' => $user_id),
  		'contents' => array('en' => 'Transaksi Anda berhasil, silahkan menikmati kursus di platform kami, terima kasih'),
  		'headings' => array('en' => 'Transaksi Berhasil'),
  		'android_group' => 'ingenio_transaction'
  	);
  	$ch = curl_init();
  	curl_setopt( $ch,CURLOPT_URL, 'https://onesignal.com/api/v1/notifications' );
  	curl_setopt( $ch,CURLOPT_POST, true );
  	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
  	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  	$result = curl_exec($ch );
  	curl_close( $ch );
    // NOTIFICATION PUSH

    $user = DB::table('users')->where('id', $Transaction->user_id)->first();
    //Mail::to($user->email)->send(new TransactionInformation($Transaction));
    \Session::flash('success', 'Course berhasil dibeli. Selamat Semangat Belajar!');
    return redirect('my-course');
  }

}
