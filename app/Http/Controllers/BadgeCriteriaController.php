<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BadgeCriteriaAssign;
use App\BadgeContentAssign;
use App\BadgeCourseAssign;
use App\Content;

class BadgeCriteriaController extends Controller
{
    public function create()
    {
        return view('badge_criteria.create');
    }

    public function assign(Request $request)
    {
        if($request->criteria_id == 1){

            $badge_criteria_assign = new BadgeCriteriaAssign;
            $badge_criteria_assign->badge_id = $request->badge_id;
            $badge_criteria_assign->criteria_id = $request->criteria_id;
            $badge_criteria_assign->save();

            $badge_course_assign = new BadgeCourseAssign;
            $badge_course_assign->badge_id = $request->badge_id;
            $badge_course_assign->course_id = $request->course_id;
            $badge_course_assign->save();

            // return 'sukses';
            return redirect()->back()->with('success', 'Course completion successfully added');
        }

        else{

            $contents = Content::join('sections', 'contents.id_section', '=', 'sections.id')
            ->join('courses', 'courses.id', '=', 'sections.id_course')
            ->where('courses.id', $request->course_id)
            ->select('contents.title', 'contents.id')
            ->get();

            $data = [
                'contents' => $contents,
                'badge_id' => $request->badge_id,
                // 'course_id' => $request->course_id,
                'criteria_id' => $request->criteria_id
            ];

            return view('badge_criteria.activity_completion', $data);
        }
    }

    public function store(Request $request)
    {
        $contents_array = $request->idcontent;

        $badge_criteria_assign = new BadgeCriteriaAssign;
        $badge_criteria_assign->badge_id = $request->badge_id;
        $badge_criteria_assign->criteria_id = $request->criteria_id;
        $badge_criteria_assign->complete_type = $request->complete_type;

        $badge_criteria_assign->save();

        foreach($contents_array as $content){
            $badge_content = new BadgeContentAssign;
            $badge_content->content_id = $content;
            $badge_content->badge_id = $request->badge_id;
            $badge_content->save();
        }

        // return 'sukses';
        return redirect('/5/'. $badge_criteria_assign->badge_id .'/badge_overview')->with('success', 'Activity completion successfully added');
    }
}
