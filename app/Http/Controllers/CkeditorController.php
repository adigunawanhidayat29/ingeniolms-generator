<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class CkeditorController extends Controller
{
    public function browser(){
      $directory = "img/ckeditor/".Auth::user()->slug;
      $images = glob($directory. "/*");
      $data = [
        'images' => $images,
      ];
      // dd($data);
      return view('ckeditor.browse', $data);
    }

    public function uploader(Request $request){
      $CKEditor = $request->get('CKEditor');
    	$funcNum  = $request->get('CKEditorFuncNum');
    	$message  = $url = '';
    	if ($request->hasFile('upload')) {
    		$file = $request->file('upload');
    		if ($file->isValid()) {
    			$filename =rand(1000,9999).$file->getClientOriginalName();
    			$file->move(public_path().'/img/ckeditor/'.Auth::user()->slug.'/', $filename);
    			$url = url('img/ckeditor/'.Auth::user()->slug.'/' . $filename);
    		} else {
    			$message = 'An error occurred while uploading the file.';
    		}
    	} else {
    		$message = 'No file uploaded.';
    	}
    	echo $CKEditor.'<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
    }
}
