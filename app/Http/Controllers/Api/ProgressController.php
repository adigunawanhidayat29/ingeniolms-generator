<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Progress;
use App\User;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;

class ProgressController extends Controller{
	public function reset(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'course_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
		$Progress = Progress::where(['user_id' => $request->user_id, 'course_id' => $request->course_id])->update(['status' => '0']);
	}

	public function set(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'course_id' => 'required',
			'content_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

		if(!Progress::where(['content_id' => $request->content_id, 'user_id' => $request->user_id])->first()){
	        // insert progress
	        $Progress = new Progress;
	        $Progress->content_id = $request->content_id;
	        $Progress->course_id = $request->course_id;
	        $Progress->user_id = $request->user_id;
	        $Progress->status = '1';
	        $Progress->save();
	        // insert progress
	    }else{
	        $Progress = Progress::where(['content_id' => $request->content_id, 'user_id' => $request->user_id])->first();
	        $Progress->status = '1';
	        $Progress->save();
	    }
	}

	public function checkProgress($user_id, $content_id){
		$content_progress = count(DB::table('progresses')->where(['content_id'=>$content_id, 'user_id' => $user_id, 'status' => '1'])->first());
		return response()->json([
			'message' => 'success',
			'code' => 200,
			'data' => $content_progress
		]);
	}
}



?>