<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller {
	public function index($id){
		$user = User::where(['id' => $id])->first();
		return response()->json([
			'message' => 'sukses',
			'data' => $user
		]);
	}

	public function update(Request $request){
		$User = User::where('id', $request->user_id)->first();
		$User->name = $request->name;
		$User->phone = $request->phone;
    	$User->save();
	}

	public function update_password(Request $request){
	    $User = User::where('id', $request->user_id)->first();
	    $User->password = bcrypt($request->password);
	    $User->save();
	}

	public function update_avatar(Request $request){
	    $User = User::where('id', $request->user_id)->first();
	    $default_photo = "/uploads/users/default.png";
	    $import_photo = explode('/', $User->photo);
	    if($default_photo != $User->photo){
		    if($import_photo[1] == 'uploads'){        
		      unlink(asset_path($User->photo));
		    }
	    }

	    $destinationPath = asset_path('uploads/users/'); // upload path
	    $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
	    $image = str_slug($User->name).rand(1111,9999).'.'.$extension; // renameing image
	    $request->file('image')->move($destinationPath, $image); // uploading file to given path

	    $User = User::where('id', $request->user_id)->first();
	    $User->photo = '/uploads/users/'.$image;
	    $User->save();
	}
}


?>