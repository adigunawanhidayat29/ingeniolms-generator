<?php

namespace App\Http\Controllers\Api\V3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Course;
Use App\CourseProjectUser;
Use App\Category;
Use App\ProgramCourse;
Use App\Program;
Use App\Video;
Use App\Degree;
Use App\Slider;

class HomeController extends Controller
{
    public function index(){
        $courses = Course::with('sections', 'user')->paginate(6);
        $sliders = Slider::where('status', '1')->get();
        $categories = Category::get();

        $data = [
            'courses' => $courses,
            'sliders' => $sliders,
            'categories' => $categories,
        ];
        
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ], 200);
    }
}
