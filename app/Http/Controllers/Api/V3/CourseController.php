<?php

namespace App\Http\Controllers\Api\V3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Course;
Use App\Content;

class CourseController extends Controller
{
    public function detail($id, Request $request) {

        $courses = Course::with('sections', 'user')->find($id);

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $courses
        ], 200);
    }

    public function courseContent($id, $content_id, Request $request) {

        $courses = Course::with('sections', 'user')->find($id);
        $content = Content::find($content_id);

        $data = [
            'courses' => $courses,
            'content' => $content
        ];

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ], 200);
    }
}
