<?php

namespace App\Http\Controllers\Api\V3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;

class SettingController extends Controller
{
    public function index(){

        $data = [];
        $settings = Setting::all();
        foreach($settings as $setting) {            
            $data[$setting->name] = [
                'name' => $setting->name,
                'value' => $setting->value
            ];         
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ], 200);
    }
    public function detail($name){

        $setting = Setting::where('name', $name)->first();        
        
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $setting
        ], 200);
    }
    public function menu(){

        $menu = [
            [
                'name' => 'home',
                'name_ID' => 'beranda',
                'url' => '/home',
            ],
            [
                'name' => 'courses',
                'name_ID' => 'kelas',
                'url' => '/courses',
            ],
            [
                'name' => 'categories',
                'name_ID' => 'kategori',
                'url' => '/categories',
            ],
            [
                'name' => 'profile',
                'name_ID' => 'profil',
                'url' => '/profile',
            ],
            [
                'name' => 'login',
                'name_ID' => 'masuk',
                'url' => '/login',
            ],
            [
                'name' => 'register',
                'name_ID' => 'daftar',
                'url' => '/register',
            ],
        ];        
        
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $menu
        ], 200);
    }
}
