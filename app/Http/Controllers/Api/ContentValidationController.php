<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\Content;
use Validator;

class ContentValidationController extends Controller {
    public function index($folder_name){
        $Course = Course::where('folder', $folder_name)->first();
        if (empty($Course)) {
            return response()->json([
                'message' => 'Success',
                'data' => null
            ]);
        }
        $sections = DB::table('sections')->where('id_course', $Course->id)->orderBy('sequence','asc')->orderBy('updated_at','desc')->get();
        $contents = [];
        foreach ($sections as $section) {
            $contents_get = DB::table('contents')
                ->where('id_section', $section->id)
                ->where('type_content', 'video')
                ->get();
            for ($i=0; $i < count($contents_get); $i++){
                if ($contents_get[$i]->full_path_file_resize==null || $contents_get[$i]->full_path_original_resize==null ) {
                    array_push($contents, $contents_get[$i]);
                }
            }
        }
        return response()->json([
            'message' => 'success',
            'code' => 200,
            'course_id' => $Course->id,
            'data' => $contents
        ]);
    }
}

?>