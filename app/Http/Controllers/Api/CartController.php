<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use Cart;
use App\Course;
use App\Program;
use App\Promotion;
use App\PromotionUser;
use Session;

class CartController extends Controller {
    public function promotion(Request $request){
        $Promotion = Promotion::
        where([
            'discount_code' => $request->discount_code,
            'status' => '1',
        ])
        ->where('start_date', '<=', date("Y-m-d H:i:s"))
        ->where('end_date', '>=', date("Y-m-d H:i:s"))
        ->first();

        if($Promotion){
        $PromotionUser = PromotionUser::where(['promotion_users.user_id' => Auth::user()->id, 'promotion_users.promotion_id' => $Promotion->id, 'transactions.status' => '1'])
            ->join('transactions', 'transactions.invoice', '=', 'promotion_users.invoice')
            ->first();
        if($PromotionUser){
            return response()->json([
	        	'message' => 'error',
	        	'data' => 'Kode sudah dipakai'
	        ]);
        }else{
            return response()->json([
                'message' => 'success',
                'data' => $Promotion
            ]);
        }
        }else{
            return response()->json([
	        	'message' => 'error',
	        	'data' => 'Kode sudah dipakai'
	        ]);
        }
    }
}


?>