<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Transaction;
use App\AffiliateTransaction;
use App\AffiliateUser;
use App\AffiliateSetting;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\Promotion;
use App\PromotionUser;
use Validator;

class TransactionController extends Controller {
    public function process(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cart' => 'required',
            'payment_method' => 'required',
            'user_id' => 'required',
            'promotion' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' => $validator->errors()
            ], 406);
        }

        // The ugly converter -_- =====
        $carts = json_decode(json_encode((array)$request->cart), false);
        if (!empty($request->promotion)) {
            $Promotion = json_decode(json_encode((array)$request->promotion), false);
        }
        // ==========================

        $invoice = rand(100000000,999999999);
        $unique_number = rand(100,999);

        //insert transaction detail
        $subtotal = 0;
        foreach($carts as $cart){
            $subtotal += $cart->price;
            $TransactionDetail = new TransactionDetail;
            $TransactionDetail->invoice = $invoice;
            $TransactionDetail->course_id = $cart->id;
            $TransactionDetail->total = $cart->price;
            $TransactionDetail->save();
        }
        //insert transaction detail

        if(!empty($request->promotion)){
            $PromotionUser = new PromotionUser;
            $PromotionUser->promotion_id = $Promotion->id;
            $PromotionUser->user_id = $request->user_id;
            $PromotionUser->invoice = $invoice;
            $PromotionUser->save();

            $CheckPromotionAffiliate = AffiliateUser::where('affiliate_users.code', $Promotion->discount_code)->first();
            if ($CheckPromotionAffiliate) {
                $AffiliateSetting = AffiliateSetting::where('status', '1')->orderBy('id', 'desc')->first();
                $AffiliateTransaction = new AffiliateTransaction;
                $AffiliateTransaction->affiliate_id = $CheckPromotionAffiliate->id;
                $AffiliateTransaction->user_id = $request->user_id;
                $AffiliateTransaction->invoice = $invoice;
                $AffiliateTransaction->status = '0';
                $AffiliateTransaction->commission = $AffiliateSetting->commission;
                $AffiliateTransaction->save();
            }

            $discount_price = ($subtotal * $Promotion->discount) / 100;
            //insert transaction
            $Transaction = new Transaction;
            $Transaction->invoice = $invoice;
            $Transaction->user_id = $request->user_id;
            $Transaction->method = $request->payment_method;
            $Transaction->subtotal = ($subtotal - $discount_price) + $unique_number;
            $Transaction->unique_number = $unique_number;
            $Transaction->promotion = '1';
            $Transaction->promotion_id = $Promotion->id;
            $Transaction->discount_code = $Promotion->discount_code;
            $Transaction->status = '0';
            $Transaction->save();
        //insert transaction

        //$request->session()->forget('promotion'); // delete session promo

        }else{
        //insert transaction
            $Transaction = new Transaction;
            $Transaction->invoice = $invoice;
            $Transaction->user_id = $request->user_id;
            $Transaction->method = $request->payment_method;
            $Transaction->subtotal = $subtotal + $unique_number;
            $Transaction->unique_number = $unique_number;
            $Transaction->promotion = '0';
            $Transaction->status = '0';
            $Transaction->save();

        //insert transaction
        }
        return response()->json([
            'message' => 'success',
            'data' => 'Checkout berhasil, silahkan melakukan transfer ke nomer rekening kami',
            'checkout_detail' => Transaction::where(['invoice' => $invoice])->get()
        ], 200);
    }

    public function confirmation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'invoice' => 'required',
            'account_name' => 'nullable',
            'account_number' => 'nullable',
            'bank_name' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json([
              'message' => 'error',
              'data' => $validator->errors()
            ], 406);
        }
        if(Transaction::where('invoice', $request->invoice)->first()){

            $TransactionConfirmation = new TransactionConfirmation;

            if($request->file('file')){
              $destinationPath = asset_path('uploads/confirmations/'); // uploaded file path
              $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
              $file = $request->invoice.'-'.rand(1111,9999).'.'.$extension; // rename image
              $request->file('file')->move($destinationPath, $file); // uploading file to given path
              $TransactionConfirmation->file = '/uploads/confirmations/'.$file; // record to database
            }

            // save confirmation
            $TransactionConfirmation->invoice = $request->invoice;
            $TransactionConfirmation->account_name = $request->account_name;
            $TransactionConfirmation->account_number = $request->account_number;
            $TransactionConfirmation->bank_name = $request->bank_name;
            $TransactionConfirmation->save();
            // save confirmation

            return response()->json([
                'message' => 'success',
                'data' => 'Terima kasih',
            ], 200);

        }
        return response()->json([
            'message' => 'error',
            'data' => 'Transaction not found',
        ], 401);
    }
}


?>
