<?php
namespace App\Http\Controllers\Api\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App\Instructor;
use App\Application;
use Illuminate\Support\Facades\DB;
use Hash;
use Session;

use App\Http\Controllers\Api\App\AuthUser;

class LoginController extends Controller {
/**
* @SWG\Post(
*   path="/auth",
*   summary="Login and get auth token",
*   operationId="Login",
*   tags={"Login"},
*   consumes={"application/json"},
*   produces={"application/json"},
*   @SWG\Parameter(
*     name="Authorization",
*     description="header must be set authorization",
*     required=true,
*     type="string",
*     in="header",
*       @SWG\Schema(ref="#/definitions/Header"),
*   ),
*   @SWG\Parameter(
*     name="Body",
*     description="Email",
*     required=true,
*     type="string",
*     in="body",
*       @SWG\Schema(ref="#/definitions/Body"),
*   ),
*   @SWG\Response(response=200, description="successful operation"),
* )
*
*/
 
    public function login(Request $request){

        // Application Registered
        $Application = Application::where('api_key', $request->header('Authorization'))->first();

        // Validating Input
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        // Return error
        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Not Acceptable',
                'data' => $validator->errors(),
            ]);
        }
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        $User = AuthUser::Attempt(ArrayToJson($credentials));
        if($User!=null){
            if($User->is_active){
                $DataUser = AuthUser::User($User->token, $Application);
                return response()->json([
                    'error' => 0,
                    'message' => 'Success',
                    'data' => $DataUser
                ]);
            }
            return response()->json([
                'error' => 401,
                'message' => 'Activation Required',
            ]);

        }
        return response()->json([
            'error' => 401,
            'message' => 'Invalid Email or Password, please try again',
        ]);

    }

    

    public function getSession(Request $request){
        return response()->json([
            'users' => AuthUser::User($request->get('auth')),
        ]);
        // return response()->json([
        //     'data' => Session::all(),
        // ]);
    }

    public function check_token(Request $request){
        // Application Registered
        $Application = Application::where('api_key', $request->header('Authorization'))->first();

        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => AuthUser::User($request->token, $Application),
        ]);
    }

    public function logout(Request $request){
        AuthUser::DestroySession(Session::get('_token'));
        Auth::logout();
        if($request->get('auth')){
            AuthUser::DestroySessionByToken($request->get('auth'));
            return response()->json([
                'error' => 0,
                'message' => 'Success'
            ]);
        }
        // AuthUser::DestroySessionByToken($request->get('auth'));
    }

    public function check_sso(Request $request){
        $AuthenticateUser = DB::table('authenticated_users')->where('session_id', $request->session_id)->first();
        if(empty($AuthenticateUser)){
            return response()->json([
                'error' => 404,
                'message' => 'Not Found'
            ]);
        }
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => AuthUser::User($AuthenticateUser->token),
        ]);

    }
}

?>