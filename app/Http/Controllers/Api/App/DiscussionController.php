<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Discussion;
use App\DiscussionAnswer;
use App\Course;
use Auth;
use App\Http\Controllers\Api\App\AuthUser;

class DiscussionController extends Controller
{
  public function ask(Request $request){
    $auth_token = $request->get('auth');
    $user = AuthUser::User($auth_token);

    $Discussion = new Discussion;
    $Discussion->user_id = $user->id;
    $Discussion->course_id = $request->course_id;
    $Discussion->status = '1';
    $Discussion->body = $request->discussion_content;
    $Discussion->save();

    return response()->json([
        'error' => 0,
        'message' => 'success',
    ]);
  }

  public function answer(Request $request){
    $auth_token = $request->get('auth');
    $user = AuthUser::User($auth_token);

    $DiscussionAnswer = new DiscussionAnswer;
    $DiscussionAnswer->user_id = $user->id;
    $DiscussionAnswer->discussion_id = $request->discussion_id;
    $DiscussionAnswer->body = $request->answer_content;
    $DiscussionAnswer->status = '1';
    $DiscussionAnswer->save();

    return response()->json([
        'error' => 0,
        'message' => 'success',
    ]);
  }

  public function detail($id){
    $Discussion = Discussion::select('discussions.id as id','discussions.course_id','discussions.body','discussions.created_at','users.name','users.photo')
                  ->where('discussions.id', $id)
                  ->join('users','users.id','=','discussions.user_id')
                  ->first();

    $DiscussionAnswers = DiscussionAnswer::where(['discussion_id'=>$id, 'status' => '1'])->join('users','users.id','=','discussions_answers.user_id')->get();
    $Course = Course::where('id', $Discussion->course_id)->first();
    $data = [
      'discussion' => $Discussion,
      'discussion_answers' => $DiscussionAnswers,
      'course' => $Course,
    ];

    return response()->json([
        'error' => 0,
        'message' => 'success',
        'data' => $data,
    ]);
  }
}
