<?php

namespace App\Http\Controllers\Api\App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AuthenticatedUser;
use App\User;
use App\Course;
use App\CourseUser;
use Auth;
use Hash;
use DateTime, DateInterval;

class AuthUser {
    public static function PutSession($session_id, $user_id){
        $date_now = new DateTime('now');
        DB::table('authenticated_users')->where('expired_at', '<', $date_now)->delete();
        $token = str_random(60);
        $date = new DateTime('now');
        $expired_date = $date->add(new DateInterval('PT2H'));
        $login_date = new DateTime('now');
        $data = [
            'session_id' => $session_id,
            'user_id' => $user_id,
            'token' => $token,
            'expired_at' => $expired_date->format('Y-m-d H:i:s'),
        ];
        AuthenticatedUser::create($data);
    }

    public static function DestroySession($session_id){
        DB::table('authenticated_users')->where('session_id', $session_id)->delete();
    }
    public static function DestroySessionByToken($token){
        DB::table('authenticated_users')->where('token', $token)->delete();
        
    }
    public static function DestroySSOSession($token){
        
    }

    // Login using user instance
    public static function login($User){
        Auth::loginUsingId($User->id);

        $User = Auth::user();
        $token = $User->auth_token;
        // $CheckingToken = User::where('id', $User->id)->first();
        if ($User->auth_token==null) {
            $token = str_random(60);
            User::where('id', $User->id)->update(['auth_token' => $token]);
        }
        return ArrayToJson([
            'id' => $User->id,
            'is_active' => $User->is_active,
            'application_registered' => $User->application_registered,
            'token' => $token,
        ]);
        return null;

    }

    public static function Attempt($credential){
        // Delete Expired Token before login ======
        $date_now = new DateTime('now');
        DB::table('authenticated_users')->where('expired_at', '<', $date_now)->delete();
        // ===========================
        if(Auth::attempt(['email' => $credential->email, 'password' => $credential->password])){
            $User = Auth::user();
            $token = $User->auth_token;
            // $CheckingToken = User::where('id', $User->id)->first();
            if ($User->auth_token==null) {
                $token = str_random(60);
                User::where('id', $User->id)->update(['auth_token' => $token]);
            }
            // $date = new DateTime('now');
            // $expired_date = $date->add(new DateInterval('PT2H'));
            // $login_date = new DateTime('now');
            // $data = [
            //     'session_id' => null,
            //     'user_id' => $User->id,
            //     'token' => $token,
            //     'expired_at' => $expired_date->format('Y-m-d H:i:s'),
            // ];
            // AuthenticatedUser::create($data);
            return ArrayToJson([
                'id' => $User->id,
                'is_active' => $User->is_active,
                'application_registered' => $User->application_registered,
                'token' => $token,
            ]);
        }
        return null;
    }

    public static function User($token, $App=null){
        $Selector = [
            'users.id',
            'users.name',
            'users.email',
            'users.photo as avatar',
            'users.is_active',
            'users.activation_token',
            'users.slug as user_slug',
            'users.auth_token as token',
            'users.phone',
            'users.facebook',
            'users.instagram',
            'users.twitter',
            'users.user_folder_name',
            'users.user_folder_path',
            'users.application_registered',
        ];
        $User = DB::table('users')
            ->select($Selector)
            ->where('auth_token', $token)
            ->first();
        
        if($User){
            $User->avatar = asset_url($User->avatar);
            $CourseUsers = CourseUser::where('user_id', $User->id)->get();
            $DataUserCourse = [];
            foreach($CourseUsers as $CourseUser){
                if($App==null){
                    $Course = Course::where(['id' => $CourseUser->course_id, 'status' => '1'])->first();
                }else{
                    $Course = Course::where(['id' => $CourseUser->course_id, 'status' => '1', 'id_instructor_group' => $App->id_instructor_group])->first();
                }
                
                if ($Course) {
                    $Course['image'] = asset_url($Course['image']);
                    array_push($DataUserCourse, json_decode(json_encode($Course), true));
                }
            }
            $DataToResponse = array_merge((array)$User, [
                'registered_courses' => $DataUserCourse,
            ]);
            return ArrayToJson($DataToResponse);
            //return $User;
        }
        return null;
    }

    public static function GetAuthToken($session_id){
        $AuthenticatedUser = AuthenticatedUser::where('session_id', $session_id)->first();
        return $AuthenticatedUser ? $AuthenticatedUser->token : null;
    }

    public static function GetDataUser($token){
        $User = User::where('auth_token', $token)->with('courses_users')->get();
        // $DataCourses = CourseUser::where('user_id', $User->id)->with('registered_courses')->get();
        return $User;
    }
}

?>