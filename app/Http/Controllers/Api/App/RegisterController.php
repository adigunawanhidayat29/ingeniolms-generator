<?php
namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use App\User;
use App\UserGroup;
use App\Instructor;
use App\Application;
use App\UserRegister;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use Illuminate\Http\Request;
use App\Mail\InstructorJoin;
use Validator;

use App\Http\Controllers\Api\App\AuthUser;

/**
* @SWG\Post(
*   path="/user/register",
*   summary="Register User",
*   operationId="Register",
*   tags={"Register"},
*   consumes={"application/json"},
*   produces={"application/json"},
*   @SWG\Parameter(
*     name="Authorization",
*     description="header must be set authorization",
*     required=true,
*     type="string",
*     in="header",
*       @SWG\Schema(ref="#/definitions/Header"),
*   ),
*   @SWG\Parameter(
*     name="Register",
*     description="Register user for client application",
*     required=true,
*     type="string",
*     in="body",
*       @SWG\Schema(ref="#/definitions/Register"),
*   ),
*   @SWG\Response(response=200, description="successful operation"),
* )
*
*/

class RegisterController extends Controller {
    public function index(Request $request){
        // Application Regitered
        $Application = Application::where('api_key', $request->header('Authorization'))->first();

        // Validating Input
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|min:8|max:20',
            'password' => 'required|string|min:6',
        ]);

        // Return error
        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Not Acceptable',
                'data' => $validator->errors(),
            ]);
        }
        $DataUser = User::where('email', $request->email);
        if (!$DataUser->first()) {
            $Register = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'slug' => str_slug($request->name),
                'activation_token' => str_random(40),
                'password' => bcrypt($request->password),
                'application_registered' => $Application->id_instructor_group,
            ]);

            $UserGroup = new UserGroup;
            $UserGroup->user_id = $Register['id'];
            $UserGroup->level_id = '3';
            $UserGroup->save();

            $UserRegister = new UserRegister;
            $UserRegister->user_id = $Register['id'];
            $UserRegister->app_register = $Application->id_instructor_group;
            $UserRegister->save();

            Mail::to($request->email)->send(new Register($Register));

            return response()->json([
                'error' => 0,
                'message' => 'Success',
                'data' => $Register,
            ]);

        }else{
            $UserRegister = new UserRegister;
            $UserRegister->user_id = $DataUser->first()->id;
            $UserRegister->app_register = $Application->id_instructor_group;
            $UserRegister->save();

            Mail::to($request->email)->send(new Register($DataUser->first()));

            return response()->json([
                'error' => 0,
                'message' => 'Success',
                'data' => $DataUser->first(),
            ]);
        }

        // check if register instruktor
        // if(isset($data['instructor'])){
        //     if($data['instructor'] == 'true'){

        //         // insert instructor
        //         $Instructor = new Instructor;
        //         $Instructor->user_id = $Register['id'];
        //         $Instructor->status = '0';
        //         $Instructor->save();
        //         // insert instructor

        //         $UserGroup = new UserGroup;
        //         $UserGroup->user_id = $Register['id'];
        //         $UserGroup->level_id = '2';
        //         $UserGroup->save();

        //         Mail::to(['tsauri@dataquest.co.id'])->send(new InstructorJoin($Instructor));
        //     }
        // }
        // check if register instruktor

    }

    public function without_activation(Request $request){
        // Application Regitered
        $Application = Application::where('api_key', $request->header('Authorization'))->first();

        // Validating Input
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|min:8|max:20',
            'password' => 'required|string|min:6',
        ]);

        // Return error
        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Not Acceptable',
                'data' => $validator->errors(),
            ]);
        }
        $DataUser = User::where('email', $request->email);
        if (!$DataUser->first()) {
            $Register = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'slug' => str_slug($request->name),
                'activation_token' => str_random(40),
                'password' => bcrypt($request->password),
                'application_registered' => $Application->id_instructor_group,
                'is_active' => '1',
                'auth_token' => str_random(60),
            ]);

            $UserGroup = new UserGroup;
            $UserGroup->user_id = $Register['id'];
            $UserGroup->level_id = '3';
            $UserGroup->save();

            $UserRegister = new UserRegister;
            $UserRegister->user_id = $Register['id'];
            $UserRegister->app_register = $Application->id_instructor_group;
            $UserRegister->save();

            Mail::to($request->email)->send(new Register($Register));

            return response()->json([
                'error' => 0,
                'message' => 'Success',
                'data' => $Register,
            ]);

        }else{
            $UserRegister = new UserRegister;
            $UserRegister->user_id = $DataUser->first()->id;
            $UserRegister->app_register = $Application->id_instructor_group;
            $UserRegister->save();

            //Mail::to($request->email)->send(new Register($DataUser->first()));

            return response()->json([
                'error' => 0,
                'message' => 'Success',
                'data' => $DataUser->first(),
            ]);
        }

    }
}

?>
