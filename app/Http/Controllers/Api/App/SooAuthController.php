<?php
namespace App\Http\Controllers\Api\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App\Instructor;
use App\Application;
use App\SsoAuth;
use Illuminate\Support\Facades\DB;
use Hash;
use Session;

class SooAuthController extends Controller {
    public function index(Request $request){
        if(!$request->get('hostname')){
            return response()->json([
                'error' => 406,
                'message' => 'Invalid',
            ]);
        }
        if(Auth::check()){
            return redirect($request->get('hostname') . '/auth/validate?token=' . Auth::user()->auth_token);
            // return response()->json([
            //     'error' => 0,
            //     'data' => Auth::user()
            // ]);
        }
        else{
            return redirect('/login');
        }
        
        
    }
}

?>