<?php
namespace App\Http\Controllers\Api\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App\UserGroup;
use App\Instructor;
use App\Application;
use Illuminate\Support\Facades\DB;
use Hash;
use Session;
use App\Http\Controllers\Api\App\AuthUser;

class SocialLoginController extends Controller {
    public function index(Request $request){
        // Application Regitered
        $Application = Application::where('api_key', $request->header('Authorization'))->first();

        // Validating Input
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'avatar' => 'required',
        ]);

        // If input not valid
        if($validator->fails()){
            return response()->json([
                'error' => 406,
                'message' => 'Not Acceptable',
                'data' => $validator->errors(),
            ]);
        }
        $SocialUserData = User::where('email', $request->email)->first();

        if (!$SocialUserData) {
            // If User not register as manual registration or social registration
            $User = new User;
            $User->email = $request->email;
            $User->name = $request->name;
            $User->slug = str_slug($request->name);
            $User->photo = $request->avatar;
            $User->password = bcrypt('password_default'); // Using default password
            $User->save();

            // Input User group and Level user
            $UserGroup = new UserGroup;
            $UserGroup->user_id = $User->id;
            $UserGroup->level_id = '3';
            $UserGroup->save();

            // Login and get User information
            $InstanceUser = AuthUser::login($User);
            $DataUser = AuthUser::User($InstanceUser->token);
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $DataUser,
            ]);
        }else{
            // If User Exist
            // Login and get User information
            $InstanceUser = AuthUser::login($SocialUserData);
            $DataUser = AuthUser::User($InstanceUser->token);
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $DataUser,
            ]);
        }
    }
}

?>