<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Api\App\AuthUser;

class UserController extends Controller{
    public function index(Request $request){
        $auth_token = $request->get('auth');
        $DataUser = AuthUser::GetDataUser($auth_token);
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $DataUser,
        ]);
    }
}


?>