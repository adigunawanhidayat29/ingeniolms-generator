<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Validator;
use Storage;
use App\Course;
use App\CourseLive;
use App\CourseBatch;
use App\Section;
use App\Content;
use App\CourseUser;
use App\Meeting;
use App\Progress;
use App\Discussion;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\Assignment;
use App\CourseAnnouncement;
use App\Rating;
use App\CourseProject;
use App\CourseBroadcast;
use App\ContentVideoQuiz;
use App\PlayerIdUser;
use App\MeetingSchedule;
use App\UserLibrary;
use App\User;
use App\ProgramCourse;
use Auth;
use Cart;
use Illuminate\Support\Facades\Mail;
use App\Mail\Course as MailCourse;
use Image;
use Plupload;
use FFMpeg;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Api\App\AuthUser;

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     schemes={"https"},
 *     host="api.ingenio.co.id",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Ingenio API Docs",
 *         description="Ingenio API Documentation",
 *         @SWG\Contact(
 *             email="adigunawanhidayat29@gmail.com"
 *         ),
 *     )
 * )
 */
/**
 *
 *    @SWG\Definition(
 * 			definition="Header",
 * 			required={"Authorization"},
 * 			@SWG\Property(property="Authorization", type="string"),
 * 		),
 *
 */
/**
 *
 *    @SWG\Definition(
 * 			definition="Body",
 * 			required={"email", "password"},
 * 			@SWG\Property(property="email", type="string"),
 *      @SWG\Property(property="password", type="string"),
 * 		),
 *
 */
/**
 *
 *    @SWG\Definition(
 * 			definition="Register",
 * 			required={"name", "email", "phone", "password"},
 *      @SWG\Property(property="name", type="string"),
 * 			@SWG\Property(property="email", type="string"),
 *      @SWG\Property(property="phone", type="string"),
 *      @SWG\Property(property="password", type="string"),
 * 		),
 *
 */
/**
 *
 *    @SWG\Definition(
 * 			definition="Enroll",
 * 			required={"course_id"},
 * 			@SWG\Property(property="course_id", type="string"),
 * 		),
 *
 */

/**
* @SWG\Get(
*   path="/course/all",
*   summary="List all course",
*   operationId="getcourse",
*   tags={"Courses"},
*   @SWG\Parameter(
*     name="Authorization",
*     description="e.g. i3jr29irji293ir313r",
*     required=true,
*     type="string",
*     in="header",
*       @SWG\Schema(ref="#/definitions/Header"),
*   ),
*   @SWG\Response(response=200, description="successful operation"),
* )
*
*/

/**
* @SWG\Get(
*      path="/course/detail/{slug}",
*      operationId="getDetailCourseBySlug",
*      tags={"Courses"},
*      summary="Get detail course information",
*      description="Returns course data",
*      @SWG\Parameter(
  *     name="Authorization",
  *     description="e.g. i3jr29irji293ir313r",
  *     required=true,
  *     type="string",
  *     in="header",
  *       @SWG\Schema(ref="#/definitions/Header"),
  *   ),
*      @SWG\Parameter(
*          name="slug",
*          description="e.g. slug-of-course",
*          required=true,
*          type="string",
*          in="path"
*      ),
 *   @SWG\Response(response=200, description="successful operation"),
 * )
 *
 */

 /**
* @SWG\Get(
*      path="/course/learn/{slug}",
*      operationId="learnCourseBySlug",
*      tags={"Courses"},
*      summary="Get registered course information",
*      description="Returns course data",
*      @SWG\Parameter(
*          name="Authorization",
*          description="e.g. i3jr29irji293ir313r",
*          required=true,
*          type="string",
*          in="header",
*   			 @SWG\Schema(ref="#/definitions/Header"),
*      ),
*      @SWG\Parameter(
*          name="slug",
*          description="e.g. slug-of-course",
*          required=true,
*          type="string",
*          in="path"
*      ),
*       @SWG\Parameter(
*          name="auth",
*          description="e.g. 6zwmWTP3EB4Hn3izcAkasfFSovmOJQbpGK8lsisTZoBStjeEcL61ZILHmTCwdOk",
*          required=true,
*          type="string",
*          in="query"
*      ),
 *   @SWG\Response(response=200, description="successful operation"),
 * )
 *
 */

/**
* @SWG\Get(
*      path="/course/learn/content/{slug}",
*      operationId="learnContentBySlug",
*      tags={"Courses"},
*      summary="Get content of course",
*      description="Returns course data",
*      @SWG\Parameter(
*          name="Authorization",
*          description="e.g. i3jr29irji293ir313r",
*          required=true,
*          type="string",
*          in="header",
*   			 @SWG\Schema(ref="#/definitions/Header"),
*      ),
*      @SWG\Parameter(
*          name="slug",
*          description="e.g. slug-of-course",
*          required=true,
*          type="string",
*          in="path"
*      ),
*       @SWG\Parameter(
*          name="auth",
*          description="e.g. 6zwmWTP3EB4Hn3izcAkasfFSovmOJQbpGK8lsisTZoBStjeEcL61ZILHmTCwdOk",
*          required=true,
*          type="string",
*          in="query"
*      ),
*       @SWG\Parameter(
*          name="content_id",
*          description="e.g. 99",
*          required=true,
*          type="string",
*          in="query"
*      ),
 *   @SWG\Response(response=200, description="successful operation"),
 * )
 *
 */
/**
* @SWG\Post(
*   path="/course/user/enroll",
*   summary="Enroll user to one course",
*   operationId="EnrollUserCourse",
*   tags={"Enroll"},
*   consumes={"application/json"},
*   produces={"application/json"},
*   @SWG\Parameter(
*     name="Authorization",
*     description="header must be set authorization",
*     required=true,
*     type="string",
*     in="header",
*       @SWG\Schema(ref="#/definitions/Header"),
*   ),
*   @SWG\Parameter(
*     name="auth",
*     description="Auth Token user",
*     required=true,
*     type="string",
*     in="query"
*   ),
*   @SWG\Parameter(
*     name="Body",
*     description="Course Id",
*     required=true,
*     type="string",
*     in="body",
*       @SWG\Schema(ref="#/definitions/Enroll"),
*   ),
*   @SWG\Response(response=200, description="successful operation"),
* )
*
*/

class CourseController extends Controller {
    public function index(Request $request){
      $q = $request->get('q');
      $perPage = 10;
      $page = $request->get('page') ? $request->get('page') : 1;
      $ClientApp = DB::table('applications')->where('api_key', $request->header('Authorization'))->first();
      if (!$ClientApp) {
        return response()->json([
          'error' => 406,
          'message' => 'Invalid API KEY'
        ]);
      }
      // Getting instructors
      $InstructorGroup = DB::table('instructor_groups')->where('id', $ClientApp->id_instructor_group)->first();

      // get course on program
      $ProgramCourses = ProgramCourse::get();
      $data_course_id = [];
      foreach($ProgramCourses as $ProgramCourse){
        $data_course_id[] = $ProgramCourse->course_id;
      }
      $data_course_id = implode(',', $data_course_id);
      $data_course_id = explode(',', $data_course_id);
      // get course on program

      // if ($ClientApp->id_instructor_group=='0') {
      //   if (!$q) {
      //     $courses = DB::table('courses')
      //         ->select('courses.*', 'users.name as instructor')
      //         ->join('users', 'users.id', '=', 'courses.id_author')
      //         ->where('courses.status', '1');

      //   }else{
      //     $courses = DB::table('courses')
      //       ->select('courses.*', 'users.name as instructor')
      //       ->join('users', 'users.id', '=', 'courses.id_author')
      //       ->where('courses.status', '1')
      //       ->where('courses.title', 'like', '%'.$q.'%');
      //   }
      // }else{
      //   if (!$q) {
      //     $courses = DB::table('courses')
      //         ->select('courses.*', 'users.name as instructor')
      //         ->join('users', 'users.id', '=', 'courses.id_author')
      //         ->where('courses.status', '1')
      //         ->where('id_instructor_group', $ClientApp->id_instructor_group);
      //   }else{
      //     $courses = DB::table('courses')
      //       ->select('courses.*', 'users.name as instructor')
      //       ->join('users', 'users.id', '=', 'courses.id_author')
      //       ->where('courses.status', '1')
      //       ->where('id_instructor_group', $ClientApp->id_instructor_group)
      //       ->where('courses.title', 'like', '%'.$q.'%');
      //   }
      // }

      if($ClientApp->id_instructor_group=='0'){
        $courses = Course::select('courses.*', 'users.name as instructor')
            ->join('users', 'users.id', '=', 'courses.id_author')
            ->where('courses.status', '1')
            ->where('courses.public', '1')
            ->where('courses.title', 'like', '%'.$q.'%')
            ->withCount('participants')
            ->with('sections', 'contents');
      }else{
        $courses = Course::select('courses.*', 'users.name as instructor')
            ->join('users', 'users.id', '=', 'courses.id_author')
            ->where('courses.status', '1')
            ->where('courses.public', '1')
            ->where('id_instructor_group', $ClientApp->id_instructor_group)
            ->where('courses.title', 'like', '%'.$q.'%')
            ->withCount('participants')
            ->with('sections', 'contents');
      }

      $CountCourse = $courses->count();
      if($q=='' || $q==null){
        $GetCourses = $courses->offset($perPage * ($page - 1))->limit($perPage)->get();
        $isFilterMode = false;
      }else{
        $GetCourses = $courses->limit(20)->get();
        $isFilterMode = true;
      }

      $DataCourse = [];
      foreach($GetCourses as $course){
        $course->image = asset_url($course->image);
        $video_duration = 0;
        foreach($course->contents()->where('contents.status', '1')->get() as $course_content){
          $video_duration += $course_content->video_duration;
        }
        $course->content_duration = floor($video_duration / 60);
        $course->total_content = $course->contents()->where('contents.status', '1')->get()->count();
        array_push($DataCourse, json_decode(json_encode($course), true));
      }
      $Instructors = [];
      if(!empty($InstructorGroup)){
        $instructor_ids = explode(",", $InstructorGroup->user_id);

        foreach($instructor_ids as $instructor_id){
          $instructor = User::where('id', $instructor_id)->first();
          array_push($Instructors, [
            'id' => $instructor->id,
            'name' => $instructor->name,
          ]);
        }
      }

      $availablePages = [];
      for($i=1;$i<=ceil($CountCourse / $perPage);$i++){
        array_push($availablePages, $i);
      }
      $DataResponse = [
        'courses' => $DataCourse,
        'meta' => [
          'isFilterMode' => $isFilterMode,
          'instructors' => $Instructors,
          'count_course' => $CountCourse,
          'perPage' => $perPage,
          'available_pages' => $availablePages,
          'page' => $page,
          'last_page' => 'page=' . ceil($CountCourse / $perPage),
          'prev' => $page > 1 ? 'page=' . ($page-1) : null,
          'next' => $page < ceil($CountCourse / $perPage) ? 'page=' . ($page+1) : null,
          'url_prev' => $page > 1 ? api_base_url('course/all?page='.($page-1)) : null,
          'url_next' => ($CountCourse - (($page - 1) * $perPage)) > 0 ? api_base_url('course/all?page='.($page+1)) : null,
        ]
      ];

      return response()->json([
        'error' => 0,
        'message' => 'Success',
        'data' => $DataResponse,
      ]);
    }

    public function my_course(Request $request){
      $auth_token = $request->get('auth');
      $user = AuthUser::User($auth_token);
      $q = $request->get('q');
      $perPage = 8;
      $page = $request->get('page') ? $request->get('page') : 1;
      $field = $request->get('field') ? $request->get('field') : 'id';
      $sort = $request->get('sort') ? $request->get('sort') : 'desc';
      $course_type = $request->get('type')==null ? 'all' : $request->get('type');
      $ClientApp = DB::table('applications')->where('api_key', $request->header('Authorization'))->first();
      if (!$ClientApp) {
        return response()->json([
          'error' => 406,
          'message' => 'Invalid API KEY'
        ]);
      }
      // get course on program
      $ProgramCourses = ProgramCourse::get();
      $data_course_id = [];
      foreach($ProgramCourses as $ProgramCourse){
        $data_course_id[] = $ProgramCourse->course_id;
      }
      $data_course_id = implode(',', $data_course_id);
      $data_course_id = explode(',', $data_course_id);
      // get course on program
      if($course_type=='all'){
        $courses = Course::select('courses.*','users.name as author')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('courses.status','1')
        ->where('courses.archive','0')
        // ->where('id_instructor_group', $ClientApp->id_instructor_group)
        ->where(['courses_users.user_id' => $user->id])
        // ->where('title', 'like', '%'.$q.'%')
        ->whereNotIn('courses.id',function($query) {
          $query->select('course_id')->from('course_lives');
        })
        ->orderBy('courses.'.$field , $sort)
        ->groupBy('courses.id');
      }else{
        $courses = Course::select('courses.*','users.name as author')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('courses.status','1')
        ->where('courses.archive','0')
        // ->where('id_instructor_group', $ClientApp->id_instructor_group)
        // ->where('courses.public', $course_type)
        ->where(['courses_users.user_id' => $user->id])
        // ->where('title', 'like', '%'.$q.'%')
        ->whereNotIn('courses.id',function($query) {
          $query->select('course_id')->from('course_lives');
        })
        ->orderBy('courses.'.$field , $sort)
        ->groupBy('courses.id');
      }


      $courses_count = $courses->get()->count();

      $courses_query =$courses->offset($perPage * ($page - 1))
        ->limit($perPage)
        ->get();

      // COURSE ARCHIVED
      $Course_archived = Course::select('courses.*', 'users.slug as user_slug', 'courses.model as model', 'courses.public','courses.slug as slug', 'courses.id as id')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('id_instructor_group', $ClientApp->id_instructor_group)
        ->where(['courses_users.user_id' => $user->id])
        ->where('archive','1')
        ->orderBy('courses.id', 'desc')
        ->get();
      // COURSE ARCHIVED

      $courses_data = [];
      foreach($courses_query as $course){
        $sections = Section::where('id_course', $course->id)->get();
        $num_contents = 0;
        foreach($sections as $section){
          $num_contents += count(Content::where('id_section', $section->id)->get());
        }
        //count percentage
        $num_progress = 0;
        $num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' => $user->id, 'status' => '1'])->get());
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = $percentage == 0 ? 1 : $percentage ;
        $percentage = 100 / $percentage;
        //count percentage

        array_push($courses_data, [
          'num_contents' => $num_contents,
          'slug' => $course->slug,
          'title' => $course->title,
          'image' => $course->image,
          'progress' => floor($percentage),
          'name' => $course->author,
          'id' => $course->id,
          'public' => $course->public,
          'model' => $course->model,
        ]);
      }

      $data = [
        'courses_datas' => $courses_data,
        'courses_datas_count' => $courses_count,
        'meta'=> [
          'hasNext' => $page < ceil($courses_count / $perPage) ? true : false,
        ],
        'courses' => $courses,
        'course_archives' => $Course_archived,
      ];

      return response()->json([
        'error' => 0,
        'message' => 'success',
        'data' => $data,
      ]);
    }

    public function my_course_(Request $request){
      $auth_token = $request->get('auth');
      $user = AuthUser::User($auth_token);
      $q = $request->get('q');
      $perPage = 8;
      $page = $request->get('page') ? $request->get('page') : 1;
      $field = $request->get('field') ? $request->get('field') : 'id';
      $sort = $request->get('sort') ? $request->get('sort') : 'desc';
      $course_type = $request->get('type')==null ? 'all' : $request->get('type');
      $ClientApp = DB::table('applications')->where('api_key', $request->header('Authorization'))->first();
      if (!$ClientApp) {
        return response()->json([
          'error' => 406,
          'message' => 'Invalid API KEY'
        ]);
      }
      // get course on program
      $ProgramCourses = ProgramCourse::get();
      $data_course_id = [];
      foreach($ProgramCourses as $ProgramCourse){
        $data_course_id[] = $ProgramCourse->course_id;
      }
      $data_course_id = implode(',', $data_course_id);
      $data_course_id = explode(',', $data_course_id);
      // get course on program
      if($course_type=='all'){
        $courses = Course::select('courses.*','users.name as author')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('courses.status','1')
        ->where('courses.archive','0')
        ->where('id_instructor_group', $ClientApp->id_instructor_group)
        ->where(['courses_users.user_id' => $user->id])
        ->where('title', 'like', '%'.$q.'%')
        ->whereNotIn('courses.id',function($query) {
          $query->select('course_id')->from('course_lives');
        })
        ->orderBy('courses.'.$field , $sort)
        ->groupBy('courses.id');
      }else{
        $courses = Course::select('courses.*','users.name as author')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('courses.status','1')
        ->where('courses.archive','0')
        ->where('id_instructor_group', $ClientApp->id_instructor_group)
        ->where('courses.public', $course_type)
        ->where(['courses_users.user_id' => $user->id])
        ->where('title', 'like', '%'.$q.'%')
        ->whereNotIn('courses.id',function($query) {
          $query->select('course_id')->from('course_lives');
        })
        ->orderBy('courses.'.$field , $sort)
        ->groupBy('courses.id');
      }


      $courses_count = $courses->get()->count();

      $courses_query =$courses->offset($perPage * ($page - 1))
        ->limit($perPage)
        ->get();

      // COURSE ARCHIVED
      $Course_archived = Course::select('courses.*', 'users.slug as user_slug', 'courses.model as model', 'courses.public','courses.slug as slug', 'courses.id as id')
        ->join('courses_users','courses_users.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->where('id_instructor_group', $ClientApp->id_instructor_group)
        ->where(['courses_users.user_id' => $user->id])
        ->where('archive','1')
        ->orderBy('courses.id', 'desc')
        ->get();
      // COURSE ARCHIVED

      $courses_data = [];
      foreach($courses_query as $course){
        $sections = Section::where('id_course', $course->id)->get();
        $num_contents = 0;
        foreach($sections as $section){
          $num_contents += count(Content::where('id_section', $section->id)->get());
        }
        //count percentage
        $num_progress = 0;
        $num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' => $user->id, 'status' => '1'])->get());
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = $percentage == 0 ? 1 : $percentage ;
        $percentage = 100 / $percentage;
        //count percentage

        array_push($courses_data, [
          'num_contents' => $num_contents,
          'slug' => $course->slug,
          'title' => $course->title,
          'image' => $course->image,
          'progress' => floor($percentage),
          'name' => $course->author,
          'id' => $course->id,
          'public' => $course->public,
          'model' => $course->model,
        ]);
      }

      $data = [
        'courses_datas' => $courses_data,
        'courses_datas_count' => $courses_count,
        'meta'=> [
          'hasNext' => $page < ceil($courses_count / $perPage) ? true : false,
        ],
        'courses' => $courses,
        'course_archives' => $Course_archived,
      ];

      return response()->json([
        'error' => 0,
        'message' => 'success',
        'data' => $data,
      ]);
    }

    public function detail($slug){
      $AuthorCourses = [];
      $totalStudents = 0;
      $authorTotalRating = 0;
      $authorCountRating = 0;
      $authorAverageRating = 0;

      $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo', 'users.slug as author_slug', 'users.short_description as author_short_description', 'users.description as author_description', 'courses.id as id')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->join('categories', 'categories.id', '=', 'courses.id_category')
        ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
        ->where('courses.slug', $slug)
        ->first();

        if($courses){
          $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))
            ->where('course_id', $courses->id)
            ->first();

          if($Rating->count_rating != 0){
            $Rate = $Rating->rating / $Rating->count_rating;
          }else{
            $Rate = 0;
          }

          $GetAuthorCourses = DB::table('courses')->select('courses.*', 'courses.id as course_id', 'users.*', 'users.slug as user_slug', 'courses.slug as slug')
              ->where(['status' => '1', 'id_author' => $courses->id_author])
              ->join('users','users.id','=','courses.id_author')
              ->orderBy('courses.id', 'desc')
              ->get();

          $iter = 0;
          foreach($GetAuthorCourses as $getCourse){
            $RatingA = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $getCourse->course_id)->first();
            if($RatingA->count_rating != 0){
              $RateA = $RatingA->rating / $RatingA->count_rating;
            }else{
              $RateA = 0;
            }
            $array_ratingA = array('course_rating' => $RateA, 'course_count_rating' => $RatingA->count_rating);
            $contents_mergeA = array_merge((array)$GetAuthorCourses[$iter], $array_ratingA);
            array_push($AuthorCourses, $contents_mergeA);
            $iter++;
          }

          $totalCourse = count($GetAuthorCourses);
          foreach($GetAuthorCourses as $AuthorCourse){
            $totalStudents = $totalStudents + count(DB::table('courses_users')->where(['course_id' => $AuthorCourse->course_id])->get());
            $AuthorRatings = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $AuthorCourse->course_id)->first();
            $authorTotalRating = $authorTotalRating + $AuthorRatings->rating;
            $authorCountRating = $authorCountRating + $AuthorRatings->count_rating;
          }
          if ($authorCountRating != 0) {
            $authorAverageRating = $authorTotalRating / $authorCountRating;
          }

        // check course on cart
        // $CourseOnCart = false;
        // foreach(Cart::content() as $cart){
        //   if($cart->id == $courses->id){
        //     $CourseOnCart = true;
        //   }
        // }
        // check course on cart

        // check logged in or not
        // $user = Auth::user();
        // if($user){
        //   $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id])->first();
        // }else{
        //   $course_user = [];
        // }
        // check logged in or not

        // check user joined course or not
        $content_previews_data = [];
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence', 'asc')->get();
        $video_duration = 0;
        $total_contents = 0;
        foreach($sections as $section){
          $contents = DB::table('contents')->where('id_section', $section->id)->where('status', '1')->orderBy('sequence', 'asc')->get();
          $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();

          foreach($contents as $content){
            $video_duration += $content->video_duration;
            $total_contents += 1;
          }

          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
          ]);

          $content_previews = DB::table('contents')->where('id_section', $section->id)->where('preview', '1')->orderBy('sequence', 'asc')->get();
          foreach($content_previews as $index => $content_preview){
            array_push($content_previews_data, [
              'id' => $content_preview->id,
              'title' => $content_preview->title,
              'type_content' => $content_preview->type_content,
            ]);
          }
        }

        $courses->total_video_duration = floor($video_duration / 60);
        $courses->total_contents = $total_contents;
        $courses->total_students = DB::table('courses_users')->where('course_id', $courses->id)->count();
        // dd($content_previews_data);

        $data = [
          'course' => $courses,
          'sections' => $section_data,
          'content_previews' => $content_previews_data,
          'average_rating' => round($authorAverageRating, 2),
          'total_courses' => $totalCourse,
          'total_students' => $totalStudents,
          'rate' => $Rate,
          'count' => ($Rating->count_rating != 0 ? $Rating->count_rating : 0),
        ];
        return response()->json([
          'error' => 0,
          'message' => 'Success',
          'data' => $data,
        ]);
        // check user joined course or not
      }else{
        return response()->json([
          'error' => 404,
          'message' => 'Not Found',
        ]);
      }
    }

    public function joinPrivateCourse(Request $request){
      $Course = Course::where(['public' => '0', 'password' => $request->password])->first();
      if($Course){
        if($Course->archive == '0'){
          $CourseUser = CourseUser::where('course_id', $Course->id)->get()->count();
          if($CourseUser < 100000){ // unlimited
            return response()->json([
              'error' => 0,
              'message' => 'success',
              'data' => $Course,
            ]);
          }else{
            return response()->json([
              'error' => 400,
              'message' => 'Kelas sudah penuh',
            ]);
          }
        }else{
          return response()->json([
            'error' => 400,
            'message' => 'Kelas sudah tidak aktif',
          ]);
        }
      }else{
        return response()->json([
          'error' => 400,
          'message' => 'Password atau kode yang Anda masukan salah',
        ]);
      }
    }

    public function checkingEnrolled(Request $request, $course_id){
      $auth_token = $request->get('auth');
      $user = AuthUser::User($auth_token);
      $CourseUser = CourseUser::where(['course_id' => $course_id, 'user_id' => $user->id])->first();
      if ($CourseUser) {
        $Course = Course::where('id', $course_id)->first();
        return response()->json([
          'error' => 0,
          'message' => 'success',
          'data' => [
            'isEnrolled' => true,
            'slug' => $Course->slug,
          ]
        ]);
      }else{
        return response()->json([
          'error' => 0,
          'message' => 'success',
          'data' => [
            'isEnrolled' => false,
            'slug' => null,
          ]
        ]);
      }
    }

    public function enroll(Request $request){
      $auth_token = $request->get('auth');

      $user = AuthUser::User($auth_token);

      $Course = CourseUser::where(['course_id' => $request->course_id, 'user_id' => $user->id])->first();
      if ($Course) {
        return response()->json([
          'error' => 406,
          'message' => 'Course Already Enrolled'
        ]);
      }

      $PasswordCourse = Course::where('id', $request->course_id)->first();
      if ($PasswordCourse->password!=null) {
        if($PasswordCourse->price != 0){
          if (!$request->password || $request->password!=$PasswordCourse->password) {
            return response()->json([
              'error' => 403,
              'message' => 'Invalid Password, Please try again',
            ]);
          }
        }

      }else{
        if($PasswordCourse->price != 0){
          return response()->json([
            'error' => 403,
            'message' => 'You must buy this course first',
          ]);
        }
      }

      $CourseUser = new CourseUser;
      $CourseUser->user_id = $user->id;
      $CourseUser->course_id = $request->course_id;
      $CourseUser->save();



      return response()->json([
        'error' => 0,
        'message' => 'Success',
      ]);

      // Mail::to($user->email)->send(new MailCourse($CourseUser));
      // if($request->ajax()){
      //   return "TRUE";
      // }else{
      //   return redirect('course/learn/'.$request->slug);
      // }
    }

    public function learn(Request $request, $slug){ // Need Authentication
      $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo', 'users.short_description as author_short_description', 'users.slug as author_slug', 'courses.id as id')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->join('categories', 'categories.id', '=', 'courses.id_category')
        ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
        ->where('courses.slug', $slug)->first();
      $courses->is_live = is_liveCourse($courses->id);
      $perPage = 5;
      $page = $request->get('page') ? $request->get('page') : 1;
      $auth_token = $request->get('auth');
      if ($auth_token==null) {
        return response()->json([
          'error' => 403,
          'message' => 'Authentication Required'
        ]);
      }
      $user = AuthUser::User($auth_token);
      $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id]);
      if($course_user->first()){
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

        $num_contents = 0;
        foreach($sections as $section){
          $contents_get = DB::table('contents')
            ->where('status', '1')
            ->where('id_section', $section->id)
            ->orderBy('sequence', 'asc')
            ->get();
          if (!$contents_get) {
            continue;
          }
          $contents = [];
          for($i=0; $i < count($contents_get) ; $i++){
            $count_progress = DB::table('progresses')->where(['content_id'=>$contents_get[$i]->id, 'user_id' => $user->id, 'status' => '1'])->get();
            if ($count_progress) {
              $content_progress = count($count_progress);
            }else{
              $content_progress = 0;
            }
            $array_progress = array('isComplete' => $content_progress);
            $contents_merge = array_merge((array)$contents_get[$i], $array_progress);
            array_push($contents, $contents_merge);
          }

          $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();
          $assignments = DB::table('assignments')->where(['id_section' => $section->id, 'status' => '1'])->get();
          $num_contents += count($contents);

          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
            'section_assignments' => $assignments,
          ]);
        }

        $meeting = Meeting::where('course_id', $courses->id)->first();
        $discussions = [];
        $get_discussions = Discussion::select('discussions.id as id','discussions.course_id','discussions.body','discussions.created_at','users.name','users.photo')
                      ->where(['course_id'=>$courses->id, 'status' => '1'])
                      ->join('users','users.id','=','discussions.user_id')
                      ->orderby('discussions.created_at','desc');
        foreach($get_discussions->offset($perPage * ($page - 1))->limit($perPage)->get() as $d){
          $d->count_answers = DB::table('discussions_answers')->where('discussion_id', $d->id)->count();
          array_push($discussions, $d);
        }
        $answerCount = $get_discussions->count();
        $get_discussions_data = [
          'data' => $discussions,
          'meta' => [
            'hasNext'  => $page < ceil($answerCount / $perPage) ? true : false,
          ]
        ];
        $announcements = CourseAnnouncement::select('course_announcements.*','users.photo as photo','users.name')
          ->where('course_id', $courses->id)
          ->join('courses','course_announcements.course_id','=','courses.id')
          ->join('users','users.id','=','courses.id_author')
          ->orderby('course_announcements.created_at','desc')->get();

        $announcements_data = [];
        foreach($announcements as $announcement){
          $announcement_comments = DB::table('course_announcement_comments')
            ->join('users','users.id','=','course_announcement_comments.user_id')
            ->where('course_announcement_id', $announcement->id)->get();
          array_push($announcements_data, [
            'id' => $announcement->id,
            'course_id' => $announcement->course_id,
            'title' => $announcement->title,
            'description' => $announcement->description,
            'status' => $announcement->status,
            'created_at' => $announcement->created_at,
            'photo' => $announcement->photo,
            'name' => $announcement->name,
            'announcement_comments' => $announcement_comments,
          ]);
        }
        // dd($announcements_data);

        $Progress = Progress::where(['course_id'=>$courses->id, 'user_id' => $user->id, 'status' => '1']);

        //count percentage
        $num_progress = 0;
        $num_progress = $Progress->count();
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = $percentage == 0 ? 1 : $percentage ;
        $percentage = 100 / $percentage;
        //count percentage

        //count rating
        $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $courses->id)->first();
        if($Rating->count_rating != 0){
          $Rate = $Rating->rating / $Rating->count_rating;
        }else{
          $Rate = 0;
        }
        //count rating

        // get projects
        $Projects = CourseProject::where('course_id', $courses->id)->get();
        // get projects

        // get Broadcast
        $Broadcast = CourseBroadcast::where(['course_id' => $courses->id ,'status' => '1'])->orderBy('id', 'desc')->first();
        // get Broadcast

        // get meeting_schedules
        $meeting_schedules = MeetingSchedule::where('course_id', $courses->id)->orderBy('date', 'asc')->get();
        // get meeting_schedules

        // get content next learn
        $NextContent = Content::select('contents.id as id')->whereNotIn('contents.id', function($query) use ($courses, $user){
            $query->select('progresses.content_id')
            ->from('progresses')
            ->where(['progresses.status' => '1', 'progresses.course_id' => $courses->id, 'progresses.user_id' => $user->id]);
        })
        ->join('sections', 'sections.id', '=', 'contents.id_section')
        ->where('sections.id_course', $courses->id)
        ->where('contents.status', '1')
        ->orderBy('sections.sequence','asc')
        ->orderBy('sections.updated_at','desc')
        ->orderBy('contents.id', 'asc')
        ->first();
        // dd($NextContent);

        $FirstContent = $contents = Content::select('contents.id as id')
          ->join('sections', 'sections.id', '=', 'contents.id_section')
          ->where('sections.id_course', '=', $courses->id)
          ->where('contents.status', '1')
          ->orderBy('sections.sequence','asc')
          ->orderBy('sections.updated_at','desc')
          ->orderBy('contents.id', 'asc')
          ->first();
          // dd($FirstContent);
        // get content next learn

        $data = [
          'course' => $courses,
          'sections' => $section_data,
          'meeting' => $meeting,
          'meeting_schedules' => $meeting_schedules,
          'discussions' => $get_discussions_data,
          'announcements' => $announcements_data,
          'projects' => $Projects,
          'num_contents' => $num_contents,
          'num_progress' => $num_progress,
          'percentage' => $percentage,
          'rate' => $Rate,
          'Broadcast' => $Broadcast,
          'NextContent' => $NextContent == null ? $FirstContent : $NextContent,
        ];
        // return view('course.learn', $data);
        return response()->json([
          'error' => 0,
          'message' => 'Success',
          'data' => $data,
        ]);
      }else{
        // return redirect('course/'.$slug);
        return response()->json([
          'error' => 406,
          'message' => 'User not registered to this course, please register first',
        ]);
      }
    }

    public function learn_content(Request $request, $slug){
      if(!$request->get('content_id')){
        return response()->json([
          'error' => 406,
          'message' => "Insufficient parameter 'content_id'"
        ]);
      }
      $content_id = $request->get('content_id');
      $type = $request->get('type') ? $request->get('type') : 'content';
      $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->join('categories', 'categories.id', '=', 'courses.id_category')
        ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
        ->where('courses.slug', $slug)->first();

      $auth_token = $request->get('auth');
      if ($auth_token==null) {
        return response()->json([
          'error' => 403,
          'message' => 'Authentication Required'
        ]);
      }
      $user = AuthUser::User($auth_token);
      $course_user = CourseUser::where(['course_id' => $courses->id, 'user_id' => $user->id]); // check user course
      if($course_user->first()){
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

        $num_contents = 0;
        $meta_content = [];
        $availableContentId = [];
        foreach($sections as $section){
          $contents = DB::table('contents')
            ->where('id_section', $section->id)
            ->where('status', '1')
            ->orderBy('sequence', 'asc')
            ->get();

          foreach($contents as $content){
            $count_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => $user->id, 'status' => '1'])->get();
            if ($count_progress) {
              $content_progress = count($count_progress);
            }else{
              $content_progress = 0;
            }
            $content->isComplete = $content_progress;
            array_push($availableContentId, [
              'type' => 'content',
              'id' => $content->id,
            ]);
          }


          $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();
          foreach($quizzes as $quiz){
            array_push($availableContentId, [
              'type' => 'quiz',
              'id' => $quiz->id,
            ]);
          }
          $assignments = DB::table('assignments')->where(['id_section' => $section->id, 'status' => '1'])->get();
          foreach($assignments as $assignment){
            array_push($availableContentId, [
              'type' => 'assignment',
              'id' => $assignment->id,
            ]);
          }
          $num_contents += count($contents);

          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
            'section_assignments' => $assignments,
          ]);
        }
        if($type=='content'){
          $Content = Content::where('id', $content_id)->first();
        }else if($type=='quiz'){
          $Content = DB::table('quizzes')->where(['id' => $content_id])->first();
        }else if($type=='assignment'){
          $Content = DB::table('assignments')->where(['id' => $content_id])->first();
        }else{
          $Content =[];
        }

        //$meta_content['current_content_id'] = $content_id;
        //$current_key = array_search($content_id, $availableContentId, true);
        $long_way_int = 0;
        foreach($availableContentId as $available_content_id){ // find current
          if($available_content_id['id'] == $content_id){
            $current_key = $long_way_int;
          }
          $long_way_int++;
        }
        $meta_content['current_id'] = $availableContentId[$current_key];
        $meta_content['next_id'] = $current_key == count($availableContentId)-1 ? false : $availableContentId[$current_key+1];
        $meta_content['prev_id'] = $current_key==0 ? false : $availableContentId[$current_key-1];
        $meta_content['available_content_id'] = $availableContentId;


        // get file video google drive
        // $data = getVideoGoogleDrive($Content->full_path_file_resize_720);
        // $data_360 = getVideoGoogleDrive($Content->full_path_file_resize);
        // $client_info = getClientInfoGoogleDrive();
        // get file video google drive

        // check content on progress
        if($type=='content'){
          if($Content->type_content != 'video'){ // jika konten bukan video
            if(!Progress::where(['content_id' => $content_id, 'user_id' => $user->id])->first()){
              // insert progress
              $Progress = new Progress;
              $Progress->content_id = $Content->id;
              $Progress->course_id = $courses->id;
              $Progress->user_id = $user->id;
              $Progress->status = '1';
              $Progress->save();
              // insert progress
            }else{
              $Progress = Progress::where(['content_id' => $content_id, 'user_id' => $user->id])->first();
              $Progress->status = '1';
              $Progress->save();
            }
          }
        }

        // check content on progress

        /*
        "id": 414,
        "id_section": 200,
        "title": "Penutup Materi",
        "description": null,
        "type_content": "video",
        "name_file": "vid-20180808-103109mp4-1659.mp4",
        "path_file": "15dU-9thW2Y5gYv9ZftIiKxx1cuSyMh19",
        "full_path_file": "15dU-9thW2Y5gYv9ZftIiKxx1cuSyMh19",
        "sequence": 0,
        "created_at": "2018-08-08 15:14:06",
        "updated_at": "2018-08-08 15:18:56",
        "full_path_file_resize": "10cvu4CdKIx6XgsyTbf_lShkrcv9NHBTh",
        "full_path_file_resize_720": "1jg1P4N8Hp4T86zGzrxPTjgiEpngkcNLX",
        "full_path_original": "/uploads/videos/aksara-penulis-pemula-2018-08-03-752/vid-20180808-103109mp4-1659.mp4",
        "full_path_original_resize": "/uploads/videos/aksara-penulis-pemula-2018-08-03-752/360p_vid-20180808-103109mp4-1659.mp4",
        "full_path_original_resize_720": "/uploads/videos/aksara-penulis-pemula-2018-08-03-752/720p_vid-20180808-103109mp4-1659.mp4",
        "file_status": "2",
        "file_size": 9437184,
        "video_duration": "52.117000",
        "status": "1",
        "preview": "0"

        */
        if($type=='content'){
          $content_data = [];
          if ($Content->type_content=='video') {
            // Checking video if resized is exist

            $content_data = [
              'type_content' => 'video',
              'uri' => $Content->full_path_file_resize!=null ? playContent($Content->full_path_file_resize)['uri'] : null,
              'v_360' => $Content->full_path_file_resize!=null ? playContent($Content->full_path_file_resize)['uri'] : null,
              'v_720' => $Content->full_path_file_resize_720!=null ? playContent($Content->full_path_file_resize_720)['uri'] : null,
              'v_original' => $Content->full_path_original!=null ? server_assets() . $Content->full_path_original : null,
            ];
          }else if ($Content->type_content=='file') {
            $content_data = [
              'type_content' => 'file',
              'text' => $Content->description==null ? '' : $Content->description,
              'attachment' => $Content->full_path_file,
            ];
          } else if($Content->type_content=='url-video'){
            $content_data = [
              'type_content' => 'url-video',
              'text' => $Content->description==null ? '' : $Content->description,
              'attachment' => $Content->full_path_file,
            ];
          } else {
            $content_data = [
              'type_content' => 'text',
              'text' => $Content->description==null ? '' : $Content->description,
            ];
          }
        }
        if($type=='content'){
          $data_content = [
            'id' => $Content->id,
            'id_section' => $Content->id_section,
            'title' => $Content->title,
            'description' => $Content->description,
            'type_content' => $Content->type_content,
            'sequence' => $Content->sequence,
            'created_at' => $Content->created_at,
            'updated_at' => $Content->updated_at,
            'file_size' => $Content->file_size,
            'video_duration' => $Content->video_duration,
            'status' => $Content->status,
            'preview' => $Content->preview,
            'content_data' => $content_data,
            'meta_content' => $meta_content,
          ];
        }else{
          $data_content = $Content;
          $data_content->meta_content = $meta_content;
        }

        $data = [
          'course' => [
            'slug' => $slug,
          ],
          'sections' => $section_data,
          'content' => $data_content,
          'content_video_quizes' => ContentVideoQuiz::where('content_id', $content_id)->with('content_video_quiz_answers')->get(),
        ];

        return response()->json([
          'error' => 0,
          'message' => 'Success',
          'data' => $data,
        ]);
      }else{
        return response()->json([
          'error' => 406,
          'message' => 'User not registered to this course, please register first',
        ]);
      }
    }

    public function completion_finish(Request $request){
      // Checking the user using token
      $auth_token = $request->get('auth');
      $user = AuthUser::User($auth_token);

      $Progress = Progress::where(['content_id' => $request->content_id, 'user_id' => $user->id])->first();
      if($Progress){
        $Progress->status = '1';
        $Progress->save();
      }else{
        $NewProgress = new Progress;
        $NewProgress->course_id = $request->course_id;
        $NewProgress->content_id = $request->content_id;
        $NewProgress->status = '1';
        $NewProgress->user_id = $user->id;
        $NewProgress->save();
      }

      return response()->json([
        'error' => 0,
        'message' => 'success',
      ]);
    }
}




?>
