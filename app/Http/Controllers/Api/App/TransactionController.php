<?php

namespace App\Http\Controllers\Api\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AffiliateTransaction;
use App\AffiliateUser;
use App\AffiliateSetting;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\Promotion;
use App\PromotionUser;
use Auth;
use Cart;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionInformation;
use App\Http\Controllers\Api\App\AuthUser;


class TransactionController extends Controller {
    public function process_transaction(Request $request){
        $auth_token = $request->get('auth');
        $user = AuthUser::User($auth_token);
        $invoice = rand(100000000,999999999);
        $unique_number = rand(100,999);

        /*
            'course_id' => Course / Program id,
            'name' => Course title,
            'qty' => quatity,
            'price' => Price one course or program,
            'options' => [
                'image' => course / program image,
                'affiliate' => $affiliate,
                'type' => course,
            ]
        */

        if($request->get('aff')){
            $affiliate = '?aff='.$request->get('aff');
            $AffiliateSetting = AffiliateSetting::where('status', '1')->orderBy('id', 'desc')->first();
            $AffiliateUser = AffiliateUser::where('url', $affiliate)->first();
            $AffiliateTransaction = new AffiliateTransaction;
            $AffiliateTransaction->affiliate_id = $AffiliateUser->id;
            $AffiliateTransaction->user_id = $user->id;
            $AffiliateTransaction->invoice = $invoice;
            $AffiliateTransaction->status = '0';
            $AffiliateTransaction->commission = $AffiliateSetting->commission;
            $AffiliateTransaction->save();
        }

        $carts = json_decode(json_encode((array)$request->carts), false);

        $subtotal = 0;
        foreach($carts as $cart){
            $subtotal += $cart->price;
            $TransactionDetail = new TransactionDetail;
            $TransactionDetail->invoice = $invoice;
            if($cart->options->type == 'course'){
                $TransactionDetail->course_id = $cart->course_id;
            }else{
                $TransactionDetail->program_id = $cart->course_id;
            }
            $TransactionDetail->total = $cart->price;
            $TransactionDetail->save();
        }
        if($request->promotion!=false){
            $Promotion = json_decode(json_encode((array)$request->promotion), false);
            $PromotionUser = new PromotionUser;
            $PromotionUser->promotion_id = $Promotion->id;
            $PromotionUser->user_id = $user->id;
            $PromotionUser->invoice = $invoice;
            $PromotionUser->save();

            $discount_price = ($subtotal * $Promotion->discount) / 100;

            $Transaction = new Transaction;
            $Transaction->invoice = $invoice;
            $Transaction->user_id = $user->id;
            $Transaction->method = $request->payment_method;
            $Transaction->total = ($subtotal - $discount_price);
            $Transaction->subtotal = ($subtotal - $discount_price) + $unique_number;
            $Transaction->unique_number = $unique_number;
            $Transaction->promotion = '1';
            $Transaction->promotion_id = $Promotion->id;
            $Transaction->discount_code = $Promotion->discount_code;
            $Transaction->status = $Promotion->discount == 100 ? '1' : '0';
            $Transaction->save();

            if($Promotion->discount == 100){

              foreach($carts as $cart){
                if($cart->options->type == 'course'){
                  $TransactionDetail->course_id = $cart->course_id;
                  $CourseUser = new CourseUser;
                  $CourseUser->user_id = $user->id;
                  $CourseUser->course_id = $cart->course_id;
                  $CourseUser->save();
                }
              }

              return response()->json([
                  'message' => 'success',
                  'data' => 'Course berhasil di enroll',
                  'checkout_detail' => Transaction::where(['invoice' => $invoice])->get(),
                  'data' => $Transaction,
              ], 200);
            }

        }else{
            $Transaction = new Transaction;
            $Transaction->invoice = $invoice;
            $Transaction->user_id = $user->id;
            $Transaction->method = $request->payment_method;
            $Transaction->total = $subtotal;
            $Transaction->subtotal = $subtotal + $unique_number;
            $Transaction->unique_number = $unique_number;
            $Transaction->promotion = '0';
            $Transaction->affiliate_id = $request->get('aff') ? $AffiliateUser->id : NULL;
            $Transaction->status = '0';
            $Transaction->save();
        }


        Mail::to($user->email)->send(new TransactionInformation($Transaction));
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $Transaction,
        ]);
    }

    public function transaction_information($invoice){
        $Transaction = Transaction::where('invoice', $invoice);
        $TransactionDetail = TransactionDetail::where('invoice', $invoice)->join('courses','courses.id','=','transactions_details.course_id');
        $data = [
            'transaction' => $Transaction->first(),
            'transactions_details' => $TransactionDetail->get(),
        ];
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $data,
        ]);

    }

    public function confirmation_transaction(Request $request){
        $invoice = $request->invoice;
        $Transaction = Transaction::where('invoice', $invoice)->first();

        if ($Transaction) {
            $TransactionConfirmation = new TransactionConfirmation;

            if($request->file('file')){
                $destinationPath = asset_path('uploads/confirmations/'); // upload path
                $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
                $file = $invoice.'-'.rand(1111,9999).'.'.$extension; // renameing image
                $request->file('file')->move($destinationPath, $file); // uploading file to given path
                $TransactionConfirmation->file = '/uploads/confirmations/'.$file;
            }

            // Store Confirmation to Transaction Confirmation Table
            $TransactionConfirmation->invoice = $invoice;
            $TransactionConfirmation->account_name = $request->account_name;
            $TransactionConfirmation->account_number = $request->account_number;
            $TransactionConfirmation->bank_name = $request->bank_name;
            $TransactionConfirmation->save();
            return response()->json([
                'error' => 0,
                'message' => 'success',
                'data' => $TransactionConfirmation
            ]);
        }

        // Invoice Invalid or Not Found
        return response()->json([
            'error' => 404,
            'message' => 'Transaction Not Found'
        ]);
    }

    public function history_transaction(Request $request){
        $auth_token = $request->get('auth');
        $user = AuthUser::User($auth_token);

        $Transactions = Transaction::where('user_id', $user->id)->with('detail_transactions')->get();
        return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $Transactions,
        ]);
    }

    public function promotion(Request $request){
        $auth_token = $request->get('auth');
        $user = AuthUser::User($auth_token);

        $Promotion = Promotion::
            where([
                'discount_code' => $request->discount_code,
                'status' => '1',
            ])
            ->where('start_date', '<=', date("Y-m-d H:i:s"))
            ->where('end_date', '>=', date("Y-m-d H:i:s"))
            ->first();

        if($Promotion){
            $PromotionUser = PromotionUser::where(['promotion_users.user_id' => $user->id, 'promotion_users.promotion_id' => $Promotion->id, 'transactions.status' => '1'])
                ->join('transactions', 'transactions.invoice', '=', 'promotion_users.invoice')
                ->first();
            if($PromotionUser){
                //Session::flash('error', 'Kode sudah dipakai');
                return response()->json([
                    'error' => 406,
                    'message' => 'Promotion code already taken'
                ]);
            }else{
                //Session::put('promotion', $Promotion);
                return response()->json([
                    'error' => 0,
                    'message' => 'success',
                    'data' => $Promotion
                ]);
            }
        }else{
            //Session::flash('error', 'Kode tidak ditemukan');
            return response()->json([
                'error' => 406,
                'message' => 'Promotion code not found'
            ]);
        }
    }
}

?>
