<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\UserGroup;
use App\Instructor;
use RegistersUsers;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\InstructorJoin;
use App\Http\Controllers\Controller;

class InstructorController extends Controller {

    public function join(Request $request){
        $validator = Validator::make($request->all(), [
			'user_id' => 'required',
        ]);
        $User = User::where(['id' => $request->user_id ])->first();
        if (Instructor::where(['user_id' => $request->user_id ])->first()) {
            return response()->json([
                'message' => 'Anda sudah mengajukan menjadi pengajar',
                'error' => 'not created'
            ], 200);
        }else{
            $Instructor = new Instructor;
            $Instructor->user_id = $request->user_id;
            $Instructor->status = '0';
            $Instructor->save();

            Mail::to([$User->email, 'tsauri@dataquest.co.id'])->send(new InstructorJoin($Instructor));

            return response()->json([
                'message' => 'Berhasil',
                'success' => 'created'
            ], 200);
            
        }

    }
    public function instr_summary(Request $request){
        $validator = Validator::make($request->all(), [
			'user_id' => 'required',
        ]);
        $User = User::where(['id' => $request->user_id])->first();
        if (!Instructor::where(['user_id' => $request->user_id ])->first()) {
            return response()->json([
                'message' => 'Not Acceptable',
                'error' => 'Bukan Pengajar'
            ], 406);
        }
        
    }
}

?>