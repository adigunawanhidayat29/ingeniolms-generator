<?php 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\PlayerIdUser;
use Illuminate\Support\Facades\DB;


class OneSignalController extends Controller{

	public function unregisterPlayer(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'player_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
		PlayerIdUser::where(['user_id' => $request->user_id, 'player_id' => $request->player_id])->delete();
		return response()->json([
			'message' => 'success',
			'data' => 'Deleted'
		], 200);

	}

	public function registerPlayer(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'player_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
		if (!PlayerIdUser::where(['player_id' => $request->player_id])->first()) {
			$player_id_users = new PlayerIdUser;
			$player_id_users->user_id = $request->user_id;
			$player_id_users->player_id = $request->player_id;
			$player_id_users->save();

			return response()->json([
				'message' => 'success',
				'data' => 'Registered'
			], 200);

		}else{
			DB::table('player_id_users')->where('player_id', $request->player_id)->update(['user_id' => $request->user_id]);
			return response()->json([
				'message' => 'success',
				'data' => 'Registered'
			], 200);
		}
		
	}

	public function sendNotification(Request $request){
		$validator = Validator::make($request->all(), [
			'player_ids' => 'required'
		]);


		$headers = array(
			'Content-type: application/json',
			'Accept: application/json',
    		'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'
		);

		$fields = array(
			'app_id' => '611e29ae-92d3-4571-be63-062b0f10b000',
			'include_player_ids' => ["08fad2a2-741c-4be4-9e31-ee1f1b4dba1f", "3c4a984c-5a51-44f7-9f99-e5d0f43c6a48"],
			'data' => array('CourseId' => '8', 'userId' => '4'),
			'contents' => array('en' => 'Kursus baru telah rilis'),
			'headings' => array('en' => 'Learn Vue JS'),
			'android_group' => 'ingenio'
		);

		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://onesignal.com/api/v1/notifications' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return response()->json([
			'message' => 'sukses',
			'data' => $result
		]);
	}

}



?>