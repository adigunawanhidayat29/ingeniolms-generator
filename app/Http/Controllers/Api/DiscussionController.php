<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Discussion;
use App\DiscussionAnswer;
use App\Course;
use App\CourseUser;
use App\User;
use App\Http\Controllers\Controller;
use Validator;

class DiscussionController extends Controller{
	public function ask(Request $request){
		$validator = Validator::make($request->all(), [
			'course_id' => 'required',
			'user_id' => 'required',
			'body' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

	    $Discussion = new Discussion;
	    $Discussion->user_id = $request->user_id;
	    $Discussion->course_id = $request->course_id;
	    $Discussion->status = '1';
	    $Discussion->body = $request->body;
	    $Discussion->save();

	    $name = User::where(['id' => $request->user_id])->first();
	    $course_name = Course::where(['id' => $request->course_id])->first();

	    $player_id_users = CourseUser::select('player_id_users.player_id')
	    ->join('player_id_users', 'player_id_users.user_id', '=', 'courses_users.user_id')
	    ->where(['course_id' => $request->course_id])->get();

	    $headers = array(
			'Content-type: application/json',
			'Accept: application/json',
    		'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'
		);

		$player_ids = [];

		foreach ($player_id_users as $player_id_user) {
			 array_push($player_ids, $player_id_user->player_id);
		}

		$fields = array(
			'app_id' => '611e29ae-92d3-4571-be63-062b0f10b000',
			'include_player_ids' => $player_ids,
			'data' => array('CourseId' => $request->course_id),
			'contents' => array('en' => $name->name.' Bertanya di kursus '.$course_name->title),
			'headings' => array('en' => 'Pertanyaan Baru'),
			'android_group' => 'ingenio_ask'
		);

		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://onesignal.com/api/v1/notifications' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

		return response()->json([
				'message' => 'success',
				'data' => $fields
		]);

	}

	public function answer(Request $request){
		$validator = Validator::make($request->all(), [
			'discussion_id' => 'required',
			'user_id' => 'required',
			'body' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

	    $DiscussionAnswer = new DiscussionAnswer;
	    $DiscussionAnswer->user_id = $request->user_id;
	    $DiscussionAnswer->discussion_id = $request->discussion_id;
	    $DiscussionAnswer->body = $request->body;
	    $DiscussionAnswer->status = '1';
	    $DiscussionAnswer->save();
	}
}

?>