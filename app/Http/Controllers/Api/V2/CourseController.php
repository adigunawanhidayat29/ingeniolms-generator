<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;

use App\Course;
use App\Discussion;
use App\CourseUser;

class CourseController extends BaseController
{

    public function index () {
        $courses = Course::query();
        $courses = Course::with('author', 'category', 'level');
        if (\Request::has('q')){
            $courses->where('title', 'like', '%' . \Request::get('q') . '%');
        }
        if (\Request::has('filter')){
            $courses = $courses->orderBy('id', \Request::get('filter'));
        } else {
            $courses = $courses->orderBy('id', 'desc');
        }
        $courses = $courses->paginate(6);
        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function detail ($id) {
        $courses = Course::with('author', 'category', 'level')->where('id', $id)->with('sections')->first();
        $data = $courses;
        
        return $this->sendResponse('success', $data);
    }

    public function enroll ($id, Request $request) {
        $course_user = new CourseUser;
        $course_user->course_id = $id;
        $course_user->user_id = $request->user_id;
        $course_user->save();

        $data = $course_user;
        
        return $this->sendResponse('success', $data);
    }

    public function enroll_password ($id, Request $request) {
        $course = Course::where(['id' => $id, 'password' => $request->password])->first();

        if ($course) {

            $checkCourseUser = CourseUser::where(['user_id' => $request->user_id, 'course_id' => $id])->first();

            if (!$checkCourseUser) {
                $course_user = new CourseUser;
                $course_user->course_id = $id;
                $course_user->user_id = $request->user_id;
                $course_user->save();
        
                $data = $course_user;
                return $this->sendResponse('success', $data);
            } else {
                return $this->sendError('User sudah terdaftar');
            }
        } else {
            return $this->sendError('password tidak sesuai');
        }
    }

    public function my() {
        $user = Auth::user();

        $courses = Course::select('courses.*')->with('author', 'category', 'level')
            ->join('courses_users','courses_users.course_id','=','courses.id')
            ->where('courses_users.user_id', $user->id)
            ->orderBy('courses.id', 'desc')
            ->paginate(6);

        $data = [
            'courses' => $courses,
        ];
        
        return $this->sendResponse('success', $data);
    }

    public function discussion($id, Request $request) {
        $user = Auth::user();

        $discussion = new Discussion;
        $discussion->user_id = $user->id;
        $discussion->course_id = $id;
        $discussion->body = $request->body;
        $discussion->status = $request->status ?? '1';
        $discussion->save();

        $data = [
            'discussion' => $discussion,
        ];

        return $this->sendResponse('success', $data);
    }

}
