<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;

class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $input['username'] = $input['email'];
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('IngeniolmsApi')->accessToken;
        $success['user'] =  $user;

        return $this->sendResponse('User register successfully.', $success);
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('IngeniolmsApi')->accessToken; 
            $success['user'] =  $user;

            return $this->sendResponse('User login successfully.', $success);
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }

    public function facebook(Request $request)
    {
        $user = User::where('email', '=', $request->email)->first();

        if($user){ 
            Auth::login($user);
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('IngeniolmsApi')->accessToken; 
            $success['user'] =  $user;

            return $this->sendResponse('User login successfully.', $success);
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }

    public function google(Request $request)
    {
        $user = User::where('email', '=', $request->email)->first();

        if($user){ 
            Auth::login($user);
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('IngeniolmsApi')->accessToken; 
            $success['user'] =  $user;

            return $this->sendResponse('User login successfully.', $success);
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }
}
