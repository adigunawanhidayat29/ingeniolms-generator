<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\User;

class UserController extends BaseController
{
    public function index()
    {
        $user = Auth::user();

        $data = [
            'user' => $user,
        ];

        return $this->sendResponse('success', $data);
    }

    public function updateProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user = Auth::user();
        $userUpdate = User::where('id', $user->id)->first();
        $userUpdate->name = $request->name;
        $userUpdate->email = $request->email;
        $userUpdate->phone = $request->phone;
        $userUpdate->description = $request->description;
        $userUpdate->facebook = $request->facebook;
        $userUpdate->instagram = $request->instagram;
        $userUpdate->save();

        $data = [
            'user' => $userUpdate,
        ];

        return $this->sendResponse('success', $data);
    }

    public function updatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user = Auth::user();
        $userUpdate = User::where('id', $user->id)->first();
        $userUpdate->password = bcrypt($request->password);
        $userUpdate->save();

        $data = [
            'user' => $userUpdate,
        ];

        return $this->sendResponse('success', $data);
    }
}
