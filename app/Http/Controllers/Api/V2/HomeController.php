<?php
namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;

use App\Category;
use App\Course;

class HomeController extends BaseController {

	public function index(Request $request){
    $sliders = [
      [
        'title' => 'Lorem ipsum',
        'image' => '',
        'description' => 'Lorem ipsum'
      ],
      [
        'title' => 'Lorem ipsum',
        'image' => '',
        'description' => 'Lorem ipsum'
      ],
    ];
    $categories = Category::limit(6)->get();
    $courses = Course::with('author', 'category', 'level')->orderBy('id', 'desc')->paginate(6);
    $data = [
      'sliders' => $sliders,
      'categories' => $categories,
      'courses' => $courses,
    ];

    return $this->sendResponse('success', $data);
  }
  
}