<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use Storage;

use App\Course;
use App\CourseUser;
use App\Content;
use App\Quiz;
use App\QuizParticipant;
use App\QuizParticipantAnswer;
use App\QuizQuestion;
use App\Assignment;
use App\AssignmentAnswer;
use App\User;

use App\Http\Resources\AssignmentResource;
use App\Http\Resources\QuizQuestionResource;

class ContentController extends BaseController
{
    public function learn($id) {
        $content = Content::where('id', $id)->first();
        $data = [
            'content' => $content,
        ];
        return $this->sendResponse('success', $data);
    }

    public function quiz_learn($id) {
        $quiz = Quiz::where('id', $id)->first();
        $data = [
            'quiz' => $quiz,
        ];
        return $this->sendResponse('success', $data);
    }

    public function quiz_start($id, Request $request) {
        $QuizParticipant = new QuizParticipant;
        $QuizParticipant->quiz_id = $id;
        $QuizParticipant->user_id = $request->user_id;
        $QuizParticipant->time_start = date("Y-m-d H:i:s");
        $QuizParticipant->save();

        $data = [
            'quiz_participant' => $QuizParticipant,
        ];

        return $this->sendResponse('success', $data);
    }

    public function quiz_questions($id) {
        $QuizQuestion = QuizQuestion::where('quiz_id', $id)
            ->with('quiz_question_answers', 'quiz_type')
            ->paginate(1);

        $data = [
            'quiz_questions' => new QuizQuestionResource($QuizQuestion),
        ];

        return $this->sendResponse('success', $data);
    }

    public function quiz_finish(Request $request) {
        $QuizParticipant = QuizParticipant::where(['id' => $request->quiz_participant_id])->first();
        $QuizParticipant->finish = 1;
        $QuizParticipant->submitted_date = date('Y-m-d H:i:s');
        $QuizParticipant->save();

        foreach($request->data as $index => $req) {
            $QuizParticipantAnswer = new QuizParticipantAnswer;
            $QuizParticipantAnswer->quiz_id = $request->quiz_id;
            $QuizParticipantAnswer->quiz_participant_id = $request->quiz_participant_id;
            $QuizParticipantAnswer->quiz_question_id = $req['quiz_question_id'];
            $QuizParticipantAnswer->quiz_question_answer_id = empty($req['quiz_question_answer_id']) ? 0 : $req['quiz_question_answer_id'];
            $QuizParticipantAnswer->answer_short_answer = empty($req['answer_short_answer']) ? null : $req['answer_short_answer'];
            $QuizParticipantAnswer->answer_essay = empty($req['answer_essay']) ? null : $req['answer_essay'];
            $QuizParticipantAnswer->save();
        }

        $data = [
            'quiz_participant_answer' => QuizParticipantAnswer::where('quiz_participant_id', $request->quiz_participant_id)->get(),
        ];

        return $this->sendResponse('success', $data);
    }

    public function assignment_learn($id) {
        $assignment = Assignment::where('id', $id)->first();
        $data = [
            'assignment' => new AssignmentResource($assignment),
        ];
        return $this->sendResponse('success', $data);
    }

    public function assignment_finish($id, Request $request) {
        $assignment = Assignment::where('id', $id)->first();
        $User = User::where('id', $request->user_id)->first();

        // jika tipe tugas adalah file maka upload ke storage
        if ($assignment->type == 1) {
            // DIGITALOCEAN UPLOAD
            $destinationPath = asset_path('uploads/assignments/');
            $file = $request->file('answer');
            $extension = $request->file('answer')->getClientOriginalExtension();
            $name_file = 'assignment-'.str_slug($User->name).rand(111,9999).'.'.$extension;
            $folder = get_folderName_course($assignment->id_section); // get folder course by section id
            Storage::disk('do_spaces')->put('assets/' . $folder.'/'.$name_file, fopen($file, 'r+'), 'public');//upload digital ocean
            $path_file = get_asset_newpath('assets/'.$folder, $name_file);
            $answer = "https://ingeniolms.sgp1.digitaloceanspaces.com/" . $path_file;
        } else {
            $answer = $request->answer;
        }

        $AssignmentAnswer = new AssignmentAnswer;
        $AssignmentAnswer->assignment_id = $id;
        $AssignmentAnswer->user_id = $request->user_id;
        $AssignmentAnswer->answer = $answer;
        $AssignmentAnswer->description = $request->description;
        $AssignmentAnswer->save();

        $data = [
            'assignment_answer' => $AssignmentAnswer,
        ];

        return $this->sendResponse('success', $data);
    }
}
