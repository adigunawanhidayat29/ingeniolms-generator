<?php

namespace App\Http\Controllers\Api\V2\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use App\Course;
use App\Section;
use App\Content;
use App\Assignment;
use App\Quiz;
use App\CourseAnnouncement;
use App\Discussion;
use Storage;
use Auth;
use File;
use ZipArchive;
use League\Flysystem\MountManager;
use App\Http\Resources\ContentResource;

class ContentController extends BaseController
{
    public function sectionGet($id){
        $sections = Section::where('id_course', $id)->get();

        $data = [
            'sections' => $sections,
        ];

        return $this->sendResponse('success', $data);
    }

    public function sectionStore($id, Request $request){
        $sections = new Section;
        $sections->id_course = $id;
        $sections->title = $request->title;
        $sections->description = $request->description;
        $sections->save();

        $data = [
            'sections' => $sections,
        ];

        return $this->sendResponse('success', $data);
    }

    public function sectionUpdate($id, $section_id, Request $request){
        $sections = Section::find($section_id);
        $sections->id_course = $id;
        $sections->title = $request->title;
        $sections->description = $request->description;
        $sections->save();

        $data = [
            'sections' => $sections,
        ];

        return $this->sendResponse('success', $data);
    }

    public function sectionDestroy($id, $section_id, Request $request){
        $section = Section::find($section_id)->delete();
        $data = [];
        return $this->sendResponse('success', $data);
    }

    public function contentGet($id){
        $contents = Content::where('id_section', $id)->get();

        $data = [
            'contents' => $contents,
        ];

        return $this->sendResponse('success', $data);
    }

    public function contentStore($id, Request $request){

        $folder = get_folderName_course($id);

        $contents = new Content;
        $contents->id_section = $id;
        $contents->title = $request->title;
        $contents->description = $request->description;
        $contents->type_content = $request->type_content;
        $contents->preview = $request->preview ?? '0';
        $contents->is_description_show = $request->is_description_show ?? '0';
        $contents->status = $request->status ?? '0';

        if ($request->type_content == 'scorm') {

            $file = $request->file('file');
            // upload
            $name_file = str_slug($request->title) .'-'. $file->getClientOriginalName();
            $folder = get_folderName_course($id); // get folder course by section id
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $name_file, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/', $name_file);
            // upload
            //  extract zip
            $zip = new ZipArchive;
            $zip->open($file);
            $zip->extractTo(storage_path('app/public/' . str_slug($request->title)));
            $zip->close();
            //  extract zip
            // upload to storage
            $files = Storage::disk('public')->allFiles(str_slug($request->title));
            foreach ($files as $fileExtract) {
                // echo $fileExtract;
                $inputStream = Storage::disk('public')->getDriver()->readStream($fileExtract);
                $destination = Storage::disk(env('APP_DISK'))->getDriver()->getAdapter()->getPathPrefix(). env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $fileExtract;
                Storage::disk(env('APP_DISK'))->getDriver()->putStream($destination, $inputStream, ['visibility' => 'public']);
            }
            // upload to storage
            $contents->path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . str_slug($request->title) . '/imsmanifest.xml';
            $contents->full_path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . str_slug($request->title) . '/imsmanifest.xml';

        } else {

            if($request->file('file')){

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension(); // getting extension
                $filename = str_slug($request->title).rand(111,9999).'.'.$extension; // renaming file
                Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/contents/files/' . $filename, fopen($file, 'r+'), 'public'); //upload
                $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/contents/files/' , $filename);
                $contents->name_file = $filename;
                $contents->path_file = $path_file;
                $contents->full_path_file = $path_file;

            }

        }

        $contents->save();

        $data = [
            'contents' => $contents,
        ];

        return $this->sendResponse('success', $data);
    }

    public function contentUpdate($id, $content_id, Request $request){
        $folder = get_folderName_course($id);
        
        $contents = Content::find($content_id);
        $contents->id_section = $id;
        $request->has('title') && $request->type_content != null ? $contents->title = $request->title : null;
        $request->has('description') && $request->type_content != null ? $contents->description = $request->description : null;
        $request->has('type_content') && $request->type_content != null ? $contents->type_content = $request->type_content : null;
        $request->has('preview') && $request->type_content != null ? $contents->preview = $request->preview : null;
        $request->has('is_description_show') && $request->type_content != null ? $contents->is_description_show = $request->is_description_show : null;

        if ($contents->type_content == 'scorm') {

            $file = $request->file('file');
            // upload
            $name_file = str_slug($request->title) .'-'. $file->getClientOriginalName();
            $folder = get_folderName_course($id); // get folder course by section id
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $name_file, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/', $name_file);
            // upload
            //  extract zip
            $zip = new ZipArchive;
            $zip->open($file);
            $zip->extractTo(storage_path('app/public/' . str_slug($request->title)));
            $zip->close();
            //  extract zip
            // upload to storage
            $files = Storage::disk('public')->allFiles(str_slug($request->title));
            foreach ($files as $fileExtract) {
                // echo $fileExtract;
                $inputStream = Storage::disk('public')->getDriver()->readStream($fileExtract);
                $destination = Storage::disk(env('APP_DISK'))->getDriver()->getAdapter()->getPathPrefix(). env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $fileExtract;
                Storage::disk(env('APP_DISK'))->getDriver()->putStream($destination, $inputStream, ['visibility' => 'public']);
            }
            // upload to storage
            $contents->path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . str_slug($request->title) . '/imsmanifest.xml';
            $contents->full_path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . str_slug($request->title) . '/imsmanifest.xml';

        } else {

            if($request->file('file')){

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension(); // getting extension
                $filename = str_slug($request->title).rand(111,9999).'.'.$extension; // renaming file
                Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/contents/files/' . $filename, fopen($file, 'r+'), 'public'); //upload
                $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/contents/files/' , $filename);
                $contents->name_file = $filename;
                $contents->path_file = $path_file;
                $contents->full_path_file = $path_file;

            }

        }

        $contents->save();

        $data = [
            'contents' => $contents,
        ];

        return $this->sendResponse('success', $data);
    }

    public function contentDestroy($id, $content_id, Request $request){
        $content = Content::find($content_id)->delete();
        $data = [];
        return $this->sendResponse('success', $data);
    }

    public function assignmentGet($id){
        $assignments = Assignment::where('id_section', $id)->get();

        $data = [
            'assignments' => $assignments,
        ];

        return $this->sendResponse('success', $data);
    }

    public function assignmentStore($id, Request $request){

        $folder = get_folderName_course($id);

        $assignments = new Assignment;
        $assignments->id_section = $id;
        $assignments->title = $request->title;
        $assignments->description = $request->description;
        $assignments->type = $request->type;
        $assignments->status = $request->status ?? '0';
        $assignments->attempt = $request->attempt ?? '-1';
        $assignments->time_start = $request->time_start ?? date("Y-m-d");
        $assignments->time_end = $request->time_end ?? date('Y-m-d', strtotime('+1 year'));

        if($request->file('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension(); // getting extension
            $filename = str_slug($request->title).rand(111,9999).'.'.$extension; // renaming file
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/assignments/' . $filename, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/assignments/' , $filename);
            $assignments->file = $path_file;
        }

        $assignments->save();

        $data = [
            'assignments' => $assignments,
        ];

        return $this->sendResponse('success', $data);
    }

    public function assignmentUpdate($id, $assignment_id, Request $request){
        $folder = get_folderName_course($id);

        $assignments = Assignment::find($assignment_id);
        $assignments->id_section = $id;
        $assignments->title = $request->title;
        $assignments->description = $request->description;
        $assignments->type = $request->type;
        $assignments->status = $request->status;

        if($request->file('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension(); // getting extension
            $filename = str_slug($request->title).rand(111,9999).'.'.$extension; // renaming file
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/assignments/' . $filename, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/assignments/' , $filename);
            $assignments->file = $path_file;
        }

        $assignments->save();

        $data = [
            'assignments' => $assignments,
        ];

        return $this->sendResponse('success', $data);
    }

    public function assignmentDestroy($id, $assignment_id, Request $request){
        $assignment = Assignment::find($assignment_id)->delete();
        $data = [];
        return $this->sendResponse('success', $data);
    }

    public function quizGet($id){
        $quiz = Quiz::where('id_section', $id)->get();

        $data = [
            'quizzes' => $quiz,
        ];

        return $this->sendResponse('success', $data);
    }

    public function quizStore($id, Request $request){

        $quiz = new Quiz;
        $quiz->id_section = $id;
        $quiz->name = $request->name;
        $quiz->slug = str_slug($request->name);
        $quiz->description = $request->description;
        $quiz->status = $request->status ?? '0';
        $quiz->quizz_type = $request->quizz_type ?? 'quiz';
        $quiz->attempt = $request->attempt ?? '-1';
        $quiz->save();

        $data = [
            'quizzes' => $quiz,
        ];

        return $this->sendResponse('success', $data);
    }

    public function quizUpdate($id, $quiz_id, Request $request){
        $quiz = Quiz::find($quiz_id);
        $quiz->id_section = $id;
        $quiz->name = $request->name ?? $quiz->name;
        $quiz->description = $request->description ?? $quiz->description;
        $quiz->status = $request->status ?? '0';
        $quiz->attempt = $request->attempt ?? '-1';
        $quiz->save();

        $data = [
            'quizzes' => $quiz,
        ];

        return $this->sendResponse('success', $data);
    }

    public function quizDestroy($id, $quiz_id, Request $request){
        $quiz = Quiz::find($quiz_id)->delete();
        $data = [];
        return $this->sendResponse('success', $data);
    }
}
