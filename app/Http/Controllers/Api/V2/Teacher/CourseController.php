<?php

namespace App\Http\Controllers\Api\V2\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V2\BaseController as BaseController;
use App\Course;
use App\CourseUser;
use App\Section;
use App\Content;
use App\Assignment;
use App\Quiz;
use App\CourseAnnouncement;
use App\Discussion;
use App\Progress;
use App\CourseStudentGroupUser;
use App\CourseStudentGroup;
use App\CourseAttendance;
use App\CourseAttendanceUser;
use Storage;
use Auth;
use DB;
use App\Http\Resources\ContentResource;
use DateTime;
use DateInterval;
use DatePeriod;
use Excel;
use App\Exports\CourseAttendanceReport;

class CourseController extends BaseController
{
    public function index()
    {
        // get user
        $user = Auth::user();

        $courses = Course::with('sections')->where('id_author', $user->id)->latest()->paginate(10);

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function store(Request $request)
    {
        // get user
        $user = Auth::user();

        //create folder by title
        $folder = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100, 999);
        Storage::disk(env('APP_DISK'))->makeDirectory(env('UPLOAD_PATH') . '/courses/' . $folder);

        // image
        if ($request->file('image')) {
            // $destinationPath = asset_path('uploads/courses/'); // upload path
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($request->title) . rand(111, 9999) . '.' . $extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/', $image);
            $courseImage = $path_file;
        } else {
            $courseImage = 'dynamic/uploads/courses/default.jpg';
        }

        // new courses
        $courses = new Course;
        $courses->title = $request->title;
        $courses->subtitle = $request->subtitle;
        $courses->slug = str_slug($request->title);
        $courses->description = $request->description;
        $courses->price = isset($request->price) ? $request->price : '0';
        $courses->public = isset($request->public) ? $request->public : '0';
        $courses->id_category = isset($request->id_category) ? $request->id_category : '1';
        $courses->id_author = $user->id;
        $courses->password = $request->password;
        $courses->folder = $folder;
        $courses->image = $courseImage;
        $courses->shortname = str_slug($request->title);
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function update($id, Request $request)
    {
        // get user
        $user = Auth::user();

        // update courses
        $courses = Course::find($id);

        // image
        if ($request->file('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($request->title) . rand(111, 9999) . '.' . $extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $courses->folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $courses->folder . '/thumbnails/', $image);
            $courseImage = $path_file;
        } else {
            $courseImage = 'dynamic/uploads/courses/default.jpg';
        }

        if ($request->has('title') && $request->title != null) {
            $courses->title = $request->title;
            $courses->slug = str_slug($request->title);
            $courses->shortname = str_slug($request->title);
        }

        $request->has('description') && $request->description != null ? $courses->description = $request->description : null;
        $request->has('subtitle') && $request->subtitle != null ? $courses->subtitle = $request->subtitle : null;
        $request->has('price') && $request->price != null ? $courses->price = $request->price : null;
        $request->has('public') && $request->public != null ? $courses->public = $request->public : null;
        $request->has('id_category') && $request->id_category != null ? $courses->id_category = $request->id_category : null;
        $request->has('password') && $request->password != null ? $courses->password = $request->password : null;
        $courses->image = $courseImage;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function destroy(Request $request)
    {
        $courses = Course::where('id', $request->id)->delete();
        $data = [];
        return $this->sendResponse('success', $data);
    }

    public function publish($id)
    {
        // update courses
        $courses = Course::find($id);
        $courses->public = 1;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function unpublish($id)
    {
        // update courses
        $courses = Course::find($id);
        $courses->public = 0;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function archive($id)
    {
        // update courses
        $courses = Course::find($id);
        $courses->archive = 1;
        $courses->public = 0;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function restore($id)
    {
        // update courses
        $courses = Course::find($id);
        $courses->archive = 0;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function changePassword($id, Request $request)
    {
        // update courses
        $courses = Course::find($id);
        $courses->password = $request->password;
        $courses->save();

        $data = [
            'courses' => $courses,
        ];

        return $this->sendResponse('success', $data);
    }

    public function duplicate($id, Request $request)
    {
        $user = Auth::user();
        $Course = Course::where('id', $id)->first();

        // duplicate course
        $newCourse = $Course->replicate();
        $newCourse->title = $Course->title . " - (copy)";
        $newCourse->slug = $Course->slug . "-" . rand(100, 999);
        $newCourse->password = generateRandomString(8);
        $newCourse->save();

        // duplicate section
        $Sections = Section::where('id_course', $Course->id)->get();
        foreach ($Sections as $Section) {
            $newSection = $Section->replicate();
            $newSection->id_course = $newCourse->id;
            $newSection->save();

            // duplicate content
            $Contents = Content::where('id_section', $Section->id)->get();
            foreach ($Contents as $Content) {
                $newContent = $Content->replicate();
                $newContent->id_section = $newSection->id;
                $newContent->save();
            }

            // duplicate assignment
            $Assignments = Assignment::where('id_section', $Section->id)->get();
            foreach ($Assignments as $Assignment) {
                $newAssignment = $Assignment->replicate();
                $newAssignment->id_section = $newSection->id;
                $newAssignment->save();
            }

            // duplicate quiz
            $Quizzes = Quiz::where('id_section', $Section->id)->get();
            foreach ($Quizzes as $Quiz) {
                $newQuiz = $Quiz->replicate();
                $newQuiz->id_section = $newSection->id;
                $newQuiz->save();

                // duplicate quiz question
                $QuizQuestions = QuizQuestion::where('quiz_id', $Quiz->id)->get();
                foreach ($QuizQuestions as $QuizQuestion) {
                    $newQuizQuestions = $QuizQuestion->replicate();
                    $newQuizQuestions->quiz_id = $newQuiz->id;
                    $newQuizQuestions->save();

                    $QuizQuestionAnswers = QuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->id)->get();
                    foreach ($QuizQuestionAnswers as $QuizQuestionAnswer) {
                        $newQuizQuestionAnswer = $QuizQuestionAnswer->replicate();
                        $newQuizQuestionAnswer->quiz_question_id = $newQuizQuestions->id;
                        $newQuizQuestionAnswer->save();
                    }
                }
            }
        }

        $data = [
            'courses' => $newCourse,
        ];

        return $this->sendResponse('success', $data);
    }

    public function announcement($id, Request $request)
    {
        $course_announcement = new CourseAnnouncement;
        $course_announcement->course_id = $id;
        $course_announcement->title = $request->title;
        $course_announcement->description = $request->description;
        $course_announcement->status = $request->status ?? '0';
        $course_announcement->save();

        $data = [
            'course_announcement' => $course_announcement,
        ];

        return $this->sendResponse('success', $data);
    }

    public function discussion($id, Request $request)
    {
        $user = Auth::user();

        $discussion = new Discussion;
        $discussion->user_id = $user->id;
        $discussion->course_id = $id;
        $discussion->body = $request->body;
        $discussion->status = $request->status ?? '1';
        $discussion->save();

        $data = [
            'discussion' => $discussion,
        ];

        return $this->sendResponse('success', $data);
    }

    public function atendee($course_id)
    {
        $Course = Course::where('id', $course_id)->first();
        $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
            ->join('users', 'users.id', '=', 'courses_users.user_id')->get();

        $sections = Section::where('id_course', $course_id)->get();
        $num_contents = 0;
        foreach ($sections as $section) {
            $contents = \DB::table('contents')
                ->where('id_section', $section->id)
                ->get();
            $num_contents += count($contents);
        }

        $completions = [];
        foreach ($CoursesUsers as $CourseUser) {
            //count percentage
            $num_progress = 0;
            $num_progress = count(Progress::where(['course_id' => $course_id, 'user_id' => $CourseUser->user_id, 'status' => '1'])->get());
            $percentage = 0;
            $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
            $percentage = 100 / $percentage;
            //count percentage

            $section_data = [];
            $sectionscontents = Section::where('id_course', $course_id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'desc')->get();
            foreach ($sectionscontents as $section) {
                $contents = \DB::table('contents')
                    ->where('id_section', $section->id)
                    ->get();

                array_push($section_data, [
                    'id' => $section->id,
                    'title' => $section->title,
                    'section_contents' => $contents,
                ]);
            }

            array_push($completions, [
                'name' => $CourseUser->name,
                'user_id' => $CourseUser->user_id,
                'percentage' => $percentage,
                'section_data' => $section_data
            ]);
        }

        $CourseStudentGroupUsers = CourseStudentGroupUser::where('course_id', $course_id)->get();

        $course_users = [];
        foreach ($CourseStudentGroupUsers as $value) {
            array_push($course_users, $value->user_id);
        }

        $course_student_group_users_by_course = CourseUser::with('student')->where('course_id', $course_id)->whereNotIn('user_id', $course_users)->get();

        $data = [
            'completions' => $completions,
            'course_student_groups' => CourseStudentGroup::where('course_id', $course_id)->with('course_student_group_user')->get(),
            'course_student_group_users_by_course' => $course_student_group_users_by_course,
            'Course' =>  $Course,
        ];

        return $this->sendResponse('success', $data);
    }

    public function atendeeGroup($course_id)
    {

        $Course = Course::where('id', $course_id)->first();

        $data = [
            'course_student_groups' => CourseStudentGroup::where('course_id', $course_id)->with('course_student_group_user')->get(),
            'Course' =>  $Course,
        ];

        return $this->sendResponse('success', $data);
    }

    public function atendeeGroupUsers($course_id)
    {

        $Course = Course::where('id', $course_id)->first();

        $CourseStudentGroupUsers = CourseStudentGroupUser::where('course_id', $course_id)->get();

        $course_users = [];
        foreach ($CourseStudentGroupUsers as $value) {
            array_push($course_users, $value->user_id);
        }

        $course_student_group_users_by_course = CourseUser::with('student')->where('course_id', $course_id)->whereNotIn('user_id', $course_users)->get();

        $data = [
            'course_student_group_users_by_course' => $course_student_group_users_by_course,
            'Course' =>  $Course,
        ];

        return $this->sendResponse('success', $data);
    }

    public function accessContents($course_id)
    {
        $user = Auth::user();
        // {{ For checking the author of the course, please pull snippets below on server }}
        $UserAuthorId = Auth::user()->id; // {{ Getting signed in instructor }}

        $courses = Course::select('courses.*', 'categories.title as category', 'course_levels.title as level', 'users.name as author', 'users.photo as photo', 'course_levels.id as level_id')
            ->leftJoin('users', 'users.id', '=', 'courses.id_author')
            ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
            ->leftJoin('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
            ->where('courses.id', $course_id)->first();

        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence', 'asc')->get();

        $num_contents = 0;

        foreach ($sections as $index => $section) {

            $section_all = [];

            $contents = Content::with('activity_order', 'progress_users')->where('id_section', $section->id)
                ->orderBy('contents.sequence', 'asc')
                ->get();
            $quizzes = Quiz::with('activity_order', 'progress_users')->where(['id_section' => $section->id])->get();
            $assignments = Assignment::with('activity_order', 'progress_users')->where(['id_section' => $section->id])->get();
            $num_contents += count($contents);

            foreach ($contents as $content) {
                // dd($content);
                $content->section_type = 'content';
                $section_all[] = $content;
            }

            foreach ($quizzes as $quizze) {
                $quizze->section_type = 'quizz';
                $section_all[] = $quizze;
            }

            foreach ($assignments as $assignment) {
                $assignment->section_type = 'assignment';
                $section_all[] = $assignment;
            }

            array_push($section_data, [
                'id' => $section->id,
                'title' => $section->title,
                'status' => $section->status,
                'sequence' => $section->sequence,
                'description' => $section->description,
                'id_course' => $section->id_course,
                'created_at' => $section->created_at,
                'updated_at' => $section->updated_at,
                'section_contents' => $contents,
                'section_quizzes' => $quizzes,
                'section_assignments' => $assignments,
                'section_all' => $section_all,
            ]);
        }

        $data = [
            'course' => $courses,
            'sections' => $section_data,
        ];

        return $this->sendResponse('success', $data);
    }

    public function attendance($course_id)
    {
        $Course = Course::where('id', $course_id)->first();
        $CourseAttendances = CourseAttendance::with('course_attendance_user')->where('course_id', $course_id)->get();

        $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
            ->join('users', 'users.id', '=', 'courses_users.user_id')->get();

        $Reports = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), DB::raw('count(course_attendances_id) as count_attendances'), 'user_id')
            ->with('user')
            ->where('course_id', $course_id)->groupBy('user_id')->get();

        $CourseAttendancesSummary = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), 'course_attendances.description', 'course_attendance_users.course_attendances_id')
            ->where('course_attendance_users.course_id', $course_id)->groupBy('course_attendances_id')
            ->join('course_attendances', 'course_attendances.id', '=', 'course_attendance_users.course_attendances_id')
            ->get();

        $data = [
            'Course' =>  $Course,
            'CourseAttendances' =>  $CourseAttendances,
            'courses_users' => $CoursesUsers,
            'reports' => $Reports,
            'CourseAttendancesSummary' => $CourseAttendancesSummary,
        ];

        // dd($data);

        return $this->sendResponse('success', $data);
    }

    public function attendanceStore(Request $request, $course_id)
    {

        $start_date = $request->datetime;
        $end_date = $request->end_date;

        if (isset($end_date)) { // jika terdapat multiple absensi
            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            // dd($request->repeat_on);

            foreach ($period as $dt) {
                if (in_array(strtolower($dt->format("l")), $request->repeat_on)) {
                    // echo $dt->format("l Y-m-d") . "<br>";

                    $CourseAttendances = new CourseAttendance;
                    $CourseAttendances->course_id = $course_id;
                    $CourseAttendances->name = $dt->format("D, d M Y ");
                    $CourseAttendances->description = 'Reguler';
                    $CourseAttendances->datetime = $dt->format("Y-m-d H:i:s");
                    $CourseAttendances->end_datetime = $dt->format("Y-m-d H:i:s");
                    $CourseAttendances->manual_student = $request->manual_student;
                    $CourseAttendances->save();
                }
            }
        } else {

            $CourseAttendances = new CourseAttendance;
            $CourseAttendances->course_id = $course_id;
            $CourseAttendances->description = $request->description;
            $CourseAttendances->datetime = $request->datetime;
            $CourseAttendances->end_datetime = $request->end_datetime;
            $CourseAttendances->name = date_format(date_create($request->datetime), "D, d M Y");
            $CourseAttendances->manual_student = $request->manual_student;
            $CourseAttendances->save();
        }

        $CourseAttendances = CourseAttendance::with('course_attendance_user')->where('course_id', $course_id)->get();

        $data = [
            'course_attendances' => $CourseAttendances,
        ];

        return $this->sendResponse('success', $data);
    }

    public function attendanceUpdate(Request $request, $course_id, $id)
    {
        $CourseAttendances = CourseAttendance::where('id', $id)->first();
        $CourseAttendances->description = $request->description;
        $CourseAttendances->datetime = $request->datetime;
        $CourseAttendances->end_datetime = $request->end_datetime;
        $CourseAttendances->manual_student = $request->manual_student;
        $CourseAttendances->save();

        $data = [
            'course_attendances' => $CourseAttendances,
        ];

        return $this->sendResponse('success', $data);
    }

    public function attendanceDelete($course_id, $id)
    {
        $CourseAttendances = CourseAttendance::where('id', $id)->first();
        $CourseAttendanceUser = CourseAttendanceUser::where('course_attendances_id', $id)->delete();
        $CourseAttendances->delete();

        return $this->sendResponse('success', []);
    }

    public function attendanceSubmit(Request $request, $course_id, $id)
    {

        $CheckCourseAttendanceUser = CourseAttendanceUser::where(['course_id' => $course_id, 'course_attendances_id' => $id])->first();

        if ($CheckCourseAttendanceUser) {
            CourseAttendanceUser::where(['course_id' => $course_id, 'course_attendances_id' => $id])->delete();
        }

        foreach ($request->user_id as $index => $data) {

            $status = $data . '_status';
            $remarks = $data . '_remarks';

            $CourseAttendanceUser = new CourseAttendanceUser;
            $CourseAttendanceUser->course_attendances_id = $id;
            $CourseAttendanceUser->course_id = $course_id;
            $CourseAttendanceUser->user_id = $data;
            $CourseAttendanceUser->status = $request->$status;
            $CourseAttendanceUser->remarks = $request->$remarks;
            $CourseAttendanceUser->save();
        }

        $data = [
            'course_attendance_users' => CourseAttendanceUser::where('id', $id)->get(),
        ];

        return $this->sendResponse('success', $data);
    }
}
