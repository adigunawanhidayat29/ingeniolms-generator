<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quiz;
use App\QuizParticipant;
use App\QuizParticipantAnswer;
use App\Http\Controllers\Controller;
use Validator;

class QuizController extends Controller{
	public function detail($id){
		$quiz = Quiz::where('id', $id)->first();
		return response()->json([
			'quiz' => $quiz
		]);
	}

	public function download(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'quiz_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
		$quiz = Quiz::where('id', $request->quiz_id)->first();
	    $quiz_questions_answers = [];
	    $quiz_questions = DB::table('quiz_questions')->where('quiz_id', $quiz->id)->get();
	    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $request->user_id))->first();
	    if(!$QuizParticipant_check){
	      // insert quiz participant
	      $QuizParticipant = new QuizParticipant;
	      $QuizParticipant->quiz_id = $quiz->id;
	      $QuizParticipant->user_id = $request->user_id;
	      $QuizParticipant->time_start = date("Y-m-d H:i:s");
	      $QuizParticipant->save();
	      // insert quiz participant
	    }
	    foreach($quiz_questions as $quiz_question){
	      $quiz_question_answers = DB::table('quiz_question_answers')->where('quiz_question_id', $quiz_question->id)->get();
	      array_push($quiz_questions_answers, array(
	        'id' => $quiz_question->id,
	        'question' => $quiz_question->question,
	        'quiz_type_id' => $quiz_question->quiz_type_id,
	        'question_answers' => $quiz_question_answers,
	      ));
	    }

	    $data = [
	      'quiz' => $quiz,
	      'quiz_questions_answers' => $quiz_questions_answers,
	    ];
	    return response()->json([
			'message' => 'sukses',
			'data' => $data
		], 200);

	}

	public function start(Request $request){
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'quiz_id' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

		$QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $request->quiz_id, 'user_id' => $request->user_id))->first();
		if(!$QuizParticipant_check){
			// insert quiz participant
			$QuizParticipant = new QuizParticipant;
			$QuizParticipant->quiz_id = $request->quiz_id;
			$QuizParticipant->user_id = $request->user_id;
			$QuizParticipant->time_start = date("Y-m-d H:i:s");
			$QuizParticipant->save();
			// insert quiz participant
			return response()->json([
				'message' => 'success',
				'data' => 'not_finish'
			], 200);

		}
		elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
			return response()->json([
				'message' => 'success',
				'data' => 'finish'
			], 200);
		}
		return response()->json([
			'message' => 'success',
			'data' => 'not_finish'
		], 200);
	}

	public function get_quiz(Request $request){
		$validator = Validator::make($request->all(), [
			'quiz_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

		$quiz = Quiz::where('id', $request->quiz_id)->first();
		$quiz_questions_answers = [];
		$quiz_questions = DB::table('quiz_questions')->where('quiz_id', $quiz->id)->get();

		foreach($quiz_questions as $quiz_question){
			$quiz_question_answers = DB::table('quiz_question_answers')->where('quiz_question_id', $quiz_question->id)->get();
			array_push($quiz_questions_answers, array(
				'id' => $quiz_question->id,
				'question' => $quiz_question->question,
				'quiz_type_id' => $quiz_question->quiz_type_id,
				'question_answers' => $quiz_question_answers,
			));
		}

		$data = [
			'quiz' => $quiz,
			'quiz_questions_answers' => $quiz_questions_answers,
		];
		return response()->json([
			'message' => 'success',
			'data' => $data
		], 200);
		

	}

	public function startOld(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'quiz_id' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
	    $quiz = Quiz::where('id', $request->quiz_id)->first();
	    $quiz_questions_answers = [];
	    $quiz_questions = DB::table('quiz_questions')->where('quiz_id', $quiz->id)->get();

	    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $request->user_id))->first();
	    if(!$QuizParticipant_check){
	      // insert quiz participant
	      $QuizParticipant = new QuizParticipant;
	      $QuizParticipant->quiz_id = $quiz->id;
	      $QuizParticipant->user_id = $request->user_id;
	      $QuizParticipant->time_start = date("Y-m-d H:i:s");
	      $QuizParticipant->save();
	      // insert quiz participant
	    }
	    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
	      return response()->json([
				'message' => 'sukses',
				'data' => 'result'
			], 200);
	    }

	    foreach($quiz_questions as $quiz_question){
	      $quiz_question_answers = DB::table('quiz_question_answers')->where('quiz_question_id', $quiz_question->id)->get();
	      array_push($quiz_questions_answers, array(
	        'id' => $quiz_question->id,
	        'question' => $quiz_question->question,
	        'quiz_type_id' => $quiz_question->quiz_type_id,
	        'question_answers' => $quiz_question_answers,
	      ));
	    }

	    $data = [
	      'quiz' => $quiz,
	      'quiz_questions_answers' => $quiz_questions_answers,
	    ];
	    return response()->json([
			'message' => 'sukses',
			'data' => $data
		], 200);
	}

	public function sync_handler(Request $request){
		$validator = Validator::make($request->all(), [
			'sync_data' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
		$datas = json_decode(json_encode($request->sync_data), FALSE);
		foreach ($datas as $data) {
			$quiz = Quiz::where('id', $data->quiz_id)->first();
		    $quiz_participant = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $data->user_id))->first();

		    if(QuizParticipantAnswer::where(array('quiz_participant_id' => $quiz_participant->id))->first()){
		      return redirect('quiz/'.$id.'/result'); // jika sudah mengikuti quiz redirect ke result
		    }else{
		      $question = $data->questions;
		      foreach($question as $index => $value){
		        $QuizParticipantAnswer = new QuizParticipantAnswer;
		        $QuizParticipantAnswer->quiz_id = $quiz->id;
		        $QuizParticipantAnswer->quiz_participant_id = $quiz_participant->id;
		        $QuizParticipantAnswer->quiz_question_id = $value;
		        $QuizParticipantAnswer->quiz_question_answer_id = isset ($data->answers[$index]) ? $data->answers[$index] : 0;
		        $QuizParticipantAnswer->save();
		      }

		      $quiz_participant->time_end = date("Y-m-d H:i:s"); // update time end
		      $quiz_participant->save();
		    }
		}
	}

	public function finish(Request $request){
	$validator = Validator::make($request->all(), [
		'user_id' => 'required',
		'quiz_id' => 'required',
		'answers' => 'required',
		'questions' => 'required'
	]);
	if ($validator->fails()) {
		return response()->json([
			'message' => 'error',
			'data' => $validator->errors()
		], 406);
	}
    $quiz = Quiz::where('id', $request->quiz_id)->first();
    $quiz_participant = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $request->user_id))->first();

    if(QuizParticipantAnswer::where(array('quiz_participant_id' => $quiz_participant->id))->first()){
      return redirect('quiz/'.$id.'/result'); // jika sudah mengikuti quiz redirect ke result
    }else{
      $question = $request->questions;
      foreach($question as $index => $value){
        $QuizParticipantAnswer = new QuizParticipantAnswer;
        $QuizParticipantAnswer->quiz_id = $quiz->id;
        $QuizParticipantAnswer->quiz_participant_id = $quiz_participant->id;
        $QuizParticipantAnswer->quiz_question_id = $value;
        $QuizParticipantAnswer->quiz_question_answer_id = isset ($request->answers[$index]) ? $request->answers[$index] : 0;
        $QuizParticipantAnswer->save();
      }

      $quiz_participant->time_end = date("Y-m-d H:i:s"); // update time end
      $quiz_participant->save();
    }
  }

  public function all_result($user_id){
  	$quizzes = Quiz::all();
  	$data = [];
  	foreach ($quizzes as $quiz) {
  		$QuizParticipant = QuizParticipant::where(['quiz_id' => $quiz->id, 'user_id' => $user_id])->first();
  		if ($QuizParticipant) {
  			if ($QuizParticipant->time_end != null) {
		    	$QuizParticipantAnswers = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
			      ->where('quiz_participant_id', $QuizParticipant->id)
			      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
			      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id');

			    // get count answer correct n incorrect n score
			    $count_answer_correct = 0;
			    $count_answer_incorrect = 0;
			    $score = 0;
			    $weight = 100;
			    foreach($QuizParticipantAnswers->get() as $QuizParticipantAnswer){
			      // $weight += $QuizParticipant->weight;
			      if($QuizParticipantAnswer->answer_correct == '1'){
			        $count_answer_correct += count($QuizParticipantAnswer->answer_correct);
			      }else{
			        $count_answer_incorrect += count($QuizParticipantAnswer->answer_correct);
			      }
			    }
			    $score = ($weight / $QuizParticipantAnswer->count()) * $count_answer_correct;
			    // get count answer correct n incorrect n score

			    $QuizParticipant->grade = $score; // update grade
			    $QuizParticipant->save();
			    array_push($data, [
			      'quiz' => $quiz,
			      'quiz_participants' => $QuizParticipantAnswers->get(),
			      'count_question' => $QuizParticipantAnswers->count(),
			      'count_answer_correct' => $count_answer_correct,
			      'count_answer_incorrect' => $count_answer_incorrect,
			      'score' => $score,
			    ]);
		    }
  		}
	    
  	}
  	return response()->json([
    	'message' => 'success',
    	'data' => $data
    ]);
	}
	


  public function result(Request $request){
  	$validator = Validator::make($request->all(), [
		'user_id' => 'required',
		'quiz_id' => 'required',
	]);
	if ($validator->fails()) {
		return response()->json([
			'message' => 'error',
			'data' => $validator->errors()
		], 406);
	}
    $quiz = Quiz::where('id', $request->quiz_id)->first();
    $QuizParticipant = QuizParticipant::where(['quiz_id' => $quiz->id, 'user_id' => $request->user_id])->first();
    if ($QuizParticipant->time_end == null) {
    	return response()->json([
			'message' => 'success',
			'data' => 'not_ready'
		], 200);
    }
    $QuizParticipantAnswers = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participant_id', $QuizParticipant->id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id');

    // get count answer correct n incorrect n score
    $count_answer_correct = 0;
    $count_answer_incorrect = 0;
    $score = 0;
    $weight = 100;
    foreach($QuizParticipantAnswers->get() as $QuizParticipantAnswer){
      // $weight += $QuizParticipant->weight;
      if($QuizParticipantAnswer->answer_correct == '1'){
        $count_answer_correct += count($QuizParticipantAnswer->answer_correct);
      }else{
        $count_answer_incorrect += count($QuizParticipantAnswer->answer_correct);
      }
    }
    $score = ($weight / $QuizParticipantAnswer->count()) * $count_answer_correct;
    // get count answer correct n incorrect n score

    $QuizParticipant->grade = $score; // update grade
    $QuizParticipant->save();

    $data = [
      'quiz' => $quiz,
      'quiz_participants' => $QuizParticipantAnswers->get(),
      'count_question' => $QuizParticipantAnswers->count(),
      'count_answer_correct' => $count_answer_correct,
      'count_answer_incorrect' => $count_answer_incorrect,
      'score' => $score,
    ];
    return response()->json([
    	'message' => 'success',
    	'data' => $data
    ]);
  }
}

?>