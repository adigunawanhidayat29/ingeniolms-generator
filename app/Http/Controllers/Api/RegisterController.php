<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;


class RegisterController extends Controller
{
  public function index(Request $request){

    $validator = Validator::make($request->all(), [
      'email' => 'required|unique:users|max:255',
      'password' => 'required|min:6',
      'name' => 'required',
    ]);

    if($validator->fails()){
      return response()->json([
        'message' => 'error',
        'data' => $validator->errors(),
      ], 406);
    }else{
      $Register = new User;
      $Register->email = $request->email;
      $Register->name = $request->name;
      $Register->phone = $request->phone;
      $Register->activation_token = str_random(40);
      $Register->password = bcrypt($request->password);
      $Register->save();
      
      Mail::to($request->email)->send(new Register($Register));

      return response()->json([
        'message' => 'created',
        'data' => $Register,
      ], 201);
    }
  }
}
