<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;
use App\Section;
use App\Content;
use Auth;
use Validator;
use App\User;
use App\Instructor;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
  

  public function index(Request $request){
    if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        $User = Auth::user();
        if (!$User->is_active) {
          return response()->json([
            'message' => 'error',
            'data' => 'activation_required',
          ], 401);
        }
        $token = str_random(5).'Dfbh-cd'.str_random(47).'.121a';
        $isInstructor = Instructor::where(['user_id' => $User->id, 'status' => '1'])->first();
        $data = [
          'id' => $User->id,
          'name' => $User->name,
          'slug' => $User->slug,
          'email' => $User->email,
          'phone' =>  $User->phone,
          'description' => $User->description,
          'is_active' => $User->is_active,
          'photo' => $User->photo,
          'token' => $token,
          'isInstructor' => $isInstructor ? 1 : 0
        ];
        return response()->json([
          'message' => 'success',
          'data' => $data,
        ], 200);
    }else{
      return response()->json([
        'message' => 'error',
        'data' => 'invalid_account'
      ], 401);
    }
  }
  public function social_login(Request $request){
    $validator = Validator::make($request->all(), [
      'email' => 'required|max:255',
      'name' => 'required',
      'photo' => 'required',
      'provider' => 'required',
      'access_id' => 'required'
    ]);
    
    if ($validator->fails()) {
      return response()->json([
        'message' => 'error',
        'data' => $validator->errors()
      ], 406);
    }

    if (User::where(['email' => $request->email])->first()) {
      DB::table('users')->where('email', $request->email)->update(['name' => $request->name, 'photo' => $request->photo]);
      $User = User::where(['email' => $request->email])->first();
      $token = str_random(5).'Dfbh-cd'.str_random(47).'.121a';
      $isInstructor = Instructor::where(['user_id' => $User->id, 'status' => '1'])->first();
      $data = [
          'id' => $User->id,
          'name' => $User->name,
          'email' => $User->email,
          'phone' =>  $User->phone,
          'is_active' => $User->is_active,
          'photo' => $User->photo,
          'token' => $token,
          'isInstructor' => $isInstructor ? 1 : 0
      ];
      return response()->json([
        'message' => 'success',
        'data' => $data,
      ], 200);
    }else{
      $Register = new User;
      $Register->email = $request->email;
      $Register->name = $request->name;
      $Register->photo = $request->photo;
      $Register->activation_token = str_random(40);
      $Register->is_active = '1';
      $Register->password = bcrypt(str_random(20));
      $Register->save();

      $User_social = User::where(['email' => $request->email])->first();
      $token = str_random(5).'Dfbh-cd'.str_random(47).'.121a';
      $isInstructor = Instructor::where(['user_id' => $User_social->id, 'status' => '1'])->first();
      $data = [
        'id' => $User_social->id,
        'name' => $User_social->name,
        'email' => $User_social->email,
        'phone' =>  $User_social->phone,
        'is_active' => $User_social->is_active,
        'photo' => $User_social->photo,
        'token' => $token,
        'isInstructor' => $isInstructor ? 1 : 0
      ];
      return response()->json([
        'message' => 'success',
        'data' => $data,
      ], 200);
    }
  }
}
