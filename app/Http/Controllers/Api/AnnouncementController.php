<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\CourseAnnouncementComment;
use App\Http\Controllers\Controller;
use Validator;

class AnnouncementController extends Controller {
	public function comment(Request $request){
		$validator = Validator::make($request->all(), [
			'announcement_id' => 'required',
			'user_id' => 'required',
			'comment' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}

		$CourseAnnouncementComment = new CourseAnnouncementComment;
	    $CourseAnnouncementComment->course_announcement_id = $request->announcement_id;
	    $CourseAnnouncementComment->comment = $request->comment;
	    $CourseAnnouncementComment->user_id = $request->user_id;
	    $CourseAnnouncementComment->status = '1';
	    $CourseAnnouncementComment->save();
	}
}

?>