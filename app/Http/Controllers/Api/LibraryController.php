<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Content;
use App\Course;
use App\CourseLive;
use App\Section;
use App\UserLibrary;
use Validator;
use Storage;

class LibraryController extends Controller{

  public function library_create_folder_api(Request $request){
    if (empty($request->user_id) || empty($request->title) || empty($request->parent)) {
      return response()->json([
        'message' => 'failed',
        'error' => 'No Data Selected'
      ], 406);
    }

    $UserLibrary = new UserLibrary;
    $UserLibrary->user_id = $request->user_id;
    $UserLibrary->title = $request->title;
    $UserLibrary->name_file = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100,999);
    $UserLibrary->parent = $request->parent;
    $UserLibrary->is_directory = '1';
    $UserLibrary->save();

    return response()->json([
      'message' => 'success'
    ], 200);
    
  }

    public function move_lib_api(Request $request){
      if (empty($request->datas) || empty($request->parent)) {
        return response()->json([
          'message' => 'failed',
          'error' => 'No Data Selected'
        ], 406);
      }

      foreach($request->datas as $data){
        $UserLibrary = DB::table('userlibraries')->where(['id' => $data['data']])->first();
        if(!empty($UserLibrary)){
          DB::table('userlibraries')->where(['id' => $data['data']])->update(['parent' => $request->parent]);
        }
      }
      return response()->json([
        'message' => 'success'
      ], 200);
    }

    public function delete_lib_api(Request $request){
      if (empty($request->datas)) {
        return response()->json([
          'message' => 'failed',
          'error' => 'No Data Selected'
        ], 406);
      }
      foreach($request->datas as $data){
        $UserLibrary = DB::table('userlibraries')->where(['id' => $data['data']])->first();
        if(!empty($UserLibrary)){
          $fromContent = DB::table('contents')->where(['name_file' => $UserLibrary->name_file])->first();
          if ($UserLibrary->path_file!=null && empty($fromContent)) {
            Storage::disk('google')->delete($UserLibrary->path_file);
          }
          if ($UserLibrary->path_file==null) {
            // Has file?
            DB::table('userlibraries')->where(['parent' => $UserLibrary->name_file])->delete();
          }
          DB::table('userlibraries')->where(['id' => $data['data']])->delete();
        }
      }
      return response()->json([
        'message' => 'success'
      ], 200);
      
    }

    public function library_api(Request $request){
        $user = DB::table('users')->where(['id' => $request->user_id])->first();
        if (empty($user)) {
          return response()->json([
            'message' => 'Not Acceptable',
            'error' => 'User Not Found'
          ], 406);
        }
        if ($user->user_folder_path==null || $user->user_folder_name==null) {
          $folder_name = $user->slug. '-' . date("Y-m-d") . '-' . rand(100,999);
          Storage::disk('google')->makeDirectory($folder_name);
          Storage::disk('sftp')->makeDirectory( server_assets_path($folder_name), 0777, true, true );
          $folder_path = get_folder_path_course($folder_name);
          DB::table('users')->where('id', $user->id)->update(['user_folder_name' => $folder_name, 'user_folder_path' => $folder_path]);
        }
    
        if (empty($request->parents)) {
          return response()->json([
            'message' => 'Not Acceptable',
            'error' => 'Parent Undefined'
          ], 406);
        }
        $parent = $request->parents;
        $contents = [];
        $file_icons = [];
        //$file_icon = 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/folder-icon.png';
        
        $courses = Course::where(['id_author' => $user->id])->get();
        if (!empty($courses)) {
          foreach($courses as $course){
            $sections = Section::where(['id_course' => $course->id])->get();
            if (!empty($sections)) {
              foreach($sections as $section){
                $contentes = Content::where(['id_section' => $section->id])->get();
                foreach($contentes as $content){
                  if ($content==null) {
                    continue;
                  }
                  if ($content->name_file==null) {
                    $userLib = UserLibrary::where(['name_file' => $content->id])->first();
                  if (empty($userLib)) {
                      $UserLibrary = new UserLibrary;
                      $UserLibrary->user_id = $user->id;
                      $UserLibrary->title = $content->title;
                      $UserLibrary->description = $content->description;
                      $UserLibrary->type_content = 'text';
                      $UserLibrary->name_file = $content->id;
                      $UserLibrary->status = $content->status;
                      $UserLibrary->is_directory = 0;
                      $UserLibrary->created_at = $content->created_at;
                      $UserLibrary->save();
                    }
                  }else{
                    $userLib = UserLibrary::where(['name_file' => $content->name_file])->first();
                    if (empty($userLib)) {
                      $UserLibrary = new UserLibrary;
                      $UserLibrary->user_id = $user->id;
                      $UserLibrary->title = $content->title;
                      $UserLibrary->description = $content->description;
                      $UserLibrary->type_content = $content->type_content;
                      $UserLibrary->name_file = $content->name_file;
                      $UserLibrary->path_file = $content->path_file;
                      $UserLibrary->full_path_file = $content->full_path_file;
                      $UserLibrary->full_path_file_resize = $content->full_path_file_resize;
                      $UserLibrary->full_path_file_resize_720 = $content->full_path_file_resize_720;
                      $UserLibrary->full_path_original = $content->full_path_original;
                      $UserLibrary->full_path_original_resize = $content->full_path_original_resize;
                      $UserLibrary->full_path_original_resize_720 = $content->full_path_original_resize_720;
                      $UserLibrary->file_status = $content->file_status==null ? 0 : $content->file_status;
                      $UserLibrary->file_size	 = $content->file_size;
                      $UserLibrary->video_duration = $content->video_duration;
                      $UserLibrary->status = $content->status;
                      $UserLibrary->created_at = $content->created_at;
                      $UserLibrary->is_directory = 0;
                      $UserLibrary->save();
                    }
                  }
                  
                }
              }
            }

            
            $quizzes = DB::table('quizzes')->where(['id_section' => $section->id])->get();
            foreach($quizzes as $quiz){
              if ($quiz==null) {
                continue;
              }
              $userLib = UserLibrary::where(['name_file' => $quiz->slug])->first();
              if (empty($userLib)) {
                $UserLibrary = new UserLibrary;
                $UserLibrary->user_id = $user->id;
                $UserLibrary->title = $quiz->name;
                $UserLibrary->description = $quiz->description;
                $UserLibrary->type_content = 'quiz';
                $UserLibrary->name_file = $quiz->slug;
                $UserLibrary->status = $quiz->status;
                $UserLibrary->is_directory = 0;
                $UserLibrary->created_at = $content->created_at;
                $UserLibrary->save();
              }
            }

          }
        }
        if (!empty($request->get('field')) && !empty($request->get('sort'))) {
          $UserLibraryAddeds = UserLibrary::where(['user_id' => $user->id, 'parent' => $parent])->orderBy($request->get('field'), $request->get('sort'))->get();
        }else{
          $UserLibraryAddeds = UserLibrary::where(['user_id' => $user->id, 'parent' => $parent])->get();
        }
        
        foreach($UserLibraryAddeds as $UserLibraryAdded){
          array_push($contents, [
            'id' => $UserLibraryAdded->id,
            'title' => $UserLibraryAdded->title,
            'name_file' => $UserLibraryAdded->name_file,
            'path_file' => $UserLibraryAdded->path_file,
            'type_content' => $UserLibraryAdded->type_content,
            'status' => $UserLibraryAdded->status,
            'created_at' => $UserLibraryAdded->created_at,
          ]);
        }
        $hasParents = [];
        array_push($hasParents, [
          'parent' => $parent=='.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $parent])->first()->title,
          'link_parent' => $parent=='.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $parent])->first()->name_file
        ]);
        $hasParent = $parent;
        while ($hasParent!='.'){
          $hasParent = DB::table('userlibraries')->select('parent')->where(['name_file' => $hasParent])->first()->parent;
          array_push($hasParents, [
            'parent' => $hasParent=='.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $hasParent])->first()->title,
            'link_parent' => $hasParent=='.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $hasParent])->first()->name_file
          ]);
        }
    
        $data = [
          'id_folder_name' => old('id_folder_name'),
          'data_id' => 1,
          'resources' => $contents,
          'parents' => $hasParents,
          'user_id' => $user->id,
        ];
        return response()->json([
          'resources' => $contents,
          'parents' => $hasParents,
          'user_id' => $user->id,
        ]);
        // return view('course.library', $data);
    }

    public function upload_image(Request $request){
      // file
      $destinationPath = './site_assets/background/'; // upload path
      $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      $file = rand(11111,999999).'.'.$extension; // renameing image
      $request->file('file')->move($destinationPath, $file); // uploading file to given path
      // file
      return response()->json([
        'message' => 'Success',
        'data' => [
          'path' => url('site_assets/background/'.$file),
        ]
      ], 200);
    }

    public function get_user_lib($lib_id){
      // $User = DB::table('users')->where('id', $user_id)->first();
      $Library = DB::table('userlibraries')
        ->select('userlibraries.*', 'users.*')
        ->join('users', 'users.id', '=', 'userlibraries.user_id')
        ->where('userlibraries.id', $lib_id)->first();

      $headers_a = array(
        'Content-type: application/x-www-form-urlencoded',
        'Accept: application/x-www-form-urlencoded',
          /*'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'*/
      );
      $fields_a = 'client_secret='.env('GOOGLE_DRIVE_CLIENT_SECRET').'&grant_type=refresh_token&refresh_token='.env('GOOGLE_DRIVE_REFRESH_TOKEN').'&client_id='.env('GOOGLE_DRIVE_CLIENT_ID');
  
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_a);
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields_a  );
      $result = curl_exec($ch );
      curl_close( $ch );

      $client_info = json_decode($result);
      return response()->json([
        'message' => 'success',
        'code' => 200,
        'data' => [
          'access_token' => $client_info->access_token,
          'folder_name' => $Library->user_folder_name,
          'google_folder_id' => $Library->user_folder_path,  
          'library' => $Library
        ]
      ]);
    }
}


?>