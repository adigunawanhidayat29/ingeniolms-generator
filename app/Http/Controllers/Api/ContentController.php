<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Content;
use Validator;
use DateTime;

class ContentController extends Controller{

    public function get_host(Request $request){
        $date_now = new DateTime('now');
        $date_expired = new DateTime('2018-09-19 11:04:00');
        // $interval = $date_registered->diff($date_expired);
        $isExpired = $date_now > $date_expired ? true : false;
        $params = [
            'data' => 'Object is cannot serialized',
            'message' => 'Anything will be fine',
        ];
        $data = json_decode(json_encode($params));
        return response()->json([
            'hostname' => $request->header('Origin'),
            'message' => $data->message,
            'interval' => $isExpired,
        ], 200);
    } 

    public function detail($course_id){
        $courses = DB::table('courses')->select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo', 'users.slug as author_slug', 'users.short_description as author_short_description', 'users.description as author_description', 'courses.id as id')
            ->join('users', 'users.id', '=', 'courses.id_author')
            ->join('categories', 'categories.id', '=', 'courses.id_category')
            ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
            ->where('courses.id', $course_id)->first();
        
        $content_previews_data = [];
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $courses->id)->get();
        
        $total_contents = 0;
        $total_video_duration_data = 0;
        foreach($sections as $section){
            $contents = DB::table('contents')->where('id_section', $section->id)->orderBy('sequence', 'asc')->get();
            $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();
            $contents_data = [];
            if (!empty($contents)) {
                $total_contents = $total_contents + count($contents) + count($quizzes);
                for ($i = 0;$i < count($contents);$i++) {
                    $contents_merge = array_merge((array)$contents[$i], [
                        'video_duration_data' => $contents[$i]->video_duration >= 3600 ? gmdate("H:i:s", $contents[$i]->video_duration) : gmdate("i:s", $contents[$i]->video_duration),
                    ]);
                    array_push($contents_data, $contents_merge);
                    $total_video_duration_data = $total_video_duration_data + $contents[$i]->video_duration;
                }
            }
            array_push($section_data, [
                'id' => $section->id,
                'title' => $section->title,
                'description' => $section->description,
                'id_course' => $section->id_course,
                'created_at' => $section->created_at,
                'updated_at' => $section->updated_at,
                'section_contents' => $contents_data,
                'section_quizzes' => $quizzes,
            ]);

            $content_previews = DB::table('contents')->where('id_section', $section->id)->where('preview', '1')->orderBy('sequence', 'asc')->get();
            foreach($content_previews as $index => $content_preview){
                array_push($content_previews_data, [
                    'id' => $content_preview->id,
                    'title' => $content_preview->title,
                    'type_content' => $content_preview->type_content,
                ]);
            }
        }

        // dd($content_previews_data);

        $data = [
            'course' => $courses,
            'sections' => $section_data,
            'content_previews' => $content_previews_data,
            'total_video_duration' => $total_video_duration_data >= 3600 ? gmdate("H:i:s", $total_video_duration_data) : gmdate("i:s", $total_video_duration_data),
            'total_content' => $total_contents,
        ];

        return response()->json([
            'message' => 'Success',
            'data' => $data
        ], 200);
        // return view('course.detail', $data);
    }

    public function update_content(Request $request){
        $validator = Validator::make($request->all(), [
            'id_content' => 'required',
            'file_path' => 'required',
            'field' => 'required',
			'status' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json([
				'message' => 'error',
				'data' => $validator->errors()
			], 406);
		}
        Content::where(['id' => $request->id_content])->update(['file_status' => $request->status, $request->field => $request->file_path]);
        return response()->json([
            'message' => 'success',
            'code' => 200,
            'data' => 'Data updated succesfully'
        ]);
    }

    public function get_all_content(){
        $content = DB::table('contents')
          ->where('path_file', null)
          ->where('full_path_file', null)
          ->where('full_path_file_resize', null)
          ->where('full_path_file_resize_720', null)
          ->where('full_path_original_resize', null)
          ->where('full_path_original_resize_720', null)
          ->where('type_content', 'video')
          ->get();
        
        return response()->json([
            'message' => 'success',
            'code' => 200,
            'data' => $content
        ]);
    }
    
    public function get_some_content($folder_name){
        $course = DB::table('courses')->where('folder', $folder_name)->first();
        if (!$course) {
            return response()->json([
                'message' => 'success',
                'code' => 200,
                'data' => null
            ]);
        }

        $sections = DB::table('sections')->where('id_course', $course->id)->orderBy('sequence','asc')->orderBy('updated_at','desc')->get();
        $contents = [];
        foreach ($sections as $section) {
            $contents_get = DB::table('contents')
                ->where('id_section', $section->id)
                ->where('type_content', 'video')
                ->get();
            for ($i=0; $i < count($contents_get); $i++){
                if ($contents_get[$i]->path_file==null || $contents_get[$i]->full_path_file_resize==null || $contents_get[$i]->full_path_original_resize==null ) {
                    array_push($contents, $contents_get[$i]);
                }
            }
        }
        return response()->json([
            'message' => 'success',
            'code' => 200,
            'course_id' => $course->id,
            'data' => $contents
        ]);
    }

    public function get_content($content_id, $course_id){
        $content = DB::table('contents')
          ->where('id', $content_id)
          ->first();

        $course = DB::table('courses')->where('id', $course_id)->first();

        $headers_a = array(
			'Content-type: application/x-www-form-urlencoded',
			'Accept: application/x-www-form-urlencoded',
    		/*'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'*/
		);
		$fields_a = 'client_secret='.env('GOOGLE_DRIVE_CLIENT_SECRET').'&grant_type=refresh_token&refresh_token='.env('GOOGLE_DRIVE_REFRESH_TOKEN').'&client_id='.env('GOOGLE_DRIVE_CLIENT_ID');

		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_a);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields_a  );
        $result = curl_exec($ch );
        curl_close( $ch );

        $client_info = json_decode($result);
        return response()->json([
			'message' => 'success',
			'code' => 200,
			'data' => [
                'access_token' => $client_info->access_token,
                'folder_name' => $course->folder,
                'google_folder_id' => $course->folder_path,  
                'contents' => $content
            ]
		]);
    }

	public function index($contentId)
	{
		$headers_a = array(
			'Content-type: application/x-www-form-urlencoded',
			'Accept: application/x-www-form-urlencoded',
    		/*'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'*/
		);
		$fields_a = 'client_secret='.env('GOOGLE_DRIVE_CLIENT_SECRET').'&grant_type=refresh_token&refresh_token='.env('GOOGLE_DRIVE_REFRESH_TOKEN').'&client_id='.env('GOOGLE_DRIVE_CLIENT_ID');

		$ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_a);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields_a  );
        $result = curl_exec($ch );
        curl_close( $ch );

        $client_info = json_decode($result);

        $headers_b = array(
			'Content-type: application/json',
			'Accept: application/json',
    		'Authorization: Bearer '.$client_info->access_token
		);

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/drive/v2/files/'.$contentId );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_b );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        $content = curl_exec($ch );
        curl_close( $ch );


        return response()->json([
			'message' => 'sukses',
			'token_access' => $client_info->access_token,
			'data' => json_decode($content)
		]);
    }
    


    public function playContent($contentId){
        $headers_a = array(
            'Content-type: application/x-www-form-urlencoded',
            'Accept: application/x-www-form-urlencoded',
            /*'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'*/
        );
        $fields_a = 'client_secret='.env('GOOGLE_DRIVE_CLIENT_SECRET').'&grant_type=refresh_token&refresh_token='.env('GOOGLE_DRIVE_REFRESH_TOKEN').'&client_id='.env('GOOGLE_DRIVE_CLIENT_ID');

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_a);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields_a  );
        $result = curl_exec($ch );
        curl_close( $ch );

        $client_info = json_decode($result);

        $headers_b = array(
            'Content-type: application/json',
            'Accept: application/json',
            'Authorization: Bearer '.$client_info->access_token
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://www.googleapis.com/drive/v2/files/'.$contentId );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers_b );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        $content = curl_exec($ch );
        curl_close( $ch );
        $url = json_decode($content);

        $playUrl = explode("&", $url->downloadUrl)[0].'&'.explode("&", $url->downloadUrl)[2].'&access_token='.$client_info->access_token;
        $fileSize = $url->fileSize;


        return response()->json([
            'message' => 'success',
            'data' => [
                'uri' => $playUrl,
                'fileSize' => $fileSize
            ]
        ]);


    }
}

?>