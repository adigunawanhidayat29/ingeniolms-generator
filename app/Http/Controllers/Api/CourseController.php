<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Datatables;
use Validator;
use Storage;
use App\Course;
use App\Category;
use App\Section;
use App\Content;
use App\CourseUser;
use App\Meeting;
use App\Progress;
use App\Discussion;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\Assignment;
use App\CourseAnnouncement;
use App\Rating;
use App\CourseProject;
use App\CourseBroadcast;
use App\ContentVideoQuiz;
use App\MeetingSchedule;
use App\User;
use App\UserLibrary;
use Auth;
use Cart;
use Illuminate\Support\Facades\Mail;
use App\Mail\Course as MailCourse;
use Image;




class CourseController extends Controller
{

  public function index(Request $request){
    if ($request->get('provider')!='ingenio_access_id' || $request->get('access_id')!=env('MOBILE_LOGIN_ACCESS_ID')) {
        return response()->json([
          'message' => 'error',
          'data' => 'Invalid Access ID or Access ID is Missing, The end point not valid'
        ], 406);
    }
    $Courses = Course::select('courses.*','users.name')
    ->join('users','users.id','=','courses.id_author')
    ->where(['status'=>'1'])
    ->where(['public' => '1'])
    ->whereNotIn('courses.id',function($query) {
        $query->select('course_id')->from('course_lives');
    })
    ->paginate(10);
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $Courses,
    ]);
  }

  public function live_course(){
    $course_lives = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->where('courses.status','1')
      ->where('course_lives.start_date', '<=', date("Y-m-d"))
      ->where('course_lives.end_date', '>=', date("Y-m-d"))
      ->join('users','users.id','=','courses.id_author')
      ->join('course_lives','course_lives.course_id','=','courses.id')
      ->orderBy('courses.id', 'desc')->paginate(10);
      return response()->json([
        'message' => 'success',
        'code' => 200,
        'data' => $course_lives,
      ]);
  }

  public function n_live_course(){
    $course_lives = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->where('courses.status','1')
      ->where('course_lives.start_date', '<=', date("Y-m-d"))
      ->where('course_lives.end_date', '>=', date("Y-m-d"))
      ->join('users','users.id','=','courses.id_author')
      ->join('course_lives','course_lives.course_id','=','courses.id')
      ->limit(10)
      ->orderBy('courses.id', 'desc')->get();
      return response()->json([
        'message' => 'success',
        'code' => 200,
        'data' => $course_lives
      ]);
      
  }

  public function n_course(){
    $Course = DB::table('courses')->select('courses.*','users.name')
    ->join('users','users.id','=','courses.id_author')
    ->where('status','=', '1')
    ->where('public','=', '1')
    ->whereNotIn('courses.id',function($query) {
        $query->select('course_id')->from('course_lives');
    })
    ->limit(10)
    ->orderBy('id', 'desc')->get();
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $Course
    ]);

  }

  public function search_course($keyword, Request $request){
    if ($request->get('provider')!='ingenio_access_id' || $request->get('access_id')!=env('MOBILE_LOGIN_ACCESS_ID')) {
        return response()->json([
          'message' => 'error',
          'data' => 'Invalid Access ID or Access ID is Missing, The end point not valid'
        ], 406);
    }
    $Course = Course::where('status', '=', '1')->select('courses.*','users.name')->join('users','users.id','=','courses.id_author')->where('title', 'like', '%'.$keyword.'%')->paginate(10);
    return response()->json([
       'message' => 'success',
       'code' => 200,
       'data' => $Course
    ]);
  }

  public function enroll(Request $request){
    $validator = Validator::make($request->all(), [
      'user_id' => 'required',
      'course_id' => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'message' => 'error',
        'data' => $validator->errors(),
      ], 406);
    }
    if (!CourseUser::where(['user_id' => $request->user_id, 'course_id' => $request->course_id])->first()) 
    {
      $CourseUser = new CourseUser;
      $CourseUser->user_id = $request->user_id;
      $CourseUser->course_id = $request->course_id;
      $CourseUser->save();
      return response()->json([
        'message' => 'success',
        'code' => 200,
        'data' => 'Register Berhasil',
      ]);
    }
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => 'Kursus Sudah Terdaftar',
    ]);
    
  }

  public function join_private_class(Request $request){
    $Course = Course::where(['public' => '0', 'password' => $request->password])->first();
    if($Course){
      if($Course->archive == '0'){
        $CourseUser = CourseUser::where('course_id', $Course->id)->get()->count();
        if($CourseUser < 50){
          return response()->json([
            'error' => 0,
            'message' => 'success',
            'data' => $Course,
          ]);
        }else{
          return response()->json([
            'error' => 400,
            'message' => 'Kelas sudah penuh',
          ]);
        }
      }else{
        return response()->json([
          'error' => 400,
          'message' => 'Kelas sudah tidak aktif',
        ]);
      }
    }else{
      return response()->json([
        'error' => 400,
        'message' => 'Password atau kode yang Anda masukan salah',
      ]);
    }
  }

  public function my_course($id){
    $courses = Course::select('courses.*','users.name as author')
      ->join('courses_users','courses_users.course_id','=','courses.id')
      ->join('users','users.id','=','courses.id_author')
      ->where(['user_id' => $id])->orderBy('courses.id', 'desc')->get();

      $courses_data = [];
      foreach($courses as $course){
        $sections = Section::where('id_course', $course->id)->get();
        $num_contents = 0;
        foreach($sections as $section){
          $num_contents += count(Content::where('id_section', $section->id)->get());
        }
        $num_progress = count(DB::table('progresses')->where(['course_id' => $course->id, 'user_id' => $id, 'status' => '1'])->get());

        array_push($courses_data, [
          'num_contents' => $num_contents,
          'title' => $course->title,
          'image' => $course->image,
          'name' => $course->author,
          'id' => $course->id,
          'id_category' => $course->id_category,
          'num_progress' => $num_progress
        ]);
      }
      return response()->json([
        'message' => 'success',
        'code' => 200,
        'data' => $courses_data,
      ]);
  }

  public function category(){
    $Courses = Category::with('courses')->get();
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $Courses,
    ]);
  }
  public function _category($id_cat, Request $request){
    if ($request->get('provider')!='ingenio_access_id' || $request->get('access_id')!=env('MOBILE_LOGIN_ACCESS_ID')) {
        return response()->json([
          'message' => 'error',
          'data' => 'Invalid Access ID or Access ID is Missing, The end point not valid'
        ], 406);
    }
    $Course = Course::select('courses.*','users.name')->join('users','users.id','=','courses.id_author')->where(['status' => '1', 'id_category' => $id_cat])->paginate(10);
    return response()->json([
       'message' => 'success',
       'code' => 200,
       'data' => $Course
    ]);
  }

  public function show($id, Request $request){
    $data = [];
    $AuthorCourses = [];
    $totalStudents = 0;
    $authorTotalRating = 0;
    $authorCountRating = 0;
    $authorAverageRating = 0;

    $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $id)->first();
    if($Rating->count_rating != 0){
      $Rate = $Rating->rating / $Rating->count_rating;
    }else{
      $Rate = 0;
    }
    $Courses = DB::table('courses')->where(['id' => $id, 'status'=>'1'])->first();
    $dataAuthor = DB::table('users')->where('id', $Courses->id_author)->first();
    $GetAuthorCourses = DB::table('courses')->select('courses.*', 'courses.id as course_id', 'users.*', 'users.slug as user_slug', 'courses.slug as slug')
        ->where(['status' => '1', 'id_author' => $Courses->id_author])
        ->join('users','users.id','=','courses.id_author')
        ->orderBy('courses.id', 'desc')->get();
    $iter = 0;
    foreach($GetAuthorCourses as $getCourse){
      $RatingA = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $getCourse->course_id)->first();
      if($RatingA->count_rating != 0){
        $RateA = $RatingA->rating / $RatingA->count_rating;
      }else{
        $RateA = 0;
      }
      $array_ratingA = array('course_rating' => $RateA, 'course_count_rating' => $RatingA->count_rating);
      $contents_mergeA = array_merge((array)$GetAuthorCourses[$iter], $array_ratingA);
      array_push($AuthorCourses, $contents_mergeA);
      $iter++;
    }
    
    $totalCourse = count($GetAuthorCourses);
    foreach($GetAuthorCourses as $AuthorCourse){
      $totalStudents = $totalStudents + count(DB::table('courses_users')->where(['course_id' => $AuthorCourse->course_id])->get());
      $AuthorRatings = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $AuthorCourse->course_id)->first();
      $authorTotalRating = $authorTotalRating + $AuthorRatings->rating;
      $authorCountRating = $authorCountRating + $AuthorRatings->count_rating;
    }
    if ($authorCountRating != 0) {
      $authorAverageRating = $authorTotalRating / $authorCountRating;
    }
    
    $array_rating = array('average_rating' => round($authorAverageRating, 2), 'author' => $dataAuthor, 'author_courses' => $AuthorCourses, 'total_courses' => $totalCourse, 'total_students' => $totalStudents, 'rate' => $Rate, 'count' => ($Rating->count_rating != 0 ? $Rating->count_rating : 0));
    $contents_merge = array_merge((array)$Courses, $array_rating);

    array_push($data, $contents_merge);
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $data,
    ]);
  }

  public function _my_course($id){

  }

  public function show_category(){
    $Categories = DB::table('categories')->get();
    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $Categories,
    ]);
  }

  public function get_meeting_schedules($course_id){
    //$meeting_schedules = MeetingSchedule::where('course_id', $course_id)->orderBy('date', 'asc')->get();
    $meeting_schedules = DB::table('meeting_schedules')->where('course_id', $course_id)->orderBy('date', 'asc')->get();
    $meeting = [];

    foreach ($meeting_schedules as $meeting_schedule) {
      array_push($meeting, [
        'id' => $meeting_schedule->id,
        'course_id' => $meeting_schedule->course_id,
        'name' => $meeting_schedule->name,
        'description' => $meeting_schedule->description,
        'date' => $meeting_schedule->date,
        'time' => $meeting_schedule->time,
        'isReady' => ($meeting_schedule->date == date("Y-m-d") ? 1 : 0),
      ]);
    }

    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $meeting,
    ]);
  }

  public function learn($user_id){
    // $id = $request->id;
    // $user_id = $request->user_id;
    
    $course_users = CourseUser::where(['user_id' => $user_id])->get();
    $data = [];
    //==========Here
    foreach ($course_users as $course_user) {
      $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.status','1')
      ->where('courses.id', $course_user->course_id)->first();

      $CourseLive = DB::table('course_lives')
      ->where('course_id', $course_user->course_id)
      ->where('start_date', '<=', date("Y-m-d"))
      ->where('end_date', '>=', date("Y-m-d"));
      // if ($CourseLive->first()) {
      //   continue;
      // }
      if (!$courses) {
        continue;
      }
      $section_data = [];
      $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

      $num_contents = 0;
      foreach($sections as $section){
        $contents_get = DB::table('contents')
          ->where('status', '1')
          ->where('id_section', $section->id)
          ->orderBy('sequence', 'asc')
          ->get();
        $contents = [];
        for ($i=0; $i < count($contents_get) ; $i++) { 
          $count_progress = DB::table('progresses')->where(['content_id'=>$contents_get[$i]->id, 'user_id' => $user_id, 'status' => '1'])->get();
          if ($count_progress) {
            $content_progress = count($count_progress);
          }else{
            $content_progress = 0;
          }
          $ContentVideoQuiz = ContentVideoQuiz::where('content_id', $contents_get[$i]->id)->with('content_video_quiz_answers')->get();
          $array_progress = array('completion' => $content_progress, 'content_video_quiz' => $ContentVideoQuiz);
          $contents_merge = array_merge((array)$contents_get[$i], $array_progress);
          array_push($contents, $contents_merge);
        }
        /*$contents = DB::table('contents')->join('progresses', 'contents.id', '=', 'progresses.content_id')->where(['contents.id_section' => $section->id, 'progresses.user_id' => $user_id, 'progresses.course_id' => $id])->get();*/

        $quizzes = DB::table('quizzes')->where(['id_section' => $section->id, 'status' => '1'])->get();
        $assignments = DB::table('assignments')->where(['id_section' => $section->id, 'status' => '1'])->get();
        $num_contents += count($contents);

        if(count($contents) > 0 || count($quizzes) > 0){
        
          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
            'section_assignments' => $assignments,
          ]);

        }
        
      }

      $meeting = Meeting::where('course_id', $courses->id)->first();
      $discussions = Discussion::select('discussions.id as id','discussions.course_id','discussions.body','discussions.created_at','users.name','users.photo')
                    ->where(['course_id'=>$courses->id, 'status' => '1'])
                    ->join('users','users.id','=','discussions.user_id')
                    ->orderby('discussions.created_at','desc')
                    ->get();
      $discussion_data = [];
      foreach ($discussions as $discussion) {
        $discussion_answers = DB::table('discussions_answers')->select('discussions_answers.*', 'users.name', 'users.photo')->where(['discussion_id'=>$discussion->id, 'status' => '1'])->join('users','users.id','=','discussions_answers.user_id')->get();
        array_push($discussion_data, [
          'id' => $discussion->id,
          'course_id' => $discussion->course_id,
          'body' => $discussion->body,
          'created_at' => $discussion->created_at,
          'name' => $discussion->name,
          'photo' => $discussion->photo,
          'discussion_answers' => $discussion_answers
        ]);
      }

      $announcements = CourseAnnouncement::select('course_announcements.*','users.photo as photo','users.name')
        ->where('course_id', $courses->id)
        ->join('courses','course_announcements.course_id','=','courses.id')
        ->join('users','users.id','=','courses.id_author')
        ->orderby('course_announcements.created_at','desc')->get();

      $announcements_data = [];
      foreach($announcements as $announcement){
        $announcement_comments = DB::table('course_announcement_comments')->select('course_announcement_comments.*', 'users.name', 'users.photo')
          ->join('users','users.id','=','course_announcement_comments.user_id')
          ->where('course_announcement_id', $announcement->id)->get();
        array_push($announcements_data, [
          'id' => $announcement->id,
          'course_id' => $announcement->course_id,
          'title' => $announcement->title,
          'description' => $announcement->description,
          'status' => $announcement->status,
          'created_at' => $announcement->created_at,
          'photo' => $announcement->photo,
          'name' => $announcement->name,
          'announcement_comments' => $announcement_comments,
        ]);
      }
      // dd($announcements_data);

      //count percentage
      $num_progress = 0;
      $num_progress = count(Progress::where(['course_id'=>$courses->id, 'user_id' => $user_id, 'status' => '1'])->get());
      $percentage = 0;
      $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
      $percentage = 100 / $percentage;
      //count percentage

      //count rating
      $Rating = Rating::select(DB::raw('SUM(rating) as rating'), DB::raw('COUNT(rating) as count_rating'))->where('course_id', $courses->id)->first();
      if($Rating->count_rating != 0){
        $Rate = $Rating->rating / $Rating->count_rating;
      }else{
        $Rate = 0;
      }
      //count rating

      // get projects
      $Projects = CourseProject::where('course_id', $courses->id)->get();
      // get projects

      // get Broadcast
      $Broadcast = CourseBroadcast::where(['course_id' => $courses->id ,'status' => '1'])->orderBy('id', 'desc')->first();
      // get Broadcast

      $meeting_schedules = MeetingSchedule::where('course_id', $courses->id)->orderBy('date', 'asc')->get();
      
      array_push($data, [
        'course' => $courses,
        'sections' => $section_data,
        'meeting' => $meeting,
        'meeting_schedules' => $meeting_schedules,
        'discussions' => $discussion_data,
        'announcements' => $announcements_data,
        'projects' => $Projects,
        'num_contents' => $num_contents,
        'num_progress' => $num_progress,
        'percentage' => $percentage,
        'rate' => $Rate,
        'Broadcast' => $Broadcast,
        'IsLive' => is_liveCourse($courses->id) ? 1 : 0
      ]);
    }
    //======End Here

    return response()->json([
      'message' => 'success',
      'code' => 200,
      'data' => $data,
    ]);
  }


  //Front of web====================


  public function store_lib(Request $request){
    // $field = $request->field;
    // $value = $request->value;

    $file = false;
    $UserLibrary = new UserLibrary;
    if ($request->file) {
      $file = true;
      if ($request->type_content=='video') {
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->title = $request->original_file_name;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->full_path_file = $request->path_file;
        $UserLibrary->file_size	 = $request->file_size;
        $UserLibrary->video_duration = $request->video_duration;
        $UserLibrary->full_path_original = $request->full_path_original;
        $UserLibrary->type_content = $request->type_content;
        $UserLibrary->is_directory = 0;
        $UserLibrary->parent = $request->parent;
        $UserLibrary->save();
      }elseif($request->type_content=='file'){
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->title = $request->original_file_name;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->type_content = $request->type_content;
        $UserLibrary->full_path_file = 'https://drive.google.com/file/d/'.$request->path_file;
        $UserLibrary->is_directory = 0;
        $UserLibrary->parent = $request->parent;
        $UserLibrary->save();
      }
    }
    

    // $Section = Section::where('id', $request->section_id)->first();
    // $Content = Content::where('id', $request->id)->first(); // checking record content
    // if($Content){ // jika konten telah ada maka hanya melakaukan update konten
    //   // $folder = get_folder_course($Content->id_section); // get folder id
    //   $Content->type_content = $request->type_content;
    //   if($request->file){ // jika ada inputan file
    //     $Content->name_file = $request->name_file;
    //     $Content->path_file = $request->path_file;
    //     if($Content->type_content == 'video'){ // jika inputan file berupa video
    //       $Content->file_size = $request->file_size;
    //       $Content->video_duration = $request->video_duration;
    //       $Content->full_path_original = $request->full_path_original;
    //       $Content->save();
    //       // exec resize video
    //       file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
    //       // exec resize video

    //     }elseif($Content->type_content == 'file'){
    //       $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
    //       $Content->save();
    //     }
    //   }else{
    //     $Content->$field = $value;
    //     $Content->save();
    //   }

    // }else{ // jika belum ada konten maka buat konten baru
    //   // $folder = get_folder_course($request->section_id); // get folder id
    //   $Content = new Content;
    //   $Content->type_content = $request->type_content;
    //   $Content->id_section = $request->section_id;
    //   if($request->file){  // jika ada inputan file
    //     $Content->name_file = $request->name_file;
    //     $Content->path_file = $request->path_file;
    //     if($Content->type_content == 'video'){ // jika inputan file berupa video
    //       $Content->file_size = $request->file_size;
    //       $Content->video_duration = $request->video_duration;
    //       $Content->full_path_original = $request->full_path_original;
    //       $Content->save();

    //       // exec resize video
    //       file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
    //       // exec resize video

    //     }elseif($Content->type_content == 'file'){
    //       $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
    //       $Content->save();
    //     }
    //   }else{
    //     $Content->$field = $value;
    //     $Content->save();
    //   }
    // }

    return response()->json([
      'message' => 'success',
      'file' => $file
    ]);
  }

  //Front of web===================

}
