<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;

class LanguageController extends Controller
{
  public function ChangeLanguage(Request $request){
    if($request->ajax()){
      $request->session()->put(['locale' => $request->locale]);
      App::setLocale($request->locale);      
    }
  }
}
