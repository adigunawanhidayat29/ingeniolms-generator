<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\CourseProjectUser;
use App\Category;
use App\ProgramCourse;
use App\Program;
use App\Slider;
use App\Models\Banner;
use App\Models\Feature;
use App\Video;
use App\Theme;
use App\Degree;
use App\User;
use App\Community;
use App\Models\Language;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Storage;

class HomeController extends Controller
{

  public function index()
  {
    // set default locale
    if (Language::get()->count() > 1) {
      if (!\Session::has('locale')) {
        $language = Language::where('status', '1')->first();
        \Session::put('locale', $language->alias);
        \App::setLocale($language->alias);
      } else {
        \App::setLocale(\Session::get('locale'));
      }
    } else {
      $language = Language::where('status', '1')->first();
      \Session::put('locale', $language->alias);
      \App::setLocale($language->alias);
    }

    //dd(\App::getLocale());

    if (isset(Setting('redirect')->value) && Setting('redirect')->value != null && Setting('redirect')->value != "") {
      if (Setting('redirect')->value != 'false') {
        if (!\Auth::check()) {
          return redirect(Setting('redirect')->value);
        } else {
          return redirect('/my-course');
        }
      }
    }

    // get course on program
    $ProgramCourses = ProgramCourse::get();
    $data_course_id = [];
    foreach ($ProgramCourses as $ProgramCourse) {
      $data_course_id[] = $ProgramCourse->course_id;
    }
    $data_course_id = implode(',', $data_course_id);
    $data_course_id = explode(',', $data_course_id);
    // get course on program

    $setting_max_catalog_column = isset(Setting('max_catalog_column')->value) ? Setting('max_catalog_column')->value : 4;

    $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id', 'courses.description as course_description')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where('courses.status', '1')
      ->where('courses.public', '1')
      ->where('courses.archive', '0')
      ->whereNotIn('courses.id', $data_course_id)
      ->whereNotIn('courses.id', function ($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
      ->with('sections', 'contents')
      ->orderBy('courses.created_at', 'desc')
      ->paginate($setting_max_catalog_column);

    $course_populars = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->where('courses.status', '1')
      ->where('public', '1')
      ->where('courses.archive', '0')
      ->whereNotIn('courses.id', $data_course_id)
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
      ->withCount('participants')
      ->with('sections', 'contents')
      ->orderBy('participants_count', 'desc')
      ->paginate($setting_max_catalog_column);

    // dd($course_populars);

    $categories = Category::withCount('courses')
      ->where('parent_category', '0')
      ->orderBy('courses_count', 'desc')
      ->limit($setting_max_catalog_column)
      ->get();

    $programs = Program::orderBy('id', 'desc')
      ->limit(4)
      ->get();
    // dd($programs);

    $data = [
      'courses' => $courses,
      'programs' => $programs,
      'course_populars' => $course_populars,
      'projects' => CourseProjectUser::orderBy('id', 'desc')->limit(4)->get(),
      'categories' => $categories,
      'sliders' => Slider::where('status', '1')->get(),
      'banners' => Banner::where('status', '1')->get(),
      'features' => Feature::where('status', '1')->get(),
      'videos' => Video::where('status', '1')->get(),
      'degrees' => Degree::get(),
    ];

    $checkTheme = Theme::where(['status' => '1'])->first();
    if ($checkTheme) {
      $theme = $checkTheme->name . '/';
    } else {
      $theme = '';
    }

    return view($theme . 'home.index', $data);
  }

  public function index_()
  {

    if (Cache::has('home')) {
      $data = Cache::get('home');
    } else {
      // get course on program
      $ProgramCourses = ProgramCourse::get();
      $data_course_id = [];
      foreach ($ProgramCourses as $ProgramCourse) {
        $data_course_id[] = $ProgramCourse->course_id;
      }
      $data_course_id = implode(',', $data_course_id);
      $data_course_id = explode(',', $data_course_id);
      // get course on program

      $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id', 'courses.description as course_description')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->where('courses.status', '1')
        ->where('courses.public', '1')
        ->where('courses.archive', '0')
        ->whereNotIn('courses.id', $data_course_id)
        ->whereNotIn('courses.id', function ($query) {
          $query->select('course_id')->from('course_lives');
        })
        ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
        ->with('sections', 'contents')
        ->orderBy('courses.created_at', 'desc')
        ->paginate(6);

      // dd($courses);

      $course_populars = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
        ->where('courses.status', '1')
        ->where('public', '1')
        ->where('courses.archive', '0')
        ->whereNotIn('courses.id', $data_course_id)
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->leftJoin('course_lives', 'course_lives.course_id', '=', 'courses.id')
        ->withCount('participants')
        ->with('sections', 'contents')
        ->orderBy('participants_count', 'desc')
        ->paginate(6);

      // dd($course_populars);

      $categories = Category::withCount('courses')
        ->limit(6)
        ->orderBy('courses_count', 'desc')
        ->get();

      $programs = Program::orderBy('id', 'desc')
        ->limit(6)
        ->get();
      // dd($programs);

      $data = [
        'courses' => $courses,
        'programs' => $programs,
        'course_populars' => $course_populars,
        'projects' => CourseProjectUser::orderBy('id', 'desc')->limit(6)->get(),
        'categories' => $categories,
        'sliders' => Slider::where('status', '1')->get(),
        'videos' => Video::where('status', '1')->get(),
        'degrees' => Degree::get(),
      ];

      $expiresAt = now()->addMinutes(20);
      // Cache::put('home', $data, $expiresAt);
      Cache::store('redis')->put('home', $data, $expiresAt);
      $data = $data;
    }

    $checkTheme = Theme::where(['status' => '1'])->first();
    if ($checkTheme) {
      $theme = $checkTheme->name . '/';
    } else {
      $theme = '';
    }

    return view($theme . 'home.index', $data);
  }

  public function category($slug)
  {
    $Category = Category::where('slug', $slug)->first();
    $ParentCategory = Category::where('id', $Category->parent_category)->first();

    $categoriesWithCourses = Category::where('slug', $slug)->with('children', 'courses')->first();
    // dd($categoriesWithCourses);

    $courses = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->where(['courses.status' => '1', 'courses.id_category' => $Category->id])
      ->where('courses.public', '1')
      ->where('courses.archive', '0')
      ->whereNotIn('courses.id', function ($query) {
        $query->select('course_id')->from('course_lives');
      })
      ->orderBy('courses.id', 'desc')
      ->paginate(12);

    // $course_lives = Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug', 'courses.id as id')
    //   ->where(['courses.status'=>'1', 'courses.id_category' => $Category->id])
    //   ->join('users','users.id','=','courses.id_author')
    //   ->join('course_lives','course_lives.course_id','=','courses.id')
    //   ->orderBy('courses.id', 'desc')
    //   ->limit(12)
    //   ->get();

    $data = [
      'courses' => $courses,
      // 'course_lives' => $course_lives,
      'Category' => $Category,
      'ParentCategory' => $ParentCategory,
      'categoriesWithCourses' => $categoriesWithCourses,
    ];
    return view('home.category', $data);
  }

  public function search(Request $request)
  {
    $qword = $request->get('q');
    if ($qword == "" || $qword == null || empty($qword)) {
      $courses = Course::select('courses.*', 'users.name', 'users.slug as user_slug')
        ->where('status', '1')
        ->where('courses.public', '1')
        ->where('courses.archive', '0')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->with('sections', 'contents')
        ->get();

      $users = User::select('users.*')
        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('users_groups.level_id', '2')
        ->get();
    } else {
      $courses = Course::select('courses.*', 'users.name', 'users.slug as user_slug')
        ->where('status', '1')
        ->where('courses.public', '1')
        ->where('courses.archive', '0')
        ->where('courses.title', 'LIKE', '%' . $qword . '%')
        ->orWhere('users.name', 'LIKE', '%' . $qword . '%')
        ->where('status', '1')
        ->where('courses.public', '1')
        ->where('courses.archive', '0')
        ->join('users', 'users.id', '=', 'courses.id_author')
        ->with('sections', 'contents')
        ->get();

      $users = User::select('users.*')
        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('users_groups.level_id', '2')
        ->where('users.name', 'LIKE', '%' . $qword . '%')
        ->get();
    }

    $data = [
      'courses' => $courses,
      'users' => $users,
    ];

    return view('home.search', $data);
  }

  public function soon()
  {
    return view('home.cooming_soon');
  }

  public function privacy_policy()
  {
    return view('home.privacy_policy');
  }

  public function tnc()
  {
    return view('home.tnc');
  }

  public function contact_us()
  {
    return view('home.contact_us');
  }

  public function teacher()
  {
    $type = \Request::get('type');
    $community_id = \Request::get('id');
    $data = [];

    if ($type == 'all') {
      $users = User::select('users.*')
        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('users_groups.level_id', '2')
        ->paginate(20);
    } else {
      $users = User::select('users.*')
        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('users_groups.level_id', '2')
        ->inRandomOrder()
        ->limit(8)
        ->get();
    }

    if ($type == 'community') {
      $communities = Community::where('id', $community_id)->with('instructors')->get();
      $community = Community::where('id', $community_id)->with('instructors')->first();
      $instructors = $community->instructors()->paginate(20);
    } else {
      $communities = Community::with('instructors')->get();
      $instructors = [];
    }

    $data = [
      'users' => $users,
      'communities' => $communities,
      'instructors' => $instructors,
    ];

    return view('home.teacher', $data);
  }

  public function uploadify(Request $request, $page)
  {

    $uploadDir = '/uploads/';

    // Set the allowed file extensions
    $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

    $verifyToken = md5('unique_salt' . time());

    if (!empty($_FILES)) {
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      $file = $_FILES['Filedata']['tmp_name'];
      $tempFile   = 'upload-' . time() . \Auth::user()->id . '.' . $fileParts['extension'];
      $uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
      $targetFile = $uploadDir . $_FILES['Filedata']['name'];

      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'fileupload/' . $page . '/' . $tempFile, fopen($file, 'r+'), 'public'); //upload

      return response()->json([
        'status' => 200,
        'data' => $tempFile
      ], 200);
    }
  }

  public function uploadify_cancel(Request $request, $path)
  {
    $data = json_decode($request->data);

    Storage::disk(env('APP_DISK'))->delete(env('UPLOAD_PATH') . '/' . 'fileupload/' . $path . '/' . $data->data);

    return response()->json([
      'status' => 200,
      'data' => $request->data
    ], 200);
  }
}
