<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discussion;
use App\DiscussionAnswer;
use App\Course;
use Auth;
use File;

class DiscussionController extends Controller
{
  public function ask(Request $request)
  {
    $Discussion = new Discussion;
    $Discussion->user_id = Auth::user()->id;
    $Discussion->course_id = $request->course_id;
    $Discussion->status = '1';
    $Discussion->body = $request->body;
    $Discussion->description = $request->deskripsi;
    $Discussion->save();

    return redirect()->back();
  }

  public function answer(Request $request)
  {
    $user = Auth::user();
    $DiscussionAnswer = new DiscussionAnswer;
    $DiscussionAnswer->user_id = $user->id;
    $DiscussionAnswer->discussion_id = $request->discussion_id;
    $DiscussionAnswer->body = $request->body;
    $DiscussionAnswer->status = '1';

    // $files = scandir(public_path('jquery-upload/server/php/question/'));
    // $new_file = [];
    // foreach ($files as $key => $value) {
    //   if($value != '.' && $value != '..' && $value != 'thumbnail'){
    //     $id = substr($value,0,strlen(Auth::user()->id));
    //     if($id == Auth::user()->id){
    //       $new_file_name = time().'-'.$value;
    //       File::copy(public_path('jquery-upload/server/php/question/'.$value),public_path('files/learn/question/'.$new_file_name));
    //       $new_file[] = $new_file_name;
    //       File::delete(public_path('jquery-upload/server/php/question/thumbnail/'.$value));
    //       File::delete(public_path('jquery-upload/server/php/question/'.$value));
    //     }
    //   }
    // }

    $new_file = [];
    if($request->fileinput){
      $new_file = $request->fileinput;
    }
    $file = json_encode(array_values($new_file));

    $DiscussionAnswer->file = $file;
    // $DiscussionAnswer->file = count($new_file) == 0 ? null : implode(',',$new_file);
    $DiscussionAnswer->save();

    return redirect()->back();
  }

  public function detail($id)
  {
    $Discussion = Discussion::select('discussions.id as id', 'discussions.course_id', 'discussions.body', 'discussions.description', 'discussions.created_at', 'users.name', 'users.photo')
      ->where('discussions.id', $id)
      ->join('users', 'users.id', '=', 'discussions.user_id')
      ->first();

    $DiscussionAnswers = DiscussionAnswer::where(['discussion_id' => $id, 'status' => '1'])->join('users', 'users.id', '=', 'discussions_answers.user_id')->get();
    $Course = Course::where('id', $Discussion->course_id)->first();
    $data = [
      'discussion' => $Discussion,
      'discussion_asnwers' => $DiscussionAnswers,
      'course' => $Course,
    ];
    return view('discussion.detail', $data);
  }
}
