<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    
  public function index()
  {
      $data = [
        'videos' => Video::where('status', '1')->get(),
      ];
      return view('video.index', $data);
  }

}
