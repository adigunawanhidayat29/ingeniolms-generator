<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\CourseUser;
use App\Progress;
use App\CourseStudentGroupUser;
use App\CourseStudentGroup;
use App\CourseAttendance;
use App\CourseAttendanceUser;
use DB;
use DateTime;
use DateInterval;
use DatePeriod;

use Excel;
use App\Exports\CourseAttendanceReport;

class CourseAttendanceController extends Controller
{

  public function index($course_id)
  {
    $Course = Course::where('id', $course_id)->first();
    $CourseAttendances = CourseAttendance::with('course_attendance_user')->where('course_id', $course_id)->get();

    $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
      ->join('users', 'users.id', '=', 'courses_users.user_id')->get();

    $Reports = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), DB::raw('count(course_attendances_id) as count_attendances'), 'user_id')
      ->with('user')
      ->where('course_id', $course_id)->groupBy('user_id')->get();

    $CourseAttendancesSummary = CourseAttendanceUser::select(DB::raw('sum(status = "p") as sum_status_p'), DB::raw('sum(status = "l") as sum_status_l'), DB::raw('sum(status = "e") as sum_status_e'), DB::raw('sum(status = "a") as sum_status_a'), 'course_attendances.description', 'course_attendance_users.course_attendances_id')
      ->where('course_attendance_users.course_id', $course_id)->groupBy('course_attendances_id')
      ->join('course_attendances', 'course_attendances.id', '=', 'course_attendance_users.course_attendances_id')
      ->get();

    $data = [
      'Course' =>  $Course,
      'CourseAttendances' =>  $CourseAttendances,
      'courses_users' => $CoursesUsers,
      'reports' => $Reports,
      'CourseAttendancesSummary' => $CourseAttendancesSummary,
    ];

    // dd($data);

    return view('course.attendances', $data);
  }

  public function store(Request $request, $course_id)
  {

    $start_date = $request->datetime;
    $end_date = $request->end_date;

    if (isset($end_date)) { // jika terdapat multiple absensi
      $begin = new DateTime($start_date);
      $end = new DateTime($end_date);

      $interval = DateInterval::createFromDateString('1 day');
      $period = new DatePeriod($begin, $interval, $end);

      // dd($request->repeat_on);

      foreach ($period as $dt) {
        if (in_array(strtolower($dt->format("l")), $request->repeat_on)) {
          // echo $dt->format("l Y-m-d") . "<br>";

          $CourseAttendances = new CourseAttendance;
          $CourseAttendances->course_id = $course_id;
          $CourseAttendances->name = $dt->format("D, d M Y ");
          $CourseAttendances->description = 'Reguler';
          $CourseAttendances->datetime = $dt->format("Y-m-d H:i:s");
          $CourseAttendances->end_datetime = $dt->format("Y-m-d H:i:s");
          $CourseAttendances->manual_student = $request->manual_student;
          $CourseAttendances->save();
        }
      }
    } else {

      $CourseAttendances = new CourseAttendance;
      $CourseAttendances->course_id = $course_id;
      $CourseAttendances->description = $request->description;
      $CourseAttendances->datetime = $request->datetime;
      $CourseAttendances->end_datetime = $request->end_datetime;
      $CourseAttendances->name = date_format(date_create($request->datetime), "D, d M Y");
      $CourseAttendances->manual_student = $request->manual_student;
      $CourseAttendances->save();
    }

    return redirect()->back()->with('message', \Lang::get('front.page_manage_attendances.message_session_added'));
  }

  public function update(Request $request, $id)
  {
    $CourseAttendances = CourseAttendance::where('id', $id)->first();
    $CourseAttendances->description = $request->description;
    $CourseAttendances->datetime = $request->datetime;
    $CourseAttendances->end_datetime = $request->end_datetime;
    if($request->manual_student){
      $CourseAttendances->manual_student = 1;
    }else {
      $CourseAttendances->manual_student = null;
    }
    $CourseAttendances->save();

    return redirect()->back()->with('message', \Lang::get('front.page_manage_attendances.message_session_updated'));
  }

  public function delete($id)
  {
    $CourseAttendances = CourseAttendance::where('id', $id)->first();
    $CourseAttendanceUser = CourseAttendanceUser::where('course_attendances_id', $id)->delete();
    $CourseAttendances->delete();

    return redirect()->back()->with('message', \Lang::get('front.page_manage_attendances.message_session_deleted'));
  }

  public function submit(Request $request, $course_id, $id)
  {

    $CheckCourseAttendanceUser = CourseAttendanceUser::where(['course_id' => $course_id, 'course_attendances_id' => $id])->first();

    if ($CheckCourseAttendanceUser) {
      CourseAttendanceUser::where(['course_id' => $course_id, 'course_attendances_id' => $id])->delete();
    }

    foreach ($request->user_id as $index => $data) {

      $status = $data . '_status';
      $remarks = $data . '_remarks';

      $CourseAttendanceUser = new CourseAttendanceUser;
      $CourseAttendanceUser->course_attendances_id = $id;
      $CourseAttendanceUser->course_id = $course_id;
      $CourseAttendanceUser->user_id = $data;
      $CourseAttendanceUser->status = $request->$status;
      $CourseAttendanceUser->remarks = $request->$remarks;
      $CourseAttendanceUser->save();
    }

    return redirect()->back()->with('message', \Lang::get('front.page_manage_attendances.message_submit'));
  }

  public function submit_user(Request $request, $course_id, $id)
  {

    $CourseAttendanceUser = new CourseAttendanceUser;
    $CourseAttendanceUser->course_attendances_id = $id;
    $CourseAttendanceUser->course_id = $course_id;
    $CourseAttendanceUser->user_id = \Auth::user()->id;
    $CourseAttendanceUser->status = $request->status;
    $CourseAttendanceUser->remarks = $request->remarks;
    $CourseAttendanceUser->save();

    return redirect()->back()->with('message', \Lang::get('front.page_manage_attendances.message_submit_attendance'));
  }

  public function download($course_id)
  {

    return (new CourseAttendanceReport($course_id))->download('courses_attendances.xlsx');
  }
}
