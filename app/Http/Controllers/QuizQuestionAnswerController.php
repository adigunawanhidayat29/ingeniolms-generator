<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuizQuestionAnswer;
use App\QuizQuestion;

class QuizQuestionAnswerController extends Controller
{
  public function create(Request $request){
    // inser quiz question answer
      $answers = $request->answer;
      foreach($answers as $index => $value){
        if($value != NULL){
          $QuizQuestionAnswer = new QuizQuestionAnswer;
          $QuizQuestionAnswer->answer = $value;
          $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
          $QuizQuestionAnswer->quiz_question_id = $request->quiz_question_id;
          $QuizQuestionAnswer->save();
        }
      }
    // inser quiz question answer
    $QuizQuestion = QuizQuestion::where('id', $request->quiz_question_id)->first();
    \Session::flash('success', 'Create reccord success');
    return redirect('course/quiz/question/update/'.$QuizQuestion->quiz_id.'/'.$QuizQuestion->id);
  }

  public function update_action(Request $request){
    $QuizQuestionAnswer = QuizQuestionAnswer::where('id', $request->id)->first();
    $QuizQuestionAnswer->answer = $request->answer;
    $QuizQuestionAnswer->answer_correct = $request->answer_correct;
    $QuizQuestionAnswer->save();
  }

  public function delete($quiz_id, $id){
    $QuizQuestionAnswer = QuizQuestionAnswer::where('id', $id);
    if($QuizQuestionAnswer){
      $QuizQuestionAnswer->delete();
      \Session::flash('success', 'Delete reccord success');
    }else{
      \Session::flash('error', 'Delete reccord failed');
    }
    return redirect('course/quiz/question/manage/'.$quiz_id);
  }
}
