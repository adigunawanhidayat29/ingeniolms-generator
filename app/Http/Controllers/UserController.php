<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Course;
use App\User;
use Cookie;

class UserController extends Controller
{

  public function detail($slug){
    // set cookies
    if(!Cookie::get('affiliate')){
      if(\Request::get('aff')){
        Cookie::queue(Cookie::make('affiliate', \Request::get('aff'), 129600));
      }
    }
    // set cookies

    $user = DB::table('users')->where('slug', $slug)->first();
    if($user){
      $data = [
        'user' => $user,
        'courses' => Course::select('courses.*', 'users.*', 'users.slug as user_slug', 'courses.slug as slug')
        ->where('archive', '=', '0')
        ->where('public', '=', '1')
        ->where(['status' => '1', 'id_author' => $user->id])
        ->join('users','users.id','=','courses.id_author')
        ->orderBy('courses.id', 'desc')
        ->paginate(16),
      ];
      return view('user.detail', $data);
    }else{
      abort(404);
    }
  }

  public function _checkEmail(Request $request){
    $User = User::where('email', $request->email)->orWhere('username', $request->email)->first();
    if($User){
      return response()->json([
        'status' => 200,
        'data' => $User
      ]);
    }else{
      \Session::put('checkEmail', $request->email);
      return response()->json([
        'status' => 400,
      ]);
    }
  }

}
