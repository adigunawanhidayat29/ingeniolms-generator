<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\AffiliateTransaction;
use App\AffiliateUser;
use App\AffiliateSetting;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\Promotion;
use App\PromotionUser;
use Auth;
use Cart;
use Cookie;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionInformation;

class TransactionController extends Controller
{
  public function process(Request $request){
    $user = Auth::user();
    $invoice = rand(100000000,999999999);
    $unique_number = rand(100,999);
    $cookie_affiliate = Cookie::get('affiliate');

    if($cookie_affiliate){
      $affiliate = '?aff='.$cookie_affiliate;
      $AffiliateSetting = AffiliateSetting::where('status', '1')->orderBy('id', 'desc')->first();
      $AffiliateUser = AffiliateUser::where('url', $affiliate)->first();
      $AffiliateTransaction = new AffiliateTransaction;
      $AffiliateTransaction->affiliate_id = $AffiliateUser->id;
      $AffiliateTransaction->user_id = $user->id;
      $AffiliateTransaction->invoice = $invoice;
      $AffiliateTransaction->status = '0';
      $AffiliateTransaction->commission = $AffiliateSetting->commission;
      $AffiliateTransaction->save();
    }

    //insert transaction detail
    $subtotal = 0;
    foreach(Cart::content() as $cart){
      $subtotal += $cart->price;
      $TransactionDetail = new TransactionDetail;
      $TransactionDetail->invoice = $invoice;
      if($cart->options->type == 'course'){
        $TransactionDetail->course_id = $cart->id;
      }else{
        $TransactionDetail->program_id = $cart->id;
      }
      $TransactionDetail->total = $cart->price;
      $TransactionDetail->save();
    }
    //insert transaction detail

    if(Session::get('promotion')){
      $Promotion = Session::get('promotion');
      $PromotionUser = new PromotionUser;
      $PromotionUser->promotion_id = $Promotion->id;
      $PromotionUser->user_id = $user->id;
      $PromotionUser->invoice = $invoice;
      $PromotionUser->save();

      $discount_price = ($subtotal * $Promotion->discount) / 100;
      //insert transaction
      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->user_id = $user->id;
      $Transaction->method = $request->method;
      $Transaction->total = ($subtotal - $discount_price);
      $Transaction->subtotal = ($subtotal - $discount_price) + $unique_number;
      $Transaction->unique_number = $unique_number;
      $Transaction->promotion = '1';
      $Transaction->promotion_id = $Promotion->id;
      $Transaction->discount_code = $Promotion->discount_code;
      $Transaction->status = $Promotion->discount == 100 ? '1' : '0';
      $Transaction->save();
      //insert transaction

      $request->session()->forget('promotion'); // delete session promo

      if($Promotion->discount == 100){

        foreach(Cart::content() as $cart){
          if($cart->options->type == 'course'){
            $TransactionDetail->course_id = $cart->id;
            $CourseUser = new CourseUser;
            $CourseUser->user_id = Auth::user()->id;
            $CourseUser->course_id = $cart->id;
            $CourseUser->save();
          }
        }

        Cart::destroy();
        \Session::flash('message', 'success');
        return redirect('my-course');
      }

    }else{
      //insert transaction
      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->user_id = $user->id;
      $Transaction->method = $request->method;
      $Transaction->total = $subtotal;
      $Transaction->subtotal = $subtotal + $unique_number;
      $Transaction->unique_number = $unique_number;
      $Transaction->promotion = '0';
      $Transaction->affiliate_id = $cookie_affiliate ? $AffiliateUser->id : NULL;
      $Transaction->status = '0';
      $Transaction->save();
      //insert transaction
    }

    Cart::destroy();
    // Mail::to($user->email)->send(new TransactionInformation($Transaction));
    return redirect('transaction/information/'.$invoice);
  }

  public function information($invoice){
    $Transaction = Transaction::where('invoice', $invoice);
    $TransactionDetail = TransactionDetail::where('invoice', $invoice)->join('courses','courses.id','=','transactions_details.course_id');
    $data = [
      'transaction' => $Transaction->first(),
      'transactions_details' => $TransactionDetail->get(),
    ];
    return view('transaction.information', $data);
  }
  public function information_v2($invoice){
    $Transaction = Transaction::where('invoice', $invoice);
    $TransactionDetail = TransactionDetail::where('invoice', $invoice)->join('courses','courses.id','=','transactions_details.course_id');
    $data = [
      'transaction' => $Transaction->first(),
      'transactions_details' => $TransactionDetail->get(),
    ];
    return view('transaction.information-v2', $data);
  }

  public function confirmation($invoice){
    $data = [
      'invoice' => $invoice
    ];
    return view('transaction.confirmation', $data);
  }

  public function confirmation_action($invoice, Request $request){
    // check transaction
    if(Transaction::where('invoice', $invoice)->first()){

      $TransactionConfirmation = new TransactionConfirmation;

      if($request->file('file')){
        $destinationPath = asset_path('uploads/confirmations/'); // upload path
        $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
        $file = $invoice.'-'.rand(1111,9999).'.'.$extension; // renameing image
        $request->file('file')->move($destinationPath, $file); // uploading file to given path
        $TransactionConfirmation->file = '/uploads/confirmations/'.$file;
      }

      // save confirmation
      $TransactionConfirmation->invoice = $invoice;
      $TransactionConfirmation->account_name = $request->account_name;
      $TransactionConfirmation->account_number = $request->account_number;
      $TransactionConfirmation->bank_name = $request->bank_name;
      $TransactionConfirmation->save();
      // save confirmation

      return redirect('transaction/payment/confirmation/'.$invoice.'/information');
    }
    // check transaction
  }

  public function confirmation_information(){
    return view('transaction.confirmation_information');
  }

  public function mutations(){
    $transactions = Transaction::where(['status' => '0'])->get();
    foreach($transactions as $index => $transaction){
      // echo $transaction->subtotal;
      ${'moota'.$index}[] = json_decode(moota('bank/vq7WPqAkADe/mutation/search/'.$transaction->subtotal), TRUE);
      foreach(${'moota' . $index}[0]['mutation'] as $data){
        if($data['type'] == 'CR'){
          Transaction::where('subtotal', $transaction->subtotal)->update(['status' => '1']);
          echo 'updated';
        }
      }
    }
  }
}
