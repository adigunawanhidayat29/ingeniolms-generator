<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Certificate;
use App\Course;
use App\CourseUser;
use App\CeritificateCourseUser;

class CertificateController extends Controller
{
    public function index($course_id)
    {

        $course = Course::where('id', $course_id)->first();
        $course_users = CourseUser::where('course_id', $course_id)->join('users', 'users.id', '=', 'courses_users.user_id')->get();

        $data = [
            'course' => $course,
            'course_users' => $course_users,
        ];

        return view('certificate.index', $data);
    }

    public function store($course_id, Request $request)
    {

        $Certificate = new Certificate;

        if ($request->file('image')) {
            $destinationPath = asset_path('uploads/certificates/'); // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $image = rand(11111111, 999999999) . '.' . $extension; // renameing image
            $request->file('image')->move($destinationPath, $image); // uploading file to given path

            $Certificate->image = '/uploads/certificates/' . $image;
        }

        $Certificate->position = $request->position;
        $Certificate->course_id = $course_id;
        $Certificate->x_coordinate = $request->x_coordinate;
        $Certificate->y_coordinate = $request->y_coordinate;
        $Certificate->save();

        return redirect()->back();
    }

    public function update($course_id, Request $request)
    {

        $Certificate = Certificate::where('id', $request->id)->first();

        if ($request->file('image')) {
            $destinationPath = asset_path('uploads/certificates/'); // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $image = rand(11111111, 999999999) . '.' . $extension; // renameing image
            $request->file('image')->move($destinationPath, $image); // uploading file to given path

            $Certificate->image = '/uploads/certificates/' . $image;
        }

        $Certificate->position = $request->position;
        $Certificate->course_id = $course_id;
        $Certificate->x_coordinate = $request->x_coordinate;
        $Certificate->y_coordinate = $request->y_coordinate;
        $Certificate->save();

        return redirect()->back();
    }

    public function learn($course_id)
    {

        $data = [
            'certificate' => Certificate::where('course_id', $course_id)->first(),
        ];

        return view('certificate.learn', $data);
    }

    public function saveUsers($course_id, Request $request)
    {

        CeritificateCourseUser::where('course_id', $course_id)->delete();

        if ($request->user_id) {
            foreach ($request->user_id as $index => $data) {
                $CeritificateCourseUser = new CeritificateCourseUser;
                $CeritificateCourseUser->course_id = $course_id;
                $CeritificateCourseUser->user_id = $data;
                $CeritificateCourseUser->save();
            }
        }

        return redirect()->back()->with('success', \Lang::get('front.page_manage_ceritificate.message_certificate_updated'));
    }
}
