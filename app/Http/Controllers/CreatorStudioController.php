<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CreatorStudioController extends Controller
{

  public function index(){
    return view('creator_studio.index');
  }

  public function steps(){
    return view('creator_studio.steps');
  }
  
}
