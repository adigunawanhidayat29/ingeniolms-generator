<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\QuizType;
use App\Section;
use App\Course;
use App\QuestionBankAnswer;
use App\QuizPageBreak;
use App\QuestionBank;
use App\LibraryDirectoryGroup;
use Excel;
use Auth;

class SurveyQuestionController extends Controller
{
  public function manage($id){
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::select('id_course')->where('id', $quiz->id_section)->first();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    // dd($quiz_page_breaks);

    $quiz_questions = QuizQuestion::select('quiz_questions.*', 'quiz_types.type')
      ->where('quiz_id', $id)
      ->where('quiz_page_break_id', NULL)
      ->join('quiz_types','quiz_types.id' ,'=','quiz_questions.quiz_type_id')
      ->get();

    $group = LibraryDirectoryGroup::where('user_id', Auth::user()->id)->where('question_bank', 1)->orWhereHas('shared', function($query){
      $query->where('user_id', Auth::user()->id);
    })->get();

    $data = [
      'quiz' => $quiz,
      'section' => $section,
      'course' => Course::where('id', $section->id_course)->first(),
      'quiz_types' => QuizType::get(),
      'quiz_questions_without_page_breaks' => $quiz_questions,
      'action' => 'course/survey/update_action/',
      'method' => 'put',
      'button' => 'Update',
      'group' => $group,
      'id' => old('id', $quiz->id),
      'name' => old('name', $quiz->name),
      'description' => old('description', $quiz->description),
      'shuffle' => old('shuffle', $quiz->shuffle),
      'time_start' => old('time_start', $quiz->time_start),
      'time_end' => old('time_end', $quiz->time_end),
      'duration' => old('duration', $quiz->duration),
      'attempt' => old('attempt', $quiz->attempt),
      'quiz_types' => QuizType::get(),
      'quiz_page_breaks' => $quiz_page_breaks,
    ];
    return view('course.survey.survey_question_manage', $data);
  }

  public function create($id){
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::select('id_course')->where('id', $quiz->id_section)->first();
    $quiz_questions = QuizQuestion::
                      select('quiz_questions.*', 'quiz_types.type')
                      ->where('quiz_id', $id)
                      ->join('quiz_types','quiz_types.id' ,'=','quiz_questions.quiz_type_id')
                      ->get();
    $quiz_questions_answers = [];
    foreach($quiz_questions as $quiz_question){
      array_push($quiz_questions_answers, array(
        'question_answers' => QuizQuestionAnswer::where('quiz_question_id', $quiz_question->id)->get(),
        'question' => $quiz_question->question,
        'weight' => $quiz_question->weight,
        'type' => $quiz_question->type,
        'id' => $quiz_question->id,
      ));
    }

    $data = [
      'action' => 'course/survey/question/create_action/'.$quiz->id,
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'quiz_type_id' => old('quiz_type_id'),
      'question' => old('question'),
      'weight' => old('weight','1'),
      'quiz' => $quiz,
      'section' => $section,
      'quiz_types' => QuizType::get(),
      'quiz_questions_answers' => $quiz_questions_answers,
    ];
    return view('course.survey.survey_question_form', $data);
  }

  public function create_action($quiz_id, Request $request){
    $QuizPageBreak = QuizPageBreak::select('id')->where('quiz_id', $quiz_id)->orderBy('id','desc')->first();
    // insert Quiz Question
    $QuizQuestion = new QuizQuestion;

    $QuizQuestion->quiz_type_id = $request->quiz_type_id;
    $QuizQuestion->question = $request->question;
    $QuizQuestion->weight = $request->weight ? $request->weight : 0;
    $QuizQuestion->quiz_id = $quiz_id;
    $QuizQuestion->quiz_page_break_id = $QuizPageBreak->id;
    $QuizQuestion->save();
    $QuizQuestion_id = $QuizQuestion->id;
    // insert Quiz Question

    // inser quiz question answer
    $answers = $request->answer;
    if($answers){
      foreach($answers as $index => $value){
        if($value != NULL){
          $QuizQuestionAnswer = new QuizQuestionAnswer;
          $QuizQuestionAnswer->answer = $value;
          if($request->quiz_type_id == '3'){ // ordering type
            $QuizQuestionAnswer->answer_ordering = $request->answer_ordering[$index];
          }else{
            // $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
          }
          $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
          $QuizQuestionAnswer->save();
        }
      }
    }
    // inser quiz question answer

    \Session::flash('success', 'Create record success');
    return redirect('course/survey/question/manage/'.$quiz_id);
  }

  public function update($quiz_id, $id){
    $QuizQuestion = QuizQuestion::where('id', $id)->first();
    $quiz = Quiz::where('id', $quiz_id)->first();
    $section = Section::select('id_course')->where('id', $quiz->id_section)->first();

    $data = [
      'action' => 'course/quiz/question/update_action/'.$quiz_id,
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $QuizQuestion->id),
      'quiz_type_id' => old('quiz_type_id', $QuizQuestion->quiz_type_id),
      'question' => old('question', $QuizQuestion->question),
      'weight' => old('weight', $QuizQuestion->weight),
      'quiz_types' => QuizType::get(),
      'answers' => QuizQuestionAnswer::where('quiz_question_id', $id)->get(),
      'quiz' => $quiz,
      'section' => $section,
    ];
    return view('course.quiz_question_update', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show_quiz_library_course($quiz_id, $id)
  {
      $quiz = Quiz::find($quiz_id);
      $bank_soal = LibraryDirectoryGroup::find($id);

      $quiz_types = QuizType::get();

      return view('library.course_bank_soal_get', compact('bank_soal', 'quiz_types', 'quiz'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function get_question_create(Request $request, $quiz_id, $id)
  {
      $bank_soal = LibraryDirectoryGroup::find($id);
      $quiz = Quiz::find($quiz_id);
      $QuizPageBreak = QuizPageBreak::select('id')->where('quiz_id', $quiz_id)->orderBy('id','desc')->first();

      if($request->bank_question_id){
        foreach ($request->bank_question_id as $key => $value) {
          $bank_soal = QuestionBank::find($value);
          $QuizQuestion = new QuizQuestion;

          $QuizQuestion->quiz_type_id = $bank_soal->quiz_library_type_id;
          $QuizQuestion->question = $bank_soal->question;
          $QuizQuestion->weight = $bank_soal->weight ? $bank_soal->weight : 0;
          $QuizQuestion->quiz_id = $quiz_id;
          $QuizQuestion->quiz_page_break_id = $QuizPageBreak->id;
          $QuizQuestion->save();
          $QuizQuestion_id = $QuizQuestion->id;
          // insert Quiz Question

          // inser quiz question answer
          $answers = QuestionBankAnswer::where('question_bank_id', $bank_soal->id)->get();
          if($answers){
            foreach($answers as $index => $value){
              if($value->answer != NULL){
                $QuizQuestionAnswer = new QuizQuestionAnswer;
                $QuizQuestionAnswer->answer = $value->answer;
                if($bank_soal->quiz_library_type_id == '3'){ // ordering type
                  $QuizQuestionAnswer->answer_ordering = $value->answer_ordering;
                }else{
                  $QuizQuestionAnswer->answer_correct = $value->answer_correct;
                }
                $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                $QuizQuestionAnswer->save();
              }
            }
          }
        }

      }

      return redirect('course/quiz/question/manage/'.$quiz_id);
  }

  public function update_action($quiz_id, Request $request){
    $Quiz = Quiz::where('id', $quiz_id)->first();

    // update Quiz question
    $QuizQuestion = QuizQuestion::where('id', $request->id)->first();
    // $QuizQuestion->quiz_type_id = $request->quiz_type_id;
    $QuizQuestion->question = $request->question;
    $QuizQuestion->weight = $request->weight;
    $QuizQuestion->save();
    // update Quiz question

    // update quiz question answer
    $old_answers = $request->old_answer;
    if($old_answers){
      foreach($old_answers as $index => $value){
        if($value != NULL){
          $QuizQuestionAnswer = QuizQuestionAnswer::where('id', $request->old_answer_id[$index])->first();
          $QuizQuestionAnswer->answer = $value;
          if($request->quiz_type_id == '3'){
            $QuizQuestionAnswer->answer_ordering = $request->old_answer_ordering[$index];
          }else{
            $QuizQuestionAnswer->answer_correct = $request->old_answer_correct[$index];
          }
          $QuizQuestionAnswer->quiz_question_id = $request->id;
          $QuizQuestionAnswer->save();
        }
      }
    }
    // update quiz question answer

    // inser quiz question answer
    $answers = $request->answer;
    if($answers){
      foreach($answers as $index => $value){
        if($value != NULL){
          $QuizQuestionAnswer = new QuizQuestionAnswer;
          $QuizQuestionAnswer->answer = $value;
          if($request->quiz_type_id == '3'){
            $QuizQuestionAnswer->answer_ordering = $request->answer_ordering[$index];
          }else{
            $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
          }
          $QuizQuestionAnswer->quiz_question_id = $request->id;
          $QuizQuestionAnswer->save();
        }
      }
    }
    // inser quiz question answer

    // delete quiz answer
    $remove_answers = $request->remove_answers;
    if($remove_answers){
      QuizQuestionAnswer::whereIn('id', explode(',', $remove_answers))->delete();
    }
    // delete quiz answer

    \Session::flash('success', 'Update record success');
    return redirect('course/quiz/question/manage/'.$quiz_id);
  }

  public function delete($quiz_id, $id){
    $QuizQuestion = QuizQuestion::where('id', $id);
    $QuizQuestionAnswer = QuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->first()->id);
    if($QuizQuestion){
      $QuizQuestionAnswer->delete();
      $QuizQuestion->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }
    return redirect('course/quiz/question/manage/'.$quiz_id);
  }

  public function import($quiz_id){
    return view('course.quiz_import');
  }

  public function import_store($quiz_id, Request $request){
    // file
    $destinationPath = asset_path('uploads/quiz/'); // upload path
    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
    $file = rand(11111,999999).'.'.$extension; // renameing image
    $request->file('file')->move($destinationPath, $file); // uploading file to given path
    // file

    Excel::load(asset_path('uploads/quiz/'.$file), function($reader) use ($quiz_id) {
      $results = $reader->get();
      // dd($results);
      foreach($results as $row){
        // dd($row->correct);

        // create quiz question
        $Quiz_question = new QuizQuestion;
        $Quiz_question->quiz_id = $quiz_id;
        $Quiz_question->quiz_type_id = '1';
        $Quiz_question->question = $row->soal;
        $Quiz_question->weight = '10';
        $Quiz_question->save();
        // create quiz question

        for ($char = 'a'; $char <= 'e'; $char++) {
          if($row->$char != ""){
            // create quiz question
            $QuizQuestionAnswer = new QuizQuestionAnswer;
            $QuizQuestionAnswer->quiz_question_id = $Quiz_question->id;
            $QuizQuestionAnswer->answer = $row->$char;
            $QuizQuestionAnswer->answer_correct = $row->correct_answer == strtoupper($char) ? '1' : '0' ;
            $QuizQuestionAnswer->save();
            // create quiz question
          }
        }
      }
    });

    \Session::flash('success', 'Quiz import successfull');
    return redirect('course/quiz/question/manage/'. $quiz_id);
  }

  public function preview($id){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->paginate(1);
    // dd($quiz_page_breaks);
    $data = [
      'quiz' => $quiz,
      'quiz_page_breaks' => $quiz_page_breaks,
      'course' => Course::where('id', $section->id_course)->first(),
    ];
    return view('course.quiz_question_preview', $data);
  }

  public function preview_($id){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $quiz_questions_answers = [];
    $quiz_questions = DB::table('quiz_questions')->where('quiz_id', $quiz->id);
      if($quiz->shuffle == '1'){
        $quiz_questions = $quiz_questions->inRandomOrder();
      }
      $quiz_questions = $quiz_questions->get();

    $section = Section::find($quiz->id_section);

    foreach($quiz_questions as $quiz_question){
      $quiz_question_answers = DB::table('quiz_question_answers')
        ->where('quiz_question_id', $quiz_question->id)
        ->inRandomOrder()
        ->get();
      array_push($quiz_questions_answers, array(
        'id' => $quiz_question->id,
        'question' => $quiz_question->question,
        'quiz_type_id' => $quiz_question->quiz_type_id,
        'question_answers' => $quiz_question_answers,
      ));
    }

    $data = [
      'quiz' => $quiz,
      'quiz_questions_answers' => $quiz_questions_answers,
      'course' => Course::where('id', $section->id_course)->first(),
    ];
    return view('course.quiz_question_preview', $data);
  }

  public function create_pagebreak($quiz_id, Request $request){
    $QuizPageBreak = new QuizPageBreak;
    $QuizPageBreak->quiz_id = $quiz_id;
    $QuizPageBreak->title = $request->title;
    $QuizPageBreak->description = $request->description;
    $QuizPageBreak->save();

    return redirect()->back();
  }

  public function update_sequence(Request $request){
    if($request->datas){
      foreach($request->datas as $key => $value){
        $QuizQuestion = QuizQuestion::find($value);
        $QuizQuestion->quiz_page_break_id = $request->quiz_page_break_id;
        $QuizQuestion->sequence = $key;
        $QuizQuestion->save();
      }
    }
  }

}
