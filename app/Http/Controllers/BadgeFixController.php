<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BadgeFix;
use App\BadgeCriteriaAssign;
use App\Course;

class BadgeFixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $badges = BadgeFix::where('course_id', $id)->get();
        $course = Course::where('id', $id)->first();

        $data = [
            'badges' => $badges,
            'course' => $course
        ];
        return view('badge_fix.index', $data);
    }

    public function index_user($id)
    {
        $badges = BadgeFix::where('course_id', $id)->where('publish', '1')->get();
        $course = Course::where('id', $id)->first();

        $data = [
            'badges' => $badges,
            'course' => $course
        ];

        return view('badge_fix.index_user', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($course_id)
    {
        $course = Course::where('id', $course_id)->first();
        return view('badge_fix.create', compact('course_id', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $badge = new BadgeFix;
        $badge->name = $request->name;
        $badge->description = $request->description;
        $badge->course_id = $request->course_id;
        $badge->complete_criteria = 'any';

        if($request->img != null) {
            $fileName = time().'_'.$request->img->getClientOriginalName();
            $filePath = $request->file('img')->storeAs('badge_content', $fileName, 'public');
            $badge->img = $fileName;
        }
    
        $badge->save();

        $data = [
            'badge' => $badge
        ];
        
        // return view('badge_criteria.create', $data);
        return redirect($request->course_id. '/list_badges');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $badge = BadgeFix::find($id);
        $badge->complete_criteria = $request->complete_criteria;
        $badge->save();

        return redirect($badge->course_id. "/" .$id. "/badge_overview");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function publish($id)
    {
        $badge = BadgeFix::find($id);
        $badge->publish = 1;
        $badge->save();

        return redirect($badge->course_id. '/list_badges');
    }

    public function unpublish($id)
    {
        $badge = BadgeFix::find($id);
        $badge->publish = 0;
        $badge->save();

        return redirect($badge->course_id. '/list_badges');
    }

    public function overview($course_id, $badge_id)
    {
        $badges_criteria = BadgeCriteriaAssign::join('badges_fix', 'badges_criteria_assign.badge_id', '=', 'badges_fix.id')
        ->join('badges_criteria', 'badges_criteria_assign.criteria_id', '=', 'badges_criteria.id')
        ->where('badge_id', $badge_id)
        ->select('badges_criteria.name', 'badges_criteria.id as criteria_id', 'badges_fix.name as badges_name', 'badges_fix.id as badge_id', 'badges_criteria.description as criteria_description', 'badges_fix.description as badges_description')
        ->get();

        $course = Course::where('id', $course_id)->first();

        $badge = BadgeFix::where('id', $badge_id)->first();
        
        $data = [
            'badges_criteria' => $badges_criteria,
            'badge_id' => $badge_id,
            'course' => $course,
            'badge' => $badge
        ];

        return view('badge_fix.overview', $data);
    }
}
