<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\CourseUserImport;

class CourseImportController extends Controller
{
    public function atendee ($course_id, Request $request) {
        $destinationPath = 'imports/courses/enroll/'; // upload path
        $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
        $file = str_slug($request->file('file')->getClientOriginalName()).time().'.'.$extension; // renameing image
        $request->file('file')->move($destinationPath, $file); // uploading file to given path

        Excel::import(new CourseUserImport($course_id), $destinationPath . $file);

        return redirect()->back()->with('success', 'All good!');
    }
}
