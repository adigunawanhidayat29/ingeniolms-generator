<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserGroup;
use App\Instructor;
use RegistersUsers;
use Auth;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\InstructorJoin;
use Session;

class InstructorController extends Controller
{
  public function index()
  {
    return view('instructor.index');
  }

  public function join()
  {
    if(Auth::check()){
      if(Instructor::where(['user_id'=>Auth::user()->id])->first()){
        return redirect('instructor/waiting');
      }else{
        // if logged in and not registered instructor, just insert instructor table

        // insert instructor
        $Instructor = new Instructor;
        $Instructor->user_id = Auth::user()->id;
        $Instructor->status = '0';
        $Instructor->save();
        // insert instructor

        Mail::to([Auth::user()->email, 'tsauri@dataquest.co.id'])->send(new InstructorJoin($Instructor));
        return redirect('instructor/waiting');
        // if logged in and not registered instructor , just insert instructor table
      }
    }else{
      return redirect('register?instructor=true');
    }
  }

  public function join_action(Request $request){

    $validation = Validator::make($request->all(), [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);

    if($validation->fails()){
      return redirect()->back()->withErrors($validation->errors());
    }else{
      $Register = User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password),
      ]);

      // insert instructor
      $Instructor = new Instructor;
      $Instructor->user_id = $Register['id'];
      $Instructor->status = '0';
      $Instructor->save();
      // insert instructor

      Mail::to($request->email)->send(new InstructorJoin($Instructor));
      return redirect('instructor/waiting');
    }

  }

  public function waiting(){
    return view('instructor.waiting');
  }

  public function redirect_facebook(){
    Session::put('register_instructor', 'true');
    return redirect('auth/facebook');
  }

  public function redirect_google(){
    Session::put('register_instructor', 'true');
    return redirect('auth/google');
  }

}
