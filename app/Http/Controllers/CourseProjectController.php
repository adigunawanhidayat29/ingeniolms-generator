<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\CourseProject;
use App\CourseProjectUser;
use App\CourseProjectComment;
use App\CourseProjectLike;
use Auth;
use Image;

class CourseProjectController extends Controller
{
  public function all(){
    $CourseProjectUsers = CourseProjectUser::orderBy('course_project_users.id', 'desc')
      ->join('users','users.id','=','course_project_users.user_id')
      ->paginate(6);
    $data = [
      'CourseProjectUsers' => $CourseProjectUsers
    ];
    return view('course_project_user.index', $data);
  }

  public function create($course_project_id){
    $CourseProject = CourseProject::where('id', $course_project_id)->first();
    if($CourseProject){
      $data = [
        'action' => '/course/project-user/store/' . $CourseProject->id,
        'CourseProject' => $CourseProject,
        'title' => '',
        'description' => '',
        'image' => '',
      ];
      return view('course_project_user.form', $data);
    }else{
      return abort(404);
    }
  }

  public function store($course_project_id, Request $request){
    $CourseProjectUser = new CourseProjectUser;

    // image
    $destinationPath = asset_path('uploads/course_project_users/'); // upload path
    $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
    $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
    $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath. "small_".$image);
    $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath. "medium_".$image);
    $request->file('image')->move($destinationPath, $image); // uploading file to given path
    // image

    $CourseProjectUser->course_project_id = $course_project_id;
    $CourseProjectUser->user_id = Auth::id();
    $CourseProjectUser->title = $request->title;
    $CourseProjectUser->slug = str_slug($request->title);
    $CourseProjectUser->description = $request->description;
    $CourseProjectUser->image = '/uploads/course_project_users/'.$image;
    $CourseProjectUser->save();

    return redirect('course/project-user/show/' . str_slug($request->title));
  }

  public function edit($course_project_id, $course_project_user_id){
    $CourseProject = CourseProject::where('id', $course_project_id)->first();
    $CourseProjectUser = CourseProjectUser::where('id', $course_project_user_id)->first();
    if($CourseProject){
      $data = [
        'action' => '/course/project-user/update/' . $course_project_user_id,
        'CourseProject' => $CourseProject,
        'title' => $CourseProjectUser->title,
        'description' => $CourseProjectUser->description,
        'image' => $CourseProjectUser->image,
      ];
      return view('course_project_user.form', $data);
    }else{
      return abort(404);
    }
  }

  public function update($course_project_user_id, Request $request){
    $CourseProjectUser = CourseProjectUser::where('id', $course_project_user_id)->first();

    // image
    if($request->file('image')){
      $destinationPath = asset_path('uploads/course_project_users/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath. "small_".$image);
      $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath. "medium_".$image);
      $request->file('image')->move($destinationPath, $image); // uploading file to given path

      $CourseProjectUser->image = '/uploads/course_project_users/'.$image;
    }
    // image

    $CourseProjectUser->title = $request->title;
    $CourseProjectUser->slug = str_slug($request->title);
    $CourseProjectUser->description = $request->description;
    $CourseProjectUser->save();

    return redirect('course/project-user/show/' . str_slug($request->title));
  }

  public function show($slug){
    $CourseProjectUser = CourseProjectUser::where('slug', $slug)->first();
    if($CourseProjectUser){
      $CourseProjectUserComments = CourseProjectComment::where('course_project_user_id', $CourseProjectUser->id)
        ->join('users', 'users.id', '=', 'course_project_comments.user_id')
        ->orderBy('course_project_comments.id', 'desc')
        ->get();

      $CourseProjectUserLikes = CourseProjectLike::where('course_project_user_id', $CourseProjectUser->id)->count();

      $data = [
        'CourseProjectUser' => $CourseProjectUser,
        'CourseProjectUserComments' => $CourseProjectUserComments,
        'CourseProjectUserLikes' => $CourseProjectUserLikes,
      ];
      return view('course_project_user.show', $data);
    }else{
      return abort('404');
    }
  }

  public function comment($course_project_user_id, Request $request){
    $CourseProjectComment = new CourseProjectComment;

    $CourseProjectComment->course_project_user_id = $course_project_user_id;
    $CourseProjectComment->user_id = Auth::id();
    $CourseProjectComment->comment = $request->comment;
    $CourseProjectComment->save();

    return redirect()->back();
  }

  public function like($course_project_user_id, Request $request){
    $CourseProjectLike = new CourseProjectLike;

    $CourseProjectLike->course_project_user_id = $course_project_user_id;
    $CourseProjectLike->user_id = Auth::id();
    $CourseProjectLike->save();

    return redirect()->back();
  }
}
