<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Validator;
use Storage;
use App\Program;
use App\Course;
use App\CourseSetting;
use App\DefaultActivityCompletion;
use App\ActivityCompletion;
use App\CourseLive;
use App\CourseBatch;
use App\Section;
use App\Content;
use App\CourseUser;
use App\Meeting;
use App\Progress;
use App\Discussion;
use App\Quiz;
use App\QuizQuestion;
use App\QuizParticipant;
use App\QuizQuestionAnswer;
use App\Assignment;
use App\CourseAnnouncement;
use App\Rating;
use App\CourseProject;
use App\Category;
use App\CourseBroadcast;
use App\ContentVideoQuiz;
use App\PlayerIdUser;
use App\MeetingSchedule;
use App\UserLibrary;
use App\User;
use App\ProgramCourse;
use App\AssignmentAnswer;
use App\CourseUpdate;
use App\Theme;
use App\InstructorGroup;
use App\LibraryDirectory;
use App\LibraryDirectoryGroup;
use App\InstructorGroupLibrary;
use App\Certificate;
use App\CeritificateCourseUser;
use App\ProgramUser;
use App\ActivityOrder;
use App\RestrictAccess;
use App\CourseStudentGroup;
use App\CourseStudentGroupUser;
use App\GroupHasContent;
use App\CourseAttendance;
use App\CourseAttendanceUser;
use Auth;
use File;
use Cart;
use Illuminate\Support\Facades\Mail;
use App\Mail\Course as MailCourse;
use Image;
use Plupload;
use FFMpeg;
use Cookie;

class CourseProgressController extends Controller
{
    public function index($course_id){
        $user = Auth::user();
        // {{ For checking the author of the course, please pull snippets below on server }}
        $UserAuthorId = Auth::user()->id; // {{ Getting signed in instructor }}
    
        $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo','course_levels.id as level_id')
          ->leftJoin('users', 'users.id', '=', 'courses.id_author')
          ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
          ->leftJoin('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
          ->where('courses.id', $course_id)->first();
    
        $courses_all = Course::where('id_author', 'like', '%'.Auth::user()->id.'%')->where('id', '!=', $courses->id)->get();
    
        $section_data = [];
        $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence','asc')->get();
    
        $num_contents = 0;
    
        foreach($sections as $index => $section){
    
          $section_all = [];
    
          $contents = Content::with('activity_order', 'progress_users')->where('id_section', $section->id)
            ->orderBy('contents.sequence', 'asc')
            ->get();
          $quizzes = Quiz::with('activity_order', 'progress_users')->where(['id_section' => $section->id])->get();
          $assignments = Assignment::with('activity_order', 'progress_users')->where(['id_section' => $section->id])->get();
          $num_contents += count($contents);
    
          foreach ($contents as $content) {
            // dd($content);
            $content->section_type = 'content';
            $section_all[] = $content;
          }
    
          foreach ($quizzes as $quizze) {
            $quizze->section_type = 'quizz';
            $section_all[] = $quizze;
          }
    
          foreach ($assignments as $assignment) {
            $assignment->section_type = 'assignment';
            $section_all[] = $assignment;
          }
    
          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'status' => $section->status,
            'sequence' => $section->sequence,
            'description' => $section->description,
            'id_course' => $section->id_course,
            'created_at' => $section->created_at,
            'updated_at' => $section->updated_at,
            'section_contents' => $contents,
            'section_quizzes' => $quizzes,
            'section_assignments' => $assignments,
            'section_all' => $section_all,
          ]);
        }
    
        $data = [
          'course' => $courses,
          'course_all' => $courses_all,
          'sections' => $section_data,
        ];

        return view('course.access_content', $data);
      }
}
