<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Assignment;
use App\AssignmentAnswer;
use App\AssignmentGrade;
use App\Course;
use App\Progress;
use App\Content;
use App\Quiz;
use App\CourseStudentGroup;
use App\GroupHasContent;
use App\CourseStudentGroupUser;

use Auth;
use Storage;
use Plupload;
use Validator;

class AssignmentController extends Controller
{
  public function create($id){
    $section = Section::where('id', $id)->first();

    $data = [
      'action' => 'course/assignment/create_action/'.$section->id,
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'description' => old('description'),
      'type' => old('type'),
      'attempt' => old('attempt', '3'),
      'time_start' => old('time_start', date("Y-m-d")),
      'time_end' => old('time_end', date("Y-m-d")),
      'section' => $section,
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get(),
      'group_has_content' => [],
    ];
    return view('assignment.form', $data);
  }

  public function create_action($section_id, Request $request){

    $validator = \Validator::make($request->all(), [
      'fileToUpload' => 'mimes:doc,docx,pdf,txt|max:2048',
      'description' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator);
    }

    $section = Section::where('id', $section_id)->first();

    // insert Assignment
    $Assignment = new Assignment;
    $Assignment->title = $request->title;
    $Assignment->description = $request->description;
    $Assignment->type = $request->type;
    if($request->file('fileToUpload')){
      if ($file = $request->file('fileToUpload')) {
        // $destinationPath = 'public/assignment/'; // upload path
        $name_file = $request->title.'-'.date('YmdHis') . "." . $file->getClientOriginalExtension();
        // $request->file('fileToUpload')->move($destinationPath, $profilefile);
        $folder = get_folderName_course($section_id); // get folder course by section id
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/assignments/' . $name_file, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/assignments/', $name_file);
        $Assignment->file = $path_file;
      }
    }
    $Assignment->attempt = $request->attempt;
    $Assignment->time_start = $request->time_start;
    $Assignment->time_end = $request->time_end;
    $Assignment->id_section = $section_id;
    $Assignment->status = '1';
    $Assignment->save();
    // insert Assignment

    // insert grouphascontent if isset
    if (isset($request->course_student_group)) {
      foreach ($request->course_student_group as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $Assignment->id, 'table' => 'assignments', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'assignments';
          $GroupHasContent->table_id = $Assignment->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
    }
    // insert grouphascontent if isset

    \Session::flash('success', 'Create Assignment success');
    return redirect('course/preview/'.$section->id_course);
  }

  public function create_draft($section_id, Request $request){
      // insert Assignment
      $Assignment = new Assignment;
      $Assignment->title = $request->title;
      if ($request->description == null)
      {
          $Assignment->description = ' ';
      }
      else{
          $Assignment->description = $request->description;
      }
      $Assignment->type = $request->type;
      $Assignment->attempt = $request->attempt;
      $Assignment->time_start = $request->time_start;
      $Assignment->time_end = $request->time_end;
      $Assignment->id_section = $section_id;
      $Assignment->status = '0';
      $Assignment->save();
      // insert Assignment

      \Session::flash('success', 'Create Assignment Draft success');
  }

  public function update($id){
    $Assignment = Assignment::where('id', $id)->first();
    $section = Section::where('id', $Assignment->id_section)->with('course')->first();

    $data = [
      'action' => 'course/assignment/update_action/',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Assignment->id),
      'title' => old('title', $Assignment->title),
      'description' => old('description', $Assignment->description),
      'type' => old('type', $Assignment->type),
      'attempt' => old('attempt', $Assignment->attempt),
      'time_start' => old('time_start', $Assignment->time_start),
      'time_end' => old('time_end', $Assignment->time_end),
      'section' => $section,
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get(),
      'group_has_content' => GroupHasContent::where(['table' => 'assignments', 'table_id' => $id])->get(),
    ];
    return view('assignment.form', $data);
  }

  public function update_action(Request $request){
    $Assignment = Assignment::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Assignment->id_section)->first();

    $Assignment->title = $request->title;
    $Assignment->description = $request->description;
    $Assignment->type = $request->type;
    $Assignment->attempt = $request->attempt;
    $Assignment->time_start = $request->time_start;
    $Assignment->time_end = $request->time_end;
    $Assignment->save();

    // insert grouphascontent if isset
    if (isset($request->course_student_group)) {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $request->id, 'table' => 'assignments'])->delete();
      foreach ($request->course_student_group as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $Assignment->id, 'table' => 'assignments', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'assignments';
          $GroupHasContent->table_id = $Assignment->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
    } else {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $request->id, 'table' => 'assignments'])->delete();
    }
    // insert grouphascontent if isset

    \Session::flash('success', 'Create Assignment success');
    return redirect('course/preview/'.$Section->id_course);
  }

  public function publish($Assignment_id){
    $Assignment = Assignment::where('id', $Assignment_id)->first();
    $Section = Section::where('id', $Assignment->id_section)->first();
    $Assignment->status = '1';
    $Assignment->save();

    \Session::flash('success', 'Tugas berhasil di publish');
    return redirect()->back();
  }

  public function unpublish($Assignment_id){
    $Assignment = Assignment::where('id', $Assignment_id)->first();
    $Section = Section::where('id', $Assignment->id_section)->first();
    $Assignment->status = '0';
    $Assignment->save();

    \Session::flash('success', 'Tugas berhasil di publish');
    return redirect()->back();
  }

  public function delete($course_id, $id){
    $Assignment = Assignment::where('id', $id);
    if($Assignment->first()){
      $AssignmentAnswer = AssignmentAnswer::where('assignment_id', $Assignment->first()->id);
      if($AssignmentAnswer->first()){
        $AssignmentAnswer->delete();
      }
      $Assignment->delete();
      \Session::flash('success', 'Delete Assignment success');
    }else{
      \Session::flash('error', 'Delete Assignment failed');
    }
    return redirect('course/preview/'.$course_id);
  }

  public function detail($slug, $id){
    $AssignmentAnswer = AssignmentAnswer::where(['assignments_answers.assignment_id'=>$id, 'user_id' => Auth::user()->id])->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');
    $Assignment = Assignment::where('id', $id)->first();
    $userId = Auth::user()->id;
    $section = Section::where('id',$Assignment->id_section)->first();
    $course = Course::find($section->id_course);
    $authorId = Course::where('id',$section->id_course)->first()->id_author;
    $content_default_setting = $course->default_activity_completion->where('type_content', 'assigment')->first();
    if($Assignment->setting){
      $content_default_setting = $Assignment->setting;
    }

    if($content_default_setting->required_view == 1 && $content_default_setting->required_grade == 0 && $content_default_setting->required_submit == 0){
      if(!Progress::where(['assignment_id' => $id, 'user_id' => $userId])->first()){
        // insert progress
        $Progress = new Progress;
        $Progress->assignment_id = $id;
        $Progress->course_id = $course->id;
        $Progress->user_id = $userId;
        $Progress->status = '1';
        $Progress->save();
        // insert progress
      }else{
        $Progress = Progress::where(['assignment_id' => $id, 'user_id' => $userId])->first();
        $Progress->status = '1';
        $Progress->save();
      }
    }
    if(date("Y-m-d H:i:s") <= $Assignment->time_end){
      $courses = Course::where('slug', $slug)->with('sections')->first();

      $section_data = [];
      $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

      $num_contents = 0;
      foreach($sections as $section){
        $section_all = [];

        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
        $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->orderBy('contents.sequence', 'asc')
          ->get();

        $group_content_id = [];
        foreach ($group_has_contents as $data) {
          array_push($group_content_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->where(function ($query) use ($group_content_id) {
          $query->whereNotIn('id', $group_content_id);
        })
        ->orderBy('contents.sequence', 'asc')
        ->get();
        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->orderBy('contents.sequence', 'asc')
        ->get();
        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
        $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->get();

        $group_quiz_id = [];
        foreach ($group_has_quizzes as $data) {
          array_push($group_quiz_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
          $query->whereNotIn('id', $group_quiz_id);
        })->get();
        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
        $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->get();

        $group_assignment_id = [];
        foreach ($group_has_assignments as $data) {
          array_push($group_assignment_id, $data->table_id);
        }
        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
          $query->whereNotIn('id', $group_assignment_id);
        })->get();
        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
        $num_contents += count($quizzes);
        $num_contents += count($assignments);

        // MERGING KONTEN
        foreach ($contents as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        foreach ($group_has_contents_users as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        // MERGING KONTEN

        // MERGING KUIS
        foreach ($quizzes as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        foreach ($group_has_quizzes_users as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        // MERGING KUIS

        // MERGING TUGAS
        foreach ($assignments as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        foreach ($group_has_assignments_users as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        // MERGING TUGAS

        array_push($section_data, [
          'id' => $section->id,
          'title' => $section->title,
          'description' => $section->description,
          'id_course' => $section->id_course,
          'created_at' => $section->created_at,
          'updated_at' => $section->updated_at,
          'section_all' => $section_all,
        ]);
      }

      $data = [
        'assignment' => $Assignment,
        'assignments_answers' => $AssignmentAnswer->get(),
        'course' => $courses,
        'userId' => $userId,
        'authorId' => $authorId,
        'sections' => $section_data,
      ];
      if($AssignmentAnswer->count() < $Assignment->attempt){ // check answer attempt
        return view('assignment.detail', $data);
      }else{
        \Session::flash('success', 'You already answer this assignment :)');
        return view('assignment.my_answer', $data);
      }
    }else{
      return redirect('/');
    }
  }

  public function post($assignment_id, Request $request){
    $User = Auth::user();
    $Assignment = Assignment::where('id', $assignment_id)->first();
    $section = Section::with('course')->where('id_course', $Assignment->id_section)->first();

    if($Assignment->type == '0'){

      $validator = Validator::make($request->all(),[
        'answer' => 'required',
      ]);

      if($validator->fails()){
        return redirect()->back()->withErrors($validator);;
      }

      $answer = $request->answer;

      $GroupHasContent = GroupHasContent::where(['table' => 'assignments', 'table_id' => $assignment_id])
        ->join('course_student_groups', 'course_student_groups.id', '=', 'group_has_contents.course_student_group_id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', '=', 'course_student_groups.id')
        ->where('course_student_group_users.user_id', $User->id)
        ->first();


      if ($GroupHasContent) {
        $course_student_group_users = CourseStudentGroupUser::where('course_student_group_id', $GroupHasContent->course_student_group_id)->get();

        foreach ($course_student_group_users as $data) {
          $AssignmentAnswer = new AssignmentAnswer;
          $AssignmentAnswer->assignment_id = $assignment_id;
          $AssignmentAnswer->answer = $answer;
          $AssignmentAnswer->user_id = $data->user_id;
          $AssignmentAnswer->description = $request->description;
          $AssignmentAnswer->save();
        }

      } else {

        $AssignmentAnswer = new AssignmentAnswer;
        $AssignmentAnswer->assignment_id = $assignment_id;
        $AssignmentAnswer->answer = $answer;
        $AssignmentAnswer->user_id = $User->id;
        $AssignmentAnswer->description = $request->description;
        $AssignmentAnswer->save();

      }

      // $AssignmentAnswer = new AssignmentAnswer;
      // $AssignmentAnswer->assignment_id = $assignment_id;
      // $AssignmentAnswer->answer = $answer;
      // $AssignmentAnswer->user_id = $User->id;
      // $AssignmentAnswer->description = $request->description;
      // $AssignmentAnswer->save();


    }else{

      // GOOGLE UPLOAD
      // $destinationPath = asset_path('uploads/assignments/'); // upload path
      // $extension = $request->file('answer_file')->getClientOriginalExtension(); // getting image extension
      // $file = 'assignment-'.str_slug($User->name).rand(111,9999).'.'.$extension; // renameing image
      // $folder = get_folder_course($Assignment->id_section); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$file, fopen($request->file('answer_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $file);
      // $answer = "https://drive.google.com/file/d/".$path_file;

      // DIGITALOCEAN UPLOAD
      // $destinationPath = asset_path('uploads/assignments/');
      // $file = $request->file('answer_file');
      // $extension = $request->file('answer_file')->getClientOriginalExtension();
      // $name_file = 'assignment-'.str_slug($User->name).rand(111,9999).'.'.$extension;
      // $folder = get_folderName_course($Assignment->id_section); // get folder course by section id
      // Storage::disk('do_spaces')->put('assets/' . $folder.'/'.$name_file, fopen($file, 'r+'), 'public');//upload digital ocean
      // $path_file = get_asset_newpath('assets/'.$folder, $name_file);
      // $answer = "https://ingeniolms.sgp1.digitaloceanspaces.com/" . $path_file;

    }

    if($Assignment->setting){

      $content_default_setting = $Assignment->setting;
    }else {
      // code...
      $content_default_setting = $Assignment->section->course->default_activity_completion->where('type_content', 'assigment')->first();
    }

    if($content_default_setting->required_grade == 0 && $content_default_setting->required_submit == 1){
      if(!Progress::where(['assignment_id' => $assignment_id, 'user_id' => $User->id])->first()){
        // insert progress
        $Progress = new Progress;
        $Progress->assignment_id = $assignment_id;
        $Progress->course_id = $Assignment->section->course->id;
        $Progress->user_id = $User->id;
        $Progress->status = '1';
        $Progress->save();
        // insert progress
      }else{
        $Progress = Progress::where(['assignment_id' => $assignment_id, 'user_id' => $User->id])->first();
        $Progress->status = '1';
        $Progress->save();
      }
    }

    return redirect('/assignment/success/' . $Assignment->id);
    // return redirect()->back();
  }

  public function upload($assignment_id){
    $User = Auth::user();
    $Assignment = Assignment::where('id', $assignment_id)->first();
    $section_id = $Assignment->id_section;
    return Plupload::receive('file', function ($file) use ($section_id)
    {
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $size_file = $file->getSize(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image

        // $folder = get_folderName_course($section_id); // get folder course by section id
        // Storage::disk('do_spaces')->put('assets/' . $folder.'/'.$name_file, fopen($file, 'r+'), 'public'); //upload do
        // $path_file = get_asset_newpath('assets/'.$folder, $name_file);
        
        $folder = get_folderName_course($section_id); // get folder course by section id
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/assignments/answers/' . $name_file, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/assignments/answers/', $name_file);

        return response()->json([
          'path' =>  $path_file,
        ]);
    });
  }

  public function upload_success($assignment_id, Request $request){
    $User = Auth::user();
    $Assignment = Assignment::where('id', $assignment_id)->first();

    $AssignmentAnswer = new AssignmentAnswer;
    $AssignmentAnswer->assignment_id = $assignment_id;
    $AssignmentAnswer->answer = $request->answer;
    $AssignmentAnswer->description = $request->description;
    $AssignmentAnswer->user_id = $User->id;
    $AssignmentAnswer->save();

    return response()->json($AssignmentAnswer);
  }

  public function success($id){

    $AssignmentAnswer = AssignmentAnswer::where(['assignments_answers.assignment_id'=>$id, 'user_id' => Auth::user()->id])->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');
    $Assignment = Assignment::where('id', $id)->first();


      $section = Section::where('id', $Assignment->id_section)->first();
      $courses = Course::where('id', $section->id_course)->with('sections')->first();

      $section_data = [];
      $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

      $num_contents = 0;
      foreach($sections as $section){
        $section_all = [];

        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
        $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'contents')
          ->orderBy('contents.sequence', 'asc')
          ->get();

        $group_content_id = [];
        foreach ($group_has_contents as $data) {
          array_push($group_content_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->where(function ($query) use ($group_content_id) {
          $query->whereNotIn('id', $group_content_id);
        })
        ->orderBy('contents.sequence', 'asc')
        ->get();
        // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->orderBy('contents.sequence', 'asc')
        ->get();
        // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
        $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'quizzes')
          ->get();

        $group_quiz_id = [];
        foreach ($group_has_quizzes as $data) {
          array_push($group_quiz_id, $data->table_id);
        }
        // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
          $query->whereNotIn('id', $group_quiz_id);
        })->get();
        // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
        $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
          ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
          ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
          ->where('group_has_contents.table', 'assignments')
          ->get();

        $group_assignment_id = [];
        foreach ($group_has_assignments as $data) {
          array_push($group_assignment_id, $data->table_id);
        }
        // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
        $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
          $query->whereNotIn('id', $group_assignment_id);
        })->get();
        // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
        $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->where('course_student_group_users.user_id', '=', Auth::user()->id)
        ->get();
        // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

        $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
        $num_contents += count($quizzes);
        $num_contents += count($assignments);

        // MERGING KONTEN
        foreach ($contents as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        foreach ($group_has_contents_users as $content) {
          $content->section_type = 'content';
          array_push($section_all, $content);
        }
        // MERGING KONTEN

        // MERGING KUIS
        foreach ($quizzes as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        foreach ($group_has_quizzes_users as $quizze) {
          $quizze->section_type = 'quizz';
          $section_all[] = $quizze;
        }
        // MERGING KUIS

        // MERGING TUGAS
        foreach ($assignments as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        foreach ($group_has_assignments_users as $assignment) {
          $assignment->section_type = 'assignment';
          $section_all[] = $assignment;
        }
        // MERGING TUGAS

        array_push($section_data, [
          'id' => $section->id,
          'title' => $section->title,
          'description' => $section->description,
          'id_course' => $section->id_course,
          'created_at' => $section->created_at,
          'updated_at' => $section->updated_at,
          'section_all' => $section_all,
        ]);
      }

      $data = [
        'assignment' => $Assignment,
        'assignments_answers' => $AssignmentAnswer->get(),
        'course' => $courses,
        'sections' => $section_data,
      ];

    \Session::flash('success', 'Tugas berhasil terkirim');
    // return view('assignment.success');
    return view('assignment.my_answer', $data);
  }

  public function answer($assignment_id){
    // cek apakah id tugas termasuk kedalam kepunyaan grup
    $CheckGroupHasContent = GroupHasContent::where(['table' => 'assignments', 'table_id' => $assignment_id])->count();

    if ($CheckGroupHasContent > 0) {

      $AssignmentsAnswers = AssignmentAnswer::select('assignments_answers.id as id','assignments_answers.answer','assignments_answers.assignment_id as assignment_id','course_student_groups.name','assignments.type','assignments_grades.grade')
        ->where('assignments_answers.assignment_id', $assignment_id)
        ->where('group_has_contents.table', 'assignments')
        ->join('assignments','assignments.id','=','assignments_answers.assignment_id')
        ->join('group_has_contents','group_has_contents.table_id','=','assignments.id')
        ->join('course_student_groups','course_student_groups.id','=','group_has_contents.course_student_group_id')
        ->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id')
        ->groupBy('course_student_groups.id');

    } else {

      $AssignmentsAnswers = AssignmentAnswer::select('assignments_answers.id as id','assignments_answers.answer','assignments_answers.assignment_id as assignment_id','users.name','assignments.type','assignments_grades.grade')
        ->where('assignments_answers.assignment_id', $assignment_id)
        ->join('users','users.id','=','assignments_answers.user_id')
        ->join('assignments','assignments.id','=','assignments_answers.assignment_id')
        ->leftJoin('assignments_grades','assignments_grades.assignment_answer_id','=','assignments_answers.id');

    }


    $Assignment = Assignment::where('id', $assignment_id)->first();
    $section = Section::where('id', $Assignment->id_section)->first();
    $course = Course::where('id', $section->id_course)->with('sections')->first();

    $data = [
      'assignments_answers' => $AssignmentsAnswers->get(),
      'course' => $course
    ];

    // dd($data);

    return view('assignment.answer', $data);
  }

  public function previewAnswer($id){
    $AssignmentAnswer = AssignmentAnswer::select('assignments_answers.id as id','assignments_answers.answer','assignments_answers.assignment_id','assignments.type', 'assignments_answers.description','assignments.id_section', 'assignments.title', 'assignments.id as assignments_id')
    ->where('assignments_answers.id', $id)
    ->join('assignments','assignments.id','=','assignments_answers.assignment_id')->first();

    $section = Section::with('course')->where('id', $AssignmentAnswer->id_section)->first();
    $data = [
      'AssignmentAnswer' => $AssignmentAnswer,
      'section' => $section,
    ];
    return view('assignment.preview_answer', $data);
  }

  public function saveGrade(Request $request){
    $Assignment = Assignment::find($request->assignment_id);

    // cek apakah id tugas termasuk kedalam kepunyaan grup
    $CheckGroupHasContent = GroupHasContent::where(['table' => 'assignments', 'table_id' => $request->assignment_id])->first();

    if ($CheckGroupHasContent) {

      $assignment_answers = AssignmentAnswer::where('assignment_id', $request->assignment_id)->get();

        foreach ($assignment_answers as $data) {
          $AssignmentGrade = new AssignmentGrade;
          $AssignmentGrade->assignment_id = $request->assignment_id;
          $AssignmentGrade->assignment_answer_id = $data->id;
          $AssignmentGrade->grade = $request->grade;
          $AssignmentGrade->save();
        }

    } else {

      $AssignmentAnswer = AssignmentAnswer::find($request->assignment_answer_id);

      $AssignmentGrade = new AssignmentGrade;
      $AssignmentGrade->assignment_id = $request->assignment_id;
      $AssignmentGrade->assignment_answer_id = $request->assignment_answer_id;
      $AssignmentGrade->grade = $request->grade;
      $AssignmentGrade->save();

      if($Assignment->setting){
        $content_default_setting = $Assignment->setting;
      }else {
        $content_default_setting = $Assignment->section->course->default_activity_completion->where('type_content', 'assigment')->first();
      }

      if($content_default_setting->required_grade == 1){
        if(!Progress::where(['assignment_id' => $request->assignment_id, 'user_id' => $AssignmentAnswer->user_id])->first()){
          // insert progress
          $Progress = new Progress;
          $Progress->assignment_id = $request->assignment_id;
          $Progress->course_id = $Assignment->section->course->id;
          $Progress->user_id = $AssignmentAnswer->user_id;
          $Progress->status = '1';
          $Progress->save();
          // insert progress
        }else{
          $Progress = Progress::where(['assignment_id' => $request->assignment_id, 'user_id' => $AssignmentAnswer->user_id])->first();
          $Progress->status = '1';
          $Progress->save();
        }
      }

    }

    \Session::flash('success', 'Grade Success Saved');
  }
}
