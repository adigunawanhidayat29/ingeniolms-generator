<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Instructor;
use App\InstructorBank;
use App\TrainingProviderTrainee;
use App\TrainingProvider;
use App\BadgeContent;
use Session;
use Validator;
use App;
use Storage;

class ProfileController extends Controller
{

  public function index(){
    $User = Auth::user();

    if(isInstructor($User->id)){
      $Instructor = Instructor::where('user_id', Auth::user()->id)->first();
      $InstructorBank = InstructorBank::where('instructor_id', $Instructor->id)->first();

      $bank_name = !$InstructorBank ? '' : $InstructorBank->bank_name;
      $bank_branch = !$InstructorBank ? '' : $InstructorBank->bank_branch;
      $account_name = !$InstructorBank ? '' : $InstructorBank->account_name;
      $account_number = !$InstructorBank ? '' : $InstructorBank->account_number;

    }else{
      $bank_name = '';
      $bank_branch = '';
      $account_name = '';
      $account_number = '';
    }

    //CHECKING TRAINING PROVIDER USER
    $user_provider = TrainingProviderTrainee::where('user_id', $User->id)->count();
    $user_provider_value = TrainingProviderTrainee::where('user_id', $User->id)->value('training_provider_id');

    $training_provider = TrainingProvider::where('id', $user_provider_value)->first();

    $data = [
      'user_provider' => $user_provider,
      'training_provider' => $training_provider,
      'User' => $User,
      'bank_name' => $bank_name,
      'bank_branch' => $bank_branch,
      'account_name' => $account_name,
      'account_number' => $account_number
    ];
    return view('profile.index', $data);
  }

  public function update(Request $request){
    $auth = Auth::user();
    $User = User::where('id', $auth->id)->first();
    $User->name = $request->name;
    $User->phone = $request->phone;
    $User->short_description = $request->short_description;
    $User->facebook = $request->facebook;
    $User->twitter = $request->twitter;
    $User->instagram = $request->instagram;
    $User->language = $request->language;
    $User->save();

    // set user language
    $request->session()->put(['locale' => $request->language]);
    App::setLocale($request->language);
    // set user language

    Session::flash('success', 'Update profile success');
    return redirect('profile');
  }

  public function update_password(Request $request){
    $auth = Auth::user();
    $User = User::where('id', $auth->id)->first();
    $User->password = bcrypt($request->password);
    $User->save();

    return redirect('profile');
  }

  public function update_avatar(Request $request){
    $Auth = Auth::user();

    $default_photo = env('UPLOAD_PATH') . "/users/default.png";
    $import_photo = explode('/', $Auth->photo);
    // if($default_photo != $Auth->photo){
    //   if($import_photo[1] == 'uploads'){
    //     unlink(asset_path($Auth->photo));
    //   }
    // }

    if($request->file('image')){

      $validator = Validator::make($request->all(),[
        'image' => 'mimes:jpeg,jpg,png,gif,svg|required|max:10000',
      ]);

      if($validator->fails()){
        return redirect()->back();
      }else{
        // $destinationPath = asset_path('uploads/users/'); // upload path
        // $file->move($destinationPath, $image); // uploading file to given path
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $image = str_slug($Auth->name).rand(1111,9999).'.'.$extension; // renameing image
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/users/' . $image, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/users/', $image);

        $User = User::where('id', $Auth->id)->first();
        $User->photo = $path_file;
        $User->save();
        return redirect('profile');
      }

    }

    // return redirect('profile');
  }

  public function download_badge($image_name)
  {
    return Storage::disk('public')->download('badge_content/'. $image_name);
  }

  public function detail_badge($id)
  {
    $badge = BadgeContent::where('id', $id)->first();

    $data = [
      'badge' => $badge
    ];

    return view('profile.badge_detail', $data);
  }
}
