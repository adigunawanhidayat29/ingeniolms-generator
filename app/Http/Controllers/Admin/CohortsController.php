<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cohort;
use App\CohortUser;
use App\User;
use Illuminate\Http\Request;

class CohortsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cohorts = Cohort::where('name', 'LIKE', "%$keyword%")
                ->orWhere('created_by', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->with('cohort_user')->latest()->paginate($perPage);
        } else {
            $cohorts = Cohort::with('cohort_user')->latest()->paginate($perPage);
				}
				
				$users = User::get();

        return view('admin.cohorts.index', compact('cohorts', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cohorts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Cohort::create($requestData);

        return redirect('admin/cohorts')->with('flash_message', 'Cohort added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
				$cohort = Cohort::with('cohort_user')->findOrFail($id);
				$users = User::get();

        return view('admin.cohorts.show', compact('cohort', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cohort = Cohort::findOrFail($id);

        return view('admin.cohorts.edit', compact('cohort'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $cohort = Cohort::findOrFail($id);
        $cohort->update($requestData);

        return redirect('admin/cohorts')->with('flash_message', 'Cohort updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Cohort::destroy($id);

        return redirect('admin/cohorts')->with('flash_message', 'Cohort deleted!');
		}

    public function addUser($cohort_id, Request $request)
    {
			$user_id = array_unique($request->user_id);
			CohortUser::where('cohort_id', $cohort_id)->delete();

			if ($request->user_id) {
				foreach ($user_id as $index => $value) {
					$CohortUser = new CohortUser;
					$CohortUser->cohort_id = $cohort_id;
					$CohortUser->user_id = $value;
					$CohortUser->save();
				}
			}

			return redirect('admin/cohorts/' . $cohort_id)->with('flash_message', 'Cohort User added!');
    }
		
}
