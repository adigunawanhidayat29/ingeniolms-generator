<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserLevel;
use App\Permission;
use App\LevelHasPermission;
use Illuminate\Http\Request;

class LevelHasPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $levelhaspermission = LevelHasPermission::where('permission_id', 'LIKE', "%$keyword%")
                ->orWhere('level_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $levelhaspermission = LevelHasPermission::latest()->paginate($perPage);
        }

        return view('admin.level-has-permission.index', compact('levelhaspermission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $levels = UserLevel::get();
        $permissions = Permission::get();
        $LevelHasPermissions = LevelHasPermission::where('level_id', \Request::get('level'))->get();
        return view('admin.level-has-permission.create', compact('levels', 'permissions', 'LevelHasPermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        LevelHasPermission::where('level_id', $request->level_id)->delete();

        if ($request->permission_id) {
            foreach ($request->permission_id as $index => $value) {
                $LevelHasPermission = new LevelHasPermission;
                $LevelHasPermission->level_id = $request->level_id;
                $LevelHasPermission->permission_id = $value;
                $LevelHasPermission->save();
            }
        }

        return redirect('admin/user-levels')->with('flash_message', 'LevelHasPermission added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $levelhaspermission = LevelHasPermission::findOrFail($id);

        return view('admin.level-has-permission.show', compact('levelhaspermission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $levelhaspermission = LevelHasPermission::findOrFail($id);

        return view('admin.level-has-permission.edit', compact('levelhaspermission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $levelhaspermission = LevelHasPermission::findOrFail($id);
        $levelhaspermission->update($requestData);

        return redirect('admin/level-has-permission')->with('flash_message', 'LevelHasPermission updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LevelHasPermission::destroy($id);

        return redirect('admin/level-has-permission')->with('flash_message', 'LevelHasPermission deleted!');
    }
}
