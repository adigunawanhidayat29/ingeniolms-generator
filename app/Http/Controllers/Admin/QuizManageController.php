<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\QuizPageBreak;
use App\Course;

class QuizManageController extends Controller
{
  public function create($id){
    $section = Section::where('id', $id)->first();

    $data = [
      'action' => 'admin/course/quiz/create_action/'.$section->id,
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'name' => old('name'),
      'description' => old('description'),
      'shuffle' => old('shuffle'),
      'time_start' => old('time_start'),
      'time_end' => old('time_end'),
      'duration' => old('duration','120'),
      'attempt' => old('attempt','1'),
      'section' => $section,
      'course' => Course::where('id', $section->id_course)->first(),
    ];
    return view('admin.course.quiz_form', $data);
  }

  public function create_action($section_id, Request $request){
    // insert Quiz
    $Quiz = new Quiz;

    $Quiz->name = $request->name;
    $Quiz->description = $request->description;
    $Quiz->slug = str_slug($request->name);
    // $Quiz->shuffle = $request->shuffle;
    $Quiz->time_start = $request->date_start ." ". $request->time_start;
    $Quiz->time_end = $request->date_end ." ". $request->time_end;
    $Quiz->duration = $request->duration;
    $Quiz->attempt = $request->attempt;
    $Quiz->id_section = $section_id;
    $Quiz->status = 1; // 1= publish, 2 =draft
    $Quiz->save();
    // insert Quiz

    // default page brak
    $QuizPageBreak = new QuizPageBreak;
    $QuizPageBreak->quiz_id = $Quiz->id;
    $QuizPageBreak->title = 'Page 1';
    $QuizPageBreak->save();
    // default page brak

    \Session::flash('success', 'Create Quiz success');
    return redirect('admin/course/quiz/question/manage/'.$Quiz->id);
    // return redirect()->back();
  }

  public function update($id){
    $Quiz = Quiz::where('id', $id)->first();
    $section = Section::where('id', $Quiz->id_section)->first();

    $data = [
      'action' => 'admin/course/quiz/update_action/',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Quiz->id),
      'name' => old('name', $Quiz->name),
      'description' => old('description', $Quiz->description),
      'shuffle' => old('shuffle', $Quiz->shuffle),
      'time_start' => old('time_start', $Quiz->time_start),
      'time_end' => old('time_end', $Quiz->time_end),
      'duration' => old('duration', $Quiz->duration),
      'attempt' => old('attempt', $Quiz->attempt),
      'section' => $section,
      'course' => Course::where('id', $section->id_course)->first(),
    ];
    return view('admin.course.quiz_form', $data);
  }

  public function update_action(Request $request){
    $Quiz = Quiz::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Quiz->id_section)->first();

    $Quiz->name = $request->name;
    $Quiz->slug = str_slug($request->name);
    $Quiz->description = $request->description;
    // $Quiz->shuffle = $request->shuffle;
    $Quiz->time_start = $request->date_start ." ". $request->time_start;
    $Quiz->time_end = $request->date_end ." ". $request->time_end;
    $Quiz->duration = $request->duration;
    $Quiz->attempt = $request->attempt;
    $Quiz->save();

    \Session::flash('success', 'Update Quiz success');
    // return redirect('course/preview/'.$Section->id_course);
    return redirect()->back();
  }

  public function publish($quiz_id, $course_id){
    $Quiz = Quiz::where('id', $quiz_id)->first();
    $Section = Section::where('id', $Quiz->id_section)->first();
    $Quiz->status = '1';
    $Quiz->save();

    \Session::flash('success', 'Publish Quiz success');
    // return redirect('course/preview/'.$Section->id_course);
    return redirect()->back();
  }

  public function unpublish($quiz_id){
      $Quiz = Quiz::where('id', $quiz_id)->first();
      $Section = Section::where('id', $Quiz->id_section)->first();
      $Quiz->status = '0';
      $Quiz->save();

      \Session::flash('success', 'Unpublish Quiz success');
      return redirect()->back();
  }

  public function delete($course_id, $id){
    $Quiz = Quiz::where('id', $id);
    if($Quiz->first()){
      $QuizQuestion = QuizQuestion::where('quiz_id', $Quiz->first()->id);
        if($QuizQuestion->first()){
          $QuizQuestionAnswer = QuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->first()->id);
          $QuizQuestionAnswer->delete();
        }
      $QuizQuestion->delete();
      $Quiz->delete();
      \Session::flash('success', 'Delete Quiz success');
    }else{
      \Session::flash('error', 'Delete Quiz failed');
    }
    return redirect()->back();
  }

  public function update_sequence(Request $request){
    foreach($request->datas as $key => $value){
      $QuizPageBreak = QuizPageBreak::find($value);
      $QuizPageBreak->quiz_id = $request->quiz_id;
      $QuizPageBreak->sequence = $key;
      $QuizPageBreak->save();
    }
  }

  public function edit_page_break(Request $request){
    $QuizPageBreak = QuizPageBreak::find($request->id);
    $QuizPageBreak->title = $request->title;
    $QuizPageBreak->description = $request->description;
    $QuizPageBreak->save();
    return redirect()->back();
  }

  public function delete_page_break($id){
    $Question = QuizQuestion::where('quiz_page_break_id', $id)->delete();
    $QuizPageBreak = QuizPageBreak::find($id)->delete();
    return redirect()->back();
  }

}
