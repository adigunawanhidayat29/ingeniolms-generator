<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Video;
use Lang;

class VideoController extends Controller
{
    public function index(){        
        return view('admin.Video.index');
    }

    public function serverside(Request $request)
    {
        if($request->ajax()){
            $data = Video::select('*')->get();
            return Datatables::of($data)      
            ->editColumn('status', function($data){
                return $data->status == '1' ? Lang::get('back.video.enabled_status') : Lang::get('back.video.disabled_status');
            })
            ->addColumn('action', function ($data)
            {
                return '                
                    <a href="'. url(\Request::route()->getPrefix() .'/videos/edit/'.$data->id) .'" class="btn btn-warning btn-sm">
                        <i class="material-icons">edit</i><span>'. Lang::get('back.video.edit_button') .'</span>
                    </a>
                    <a href="'. url(\Request::route()->getPrefix() .'/videos/delete/'.$data->id) .'" class="btn btn-danger btn-sm">
                        <i class="material-icons">delete</i><span>'. Lang::get('back.video.delete_button') .'</span>
                    </a>
                ';
            })->make(true);
        }else{
            exit("Not an AJAX request");
        }
    }

    public function create(){
        $data = [
            'action' => \Request::route()->getPrefix() .'/videos/store',
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'title' => old('title'),
            'description' => old('description'),
            'url' => old('url'),
            'status' => old('status'),
        ];
        return view('admin.Video.form', $data);
    }

    public function store(Request $request){
        $Video = new Video;
        $Video->title = $request->title ;
        $Video->description = $request->description ;
        $Video->url = $request->url ;
        $Video->status = $request->status ;
        $Video->save();
        \Session::flash('success', Lang::get('back.video.alert_created'));
        return redirect(\Request::route()->getPrefix() . '/videos');
    }

    public function edit($id){
        $Video = Video::where('id', $id)->first();
        $data = [
            'action' => \Request::route()->getPrefix() .'/videos/update',
            'method' => 'post',
            'button' => 'Update',
            'id' => old('id', $Video->id),
            'title' => old('title', $Video->title),
            'description' => old('description', $Video->description),
            'url' => old('url', $Video->url),
            'status' => old('status', $Video->status),
        ];
        return view('admin.Video.form', $data);
    }

    public function update(Request $request)
    {
        $Video = Video::where('id', $request->id)->first();      
        $Video->title = $request->title ;
        $Video->description = $request->description ;
        $Video->url = $request->url ;
        $Video->status = $request->status ;  
        $Video->save();
        \Session::flash('success', Lang::get('back.video.alert_updated'));        

        return redirect(\Request::route()->getPrefix() . '/videos');
    }

    public function delete(Request $request, $id)
    {
        $Video = Video::where('id', $id);
        if($Video){
            $Video->delete();
            \Session::flash('success', Lang::get('back.video.alert_deleted'));
        }else{
            \Session::flash('error', Lang::get('back.video.alert_deleted_failed'));
        }

        return redirect(\Request::route()->getPrefix() . '/videos');
    }
}