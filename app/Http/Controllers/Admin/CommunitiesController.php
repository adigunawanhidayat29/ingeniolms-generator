<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Community;
use Illuminate\Http\Request;
use Storage;
use Lang;

class CommunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $communities = Community::where('name', 'LIKE', "%$keyword%")
                ->orWhere('short_description', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('photo', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $communities = Community::latest()->paginate($perPage);
        }

        return view('admin.communities.index', compact('communities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.communities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        // image
        if($request->file('photo')){
            // $destinationPath = asset_path('uploads/'); // upload path
            // $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
            // $photo = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            // $request->file('photo')->move($destinationPath, $photo); // uploading file to given path
            // $requestData['photo'] = '/uploads/'.$photo;
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($requestData['name']).rand(1111,9999).'.'.$extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/communities/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/communities/', $image);
            $requestData['photo'] = $path_file;
        }
        // image
        
        Community::create($requestData);

        return redirect('admin/communities')->with('success', Lang::get('back.user_teacher_community.alert_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $community = Community::findOrFail($id);

        return view('admin.communities.show', compact('community'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $community = Community::findOrFail($id);

        return view('admin.communities.edit', compact('community'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $community = Community::findOrFail($id);

        // image
        if($request->file('photo')){
            // $destinationPath = asset_path('uploads/'); // upload path
            // $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
            // $photo = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            // $request->file('photo')->move($destinationPath, $photo); // uploading file to given path
            // $requestData['photo'] = '/uploads/'.$photo;
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($requestData['name']).rand(1111,9999).'.'.$extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/communities/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/communities/', $image);
            $requestData['photo'] = $path_file;
        }
        // image
        
        $community->update($requestData);

        return redirect('admin/communities')->with('success', Lang::get('back.user_teacher_community.alert_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Community::destroy($id);

        return redirect('admin/communities')->with('success', Lang::get('back.user_teacher_community.alert_deleted'));
    }
}
