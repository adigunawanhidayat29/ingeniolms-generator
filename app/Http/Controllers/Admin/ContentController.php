<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Content;
use App\Course;
use App\Section;
use Storage;
use Plupload;
use FFMpeg;
use Lang;

class ContentController extends Controller
{
  public function create($id){
    $section = Section::where('id', $id)->first();

    $data = [
      'action' => 'admin/course/content/create_action/'.$section->id,
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'description' => old('description'),
      'type_content' => old('type_content'),
      'name_file' => old('name_file'),
      'path_file' => old('path_file'),
      'file_size' => old('file_size'),
      'video_duration' => old('video_duration'),
      'full_path_file' => old('full_path_file'),
      'sequence' => old('sequence'),
      'section' => $section,
    ];
    return view('admin.course.content_form', $data);
  }

  public function create_action($section_id, Request $request){
    // insert content
    $folder = get_folder_course($section_id);
    $Content = new Content;

    // $destinationPath = asset_path('uploads/contents/'); // upload path
    // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
    // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
    // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
    // $folder = get_folder_course($section_id); // get folder course by section id
    // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
    // $path_file = get_asset_path($folder, $name_file);

    $Content->title = $request->title;
    $Content->description = $request->description;
    $Content->type_content = $request->type_content;
    // $Content->sequence = $request->sequence;
    $Content->name_file = $request->name_file;
    $Content->path_file = $request->path_file;
    $Content->file_size = $request->file_size;
    $Content->video_duration = $request->video_duration;
    $Content->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
    if($request->type_content == 'video'){
      $Content->full_path_file_resize = get_asset_path($folder, '240-'.$request->name_file);
    }
    $Content->id_section = $section_id;
    $Content->save();
    // insert content

    \Session::flash('success', 'Create content success');
    return redirect()->back();
  }

  public function update($id){
    $Content = Content::where('id', $id)->first();
    $section = Section::where('id', $Content->id_section)->first();

    $data = [
      'action' => 'course/content/update_action/',
      'method' => 'post',
      'button' => 'Update',
      'id' => old('id', $Content->id),
      'title' => old('title', $Content->title),
      'description' => old('description', $Content->description),
      'type_content' => old('type_content', $Content->type_content),
      'name_file' => old('name_file', $Content->name_file),
      'path_file' => old('path_file', $Content->path_file),
      'full_path_file' => old('full_path_file', $Content->full_path_file),
      'file_size' => old('file_size', $Content->file_size),
      'video_duration' => old('video_duration', $Content->video_duration),
      'sequence' => old('sequence', $Content->sequence),
      'section' => $section,
    ];
    return view('admin.course.content_form', $data);
  }

  public function update_action(Request $request){
    $Content = Content::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Content->id_section)->first();
    $folder = get_folder_course($Content->id_section);
    // if($request->file('name_file')){
    //   // unlink(asset_path($request->old_name_file));
    //   Storage::disk('google')->delete($request->old_path_file);
    //   $destinationPath = asset_path('uploads/contents/'); // upload path
    //   $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
    //   $name_file = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
    //   // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to given path
    //   $folder = get_folder_course($Content->id_section); // get folder course by section id
    //   Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
    //   $path_file = get_asset_path($folder, $name_file);
    //   $update_name_file = $name_file;
    //   $update_path_file = $path_file;
    //   $update_full_path_file = "https://drive.google.com/file/d/".$path_file;
    // }else{
    //   $update_name_file = $request->old_name_file;
    //   $update_path_file = $request->old_path_file;
    //   $update_full_path_file = $request->old_full_path_file;
    // }

    if($request->name_file && $request->path_file){
      $Content->name_file = $request->name_file;
      $Content->path_file = $request->path_file;
      $Content->file_size = $request->file_size;
      $Content->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
      if($request->type_content == 'video'){
        $Content->full_path_file_resize = get_asset_path($folder, '240-'.$request->name_file);
        $Content->video_duration = $request->video_duration;
      }
    }

    $Content->title = $request->title;
    $Content->description = $request->description;
    $Content->type_content = $request->type_content;
    // $Content->sequence = $request->sequence;
    $Content->save();

    \Session::flash('success', 'Update content success');
    return redirect()->back();
  }

  public function delete($course_id, $id){
    $Content = Content::where('id', $id);
    if($Content){
      // unlink(asset_path($Content->first()->name_file));
      Storage::disk('google')->delete($Content->first()->path_file); // delete file google drive
      $Content->delete();
      \Session::flash('success', 'Delete content success');
    }else{
      \Session::flash('error', 'Delete content failed');
    }

    return redirect('admin/course/manage/'.$course_id);
  }

  public function upload_BACKUP($section_id){
    return Plupload::receive('file', function ($file) use ($section_id)
    {
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $size_file = $file->getSize(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image
        $folder = get_folder_course($section_id); // get folder course by section id
        Storage::disk('google')->put($folder.'/'.$name_file, fopen($file, 'r+'));//upload google drive
        $path_file = get_asset_path($folder, $name_file);
        $video_duration = 0;

        $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt', 'avi', 'webm'];
        if(in_array(strtolower($extension), $video_extensions)){
          $destinationPath = asset_path('uploads/videos/'); // upload path
          $file->move($destinationPath, $name_file);
          $ffmpeg = FFMpeg\FFMpeg::create(array(
            'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/bin/ffprobe',
            'timeout'          => 3600, // The timeout for the underlying process
            'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
          ));
          $video = $ffmpeg->open($destinationPath . $name_file);
          $video
            ->filters()
            ->resize(new FFMpeg\Coordinate\Dimension(426, 240))
            ->synchronize();
          $format = new FFMpeg\Format\Video\WebM();

          $format->setKiloBitrate(320);
          $video
            ->save($format, $destinationPath.'240-'.$name_file);
            Storage::disk('google')->put($folder.'/'.'240-'.$name_file, fopen($destinationPath.'240-'.$name_file, 'r+'));//upload google drive
            $resize_path = get_asset_path($folder, '240-'.$name_file);
            $ffprobe = FFMpeg\FFProbe::create();
            $video_duration = $ffprobe
              ->format($destinationPath . $name_file) // extracts file informations
              ->get('duration');

          return response()->json([
            'path' => $path_file,
            'name' => $name_file,
            'size' => $size_file,
            'video_duration' => $video_duration,
            'resize_path' => $resize_path
          ]);
        }else{
          return response()->json([
            'path' => $path_file,
            'name' => $name_file,
          ]);
        }
    });
  }

  public function upload($section_id){
    return Plupload::receive('file', function ($file) use ($section_id)
    {
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $size_file = $file->getSize(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image

        $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt', 'avi', 'webm'];
        if(in_array(strtolower($extension), $video_extensions)){

            // $folder = get_folderName_course($section_id); // get folder course by section id
            // Storage::disk('sftp')->put(server_assets_path($folder) .'/'. $name_file, fopen($file, 'r+')); //upload google drive
            $folder = get_folderName_course($section_id); // get folder course by section id
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/videos/' .$name_file, fopen($file, 'r+'), 'public');//upload google drive
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/videos/', $name_file);
            $video_duration = 0;

            $ffprobe = FFMpeg\FFProbe::create(array(
              'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
              'ffprobe.binaries' => '/usr/bin/ffprobe',
              'timeout'          => 3600, // The timeout for the underlying process
              'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
            ));
            $video_duration = $ffprobe
              ->format($file) // extracts file informations
              ->get('duration');

          return response()->json([
            'path' => $path_file,
            'name' => $name_file,
            'size' => $size_file,
            'video_duration' => $video_duration,
            'full_path_original' => asset_url($path_file),
            'full_path_file' => asset_url($path_file),
            'resize_path' => '',
          ]);
        }else{

          $folder = get_folderName_course($section_id); // get folder course by section id
          Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/files/' .$name_file, fopen($file, 'r+'), 'public');//upload
          $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/files/' , $name_file);

          return response()->json([
            'path' => $path_file,
            'name' => $name_file,
          ]);
        }
    });
  }

  // save to draft function
  public function store_draft(Request $request){
    $field = $request->field;
    $value = $request->value;

    $Section = Section::where('id', $request->section_id)->first();
    $Content = Content::where('id', $request->id)->first(); // checking record content

    if($Content){ // jika konten telah ada maka hanya melakaukan update konten
      // $folder = get_folder_course($Content->id_section); // get folder id
      $Content->type_content = $request->type_content;
      if($request->file){ // jika ada inputan file
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        if($Content->type_content == 'video'){ // jika inputan file berupa video
          $Content->path_file = $request->path_file;
          $Content->file_size = $request->file_size;
          $Content->video_duration = $request->video_duration;
          $Content->full_path_original = $request->full_path_original;
          $Content->full_path_file = asset_url($request->path);
          $Content->save();

        }elseif($Content->type_content == 'file'){
          $Content->full_path_file = asset_url($request->path_file);
          $Content->save();
        }
      }else{
        $Content->$field = $value;
        $Content->save();
      }

    }else{ // jika belum ada konten maka buat konten baru
      // $folder = get_folder_course($request->section_id); // get folder id
      $Content = new Content;
      $Content->type_content = $request->type_content;
      $Content->id_section = $request->section_id;
      if($request->file){  // jika ada inputan file
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        if($Content->type_content == 'video'){ // jika inputan file berupa video
          $Content->path_file = $request->path_file;
          $Content->full_path_file = asset_url($request->path_file);
          $Content->file_size = $request->file_size;
          $Content->video_duration = $request->video_duration;
          $Content->full_path_original = $request->full_path_original;
          $Content->save();

          // exec resize video
          // file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
          // exec resize video

        }
        // elseif($Content->type_content == 'file'){
        //   $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
        //   $Content->save();
        // }
      }else{
        $Content->$field = $value;
        $Content->save();
      }
    }

    return response()->json($Content);
  }

  public function update_sequence(Request $request){
    if($request->datas){
      foreach($request->datas as $key => $value){
        $Content = Content::find($value);
        $Content->id_section = $request->section_id;
        $Content->sequence = $key;
        $Content->save();
      }
    }
  }

  public function update_preview(Request $request){

    $section = Section::where('id_course', $request->course_id)
    ->join('courses', 'courses.id', '=', 'sections.id_course')
    ->join('contents', 'contents.id_section', '=', 'sections.id')
    ->where('contents.preview', '1')
    ->get();

    if($request->preview == '1'){
      if(count($section) < 3){
        $Content = Content::find($request->id);
        $Content->preview = '1';
        $Content->save();

        $title = "Berhasil";
        $message = 'Konten berhasil di set sebagai preview';
        $alert = 'success';
      }else{
        $title = "Gagal";
        $message = 'Konten preview tidak boleh lebih dari 3';
        $alert = 'error';
      }
    }else{
      $Content = Content::find($request->id);
      $Content->preview = '0';
      $Content->save();

      $title = "Berhasil";
      $message = 'Konten tidak sebagai preview';
      $alert = 'success';
    }

    return response()->json([
      'title' => $title,
      'message' => $message,
      'alert' => $alert,
    ]);

  }

  public function detail(Request $request){
    $Content = Content::where('id', $request->id)->first();

    if($Content->type_content == 'video'){
      // get file video google drive
      // $data = getVideoGoogleDrive($Content->full_path_file_resize_720);
      // $data_360 = getVideoGoogleDrive($Content->full_path_file_resize);
      // $client_info = getClientInfoGoogleDrive();

      // $downloadUrl = str_replace('&e=download', '', $data->downloadUrl);
      // $downloadUrl = $downloadUrl . '&access_token=' . $client_info->access_token;
      // get file video google drive

      // $body = $downloadUrl;

      $body = $Content->full_path_original;
    }elseif($Content->type_content == 'url-video'){
      $body = $Content->full_path_file;
    }elseif($Content->type_content == 'file'){
      $body = $Content->full_path_file;
    }else{
      $body = $Content->description;
    }

    $response = [
      'id' => $Content->id,
      'title' => $Content->title,
      'content' => $body,
      'description' => $Content->description,
      'type_content' => $Content->type_content,
    ];

    return response()->json($response);
  }

  public function updatePreview($id) {
    $Content = Content::where('id', $id)->first();
    $Content->preview = $Content->preview == '1' ? '0' : '1';
    $Content->save();

    $message = $Content->preview == '1' ? 'back.course.message.update_content_preview' : 'back.course.message.update_content_nonpreview';

    return back()->with('success', Lang::get($message));
  }

}
