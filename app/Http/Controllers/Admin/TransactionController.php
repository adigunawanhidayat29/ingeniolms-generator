<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\AffiliateTransaction;
use App\AffiliateBalance;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\PlayerIdUser;
use App\Course;
use App\InstructorBalance;
use App\Instructor;
use App\ProgramCourse;
use App\ProgramUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionInformation;

class TransactionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if ($request->ajax()) {

      $data = Transaction::select('transactions.id as id', 'invoice', 'method', 'subtotal', 'unique_number', 'status', 'transactions.created_at', 'users.name as name')
        ->join('users', 'users.id', '=', 'transactions.user_id')
        ->orderBy('transactions.created_at', 'desc')
        ->get();

      return Datatables::of($data)

        ->addColumn('action', function ($data) {
          $approve_button = $data->status == '0' ? '<a class="btn btn-success btn-sm" href=/admin/transaction/approve/' . $data->invoice . ' onclick="return confirm(' . "'" . 'Are You Sure?' . "'" . ')">Approve</a>' : '';
          return '
          <form name="delete" method="POST" action="' . url('admin/transaction/delete/' . $data->id) . '">
          ' . csrf_field() . '
          <input type="hidden" name="_method" value="DELETE">
          <a class="btn btn-primary btn-sm" href=/admin/transaction/detail/' . $data->invoice . '><span class="glyphicon glyphicon-search"></span></a>
          ' . $approve_button . '
          <a class="btn btn-warning btn-sm" href="/admin/transaction/confirmation/' . $data->invoice . '">Check Confirmation</a>
          <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm(' . "'" . 'Anda yakin ingin menghapus data ini?' . "'" . ');">
          <span class="glyphicon glyphicon-trash"></span>
          </button>
          </form>
        ';
        })
        ->editColumn('status', function ($data) {
          return status_transaction($data->status);
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    } else {
      exit("Not an AJAX request");
    }
  }

  public function index()
  {
    return view('admin.transaction.list');
  }

  public function detail($invoice)
  {
    $TransactionDetail = TransactionDetail::where('invoice', $invoice);
    if ($TransactionDetail->first()->course_id != null) {
      $TransactionDetail->join('courses', 'courses.id', '=', 'transactions_details.course_id');
    } else {
      $TransactionDetail->join('programs', 'programs.id', '=', 'transactions_details.program_id');
    }
    $data = [
      'transactions_details' => $TransactionDetail->get(),
    ];
    return view('admin.transaction.detail', $data);
  }

  public function approve($invoice)
  {
    // Update status to success
    $Transaction = Transaction::where('invoice', $invoice)->first();
    $Transaction->status = '1';
    $Transaction->save();
    // Update status to success

    // check affiliate transaction
    $AffiliateTransaction = AffiliateTransaction::where('invoice', $invoice)->first();
    if ($AffiliateTransaction) {
      $AffiliateTransaction->status = '1'; // set status affiliate TRANSACTION to success
      $AffiliateTransaction->save();

      $AffiliateBalance = new AffiliateBalance;
      $AffiliateBalance->affiliate_id = $AffiliateTransaction->affiliate_id;
      $AffiliateBalance->debit = $AffiliateTransaction->commission;
      $AffiliateBalance->credit = 0;
      $AffiliateBalance->information = 'Comission transaction ' . $invoice;
      $AffiliateBalance->save();
    }
    // check affiliate transaction

    // add user to course
    $user_id = $Transaction->user_id;
    $TransactionsDetails = TransactionDetail::where('invoice', $invoice)->get();
    foreach ($TransactionsDetails as $transaction_detail) {
      if ($transaction_detail->course_id != null) {
        $CourseUser = new CourseUser;
        $CourseUser->user_id = $Transaction->user_id;
        $CourseUser->course_id = $transaction_detail->course_id;
        $CourseUser->save();

        // add to balance instructor
        $course = Course::where('id', $transaction_detail->course_id)->first();
        $instructor = Instructor::where('user_id', $course->id_author)->first();
        $InstructorBalance = new InstructorBalance;
        $InstructorBalance->instructor_id = $instructor->id;
        $InstructorBalance->debit = $transaction_detail->total;
        $InstructorBalance->credit = 0;
        $InstructorBalance->information = 'transaction course ' . $course->title . ' - ' . $transaction_detail->invoice;
        $InstructorBalance->save();
        // add to balance instructor
      } else {
        $ProgramUser = new ProgramUser;
        $ProgramUser->user_id = $Transaction->user_id;
        $ProgramUser->program_id = $transaction_detail->program_id;
        $ProgramUser->save();

        $ProgramCourses = ProgramCourse::where('program_id', $transaction_detail->program_id)->get();
        foreach ($ProgramCourses as $ProgramCourse) {
          $CourseUser = new CourseUser;
          $CourseUser->user_id = $Transaction->user_id;
          $CourseUser->course_id = $ProgramCourse->course_id;
          $CourseUser->save();
        }
      }
    }

    // add user to course

    // NOTIFICATION PUSH
    // $player_id_users = PlayerIdUser::where(['user_id' => $user_id])->get();
    // $headers = array(
    //   'Content-type: application/json',
    //   'Accept: application/json',
    //   'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'
    // );
    // $player_ids = [];
    // foreach ($player_id_users as $player_id_user) {
    //   array_push($player_ids, $player_id_user->player_id);
    // }
    // $fields = array(
    //   'app_id' => '611e29ae-92d3-4571-be63-062b0f10b000',
    //   'include_player_ids' => $player_ids,
    //   'data' => array('user_id' => $user_id),
    //   'contents' => array('en' => 'Transaksi Anda berhasil, silahkan menikmati kursus di platform kami, terima kasih'),
    //   'headings' => array('en' => 'Transaksi Berhasil'),
    //   'android_group' => 'ingenio_transaction'
    // );
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // $result = curl_exec($ch);
    // curl_close($ch);
    // NOTIFICATION PUSH

    // $user = DB::table('users')->where('id', $Transaction->user_id)->first();
    // Mail::to($user->email)->send(new TransactionInformation($Transaction));
    \Session::flash('success', 'Transaction success');
    return redirect('admin/transaction');
  }

  public function delete($id)
  {
    $Transaction = Transaction::where('id', $id);
    if ($Transaction) {
      $Transaction->delete();
      \Session::flash('success', 'Delete record success');
    } else {
      \Session::flash('error', 'Delete record failed');
    }
    return redirect('transaction');
  }

  public function confirmation($invoice)
  {
    $TransactionConfirmation = TransactionConfirmation::where('invoice', $invoice);
    $data = [
      'transactions_confirmations' => $TransactionConfirmation->get(),
    ];
    return view('admin.transaction.confirmation', $data);
  }
}
