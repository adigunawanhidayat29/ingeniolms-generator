<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AffiliateSetting;
use App\AffiliateUser;
use App\AffiliateBalance;
use App\AffiliateTransaction;
use App\AffiliateWithdrawals;
use Session;

class AffiliateController extends Controller
{
  public function setting(){
    $data = [
      'affiliate_setting' => AffiliateSetting::where('status', '1')->orderBy('id', 'desc')->first(),
    ];
    return view('adminlte::affiliate.setting', $data);
  }

  public function setting_create(Request $request){
    $AffiliateSetting = new AffiliateSetting;
    $AffiliateSetting->commission = $request->commission;
    $AffiliateSetting->discount = $request->discount;
    $AffiliateSetting->start_date = $request->start_date;
    $AffiliateSetting->end_date = $request->end_date;
    $AffiliateSetting->status = '1';
    $AffiliateSetting->save();

    Session::flash('success', 'Update setting success');
    return redirect('affiliate/setting');
  }

  public function setting_update(Request $request){
    $AffiliateSetting = AffiliateSetting::where('status', '1')->first();
    $AffiliateSetting->commission = $request->commission;
    $AffiliateSetting->discount = $request->discount;
    $AffiliateSetting->start_date = $request->start_date;
    $AffiliateSetting->end_date = $request->end_date;
    $AffiliateSetting->save();

    Session::flash('success', 'Update setting success');
    return redirect('affiliate/setting');
  }

  public function user(){
    $AffiliateUser = AffiliateUser::select('users.*', 'affiliate_users.*', 'affiliate_users.id as id')->join('users', 'users.id', '=', 'affiliate_users.user_id')->paginate(10);
    $data = [
      'AffiliateUsers' => $AffiliateUser
    ];
    return view('adminlte::affiliate.user', $data);
  }

  public function balance($id){
    $affiliate_user = AffiliateUser::where('id', $id)->first();
    $AffiliateBalances = AffiliateBalance::where('affiliate_id', $affiliate_user->id)
      ->orderBy('id', 'desc');

    $balance = 0;
    foreach($AffiliateBalances->get() as $AffiliateBalance){
      $balance += $AffiliateBalance->debit ;
    }

    $data = [
      'affiliate_balances' => $AffiliateBalances->paginate(10),
      'balance' => $balance,
    ];
    return view('adminlte::affiliate.balance', $data);
  }

  public function transaction($id){
    $affiliate_user = AffiliateUser::where('id', $id)->first();
    $AffiliateTransactions = AffiliateTransaction::where('affiliate_id', $affiliate_user->id)
      ->join('transactions','transactions.invoice','=','affiliate_transactions.invoice')
      ->join('users','users.id','=','affiliate_transactions.user_id')
      ->orderBy('affiliate_transactions.id', 'desc')
      ->paginate(10);
    $data = [
      'affiliate_transactions' => $AffiliateTransactions,
    ];
    return view('adminlte::affiliate.transaction', $data);
  }

  public function withdrawal($id){
    $affiliate_user = AffiliateUser::where('id', $id)->first();
    $data = [
      'affiliate_withdrawals' => AffiliateWithdrawals::where('affiliate_id', $affiliate_user->id)->paginate(10),
    ];
    return view('adminlte::affiliate.withdrawal', $data);
  }

  public function withdrawal_approve($id){
    $AffiliateWithdrawals = AffiliateWithdrawals::where('id', $id)->first();
    $AffiliateWithdrawals->status = '1';
    $AffiliateWithdrawals->save();

    $AffiliateBalance = new AffiliateBalance;
    $AffiliateBalance->affiliate_id = $AffiliateWithdrawals->affiliate_id;
    $AffiliateBalance->credit = $AffiliateWithdrawals->request;
    $AffiliateBalance->debit = '0';
    $AffiliateBalance->information = 'withdrawal affiliate';
    $AffiliateBalance->save();

    Session::flash('message', 'Success withdrawal');
    return redirect('affiliate/withdrawal/'.$AffiliateWithdrawals->affiliate_id);
  }

  public function profile($id){
    $data = [
      'affiliate_user' => AffiliateUser::where('id', $id)->first(),
    ];
    return view('adminlte::affiliate.profile_edit', $data);
  }

  public function profile_update($id, Request $request){
    $AffiliateUser = AffiliateUser::where('id', $id)->first();
    if($request->file('banner')){
      $destinationPath = asset_path('uploads/affiliate/'); // upload path
      $extension = $request->file('banner')->getClientOriginalExtension(); // getting image extension
      $banner = rand(1111000000,9999999999).'.'.$extension; // renameing image
      $request->file('banner')->move($destinationPath, $banner);
      $AffiliateUser->banner = '/uploads/affiliate/'.$banner;
    }
    $AffiliateUser->bank = $request->bank;
    $AffiliateUser->bank_account_name = $request->bank_account_name;
    $AffiliateUser->bank_account_number = $request->bank_account_number;
    $AffiliateUser->save();

    Session::flash('message', 'Profile berhasil disimpan');
    return redirect('affiliate/profile/'.$id);
  }

}
