<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;

use App\Vcon;
use App\VconWaiting;
use App\VconSetting;
use Illuminate\Http\Request;
use Lang;

class VconsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $vcons = Vcon::where('title', 'LIKE', "%$keyword%")
                ->orWhere('password', 'LIKE', "%$keyword%")
                ->orWhere('password_moderator', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $vcons = Vcon::latest()->paginate($perPage);
        }

        return view('admin.vcons.index', compact('vcons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $vcon_setting = VconSetting::get();
        return view('admin.vcons.create', compact('vcon_setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Vcon::create($requestData);

        return redirect('admin/vcons')->with('success', Lang::get('back.vcon.alert_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $vcon = Vcon::findOrFail($id);

        return view('admin.vcons.show', compact('vcon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $vcon = Vcon::findOrFail($id);
        $vcon_setting = VconSetting::get();

        return view('admin.vcons.edit', compact('vcon', 'vcon_setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $vcon = Vcon::findOrFail($id);
        $vcon->update($requestData);

        return redirect('admin/vcons')->with('success', Lang::get('back.vcon.alert_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Vcon::destroy($id);

        return redirect('admin/vcons')->with('success', Lang::get('back.vcon.alert_deleted'));
    }

    public function waiting($id)
    {
        $vcon_waitings = VconWaiting::where('vcon_id', $id)->get();

        return view('admin.vcons.waiting', compact('vcon_waitings'));
    }

    public function approve($id)
    {
        $vcon_waitings = VconWaiting::where('id', $id)->first();
        $vcon_waitings->status = '1';
        $vcon_waitings->save();

        return redirect()->back()->with('success', Lang::get('back.vcon.alert_approved'));
    }

    public function recording($id)
    {
        $vcon = Vcon::findOrFail($id);

        $roomID = $vcon->id . env('APP_URL', null);
        // echo $roomID;

        $bbb = new BigBlueButton();
		$recordingParams = new GetRecordingsParameters();
		$recordingParams->setMeetingId($roomID); // get by meeting id
		$response = $bbb->getRecordings($recordingParams);
		if ($response->getReturnCode() == 'SUCCESS') {
            $recordings = $response->getRawXml()->recordings;
            // dd($response->getRawXml()->recordings);
		}else{
		  $recordings = [];
        }	
        
        $data = [
            'vcon' => $vcon,
            'recordings' => $recordings
        ];

        return view('admin.vcons.recording', $data);
    }

    public function test_isrunning($id) {
        $vcon = Vcon::findOrFail($id);
        $roomID = $vcon->id . env('APP_URL', null);
        // echo $roomID;
        $bbb = new BigBlueButton();
        $IsMeetingRunningParameters = new IsMeetingRunningParameters($roomID);
        $response = $bbb->isMeetingRunning($IsMeetingRunningParameters);
    
        // dd($response->getRawXml());
        
        if ($response->getReturnCode() == 'SUCCESS') {
          $meetingIsRunning= $response->getRawXml()->running;
        }else{
          $meetingIsRunning = null;
        }	
    
        echo $meetingIsRunning;
      }
    
      public function test_endmeeting(Request $request) {
        $bbb = new BigBlueButton($request->salt, $request->base_url);
        $EndMeetingParameters = new EndMeetingParameters($request->id, $request->password);
        $response = $bbb->endMeeting($EndMeetingParameters);
    
        // echo($response->getRawXml()->message);

        return redirect()->back()->with('success', Lang::get('back.vcon.alert_ended'));
        
      }

      public function test_endmeetings($server_id) {

        $VconSetting = VconSetting::where('id', $server_id)->first();

        $BBB_SECURITY_SALT = $VconSetting->salt;
        $BBB_SERVER_BASE_URL = $VconSetting->base_url;

        $bbb = new BigBlueButton($BBB_SECURITY_SALT, $BBB_SERVER_BASE_URL);
        $response = $bbb->getMeetings();

        $meetings = $response->getRawXml()->meetings->meeting;
        
        foreach($meetings as $data) {

            $meetingID = (string) $data->meetingID;
            $moderatorPW = (string) $data->moderatorPW;

            // $endbbb = new BigBlueButton($BBB_SECURITY_SALT, $BBB_SERVER_BASE_URL);
            // $IsMeetingRunningParameters = new IsMeetingRunningParameters($meetingID);
            // $responses = $endbbb->isMeetingRunning($IsMeetingRunningParameters);
            // dd($responses->getRawXml());

            $endbbb = new BigBlueButton($BBB_SECURITY_SALT, $BBB_SERVER_BASE_URL);
            $EndMeetingParameters = new EndMeetingParameters((string) $data->meetingID, (string) $data->moderatorPW);
            $responses = $endbbb->endMeeting($EndMeetingParameters);
            // dd($responses->getRawXml());
        }

        return redirect()->back()->with('success', Lang::get('back.vcon.alert_all_ended'));
    
      }
    
      public function test_meetinginfo($id) {
        $vcon = Vcon::findOrFail($id);
        $roomID = $vcon->id . env('APP_URL', null);
        // echo $roomID;
        $bbb = new BigBlueButton();
        $GetMeetingInfoParameters = new GetMeetingInfoParameters($roomID, $vcon->password_moderator);
        $response = $bbb->getMeetingInfo($GetMeetingInfoParameters);
    
        dd($response->getRawXml());
        
      }
    
      public function test_getmeetings($server_id) {

        $VconSetting = VconSetting::where('id', $server_id)->first();

        $BBB_SECURITY_SALT = $VconSetting->salt;
        $BBB_SERVER_BASE_URL = $VconSetting->base_url;

        $bbb = new BigBlueButton($BBB_SECURITY_SALT, $BBB_SERVER_BASE_URL);
        $response = $bbb->getMeetings();
    
        // dd($response->getRawXml());

        $data = [
            'meetings' => $response->getRawXml()->meetings->meeting,
            'VconSetting' => $VconSetting
        ];
        
        return view('admin.vcons.meetings', $data);
      }
}
