<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Language;
use Illuminate\Http\Request;
use File;
use ZipArchive;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $languages = Language::where('title', 'LIKE', "%$keyword%")
                ->orWhere('alias', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('icon', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $languages = Language::latest()->paginate($perPage);
        }

        return view('admin.languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = explode(',', $request->language);

        \File::copyDirectory( base_path() . '/resources/lang/id', base_path('/resources/lang/' . strtolower($requestData[1]) ));
        
        $Language = new Language;
        $Language->title = $requestData[0];
        $Language->alias = strtolower($requestData[1]);
        $Language->status = $request->status == 'on' ? '1' : '0';
        $Language->save();

        return redirect('admin/languages')->with('flash_message', 'Language added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $language = Language::findOrFail($id);

        return view('admin.languages.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $language = Language::findOrFail($id);

        return view('admin.languages.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $language = Language::findOrFail($id);
        $requestData['status'] = $requestData['status'] == 'on' ? '1' : '0';
        $language->update($requestData);

        return redirect('admin/languages')->with('flash_message', 'Language updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Language::destroy($id);

        return redirect('admin/languages')->with('flash_message', 'Language deleted!');
    }

    public function upload(Request $request, $id) {
        $language = Language::findOrFail($id);
        $files = $request->file('files');
        foreach ($files as $data) {
            // upload
            $destinationPath = base_path() . '/resources/lang/' . $language->alias;
            $file = $data->getClientOriginalName();
            $data->move($destinationPath, $file);
            // upload
        }

        return redirect('admin/languages')->with('flash_message', 'Language uploaded!');
    }

    public function download($id) {
        $language = Language::findOrFail($id);
        
        $zip = new ZipArchive;
        $fileName = $language->alias . '.zip';
        if ($zip->open(public_path('/langs/'.$fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(base_path('/resources/lang/' . $language->alias));
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
    
        return response()->download(public_path('/langs/' . $fileName));
    }

    public function setDefault($id) {
        \DB::table('languages')->update(array('status' => '0'));
        $language = Language::findOrFail($id);
        $language->status = '1';
        $language->save();

        \Session::put(['locale' => $language->alias]);
        \App::setLocale($language->alias);

        return redirect('admin/languages')->with('success', 'Language updated!');
    }
}
