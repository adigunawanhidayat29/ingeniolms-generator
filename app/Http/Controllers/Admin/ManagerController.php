<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Manager;
use App\User;
use App\UserGroup;
use Illuminate\Support\Facades\Mail;
use Session;

class ManagerController extends Controller
{
  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = User::select('users.id as id','name','email','status')->join('managers','managers.user_id','=','users.id')->orderBy('managers.id', 'desc')->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        if($data->status == '0'){
          $button_status = '<a onclick="return confirm('."'".'Are You Sure?'."'".');" href="'. url('manager/approve/'.$data->id) .'" class="btn btn-success btn-sm">Approve</a>';
        }else{
          $button_status = '<a onclick="return confirm('."'".'Are You Sure?'."'".');" href="'. url('manager/block/'.$data->id) .'" class="btn btn-danger btn-sm">Block</a>';
        }

        return '
          <a href="'. url('manager/update/'.$data->id) .'" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span></a>
          '.$button_status.'
        ';
      })
      ->editColumn('status', function($data){
        return status_manager($data->status);
      })
      ->rawColumns(['status','action'])
      ->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('admin.manager.list');
  }

  public function create(){
    $data = [
      'action' => \Request::route()->getPrefix().'/manager/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'name' => old('name'),
      'email' => old('email'),
      'password' => old('password'),
    ];
    return view('admin.manager.form', $data);
  }

  public function create_action(Request $request){

    $User = new User;
    $User->name = $request->name;
    $User->email = $request->email;
    $User->password = bcrypt($request->password);

    if($User->save()){

      // insert manager
      $manager = new manager;
      $manager->status = '1';
      $manager->user_id = $User->id;
      $manager->save();

      // insert manager

      \Session::flash('success', 'Create record success');
    }else{
      \Session::flash('error', 'Create record failed');
    }
    return redirect(\Request::route()->getPrefix().'/manager');

  }

  public function update($id){
    $User = User::where('id', $id)->first();
    $data = [
      'action' => \Request::route()->getPrefix().'/manager/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $User->id),
      'name' => old('name', $User->name),
      'email' => old('email', $User->email),
    ];
    return view('admin.manager.form', $data);
  }

  public function update_action(Request $request){
    $User = User::where('id', $request->id)->first();

    $User->name = $request->name;
    $User->email = $request->email;

    if($User->save()){
      \Session::flash('success', 'Update record success');
    }else{
      \Session::flash('error', 'Update record failed');
    }

    return redirect(\Request::route()->getPrefix().'/manager');
  }

  public function delete($id){
    $User = User::where('id', $id);
    if($User){
      $User->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect(\Request::route()->getPrefix().'/manager');
  }

  public function approve($id){
    $manager = manager::where('user_id', $id)->first();
    $manager->status = '1';
    $manager->save();

    $User = User::find($manager->user_id);

    // insert user group
    if(!UserGroup::where('user_id', $id)->first()){
      $UserGroup = new UserGroup;
      $UserGroup->level_id = '2';
      $UserGroup->user_id = $id;
      $UserGroup->save();
    }
    // insert user group

    // Mail::to([$User->email, 'tsauri@dataquest.co.id'])->send(new managerJoin($manager));
    \Session::flash('success', 'Approve manager success');
    return redirect(\Request::route()->getPrefix().'/manager');
  }

  public function block($id){
    $manager = manager::where('user_id', $id)->first();
    $manager->status = '0';
    $manager->save();

    \Session::flash('success', 'Block manager success');
    return redirect(\Request::route()->getPrefix().'/manager');
  }
}
