<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Events\BlastNotificationEvent;
use App\BlastNotification;
use Illuminate\Http\Request;

class BlastNotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function getDataJson(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blast_notifications = BlastNotification::orderBy('id', 'desc')->paginate($perPage);
        } else {
            $blast_notifications = BlastNotification::orderBy('id', 'desc')->paginate($perPage);
        }

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => [
                'notifications' => $blast_notifications
            ]
        ]);
    }

    public function getDataDetailJson($id)
    {
        $blast_notifications = BlastNotification::find($id)->first();
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => [
                'notifications' => $blast_notifications
            ]
        ]);
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blast_notifications = BlastNotification::latest()->paginate($perPage);
        } else {
            $blast_notifications = BlastNotification::latest()->paginate($perPage);
        }

        return view('admin.blast_notifications.index', compact('blast_notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.blast_notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $BlastNotification = BlastNotification::create($requestData);
        
        event(new BlastNotificationEvent($BlastNotification));
        // dd($BlastNotification);

        return redirect('admin/blast_notifications')->with('flash_message', 'BlastNotification added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $blast_notification = BlastNotification::findOrFail($id);

        return view('admin.blast_notifications.show', compact('blast_notification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blast_notification = BlastNotification::findOrFail($id);

        return view('admin.blast_notifications.edit', compact('blast_notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $blast_notification = BlastNotification::findOrFail($id);
        $blast_notification->update($requestData);

        return redirect('admin/blast_notifications')->with('flash_message', 'BlastNotification updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BlastNotification::destroy($id);

        return redirect('admin/blast_notifications')->with('flash_message', 'BlastNotification deleted!');
    }
}
