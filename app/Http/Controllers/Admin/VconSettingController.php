<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VconSetting;
use Lang;

class VconSettingController extends Controller
{
    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            
            return true;
        } else {
            return false;
        }
    }

    public function index($id) {
      $vcon_setting = VconSetting::where('id', $id)->first();
      return view('admin.setting.vcon', compact('vcon_setting'));
    }

    public function store(Request $request) {
      $VconSetting = new VconSetting;
      $VconSetting->base_url = $request->base_url;
      $VconSetting->salt = $request->salt;
      $VconSetting->status = '1';
      $VconSetting->save();
      
      return redirect('admin/setting/vcon');
    }

    public function update(Request $request) {
      
        $VconSetting = VconSetting::where('id', $request->id)->first();
        $VconSetting->base_url = $request->base_url;
        $VconSetting->salt = $request->salt;
        // $VconSetting->status = '1';
        $VconSetting->save();

        // CHANGE ENV FILE
        // $env_update = $this->changeEnv([
        //   'BBB_SECURITY_SALT'   => $request->salt,
        //   'BBB_SERVER_BASE_URL'   => $request->base_url
        // ]);
        // CHANGE ENV FILE
        
      
        return redirect('admin/setting/vcon')->with('success', Lang::get('back.vcon_setting.setting_success'));
    }

    public function setDefault(Request $request, $id) {
      $VconSetting = VconSetting::where('id', $id)->first();
      $VconSetting->status = '1';
      $VconSetting->save();

      // CHANGE ENV FILE
      $env_update = $this->changeEnv([
        'BBB_SECURITY_SALT'   => $VconSetting->salt,
        'BBB_SERVER_BASE_URL'   => $VconSetting->base_url
      ]);
      // CHANGE ENV FILE

      $VconSetting = VconSetting::where('id', '!=', $id)->update(['status' => '0']);
      
      return redirect('admin/setting/vcon')->with('success', Lang::get('back.vcon_setting.alert_set_default'));
    }

    public function delete($id)
    {
        VconSetting::destroy($id);

        return redirect('admin/setting/vcon')->with('success', Lang::get('back.vcon_setting.alert_delete_success'));
    }
}
