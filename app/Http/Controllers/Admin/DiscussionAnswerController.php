<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\DiscussionAnswer;

class DiscussionAnswerController extends Controller
{
  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = DiscussionAnswer::select('discussions_answers.id as id', 'discussions.body as discussion','discussions_answers.body as body','discussions_answers.status', 'discussions_answers.created_at as created_at')
      ->join('users','users.id','=','discussions_answers.user_id')
      ->join('discussions','discussions.id','=','discussions_answers.discussion_id')
      ->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        $publish_button = $data->status == '0' ? '<a class="btn btn-success btn-sm" href=/discussion-answer/publish/'.$data->id.' onclick="return confirm('."'".'Are You Sure?'."'".')">Publish</a>' : '';
        $block_button = $data->status == '1' ? '<a class="btn btn-warning btn-sm" href=/discussion-answer/block/'.$data->id.' onclick="return confirm('."'".'Are You Sure?'."'".')">Block</a>' : '';
        return '
          <form name="delete" method="POST" action="'. url('discussion-answer/delete/'.$data->id) .'">
          '. csrf_field() .'
          <input type="hidden" name="_method" value="DELETE">
          '.$publish_button.'
          '.$block_button.'
          <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
          <span class="glyphicon glyphicon-trash"></span>
          </button>
          </form>
        ';
      })
      ->editColumn('status', function($data){
        return status_discussion($data->status);
      })
      ->rawColumns(['status','action'])
      ->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('adminlte::discussion-answer.list');
  }

  public function publish($id){
    $DiscussionAnswer = DiscussionAnswer::where('id', $id)->first();
    $DiscussionAnswer->status = '1';
    $DiscussionAnswer->save();

    \Session::flash('success', 'DiscussionAnswer success');
    return redirect('discussion-answer');
  }

  public function block($id){
    $DiscussionAnswer = DiscussionAnswer::where('id', $id)->first();
    $DiscussionAnswer->status = '0';
    $DiscussionAnswer->save();

    \Session::flash('success', 'DiscussionAnswer blocked');
    return redirect('discussion-answer');
  }

  public function delete($id){
    $DiscussionAnswer = DiscussionAnswer::where('id', $id);
    if($DiscussionAnswer){
      $DiscussionAnswer->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('discussion-answer');
  }
}
