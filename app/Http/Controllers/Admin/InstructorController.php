<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Instructor;
use App\InstructorWithdrawal;
use App\InstructorBalance;
use App\InstructorBank;
use App\User;
use App\UserGroup;
use Illuminate\Support\Facades\Mail;
use App\Mail\InstructorJoin;
use Session;
use Auth;
use Lang;
use Storage;

class InstructorController extends Controller
{
  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = User::select('users.id as id','name','email','status', 'photo')->join('instructors','instructors.user_id','=','users.id')->orderBy('instructors.id', 'desc')->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        if($data->status == '0'){
          $button_status = '
            <a onclick="return confirm('."'".'Are You Sure?'."'".');" href="'. url('admin/instructor/approve/'.$data->id) .'" class="btn btn-success btn-sm">
              <i class="material-icons">check</i><span>'. Lang::get('back.user_teacher.option_approve_button') .'</span>
            </a>
          ';
        }else{
          $button_status = '
            <a onclick="return confirm('."'".'Are You Sure?'."'".');" href="'. url('admin/instructor/block/'.$data->id) .'" class="btn btn-danger btn-sm">
              <i class="material-icons">block</i><span>'. Lang::get('back.user_teacher.option_block_button') .'</span>
            </a>
          ';
        }

        return '
          
          <a href="'. url('admin/instructor/update/'.$data->id) .'" class="btn btn-warning btn-sm">
            <i class="material-icons">edit</i><span>'. Lang::get('back.user_teacher.option_update_button') .'</span>
          </a>
          '.$button_status.'
        ';
      })
      ->editColumn('status', function($data){
        return status_instructor($data->status);
      })
      ->editColumn('photo', function($data){
        return $data->photo;
      })
      ->rawColumns(['status','action'])
      ->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('admin.instructor.list');
  }

  public function create(){
    $data = [
      'action' => \Request::route()->getPrefix().'/instructor/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'name' => old('name'),
      'username' => old('username'),
      'email' => old('email'),
      'password' => old('password'),
      'short_description' => old('short_description'),
      'description' => old('description'),
      'facebook' => old('facebook'),
      'twitter' => old('twitter'),
      'instagram' => old('instagram'),
      'tagline' => old('tagline'),
      'community_id' => old('community_id'),
      'communities' => DB::table('communities')->get(),
    ];
    return view('admin.instructor.form', $data);
  }

  public function create_action(Request $request){
    $Auth = Auth::user();

    $User = new User;
    $User->name = $request->name;
    $User->description = $request->description;
    $User->short_description = $request->short_description;
    $User->facebook = $request->facebook;
    $User->twitter = $request->twitter;
    $User->instagram = $request->instagram;
    $User->tagline = $request->tagline;
    $User->is_active = '1';
    $User->email = $request->email;
    $User->username = $request->username;
    $User->password = bcrypt($request->password);

    if($request->file('image')){
      // $destinationPath = asset_path('uploads/users/'); // upload path
      // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      // $image = str_slug($Auth->name).rand(1111,9999).'.'.$extension; // renameing image
      // $request->file('image')->move($destinationPath, $image);
      // $User->photo = '/uploads/users/'.$image;
      $file = $request->file('image');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->name).rand(1111,9999).'.'.$extension; // renameing image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/users/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/users/', $image);
      $User->photo = $path_file;
    }

    if($User->save()){

      // inser user group
      $UserGroup = new UserGroup;
      $UserGroup->level_id = '2';
      $UserGroup->user_id = $User->id;
      $UserGroup->save();
      // inser user group

      // insert instructor
      $Instructor = new Instructor;
      $Instructor->status = '1';
      $Instructor->user_id = $User->id;
      $Instructor->community_id = $request->community_id;
      $Instructor->save();

      // insert instructor

      \Session::flash('success', Lang::get('back.user_teacher.alert_created'));
    }else{
      \Session::flash('error', Lang::get('back.user_teacher.alert_created_failed'));
    }
    return redirect(\Request::route()->getPrefix().'/instructor');

  }

  public function update($id){
    $User = User::where('id', $id)->first();
    $Instructor = Instructor::where('user_id', $id)->first();

    $data = [
      'action' => \Request::route()->getPrefix().'/instructor/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $User->id),
      'name' => old('name', $User->name),
      'email' => old('email', $User->email),
      'username' => old('username', $User->username),
      'description' => old('description', $User->description),
      'short_description' => old('short_description', $User->short_description),
      'facebook' => old('facebook', $User->facebook),
      'instagram' => old('instagram', $User->instagram),
      'twitter' => old('twitter', $User->twitter),
      'tagline' => old('tagline', $User->tagline),
      'community_id' => old('community_id', $Instructor ? $Instructor->community_id : ''),
      'communities' => DB::table('communities')->get(),
    ];
    return view('admin.instructor.form', $data);
  }

  public function update_action(Request $request){
    $Auth = Auth::user();
    $User = User::where('id', $request->id)->first();

    $User->name = $request->name;
    $User->email = $request->email;
    $User->username = $request->username;
    $User->description = $request->description;
    $User->short_description = $request->short_description;
    $User->facebook = $request->facebook;
    $User->twitter = $request->twitter;
    $User->instagram = $request->instagram;
    $User->tagline = $request->tagline;

    if($request->file('image')){
      // $destinationPath = asset_path('uploads/users/'); // upload path
      // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      // $image = str_slug($Auth->name).rand(1111,9999).'.'.$extension; // renameing image
      // $request->file('image')->move($destinationPath, $image);
      // $User->photo = '/uploads/users/'.$image;
      $file = $request->file('image');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->name).rand(1111,9999).'.'.$extension; // renameing image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/users/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/users/', $image);
      $User->photo = $path_file;
    }

    // upddate instructor
    $Instructor = Instructor::where('user_id', $User->id)->first();
    $Instructor->community_id = $request->community_id;
    $Instructor->save();
    // upddate instructor

    if($User->save()){
      \Session::flash('success', Lang::get('back.user_teacher.alert_updated'));
    }else{
      \Session::flash('error', Lang::get('back.user_teacher.alert_updated_failed'));
    }

    return redirect(\Request::route()->getPrefix().'/instructor');
  }

  public function delete($id){
    $User = User::where('id', $id);
    if($User){
      $User->delete();
      \Session::flash('success', Lang::get('back.user_teacher.alert_deleted'));
    }else{
      \Session::flash('error', Lang::get('back.user_teacher.alert_deleted_failed'));
    }

    return redirect(\Request::route()->getPrefix().'/instructor');
  }

  public function approve($id){
    $instructor = Instructor::where('user_id', $id)->first();
    $instructor->status = '1';
    $instructor->save();

    $User = User::find($instructor->user_id);

    // insert user group
    if(!UserGroup::where('user_id', $id)->first()){
      $UserGroup = new UserGroup;
      $UserGroup->level_id = '2';
      $UserGroup->user_id = $id;
      $UserGroup->save();
    }
    // insert user group

    // Mail::to([$User->email, 'tsauri@dataquest.co.id'])->send(new InstructorJoin($instructor));
    \Session::flash('success', Lang::get('back.user_teacher.alert_activated'));
    return redirect(\Request::route()->getPrefix().'/instructor');
  }

  public function block($id){
    $instructor = Instructor::where('user_id', $id)->first();
    $instructor->status = '0';
    $instructor->save();

    \Session::flash('success', Lang::get('back.user_teacher.alert_non_activated'));
    return redirect(\Request::route()->getPrefix().'/instructor');
  }

  public function withdrawals(){
    $data = [
      'withdrawals' => InstructorWithdrawal::select('users.name', 'instructor_withdrawals.*')
        ->join('instructors', 'instructors.id', '=', 'instructor_withdrawals.instructor_id')
        ->join('users', 'users.id', '=', 'instructors.user_id')
        ->get(),
    ];
    return view('admin.instructor.withdrawal', $data);
  }

  public function withdrawal_delete($id){
    $InstructorWithdrawal = InstructorWithdrawal::where('id', $id)->delete();

    Session::flash('message', 'Permintaan penarikan Anda dihapus');
    return redirect()->back();
  }

  public function withdrawal_approve($id){
    $InstructorWithdrawal = InstructorWithdrawal::where('id', $id)->first();
    $InstructorWithdrawal->status = '1';
    $InstructorWithdrawal->save();

    $InstructorBalance = new InstructorBalance;
    $InstructorBalance->instructor_id = $InstructorWithdrawal->instructor_id;
    $InstructorBalance->debit = 0;
    $InstructorBalance->credit = $InstructorWithdrawal->amount;
    $InstructorBalance->information = 'penarikan dana transaksi';
    $InstructorBalance->save();

    Session::flash('message', 'Permintaan penarikan Anda diterima');
    return redirect()->back();
  }

  public function withdrawal_detail($id){
    $InstructorWithdrawal = InstructorWithdrawal::join('instructors', 'instructors.id', '=', 'instructor_withdrawals.instructor_id')
      ->join('users', 'users.id', '=', 'instructors.user_id')
      ->where('instructor_withdrawals.id', $id)
      ->first();
    $data = [
      'withdrawal' => $InstructorWithdrawal,
      'instructor_bank' => InstructorBank::where('instructor_id', $InstructorWithdrawal->instructor_id)->first(),
    ];
    return view('admin.instructor.withdrawal_detail', $data);
  }

}
