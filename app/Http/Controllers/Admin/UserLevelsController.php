<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\UserLevel;
use Validator;
use Lang;

class UserLevelsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if ($request->ajax()) {

      $data = UserLevel::select('id', 'name')->get();

      return DataTables::of($data)

        ->addColumn('action', function ($data) {
          return '
        
        <form name="delete" method="POST" action="' . url('admin/user-levels/delete/' . $data->id) . '">
          <a href="' . url('admin/level-has-permission/create?level=' . $data->id) . '" class="btn btn-info btn-sm">
            <i class="material-icons">add</i><span>' . Lang::get('back.user_level.add_permission_button') . '</span>
          </a>
          <a href="' . url('admin/user-levels/update/' . $data->id) . '" class="btn btn-warning btn-sm">
            <i class="material-icons">edit</i><span>' . Lang::get('back.user_level.edit_button') . '</span>
          </a>

          ' . csrf_field() . '
          <input type="hidden" name="_method" value="DELETE">
          <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm(' . "'" . 'Anda yakin ingin menghapus data ini?' . "'" . ');">
            <i class="material-icons">delete</i><span>' . Lang::get('back.user_level.delete_button') . '</span>
          </button>
        </form>

        ';
        })->make(true);
    } else {
      exit("Not an AJAX request");
    }
  }

  public function index()
  {
    return view('admin.UserLevels.list');
  }

  public function create()
  {
    $data = [
      'action' => 'admin/user-levels/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'name' => old('name'),
    ];
    return view('admin.UserLevels.form', $data);
  }

  public function create_action(Request $request)
  {

    $validator = $this->rules($request);
    if ($validator->fails()) {
      \Session::flash('error', $validator->errors());
      return $this->create();
    }

    $UserLevel = new UserLevel;
    $UserLevel->name = $request->name;

    if ($UserLevel->save()) {
      \Session::flash('success', 'Create record success');
    } else {
      \Session::flash('error', 'Create record failed');
    }
    return redirect('admin/user-levels');
  }

  public function update($id)
  {
    $UserLevel = UserLevel::where('id', $id)->first();
    $data = [
      'action' => 'admin/user-levels/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $UserLevel->id),
      'name' => old('name', $UserLevel->name),
    ];
    return view('admin.UserLevels.form', $data);
  }

  public function update_action(Request $request)
  {
    $UserLevel = UserLevel::where('id', $request->id)->first();

    $UserLevel->name = $request->name;

    if ($UserLevel->save()) {
      \Session::flash('success', 'Update record success');
    } else {
      \Session::flash('error', 'Update record failed');
    }

    return redirect('admin/user-levels');
  }

  public function delete($id)
  {
    $UserLevel = UserLevel::where('id', $id);
    if ($UserLevel) {
      $UserLevel->delete();
      \Session::flash('success', 'Delete record success');
    } else {
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('admin/user-levels');
  }

  public function rules($request)
  {
    return $validator = Validator::make($request->all(), [
      'name' => 'required',
    ]);
  }
}
