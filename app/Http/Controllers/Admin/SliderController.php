<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Slider;
use Lang;
use Storage;
use File;

class SliderController extends Controller
{
    public function index(){        
        return view('admin.Slider.index');
    }

    public function serverside(Request $request)
    {
        if($request->ajax()){
            $data = Slider::select('*')->get();
            return Datatables::of($data)      
            ->editColumn('status', function($data){
                return $data->status == '1' ? Lang::get('back.banner_slider.enabled_status') : Lang::get('back.banner_slider.disabled_status');
            })
            ->editColumn('image', function($data){
                return asset_url($data->image);
            })
            ->addColumn('action', function ($data)
            {
                return '                
                    <a href="'. url(\Request::route()->getPrefix() .'/sliders/edit/'.$data->id) .'" class="btn btn-warning btn-sm">
                        <i class="material-icons">edit</i><span>'. Lang::get('back.banner_slider.edit_button') .'</span>
                    </a>
                    <a onclick="return confirm('."'".Lang::get('back.alerts.confirm_delete')."'".')" href="'. url(\Request::route()->getPrefix() .'/sliders/delete/'.$data->id) .'" class="btn btn-danger btn-sm">
                        <i class="material-icons">delete</i><span>'. Lang::get('back.banner_slider.delete_button') .'</span>
                    </a>
                ';
            })->make(true);
        }else{
            exit("Not an AJAX request");
        }
    }

    public function create(){
        $data = [
            'action' => \Request::route()->getPrefix() .'/sliders/store',
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'title' => old('title'),
            'description' => old('description'),
            'image' => old('image'),
            'status' => old('status'),
            'header' => old('header'),
            'header_font_size' => old('header_font_size'),
            'header_font_weight' => old('header_font_weight'),
            'header_font_color' => old('header_font_color'),
            'subheader' => old('subheader'),
            'subheader_font_size' => old('subheader_font_size'),
            'subheader_font_weight' => old('subheader_font_weight'),
            'subheader_font_color' => old('subheader_font_color'),
            'text_alignment' => old('text_alignment'),
            'text_position' => old('text_position'),
            'text_button' => old('text_button'),
            'link_button' => old('link_button'),
            'overlay_color' => old('overlay_color'),
            'overlay_transparention' => old('overlay_transparention'),
        ];
        return view('admin.Slider.form', $data);
    }

    public function store(Request $request){
        $Slider = new Slider;
        // image
        if($request->file('image')){
            // $destinationPath = public_path('/uploads/sliders/'); // upload path
            // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            // $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            // $request->file('image')->move($destinationPath, $image); // uploading file to given path

            // upload
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->title) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/sliders/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/sliders/', $name_file);
            // upload

            $Slider->image = $path_file;
        }else{
            $Slider->image = 'dynamic/uploads/sliders/default.jpg';
        }
        // image
        $Slider->title = $request->title ;
        $Slider->description = $request->description ;
        $Slider->status = $request->status ? $request->status : 1 ;
        $Slider->header = $request->header;
        $Slider->header_font_size = $request->header_font_size;
        $Slider->header_font_weight = $request->header_font_weight;
        $Slider->header_font_color = $request->header_font_color;
        $Slider->subheader = $request->subheader;
        $Slider->subheader_font_size = $request->subheader_font_size;
        $Slider->subheader_font_weight = $request->subheader_font_weight;
        $Slider->subheader_font_color = $request->subheader_font_color;
        $Slider->text_alignment = $request->text_alignment;
        $Slider->text_position = $request->text_position;
        $Slider->text_button = $request->text_button;
        $Slider->link_button = $request->link_button;
        $Slider->overlay_color = $request->overlay_color;
        $Slider->overlay_transparention = $request->overlay_transparention;
        $Slider->save();

        return redirect(\Request::route()->getPrefix() . '/sliders')->with('success', Lang::get('back.banner_slider.alert_save'));
    }

    public function edit($id){
        $Slider = Slider::where('id', $id)->first();
        $data = [
            'action' => \Request::route()->getPrefix() .'/sliders/update',
            'method' => 'post',
            'button' => 'Update',
            'id' => old('id', $Slider->id),
            'title' => old('title', $Slider->title),
            'description' => old('description', $Slider->description),
            'image' => old('image', $Slider->image),
            'status' => old('status', $Slider->status),
            'header' => old('header', $Slider->header),
            'header_font_size' => old('header_font_size', $Slider->header_font_size),
            'header_font_weight' => old('header_font_weight', $Slider->header_font_weight),
            'header_font_color' => old('header_font_color', $Slider->header_font_color),
            'subheader' => old('subheader', $Slider->subheader),
            'subheader_font_size' => old('subheader_font_size', $Slider->subheader_font_size),
            'subheader_font_weight' => old('subheader_font_weight', $Slider->subheader_font_weight),
            'subheader_font_color' => old('subheader_font_color', $Slider->subheader_font_color),
            'text_alignment' => old('text_alignment', $Slider->text_alignment),
            'text_position' => old('text_position', $Slider->text_position),
            'text_button' => old('text_button', $Slider->text_button),
            'link_button' => old('link_button', $Slider->link_button),
            'overlay_color' => old('overlay_color', $Slider->overlay_color),
            'overlay_transparention' => old('overlay_transparention', $Slider->overlay_transparention),
        ];
        return view('admin.Slider.form', $data);
    }

    public function update(Request $request)
    {
        $Slider = Slider::where('id', $request->id)->first();       
        // image
        if($request->file('image')){
            // $destinationPath = public_path('/uploads/sliders/'); // upload path
            // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            // $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            // $request->file('image')->move($destinationPath, $image); // uploading file to given path

            // upload
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->title) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/sliders/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/sliders/', $name_file);
            // upload

            $Slider->image = $path_file;
        }
        // image 
        $Slider->title = $request->title ;
        $Slider->description = $request->description ;        
        $Slider->status = $request->status ;
        $Slider->header = $request->header;
        $Slider->header_font_size = $request->header_font_size;
        $Slider->header_font_weight = $request->header_font_weight;
        $Slider->header_font_color = $request->header_font_color;
        $Slider->subheader = $request->subheader;
        $Slider->subheader_font_size = $request->subheader_font_size;
        $Slider->subheader_font_weight = $request->subheader_font_weight;
        $Slider->subheader_font_color = $request->subheader_font_color;
        $Slider->text_alignment = $request->text_alignment;
        $Slider->text_position = $request->text_position;
        $Slider->text_button = $request->text_button;
        $Slider->link_button = $request->link_button;
        $Slider->overlay_color = $request->overlay_color;
        $Slider->overlay_transparention = $request->overlay_transparention;
        $Slider->save();     

        return redirect(\Request::route()->getPrefix() . '/sliders')->with('success', Lang::get('back.banner_slider.alert_save'));;
    }

    public function delete(Request $request, $id)
    {
        $Slider = Slider::where('id', $id);
        if($Slider){
            $Slider->delete();
        }else{
            \Session::flash('error', 'Delete record failed');
        }

        return redirect(\Request::route()->getPrefix() . '/sliders')->with('success', Lang::get('back.banner_slider.alert_delete'));;
    }
}