<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CourseProject;
use Image;
use Session;

class CourseProjectController extends Controller
{
  public function index()
  {
    $data = [
      'CourseProjects' => CourseProject::paginate(10),
    ];
    return view('adminlte::course_project.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($course_id)
  {
    $data = [
      'action' => '/course/project/store/'.$course_id,
      'button' => 'Create',
      'id' => old('id'),
      'course_id' => old('course_id'),
      'title' => old('title'),
      'description' => old('description'),
      'image' => old('image'),
      'status' => old('status'),
    ];
    return view('adminlte::course_project.form', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store($course_id, Request $request)
  {
    $CourseProject = new CourseProject;

    // image
    if($request->file('image')){
      $destinationPath = asset_path('uploads/course_projects/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath. "small_".$image);
      $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath. "medium_".$image);
      $request->file('image')->move($destinationPath, $image); // uploading file to given path

      $CourseProject->image = '/uploads/course_projects/'.$image;
    }
    // image

    $CourseProject->title = $request->title;
    $CourseProject->course_id = $course_id;
    $CourseProject->description = $request->description;
    $CourseProject->status = $request->status;
    $CourseProject->save();

    Session::flash('success', 'Create record success');
    return redirect('course/project');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $CourseProject = CourseProject::find($id);
    $data = [
      'CourseProject' => $CourseProject,
    ];
    return view('adminlte::course_project.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $CourseProject = CourseProject::find($id)->first();

    $data = [
      'action' => '/course/project/update',
      'button' => 'Update',
      'id' => old('id', $CourseProject->id),
      'title' => old('title', $CourseProject->title),
      'description' => old('description', $CourseProject->description),
      'image' => old('image', $CourseProject->image),
      'status' => old('status', $CourseProject->status),
    ];
    return view('adminlte::course_project.form', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $CourseProject = CourseProject::find($request->id)->first();

    // image
    if($request->file('image')){
      $destinationPath = asset_path('uploads/course_projects/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath. "small_".$image);
      $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath. "medium_".$image);
      $request->file('image')->move($destinationPath, $image); // uploading file to given path

      $CourseProject->image = '/uploads/course_projects/'.$image;
    }
    // image

    $CourseProject->title = $request->title;
    $CourseProject->description = $request->description;
    $CourseProject->status = $request->status;
    $CourseProject->save();

    Session::flash('success', 'Update record success');
    return redirect('course/project');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $CourseProject = CourseProject::find($id)->delete();

    Session::flash('success', 'Delete record success');
    return redirect('course/project ');
  }
}
