<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Certificate;
use App\CeritificateAttribute;
use App\Course;
use Storage;
use File;
use Lang;

class CertificateController extends Controller
{
    public function index (Request $request) {

        $data = [
            'certificate' => Certificate::where(['status' => '1', 'created_by' => '1', 'id' => $request->get('id')])->with('certificate_attributes')->first()
        ];

        // dd($data);

        return view('admin.ceritificate.index', $data);
    }

    public function lists () {

        $data = [
            'ceritificates' => Certificate::latest()->get(),
            'courses' => Course::latest()->get(),
        ];

        return view('admin.ceritificate.lists', $data);
    }

    public function store (Request $request) {

        $Certificate = new Certificate;

        if($request->file('image')){
            // $destinationPath = 'uploads/certificates/'; // upload path
            // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            // $image = rand(11111111,999999999).'.'.$extension; // renameing image
            // $request->file('image')->move($destinationPath, $image); // uploading file to given path

            // upload
            $file = $request->file('image');
            $name_file = date("YmdHis") .'-'. $file->getClientOriginalName();
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/ceritificates/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/ceritificates/', $name_file);
            // upload
        
            $Certificate->image = $path_file;
        }
        $Certificate->status = '1';
        $Certificate->created_by = \Auth::user()->id;
        $Certificate->save();
        
        foreach($request->position as $index => $data) {
            $CeritificateAttribute = new CeritificateAttribute;
            $CeritificateAttribute->ceritificate_id = $Certificate->id;
            $CeritificateAttribute->position = $data;
            $CeritificateAttribute->x_coordinate = $request->x_coordinate[$index];
            $CeritificateAttribute->y_coordinate = $request->y_coordinate[$index];
            $CeritificateAttribute->save();
        }

        return redirect('/admin/ceritificate/lists')->with('success', Lang::get('back.certificate.alert_created'));
    }

    public function update (Request $request) {

        $Certificate = Certificate::where('id', $request->id)->first();

        if($request->file('image')){
            $destinationPath = 'uploads/certificates/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $image = rand(11111111,999999999).'.'.$extension; // renameing image
            $request->file('image')->move($destinationPath, $image); // uploading file to given path
        
            $Certificate->image = '/uploads/certificates/'.$image;
        }
        $Certificate->save();

        // delete old attributes
        CeritificateAttribute::where('ceritificate_id', $request->id)->delete();

        // insert new attributes
        foreach($request->position as $index => $data) {
            $CeritificateAttribute = new CeritificateAttribute;
            $CeritificateAttribute->ceritificate_id = $request->id;
            $CeritificateAttribute->position = $data;
            $CeritificateAttribute->x_coordinate = $request->x_coordinate[$index];
            $CeritificateAttribute->y_coordinate = $request->y_coordinate[$index];
            $CeritificateAttribute->save();
        }

        return redirect('/admin/ceritificate/lists')->with('success', Lang::get('back.certificate.alert_updated'));
    }

    public function addCourses(Request $request) {
        $course_id = isset($request->course_id) ? implode(',', $request->course_id) : null;
        $Certificate = Certificate::where('id', $request->id)->first();
        $Certificate->course_id = $course_id;
        $Certificate->save();

        return redirect()->back()->with('success', Lang::get('back.certificate.alert_add_class'));
    }

    public function setCeritifcated(Request $request) {
        $Course = Course::where('id', $request->id)->first();
        $Course->get_ceritificate = $request->get_ceritificate;
        $Course->save();

        return redirect()->back()->with('success', Lang::get('back.certificate.alert_setting'));
    }
}
