<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SettingAuthKey;
use App\Setting;
use Lang;

class SettingAuthKeyController extends Controller
{
    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            
            return true;
        } else {
            return false;
        }
    }

    public function index() {
      $SettingAuthKey = SettingAuthKey::first();
      return view('admin.setting.auth_key', compact('SettingAuthKey'));
    }

    public function update(Request $request) {
      if ($request->action == 'store') {
        $SettingAuthKey = new SettingAuthKey;
        $SettingAuthKey->google_client_id = $request->google_client_id;
        $SettingAuthKey->google_client_secret = $request->google_client_secret;
        $SettingAuthKey->facebook_client_id = $request->facebook_client_id;
        $SettingAuthKey->facebook_client_secret = $request->facebook_client_secret;
        $SettingAuthKey->save();

        // CHANGE ENV FILE
        $env_update = $this->changeEnv([
          'GOOGLE_CLIENT_ID'=>$request->google_client_id,
          'GOOGLE_CLIENT_SECRET'=> $request->google_client_secret,
          'FACEBOOK_ID'=>$request->facebook_client_id,
          'FACEBOOK_SECRET'=> $request->facebook_client_secret
        ]);
        // CHANGE ENV FILE

      } else {
        $SettingAuthKey = SettingAuthKey::first();
        $SettingAuthKey->google_client_id = $request->google_client_id;
        $SettingAuthKey->google_client_secret = $request->google_client_secret;
        $SettingAuthKey->facebook_client_id = $request->facebook_client_id;
        $SettingAuthKey->facebook_client_secret = $request->facebook_client_secret;
        $SettingAuthKey->save();

        // CHANGE ENV FILE
        $env_update = $this->changeEnv([
          'GOOGLE_CLIENT_ID'=>$request->google_client_id,
          'GOOGLE_CLIENT_SECRET'=> $request->google_client_secret,
          'FACEBOOK_ID'=>$request->facebook_client_id,
          'FACEBOOK_SECRET'=> $request->facebook_client_secret
        ]);
        // CHANGE ENV FILE
        
      }
      return redirect('admin/setting/auth')->with('success', Lang::get('back.auth_setting.setting_success'));
    }

    public function mail_setting_update(Request $request) {

      $configs = [
        'MAIL_DRIVER'=>$request->MAIL_DRIVER,
        'MAIL_HOST'=> $request->MAIL_HOST,
        'MAIL_PORT'=>$request->MAIL_PORT,
        'MAIL_USERNAME'=> $request->MAIL_USERNAME,
        'MAIL_PASSWORD'=> $request->MAIL_PASSWORD,
        'MAIL_ENCRYPTION'=> $request->MAIL_ENCRYPTION,
        'MAIL_SENDER'=> $request->MAIL_SENDER
      ];

      // CHANGE ENV FILE
      $env_update = $this->changeEnv($configs);
      // CHANGE ENV FILE

      // CHANGE SETTINGS
      $setting = Setting::where('name', 'MAIL_DRIVER')->first();
      $setting->value = $request->MAIL_DRIVER;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_HOST')->first();
      $setting->value = $request->MAIL_HOST;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_PORT')->first();
      $setting->value = $request->MAIL_PORT;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_USERNAME')->first();
      $setting->value = $request->MAIL_USERNAME;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_PASSWORD')->first();
      $setting->value = $request->MAIL_PASSWORD;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_ENCRYPTION')->first();
      $setting->value = $request->MAIL_ENCRYPTION;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_SENDER')->first();
      $setting->value = $request->MAIL_SENDER;
      $setting->save();
      $setting = Setting::where('name', 'MAIL_FROM')->first();
      $setting->value = $request->MAIL_FROM;
      $setting->save();
      // CHANGE SETTINGS
      
      return redirect('admin/setting/auth')->with('success', Lang::get('back.auth_setting.setting_success'));
    }
}
