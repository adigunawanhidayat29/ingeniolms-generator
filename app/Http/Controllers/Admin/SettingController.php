<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Theme;
use App\VconSetting;
use App\SettingAuthKey;
use Storage;
use File;
use Lang;

class SettingController extends Controller
{
    
    public function index(){
        $data = [
            'settings' => Setting::get(),
            'themes' => Theme::get(),
            'vcon_setting' => VconSetting::get(),
            'setting_auth_keys' => SettingAuthKey::first(),
        ];

        return view('admin.setting.index', $data);
    }

    public function action(Request $request){

        if($request->file('value')){
            // file
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('value')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('value')->move($destinationPath, $file); // uploading file to given path
            // file

            // upload
            $file = $request->file('value');
            $extension = $request->file('value')->getClientOriginalExtension();
            $name_file = str_slug($request->name) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/settings/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/settings/', $name_file);
            // upload
        }

        if($request->action == 'store'){
            if (is_array($request->value)) {
                $request->value = implode(',', $request->value);
            }
            $Setting = new Setting;
            $Setting->name = $request->name;
            $Setting->type = isset($request->type) ? $request->type : 'text';
            $Setting->value = $request->type == 'file' ? $path_file : $request->value;
            $Setting->save();
        }else{
            if (is_array($request->value)) {
                $request->value = implode(',', $request->value);
            }
            $Setting = Setting::find($request->id);
            $Setting->name = $request->name;
            $Setting->type = $request->type;
            $Setting->value = $request->type == 'file' ? $path_file : $request->value;
            $Setting->save();
        }
        
        return back();
    }

    public function registration(Request $request){
        // save registration
        if (is_array($request->registration)) {
            $request->registration = implode(',', $request->registration);
        }
        if(isset(Setting('registration')->value)) {
            $Setting = Setting::find(Setting('registration')->id);
            $Setting->name = 'registration';
            $Setting->type = 'text';
            $Setting->value = $request->registration;
            $Setting->save();
        } else {
            $Setting = new Setting;
            $Setting->name = 'registration';
            $Setting->type = 'text';
            $Setting->value = $request->registration;
            $Setting->save();
        }

        // save login with
        if (is_array($request->login_with)) {
            $request->login_with = implode(',', $request->login_with);
        }

        if(isset(Setting('login_with')->value)) {
            $Setting = Setting::find(Setting('login_with')->id);
            $Setting->name = 'login_with';
            $Setting->type = 'text';
            $Setting->value = $request->login_with;
            $Setting->save();
        } else {
            $Setting = new Setting;
            $Setting->name = 'login_with';
            $Setting->type = 'text';
            $Setting->value = $request->login_with;
            $Setting->save();
        }
        
        return back()->with('message', 'Konfigurasi daftar berhasil');
    }

    public function apis() {
        return view('admin.setting.apis');
    }

    public function site() {
        return view('admin.setting.configuration.site');
    }

    public function siteSubmit(Request $request) {

        if($request->file('icon')){
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('icon')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('icon')->move($destinationPath, $file); // uploading file to given path

            // upload
            $file = $request->file('icon');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->name) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/settings/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/settings/', $name_file);
            // upload

            $Setting = Setting::find(Setting('icon')->id);
            $Setting->value = $path_file;
            $Setting->save();
        }

        if($request->file('favicon')){
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('favicon')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('favicon')->move($destinationPath, $file); // uploading file to given path

            // upload
            $file = $request->file('favicon');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->name) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/settings/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/settings/', $name_file);
            // upload

            $Setting = Setting::find(Setting('favicon')->id);
            $Setting->value = $path_file;
            $Setting->save();
        }

        if ($request->title) {
            $Setting = Setting::find(Setting('title')->id);
            $Setting->value = $request->title;
            $Setting->save();
        }

        if ($request->tagline) {
            $Setting = Setting::find(Setting('tagline')->id);
            $Setting->value = $request->tagline;
            $Setting->save();
        }
        
        if ($request->color_primary) {
            $Setting = Setting::find(Setting('color_primary')->id);
            $Setting->value = $request->color_primary;
            $Setting->save();
        }

        if ($request->color_secondary) {
            $Setting = Setting::find(Setting('color_secondary')->id);
            $Setting->value = $request->color_secondary;
            $Setting->save();
        }

        if ($request->copyright) {
            $Setting = Setting::find(Setting('copyright')->id);
            $Setting->value = $request->copyright;
            $Setting->save();
        }

        if ($request->font_color_primary) {
            $Setting = Setting::find(Setting('font_color_primary')->id);
            $Setting->value = $request->font_color_primary;
            $Setting->save();
        }

        if ($request->font_color_secondary) {
            $Setting = Setting::find(Setting('font_color_secondary')->id);
            $Setting->value = $request->font_color_secondary;
            $Setting->save();
        }

        if ($request->navbar_background_color) {
            $Setting = Setting::find(Setting('navbar_background_color')->id);
            $Setting->value = $request->navbar_background_color == 'lainnya' ? $request->navbar_background_color_another : $request->navbar_background_color;
            $Setting->save();
        }

        if ($request->navbar_background_hover) {
            $Setting = Setting::find(Setting('navbar_background_hover')->id);
            $Setting->value = $request->navbar_background_hover == 'lainnya' ? $request->navbar_background_hover_another : $request->navbar_background_hover;
            $Setting->save();
        }

        if ($request->navbar_font_color) {
            $Setting = Setting::find(Setting('navbar_font_color')->id);
            $Setting->value = $request->navbar_font_color == 'lainnya' ? $request->navbar_font_color_another : $request->navbar_font_color;
            $Setting->save();
        }

        if ($request->navbar_font_color_hover) {
            $Setting = Setting::find(Setting('navbar_font_color_hover')->id);
            $Setting->value = $request->navbar_font_color_hover == 'lainnya' ? $request->navbar_font_color_hover_another : $request->navbar_font_color_hover;
            $Setting->save();
        }

        if ($request->tabs_font_color) {
            $Setting = Setting::find(Setting('tabs_font_color')->id);
            $Setting->value = $request->tabs_font_color == 'lainnya' ? $request->tabs_font_color_another : $request->tabs_font_color;
            $Setting->save();
        }

        if ($request->banner) {
            $Setting = Setting::find(Setting('banner')->id);
            $Setting->value = $request->banner;
            $Setting->save();
        }

        if ($request->banner_style) {
            $Setting = Setting::find(Setting('banner_style')->id);
            $Setting->value = $request->banner_style;
            $Setting->save();
        }

        if ($request->banner_font_color) {
            $Setting = Setting::find(Setting('banner_font_color')->id);
            $Setting->value = $request->banner_font_color == 'lainnya' ? $request->banner_font_color_another : $request->banner_font_color;
            $Setting->save();
        }

        if ($request->header_gradient) {
            $Setting = Setting::find(Setting('header_gradient')->id);
            $Setting->value = $request->header_gradient;
            $Setting->save();
        }

        if ($request->custom_gradient_primary) {
            $Setting = Setting::find(Setting('custom_gradient_primary')->id);
            $Setting->value = $request->custom_gradient_primary;
            $Setting->save();
        }

        if ($request->custom_gradient_secondary) {
            $Setting = Setting::find(Setting('custom_gradient_secondary')->id);
            $Setting->value = $request->custom_gradient_secondary;
            $Setting->save();
        }

        if ($request->custom_gradient_rotate || $request->custom_gradient_rotate === 0) {
            $Setting = Setting::find(Setting('custom_gradient_rotate')->id);
            $Setting->value = $request->custom_gradient_rotate;
            $Setting->save();
        }

        if ($request->footer_address) {
            $Setting = Setting::find(Setting('footer_address')->id);
            $Setting->value = $request->footer_address;
            $Setting->save();
        }

        if ($request->footer_email) {
            $Setting = Setting::find(Setting('footer_email')->id);
            $Setting->value = $request->footer_email;
            $Setting->save();
        }

        if ($request->footer_phone) {
            $Setting = Setting::find(Setting('footer_phone')->id);
            $Setting->value = $request->footer_phone;
            $Setting->save();
        }

        if ($request->footer_instagram) {
            $Setting = Setting::find(Setting('footer_instagram')->id);
            $Setting->value = $request->footer_instagram;
            $Setting->save();
        }

        if ($request->footer_facebook) {
            $Setting = Setting::find(Setting('footer_facebook')->id);
            $Setting->value = $request->footer_facebook;
            $Setting->save();
        }

        if ($request->footer_twitter) {
            $Setting = Setting::find(Setting('footer_twitter')->id);
            $Setting->value = $request->footer_twitter;
            $Setting->save();
        }

        if ($request->footer_youtube) {
            $Setting = Setting::find(Setting('footer_youtube')->id);
            $Setting->value = $request->footer_youtube;
            $Setting->save();
        }

        if ($request->footer_linkedin) {
            $Setting = Setting::find(Setting('footer_linkedin')->id);
            $Setting->value = $request->footer_linkedin;
            $Setting->save();
        }

        if ($request->footer_app_link) {
            $Setting = Setting::find(Setting('footer_app_link')->id);
            $Setting->value = $request->footer_app_link;
            $Setting->save();
        }

        if ($request->app_store_link) {
            $Setting = Setting::find(Setting('app_store_link')->id);
            $Setting->value = $request->app_store_link;
            $Setting->save();
        }

        if ($request->google_play_link) {
            $Setting = Setting::find(Setting('google_play_link')->id);
            $Setting->value = $request->google_play_link;
            $Setting->save();
        }

        if($request->file('features_thumbnail')){
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('features_thumbnail')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('features_thumbnail')->move($destinationPath, $file); // uploading file to given path

            // upload
            $file = $request->file('features_thumbnail');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->name) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/settings/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/settings/', $name_file);
            // upload

            $Setting = Setting::find(Setting('features_thumbnail')->id);
            $Setting->value = $path_file;
            $Setting->save();
        }

        if ($request->tnc_title) {
            $Setting = Setting::find(Setting('tnc_title')->id);
            $Setting->value = $request->tnc_title;
            $Setting->save();
        }

        if ($request->tnc_value) {
            $Setting = Setting::find(Setting('tnc_value')->id);
            $Setting->value = $request->tnc_value;
            $Setting->save();
        }

        if ($request->privacy_policy_title) {
            $Setting = Setting::find(Setting('privacy_policy_title')->id);
            $Setting->value = $request->privacy_policy_title;
            $Setting->save();
        }

        if ($request->privacy_policy_value) {
            $Setting = Setting::find(Setting('privacy_policy_value')->id);
            $Setting->value = $request->privacy_policy_value;
            $Setting->save();
        }

        if ($request->contact_us_title) {
            $Setting = Setting::find(Setting('contact_us_title')->id);
            $Setting->value = $request->contact_us_title;
            $Setting->save();
        }

        if ($request->contact_us_value) {
            $Setting = Setting::find(Setting('contact_us_value')->id);
            $Setting->value = $request->contact_us_value;
            $Setting->save();
        }

        if ($request->catalog_column) {
            $Setting = Setting::find(Setting('catalog_column')->id);
            $Setting->value = $request->catalog_column;
            $Setting->save();
        }

        if ($request->max_catalog_column) {
            $Setting = Setting::find(Setting('max_catalog_column')->id);
            $Setting->value = $request->max_catalog_column;
            $Setting->save();
        }

        if ($request->feature_background_color) {
            $Setting = Setting::find(Setting('feature_background_color')->id);
            $Setting->value = $request->feature_background_color == 'lainnya' ? $request->feature_background_color_another : $request->feature_background_color;
            $Setting->save();
        }

        if ($request->feature_text_color) {
            $Setting = Setting::find(Setting('feature_text_color')->id);
            $Setting->value = $request->feature_text_color;
            $Setting->save();
        }

        if ($request->footer_background_color) {
            $Setting = Setting::find(Setting('footer_background_color')->id);
            $Setting->value = $request->footer_background_color;
            $Setting->save();
        }

        if ($request->footer_text_color) {
            $Setting = Setting::find(Setting('footer_text_color')->id);
            $Setting->value = $request->footer_text_color;
            $Setting->save();
        }

        return redirect()->back()->with('success', Lang::get('back.site_setting.setting_success'));
    }

    public function auth() {
        $SettingAuthKey = SettingAuthKey::first();
        return view('admin.setting.configuration.authentication', compact('SettingAuthKey'));
    }

    public function auth_post(Request $request) {
        if ($request->registration) {
            $Setting = Setting::find(Setting('registration')->id);
            $Setting->value = implode(',', $request->registration);
            $Setting->save();
        }

        if ($request->login_with) {
            $Setting = Setting::find(Setting('login_with')->id);
            $Setting->value = implode(',', $request->login_with);
            $Setting->save();
        }

        // dd($request->user_verify);
        $Setting = Setting::find(Setting('user_verify')->id);
        $Setting->value = $request->user_verify == 'on' ? '1' : '0';
        $Setting->save();

        return redirect()->back()->with('success', Lang::get('back.auth_setting.setting_success'));
    }

    public function vcon() {
        $data = [
            'vcon_setting' => VconSetting::get(),
        ];
        return view('admin.setting.configuration.vcon', $data);
    }

    public function enrollment() {
        return view('admin.setting.configuration.enrolment');
    }

    public function enrollment_post(Request $request) {
        if ($request->enrollment) {
            $Setting = Setting::find(Setting('enrollment')->id);
            $Setting->value = implode(',', $request->enrollment);
            $Setting->save();
        }
        return redirect()->back()->with('success', Lang::get('back.enrol_setting.setting_success'));
    }

}
