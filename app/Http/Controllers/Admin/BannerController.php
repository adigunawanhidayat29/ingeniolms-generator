<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use Illuminate\Http\Request;

use Lang;
use Storage;
use File;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $banner = Banner::where('name', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $banner = Banner::latest()->paginate($perPage);
        }

        return view('admin.banner.index', compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['status'] = '1';

        if($request->file('image')){
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('image')->move($destinationPath, $file); // uploading file to given path
            // $requestData['image'] = '/uploads/settings/files/'.$file;

            // upload
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->title) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/banners/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/banners/', $name_file);
            $requestData['image'] = $path_file;
            // upload
        }
        
        Banner::create($requestData);

        return redirect('admin/banner')->with('success', Lang::get('back.banner_default.alert_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $banner = Banner::findOrFail($id);

        return view('admin.banner.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);

        return view('admin.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        if($request->file('image')){
            // $destinationPath = 'uploads/settings/files/'; // upload path
            // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            // $file = str_slug($request->name).time().'.'.$extension; // renameing image
            // $request->file('image')->move($destinationPath, $file); // uploading file to given path
            // $requestData['image'] = '/uploads/settings/files/'.$file;

            // upload
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $name_file = str_slug($request->title) .'-'. str_slug($file->getClientOriginalName()) . '.' . $extension ;
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/banners/' . $name_file, fopen($file, 'r+'), 'public');
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/banners/', $name_file);
            $requestData['image'] = $path_file;
            // upload
        }
        
        $banner = Banner::findOrFail($id);
        
        $banner->update($requestData);

        return redirect('admin/banner')->with('success', Lang::get('back.banner_default.alert_save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Banner::destroy($id);

        return redirect('admin/banner')->with('success', Lang::get('back.banner_default.alert_delete'));
    }
}
