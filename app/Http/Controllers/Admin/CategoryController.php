<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Category;
use Lang;
use Storage;

class CategoryController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = Category::select('id','title', 'icon')->get();

      return Datatables::of($data)      
      ->addColumn('action', function ($data)
      {
        return '
        <form name="delete" method="POST" action="'. url( \Request::route()->getPrefix() . '/categories/delete/'.$data->id) .'">
        <a href="'. url(\Request::route()->getPrefix() .'/categories/update/'.$data->id) .'" class="btn btn-warning btn-sm">
          <i class="material-icons">edit</i><span>'. Lang::get('back.category.update_button') .'</span>
        </a>

        '. csrf_field() .'
        <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
          <i class="material-icons">delete</i><span>'. Lang::get('back.category.delete_button') .'</span>
        </button>

        </form>

        ';
      })->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('admin.category.list');
  }

  public function create(){
    $data = [
      'action' => \Request::route()->getPrefix() .'/categories/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'description' => old('description'),
      'icon' => old('icon'),
      'parent_category' => old('parent_category'),
      'categories' => Category::select('id','title')->get(),
    ];
    return view('admin.category.form', $data);
  }

  public function create_action(Request $request){

    $Category = new Category;
    $Category->title = $request->title;
    $Category->slug = str_slug($request->title);
    $Category->description = $request->description;
    $Category->parent_category = $request->parent_category;

    // image
    if($request->file('icon')){
      $file = $request->file('icon');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'categories/' . $name_file, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'categories/', $name_file);
      $Category->icon = $path_file;
    }
    // image

    if($Category->save()){
      \Session::flash('success', Lang::get('back.category.alert_created'));
    }else{
      \Session::flash('error', Lang::get('back.category.alert_created_failed'));
    }
    return redirect(\Request::route()->getPrefix() . '/categories');

  }

  public function update($id){
    $Category = Category::where('id', $id)->first();
    $data = [
      'action' => \Request::route()->getPrefix() .'/categories/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Category->id),
      'title' => old('title', $Category->title),
      'description' => old('description', $Category->description),
      'icon' => old('icon', $Category->icon),
      'parent_category' => old('parent_category', $Category->parent_category),
      'categories' => Category::select('id','title')->get(),
    ];
    return view('admin.category.form', $data);
  }

  public function update_action(Request $request){
    $Category = Category::where('id', $request->id)->first();

    $Category->title = $request->title;
    $Category->slug = str_slug($request->title);
    $Category->description = $request->description;
    $Category->parent_category = $request->parent_category;

    // if($request->file('icon')){
    //   // image
    //   $destinationPath = asset_path('uploads/categories/'); // upload path
    //   $extension = $request->file('icon')->getClientOriginalExtension(); // getting image extension
    //   $icon = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
    //   $request->file('icon')->move($destinationPath, $icon); // uploading file to given path
    //   // image

    //   $Category->icon = '/uploads/categories/'.$icon;
    // }

    // image
    if($request->file('icon')){
      $file = $request->file('icon');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'categories/' . $name_file, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'categories/', $name_file);
      $Category->icon = $path_file;
    }
    // image

    if($Category->save()){
      \Session::flash('success', Lang::get('back.category.alert_updated'));
    }else{
      \Session::flash('error', Lang::get('back.category.alert_updated_failed'));
    }

    return redirect(\Request::route()->getPrefix() . '/categories');
  }

  public function delete($id){
    $Category = Category::where('id', $id);
    if($Category){
      $Category->delete();
      \Session::flash('success', Lang::get('back.category.alert_deleted'));
    }else{
      \Session::flash('error', Lang::get('back.category.alert_deleted_failed'));
    }

    return redirect(\Request::route()->getPrefix() . '/categories');
  }

}
