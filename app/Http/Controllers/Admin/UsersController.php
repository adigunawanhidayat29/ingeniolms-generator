<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\User;
use App\UserGroup;
use App\Instructor;
use Validator;
use App\Imports\UsersImport;
use App\Imports\UsersImportAll;
use Maatwebsite\Excel\Facades\Excel;
use Lang;

class UsersController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if ($request->ajax()) {

      $data = User::select('id', 'name', 'email', 'is_active', 'created_at')->get();

      return DataTables::of($data)
        ->editColumn('is_active', function ($data) {
          return $data->is_active == '1' ? Lang::get('back.user_all.enabled_status') : Lang::get('back.user_all.disabled_status');
        })
        ->addColumn('action', function ($data) {
          if ($data->is_active == '1') {
            // $link_is_active = '<li><a href=" '.\Request::route()->getPrefix().' /users/non-activated/'.$data->id.'">Non Activated</a></li>';
            $link_is_active = '<a href=" ' . \Request::route()->getPrefix() . '/users/non-activated/' . $data->id . '" class="btn btn-danger btn-sm">
            <i class="material-icons">block</i><span>' . Lang::get('back.user_all.option_block_button') . '</span>
          </a>';
          } else {
            // $link_is_active = '<li><a href=" '.\Request::route()->getPrefix().' /users/activated/'.$data->id.'">Activated</a></li>';
            $link_is_active = '<a href=" ' . \Request::route()->getPrefix() . '/users/activated/' . $data->id . '" class="btn btn-success btn-sm">
            <i class="material-icons">check</i><span>' . Lang::get('back.user_all.option_activate_button') . '</span>
          </a>';
          }

          return '
        <a href="' . url(\Request::route()->getPrefix() . '/users/password/update/' . $data->id) . '" class="btn btn-warning btn-sm">
          <i class="material-icons">lock</i><span>' . Lang::get('back.user_all.option_password_button') . '</span>
        </a>
        <a href="' . url(\Request::route()->getPrefix() . '/users/update/' . $data->id) . '" class="btn btn-warning btn-sm">
          <i class="material-icons">edit</i><span>' . Lang::get('back.user_all.option_update_button') . '</span>
        </a>
        ' . $link_is_active . '
        ' . csrf_field() . '
        <input type="hidden" name="_method" value="DELETE">
        <a href="' . url(\Request::route()->getPrefix() . '/users/delete/' . $data->id) . '" class="btn btn-danger btn-sm" onclick="return confirm(' . "'" . Lang::get('back.alerts.confirm_delete') . "'" . ');">
          <i class="material-icons">delete</i><span>' . Lang::get('back.user_all.option_delete_button') . '</span>
        </a>';
        })->make(true);
    } else {
      exit("Not an AJAX request");
    }
  }

  public function index()
  {
    return view('admin.users.list');
  }

  public function create()
  {
    $data = [
      'action' => \Request::route()->getPrefix() . '/users/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'name' => old('name'),
      'username' => old('username'),
      'email' => old('email'),
      'password' => old('password'),
      'id_level_user' => old('id_level_user'),
      'user_levels' => DB::table('user_levels')->get(),
    ];
    return view('admin.users.form', $data);
  }

  public function create_action(Request $request)
  {

    $validator = $this->rules($request);
    if ($validator->fails()) {
      \Session::flash('error', $validator->errors());
      return $this->create();
    }

    $language = \App\Models\Language::where('status', '1')->first();

    $User = new User;
    $User->name = $request->name;
    $User->username = $request->username;
    $User->email = $request->email;
    $User->username = $request->username;
    $User->is_active = '1';
    $User->slug = str_slug($request->name);
    $User->password = bcrypt($request->password);
    $User->language = $language->alias;

    if ($User->save()) {

      // inser user group
      $user_levels = $request->id_level_user;
      // dd($user_levels);
      foreach ($user_levels as $index => $value) {
        $UserGroup = new UserGroup;
        $UserGroup->level_id = $value;
        $UserGroup->user_id = $User->id;
        $UserGroup->save();

        // if level instructur -> insert instructor
        if ($value == 2) {
          $Instructor = new Instructor;
          $Instructor->user_id = $User->id;
          $Instructor->status = '1';
          $Instructor->save();
        }
        // if level instructur -> insert instructor

      }
      // inser user group

      \Session::flash('success', Lang::get('back.user_all.alert_created'));
    } else {
      \Session::flash('error', Lang::get('back.user_all.alert_created_failed'));
    }
    return redirect('admin/users');
  }

  public function update($id)
  {
    $User = User::where('id', $id)->first();
    $users_groups_data = [];

    $user_groups = DB::table('users_groups')->where('user_id', $User->id)->get();
    foreach ($user_groups as $user_group) {
      array_push($users_groups_data, $user_group->level_id);
    }

    // dd($users_groups_data);

    $data = [
      'action' => \Request::route()->getPrefix() . '/users/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $User->id),
      'name' => old('name', $User->name),
      'username' => old('username', $User->username),
      'email' => old('email', $User->email),
      'id_level_user' => old('email', $User->id_level_user),
      'user_levels' => DB::table('user_levels')->get(),
      'user_groups' => $users_groups_data,
    ];
    return view('admin.users.form', $data);
  }

  public function update_action(Request $request)
  {
    $User = User::where('id', $request->id)->first();

    $User->name = $request->name;
    $User->username = $request->username;
    $User->email = $request->email;
    $User->slug = str_slug($request->name);

    // inser user group
    UserGroup::where('user_id', $request->id)->delete();
    $user_levels = $request->id_level_user;
    foreach ($user_levels as $index => $value) {
      $UserGroup = new UserGroup;
      $UserGroup->level_id = $value;
      $UserGroup->user_id = $User->id;
      $UserGroup->save();

      // if level instructur -> insert instructor
      if ($value == 2) {
        if (!Instructor::where('user_id', $request->id)->first()) {
          $Instructor = new Instructor;
          $Instructor->user_id = $User->id;
          $Instructor->status = '1';
          $Instructor->save();
        }
      }
      // if level instructur -> insert instructor

    }
    // inser user group

    if ($User->save()) {
      \Session::flash('success', Lang::get('back.user_all.alert_updated'));
    } else {
      \Session::flash('error', Lang::get('back.user_all.alert_updated_failed'));
    }

    return redirect('admin/users');
  }

  public function delete($id)
  {
    $User = User::where('id', $id);
    if ($User) {
      $User->delete();
      \Session::flash('success', Lang::get('back.user_all.alert_deleted'));
    } else {
      \Session::flash('error', Lang::get('back.user_all.alert_deleted_failed'));
    }

    return redirect('admin/users');
  }

  public function rules($request)
  {
    return $validator = Validator::make($request->all(), [
      'name' => 'required',
      'email'    => 'required|email|max:255|unique:users',
    ]);
  }

  public function password_update($id)
  {
    return view('admin.users/change-password');
  }

  public function password_update_action($id, Request $request)
  {
    $this->validate($request, [
      'password' => 'required|confirmed',
      'password_confirmation' => 'required',
    ]);

    $User = User::where('id', $id)->first();
    $User->password = bcrypt($request->password);
    $User->save();

    return redirect('admin/users')->with('success', Lang::get('back.user_all.alert_password_updated'));
  }

  public function activated($id)
  {
    $User = User::where('id', $id)->first();
    $User->is_active = '1';
    $User->save();

    \Session::flash('success', Lang::get('back.user_all.alert_activated'));
    return back();
  }
  public function non_activated($id)
  {
    $User = User::where('id', $id)->first();
    $User->is_active = '0';
    $User->save();

    \Session::flash('success', Lang::get('back.user_all.alert_non_activated'));
    return back();
  }

  public function import()
  {
    return view('admin.users.import');
  }

  public function import_action(Request $request)
  {
    $destinationPath = 'imports/users/'; // upload path
    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
    $file = str_slug($request->file('file')->getClientOriginalName()) . time() . '.' . $extension; // renameing image
    $request->file('file')->move($destinationPath, $file); // uploading file to given path

    Excel::import(new UsersImport, $destinationPath . $file);

    return redirect(\Request::route()->getPrefix() . '/users')->with('success', 'All good!');
  }

  public function importAll()
  {
    return view('admin.users.import_all');
  }

  public function import_action_all(Request $request)
  {

    $destinationPath = 'imports/users/all/'; // upload path
    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
    $file = str_slug($request->file('file')->getClientOriginalName()) . time() . '.' . $extension; // renameing image
    $request->file('file')->move($destinationPath, $file); // uploading file to given path

    Excel::import(new UsersImportAll, $destinationPath . $file);

    return redirect()->back()->with('success', 'All good!');
  }
}
