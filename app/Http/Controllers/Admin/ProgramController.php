<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Program;
use App\ProgramCourse;
use App\ProgramUser;
use App\Course;
use App\CourseUser;
use DB;
use Storage;

class ProgramController extends Controller
{
  public function index()
  {
    $Program = Program::paginate(10);

    $data = [
      'Programs' => $Program,
    ];

    return view('admin.program.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $data = [
      'action' => 'admin/program/store',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'price' => old('price'),
      'password' => old('password'),
      'description' => old('description'),
      'image' => old('image'),
      'introduction' => old('introduction'),
      'introduction_description' => old('introduction_description'),
      'benefits' => old('benefits'),
      'developer_team' => old('developer_team'),
      'precondition' => old('precondition'),
    ];

    return view('admin.program.form', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // $folder_path = "1ZursTRjrymWizqn_7-dyEtGb6zndlUZs"; // folder path program parenting

    $Program = new Program;

    if($request->file('image')){
      // image
      // $destinationPath = asset_path('uploads/programs/'); // upload path
      // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      // $name_file = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      // $request->file('image')->move($destinationPath, $name_file); // uploading file to given path
      // // image
      // $image = '/uploads/programs/'.$name_file;

      $file = $request->file('image');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'programs/' . $name_file, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'programs/', $name_file);
      $Program->image = $path_file;

    }else{
      $image = '';
    }

    // introduction
    // if($request->file('introduction')){
    //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
    //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
    //   Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
    //   $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
    // }else{
    //   $introduction = '';
    // }
    // introduction

    $Program->title = $request->title;
    $Program->password = $request->password;
    // $Program->image = $image;
    $Program->introduction = $introduction;
    $Program->price = $request->price;
    $Program->slug = str_slug($request->title);
    $Program->description = $request->description;
    $Program->introduction_description = $request->introduction_description;
    $Program->benefits = $request->benefits;
    $Program->developer_team = $request->developer_team;
    $Program->precondition = $request->precondition;
    $Program->author = \Auth::user()->id;

    if($Program->save()){
      \Session::flash('success', 'Create record success');
    }else{
      \Session::flash('error', 'Create record failed');
    }
    return redirect('admin/program');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $Program = Program::where('id', $id)->first();
    $data = ['Program' => $Program];
    return view('admin.program.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $Program = Program::where('id', $id)->first();
    $data = [
      'action' => 'admin/program/update',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Program->id),
      'title' => old('title', $Program->title),
      'image' => old('image', $Program->image),
      'introduction' => old('introduction', $Program->introduction),
      'price' => old('price', $Program->price),
      'password' => old('password', $Program->password),
      'description' => old('description', $Program->description),
      'introduction_description' => old('introduction_description', $Program->introduction_description),
      'benefits' => old('benefits', $Program->benefits),
      'developer_team' => old('developer_team', $Program->developer_team),
      'precondition' => old('precondition', $Program->precondition),
    ];

    return view('admin.program.form', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $Program = Program::where('id', $request->id)->first();
    $Program->title = $request->title;
    $Program->price = $request->price;
    $Program->password = $request->password;
    $Program->slug = str_slug($request->title);
    $Program->description = $request->description;
    $Program->introduction_description = $request->introduction_description;
    $Program->benefits = $request->benefits;
    $Program->developer_team = $request->developer_team;
    $Program->precondition = $request->precondition;

    $folder_path = "1ZursTRjrymWizqn_7-dyEtGb6zndlUZs"; // folder path program parenting

    // image
    if($request->file('image')){
      // $destinationPath = asset_path('uploads/programs/'); // upload path
      // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      // $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      // $request->file('image')->move($destinationPath, $image); // uploading file to given path
      // $Program->image = '/uploads/programs/'.$image;

      $file = $request->file('image');
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'programs/' . $name_file, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'programs/', $name_file);
      $Program->image = $path_file;
    }
    // image

    // introduction
    // if($request->file('introduction')){
    //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
    //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
    //   Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
    //   $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
    //   $Program->introduction = $introduction;
    // }
    // introduction

    if($Program->save()){
      \Session::flash('success', 'Update record success');
    }else{
      \Session::flash('error', 'Update record failed');
    }
    return redirect('admin/program');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $Program = Program::where('id', $id);
    if($Program){
      $Program->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('admin/program');
  }

  public function module_add($id){
    $ProgramCourses = ProgramCourse::where('program_id', $id)->get();
    $data_course_id = [];
    foreach($ProgramCourses as $ProgramCourse){
      array_push($data_course_id, $ProgramCourse->course_id);
    }
    // dd($data_course_id);
    $data = [
      'courses' => Course::whereNotIn('id', $data_course_id)->get(),
      'ProgramCourses' => Course::whereIn('id', $data_course_id)->get(),
    ];

    return view('admin.program.module_form', $data);
  }

  public function module_store(Request $request, $id){

    // insert Program course
    foreach ($request->course_id as $key => $value) {
      $ProgramCourse = new ProgramCourse;
      $ProgramCourse->program_id = $id;
      $ProgramCourse->course_id = $value;
      $ProgramCourse->save();

      // auto enrolled Program user
      $ProgramUsers = ProgramUser::where('program_id', $id)->get();
      foreach($ProgramUsers as $ProgramUser){
        $CourseUser = new CourseUser;
        $CourseUser->course_id = $value;
        $CourseUser->user_id = $ProgramUser->user_id;
        $CourseUser->save();
      }
      // auto enrolled Program user

    }
    // insert Program course

    \Session::flash('success', 'Save record success');
    return redirect()->back();
  }

  public function module_delete($id){
    $ProgramCourse = ProgramCourse::where('course_id', $id)->delete();

    \Session::flash('success', 'Delete module success');
    return redirect()->back();
  }
}
