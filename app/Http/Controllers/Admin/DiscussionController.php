<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\Discussion;

class DiscussionController extends Controller
{
  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = Discussion::select('discussions.id as id','courses.title as course','discussions.body as body','discussions.created_at as created_at','discussions.status as status','users.name as user')
      ->join('users','users.id','=','discussions.user_id')
      ->join('courses','courses.id','=','discussions.course_id')
      ->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        $publish_button = $data->status == '0' ? '<a class="btn btn-success btn-sm" href=/discussion/publish/'.$data->id.' onclick="return confirm('."'".'Are You Sure?'."'".')">Publish</a>' : '';
        $block_button = $data->status == '1' ? '<a class="btn btn-warning btn-sm" href=/discussion/block/'.$data->id.' onclick="return confirm('."'".'Are You Sure?'."'".')">Block</a>' : '';
        return '
          <form name="delete" method="POST" action="'. url('discussion/delete/'.$data->id) .'">
          '. csrf_field() .'
          <input type="hidden" name="_method" value="DELETE">
          '.$publish_button.'
          '.$block_button.'
          <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
          <span class="glyphicon glyphicon-trash"></span>
          </button>
          </form>
        ';
      })
      ->editColumn('status', function($data){
        return status_discussion($data->status);
      })
      ->rawColumns(['status','action'])
      ->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('adminlte::discussion.list');
  }

  public function publish($id){
    $Discussion = Discussion::where('id', $id)->first();
    $Discussion->status = '1';
    $Discussion->save();

    \Session::flash('success', 'Discussion success');
    return redirect('discussion');
  }

  public function block($id){
    $Discussion = Discussion::where('id', $id)->first();
    $Discussion->status = '0';
    $Discussion->save();

    \Session::flash('success', 'Discussion blocked');
    return redirect('discussion');
  }

  public function delete($id){
    $Discussion = Discussion::where('id', $id);
    if($Discussion){
      $Discussion->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('discussion');
  }
}
