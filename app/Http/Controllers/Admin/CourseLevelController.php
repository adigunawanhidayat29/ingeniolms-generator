<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\CourseLevel;
use Lang;

class CourseLevelController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = CourseLevel::select('id','title')->get();

      return DataTables::of($data)

      ->addColumn('action', function ($data)
      {
        return '
        <form name="delete" method="POST" action="'. url('admin/course-level/delete/'.$data->id) .'">
          <a href="'. url('admin/course-level/update/'.$data->id) .'" class="btn btn-warning btn-sm">
            <i class="material-icons">edit</i><span>'. Lang::get('back.course_level.edit_button') .'</span>
          </a>

          '. csrf_field() .'
          <input type="hidden" name="_method" value="DELETE">
          <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
            <i class="material-icons">delete</i><span>'. Lang::get('back.course_level.delete_button') .'</span>
          </button>
        </form>
        ';
      })->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('admin.course-level.list');
  }

  public function create(){
    $data = [
      'action' => '/admin/course-level/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
    ];
    return view('admin.course-level.form', $data);
  }

  public function create_action(Request $request){

    $CourseLevel = new CourseLevel;
    $CourseLevel->title = $request->title;

    if($CourseLevel->save()){
      \Session::flash('success', 'Create record success');
    }else{
      \Session::flash('error', 'Create record failed');
    }
    return redirect('admin/course-level');

  }

  public function update($id){
    $CourseLevel = CourseLevel::where('id', $id)->first();
    $data = [
      'action' => '/admin/course-level/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $CourseLevel->id),
      'title' => old('title', $CourseLevel->title),
    ];
    return view('admin.course-level.form', $data);
  }

  public function update_action(Request $request){
    $CourseLevel = CourseLevel::where('id', $request->id)->first();

    $CourseLevel->title = $request->title;

    if($CourseLevel->save()){
      \Session::flash('success', 'Update record success');
    }else{
      \Session::flash('error', 'Update record failed');
    }

    return redirect('admin/course-level');
  }

  public function delete($id){
    $CourseLevel = CourseLevel::where('id', $id);
    if($CourseLevel){
      $CourseLevel->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('admin/course-level');
  }

}
