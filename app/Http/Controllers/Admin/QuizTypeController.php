<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\QuizType;
use Validator;

class QuizTypeController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = QuizType::select('id','type')->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        return '
        <form name="delete" method="POST" action="'. url('quiz-type/delete/'.$data->id) .'">
        <a href="'. url('quiz-type/update/'.$data->id) .'" class="btn btn-warning btn-sm">
        <span class="glyphicon glyphicon-edit"></span>
        </a>

        '. csrf_field() .'
        <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
        <span class="glyphicon glyphicon-trash"></span>
        </button>

        </form>

        ';
      })->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('adminlte::quiz_type.list');
  }

  public function create(){
    $data = [
      'action' => 'quiz-type/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'type' => old('type'),
    ];
    return view('adminlte::quiz_type.form', $data);
  }

  public function create_action(Request $request){

    $validator = $this->rules($request);
    if ($validator->fails()){
      \Session::flash('error', $validator->errors());
      return $this->create();
    }

    $QuizType = new QuizType;
    $QuizType->type = $request->type;

    if($QuizType->save()){
      \Session::flash('success', 'Create record success');
    }else{
      \Session::flash('error', 'Create record failed');
    }
    return redirect('quiz-type');

  }

  public function update($id){
    $QuizType = QuizType::where('id', $id)->first();
    $data = [
      'action' => 'quiz-type/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $QuizType->id),
      'type' => old('type', $QuizType->type),
    ];
    return view('adminlte::quiz_type.form', $data);
  }

  public function update_action(Request $request){
    $QuizType = QuizType::where('id', $request->id)->first();

    $QuizType->type = $request->type;

    if($QuizType->save()){
      \Session::flash('success', 'Update record success');
    }else{
      \Session::flash('error', 'Update record failed');
    }

    return redirect('quiz-type');
  }

  public function delete($id){
    $QuizType = QuizType::where('id', $id);
    if($QuizType){
      $QuizType->delete();
      \Session::flash('success', 'Delete record success');
    }else{
      \Session::flash('error', 'Delete record failed');
    }

    return redirect('quiz-type');
  }

  public function rules($request){
    return $validator = Validator::make($request->all(),[
      'type' => 'required',
    ]);
  }
}
