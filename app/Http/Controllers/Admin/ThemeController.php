<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Theme;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $themes = Theme::latest()->paginate($perPage);
        } else {
            $themes = Theme::latest()->paginate($perPage);
        }

        return view('admin.themes.index', compact('themes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.themes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // upload theme to public        
        if($request->file('source')){
            // dd($request->file('source'));
            $destinationPath = 'themes/'; // upload path
            $file = rand(111,9999) . $request->file('source')->getClientOriginalName(); // renameing image
            $request->file('source')->move($destinationPath, $file); // uploading file to given path

            //extract zip
            $Path = $destinationPath . $file ;
            \Zipper::make($Path)->extractTo(public_path('themes'));
            //extract zip
        }        
        // upload theme to public

        // upload theme to view     
        if($request->file('source_blade')){
            // dd($request->file('source'));
            $destinationPath = public_path('themes/'); // upload path
            $file = rand(111,9999) . $request->file('source_blade')->getClientOriginalName(); // renameing image
            $request->file('source_blade')->move($destinationPath, $file); // uploading file to given path

            //extract zip
            $Path = $destinationPath . $file ;
            \Zipper::make($Path)->extractTo(resource_path('/views/themes/'));
            //extract zip
        }        
        // upload theme to view

        $Theme = new Theme;
        $Theme->title = $request->title;
        $Theme->name = 'themes/'.$request->name;
        $Theme->description = '/themes/'.$request->name;
        $Theme->status = 0;
        // $Theme->thumbnail = $request->thumbnail;
        $Theme->save();

        return redirect('admin/themes')->with('flash_message', 'Theme added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $theme = Theme::findOrFail($id);

        return view('admin.themes.show', compact('theme'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $theme = Theme::findOrFail($id);

        return view('admin.themes.edit', compact('theme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $theme = Theme::findOrFail($id);
        $theme->update($requestData);

        return redirect('admin/themes')->with('flash_message', 'Theme updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Theme::destroy($id);

        return redirect('admin/themes')->with('flash_message', 'Theme deleted!');
    }

    public function activated($id)
    {        

        $Theme = Theme::whereNotIn('id', [$id]);
        $Theme->update(['status' => 0]);

        $Theme = Theme::find($id);
        $Theme->status = 1;
        $Theme->save();

        return redirect('admin/settings')->with('flash_message', 'Theme activated!');
    }
}
