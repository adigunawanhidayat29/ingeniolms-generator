<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promotion;
use Session;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = [
        'promotions' => Promotion::orderBy('id', 'desc')->paginate(10),
      ];
      return view('adminlte::promotion.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data = [
        'action' => '/promotion/store',
        'name' => old('name'),
        'banner' => old('banner'),
        'description' => old('description'),
        'discount_code' => old('discount_code'),
        'discount' => old('discount'),
        'start_date' => old('start_date'),
        'end_date' => old('end_date'),
        'status' => old('status'),
      ];
      return view('adminlte::promotion.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $Promotion = new Promotion;

      // banner
      if($request->file('banner')){
        $destinationPath = asset_path('uploads/promotions/'); // upload path
        $extension = $request->file('banner')->getClientOriginalExtension(); // getting image extension
        $banner = str_slug($request->name).rand(111,9999).'.'.$extension; // renameing image
        $request->file('banner')->move($destinationPath, $banner); // uploading file to given path
        $Promotion->banner = '/uploads/promotions/'.$banner;
      }
      // banner

      $Promotion->name = $request->name;
      $Promotion->description = $request->description;
      $Promotion->discount_code = $request->discount_code;
      $Promotion->discount = $request->discount;
      $Promotion->start_date = $request->start_date;
      $Promotion->end_date = $request->end_date;
      $Promotion->status = $request->status;
      $Promotion->save();

      Session::flash('success', 'Insert record success');
      return redirect('promotion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $Promotion = Promotion::where('id', $id)->first();
      $data = [
        'action' => '/promotion/update/'.$id,
        'name' => old('name', $Promotion->name),
        'banner' => old('banner', $Promotion->banner),
        'description' => old('description', $Promotion->description),
        'discount_code' => old('discount_code', $Promotion->discount_code),
        'discount' => old('discount', $Promotion->discount),
        'start_date' => old('start_date', $Promotion->start_date),
        'end_date' => old('end_date', $Promotion->end_date),
        'status' => old('status', $Promotion->status),
      ];
      return view('adminlte::promotion.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $Promotion = Promotion::where('id', $id)->first();

      // banner
      if($request->file('banner')){
        $destinationPath = asset_path('uploads/promotions/'); // upload path
        $extension = $request->file('banner')->getClientOriginalExtension(); // getting image extension
        $banner = str_slug($request->name).rand(111,9999).'.'.$extension; // renameing image
        $request->file('banner')->move($destinationPath, $banner); // uploading file to given path
        $Promotion->banner = '/uploads/promotions/'.$banner;
      }
      // banner

      $Promotion->name = $request->name;
      $Promotion->description = $request->description;
      $Promotion->discount_code = $request->discount_code;
      $Promotion->discount = $request->discount;
      $Promotion->start_date = $request->start_date;
      $Promotion->end_date = $request->end_date;
      $Promotion->status = $request->status;
      $Promotion->save();

      Session::flash('success', 'Update record success');
      return redirect('promotion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $Promotion = Promotion::where('id', $id)->delete();

      Session::flash('success', 'Delete record success');
      return redirect('promotion');
    }
}
