<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Degree;
use App\DegreeCourse;
use App\DegreeUser;
use App\Course;
use App\CourseUser;
use DB;
use Storage;

class DegreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $degree = Degree::select('degrees.*','degree_levels.title as level')
        ->join('degree_levels', 'degree_levels.id', '=', 'degrees.degree_level_id')
        ->paginate(10);

      $data = [
        'degrees' => $degree,
      ];

      return view('admin.degree.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data = [
        'action' => 'admin/degree/store',
        'method' => 'post',
        'button' => 'Create',
        'id' => old('id'),
        'degree_level_id' => old('degree_level_id'),
        'title' => old('title'),
        'password' => old('password'),
        'description' => old('description'),
        'image' => old('image'),
        'introduction' => old('introduction'),
        'introduction_description' => old('introduction_description'),
        'benefits' => old('benefits'),
        'developer_team' => old('developer_team'),
        'precondition' => old('precondition'),
        'degree_levels' => DB::table('degree_levels')->get(),
      ];

      return view('admin.degree.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $folder_path = "11_feM8jXCIBMOWTMzfg0Ag_K-M_kX8zQ"; // folder path degeee

      $Degree = new Degree;

      // image
      if($request->file('image')){
        // $destinationPath = asset_path('uploads/degrees/'); // upload path
        // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
        // $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
        // $request->file('image')->move($destinationPath, $image); // uploading file to given path
        // $Degree->image = '/uploads/degrees/'.$image;
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'degrees/' . $name_file, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'degrees/', $name_file);
        $Degree->image = $path_file;
      }
      // image

      // introduction
      // if($request->file('introduction')){
      //   $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
      //   $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
      //   Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
      //   $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
      // }else{
      //   $introduction = '';
      // }
      // introduction

      $Degree->title = $request->title;      
      $Degree->introduction = $introduction;
      $Degree->password = $request->password;
      $Degree->slug = str_slug($request->title);
      $Degree->description = $request->description;
      $Degree->introduction_description = $request->introduction_description;
      $Degree->benefits = $request->benefits;
      $Degree->developer_team = $request->developer_team;
      $Degree->precondition = $request->precondition;
      $Degree->degree_level_id = $request->degree_level_id;

      if($Degree->save()){
        \Session::flash('success', 'Create record success');
      }else{
        \Session::flash('error', 'Create record failed');
      }
      return redirect('admin/degree');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $Degree = Degree::where('id', $id)->first();
      $data = ['degree' => $Degree];
      return view('admin.degree.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $Degree = Degree::where('id', $id)->first();
      $data = [
        'action' => 'admin/degree/update',
        'method' => 'put',
        'button' => 'Update',
        'id' => old('id', $Degree->id),
        'degree_level_id' => old('id', $Degree->degree_level_id),
        'title' => old('title', $Degree->title),
        'image' => old('image', $Degree->image),
        'introduction' => old('introduction', $Degree->introduction),
        'password' => old('password', $Degree->password),
        'description' => old('description', $Degree->description),
        'introduction_description' => old('introduction_description', $Degree->introduction_description),
        'benefits' => old('benefits', $Degree->benefits),
        'developer_team' => old('developer_team', $Degree->developer_team),
        'precondition' => old('precondition', $Degree->precondition),
        'degree_levels' => DB::table('degree_levels', $Degree->degree_levels)->get(),
      ];

      return view('admin.degree.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $Degree = Degree::where('id', $request->id)->first();
      $Degree->degree_level_id = $request->degree_level_id;
      $Degree->title = $request->title;
      $Degree->slug = str_slug($request->title);
      $Degree->description = $request->description;
      $Degree->introduction_description = $request->introduction_description;
      $Degree->benefits = $request->benefits;
      $Degree->developer_team = $request->developer_team;
      $Degree->precondition = $request->precondition;

      $folder_path = "11_feM8jXCIBMOWTMzfg0Ag_K-M_kX8zQ"; // folder path degeee

      // image
      if($request->file('image')){
        // $destinationPath = asset_path('uploads/degrees/'); // upload path
        // $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
        // $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
        // $request->file('image')->move($destinationPath, $image); // uploading file to given path
        // $Degree->image = '/uploads/degrees/'.$image;

        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renaming image
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'degrees/' . $name_file, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'degrees/', $name_file);
        $Degree->image = $path_file;
      }
      // image

      // introduction
      if($request->file('introduction')){
        $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
        $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
        Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
        $introduction = "https://drive.google.com/file/d/".get_asset_path($folder_path, $nameIntroduction);
        $Degree->introduction = $introduction;
      }
      // introduction

      if($Degree->save()){
        \Session::flash('success', 'Create record success');
      }else{
        \Session::flash('error', 'Create record failed');
      }
      return redirect('admin/degree');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $Degree = Degree::where('id', $id);
      if($Degree){
        $Degree->delete();
        \Session::flash('success', 'Delete record success');
      }else{
        \Session::flash('error', 'Delete record failed');
      }

      return redirect('admin/degree');
    }

    public function module_add($id){
      $DegreeCourses = DegreeCourse::where('degree_id', $id)->get();
      $data_course_id = [];
      foreach($DegreeCourses as $DegreeCourse){
        array_push($data_course_id, $DegreeCourse->course_id);
      }
      // dd($data_course_id);
      $data = [
        'courses' => Course::whereNotIn('id', $data_course_id)->get(),
        'DegreeCourses' => Course::select('courses.*', 'degree_courses.id as degree_course_id', 'degree_courses.semester')->whereIn('courses.id', $data_course_id)->join('degree_courses','degree_courses.course_id','=','courses.id')->get(),
      ];

      return view('adminl.degree.module_form', $data);
    }

    public function module_store(Request $request, $id){

      // insert degree course
      foreach ($request->course_id as $key => $value) {
        $DegreeCourse = new DegreeCourse;
        $DegreeCourse->degree_id = $id;
        // $DegreeCourse->semester = $request->semester;
        $DegreeCourse->course_id = $value;
        $DegreeCourse->save();

        // auto enrolled degree user
        $DegreeUsers = DegreeUser::where('degree_id', $id)->get();
        foreach($DegreeUsers as $DegreeUser){
          $CourseUser = new CourseUser;
          $CourseUser->course_id = $value;
          $CourseUser->user_id = $DegreeUser->user_id;
          $CourseUser->save();
        }
        // auto enrolled degree user

      }
      // insert degree course

      \Session::flash('success', 'Save record success');
      return redirect('admin/degree');
    }

    public function module_remove($id){
      $DegreeCourse = DegreeCourse::where('id', $id)->delete();
      return redirect()->back();
    }
}
