<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Content;
use Storage;

class SectionController extends Controller
{
  public function update_action($id, Request $request){
    $Section = Section::where('id', $id)->first();
    $Section->title = $request->section_title;
    $Section->description = $request->section_description;
    $Section->save();

    \Session::flash('success', 'Update section success');
    return redirect('courses/manage/'.$Section->id_course);
  }

  public function delete($id_course, $id){
    $Section = Section::where('id', $id);
    if($Section){
      //delete content
      $Contents = Content::where('id_section', $id);
      foreach($Contents->get() as $content){
        // unlink(asset_path($content->name_file));
        Storage::disk('google')->delete($content->path_file); // delete file google drive
      }
      $Contents->delete();
      //delete content

      $Section->delete();

      \Session::flash('success', 'Delete section success');
    }else{
      \Session::flash('error', 'Delete section failed');
    }

    return redirect('courses/manage/'.$id_course);
  }
}
