<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Blog;
use Illuminate\Http\Request;
use Storage;
use Lang;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blogs = Blog::where('title', 'LIKE', "%$keyword%")
                ->orWhere('body', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $blogs = Blog::latest()->paginate($perPage);
        }

        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            // $requestData['image'] = $request->file('image')->store('uploads', 'public');
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'blogs/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'blogs/' , $image);
            $requestData['image'] = $path_file;
        }

        Blog::create($requestData);

        return redirect('admin/blogs')->with('success', Lang::get('back.blog.alert_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            // $requestData['image'] = $request->file('image')->store('uploads', 'public');
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
            Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'blogs/' . $image, fopen($file, 'r+'), 'public'); //upload
            $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'blogs/' , $image);
            $requestData['image'] = $path_file;
        }

        $blog = Blog::findOrFail($id);
        $blog->update($requestData);

        return redirect('admin/blogs')->with('success', Lang::get('back.blog.alert_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Blog::destroy($id);

        return redirect('admin/blogs')->with('success', Lang::get('back.blog.alert_deleted'));
    }
}
