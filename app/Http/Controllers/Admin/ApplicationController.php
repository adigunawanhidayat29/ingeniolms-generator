<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Application;
use Datatables;
use Validator;
use Storage;
use Auth;

class ApplicationController extends Controller {
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        return view('adminlte::application.list');
    }

    public function serverside(Request $request){
        // $data_bak = Course::select('courses.id as id','title','image','name', 'courses.created_at as created_at')
        //     ->join('users', 'users.id', '=', 'courses.id_author')
        //     ->orderBy('courses.id', 'desc')
        //     ->get();

        $data = Application::select('applications.id as id', 'applications.name as site_title', 'image', 'hostname', 'app_id', 'app_secret', 'applications.email as email', 'users.name as name')
            ->join('users', 'users.id', '=', 'applications.created_by')
            ->get();
        
        if($request->ajax()){
            return Datatables::of($data)->addColumn('action', function ($data)
            {
                return '
                <form name="delete" method="POST" action="'. url('courses/delete/'.$data->id) .'">

                <a title="Manage App" href="'. url('courses/manage/'.$data->id) .'" class="btn btn-warning btn-sm">
                <span class="glyphicon glyphicon-pencil"></span>
                </a>

                <a title="Show Detail App" href="'. url('courses/show/'.$data->id) .'" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-search"></span>
                </a>

                '. csrf_field() .'
                <input type="hidden" name="_method" value="DELETE">
                <button title="Delete App" class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
                <span class="glyphicon glyphicon-trash"></span>
                </button>

                </form>

                ';
            })->make(true);
        }else{
            exit("Not an AJAX request");
        }
    }

}

?>