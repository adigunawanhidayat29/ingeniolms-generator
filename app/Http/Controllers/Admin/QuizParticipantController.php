<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\QuizParticipant;

class QuizParticipantController extends Controller
{
  public function serverside($quiz_id, Request $request)
  {
    if($request->ajax()){

      $data = QuizParticipant::select('quiz_participants.id as id','users.name as name', 'quiz_participants.quiz_id')
      ->join('users','users.id','=','quiz_participants.user_id')
      ->where('quiz_participants.quiz_id', $quiz_id)
      ->get();

      return DataTables::of($data)

      ->addColumn('action', function ($data)
      {
        return '
          <a href="'. url('course/quiz/participant/answer/'.$data->id.'/'.$data->quiz_id) .'" class="btn btn-info btn-sm btn-raised">
           Lihat Jawaban
          </a>
          <a href="'. url('course/quiz/participant/download/answer/'.$data->id) .'" class="btn btn-danger btn-sm btn-raised">
           Download Jawaban
          </a>
        ';
      })->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function create($quiz_id){
    $data = [
      'action' => 'course/quiz/participant/create_action/'.$quiz_id,
      'quiz_id' => $quiz_id,
      'users' => DB::table('users')->get(),
    ];
    return view('adminlte::course.quiz_participant_form', $data);
  }

  public function create_action($quiz_id, Request $request){
    $users = $request->user_id;
    foreach($users as $index => $value){
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz_id;
      $QuizParticipant->user_id = $value;
      $QuizParticipant->save();
    }

    \Session::flash('success', 'Create record success');
    return redirect('course/quiz/question/manage/'.$quiz_id);
  }
}
