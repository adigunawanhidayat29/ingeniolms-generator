<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\CourseUserImport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Validator;
use Storage;
use Image;
use Auth;
use App\Course;
use App\CourseUpdate;
use App\Cohort;
use App\CourseLive;
use App\CourseBatch;
use App\Section;
use App\Content;
use App\CourseUser;
use App\CourseCohort;
use App\Meeting;
use App\Progress;
use App\Discussion;
use App\Quiz;
use App\QuizQuestion;
use App\QuizParticipant;
use App\QuizQuestionAnswer;
use App\Assignment;
use App\CourseAnnouncement;
use App\Rating;
use App\CourseProject;
use App\CourseBroadcast;
use App\ContentVideoQuiz;
use App\PlayerIdUser;
use App\MeetingSchedule;
use App\UserLibrary;
use App\User;
use App\ProgramCourse;
use App\AssignmentAnswer;
use App\LibraryDirectoryGroup;
use App\LibraryDirectory;
use App\InstructorGroup;
use Lang;

class CourseController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function serverside(Request $request)
  {
    if($request->ajax()){

      $data = Course::select('courses.id as id','title','image', 'courses.password', 'courses.created_at as created_at')
      // ->join('users', 'users.id', '=', 'courses.id_author')
      ->orderBy('courses.id', 'desc')
      ->get();

      return Datatables::of($data)

      ->addColumn('action', function ($data)
      {
        return '
        <form name="delete" method="POST" action="'. url(\Request::route()->getPrefix() .'/course/delete/'.$data->id) .'">

        <a href="'. url(\Request::route()->getPrefix() .'/course/edit/'.$data->id) .'" class="btn btn-info btn-sm">
          <i class="material-icons">pending</i><span>'. Lang::get('back.course.edit_button') .'</span>
        </a>

        <a target="_blank" href="'. url('course/preview/'.$data->id) .'" class="btn btn-warning btn-sm">
          <i class="material-icons">pending</i><span>'. Lang::get('back.course.manage_button') .'</span>
        </a>

        '. csrf_field() .'
        <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger btn-sm" type="submit" name="submit" onclick="return confirm('."'".'Anda yakin ingin menghapus data ini?'."'".');">
          <i class="material-icons">delete</i><span>'. Lang::get('back.course.delete_button') .'</span>
        </button>

        </form>

        ';
      })
      ->addIndexColumn()
      ->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function index(){
    return view('admin.course.list');
  }

  public function show($id){
    $Course = DB::table('courses')
      ->select('courses.*','categories.title as category','course_levels.title as level','users.name as author')
      ->join('users', 'users.id', '=', 'courses.id_author')
      ->join('categories', 'categories.id', '=', 'courses.id_category')
      ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.id', $id)->first();

    $CourseUser = CourseUser::where('course_id', $id)->count();

    $data = [
      'courses' => $Course,
      'course_user' => $CourseUser,
    ];
    return view('admin.course.show', $data);
  }

  public function create(){
    $data = [
      'action' => \Request::route()->getPrefix() . '/course/create_action',
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'subtitle' => old('subtitle'),
      'goal' => old('goal'),
      'description' => old('description'),
      'image' => old('image'),
      'introduction' => old('introduction'),
      'price' => old('price','0'),
      'password' => old('password'),
      'model' => old('model'),
      'public' => old('public'),
      'id_category' => old('id_category'),
      'id_level_course' => old('id_level_course'),
      'id_author' => old('id_author'),
      'authors' => DB::table('users')->get(),
      'categories' => DB::table('categories')->get(),
      'course_levels' => DB::table('course_levels')->get(),
    ];
    return view('admin.course.form', $data);
  }

  public function create_action(Request $request){

    $validator = $this->rules($request);
    if ($validator->fails()){
      \Session::flash('error', $validator->errors());
      return $this->create();
    }

    //create folder google drive by title
    // $folder = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100,999);
    // Storage::disk('google')->makeDirectory($folder);
    // $folder_path = get_folder_path_course($folder);

    // create do folder
    $folder = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100,999);
    Storage::disk(env('APP_DISK'))->makeDirectory(env('UPLOAD_PATH') . '/courses/' .$folder);
    // Storage::disk(env('APP_DISK'))->makeDirectory('assets/'.$folder);

    // insert course
    $Course = new Course;

    // image
    if($request->file('image')){
      // $destinationPath = asset_path('uploads/courses/'); // upload path
      $file = $request->file('image');
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image

      // $imageSmall = Image::make($request->file('image'))->resize(240, 135)->save($destinationPath. "small_".$image);
      // $imageMedium = Image::make($request->file('image'))->resize(480, 270)->save($destinationPath. "medium_".$image);
      // $request->file('image')->move($destinationPath, $image); // uploading file to given path
      
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' , $image);

      // $Course->image = '/uploads/courses/'.$image;
      $Course->image = $path_file;
    }else{
      $Course->image = '';
    }
    // image

    // introduction
    if($request->file('introduction')){
      $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
      $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
      Storage::disk('google')->put($folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
      $introduction = "https://drive.google.com/file/d/" . get_asset_path($folder_path, $nameIntroduction);
    }else{
      $introduction = '';
    }
    // introduction

    $Course->title = $request->title;
    $Course->subtitle = $request->subtitle;
    $Course->goal = $request->goal;
    $Course->description = $request->description;
    $Course->folder = $folder;
    // $Course->folder_path = $folder_path;
    $Course->price = $request->price;
    $Course->password = $request->password;
    $Course->model = $request->model;
    $Course->id_category = $request->id_category;
    $Course->id_level_course = $request->id_level_course;
    $Course->id_author = $request->id_author;
    $Course->status = $request->public == 'on' ? '1' : '0';
    $Course->public = $request->public == 'on' ? '1' : '0';
    $Course->enrollment_type = isset(Setting('enrollment')->value) ? Setting('enrollment')->value : 'manual,self';
    $Course->save();
    $Course_id = $Course->id;
    // insert course

    // insert user course
    $CourseUser = new CourseUser;
    $CourseUser->user_id = $request->id_author;
    $CourseUser->course_id = $Course_id;
    $CourseUser->save();
    // insert user course

    // insert section
    $Section = new Section;
    $Section->title = 'Section 1';
    $Section->id_course = $Course_id;
    $Section->save();
    // insert section

    // insert meeting
    $Meeting = new Meeting;
    $Meeting->course_id = $Course_id;
    $Meeting->name = $request->title;
    $Meeting->password = rand(1000,9990);
    $Meeting->password_instructor = "instructor".rand(1000,9990);
    $Meeting->save();
    // insert meeting

    \Session::flash('success', Lang::get('back.course.message.create_success'));
    // return redirect(\Request::route()->getPrefix() .'/course/manage/'.$Course_id);
    return redirect(\Request::route()->getPrefix() .'/courses');

  }

  public function manage_($id){
    $Course = Course::where('id', $id)->first();

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $id)->get();

    foreach($sections as $section){
      $contents = DB::table('contents')->where('id_section', $section->id)->orderBy('sequence', 'asc')->get();
      $quizzes = DB::table('quizzes')->where('id_section', $section->id)->get();
      $assignments = DB::table('assignments')->where('id_section', $section->id)->get();
      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_contents' => $contents,
        'section_quizzes' => $quizzes,
        'section_assignments' => $assignments,
      ]);
    }

    $data = [
      'action' => 'admin/course/update_action',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Course->id),
      'title' => old('title', $Course->title),
      'goal' => old('goal', $Course->goal),
      'description' => old('description', $Course->description),
      'image' => old('image', $Course->image),
      'introduction' => old('introduction', $Course->introduction),
      'price' => old('price', $Course->price),
      'password' => old('password', $Course->password),
      'model' => old('model', $Course->model),
      'id_category' => old('id_category', $Course->id_category),
      'id_level_course' => old('id_level_course', $Course->id_level_course),
      'id_author' => old('id_author', $Course->id_author),
      'authors' => DB::table('users')->get(),
      'categories' => DB::table('categories')->get(),
      'course_levels' => DB::table('course_levels')->get(),
      'sections' => $section_data,
    ];
    return view('admin.course.manage', $data);
  }

  public function update_action(Request $request){

    $Course = Course::where('id', $request->id)->first();

    //image
    if($request->file('image')){
      // unlink(asset_path($request->old_image));
      $destinationPath = asset_path('uploads/courses/'); // upload path
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      $request->file('image')->move($destinationPath, $image); // uploading file to given path
      $update_image = '/uploads/courses/'.$image;
    }else{
      $update_image = $request->old_image;
    }
    //image

    //introduction
    if($request->file('introduction')){
      $extensionIntroduction = $request->file('introduction')->getClientOriginalExtension(); // getting image extension
      $nameIntroduction = str_slug($request->title).'-'.rand(111,9999).'.'.$extensionIntroduction; // renameing image
      Storage::disk('google')->put($Course->folder_path.'/'.$nameIntroduction, fopen($request->file('introduction'), 'r+'));//upload google drive
      $introduction = get_asset_path($Course->folder_path, $nameIntroduction);
      $Course->introduction = "https://drive.google.com/file/d/".$introduction;
    }
    //introduction

    $Course->title = $request->title;
    $Course->goal = $request->goal;
    $Course->description = $request->description;
    $Course->image = $update_image;
    $Course->price = $request->price;
    $Course->password = $request->password;
    $Course->model = $request->model;
    $Course->id_category = $request->id_category;
    $Course->id_level_course = $request->id_level_course;
    $Course->id_author = $request->id_author;
    $Course->save();

    \Session::flash('success', Lang::get('back.course.message.update_success'));
    return redirect(\Request::route()->getPrefix() . '/courses/manage/'.$Course->id);
  }

  public function save_section(Request $request){
    $Section = new Section;
    $Section->title = $request->title;
    $Section->id_course = $request->id_course;
    $Section->save();
  }

  public function delete($id){
    $Course = Course::where('id', $id);
    if($Course){
      // delete section and contents
      $Sections = Section::where('id_course', $id);
      foreach($Sections->get() as $section){
        $Contents = Content::where('id_section', $section->id);
        // foreach($Contents->get() as $content){
        //   // unlink(asset_path($content->name_file));
        //   Storage::disk('google')->delete($content->path_file); // delete file google drive
        // }
        $Contents->delete();
      }
      // Storage::disk('google')->deleteDirectory($Course->first()->folder_path); // delete directory course
      $Sections->delete();
      // delete section and contents

      // delete course
      // unlink(asset_path($Course->first()->image));
      $Course->delete();
      // delete course

      \Session::flash('success', Lang::get('back.course.message.delete_success'));
    }else{
      \Session::flash('error', Lang::get('back.course.message.create_error'));
    }
    return redirect( \Request::route()->getPrefix() . '/courses' );
  }

  public function participant($course_id){
    $CourseUsers = CourseUser::where('course_id', $course_id)->join('users','users.id','=','courses_users.user_id')->get();
    $Course = Course::where('id', $course_id)->first();
    $data = [
      'courses' => $Course,
      'course_users' => $CourseUsers
    ];
    return view('admin.course.participant', $data);
  }

  public function rules($request){
    return $validator = Validator::make($request->all(),[
      'title' => 'required',
    ]);
  }

  public function import(Request $request){
    // file
    $destinationPath = asset_path('uploads/courses/data'); // upload path
    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
    $file = rand(11111,999999).'.'.$extension; // renameing image
    $request->file('file')->move($destinationPath, $file); // uploading file to given path
    // file

    $user = Auth::user();

    Excel::load(asset_path('uploads/courses/data/'.$file), function($reader) use($user) {
      $results = $reader->get();
      // dd($results);
      foreach($results as $row){
        // dd($row->correct);

        $CheckCourse = Course::where('title', $row->course_title)->first();

        if(!$CheckCourse){

          if($row->course_title != ""){
            //create folder google drive by title
            $folder = str_slug($row->course_title) . '-' . date("Y-m-d") . '-' . rand(100,999);
            Storage::disk('google')->makeDirectory($folder);
            Storage::disk('sftp')->makeDirectory( server_assets_path($folder), 0777, true, true );
            $folder_path = get_folder_path_course($folder);
            //create folder google drive by title

            // insert course
            $Course = new Course;
            $Course->title = $row->course_title;
            $Course->image = '/uploads/courses/default.jpg';
            $Course->folder = $folder;
            $Course->folder_path = $folder_path;
            $Course->status = '0';
            $Course->id_category = '1';
            $Course->id_level_course = '1';
            $Course->id_author = $user->id;
            $Course->save();
            $Course_id = $Course->id;
            // insert course

            // insert user course
            $CourseUser = new CourseUser;
            $CourseUser->user_id = $user->id;
            $CourseUser->course_id = $Course_id;
            $CourseUser->save();
            // insert user course

            // insert meeting
            $Meeting = new Meeting;
            $Meeting->course_id = $Course_id;
            $Meeting->name = $row->course_title;
            $Meeting->password = rand(1000,9990);
            $Meeting->password_instructor = "instructor".rand(1000,9990);
            $Meeting->save();
            // insert meeting

            // insert section
            $Section = new Section;
            $Section->title = 'Section 1';
            $Section->id_course = $Course_id;
            $Section->save();
            // insert section

            if($row->content_title){
              // insert content
              $Content = new Content;
              $Content->id_section = $Section->id;
              $Content->title = $row->content_title;
              $Content->name_file = $row->content_file_name;
              $Content->path_file = $row->content_full_path_file;
              $Content->full_path_file = $row->content_full_path_file;
              $Content->full_path_file_resize = $row->content_full_path_file_resize;
              $Content->full_path_file_resize_720 = $row->content_full_path_file;
              $Content->type_content = 'video';
              $Content->status = '1';
              $Content->save();
              // insert content
            }
          }
        }else{
          $Course_id = $CheckCourse->id;
          $CheckSection = Section::where('id_course', $Course_id)->first();
          if(!$CheckSection){
            // insert section
            $Section = new Section;
            $Section->title = 'Section 1';
            $Section->id_course = $Course_id;
            $Section->save();
            // insert section
          }
          $CheckContent = Content::where(['title' => $row->content_title, 'id_section' => $CheckSection->id])->first();

          if(!$CheckContent){
            if($row->content_title){
              // insert content
              $Content = new Content;
              $Content->id_section = $CheckSection->id;
              $Content->title = $row->content_title;
              $Content->name_file = $row->content_file_name;
              $Content->path_file = $row->content_full_path_file;
              $Content->full_path_file = $row->content_full_path_file;
              $Content->full_path_file_resize = $row->content_full_path_file_resize;
              $Content->full_path_file_resize_720 = $row->content_full_path_file;
              $Content->type_content = 'video';
              $Content->status = '1';
              $Content->save();
              // insert content
            }
          }
        }
      }
    });

    \Session::flash('success', Lang::get('back.course.message.import_success'));
    return redirect()->back();
  }

  public function bulk_enroll($course_id){
    return view('admin.course.bulk');
  }

  public function bulk_enroll_action($course_id, Request $request){
      $course_id = $course_id;

      $destinationPath = 'imports/courses/enroll/'; // upload path
      $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      $file = str_slug($request->file('file')->getClientOriginalName()).time().'.'.$extension; // renameing image
      $request->file('file')->move($destinationPath, $file); // uploading file to given path

      Excel::import(new CourseUserImport($course_id), $destinationPath . $file);

      return redirect(\Request::route()->getPrefix() .'/courses')->with('success', Lang::get('back.course.message.bulk_enroll_success'));
  }

  public function preview($course_id){
    //
    // $user = Auth::user();
    // // {{ For checking the author of the course, please pull snippets below on server }}
    // $UserAuthorId = Auth::user()->id; // {{ Getting signed in instructor }}
    //
    // // $checkingAuthor = Course::whereRaw("find_in_set($user->id, id_author)")
    // //   ->orWhere('courses.id_instructor_group',function($query) use($user) {
    // //      $query->select('instructor_groups.id')
    // //        ->whereRaw("find_in_set($user->id, user_id)")
    // //        ->from('instructor_groups')
    // //        ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
    // //   })->first();
    // // if (empty($checkingAuthor)) { // {{ If the course not authored by the signed in instructor, redirect to back request }}
    // //   return back();
    // // }
    // // {{ For checking the author of the course, please pull snippets above on server }}
    //
    // $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo','course_levels.id as level_id')
    //   ->join('users', 'users.id', '=', 'courses.id_author')
    //   ->join('categories', 'categories.id', '=', 'courses.id_category')
    //   ->join('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
    //   ->where('courses.id', $course_id)->first();
    //
    // $section_data = [];
    // $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();
    //
    // $num_contents = 0;
    // foreach($sections as $section){
    //   $contents = DB::table('contents')
    //     ->where('id_section', $section->id)
    //     ->orderBy('contents.sequence', 'asc')
    //     ->get();
    //   $quizzes = DB::table('quizzes')->where(['id_section' => $section->id])->get();
    //   $assignments = DB::table('assignments')->where(['id_section' => $section->id])->get();
    //   $num_contents += count($contents);
    //
    //   array_push($section_data, [
    //     'id' => $section->id,
    //     'title' => $section->title,
    //     'sequence' => $section->sequence,
    //     'description' => $section->description,
    //     'id_course' => $section->id_course,
    //     'created_at' => $section->created_at,
    //     'updated_at' => $section->updated_at,
    //     'section_contents' => $contents,
    //     'section_quizzes' => $quizzes,
    //     'section_assignments' => $assignments,
    //   ]);
    // }
    //
    // $meeting = Meeting::where('course_id', $courses->id)->first();
    // $discussions = Discussion::select('discussions.id as id','discussions.course_id','discussions.body','discussions.created_at','users.name','users.photo')
    //               ->where(['course_id'=>$courses->id, 'status' => '1'])
    //               ->join('users','users.id','=','discussions.user_id')
    //               ->orderby('discussions.created_at','desc')
    //               ->limit(5)
    //               ->get();
    //
    // $announcements = CourseAnnouncement::select('course_announcements.*','users.photo as photo','users.name')
    //   ->where('course_id', $courses->id)
    //   ->join('courses','course_announcements.course_id','=','courses.id')
    //   ->join('users','users.id','=','courses.id_author')
    //   ->orderby('course_announcements.created_at','desc')->get();
    //
    // $announcements_data = [];
    // foreach($announcements as $announcement){
    //   $announcement_comments = DB::table('course_announcement_comments')
    //     ->join('users','users.id','=','course_announcement_comments.user_id')
    //     ->where('course_announcement_id', $announcement->id)->get();
    //   array_push($announcements_data, [
    //     'id' => $announcement->id,
    //     'course_id' => $announcement->course_id,
    //     'title' => $announcement->title,
    //     'description' => $announcement->description,
    //     'status' => $announcement->status,
    //     'created_at' => $announcement->created_at,
    //     'photo' => $announcement->photo,
    //     'name' => $announcement->name,
    //     'announcement_comments' => $announcement_comments,
    //   ]);
    // }
    //
    // $meeting_schedules = MeetingSchedule::where('course_id', $course_id)->orderBy('date', 'asc')->get();
    //
    // // library
    //
    // $folder = LibraryDirectoryGroup::where('question_bank', '0')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
    //   $query->where('user_id', Auth::user()->id);
    // })->get();
    // $myLibrary = LibraryDirectory::with(['library' => function($query) { $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
    //   $query->where('user_id', Auth::user()->id);
    // }); }])->get();
    // $publicLibrary = LibraryDirectory::get();
    // $publicLibrary = $publicLibrary[1];
    // $group = InstructorGroup::with('library')->whereIn('user_id', [Auth::user()->id])->get();
    //
    // $data = [
    //   'course' => $courses,
    //   'sections' => $section_data,
    //   'meeting' => $meeting,
    //   'meeting_schedules' => $meeting_schedules,
    //   'discussions' => $discussions,
    //   'announcements' => $announcements_data,
    //   'num_contents' => $num_contents,
    //   'num_progress' => $num_contents,
    //   'percentage' => 100,
    //   'rate' => 5,
    //   'authors' => DB::table('users')->select('users.id', 'users.name', 'users.email')->join('instructors', 'instructors.user_id', '=', 'users.id')->whereNotIn('users.id', explode(',', $courses->id_author))->get(),
    //   'instructor_groups' => DB::table('instructor_groups')->whereRaw("find_in_set($user->id, user_id)")->whereNotIn('id', explode(',', $courses->id_instructor_group))->get(),
    //   'folder' => $folder,
    //   'myLibrary' => $myLibrary,
    //   'publicLibrary' => $publicLibrary,
    //   'group' => $group,
    // ];

    $user = Auth::user();
    // {{ For checking the author of the course, please pull snippets below on server }}
    $UserAuthorId = Auth::user()->id; // {{ Getting signed in instructor }}

    // $checkingAuthor = Course::whereRaw("find_in_set($user->id, id_author)")
    //   ->orWhere('courses.id_instructor_group',function($query) use($user) {
    //      $query->select('instructor_groups.id')
    //        ->whereRaw("find_in_set($user->id, user_id)")
    //        ->from('instructor_groups')
    //        ->join('courses', 'courses.id_instructor_group', '=', 'instructor_groups.id');
    //   })->first();
    // if (empty($checkingAuthor)) { // {{ If the course not authored by the signed in instructor, redirect to back request }}
    //   return back();
    // }
    // {{ For checking the author of the course, please pull snippets above on server }}

    $courses = Course::select('courses.*','categories.title as category','course_levels.title as level','users.name as author','users.photo as photo','course_levels.id as level_id')
      ->leftJoin('users', 'users.id', '=', 'courses.id_author')
      ->leftJoin('categories', 'categories.id', '=', 'courses.id_category')
      ->leftJoin('course_levels', 'course_levels.id', '=', 'courses.id_level_course')
      ->where('courses.id', $course_id)->first();

    $courses_all = Course::where('id_author', 'like', '%'.Auth::user()->id.'%')->where('id', '!=', $courses->id)->get();

    // dd($courses);

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence','asc')->get();

    $num_contents = 0;

    foreach($sections as $index => $section){

      $section_all = [];

      $contents = Content::with('activity_order')->where('id_section', $section->id)
        ->orderBy('contents.sequence', 'asc')
        ->get();
      $quizzes = Quiz::with('activity_order')->where(['id_section' => $section->id])->get();
      $assignments = Assignment::with('activity_order')->where(['id_section' => $section->id])->get();
      $num_contents += count($contents);

      foreach ($contents as $content) {
        $content->section_type = 'content';
        $section_all[] = $content;
      }

      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }

      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'status' => $section->status,
        'sequence' => $section->sequence,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_contents' => $contents,
        'section_quizzes' => $quizzes,
        'section_assignments' => $assignments,
        'section_all' => $section_all,
      ]);
    }

    // print("<pre>".print_r($section_data,true)."</pre>");

    $meeting = Meeting::where('course_id', $course_id)->first();
    $update = CourseUpdate::where('course_id', $course_id)->get();
    $discussions = Discussion::select('discussions.id as id','discussions.course_id','discussions.body','discussions.created_at','users.name','users.photo')
      ->where(['course_id'=>$course_id, 'status' => '1'])
      ->join('users','users.id','=','discussions.user_id')
      ->orderby('discussions.created_at','desc')
      ->limit(5)
      ->get();

    $announcements = CourseAnnouncement::select('course_announcements.*','users.photo as photo','users.name')
      ->where('course_id', $course_id)
      ->join('courses','course_announcements.course_id','=','courses.id')
      ->join('users','users.id','=','courses.id_author')
      ->orderby('course_announcements.created_at','desc')->get();

    $announcements_data = [];
    foreach($announcements as $announcement){
      $announcement_comments = DB::table('course_announcement_comments')
        ->join('users','users.id','=','course_announcement_comments.user_id')
        ->where('course_announcement_id', $announcement->id)->get();
      array_push($announcements_data, [
        'id' => $announcement->id,
        'course_id' => $announcement->course_id,
        'title' => $announcement->title,
        'description' => $announcement->description,
        'status' => $announcement->status,
        'created_at' => $announcement->created_at,
        'photo' => $announcement->photo,
        'name' => $announcement->name,
        'announcement_comments' => $announcement_comments,
      ]);
    }

    $meeting_schedules = MeetingSchedule::where('course_id', $course_id)->orderBy('date', 'asc')->get();

    // library

    $folder = LibraryDirectoryGroup::where('question_bank', '0')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
      $query->where('user_id', Auth::user()->id);
    })->get();
    $myLibrary = LibraryDirectory::with(['library' => function($query) { $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
      $query->where('user_id', Auth::user()->id);
    }); }])->get();
    $publicLibrary = LibraryDirectory::get();
    $publicLibrary = $publicLibrary[1];
    $group = InstructorGroup::with('library')->whereIn('user_id', [Auth::user()->id])->get();

    $data = [
      'course' => $courses,
      'course_all' => $courses_all,
      'sections' => $section_data,
      'meeting' => $meeting,
      'meeting_schedules' => $meeting_schedules,
      'discussions' => $discussions,
      'announcements' => $announcements_data,
      'num_contents' => $num_contents,
      'num_progress' => $num_contents,
      'percentage' => 100,
      'rate' => 5,
      'authors' => DB::table('users')->select('users.id', 'users.name', 'users.email')->join('instructors', 'instructors.user_id', '=', 'users.id')->whereNotIn('users.id', explode(',', $courses->id_author))->get(),
      'instructor_groups' => DB::table('instructor_groups')->whereRaw("find_in_set($user->id, user_id)")->whereNotIn('id', explode(',', $courses->id_instructor_group))->get(),
      'folder' => $folder,
      'myLibrary' => $myLibrary,
      'publicLibrary' => $publicLibrary,
      'group' => $group,
      'update' => $update,
    ];

    return view('admin.course.preview', $data);
  }

  public function atendee($course_id){
    $Course = Course::where('id', $course_id)->with(['course_user', 'course_cohort'])->first();
    $CoursesUsers = CourseUser::where('course_id', $course_id)->where('user_id', '!=', $Course->id_author)
      ->join('users','users.id','=','courses_users.user_id')->get();

      $sections = DB::table('sections')->where('id_course', $course_id)->get();
      $num_contents = 0;
      foreach($sections as $section){
        $contents = DB::table('contents')
          ->where('id_section', $section->id)
          ->get();
        $num_contents += count($contents);
      }

      $completions = [];
      foreach($CoursesUsers as $CourseUser){
        //count percentage
        $num_progress = 0;
        $num_progress = count(Progress::where(['course_id'=>$course_id, 'user_id' => $CourseUser->user_id, 'status' => '1'])->get());
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = 100 / $percentage;
        //count percentage

        $section_data = [];
        $sectionscontents = DB::table('sections')->where('id_course', $course_id)->orderBy('sequence','asc')->orderBy('updated_at','desc')->get();
        foreach($sectionscontents as $section){
          $contents = DB::table('contents')
            ->where('id_section', $section->id)
            ->get();

          array_push($section_data, [
            'id' => $section->id,
            'title' => $section->title,
            'section_contents' => $contents,
          ]);
        }

        array_push($completions, [
          'name' => $CourseUser->name,
          'user_id' => $CourseUser->user_id,
          'percentage' => $percentage,
          'section_data' => $section_data
        ]);
      }

    $users = User::get();
    $cohorts = Cohort::get();

    $data = [
      'completions' => $completions,
      'Course' =>  $Course,
      'users' => $users,
      'cohorts' => $cohorts,
    ];

    return view('admin.course.atendee', $data);
  }

  public function addUser($course_id, Request $request){
    if ($request->user_id) {
      $user_id = array_unique($request->user_id);
      CourseUser::where('course_id', $course_id)->delete();

      if ($user_id) {
        foreach ($user_id as $index => $value) {
          $CourseUser = new CourseUser;
          $CourseUser->course_id = $course_id;
          $CourseUser->user_id = $value;
          $CourseUser->save();
        }
      }
    }

    return redirect('admin/course/atendee/' . $course_id)->with('flash_message', Lang::get('back.course.message.add_user_success'));
  }

  public function deleteCourseUser($course_id, $user_id) {
    CourseUser::where(['course_id' => $course_id, 'user_id' => $user_id])->delete();
    return redirect()->back()->with('flash_message', Lang::get('back.course.message.delete_user_success'));
  }

  public function addCourseCohort($course_id, Request $request){
    if ($request->cohort_id) {
      $cohort_id = array_unique($request->cohort_id);
      $cohorts = CourseCohort::with('cohort')->get();
      foreach ($cohorts as $data) {
        foreach ($data->cohort as $item) {
          foreach ($item->cohort_user as $user) {
            CourseUser::where(['course_id' => $course_id, 'user_id' => $user->user_id])->delete();
          }
        }
      }
      CourseCohort::where('course_id', $course_id)->delete();

      if ($cohort_id) {
        // insert cohort to course_cohort
        foreach ($cohort_id as $index => $value) {
          $CourseCohort = new CourseCohort;
          $CourseCohort->course_id = $course_id;
          $CourseCohort->cohort_id = $value;
          $CourseCohort->save();

          $cohort = Cohort::where('id', $value)->with('cohort_user')->first();
          foreach($cohort->cohort_user as $data) {
            CourseUser::where(['course_id' => $course_id, 'user_id' => $data->user_id])->delete();

            // insert cohort users to course_users
            $CourseUser = new CourseUser;
            $CourseUser->course_id = $course_id;
            $CourseUser->user_id = $data->user_id;
            $CourseUser->save();
          }
        }
      }
    } else {
      $cohorts = CourseCohort::with('cohort')->get();
      foreach ($cohorts as $data) {
        foreach ($data->cohort as $item) {
          foreach ($item->cohort_user as $user) {
            CourseUser::where(['course_id' => $course_id, 'user_id' => $user->user_id])->delete();
          }
        }
      }
      CourseCohort::where('course_id', $course_id)->delete();
    }

    return redirect('admin/course/atendee/' . $course_id)->with('flash_message', Lang::get('back.course.message.add_user_success'));
  }

  public function setEnrollment($course_id, Request $request) {
    $Course = Course::where('id', $course_id)->first();
    if ($request->enrollment_type) {
      $Course->enrollment_type = $request->enrollment_type = implode(',', $request->enrollment_type);
      $Course->save();
    } else {
      $Course->enrollment_type = null;
      $Course->save();
    }

    return redirect('admin/course/atendee/' . $course_id)->with('flash_message', Lang::get('back.course.message.update_enrollment_success'));
  }

  public function edit($id){
    $course = Course::find($id);
    $data = [
      'action' => \Request::route()->getPrefix() . '/course/edit_action',
      'method' => 'post',
      'button' => 'Update',
      'id' => old('id', $course->id),
      'title' => old('title', $course->title),
      'subtitle' => old('subtitle', $course->subtitle),
      'goal' => old('goal', $course->goal),
      'description' => old('description', $course->description),
      'image' => old('image', $course->image),
      'introduction' => old('introduction', $course->introduction),
      'price' => old('price', $course->price),
      'password' => old('password', $course->password),
      'model' => old('model', $course->model),
      'id_category' => old('id_category', $course->id_category),
      'id_level_course' => old('id_level_course', $course->id_level_course),
      'id_author' => old('id_author', $course->id_author),
      'public' => old('public', $course->public),
      'authors' => DB::table('users')->get(),
      'categories' => DB::table('categories')->get(),
      'course_levels' => DB::table('course_levels')->get(),
    ];
    return view('admin.course.form', $data);
  }

  public function edit_action(Request $request){

    $validator = $this->rules($request);
    if ($validator->fails()){
      \Session::flash('error', $validator->errors());
      return $this->create();
    }
    
    $Course = Course::find($request->id);
    $folder = $Course->folder;

    // image
    if($request->file('image')){
      // $destinationPath = asset_path('uploads/courses/'); // upload path
      $file = $request->file('image');
      $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
      $image = str_slug($request->title).rand(111,9999).'.'.$extension; // renameing image
      Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' . $image, fopen($file, 'r+'), 'public'); //upload
      $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/' . 'courses/' . $folder . '/thumbnails/' , $image);
      $Course->image = $path_file;
    }
    // image

    $Course->title = $request->title;
    $Course->subtitle = $request->subtitle;
    $Course->description = $request->description;
    $Course->price = $request->price;
    $Course->password = $request->password;
    $Course->id_category = $request->id_category;
    $Course->id_level_course = $request->id_level_course;
    $Course->id_author = $request->id_author;
    $Course->public = $request->public == 'on' ? '1' : '0';
    $Course->enrollment_type = isset(Setting('enrollment')->value) ? Setting('enrollment')->value : 'manual,self';
    $Course->save();
    $Course_id = $Course->id;
    // insert course

    \Session::flash('success', Lang::get('back.course.message.update_success'));
    return redirect('admin/courses');
  }

}
