<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Instructor;
use App\UserGroup;
use App\Course;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
      // get all users total
      $userTotal = User::get();
      $chartUser = User::select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
      ->groupby('year','month')
      ->get();

      // get all users total

      // get all teacher total
      $instructorTotal = User::select('users.id')
        ->join('instructors','instructors.user_id','=','users.id')
        ->get();
      // get all teacher total

      // get all student total
      $studentTotal = User::join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('level_id', '3')
        ->get();
      // get all student total

      // get all courses total
      $coursesTotal = Course::get();
      // get all courses total

      // get active courses by progresses update
      $activeCoursesDaily = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '=', Carbon::today())
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesWeekly = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(8))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesMonthly = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(31))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesAnual = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(366))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();
        // get active courses by progresses update

        // get active teacher by courses update
        $activeTeachersDaily = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '=', Carbon::today())
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersWeekly = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(8))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersMonthly = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(31))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersAnual = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(366))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();
        // get active teacher by courses update

        // get active student by courses update
        $activeStudentsDaily = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '=', Carbon::today())
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsWeekly = Course::select('users.name', 'progresses.created_at')
        ->where('progresses.created_at', '>=', Carbon::today()->subDays(8))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsMonthly = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(31))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsAnual = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(366))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();
        // get active student by courses update
      // set data
      $data = [
        'chartUser' => json_encode($chartUser),
        'all_users_total' => count($userTotal),
        'all_instructors_total' => count($instructorTotal),
        'all_students_total' => count($studentTotal),
        'all_courses_total' => count($coursesTotal),

        'all_active_courses_total_daily' => $activeCoursesDaily,
        'all_active_courses_total_weekly' => $activeCoursesWeekly,
        'all_active_courses_total_monthly' => $activeCoursesMonthly,
        'all_active_courses_total_anual' => $activeCoursesAnual,

        'all_active_teachers_total_daily' => $activeTeachersDaily,
        'all_active_teachers_total_weekly' => $activeTeachersWeekly,
        'all_active_teachers_total_monthly' => $activeTeachersMonthly,
        'all_active_teachers_total_anual' => $activeTeachersAnual,

        'all_active_students_total_daily' => $activeStudentsDaily,
        'all_active_students_total_weekly' => $activeStudentsWeekly,
        'all_active_students_total_monthly' => $activeStudentsMonthly,
        'all_active_students_total_anual' => $activeStudentsAnual,
      ];

      // dd($data);

      return view('admin.dashboard', $data);
    }
}
