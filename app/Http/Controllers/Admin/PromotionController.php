<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Promotion;

class PromotionController extends Controller
{
    public function index()
    {
        return view('admin.Promotion.index');
    }

    public function serverside(Request $request)
    {
        if ($request->ajax()) {
            $data = Promotion::select('*')->get();
            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    return '                
                    <a href="' . url(\Request::route()->getPrefix() . '/promotions/edit/' . $data->id) . '" class="btn btn-warning btn-sm">Edit</a>
                    <a href="' . url(\Request::route()->getPrefix() . '/promotions/delete/' . $data->id) . '" class="btn btn-danger btn-sm">Hapus</a>
                ';
                })->make(true);
        } else {
            exit("Not an AJAX request");
        }
    }

    public function create()
    {
        $data = [
            'action' => \Request::route()->getPrefix() . '/promotions/store',
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'name' => old('name'),
            'banner' => old('banner'),
            'description' => old('description'),
            'discount_code' => old('discount_code'),
            'discount' => old('discount'),
            'start_date' => old('start_date'),
            'end_date' => old('end_date'),
            'status' => old('status'),
            'notes' => old('notes'),

        ];
        return view('admin.Promotion.form', $data);
    }

    public function store(Request $request)
    {
        $Promotion = new Promotion;
        $Promotion->name = $request->name;
        $Promotion->banner = $request->banner;
        $Promotion->description = $request->description;
        $Promotion->discount_code = $request->discount_code;
        $Promotion->discount = $request->discount;
        $Promotion->start_date = $request->start_date;
        $Promotion->end_date = $request->end_date;
        $Promotion->status = $request->status;
        $Promotion->notes = $request->notes;

        $Promotion->save();

        \Session::flash('success', 'Create record success');
        return redirect(\Request::route()->getPrefix() . '/promotions');
    }

    public function edit($id)
    {
        $Promotion = Promotion::where('id', $id)->first();
        $data = [
            'action' => \Request::route()->getPrefix() . '/promotions/update',
            'method' => 'post',
            'button' => 'Update',
            'id' => old('id', $Promotion->id),
            'name' => old('name', $Promotion->name),
            'banner' => old('banner', $Promotion->banner),
            'description' => old('description', $Promotion->description),
            'discount_code' => old('discount_code', $Promotion->discount_code),
            'discount' => old('discount', $Promotion->discount),
            'start_date' => old('start_date', $Promotion->start_date),
            'end_date' => old('end_date', $Promotion->end_date),
            'status' => old('status', $Promotion->status),
            'notes' => old('notes', $Promotion->notes),

        ];
        return view('admin.Promotion.form', $data);
    }

    public function update(Request $request)
    {
        $Promotion = Promotion::where('id', $request->id)->first();
        $Promotion->id;
        $Promotion->name;
        $Promotion->banner;
        $Promotion->description;
        $Promotion->discount_code;
        $Promotion->discount;
        $Promotion->start_date;
        $Promotion->end_date;
        $Promotion->status;
        $Promotion->notes;

        $Promotion->save();
        \Session::flash('success', 'Update record success');

        return redirect(\Request::route()->getPrefix() . '/promotions');
    }

    public function delete(Request $request, $id)
    {
        $Promotion = Promotion::where('id', $id);
        if ($Promotion) {
            $Promotion->delete();
            \Session::flash('success', 'Delete record success');
        } else {
            \Session::flash('error', 'Delete record failed');
        }

        return redirect(\Request::route()->getPrefix() . '/promotions');
    }
}
