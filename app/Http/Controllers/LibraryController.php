<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Quiz;
use App\LibraryQuiz;
use App\LibraryDirectoryContent;
use App\LibraryDirectoryContentUser;
use App\LibraryAssignment;
use App\LibraryQuizQuestion;
use App\LibraryQuizQuestionAnswer;
use App\QuizQuestion;
use App\QuizPageBreak;
use App\QuizQuestionAnswer;
use App\QuizType;
use App\User;
use App\Content;
use App\Course;
use App\CourseLive;
use App\UserLibrary;
use App\ShareContent;
use App\Assignment;
use Validator;
use Storage;
use Plupload;
use FFMpeg;
use Excel;
use Auth;


class LibraryController extends Controller {

    public function index(Request $request){
        //$UserLibrary = new UserLibrary; //Instance first
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'parent' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'error' => $validator->errors()
            ], 406);
        }

        $user = User::where(['id' => $request->user_id])->first();
        //==== Getting User Information ==========
        if ($user->user_folder_path==null || $user->user_folder_name==null) {
            $folder_name = $user->slug. '-' . date("Y-m-d") . '-' . rand(100,999);
            Storage::disk('google')->makeDirectory($folder_name);
            Storage::disk('sftp')->makeDirectory(server_user_assets_path($folder_name), 0777, true, true );
            $folder_path = get_folder_path_course($folder_name);
            DB::table('users')->where('id', $user->id)->update(['user_folder_name' => $folder_name, 'user_folder_path' => $folder_path]);
        }
        //==== End Getting User Information =======
        $parent = $request->parent; //Set Parent
        $contents_data = [];
        $courses = DB::table('courses')->where(['id_author' => $user->id])->get(); //Getting Courses by Author
        $iteration = 0;

        //==== Push to array for sending as response json
        $UserLibraries = DB::table('userlibraries')->where(['user_id' => $user->id, 'parent' => $parent])->get();
        foreach($UserLibraries as $_UserLibrary){
            array_push($contents_data, [
                'id' => $_UserLibrary->id,
                'title' => $_UserLibrary->title,
                'name_file' => $_UserLibrary->name_file,
                'path_file' => $_UserLibrary->path_file,
                'type_content' => $_UserLibrary->type_content,
                'status' => $_UserLibrary->status,
                'file_size' => $_UserLibrary->file_size,
                'content_permission' => '1',
                'shared_with_me' => '0',
                'isShared' => !empty(DB::table('share_contents')->where(['content_lib_id' => $_UserLibrary->id])->first()) ? '1' : '0',
                'created_at' => $_UserLibrary->created_at,
            ]);
        }
        $CoOwners = DB::table('share_contents')->where(['user_id' => $user->id])->get();
        if (!empty($CoOwners)) {
            foreach ($CoOwners as $CoOwner) {
                $SharedUserLibraries = DB::table('userlibraries')->where(['id' => $CoOwner->content_lib_id, 'parent' => $parent])->get();
                foreach($SharedUserLibraries as $SharedUserLibrary){
                    array_push($contents_data, [
                        'id' => $SharedUserLibrary->id,
                        'title' => $SharedUserLibrary->title,
                        'name_file' => $SharedUserLibrary->name_file,
                        'path_file' => $SharedUserLibrary->path_file,
                        'type_content' => $SharedUserLibrary->type_content,
                        'status' => $SharedUserLibrary->status,
                        'file_size' => $SharedUserLibrary->file_size,
                        'content_permission' => DB::table('share_contents')->where(['content_lib_id' => $SharedUserLibrary->id, 'user_id' => $user->id])->first()->isWriteable,
                        'shared_with_me' => '1',
                        'isShared' => '1',
                        'created_at' => $SharedUserLibrary->created_at,
                    ]);
                }
            }
        }


        //=== Getting parent for Navigation
        $hasParents = [];
        array_push($hasParents, [
          'parent' => $parent=='.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $parent])->first()->title,
          'link_parent' => $parent=='.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $parent])->first()->name_file
        ]);
        $hasParent = $parent;
        while ($hasParent!='.'){
          $hasParent = DB::table('userlibraries')->select('parent')->where(['name_file' => $hasParent])->first()->parent;
          array_push($hasParents, [
            'parent' => $hasParent=='.' ? 'Directory Saya' : DB::table('userlibraries')->select('title')->where(['name_file' => $hasParent])->first()->title,
            'link_parent' => $hasParent=='.' ? '' : DB::table('userlibraries')->select('name_file')->where(['name_file' => $hasParent])->first()->name_file
          ]);
        }

        //=== End Getting parent for Navigation

        //=== Show response as json response for getting to another endpoints

        return response()->json([
            'iteration' => $iteration,
            'resources' => $contents_data,
            'parents' => $hasParents,
            'user_id' => $user->id,
        ]);

        //=== End Show response as json response for getting to another endpoints
    }

    public function show(){
      $content = UserLibrary::get();

      return view('course.library_react', compact('content'));
    }

    public function add_content_course(Request $request, $course_id, $section_id){

      if($request->content){
        foreach ($request->content as $key => $value) {
          $content = explode('/', $value);
          if($content[1] == 'assignment'){
            $assig = Assignment::find($content[0]);
            $section = Section::where('id', $section_id)->first();

            // insert Assignment
            $Assignment = new Assignment;
            $Assignment->title = $assig->title;
            $Assignment->description = $assig->description;
            $Assignment->type = $assig->type;
            $Assignment->attempt = $assig->attempt;
            $Assignment->time_start = $assig->time_start;
            $Assignment->time_end = $assig->time_end;
            $Assignment->id_section = $section_id;
            $Assignment->save();
          }else if($content[1] == 'quiz'){
            $quiz_library = Quiz::with('pagebreak', 'quiz_questions.quiz_question_answers')->find($content[0]);
            // insert Quiz
            $Quiz = new Quiz;

            $Quiz->name = $quiz_library->name;
            $Quiz->description = $quiz_library->description;
            $Quiz->slug = str_slug($quiz_library->name);
            // $Quiz->shuffle = $request->shuffle;
            $Quiz->time_start = $quiz_library->time_start;
            $Quiz->time_end =$quiz_library->time_end;
            $Quiz->duration = $quiz_library->duration;
            $Quiz->attempt = $quiz_library->attempt;
            $Quiz->quizz_type = $quiz_library->quizz_type;
            $Quiz->id_section = $section_id;
            $Quiz->status = 1; // 1= publish, 2 =draft
            $Quiz->save();
            // insert Quiz

            if($quiz_library->pagebreak){
              foreach ($quiz_library->pagebreak as $key => $value) {
                $QuizPageBreak = new QuizPageBreak;
                $QuizPageBreak->quiz_id = $Quiz->id;
                $QuizPageBreak->title = $value->title;
                $QuizPageBreak->description = $value->description;
                $QuizPageBreak->save();
                // code...
              }
            }

            if($quiz_library->question){
              foreach ($quiz_library->question as $key => $value) {
                $QuizQuestion = new QuizQuestion;

                $QuizQuestion->quiz_type_id = $value->quiz_library_type_id;
                $QuizQuestion->question = $value->question;
                $QuizQuestion->weight = $value->weight ? $value->weight : 0;
                $QuizQuestion->quiz_id = $Quiz->id;
                $QuizQuestion->save();

                $QuizQuestion_id = $QuizQuestion->id;
                // insert Quiz Question

                // inser quiz question answer
                $answers = $value->quiz_question_answers;
                if($answers){
                  foreach($answers as $index => $value_answer){
                    if($value_answer != NULL){
                      $QuizQuestionAnswer = new QuizQuestionAnswer;
                      $QuizQuestionAnswer->answer = $value_answer->answer;
                      if($value->quiz_library_type_id == '3'){ // ordering type
                        $QuizQuestionAnswer->answer_ordering = $value_answer->answer_ordering;
                      }else{
                        $QuizQuestionAnswer->answer_correct = $value_answer->answer_correct;
                      }
                      $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                      $QuizQuestionAnswer->save();
                    }
                  }
                }
                // $QuizPageBreak->save();
                // code...
              }
            }
          }else{
              $content_library = Content::find($content[0]);
            // $Content = Content::where('title', $content_library->title)->where('id_section', $section_id)->first();
            // if(!$Content){
              $folder = get_folder_course($section_id);
              $Content = new Content;
              $Content->title = $content_library->title;
              $Content->description = $content_library->description;
              $Content->type_content = $content_library->type_content;
              $Content->name_file = $content_library->name_file;
              $Content->path_file = $content_library->path_file;
              $Content->file_size = $content_library->file_size;
              $Content->video_duration = $content_library->video_duration ? $content_library->video_duration : 0;
              $Content->full_path_file = $content_library->full_path_file;
              $Content->full_path_original = $content_library->full_path_original;

              if($request->type_content == 'video'){
                $Content->full_path_file_resize = get_asset_path($folder, '240-'.$content_library->name_file);
              }

              $Content->id_section = $section_id;
              $Content->status = '1';
              $Content->save();
            // }
          }
        }
      }
      // insert content

      // $destinationPath = asset_path('uploads/contents/'); // upload path
      // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
      // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
      // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
      // $folder = get_folder_course($section_id); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $name_file);


      // $Content->sequence = $request->sequence;


      // insert content

      \Session::flash('success', 'Create content success');
      // return redirect('course/preview/'.$course_id);
      return redirect()->back();
    }

    public function add_content(Request $request, $course_id, $section_id){

      if($request->content){
        foreach ($request->content as $key => $value) {
          $content = explode('/', $value);
          if($content[1] == 'Tugas'){
            $assig = LibraryAssignment::find($content[0]);
            $section = Section::where('id', $section_id)->first();

            // insert Assignment
            $Assignment = new Assignment;
            $Assignment->title = $assig->title;
            $Assignment->description = $assig->description;
            $Assignment->type = $assig->type;
            $Assignment->attempt = $assig->attempt;
            $Assignment->time_start = $assig->time_start;
            $Assignment->time_end = $assig->time_end;
            $Assignment->id_section = $section_id;
            $Assignment->save();
          }else if($content[1] == 'Quiz'){
            $quiz_library = LibraryQuiz::with('pagebreak', 'question.quiz_question_answers')->find($content[0]);
            // insert Quiz
            $Quiz = new Quiz;

            $Quiz->name = $quiz_library->name;
            $Quiz->description = $quiz_library->description;
            $Quiz->slug = str_slug($quiz_library->name);
            // $Quiz->shuffle = $request->shuffle;
            $Quiz->time_start = $quiz_library->time_start;
            $Quiz->time_end =$quiz_library->time_end;
            $Quiz->duration = $quiz_library->duration;
            $Quiz->quizz_type = $quiz_library->quizz_type;
            $Quiz->attempt = $quiz_library->attempt;
            $Quiz->id_section = $section_id;
            $Quiz->status = 1; // 1= publish, 2 =draft
            $Quiz->save();
            // insert Quiz

            if($quiz_library->pagebreak){
              foreach ($quiz_library->pagebreak as $key => $value) {
                $QuizPageBreak = new QuizPageBreak;
                $QuizPageBreak->quiz_id = $Quiz->id;
                $QuizPageBreak->title = $value->title;
                $QuizPageBreak->description = $value->description;
                $QuizPageBreak->save();
                // code...
              }
            }

            if($quiz_library->question){
              foreach ($quiz_library->question as $key => $value) {
                $QuizQuestion = new QuizQuestion;

                $QuizQuestion->quiz_type_id = $value->quiz_library_type_id;
                $QuizQuestion->question = $value->question;
                $QuizQuestion->weight = $value->weight ? $value->weight : 0;
                $QuizQuestion->quiz_id = $Quiz->id;
                $QuizQuestion->save();

                $QuizQuestion_id = $QuizQuestion->id;
                // insert Quiz Question

                // inser quiz question answer
                $answers = $value->quiz_question_answers;
                if($answers){
                  foreach($answers as $index => $value_answer){
                    if($value_answer != NULL){
                      $QuizQuestionAnswer = new QuizQuestionAnswer;
                      $QuizQuestionAnswer->answer = $value_answer->answer;
                      if($value->quiz_library_type_id == '3'){ // ordering type
                        $QuizQuestionAnswer->answer_ordering = $value_answer->answer_ordering;
                      }else{
                        $QuizQuestionAnswer->answer_correct = $value_answer->answer_correct;
                      }
                      $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                      $QuizQuestionAnswer->save();
                    }
                  }
                }
                // $QuizPageBreak->save();
                // code...
              }
            }
          }else{
            $content_library = UserLibrary::find($value);
            $Content = Content::where('title', $content_library->title)->first();
            if(!$Content){
              $folder = get_folder_course($section_id);
              $Content = new Content;
              $Content->title = $content_library->title;
              $Content->description = $content_library->description;
              $Content->type_content = $content_library->type_content;
              $Content->name_file = $content_library->name_file;
              $Content->path_file = $content_library->path_file;
              $Content->file_size = $content_library->file_size;
              $Content->video_duration = $content_library->video_duration ? $content_library->video_duration : 0;
              $Content->full_path_file = $content_library->full_path_file;
              $Content->full_path_original = $content_library->full_path_original;

              if($request->type_content == 'video'){
                $Content->full_path_file_resize = get_asset_path($folder, '240-'.$content_library->name_file);
              }

              $Content->id_section = $section_id;
              $Content->status = '1';
              $Content->save();
            }
          }
        }
      }
      // insert content

      // $destinationPath = asset_path('uploads/contents/'); // upload path
      // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
      // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
      // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
      // $folder = get_folder_course($section_id); // get folder course by section id
      // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
      // $path_file = get_asset_path($folder, $name_file);


      // $Content->sequence = $request->sequence;


      // insert content

      \Session::flash('success', 'Create content success');
      // return redirect('course/preview/'.$course_id);
      return redirect()->back();
    }

    public function update($id){
      $Content = Content::where('id', $id)->first();
      $section = Section::where('id', $Content->id_section)->first();

      $data = [
        'action' => 'course/content/update_action/',
        'method' => 'post',
        'button' => 'Update',
        'id' => old('id', $Content->id),
        'title' => old('title', $Content->title),
        'description' => old('description', $Content->description),
        'type_content' => old('type_content', $Content->type_content),
        'name_file' => old('name_file', $Content->name_file),
        'path_file' => old('path_file', $Content->path_file),
        'full_path_file' => old('full_path_file', $Content->full_path_file),
        'file_size' => old('file_size', $Content->file_size),
        'video_duration' => old('video_duration', $Content->video_duration),
        'sequence' => old('sequence', $Content->sequence),
        'section' => $section,
      ];
      return view('course.content_form', $data);
    }

    public function library_delete(Request $request){
        $warning = '';
        $User = Auth::user();
        foreach($request->datas as $data){
            $UserLibrary = DB::table('userlibraries')->where(['id' => $data['data'], 'user_id' => $User->id])->first();
            if(!empty($UserLibrary)){
                // $fromContent = DB::table('contents')->where(['name_file' => $UserLibrary->name_file])->first();
                // if ($UserLibrary->path_file!=null && empty($fromContent)) {
                //     Storage::disk('google')->delete($UserLibrary->path_file);
                // }
                // if ($UserLibrary->path_file==null) {
                // // Has file?
                //     DB::table('userlibraries')->where(['parent' => $UserLibrary->name_file])->delete();
                // }
                DB::table('userlibraries')->where(['id' => $data['data']])->delete();
            }else{
                $SharedUserLibrary = ShareContent::where(['user_id' => $User->id, 'content_lib_id' => $data['data'], 'isWriteable' => '1'])->first();
                if (!empty($SharedUserLibrary)) {
                    DB::table('userlibraries')->where(['id' => $SharedUserLibrary->content_lib_id])->delete();
                }else{
                    $warning = 'Beberapa konten tidak dihapus karena akses dibatasi';
                }
            }
        }
        if ($warning!='') {
            return response()->json([
                'message' => 'success',
                'warning' => $warning,
            ], 200);
        }
        return response()->json([
            'message' => 'success',
        ], 200);
    }

    public function library_create_folder(Request $request){
        $UserLibrary = new UserLibrary;
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->title = $request->title;
        $UserLibrary->name_file = str_slug($request->title) . '-' . date("Y-m-d") . '-' . rand(100,999);
        $UserLibrary->parent = $request->parent;
        $UserLibrary->is_directory = '1';
        $UserLibrary->save();

        return response()->json([
        'message' => 'success'
        ], 200);
    }

    public function pick_from_library($section_id){
        $data = [
            'section_id' => $section_id,
        ];
        return view('course.library_get_content', $data);
    }

    public function store_to_quiz(Request $request){
        $UserLibrary = UserLibrary::where(['id' => $request->id])->first();
        if (empty($UserLibrary)) {
            return response()->json([
                'message' => 'Bad Request',
                'error' => 'Content not Found'
            ]);
        }
        $isQuizExist = DB::table('quizzes')->where(['slug' => $UserLibrary->name_file, 'id_section' => $request->section_id])->first();
        if (empty($isQuizExist)) {
            $Quiz = new Quiz;

            $Quiz->name = $UserLibrary->title;
            $Quiz->description = $UserLibrary->description;
            $Quiz->slug = $UserLibrary->name_file;
            $Quiz->id_section = $request->section_id;
            $Quiz->save();

            $LibraryQuizQuestion = LibraryQuizQuestion::where(['quiz_library_id' => $UserLibrary->id])->first();

            $QuizQuestion = new QuizQuestion;

            $QuizQuestion->quiz_type_id = $LibraryQuizQuestion->quiz_library_type_id;
            $QuizQuestion->question = $LibraryQuizQuestion->question;
            $QuizQuestion->weight = $LibraryQuizQuestion->weight;
            $QuizQuestion->quiz_id = $Quiz->id;
            $QuizQuestion->save();
            // insert Quiz Question

            // inser quiz question answer
            $LibraryQuizAnswers = LibraryQuizQuestionAnswer::where(['quiz_question_id' => $LibraryQuizQuestion->id])->get();
            if (!empty($LibraryQuizAnswers)) {
                foreach($LibraryQuizAnswers as $LibraryQuizAnswer){
                    $QuizQuestionAnswer = new QuizQuestionAnswer;
                    $QuizQuestionAnswer->answer = $LibraryQuizAnswer->answer;
                    $QuizQuestionAnswer->answer_correct = $LibraryQuizAnswer->answer_correct;
                    $QuizQuestionAnswer->quiz_question_id = $QuizQuestion->id;
                    $QuizQuestionAnswer->save();
                }
            }
            return response()->json([
                'message' => 'Success',
                'data' => [
                    'quiz_id' => $Quiz->id
                ]
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'data' => [
                'quiz_id' => $isQuizExist->id
            ]
        ]);

        // insert Quiz
    }

    public function store_to_content(Request $request){
        $UserLibrary = UserLibrary::where(['id' => $request->id])->first();
        if (empty($UserLibrary)) {
            return response()->json([
                'message' => 'Bad Request',
                'error' => 'Content not Found'
            ]);
        }
        $course_id = DB::table('sections')->where(['id' => $request->section_id])->first();
        $isContentExist = DB::table('contents')->where(['name_file' => $UserLibrary->name_file, 'id_section' => $request->section_id])->first();
        if (empty($isContentExist)) {
            $Content = new Content;
            $Content->id_section = $request->section_id;
            $Content->title = $UserLibrary->title;
            $Content->description = $UserLibrary->description;
            $Content->type_content = $UserLibrary->type_content;
            $Content->name_file = $UserLibrary->name_file;
            $Content->path_file = $UserLibrary->path_file;
            $Content->full_path_file = $UserLibrary->full_path_file;
            $Content->full_path_file_resize = $UserLibrary->full_path_file_resize;
            $Content->full_path_file_resize_720 = $UserLibrary->full_path_file_resize_720;
            $Content->full_path_original = $UserLibrary->full_path_original;
            $Content->full_path_original_resize = $UserLibrary->full_path_original;
            $Content->full_path_original_resize_720 = $UserLibrary->full_path_original_resize_720;
            $Content->file_size = $UserLibrary->file_size;
            $Content->video_duration = $UserLibrary->video_duration;
            $Content->save();
            return response()->json([
                'message' => 'Success',
                'data' => [
                    'course_id' => $course_id->id_course
                ]
            ]);
        }
        return response()->json([
            'message' => 'File Already Exists',
            'data' => [
                'course_id' => $course_id->id_course
            ]
        ]);
    }

    public function store_draft(Request $request){
        $field = $request->field;
        $value = $request->value;
        $UserLibrary = UserLibrary::where(['id' => $request->id])->first();

        if ($UserLibrary) {
            $UserLibrary->type_content = $request->type_content;
            if($request->file){ // jika ada inputan file
                $UserLibrary->name_file = $request->name_file;
                $UserLibrary->path_file = $request->path_file;
                if($UserLibrary->type_content == 'video'){ // jika inputan file berupa video
                    $UserLibrary->file_size = $request->file_size;
                    $UserLibrary->video_duration = $request->video_duration;
                    $UserLibrary->full_path_original = $request->full_path_original;
                    $UserLibrary->save();
                    // exec resize video
                    //file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
                    // exec resize video

                }elseif($UserLibrary->type_content == 'file'){
                    $UserLibrary->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
                    $UserLibrary->save();
                }
            }else{
                $UserLibrary->$field = $value;
                if ($UserLibrary->type_content == 'text') {
                    if ($UserLibrary->title!=null) {
                        $UserLibrary->name_file = str_slug($UserLibrary->title) . '_' . $UserLibrary->id;
                    }
                }
                $UserLibrary->save();
            }
        }else{
            $UserLibrary = new UserLibrary;
            $UserLibrary->type_content = $request->type_content;
            $UserLibrary->user_id = $request->user_id;
            if($request->file){  // jika ada inputan file
                $UserLibrary->name_file = $request->name_file;
                $UserLibrary->path_file = $request->path_file;
                if($UserLibrary->type_content == 'video'){ // jika inputan file berupa video
                    $UserLibrary->file_size = $request->file_size;
                    $UserLibrary->video_duration = $request->video_duration;
                    $UserLibrary->full_path_original = $request->full_path_original;
                    $UserLibrary->save();

                    // exec resize video
                    //file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
                    // exec resize video

                }elseif($UserLibrary->type_content == 'file'){
                    $UserLibrary->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
                    $Content->save();
                }
            }else{
                $UserLibrary->$field = $value;
                if ($UserLibrary->type_content == 'text') {
                    if ($UserLibrary->title!=null) {
                        $UserLibrary->name_file = str_slug($UserLibrary->title) . '_' . $UserLibrary->id;
                    }
                }
                $UserLibrary->save();
            }
        }

        return response()->json($UserLibrary);
    }

    public function library_upload($user_id){
        return Plupload::receive('file', function ($file) use ($user_id)
        {
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $size_file = $file->getSize(); // getting image extension
            $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image

            $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt'];
            if(in_array(strtolower($extension), $video_extensions)){

                $folder = get_folderName_user($user_id); // get folder course by user id
                Storage::disk('sftp')->put(server_user_assets_path($folder) .'/'. $name_file, fopen($file, 'r+')); //upload google drive
                $video_duration = 0;

                $ffprobe = FFMpeg\FFProbe::create();
                $video_duration = $ffprobe
                    ->format($file) // extracts file informations
                    ->get('duration');

                return response()->json([
                    'original_file_name' => $file->getClientOriginalName(),
                    'path' => '',
                    'name' => $name_file,
                    'size' => $size_file,
                    'video_duration' => $video_duration,
                    'full_path_original' => '/uploads/videos/' . $folder .'/'. $name_file,
                    'resize_path' => '',
                    'type_content' => 'video'
                ]);
            }else{
                $folder = get_user_folder_path($user_id); // get folder course by section id
                Storage::disk('google')->put($folder.'/'.$name_file, fopen($file, 'r+'));//upload google drive
                $path_file = get_asset_user_path($folder, $name_file);
                return response()->json([
                    'original_file_name' => $file->getClientOriginalName(),
                    'path' => $path_file,
                    'name' => $name_file,
                    'type_content' => 'file'
                ]);
            }
        });
    }

    //======= For Library Quiz Controller (you can manage, add, or remove another quiz you want)===
    public function library_quiz_create($user_id){
        $data = [
            'action' => 'library/quiz/create/'.$user_id,
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'name' => old('name'),
            'description' => old('description'),
            'user_id' => $user_id
        ];
        return view('course.library_quiz_form', $data);
    }

    public function library_quiz_create_action($user_id, Request $request){
        // insert Quiz to Quiz Library Table
        $UserLibrary = new UserLibrary;

        $UserLibrary->title = $request->name;
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->description = $request->description;
        $UserLibrary->name_file = str_slug($request->name);
        $UserLibrary->type_content = 'quiz';
        $UserLibrary->parent = '.';
        $UserLibrary->save();

        return redirect('instructor/library/quiz/question/manage/'.$UserLibrary->id);
        // insert Quiz to Quiz Library Table
    }

    public function library_quiz_update($user_id, $content_lib_id){
        $UserLibrary = UserLibrary::where(['id' => $content_lib_id])->first();
        $User = User::where(['id' => $user_id])->first();
        $data = [
            'action' => 'library/quiz/update/'.$user_id.'/'.$content_lib_id,
            'method' => 'post',
            'button' => 'Update',
            'id' => old('id', $content_lib_id),
            'name' => old('name', $UserLibrary->title),
            'description' => old('description', $UserLibrary->description),
            'user_id' => $user_id
        ];
        return view('course.library_quiz_form', $data);
    }

    public function library_quiz_update_action($user_id, $content_lib_id, Request $request){
        $UserLibrary = UserLibrary::where(['id' => $content_lib_id])->first();

        $UserLibrary->title = $request->name;
        $UserLibrary->user_id = $request->user_id;
        $UserLibrary->description = $request->description;
        $UserLibrary->name_file = str_slug($request->name);
        $UserLibrary->type_content = 'quiz';
        $UserLibrary->parent = '.';
        $UserLibrary->save();

        return redirect('instructor/library');
    }

    public function library_quiz_question_manage($id){
        $UserLibrary = UserLibrary::where('id', $id)->first();
        $quiz_questions = LibraryQuizQuestion::
                        select('library_quiz_questions.*', 'quiz_types.type')
                        ->where('quiz_library_id', $id)
                        ->join('quiz_types','quiz_types.id' ,'=','library_quiz_questions.quiz_library_type_id')
                        ->get();
        $quiz_questions_answers = [];
        foreach($quiz_questions as $quiz_question){
            array_push($quiz_questions_answers, array(
                'question_answers' => LibraryQuizQuestionAnswer::where('quiz_question_id', $quiz_question->id)->get(),
                'question' => $quiz_question->question,
                'weight' => $quiz_question->weight,
                'type' => $quiz_question->type,
                'id' => $quiz_question->id,
            ));
        }

        $data = [
            'quiz' => $UserLibrary,
            'quiz_types' => QuizType::get(),
            'quiz_questions_answers' => $quiz_questions_answers,
        ];
        // return response()->json([
        //     'data' => $data
        // ], 200);
        return view('course.library_quiz_question_manage', $data);
    }

    public function library_quiz_question_create($id){
        $UserLibrary = UserLibrary::where('id', $id)->first();
        $quiz_questions = LibraryQuizQuestion::
                        select('library_quiz_questions.*', 'quiz_types.type')
                        ->where('quiz_library_id', $id)
                        ->join('quiz_types','quiz_types.id' ,'=','library_quiz_questions.quiz_library_type_id')
                        ->get();
        $quiz_questions_answers = [];
        foreach($quiz_questions as $quiz_question){
            array_push($quiz_questions_answers, array(
                'question_answers' => LibraryQuizQuestionAnswer::where('quiz_question_id', $quiz_question->id)->get(),
                'question' => $quiz_question->question,
                'weight' => $quiz_question->weight,
                'type' => $quiz_question->type,
                'id' => $quiz_question->id,
            ));
        }

        $data = [
            'action' => 'library/quiz/question/create/'.$UserLibrary->id,
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'quiz_type_id' => old('quiz_type_id'),
            'question' => old('question'),
            'weight' => old('weight','0'),
            'quiz' => $UserLibrary,
            'quiz_types' => QuizType::get(),
            'quiz_questions_answers' => $quiz_questions_answers,
        ];
        return view('course.library_quiz_question_form', $data);
    }

    public function library_quiz_question_create_action($quiz_id, Request $request){
        // insert Quiz Question
        $QuizQuestion = new LibraryQuizQuestion;

        $QuizQuestion->quiz_library_type_id = $request->quiz_type_id;
        $QuizQuestion->question = $request->question;
        $QuizQuestion->weight = $request->weight;
        $QuizQuestion->quiz_library_id = $quiz_id;
        $QuizQuestion->save();
        $QuizQuestion_id = $QuizQuestion->id;
        // insert Quiz Question

        // inser quiz question answer
        $answers = $request->answer;
        foreach($answers as $index => $value){
            if($value != NULL){
                $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                $QuizQuestionAnswer->answer = $value;
                $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
                $QuizQuestionAnswer->quiz_question_id = $QuizQuestion_id;
                $QuizQuestionAnswer->save();
            }
        }
        // inser quiz question answer

        \Session::flash('success', 'Create record success');
        return redirect('instructor/library/quiz/question/manage/'.$quiz_id);
    }

    public function library_quiz_question_update($quiz_id, $ques_id){
        $QuizQuestion = LibraryQuizQuestion::where('id', $ques_id)->first();
        $quiz = UserLibrary::where('id', $quiz_id)->first();

        $data = [
            'action' => 'library/quiz/question/update/'.$quiz_id,
            'method' => 'put',
            'button' => 'Update',
            'id' => old('id', $QuizQuestion->id),
            'quiz_type_id' => old('quiz_type_id', $QuizQuestion->quiz_library_type_id),
            'question' => old('question', $QuizQuestion->question),
            'weight' => old('weight', $QuizQuestion->weight),
            'quiz_types' => QuizType::get(),
            'answers' => LibraryQuizQuestionAnswer::where('quiz_question_id', $ques_id)->get(),
            'quiz' => $quiz,
        ];
        return view('course.library_quiz_question_update', $data);
    }

    public function library_quiz_question_update_action($quiz_id, Request $request){
        $Quiz = UserLibrary::where('id', $quiz_id)->first();

        // update Quiz question
        $QuizQuestion = LibraryQuizQuestion::where('id', $request->id)->first();
        $QuizQuestion->quiz_library_type_id = $request->quiz_type_id;
        $QuizQuestion->question = $request->question;
        $QuizQuestion->weight = $request->weight;
        $QuizQuestion->save();
        // update Quiz question

        \Session::flash('success', 'Update record success');
        return redirect('instructor/library/quiz/question/manage/'.$quiz_id);
    }

    public function library_quiz_question_delete($quiz_id, $id){
        $QuizQuestion = LibraryQuizQuestion::where('id', $id);
        $QuizQuestionAnswer = LibraryQuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->first()->id);
        if($QuizQuestion){
            $QuizQuestionAnswer->delete();
            $QuizQuestion->delete();
        \Session::flash('success', 'Delete record success');
        }else{
        \Session::flash('error', 'Delete record failed');
        }
        return redirect('instructor/library/quiz/question/manage/'.$quiz_id);
    }

    public function library_quiz_question_import($quiz_id, Request $request){
        // file
        $destinationPath = asset_path('uploads/quiz/'); // upload path
        $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
        $file = rand(11111,999999).'.'.$extension; // renameing image
        $request->file('file')->move($destinationPath, $file); // uploading file to given path
        // file

        Excel::load(asset_path('uploads/quiz/'.$file), function($reader) use ($quiz_id) {
        $results = $reader->get();
        // dd($results);
        foreach($results as $row){
            // dd($row->correct);

            // create quiz question
            $Quiz_question = new LibraryQuizQuestion;
            $Quiz_question->quiz_library_id = $quiz_id;
            $Quiz_question->quiz_library_type_id = '1';
            $Quiz_question->question = $row->soal;
            $Quiz_question->weight = '10';
            $Quiz_question->save();
            // create quiz question

            for ($char = 'a'; $char <= 'e'; $char++) {
                if($row->$char != ""){
                    // create quiz question
                    $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                    $QuizQuestionAnswer->quiz_question_id = $Quiz_question->id;
                    $QuizQuestionAnswer->answer = $row->$char;
                    $QuizQuestionAnswer->answer_correct = $row->correct_answer == strtoupper($char) ? '1' : '0' ;
                    $QuizQuestionAnswer->save();
                    // create quiz question
                }
            }
        }
        });

        \Session::flash('success', 'Quiz import successfull');
        return redirect('instructor/library/quiz/question/manage/'. $quiz_id);
    }

    // Answer Question
    public function library_quiz_answer_create(Request $request){
    // inser quiz question answer
        $answers = $request->answer;
        foreach($answers as $index => $value){
            if($value != NULL){
                $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                $QuizQuestionAnswer->answer = $value;
                $QuizQuestionAnswer->answer_correct = $request->answer_correct[$index];
                $QuizQuestionAnswer->quiz_question_id = $request->quiz_question_id;
                $QuizQuestionAnswer->save();
            }
        }
        // inser quiz question answer
        $QuizQuestion = LibraryQuizQuestion::where('id', $request->quiz_question_id)->first();
        \Session::flash('success', 'Create reccord success');
        return redirect('instructor/library/quiz/question/update/'.$QuizQuestion->quiz_library_id.'/'.$QuizQuestion->id);
    }

    public function library_quiz_answer_update_action(Request $request){
        $QuizQuestionAnswer = LibraryQuizQuestionAnswer::where('id', $request->id)->first();
        $QuizQuestionAnswer->answer = $request->answer;
        $QuizQuestionAnswer->answer_correct = $request->answer_correct;
        $QuizQuestionAnswer->save();
    }

    public function library_quiz_answer_delete($quiz_id, $id){
        $QuizQuestionAnswer = LibraryQuizQuestionAnswer::where('id', $id);
        if($QuizQuestionAnswer){
            $QuizQuestionAnswer->delete();
            \Session::flash('success', 'Delete reccord success');
        }else{
            \Session::flash('error', 'Delete reccord failed');
        }
        return redirect('instructor/library/quiz/question/manage/'.$quiz_id);
    }
    // Answer Question

    public function load_question_quiz($id){
        $data = [];
        $Questions = LibraryQuizQuestion::where('quiz_library_id', '=', $id)->get();
        foreach ($Questions as $Question) {
            $AnswerQuestions = LibraryQuizQuestionAnswer::where('quiz_question_id', '=', $Question->id)->get();
            array_push($data, [
                'id' => $Question->id,
                'quiz_library_id' => $Question->quiz_library_id,
                'quiz_library_type_id' => $Question->quiz_library_type_id,
                'question' => $Question->question,
                'weight' => $Question->weight,
                'created_at' => $Question->created_at,
                'answers' => $AnswerQuestions,
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'data' => $data,
        ], 200);
    }

    public function show_quiz_library($id){
        $data = [
            'id' => $id,
            'target' => 'library'
        ];
        return view('course.library_get_question', $data);
    }

    public function show_quiz_library_course($id){
        $data = [
            'id' => $id,
            'target' => 'course'
        ];
        return view('course.library_get_question', $data);
    }

    public function submit_to_course_quiz(Request $request){
        $datas = $request->datas;
        $library_quiz_id = $request->id;

        foreach ($datas as $data) {
            $LibraryQuizQuestion = LibraryQuizQuestion::where('id', '=', $data['data'])->first();

            $Quiz_question = new QuizQuestion;
            $Quiz_question->quiz_id = $library_quiz_id;
            $Quiz_question->quiz_type_id = $LibraryQuizQuestion->quiz_library_type_id;
            $Quiz_question->question = $LibraryQuizQuestion->question;
            $Quiz_question->weight = $LibraryQuizQuestion->weight;
            $Quiz_question->save();

            $LibraryQuizQuestionAnswers = LibraryQuizQuestionAnswer::where('quiz_question_id', '=', $LibraryQuizQuestion->id)->get();
            if (!empty($LibraryQuizQuestionAnswers)) {
                foreach ($LibraryQuizQuestionAnswers as $LibraryQuizQuestionAnswer) {
                    $QuizQuestionAnswer = new QuizQuestionAnswer;
                    $QuizQuestionAnswer->answer = $LibraryQuizQuestionAnswer->answer;
                    $QuizQuestionAnswer->answer_correct = $LibraryQuizQuestionAnswer->answer_correct;
                    $QuizQuestionAnswer->quiz_question_id = $Quiz_question->id;
                    $QuizQuestionAnswer->save();
                }
            }
        }

        return response()->json([
            'message' => 'Success',
            'quiz_id' => $library_quiz_id,
        ], 200);
    }




    public function submit_to_library_quiz(Request $request){
        $datas = $request->datas;
        $library_quiz_id = $request->id;

        foreach ($datas as $data) {
            $LibraryQuizQuestion = LibraryQuizQuestion::where('id', '=', $data['data'])->first();

            $Quiz_question = new LibraryQuizQuestion;
            $Quiz_question->quiz_library_id = $library_quiz_id;
            $Quiz_question->quiz_library_type_id = $LibraryQuizQuestion->quiz_library_type_id;
            $Quiz_question->question = $LibraryQuizQuestion->question;
            $Quiz_question->weight = $LibraryQuizQuestion->weight;
            $Quiz_question->save();

            $LibraryQuizQuestionAnswers = LibraryQuizQuestionAnswer::where('quiz_question_id', '=', $LibraryQuizQuestion->id)->get();
            if (!empty($LibraryQuizQuestionAnswers)) {
                foreach ($LibraryQuizQuestionAnswers as $LibraryQuizQuestionAnswer) {
                    $QuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                    $QuizQuestionAnswer->answer = $LibraryQuizQuestionAnswer->answer;
                    $QuizQuestionAnswer->answer_correct = $LibraryQuizQuestionAnswer->answer_correct;
                    $QuizQuestionAnswer->quiz_question_id = $Quiz_question->id;
                    $QuizQuestionAnswer->save();
                }
            }
        }

        return response()->json([
            'message' => 'Success',
            'quiz_id' => $library_quiz_id,
        ], 200);
    }


    // End of Another Library Quiz Controller (you can manage, add, or remove another quiz you want)

    public function save_as_course_library($content_id){
        $Content = DB::table('contents')->where('id', '=', $content_id)->first();

        $isContentExist = UserLibrary::where('name_file', '=', $Content->name_file)->first();

        if (!empty($isContentExist)) {
            \Session::flash('success', 'Data already saved to Library');
            return redirect()->back();
        }

        $UserLibrary = new UserLibrary;
        $UserLibrary->user_id = Auth::user()->id;
        $UserLibrary->title = $Content->title;
        $UserLibrary->description = $Content->description;
        $UserLibrary->type_content = $Content->type_content;
        $UserLibrary->name_file = $Content->name_file;
        $UserLibrary->path_file = $Content->path_file;
        $UserLibrary->full_path_file = $Content->full_path_file;
        $UserLibrary->full_path_file_resize = $Content->full_path_file_resize;
        $UserLibrary->full_path_file_resize_720 = $Content->full_path_file_resize_720;
        $UserLibrary->full_path_original = $Content->full_path_original;
        $UserLibrary->full_path_original_resize = $Content->full_path_original;
        $UserLibrary->full_path_original_resize_720 = $Content->full_path_original_resize_720;
        $UserLibrary->file_size = $Content->file_size;
        $UserLibrary->video_duration = $Content->video_duration;
        $UserLibrary->save();

        \Session::flash('success', 'Saved to Library');
        return redirect()->back();

    }

    public function save_as_quiz_library($quiz_id){
        $Content = DB::table('quizzes')->where('id', '=', $quiz_id)->first();

        $isContentExist = UserLibrary::where('name_file', '=', $Content->slug)->first();

        if (!empty($isContentExist)) {
            \Session::flash('success', 'Data already saved to Library');
            return redirect()->back();
        }

        $UserLibrary = new UserLibrary;
        $UserLibrary->user_id = Auth::user()->id;
        $UserLibrary->title = $Content->name;
        $UserLibrary->description = $Content->description;
        $UserLibrary->type_content = 'quiz';
        $UserLibrary->name_file = $Content->slug;
        $UserLibrary->save();

        $QuizQuestions = QuizQuestion::where('quiz_id', '=', $quiz_id)->get();
        foreach ($QuizQuestions as $QuizQuestion) {
            $Quiz_question = new LibraryQuizQuestion;
            $Quiz_question->quiz_library_id = $UserLibrary->id;
            $Quiz_question->quiz_library_type_id = $QuizQuestion->quiz_type_id;
            $Quiz_question->question = $QuizQuestion->question;
            $Quiz_question->weight = $QuizQuestion->weight;
            $Quiz_question->save();

            $QuizQuestionAnswers = QuizQuestionAnswer::where('quiz_question_id', '=', $QuizQuestion->id)->get();
            if (!empty($QuizQuestionAnswers)) {
                foreach ($QuizQuestionAnswers as $QuizQuestionAnswer) {
                    $LibraryQuizQuestionAnswer = new LibraryQuizQuestionAnswer;
                    $LibraryQuizQuestionAnswer->answer = $QuizQuestionAnswer->answer;
                    $LibraryQuizQuestionAnswer->answer_correct = $QuizQuestionAnswer->answer_correct;
                    $LibraryQuizQuestionAnswer->quiz_question_id = $Quiz_question->id;
                    $LibraryQuizQuestionAnswer->save();
                }
            }
        }
        \Session::flash('success', 'Saved to Library');
        return redirect()->back();
    }

    public function library_update($user_id, $content_lib_id){
        $UserLibrary = UserLibrary::where(['id' => $content_lib_id])->first();
        $User = User::where(['id' => $user_id])->first();
        $data = [
            'action' => 'library/content/update/'.$user_id.'/'.$content_lib_id,
            'method' => 'post',
            'button' => 'Update',
            'id' => old('id', $content_lib_id),
            'title' => old('title', $UserLibrary->title),
            'description' => old('description', $UserLibrary->description),
            'type_content' => old('type_content', $UserLibrary->type_content),
            'name_file' => old('name_file', $UserLibrary->name_file),
            'path_file' => old('path_file', $UserLibrary->path_file),
            'file_size' => old('file_size', $UserLibrary->file_size),
            'video_duration' => old('video_duration', $UserLibrary->video_duration),
            'full_path_file' => old('full_path_file', $UserLibrary->full_path_file),
            'user_id' => $user_id
        ];
        return view('course.library_form', $data);
    }

    public function library_update_action(Request $request){
        // insert content
        // $folder = get_folderName_user($user_id);
        $UserLibrary = UserLibrary::where(['id' => $request->id])->first();

        // $destinationPath = asset_path('uploads/contents/'); // upload path
        // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
        // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
        // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
        // $folder = get_folder_course($section_id); // get folder course by section id
        // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
        // $path_file = get_asset_path($folder, $name_file);

        $UserLibrary->title = $request->title;
        $UserLibrary->description = $request->description;
        $UserLibrary->type_content = $request->type_content;
        // $Content->sequence = $request->sequence;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->file_size = $request->file_size;
        $UserLibrary->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
        if($request->type_content == 'video'){
            $UserLibrary->full_path_file_resize = get_asset_path($folder, '240-'.$request->name_file);
        }
        $UserLibrary->save();
        return redirect('instructor/library');
    }



    public function library_create($user_id){
        $data = [
            'action' => 'library/content/create/'.$user_id,
            'method' => 'post',
            'button' => 'Create',
            'id' => old('id'),
            'title' => old('title'),
            'description' => old('description'),
            'type_content' => old('type_content'),
            'name_file' => old('name_file'),
            'path_file' => old('path_file'),
            'file_size' => old('file_size'),
            'video_duration' => old('video_duration'),
            'full_path_file' => old('full_path_file'),
            'user_id' => $user_id
        ];
        return view('course.library_form', $data);
    }


    public function library_create_action($user_id, Request $request){
        // insert content
        $folder = get_folderName_user($user_id);
        $UserLibrary = new UserLibrary;

        // $destinationPath = asset_path('uploads/contents/'); // upload path
        // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
        // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
        // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
        // $folder = get_folder_course($section_id); // get folder course by section id
        // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
        // $path_file = get_asset_path($folder, $name_file);

        $UserLibrary->title = $request->title;
        $UserLibrary->description = $request->description;
        $UserLibrary->type_content = $request->type_content;
        // $Content->sequence = $request->sequence;
        $UserLibrary->name_file = $request->name_file;
        $UserLibrary->path_file = $request->path_file;
        $UserLibrary->file_size = $request->file_size;
        $UserLibrary->video_duration = $request->video_duration;
        $UserLibrary->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
        if($request->type_content == 'video'){
            $UserLibrary->full_path_file_resize = get_asset_path($folder, '240-'.$request->name_file);
        }
        $UserLibrary->parent = '.';
        $UserLibrary->save();
        return redirect('instructor/library');
    }

    public function instructor_get(){
        $data = [];
        $User = Auth::user();
        $Instructors = DB::table('instructors')
            ->select('users.email')
            ->join('users', 'users.id', '=', 'instructors.user_id')
            ->where(['instructors.status' => '1'])
            ->where('users.id', '<>', $User->id)
            ->get();
        foreach ($Instructors as $Instructor) {
            array_push($data, $Instructor->email);
        }
        return response()->json([
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    public function shared_content($content_lib_id){
        $data = [];
        $User = Auth::user();
        $Shareds = DB::table('users')
            ->select('users.*')
            ->join('share_contents', 'share_contents.user_id', '=', 'users.id')
            ->where('share_contents.content_lib_id', '=', $content_lib_id)
            ->get();
        foreach ($Shareds as $Shared) {
            array_push($data, [
                'name' => $Shared->email==$User->email ? 'Anda' : $Shared->name,
                'email' => $Shared->email
            ]);
        }
        return response()->json([
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    public function add_share_content(Request $request){
        $Emails = $request->emails;
        foreach ($Emails as $key=>$value) {
            $User = User::where(['email' => $value])->first();
            $isExist = ShareContent::where(['user_id' => $User->id, 'content_lib_id' => $request->content_lib_id])->first();
            if (empty($isExist)) {
                $isHasParent = UserLibrary::where(['id' => $request->content_lib_id])->first();
                while ($isHasParent->parent!='.') {
                    $contentParent = UserLibrary::where(['name_file' => $isHasParent->parent])->first();
                    $isParentExist = ShareContent::where(['user_id' => $User->id, 'content_lib_id' => $contentParent->id])->first();
                    if (empty($isParentExist)) {
                        $ShareContent = new ShareContent;
                        $ShareContent->content_lib_id = $contentParent->id;
                        $ShareContent->user_id = $User->id;
                        $ShareContent->isReadable = 1;
                        $ShareContent->isWriteable = $request->isWriteable;
                        $ShareContent->save();
                    }
                    $isHasParent = UserLibrary::where(['id' => $contentParent->id])->first();
                }
                $ShareContent = new ShareContent;
                $ShareContent->content_lib_id = $request->content_lib_id;
                $ShareContent->user_id = $User->id;
                $ShareContent->isReadable = 1;
                $ShareContent->isWriteable = $request->isWriteable;
                $ShareContent->save();
            }
        }

        return response()->json([
            'message' => 'Success',
        ], 200);

    }

    public function library_quiz_redirect($slug){
        $quiz = DB::table('quizzes')->where(['slug' => $slug])->first();
        return redirect('course/quiz/question/manage/'.$quiz->id);
    }

    // public function library_quiz_create($id){
    //     $section = Section::where('id', $id)->first();

    //     $data = [
    //       'action' => 'course/quiz/create_action/'.$section->id,
    //       'method' => 'post',
    //       'button' => 'Create',
    //       'id' => old('id'),
    //       'name' => old('name'),
    //       'description' => old('description'),
    //       'shuffle' => old('shuffle'),
    //       'time_start' => old('time_start'),
    //       'time_end' => old('time_end'),
    //       'duration' => old('duration'),
    //       'attempt' => old('attempt'),
    //       'section' => $section,
    //     ];
    //     return view('course.library.quiz_form', $data);
    //   }
}

?>
