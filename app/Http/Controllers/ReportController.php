<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Progress;
use App\User;
use App\Quiz;
use App\QuizParticipant;
use App\Course;
use App\Section;
use App\CourseUser;
use Auth;
use DB;


class ReportController extends Controller{
	public function index(){
		$content = Content::all();
		$user = User::all();
		$kuis = Quiz::all();
		$kursus = Course::all();
		
		$data = [
			'contents' => $content,
			'users' => $user,
			'kuis' => $kuis,
			'kursus' => $kursus,
		];
		
		return  view('report.status-report', $data);
	}
	
	public function summary($course_id){
		$Quiz = Quiz::all();
		$User = User::groupBy('name')->get();
		
		$QuizParticipant = QuizParticipant::select('users.name', 'quiz_participants.grade', 'quiz_participants.quiz_id')
		->join ('users', 'users.id', '=', 'quiz_participants.user_id')
		->get();
		
		$name = QuizParticipant::select('users.name', 'quiz_participants.grade', 'quiz_participants.quiz_id')
		->join ('users', 'users.id', '=', 'quiz_participants.user_id')
		->groupBy('quiz_participants.user_id')
		->get();
		
		$data = [
			'quiz' => $Quiz,
			'user' => $User,
			'quizparticipant' => $QuizParticipant,
			'name' => $name,
		];
		
		return  view('report.summary', $data);
	}
	
	public function summary_detail($id){
		$quiz = Quiz::where('id', $id)->first();
		
		$QuizParticipant = QuizParticipant::select('users.name', 'quiz_participants.user_id', 'quiz_participants.quiz_id', 'quiz_participants.time_start', 'quiz_participants.time_end', 'quiz_participants.grade')
		->where('quiz_id', $id)
		->join('quizzes', 'quizzes.id', '=', 'quiz_participants.quiz_id')
		->join('users','users.id','=','quiz_participants.user_id')->get();
		
		$data = [
			'quiz' => $quiz,
			'quizparticipant' => $QuizParticipant,
		];
		
		return  view('report.summary-detail', $data);
	}
	
	
	
}
