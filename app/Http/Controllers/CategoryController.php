<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index() {
      $categories = Category::withCount('courses')
      ->where('parent_category','0')
      ->orderBy('courses_count','desc')
      ->get();

      $data = [
        'categories' => $categories,
      ];

      return view('home.category_all', $data);
    }
}
