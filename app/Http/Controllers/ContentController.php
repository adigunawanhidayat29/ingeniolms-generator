<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Content;
use App\CourseUser;
use App\ContentFile;
use App\Course;
use App\Section;
use App\DiscussionAnswers;
use App\DiscussionGrade;
use App\ActivityOrder;
use App\CourseStudentGroup;
use App\GroupHasContent;
use Auth;
use Storage;
use Plupload;
use FFMpeg;
use File;
use ZipArchive;
use League\Flysystem\MountManager;

class ContentController extends Controller
{
  public function create($id)
  {
    $section = Section::where('id', $id)->first();

    $data = [
      'action' => 'course/content/create_action/' . $section->id,
      'method' => 'post',
      'button' => 'Create',
      'id' => old('id'),
      'title' => old('title'),
      'description' => old('description'),
      'is_description_show' => old('is_description_show'),
      'type_content' => old('type_content'),
      'name_file' => old('name_file'),
      'path_file' => old('path_file'),
      'file_size' => old('file_size'),
      'video_duration' => old('video_duration'),
      'full_path_file' => old('full_path_file'),
      'sequence' => old('sequence'),
      'section' => $section,
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get()
    ];
    return view('course.content_form', $data);
  }

  public function folder($content_id)
  {
    $folder = Content::find($content_id);

    return view('course.folder', compact('folder'));
  }

  public function discussion($content_id)
  {
    $discussion = Content::find($content_id);
    $course = $discussion->section->course;
    $discussion_user = CourseUser::where('course_id', $discussion->section->id_course)->get();
    $discussion_list = DiscussionAnswers::where('content_id', $content_id)->get();

    return view('course.discussion', compact('discussion', 'discussion_list', 'discussion_user', 'course'));
  }

  public function discussion_grade($content_id)
  {
    $discussion = Content::find($content_id);
    $course = $discussion->section->course;
    $discussion_user = CourseUser::where('course_id', $discussion->section->id_course)->get();
    $discussion_list = DiscussionAnswers::where('content_id', $content_id)->get();

    return view('course.discussion_grade', compact('discussion', 'discussion_list', 'discussion_user', 'course'));
  }

  public function discussion_delete($answer_id)
  {
    $discussion_list = DiscussionAnswers::find($answer_id);
    $discussion_list->delete();

    return redirect()->back();
  }

  public function discussion_grade_nilai(Request $request, $siswa_id, $content_id)
  {

    $grade = DiscussionGrade::where('user_id', $siswa_id)->where('content_id', $content_id)->first();
    if (!$grade) {
      $grade = new DiscussionGrade;
      $grade->content_id = $content_id;
      $grade->user_id = $siswa_id;
      if ($request->nilai) {
        $grade->grade = $request->nilai;
      } else {
        $grade->grade = 0;
        // code...
      }

      if ($request->comment) {
        $grade->comment = $request->comment;
      } else {
        $grade->grade = '';
        // code...
      }
      $grade->save();
    } else {
      $grade->content_id = $content_id;
      $grade->user_id = $siswa_id;
      if ($request->nilai) {
        $grade->grade = $request->nilai;
      }
      if ($request->comment) {
        $grade->comment = $request->comment;
      }
      $grade->save();
    }

    return $grade;
  }

  public function discussion_submit(Request $request, $content_id)
  {
    $discussion_answer = new DiscussionAnswers;
    $discussion_answer->content_id = $content_id;
    $discussion_answer->user_id = Auth::user()->id;
    $discussion_answer->body = $request->body;
    $new_file = [];
    if ($request->fileinput) {
      $new_file = $request->fileinput;
    }
    $file = json_encode(array_values($new_file));

    $discussion_answer->file = $file;

    if ($request->parents) {
      $discussion_answer->level_1 = $request->parents;
    }

    $discussion_answer->save();

    return redirect()->back();
  }

  public function folder_submit(Request $request, $content_id)
  {
    // code...
    $files = scandir(public_path('jquery-upload/server/php/folder/'));
    $new_file = [];
    foreach ($files as $key => $value) {
      if ($value != '.' && $value != '..' && $value != 'thumbnail') {
        $id = substr($value, 0, strlen(Auth::user()->id));
        if ($id == Auth::user()->id) {
          $ext = pathinfo(public_path('jquery-upload/server/php/folder/' . $value), PATHINFO_EXTENSION);
          $fileSize = fileSize(public_path('jquery-upload/server/php/folder/' . $value));
          $new_file_name = time() . '-' . $value;

          $folder_file = new ContentFile;
          $folder_file->content_id = $content_id;
          $folder_file->file_name = $value;
          $folder_file->file_path = asset('files/learn/folder/' . $new_file_name);
          $folder_file->file_type = $ext;
          $folder_file->file_size = $fileSize;
          // $fileSize = formatSizeUnits(fileSize(public_path('jquery-upload/server/php/folder/'.$value)))
          // dd(public_path('jquery-upload/server/php/folder/'.$value));
          $folder_file->save();
          File::copy(public_path('jquery-upload/server/php/folder/' . $value), public_path('files/learn/folder/' . $new_file_name));
          File::delete(public_path('jquery-upload/server/php/folder/thumbnail/' . $value));
          File::delete(public_path('jquery-upload/server/php/folder/' . $value));
        }
      }
    }


    return redirect()->back();
  }

  public function folder_item_delete($item_id)
  {
    // code...
    $folder_file = ContentFile::find($item_id);
    $url = parse_url($folder_file->file_path);
    File::delete(public_path($url["path"]));
    $folder_file->delete();

    return redirect()->back();
  }

  public function create_action($section_id, Request $request)
  {
    // insert content
    $folder = get_folder_course($section_id);
    $Content = new Content;

    // $destinationPath = asset_path('uploads/contents/'); // upload path
    // $extension = $request->file('name_file')->getClientOriginalExtension(); // getting image extension
    // $name_file = str_slug($request->title).'-'.rand(111,9999).'.'.$extension; // renameing image
    // // $request->file('name_file')->move($destinationPath, $name_file); // uploading file to local
    // $folder = get_folder_course($section_id); // get folder course by section id
    // Storage::disk('google')->put($folder.'/'.$name_file, fopen($request->file('name_file'), 'r+'));//upload google drive
    // $path_file = get_asset_path($folder, $name_file);

    $Content->title = $request->title;
    $Content->description = $request->description;
    $Content->type_content = $request->type_content;
    // $Content->sequence = $request->sequence;
    $Content->name_file = $request->name_file;
    $Content->path_file = $request->path_file;
    $Content->file_size = $request->file_size;
    $Content->video_duration = $request->video_duration;
    $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
    if ($request->type_content == 'video') {
      $Content->full_path_file_resize = get_asset_path($folder, '240-' . $request->name_file);
    }
    $Content->id_section = $section_id;
    $Content->save();
    // insert content

    \Session::flash('success', 'Create content success');
    // return redirect()->back();
  }

  public function update($id)
  {
    $Content = Content::where('id', $id)->first();
    $section = Section::where('id', $Content->id_section)->first();

    $data = [
      'action' => 'course/content/update_action/',
      'method' => 'post',
      'button' => 'Update',
      'id' => old('id', $Content->id),
      'title' => old('title', $Content->title),
      'description' => old('description', $Content->description),
      'is_description_show' => old('is_description_show', $Content->is_description_show),
      'type_content' => old('type_content', $Content->type_content),
      'name_file' => old('name_file', $Content->name_file),
      'path_file' => old('path_file', $Content->path_file),
      'full_path_file' => old('full_path_file', $Content->full_path_file),
      'file_size' => old('file_size', $Content->file_size),
      'video_duration' => old('video_duration', $Content->video_duration),
      'sequence' => old('sequence', $Content->sequence),
      'section' => $section,
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get(),
      'group_has_content' => GroupHasContent::where(['table' => 'contents', 'table_id' => $id])->get(),
    ];
    return view('course.content_form_update', $data);
  }

  public function update_action(Request $request)
  {
    $Content = Content::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Content->id_section)->first();

    // $folder = get_folder_course($Content->id_section);
    // if($request->name_file && $request->path_file){
    //   $Content->name_file = $request->name_file;
    //   $Content->path_file = $request->path_file;
    //   $Content->file_size = $request->file_size;
    //   $Content->full_path_file = "https://drive.google.com/file/d/".$request->path_file;
    //   if($request->type_content == 'video'){
    //     $Content->full_path_file_resize = get_asset_path($folder, '240-'.$request->name_file);
    //     $Content->video_duration = $request->video_duration;
    //   }
    // }

    if (!empty($request->get('course_student_group'))) {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $request->id, 'table' => 'contents'])->delete();
      foreach ($request->get('course_student_group') as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $request->id, 'table' => 'contents', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'contents';
          $GroupHasContent->table_id = $request->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
    } else {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $request->id, 'table' => 'contents'])->delete();
    }

    $Content->title = $request->title;
    $Content->description = $request->description;
    $Content->is_description_show = $request->is_description_show;
    $Content->type_content = $request->type_content;
    $Content->save();

    \Session::flash('success', 'Update content success');
    return redirect('course/preview/' . $Section->id_course);
  }

  public function publish($content_id, $course_id)
  {
    $content = Content::find($content_id);
    $content->status = '1';
    $content->save();

    \Session::flash('success', 'Publish Content success');
    // return redirect('course/preview/'.$Section->id_course);
    return redirect()->back();
  }

  public function unpublish($content_id, $course_id)
  {
    $content = Content::find($content_id);
    $content->status = '0';
    $content->save();

    \Session::flash('success', 'Unpublish Content success');
    return redirect()->back();
  }


  public function delete($course_id, $id)
  {
    $Content = Content::where('id', $id);
    if ($Content) {
      // unlink(asset_path($Content->first()->name_file));
      // Storage::disk('google')->delete($Content->first()->path_file); // delete file google drive
      $Content->delete();
      \Session::flash('success', 'Delete content success');
    } else {
      \Session::flash('error', 'Delete content failed');
    }

    return redirect('course/preview/' . $course_id);
  }

  // public function upload_BACKUP($section_id){
  //   return Plupload::receive('file', function ($file) use ($section_id)
  //   {
  //       $extension = $file->getClientOriginalExtension(); // getting image extension
  //       $size_file = $file->getSize(); // getting image extension
  //       $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image
  //       $folder = get_folder_course($section_id); // get folder course by section id
  //       Storage::disk('google')->put($folder.'/'.$name_file, fopen($file, 'r+'));//upload google drive
  //       $path_file = get_asset_path($folder, $name_file);
  //       $video_duration = 0;

  //       $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt', 'avi', 'webm'];
  //       if(in_array(strtolower($extension), $video_extensions)){
  //         $destinationPath = asset_path('uploads/videos/'); // upload path
  //         $file->move($destinationPath, $name_file);
  //         $ffmpeg = FFMpeg\FFMpeg::create(array(
  //           'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
  //           'ffprobe.binaries' => '/usr/bin/ffprobe',
  //           'timeout'          => 3600, // The timeout for the underlying process
  //           'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
  //         ));
  //         $video = $ffmpeg->open($destinationPath . $name_file);
  //         $video
  //           ->filters()
  //           ->resize(new FFMpeg\Coordinate\Dimension(426, 240))
  //           ->synchronize();
  //         $format = new FFMpeg\Format\Video\WebM();

  //         $format->setKiloBitrate(320);
  //         $video
  //           ->save($format, $destinationPath.'240-'.$name_file);
  //           Storage::disk('google')->put($folder.'/'.'240-'.$name_file, fopen($destinationPath.'240-'.$name_file, 'r+'));//upload google drive
  //           $resize_path = get_asset_path($folder, '240-'.$name_file);
  //           $ffprobe = FFMpeg\FFProbe::create();
  //           $video_duration = $ffprobe
  //             ->format($destinationPath . $name_file) // extracts file informations
  //             ->get('duration');

  //         return response()->json([
  //           'path' => $path_file,
  //           'name' => $name_file,
  //           'size' => $size_file,
  //           'video_duration' => $video_duration,
  //           'resize_path' => $resize_path
  //         ]);
  //       }else{
  //         return response()->json([
  //           'path' => $path_file,
  //           'name' => $name_file,
  //         ]);
  //       }
  //   });
  // }

  public function upload($section_id)
  {
    return Plupload::receive('file', function ($file) use ($section_id) {
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $size_file = $file->getSize(); // getting image extension
      $name_file = str_slug($file->getClientOriginalName()) . '-' . rand(111, 9999) . '.' . $extension; // renameing image

      $video_extensions = ['mp4', 'mkv', 'flv', 'mov', 'ogg', 'qt', 'avi', 'webm'];
      if (in_array(strtolower($extension), $video_extensions)) {

        // $folder = get_folderName_course($section_id); // get folder course by section id
        // Storage::disk('sftp')->put(server_assets_path($folder) .'/'. $name_file, fopen($file, 'r+')); //upload google drive
        $folder = get_folderName_course($section_id); // get folder course by section id
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/videos/' . $name_file, fopen($file, 'r+'), 'public'); //upload google drive
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/videos/', $name_file);
        $video_duration = 0;

        $ffprobe = FFMpeg\FFProbe::create([
          'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
          'ffprobe.binaries' => '/usr/bin/ffprobe',
          'timeout'          => 3600, // The timeout for the underlying process
          'ffmpeg.threads'   => 12,
        ]);
        $video_duration = $ffprobe
          ->format($file) // extracts file informations
          ->get('duration');

        return response()->json([
          'path' => $path_file,
          'name' => $name_file,
          'size' => $size_file,
          'video_duration' => $video_duration,
          'full_path_original' => asset_url($path_file),
          'full_path_file' => asset_url($path_file),
          'resize_path' => '',
        ]);
      } else {

        $folder = get_folderName_course($section_id); // get folder course by section id
        Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/files/' . $name_file, fopen($file, 'r+'), 'public'); //upload
        $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/files/', $name_file);

        return response()->json([
          'path' => $path_file,
          'name' => $name_file,
        ]);
      }
    });
  }

  // save to draft function
  public function store_draft(Request $request)
  {
    $field = $request->field;
    $value = $request->value;

    // jika melakukan set konten untuk group pelajar
    if ($field == 'group_student_id') {
      foreach ($value as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $request->id, 'table' => 'contents', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'contents';
          $GroupHasContent->table_id = $request->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
      return response()->json('success');
    }
    // jika melakukan set konten untuk group pelajar

    $Section = Section::where('id', $request->section_id)->first();
    $Content = Content::where('id', $request->id)->first(); // checking record content

    if ($Content) { // jika konten telah ada maka hanya melakaukan update konten
      // $folder = get_folder_course($Content->id_section); // get folder id
      $Content->type_content = $request->type_content;
      if ($request->file) { // jika ada inputan file
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        if ($Content->type_content == 'video') { // jika inputan file berupa video
          $Content->path_file = $request->path_file;
          $Content->file_size = $request->file_size;
          $Content->video_duration = $request->video_duration;
          $Content->full_path_original = $request->full_path_original;
          $Content->full_path_file = asset_url($request->path);
          $Content->save();
        } elseif ($Content->type_content == 'file') {
          $Content->full_path_file = asset_url($request->path_file);
          $Content->save();
        }
      } else {
        $Content->$field = $value;
        $Content->save();
      }
    } else { // jika belum ada konten maka buat konten baru
      // $folder = get_folder_course($request->section_id); // get folder id
      $Content = new Content;
      $Content->type_content = $request->type_content;
      $Content->id_section = $request->section_id;
      if ($request->file) {  // jika ada inputan file
        $Content->name_file = $request->name_file;
        $Content->path_file = $request->path_file;
        if ($Content->type_content == 'video') { // jika inputan file berupa video
          $Content->path_file = $request->path_file;
          $Content->full_path_file = asset_url($request->path_file);
          $Content->file_size = $request->file_size;
          $Content->video_duration = $request->video_duration;
          $Content->full_path_original = $request->full_path_original;
          $Content->save();

          // exec resize video
          // file_get_contents('http://159.69.35.86:3000/compress/' . $Content->id . '/' . $Section->id_course);
          // exec resize video

        }
        // elseif($Content->type_content == 'file'){
        //   $Content->full_path_file = "https://drive.google.com/file/d/" . $request->path_file;
        //   $Content->save();
        // }
      } else {
        $Content->$field = $value;
        $Content->save();
      }
    }

    \Session::flash('success', 'Create content success');

    return response()->json($Content);
  }

  public function update_sequence(Request $request)
  {
    if ($request->datas) {
      foreach ($request->datas as $key => $value) {
        // $Content = Content::find($value);
        // $Content->id_section = $request->section_id;
        // $Content->sequence = $key;
        // $Content->save();

        $ActivityOrder = ActivityOrder::where($request->content_datas[$key] . '_id', $value)->first();
        if ($ActivityOrder) {
          $ActivityOrder->content_id = $request->content_datas[$key] == 'content' ? $value : null;
          $ActivityOrder->quizz_id = $request->content_datas[$key] == 'quizz' ? $value : null;
          $ActivityOrder->assignment_id = $request->content_datas[$key] == 'assignment' ? $value : null;
          $ActivityOrder->order = $key;
          $ActivityOrder->save();
        } else {
          $newActivityOrder = new ActivityOrder;
          $newActivityOrder->content_id = $request->content_datas[$key] == 'content' ? $value : null;
          $newActivityOrder->quizz_id = $request->content_datas[$key] == 'quizz' ? $value : null;
          $newActivityOrder->assignment_id = $request->content_datas[$key] == 'assignment' ? $value : null;
          $newActivityOrder->order = $key;
          $newActivityOrder->save();
        }
      }
    }
  }

  public function update_preview(Request $request)
  {

    $section = Section::where('id_course', $request->course_id)
      ->join('courses', 'courses.id', '=', 'sections.id_course')
      ->join('contents', 'contents.id_section', '=', 'sections.id')
      ->where('contents.preview', '1')
      ->get();

    if ($request->preview == '1') {
      if (count($section) < 3) {
        $Content = Content::find($request->id);
        $Content->preview = '1';
        $Content->save();

        $title = "Berhasil";
        $message = 'Konten berhasil di set sebagai preview';
        $alert = 'success';
      } else {
        $title = "Gagal";
        $message = 'Konten preview tidak boleh lebih dari 3';
        $alert = 'error';
      }
    } else {
      $Content = Content::find($request->id);
      $Content->preview = '0';
      $Content->save();

      $title = "Berhasil";
      $message = 'Konten tidak sebagai preview';
      $alert = 'success';
    }

    return response()->json([
      'title' => $title,
      'message' => $message,
      'alert' => $alert,
    ]);
  }

  public function detail(Request $request)
  {
    $Content = Content::where('id', $request->id)->first();

    if ($Content->type_content == 'video') {
      // get file video google drive
      // $data = getVideoGoogleDrive($Content->full_path_file_resize_720);
      // $data_360 = getVideoGoogleDrive($Content->full_path_file_resize);
      // $client_info = getClientInfoGoogleDrive();

      // $downloadUrl = str_replace('&e=download', '', $data->downloadUrl);
      // $downloadUrl = $downloadUrl . '&access_token=' . $client_info->access_token;
      // get file video google drive

      // $body = $downloadUrl;

      $body = $Content->full_path_original;
    } elseif ($Content->type_content == 'url-video') {
      $body = $Content->full_path_file;
    } elseif ($Content->type_content == 'file') {
      $body = $Content->full_path_file;
    } else {
      $body = $Content->description;
    }

    $response = [
      'id' => $Content->id,
      'title' => $Content->title,
      'content' => $body,
      'description' => $Content->description,
      'type_content' => $Content->type_content,
    ];

    return response()->json($response);
  }

  public function create_scorm($id)
  {
    $section = Section::where('id', $id)->first();
    $data = [
      'section' => $section,
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get()
    ];
    return view('course.content_form_scorm', $data);
  }

  public function store_scorm($id, Request $request)
  {
    $section = Section::where('id', $id)->first();
    $file = $request->file('files');

    // upload
    $name_file_origin = time() . '-' . str_slug($request->title);
    $name_file = $name_file_origin . '-' . $file->getClientOriginalName();
    $folder = get_folderName_course($section->id); // get folder course by section id
    Storage::disk(env('APP_DISK'))->put(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $name_file, fopen($file, 'r+'), 'public'); //upload
    $path_file = get_asset_newpath(env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/', $name_file);
    // upload
    //  extract zip
    $zip = new ZipArchive;
    $zip->open($file);
    $zip->extractTo(asset_path() . 'uploads/courses/' . $folder . '/contents/scorms/' . $name_file_origin);
    // $zip->extractTo(storage_path('app/public/' . str_slug($request->title)));
    $zip->close();
    //  extract zip

    // upload to storage
    $files = Storage::disk('public')->allFiles(str_slug($request->title));
    foreach ($files as $fileExtract) {
      // echo $fileExtract;
      $inputStream = Storage::disk('public')->getDriver()->readStream($fileExtract);
      $destination = Storage::disk(env('APP_DISK'))->getDriver()->getAdapter()->getPathPrefix() . env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $fileExtract;
      Storage::disk(env('APP_DISK'))->getDriver()->putStream($destination, $inputStream, ['visibility' => 'public']);
    }
    // upload to storage

    $content = new Content;
    $content->title = $request->title;
    $content->id_section = $id;
    $content->status = '1';
    $content->type_content = 'scorm';
    $content->path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $name_file_origin . '/imsmanifest.xml';
    $content->full_path_file = env('UPLOAD_PATH') . '/courses/' . $folder . '/contents/scorms/' . $name_file_origin . '/imsmanifest.xml';
    $content->save();

    return redirect('/course/preview/' . $section->id_course)->with('message', 'Berhasil upload scorm');
  }
}
