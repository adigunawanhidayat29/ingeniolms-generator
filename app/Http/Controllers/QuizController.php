<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\Quiz;
use App\QuizQuestionAnswer;
use App\QuizParticipant;
use App\QuizParticipantAnswer;
use App\QuizPageBreak;
use App\Section;
use App\Content;
use App\Assignment;
use App\Progress;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use File;
use Auth;

class QuizController extends Controller
{
  public function detail($slug, $id){
    $courses = Course::where('slug', $slug)->with('sections')->first();
    $quiz = Quiz::where('id', $id)->first();
    $QuizParticipant = QuizParticipant::where('quiz_id', $id)->where('user_id', Auth::user()->id)->get();

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

    $num_contents = 0;
    foreach($sections as $section){
      $section_all = [];

      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
      $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->orderBy('contents.sequence', 'asc')
        ->get();

      $group_content_id = [];
      foreach ($group_has_contents as $data) {
        array_push($group_content_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->where(function ($query) use ($group_content_id) {
        $query->whereNotIn('id', $group_content_id);
      })
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'contents')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
      $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->get();

      $group_quiz_id = [];
      foreach ($group_has_quizzes as $data) {
        array_push($group_quiz_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
        $query->whereNotIn('id', $group_quiz_id);
      })->get();
      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'quizzes')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
      $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->get();

      $group_assignment_id = [];
      foreach ($group_has_assignments as $data) {
        array_push($group_assignment_id, $data->table_id);
      }
      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
        $query->whereNotIn('id', $group_assignment_id);
      })->get();
      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'assignments')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
      $num_contents += count($quizzes);
      $num_contents += count($assignments);

      // MERGING KONTEN
      foreach ($contents as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      foreach ($group_has_contents_users as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      // MERGING KONTEN

      // MERGING KUIS
      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      foreach ($group_has_quizzes_users as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      // MERGING KUIS

      // MERGING TUGAS
      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      foreach ($group_has_assignments_users as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      // MERGING TUGAS

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_all' => $section_all,
      ]);
    }

    if($quiz){
      $data = [
        'quiz' => $quiz,
        'course' => $courses,
        'sections' => $section_data,
        'quiz_participant' => $QuizParticipant
      ];
      return view('quiz.detail', $data);
    }
  }

  public function start($slug, $id){

    $user = Auth::user();
    $userId = $user->id;
    $courses = Course::where('slug', $slug)->with('sections')->first();
    $authorId = $courses->id_author;

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

    $num_contents = 0;
    foreach($sections as $section){
      $section_all = [];

      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
      $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->orderBy('contents.sequence', 'asc')
        ->get();

      $group_content_id = [];
      foreach ($group_has_contents as $data) {
        array_push($group_content_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->where(function ($query) use ($group_content_id) {
        $query->whereNotIn('id', $group_content_id);
      })
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'contents')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
      $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->get();

      $group_quiz_id = [];
      foreach ($group_has_quizzes as $data) {
        array_push($group_quiz_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
        $query->whereNotIn('id', $group_quiz_id);
      })->get();
      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'quizzes')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
      $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->get();

      $group_assignment_id = [];
      foreach ($group_has_assignments as $data) {
        array_push($group_assignment_id, $data->table_id);
      }
      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
        $query->whereNotIn('id', $group_assignment_id);
      })->get();
      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'assignments')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
      $num_contents += count($quizzes);
      $num_contents += count($assignments);

      // MERGING KONTEN
      foreach ($contents as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      foreach ($group_has_contents_users as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      // MERGING KONTEN

      // MERGING KUIS
      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      foreach ($group_has_quizzes_users as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      // MERGING KUIS

      // MERGING TUGAS
      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      foreach ($group_has_assignments_users as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      // MERGING TUGAS

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_all' => $section_all,
      ]);
    }

    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    if($quiz->paginate_type == 1){
      $newPageBreak = [];
      $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
      foreach ($quiz_page_breaks as $key => $value) {
        foreach ($value->quiz_questions as $key2 => $value2) {
          $newPageBreak[] = $value2;
        }
      }
      $newPageBreak = $this->paginate($newPageBreak, $quiz->paginate);;
    }else {
      $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->paginate($quiz->paginate);
      $newPageBreak = $quiz_page_breaks;
    }

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->get();
    // if(!$QuizParticipant_check){
    //   // insert quiz participant
    //   $QuizParticipant = new QuizParticipant;
    //   $QuizParticipant->quiz_id = $quiz->id;
    //   $QuizParticipant->user_id = $user->id;
    //   $QuizParticipant->time_start = date("Y-m-d H:i:s");
    //   $QuizParticipant->save();
    //   // insert quiz participant
    //   $quiz_time_start = $QuizParticipant->time_start;
    // }
    // elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
    //   return redirect('course/learn/quiz/'.$slug.'/'.$id.'/result'); // jika sudah mengikuti quiz redirect ke result
    // }else{
    //   $quiz_time_start = $QuizParticipant_check->time_start;
    // }

    if($quiz->attempt < 0 || count($QuizParticipant_check) <= $quiz->attempt){
      if(count($QuizParticipant_check) > 0){
        $QuizParticipant = $QuizParticipant_check->last();
        if($QuizParticipant->finish == 1){
          if($quiz->attempt < 0 || count($QuizParticipant_check) < $quiz->attempt){
            $QuizParticipant = new QuizParticipant;
            $QuizParticipant->quiz_id = $quiz->id;
            $QuizParticipant->user_id = $user->id;
            $QuizParticipant->time_start = date("Y-m-d H:i:s");
            $QuizParticipant->save();
            $quiz_time_start = $QuizParticipant->time_start;
          }else {
            return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
          }
        }else {
          if($quiz->duration_required == 1){
            $time = date('Y-m-d H:i:s', strtotime($QuizParticipant->time_start . ' +'.$quiz->duration.' minutes'));
            $minutes = (time() - strtotime($time)) / 60;
            if($minutes >= 0){
              $QuizParticipant->finish = 1;
              $QuizParticipant->save();
              return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
            }
          }
        }
        $quiz_time_start = $QuizParticipant->time_start;
      }else {
        $QuizParticipant = new QuizParticipant;
        $QuizParticipant->quiz_id = $quiz->id;
        $QuizParticipant->user_id = $user->id;
        $QuizParticipant->time_start = date("Y-m-d H:i:s");
        $QuizParticipant->save();
        $quiz_time_start = $QuizParticipant->time_start;
      }
    }else {
      return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
    }

    $data = [
      'course' => $courses,
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $newPageBreak,
      'userId' => $userId,
      'authorId' => $authorId,
      'sections' => $section_data,
      'patricipant' => $QuizParticipant,
      'answer' => $QuizParticipant->answer
    ];
    return view('quiz.start', $data);;
  }

  public function paginate($items, $perPage = 5, $page = null, $options = [])
  {
      $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
      $items = $items instanceof Collection ? $items : Collection::make($items);
      return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
  }

  public function start_ajax(Request $request, $slug, $id){
    $user = Auth::user();
    $userId = $user->id;
    $courses = Course::where('slug', $slug)->with('sections')->first();
    $authorId = $courses->id_author;

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

    $num_contents = 0;
    foreach($sections as $section){
      $section_all = [];

      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
      $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->orderBy('contents.sequence', 'asc')
        ->get();

      $group_content_id = [];
      foreach ($group_has_contents as $data) {
        array_push($group_content_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->where(function ($query) use ($group_content_id) {
        $query->whereNotIn('id', $group_content_id);
      })
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'contents')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
      $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->get();

      $group_quiz_id = [];
      foreach ($group_has_quizzes as $data) {
        array_push($group_quiz_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
        $query->whereNotIn('id', $group_quiz_id);
      })->get();
      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'quizzes')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
      $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->get();

      $group_assignment_id = [];
      foreach ($group_has_assignments as $data) {
        array_push($group_assignment_id, $data->table_id);
      }
      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
        $query->whereNotIn('id', $group_assignment_id);
      })->get();
      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'assignments')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
      $num_contents += count($quizzes);
      $num_contents += count($assignments);

      // MERGING KONTEN
      foreach ($contents as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      foreach ($group_has_contents_users as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      // MERGING KONTEN

      // MERGING KUIS
      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      foreach ($group_has_quizzes_users as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      // MERGING KUIS

      // MERGING TUGAS
      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      foreach ($group_has_assignments_users as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      // MERGING TUGAS

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_all' => $section_all,
      ]);
    }

    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    if($quiz->paginate_type == 1){
      $newPageBreak = [];
      $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
      foreach ($quiz_page_breaks as $key => $value) {
        foreach ($value->quiz_questions as $key2 => $value2) {
          $newPageBreak[] = $value2;
        }
      }
      $newPageBreak = $this->paginate($newPageBreak, $quiz->paginate);;
    }else {
      $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->paginate($quiz->paginate);
      $newPageBreak = $quiz_page_breaks;
    }

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->get();

    if($quiz->attempt < 0 || count($QuizParticipant_check) <= $quiz->attempt){
      if($QuizParticipant_check){
        $QuizParticipant = $QuizParticipant_check->last();
        if($QuizParticipant->finish == 1){
          if($quiz->attempt < 0 || count($QuizParticipant_check) < $quiz->attempt){
            $QuizParticipant = new QuizParticipant;
            $QuizParticipant->quiz_id = $quiz->id;
            $QuizParticipant->user_id = $user->id;
            $QuizParticipant->time_start = date("Y-m-d H:i:s");
            $QuizParticipant->save();
            $quiz_time_start = $QuizParticipant->time_start;
          }else {
            return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
          }
        }else {
          if($quiz->duration_required == 1){
            $time = date('Y-m-d H:i:s', strtotime($QuizParticipant->time_start . ' +'.$quiz->duration.' minutes'));
            $minutes = (time() - strtotime($time)) / 60;
            if($minutes >= 0){
              $QuizParticipant->finish = 1;
              $QuizParticipant->save();
              return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
            }
          }
        }
        $quiz_time_start = $QuizParticipant->time_start;
      }else {
        $QuizParticipant = new QuizParticipant;
        $QuizParticipant->quiz_id = $quiz->id;
        $QuizParticipant->user_id = $user->id;
        $QuizParticipant->time_start = date("Y-m-d H:i:s");
        $QuizParticipant->save();
        $quiz_time_start = $QuizParticipant->time_start;
      }
    }else {
      return redirect()->back(); // jika sudah mengikuti quiz redirect ke result
    }

    $data = [
      'course' => $courses,
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $newPageBreak,
      'userId' => $userId,
      'authorId' => $authorId,
      'sections' => $section_data,
      'patricipant' => $QuizParticipant,
      'answer' => $QuizParticipant->answer
    ];

    $question = [];
    $question = $request->answer[1];

    foreach($question as $index => $value){
      $QuizParticipantAnswer = QuizParticipantAnswer::where(['quiz_id' => $id, 'quiz_participant_id' => $request->quiz_participant_id, 'quiz_question_id' => $value])->first();
      if(!$QuizParticipantAnswer){
        $QuizParticipantAnswer = new QuizParticipantAnswer;
        $QuizParticipantAnswer->quiz_id = $quiz->id;
        $QuizParticipantAnswer->quiz_participant_id = $request->quiz_participant_id;
        $QuizParticipantAnswer->quiz_question_id = $value;
      }
      if(in_array($request->answer[2][$index], ['13'])){
        $QuizParticipantAnswer->quiz_question_answer_id = 0;
        $QuizParticipantAnswer->answer_essay = implode(',',$request->answer[0][$index]);
        $QuizParticipantAnswer->quiz_question_answer_id = 0;
      }else {
        $QuizParticipantAnswer->quiz_question_answer_id = isset ($request->answer[0][$index]) ? rtrim($request->answer[0][$index], ',') : 0;
      }

      if(in_array($request->answer[2][$index], ['5', '8', '10', '11', '14'])){
        $QuizParticipantAnswer->quiz_question_answer_id = 0;
        $QuizParticipantAnswer->answer_essay = $request->answer[0][$index];
      }

      if($request->answer[2][$index] == '6'){
        $check_short_answer = QuizQuestionAnswer::where('answer', 'like', '%' . $request->answer[0][$index] . '%')->first();
        if($check_short_answer){
          $QuizParticipantAnswer->quiz_question_answer_id = $check_short_answer->id;
        }else{
          $QuizParticipantAnswer->quiz_question_answer_id = 0;
        }
        $QuizParticipantAnswer->answer_short_answer = $request->answer[0][$index];
      }
      $QuizParticipantAnswer->save();
    }

    if($quiz->paginate_type == 1){
      return view('quiz.start_quiz_question', $data)->render();
    }else {
      return view('quiz.start_quiz_page_break', $data)->render();
      // code...
    }
  }

  public function start_($id){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $quiz_questions_answers = [];
    $quiz_questions = DB::table('quiz_questions')->where('quiz_id', $quiz->id);
      if($quiz->shuffle == '1'){
        $quiz_questions = $quiz_questions->inRandomOrder();
      }
      $quiz_questions = $quiz_questions->get();

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('quiz/'.$id.'/result');
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    foreach($quiz_questions as $quiz_question){
      $quiz_question_answers = DB::table('quiz_question_answers')->where('quiz_question_id', $quiz_question->id)->get();
      array_push($quiz_questions_answers, array(
        'id' => $quiz_question->id,
        'question' => $quiz_question->question,
        'quiz_type_id' => $quiz_question->quiz_type_id,
        'question_answers' => $quiz_question_answers,
      ));
    }

    $data = [
      'quiz' => $quiz,
      'quiz_time_start' => $quiz_time_start,
      'quiz_questions_answers' => $quiz_questions_answers,
    ];
    return view('quiz.start', $data);;
  }

  public function finish($slug, $id, Request $request){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $courses = Course::where('slug', $slug)->with('sections')->first();
    $quiz_participant = QuizParticipant::find($request->participant_id);
    $quiz_participant->finish = 1;
    $quiz_participant->submitted_date = date('Y-m-d H:i:s');
    $quiz_participant->save();
    // if(QuizParticipantAnswer::where(array('quiz_participant_id' => $quiz_participant->id))->first()){
    //   return ['status' => false, 'id' => $quiz_participant->id];
    //   // return redirect('course/learn/quiz/'.$slug.'/'.$id.'/result/'.$quiz_participant->id); // jika sudah mengikuti quiz redirect ke result
    // }else{
      $question = [];

      if($request->questions){
        $question = $request->questions;
      }
      foreach($question as $index => $value){
        $QuizParticipantAnswer = QuizParticipantAnswer::where(['quiz_id' => $id, 'quiz_participant_id' => $request->participant_id, 'quiz_question_id' => $value])->first();
        if(!$QuizParticipantAnswer){
          $QuizParticipantAnswer = new QuizParticipantAnswer;
          $QuizParticipantAnswer->quiz_id = $quiz->id;
          $QuizParticipantAnswer->quiz_participant_id = $request->participant_id;
          $QuizParticipantAnswer->quiz_question_id = $value;
        }
        if(in_array($request->question_type[$index], ['13'])){
          $QuizParticipantAnswer->quiz_question_answer_id = 0;
          $QuizParticipantAnswer->answer_essay = implode(',',$request->answers[$index]);
          $QuizParticipantAnswer->quiz_question_answer_id = 0;
        }else {
          $QuizParticipantAnswer->quiz_question_answer_id = isset ($request->answers[$index]) ? rtrim($request->answers[$index], ',') : 0;
        }

        if(in_array($request->question_type[$index], ['5', '8', '10', '11', '14'])){
          $QuizParticipantAnswer->quiz_question_answer_id = 0;
          $QuizParticipantAnswer->answer_essay = $request->answers[$index];
        }

        if($request->question_type[$index] == '6'){
          $check_short_answer = QuizQuestionAnswer::where('answer', 'like', '%' . $request->answers[$index] . '%')->first();
          if($check_short_answer){
            $QuizParticipantAnswer->quiz_question_answer_id = $check_short_answer->id;
          }else{
            $QuizParticipantAnswer->quiz_question_answer_id = 0;
          }
          $QuizParticipantAnswer->answer_short_answer = $request->answers[$index];
        }
        $QuizParticipantAnswer->save();
      }

      $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
        ->where('quiz_participant_id', $quiz_participant->id)
        ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
        ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id');

      // get count answer correct n incorrect n score
      $count_answer_correct = 0;
      $count_answer_incorrect = 0;
      $score = 0;
      $weight = 100;
      foreach($QuizParticipants->get() as $QuizParticipant){
        // $weight += $QuizParticipant->weight;
        if($QuizParticipant->answer_correct == '1'){
          $count_answer_correct += $QuizParticipant->answer_correct;
        }else{
          $count_answer_incorrect += $QuizParticipant->answer_correct;
        }
      }

      $quiz_participants_count = 1;
      if($QuizParticipants->count() > 0){
        $quiz_participants_count = $QuizParticipants->count();
      }

      $score = ($weight / $quiz_participants_count) * $count_answer_correct;
      // get count answer correct n incorrect n score

      // update time end
      $quiz_participant->time_end = date("Y-m-d H:i:s");
      if($quiz->quizz_type == 'survey'){
        // code...
        $quiz_participant->grade = $quiz->submission_grade;
      }else {
        $quiz_participant->grade = $score;
      }
      $quiz_participant->save();
      // update time end
      $Progress = Progress::where(['quiz_id' => $id, 'user_id' => Auth::user()->id])->first();
      if(!$Progress){
        $NewProgress = new Progress;
        $NewProgress->course_id = $courses->id;
        $NewProgress->quiz_id = $id;
        $NewProgress->status = '1';
        $NewProgress->user_id = Auth::user()->id;
        $NewProgress->save();
      }else {
        // code...
        $Progress->status = '1';
        $Progress->save();
      }

      return ['status' => true, 'id' => $quiz_participant->id];
      // return $quiz_participant->id;
    // }
  }

  public function result($slug, $id, $id_participant){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $courses = Course::where('slug', $slug)->with('sections')->first();
    $QuizParticipantData = QuizParticipant::find($id_participant);
    $QuizParticipant = QuizParticipant::find($id_participant);
    $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*', 'quiz_questions.feedback as feedback_question', 'quiz_question_answers.feedback as feedback_answer')
      ->where('quiz_participant_id', $QuizParticipant->id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id');

    // get count answer correct n incorrect n score
    $count_answer_correct = 0;
    $count_answer_incorrect = 0;
    $score = 0;
    $weight = 100;
    foreach($QuizParticipants->get() as $QuizParticipant){
      // $weight += $QuizParticipant->weight;
      if($QuizParticipant->answer_correct == '1'){
        $count_answer_correct += $QuizParticipant->answer_correct;
      }else{
        $count_answer_incorrect += $QuizParticipant->answer_correct;
      }
    }
    $quiz_participants_count = 1;
    if($QuizParticipants->count() > 0){
      $quiz_participants_count = $QuizParticipants->count();
    }
    $score = ($weight / $quiz_participants_count) * $count_answer_correct;
    // get count answer correct n incorrect n score

    $section_data = [];
    $sections = DB::table('sections')->where('id_course', $courses->id)->orderBy('sequence','asc')->get();

    $num_contents = 0;
    foreach($sections as $section){
      $section_all = [];

      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP
      $group_has_contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'contents')
        ->orderBy('contents.sequence', 'asc')
        ->get();

      $group_content_id = [];
      foreach ($group_has_contents as $data) {
        array_push($group_content_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KONTEN ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $contents = Content::with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->where(function ($query) use ($group_content_id) {
        $query->whereNotIn('id', $group_content_id);
      })
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK MENGAMBIL KONTEN YANG TIDAK DIMILIKI OLEH GROP

      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_contents_users = Content::select('contents.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'contents.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'contents')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->orderBy('contents.sequence', 'asc')
      ->get();
      // UNTUK NGAMBIL KONTEN YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP
      $group_has_quizzes = Quiz::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'quizzes')
        ->get();

      $group_quiz_id = [];
      foreach ($group_has_quizzes as $data) {
        array_push($group_quiz_id, $data->table_id);
      }
      // UNTUK MENGAMBIL KUIS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $quizzes = Quiz::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_quiz_id) {
        $query->whereNotIn('id', $group_quiz_id);
      })->get();
      // UNTUK MENGAMBIL KUIS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_quizzes_users = Quiz::select('quizzes.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'quizzes.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'quizzes')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP
      $group_has_assignments = Assignment::with('activity_order')->with('restrict')->where('id_section', $section->id)
        ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
        ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
        ->where('group_has_contents.table', 'assignments')
        ->get();

      $group_assignment_id = [];
      foreach ($group_has_assignments as $data) {
        array_push($group_assignment_id, $data->table_id);
      }
      // UNTUK MENGAMBIL TUGAS ANG DIMILIKI OLEH GRUP

      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)
      $assignments = Assignment::with('activity_order')->with('restrict')->where(['id_section' => $section->id])->where(function ($query) use ($group_assignment_id) {
        $query->whereNotIn('id', $group_assignment_id);
      })->get();
      // UNTUK MENGAMBIL TUGAS YANG TIDAK DIMILIKI OLEH GROP (PAKE WHERENOTIN)

      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN
      $group_has_assignments_users = Assignment::select('assignments.*')->with('activity_order')->with('restrict')->where('id_section', $section->id)
      ->join('group_has_contents', 'group_has_contents.table_id', '=', 'assignments.id')
      ->join('course_student_group_users', 'course_student_group_users.course_student_group_id', 'group_has_contents.course_student_group_id')
      ->where('group_has_contents.table', 'assignments')
      ->where('course_student_group_users.user_id', '=', Auth::user()->id)
      ->get();
      // UNTUK NGAMBIL KUIS YANG HANYA DIMILIKI GRUP USER LOGGED IN

      $num_contents += count($contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder'));
      $num_contents += count($quizzes);
      $num_contents += count($assignments);

      // MERGING KONTEN
      foreach ($contents as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      foreach ($group_has_contents_users as $content) {
        $content->section_type = 'content';
        array_push($section_all, $content);
      }
      // MERGING KONTEN

      // MERGING KUIS
      foreach ($quizzes as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      foreach ($group_has_quizzes_users as $quizze) {
        $quizze->section_type = 'quizz';
        $section_all[] = $quizze;
      }
      // MERGING KUIS

      // MERGING TUGAS
      foreach ($assignments as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      foreach ($group_has_assignments_users as $assignment) {
        $assignment->section_type = 'assignment';
        $section_all[] = $assignment;
      }
      // MERGING TUGAS

      array_push($section_data, [
        'id' => $section->id,
        'title' => $section->title,
        'description' => $section->description,
        'id_course' => $section->id_course,
        'created_at' => $section->created_at,
        'updated_at' => $section->updated_at,
        'section_all' => $section_all,
      ]);
    }

    $data = [
      'course' => $courses,
      'quiz' => $quiz,
      'quiz_participant_data' => $QuizParticipantData,
      'quiz_participants' => $QuizParticipants->get(),
      'count_question' => $QuizParticipants->count(),
      'count_answer_correct' => $count_answer_correct,
      'count_answer_incorrect' => $count_answer_incorrect,
      'score' => $score,
      'sections' => $section_data,
    ];
    return view('quiz.result', $data);
  }

  public function upload(Request $request){
    if($request->image){
      $filename = time() . '.' . $request->image->getClientOriginalExtension();
      $request->image->move(public_path('files/learn/survey'), $filename);
    }

    return [$filename, $request->image->getClientOriginalName()];
  }
}
