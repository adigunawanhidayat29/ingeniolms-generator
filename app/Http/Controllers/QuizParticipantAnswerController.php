<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\QuizParticipantAnswer;
use App\QuizParticipant;
use App\Quiz;
use App\Course;
use App\User;
use Excel;

class QuizParticipantAnswerController extends Controller
{
  public function index($quiz_participant_id, $quiz_id){
    $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*', 'quiz_participant_answers.id as quiz_participant_answer_id','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participant_id', $quiz_participant_id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
      ->get();

    // $Quiz = Quiz::find($QuizParticipants->first()->quiz_id);
    $Quiz = Quiz::select('quiz_participants.grade', 'quizzes.*')
      ->where(['quizzes.id' => $quiz_id, 'quiz_participants.id' => $quiz_participant_id])
      ->join('quiz_participants', 'quiz_participants.quiz_id', '=', 'quizzes.id')
      ->first();

    $Course = Course::select('courses.*')->join('sections', 'sections.id_course', '=', 'courses.id')->where('sections.id', $Quiz->id_section)->first();

    $QuizParticipantUserId = QuizParticipant::where('id', $quiz_participant_id)->first();
    $UserName = User::where('id', $QuizParticipantUserId->user_id)->select('name', 'email', 'id')->first();

    $data = [
      'quiz_participants' => $QuizParticipants,
      'quiz' => $Quiz,
      'course' => $Course,
      'user' => $UserName,
    ];
    return view('course.quiz_participant_answer', $data);
  }

  public function grade(Request $request) {
    $QuizParticipantAnswer = QuizParticipantAnswer::where('id', $request->id)->first();
    $QuizParticipantAnswer->answer_essay_grade = $request->grade;
    $QuizParticipantAnswer->save();
  }

  public function gradeOrder(Request $request) {
      $QuizParticipantAnswer = QuizParticipantAnswer::where('id', $request->id)->first();
      $QuizParticipantAnswer->answer_ordering_grade = $request->grade;
      $QuizParticipantAnswer->save();
  }

  public function download($quiz_participant_id){
    Excel::create('Filename', function($excel) {
      $excel->sheet('New sheet', function($sheet) {
        $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
          ->where('quiz_participant_id', $quiz_participant_id)
          ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
          ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')->get();

        $Quiz = Quiz::find($QuizParticipants->first()->quiz_id);
        $Course = Course::select('courses.*')->join('sections', 'sections.id_course', '=', 'courses.id')->where('sections.id', $Quiz->id_section)->first();

        $data = [
          'quiz_participants' => $QuizParticipants,
          'quiz' => $Quiz,
          'course' => $Course,
        ];

        $sheet->loadView('course.quiz_participant_answer_excel', $data);
      });
    })->download('xls');
  }

  //method baru --egi--
  public function grade_total(Request $request)
  {
    $QuizParticipantAnswer = QuizParticipant::where('id', $request->id)->first();
    $QuizParticipantAnswer->grade = $request->grade;
    $QuizParticipantAnswer->save();
  }
}
