<?php

namespace App\Http\Controllers\Employeer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Instructor;
use App\UserGroup;
use App\Course;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
      
    }

    public function index()
    {
      // get all users total
      $userTotal = User::get();
      // get all users total

      // get all teacher total
      $instructorTotal = User::select('users.id')
        ->join('instructors','instructors.user_id','=','users.id')
        ->get();
      // get all teacher total

      // get all student total
      $studentTotal = User::join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->where('level_id', '3')
        ->get();
      // get all student total

      // get all courses total
      $coursesTotal = Course::get();
      // get all courses total

      // get active courses by progresses update
      $activeCoursesDaily = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '=', Carbon::today())
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesWeekly = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(8))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesMonthly = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(31))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();

      $activeCoursesAnual = Course::select('courses.title', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(366))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('progresses.course_id')
        ->get();
        // get active courses by progresses update

        // get active teacher by courses update
        $activeTeachersDaily = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '=', Carbon::today())
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersWeekly = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(8))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersMonthly = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(31))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();

        $activeTeachersAnual = Course::select('users.name', 'courses.updated_at as created_at')
          ->whereDate('courses.updated_at', '>=', Carbon::today()->subDays(366))
          ->join('users', 'users.id', '=', 'courses.id_author')
          ->join('instructors', 'instructors.user_id', '=', 'users.id')
          ->orderBy('courses.updated_at', 'desc')
          ->groupBy('users.id')
          ->get();
        // get active teacher by courses update

        // get active student by courses update
        $activeStudentsDaily = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '=', Carbon::today())
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsWeekly = Course::select('users.name', 'progresses.created_at')
        ->where('progresses.created_at', '>=', Carbon::today()->subDays(8))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsMonthly = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(31))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $activeStudentsAnual = Course::select('users.name', 'progresses.created_at')
        ->whereDate('progresses.created_at', '>=', Carbon::today()->subDays(366))
        ->join('progresses', 'progresses.course_id', '=', 'courses.id')
        ->join('users', 'users.id', '=', 'progresses.user_id')
        ->orderBy('progresses.created_at', 'desc')
        ->groupBy('users.id')
        ->get();
        // get active student by courses update

      // set data
      $data = [
        'all_users_total' => count($userTotal),
        'all_instructors_total' => count($instructorTotal),
        'all_students_total' => count($studentTotal),
        'all_courses_total' => count($coursesTotal),

        'all_active_courses_total_daily' => $activeCoursesDaily,
        'all_active_courses_total_weekly' => $activeCoursesWeekly,
        'all_active_courses_total_monthly' => $activeCoursesMonthly,
        'all_active_courses_total_anual' => $activeCoursesAnual,

        'all_active_teachers_total_daily' => $activeTeachersDaily,
        'all_active_teachers_total_weekly' => $activeTeachersWeekly,
        'all_active_teachers_total_monthly' => $activeTeachersMonthly,
        'all_active_teachers_total_anual' => $activeTeachersAnual,

        'all_active_students_total_daily' => $activeStudentsDaily,
        'all_active_students_total_weekly' => $activeStudentsWeekly,
        'all_active_students_total_monthly' => $activeStudentsMonthly,
        'all_active_students_total_anual' => $activeStudentsAnual,
      ];

      // dd($data);

      return view('employeer.dashboard', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
