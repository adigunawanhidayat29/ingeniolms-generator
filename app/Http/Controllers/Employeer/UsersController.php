<?php

namespace App\Http\Controllers\Employeer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CorporateUser;
use App\CorporateUserEmployeer;
use App\User;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        
        $corporate_id = CorporateUserEmployeer::where('user_id', $user_id)->value('corporate_id');

        $users = CorporateUser::join('users', 'corporate_users.user_id', '=', 'users.id')
        ->select('users.name', 'users.email', 'users.username')
        ->where('corporate_users.corporate_id', $corporate_id)
        ->orderBy('users.name')
        ->get();

        $data = [
            'users' => $users
        ];

        return view('employeer.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::join('users_groups', 'users.id', '=', 'users_groups.user_id')
        ->select('users.name', 'users.id')
        ->where('users_groups.level_id', 3)
        ->where('users.is_employeed', null)
        ->orderBy('users.name')
        ->get();

        $data = [
            'users' => $users
        ];

        return view('employeer.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        
        $corporate_id = CorporateUserEmployeer::where('user_id', $user_id)->value('corporate_id');
        
        $user = new CorporateUser;
        $user->corporate_id = $corporate_id;
        $user->user_id = $request->user_id;
        $user->save();

        $user_status = User::find($request->user_id);
        $user_status->is_employeed = '1';
        $user_status->save();

        return redirect('/employeer/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
