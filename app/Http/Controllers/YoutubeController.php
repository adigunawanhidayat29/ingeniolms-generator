<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperYoutube;
use App\CourseBroadcast;
use Session;

class YoutubeController extends Controller
{
  public function playlistCreate(){
    $playlist = HelperYoutube::playlistCreate($playlistTitle = 'Judul Playlist', $playlistDescription = 'Description Playlist', 'http://dataquest-quiz.dev/youtube/playlistCreate');
    // dd( $playlist); $playlist['id'] // to get response playlist id
  }

  public function videoCreate(){
    $videoCreate = HelperYoutube::videoCreate($videoPath = 'testvid.mp4', $videoTitle = 'Judul Video', $videoDescripion = 'Video Dwescription', $videoTag = ['learning'], $videoCategoryId = '22', $videoStatus = 'unlisted', $redirectUrl = 'http://dataquest-quiz.dev/youtube/playlistCreate');
    dd( $videoCreate);
  }

  public function playlistItemCreate(){
    $playlistItemCreate = HelperYoutube::playlistItemCreate($playlistId = 'PLNvYMAstpcLfXa7mNW9j5zjsFeV7lOw8P', $videoId = 'k5YfHW83cFM', $redirectUrl = 'http://dataquest-quiz.dev/youtube/playlistItemCreate');
    dd ($playlistItemCreate);
  }

  public function streaming(Request $request){
    if($request->title){
      Session::put('title', $request->title);
      Session::put('description', $request->description);
      Session::put('course_id', $request->course_id);
    }

    $streaming = HelperYoutube::streaming();
    // return $streaming;
    if($streaming['status'] == '401'){
      return redirect($streaming['authUrl']);
    }else{
      $CourseBroadcast = new CourseBroadcast;
      $CourseBroadcast->title = Session::get('title');
      $CourseBroadcast->description = Session::get('description');
      $CourseBroadcast->embed_html = $streaming['bindBroadcastResponse']['contentDetails']['monitorStream']['embedHtml'];
      $CourseBroadcast->youtube_id = $streaming['broadcastsResponse']['id'];
      $CourseBroadcast->stream_url = $streaming['streamsResponse']['cdn']['ingestionInfo']['ingestionAddress'];
      $CourseBroadcast->stream_name = $streaming['streamsResponse']['cdn']['ingestionInfo']['streamName'];
      $CourseBroadcast->course_id = Session::get('course_id');
      $CourseBroadcast->status = '1';
      $CourseBroadcast->save();
      return redirect('course/streaming/' . $CourseBroadcast->id . '/' . str_slug(Session::get('title')));
    }
  }

  public function streaming_complete(Request $request){
    $CourseBroadcast = CourseBroadcast::where('id', $request->id);
    $CourseBroadcast->status = '0';
    $CourseBroadcast->save();
    return $CourseBroadcast;
  }

  public function screen_sharing(){
    return view('broadcast.screen-sharing');
  }

}
