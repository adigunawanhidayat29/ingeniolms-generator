<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use App\Meeting;
use App\MeetingRecording;
use App\MeetingSchedule;
use App\CourseChat;
use App\Course;
use App\CourseUser;
use Auth;
use Storage;
use Illuminate\Support\Facades\DB;

class BigBlueButtonController extends Controller
{
  public function get(){

    $bbb = new BigBlueButton();
    $response = $bbb->getMeetings();

    if ($response->getReturnCode() == 'SUCCESS') {
      dd($response->getRawXml()->meetings->meeting);
    	// foreach ($response->getRawXml()->meetings->meeting as $meeting) {
    	// 	// process all meeting
    	// }
    }
  }

  public function create($course_id, $MeetingScheduleId){

    $Meeting = Meeting::where('course_id', $course_id)->first();
    $MeetingSchdule = MeetingSchedule::where('id', $MeetingScheduleId)->first();

    $bbb = new BigBlueButton();
    $createMeetingParams = new CreateMeetingParameters($meetingID = $MeetingSchdule->id, $meetingName = $Meeting->name);
    $createMeetingParams->setAttendeePassword($attendee_password = $Meeting->password);
    $createMeetingParams->setModeratorPassword($moderator_password = $Meeting->password_instructor);
    $createMeetingParams->setDuration($duration = "50:00");
    $createMeetingParams->setLogoutUrl($urlLogout = url('/bigbluebutton/logout/'.$course_id));

    if($MeetingSchdule->recording == '1') {
    	$createMeetingParams->setRecord(true);
    	$createMeetingParams->setAllowStartStopRecording(true);
    	$createMeetingParams->setAutoStartRecording(true);
    }

    $response = $bbb->createMeeting($createMeetingParams);
    if ($response->getReturnCode() == 'FAILED') {
    	return 'Can\'t create room! please contact our administrator.';
    } else {
    	// process after room created
    }
  }

  public function joinInstructor($course_id, $MeetingScheduleId){
    $this->create($course_id, $MeetingScheduleId);

    $Meeting = Meeting::where('course_id', $course_id)->first();
    $MeetingSchdule = MeetingSchedule::where('id', $MeetingScheduleId)->first();
    $user = Auth::user();
    $bbb = new BigBlueButton();

    $joinMeetingParams = new JoinMeetingParameters($meetingID = $MeetingSchdule->id, $name = $user->name, $password = $Meeting->password_instructor); // $moderator_password for moderator
    $joinMeetingParams->setRedirect(true);
    $joinMeetingParams->setJoinViaHtml5(true);
    $url = $bbb->getJoinMeetingURL($joinMeetingParams);

    $data = [
      'url' => $url,
      'course_id' => $course_id,
    ];

    return redirect($url);
    // return view('meeting.index', $data);
  }

  public function join_mobile($course_id, $user_id, $meeting_schedule_id, Request $request){
    if ($request->get('accesstype')!='mobile') {
      return response()->json([
        'message' => 'error',
        'data' => 'Invalid type or internal error, request failed'
      ]);
    }
    $Meeting = Meeting::where('course_id', $course_id)->first();
    $MeetingSchdule = MeetingSchedule::where('id', $meeting_schedule_id)->first();
    $user = DB::table('users')->where('id', $user_id)->first();
    $bbb = new BigBlueButton();

    $joinMeetingParams = new JoinMeetingParameters($meetingID = $MeetingSchdule->id, $name = $user->name, $password = $Meeting->password); // $moderator_password for moderator
    $joinMeetingParams->setRedirect(true);
    $joinMeetingParams->setJoinViaHtml5(true);
    $url = $bbb->getJoinMeetingURL($joinMeetingParams);

    $data = [
      'url' => $url,
      'course_id' => $course_id,
    ];

    $course_user = CourseUser::where(['course_id' => $course_id, 'user_id' => $user_id]);
    $course = Course::where('id', $course_id)->first();

    if($course_user->first()){ // check jika user sudah join course
        return redirect($url);
      }else{
        return redirect('course/' . $course->slug);
      }
  }

  public function join($course_id, $MeetingScheduleId){
    $Meeting = Meeting::where('course_id', $course_id)->first();
    $MeetingSchdule = MeetingSchedule::where('id', $MeetingScheduleId)->first();
    $user = Auth::user();
    $bbb = new BigBlueButton();

    $joinMeetingParams = new JoinMeetingParameters($meetingID = $MeetingSchdule->id, $name = $user->name, $password = $Meeting->password); // $moderator_password for moderator
    $joinMeetingParams->setRedirect(true);
    $joinMeetingParams->setJoinViaHtml5(true);
    $url = $bbb->getJoinMeetingURL($joinMeetingParams);

    $data = [
      'url' => $url,
      'course_id' => $course_id,
    ];

    $course_user = CourseUser::where(['course_id' => $course_id, 'user_id' => $user->id]);
    $course = Course::where('id', $course_id)->first();

    if($course->id_author == Auth::user()->id){ // check jika user adalah pemilik course
      return redirect('bigbluebutton/meeting/instructor/join/' . $course_id . '/' . $MeetingScheduleId);
    }else{
      if($course_user->first()){ // check jika user sudah join course
        return redirect($url);
      }else{
        return redirect('course/' . $course->slug);
      }
    }

    // return view('meeting.index', $data);
  }

  public function close(){
    $bbb = new BigBlueButton();

    $endMeetingParams = new EndMeetingParameters($meetingID, $moderator_password);
    $response = $bbb->endMeeting($endMeetingParams);
  }

  public function info(){
    $bbb = new BigBlueButton();

    $getMeetingInfoParams = new GetMeetingInfoParameters($meetingID = "123", '', $moderator_password = "password");
    $response = $bbb->getMeetingInfo($getMeetingInfoParams);
    if ($response->getReturnCode() == 'FAILED') {
    	// meeting not found or already closed
    } else {
    	// process $response->getRawXml();
      dd($response->getRawXml());
    }
  }

  public function getRecording(){
    $bbb = new BigBlueButton();
    $recordingParams = new GetRecordingsParameters();
    $recordingParams->setMeetingId(5); // get by meeting id
    $response = $bbb->getRecordings($recordingParams);

    if ($response->getReturnCode() == 'SUCCESS') {
      // dd($response->getRawXml()->recordings);
    	// foreach ($response->getRawXml()->recordings->recording as $recording) {
    	// 	// process all recording
      //   dd($recording);
    	// }
      return $response->getRawXml()->recordings->recording;
    }else{
      return false;
    }

  }

  public function deleteRecording(){
    $bbb = new BigBlueButton();
    $deleteRecordingsParams= new DeleteRecordingsParameters($recordingID); // get from "Get Recordings"
    $response = $bbb->deleteRecordings($deleteRecordingsParams);

    if ($response->getReturnCode() == 'SUCCESS') {
    	// recording deleted
    } else {
    	// something wrong
    }
  }

  public function uploadRecord($course_id){
    $Meeting = Meeting::where('course_id', $course_id)->first();
    $data = ['meeting' => $Meeting];
    return view('meeting.upload', $data);
  }

  public function saveRecord($course_id, Request $request){
    $Course = Course::where('id', $course_id)->first();
    $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
    $record = 'Recording-'.str_slug($Course->title).'-'.rand(111,9999).'.'.$extension; // renameing image
    $folder = $Course->folder_path; // get folder course by section id
    Storage::disk('google')->put($folder.'/'.$record, fopen($request->file('file'), 'r+'));//upload google drive
    $path_file = get_asset_path($folder, $record);

    $Meeting = Meeting::where('course_id', $course_id)->first();
    $MeetingRecording = new MeetingRecording;
    $MeetingRecording->meeting_id = $Meeting->id;
    $MeetingRecording->record = "https://drive.google.com/file/d/".$path_file;
    $MeetingRecording->status = '0';
    $MeetingRecording->save();

    \Session::flash('success', 'Video Record uploaded');
    return redirect('bigbluebutton/meeting/upload/record/success/');
  }

  public function successUploadRecord(){
    return view('meeting.upload_success');
  }

  public function logout($course_id){
    $course = Course::where(['id' => $course_id])->first();
    if($course->id_author == Auth::user()->id){
      return redirect('course/preview/'.$course_id);
    }else{
      return redirect('course/learn/'. $course->slug);
    }
  }
}
