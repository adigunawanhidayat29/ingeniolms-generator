<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Transaction;
use App\TransactionDetail;
use App\Instructor;
use App\InstructorBalance;
use App\InstructorWithdrawal;
use App\InstructorBank;
use Auth;
use Session;

class InstructorTransactionController extends Controller
{
  public function index(){
    $user = Auth::user();
    $instructor = Instructor::where('user_id', $user->id)->first();

    $debit = 0;
    $credit = 0;
    $instructor_balances = InstructorBalance::where('instructor_id', $instructor->id)->get();
    foreach ($instructor_balances as $key => $instructor_balance) {
      $debit += $instructor_balance->debit;
      $credit += $instructor_balance->credit;
    }
    $balance = $debit - $credit;

    $TransactionDetails = TransactionDetail::select('users.*', 'courses.*', 'transactions_details.*', 'transactions.status as transaction_status')
      ->join('courses', 'courses.id', '=', 'transactions_details.course_id')
      ->where('courses.id_author', $user->id)
      ->join('transactions', 'transactions.invoice', '=', 'transactions_details.invoice')
      ->join('users', 'users.id', '=', 'transactions.user_id')
      ->orderBy('transactions.created_at', 'desc')
    ;

    $data = [
      'transactions' => $TransactionDetails->get(),
      'balance' => $balance,
    ];
    return view('instructor.transaction', $data);
  }

  public function mutation(){
    $user = Auth::user();
    $instructor = Instructor::where('user_id', $user->id)->first();

    $debit = 0;
    $credit = 0;
    $instructor_balances = InstructorBalance::where('instructor_id', $instructor->id)->get();
    foreach ($instructor_balances as $key => $instructor_balance) {
      $debit += $instructor_balance->debit;
      $credit += $instructor_balance->credit;
    }
    $balance = $debit - $credit;

    $InstructorWithdrawal = InstructorWithdrawal::where('instructor_id',  $instructor->id)->get();

    $data = [
      'mutations' => $instructor_balances,
      'withdrawals' => $InstructorWithdrawal,
      'balance' => $balance,
    ];
    return view('instructor.mutation', $data);
  }

  public function withdrawal(Request $request){

    $Instructor = Instructor::where('user_id', Auth::user()->id)->first();
    $InstructorBalances = InstructorBalance::where('instructor_id', $Instructor->id)
      ->orderBy('id', 'asc');

    $debit = 0;
    $credit = 0;
    $instructor_balances = InstructorBalance::where('instructor_id', $Instructor->id)->get();
    foreach ($instructor_balances as $key => $instructor_balance) {
      $debit += $instructor_balance->debit;
      $credit += $instructor_balance->credit;
    }
    $balance = $debit - $credit;

    //validation
    $validatedData = $request->validate([
        'amount' => 'required|numeric|min:20000|max:'.$balance,
    ]);
    //validation

    $InstructorWithdrawal  = new InstructorWithdrawal;
    $InstructorWithdrawal->instructor_id = $Instructor->id;
    $InstructorWithdrawal->amount = $request->amount;
    $InstructorWithdrawal->save();

    Session::flash('message', 'Permintaan penarikan Anda sedang di proses');
    return redirect()->back();
  }

  public function withdrawal_delete($id){
    $InstructorWithdrawal = InstructorWithdrawal::where('id', $id)->delete();

    Session::flash('message', 'Permintaan penarikan Anda dihapus');
    return redirect()->back();
  }

  public function account_bank(Request $request){
    $Instructor = Instructor::where('user_id', Auth::user()->id)->first();
    $InstructorBank = InstructorBank::where('instructor_id', $Instructor->id)->first();

    if(!$InstructorBank){
      // NEW BANK ACCOUNT
      $InstructorBank = new InstructorBank;
      $InstructorBank->instructor_id = $Instructor->id;
      $InstructorBank->bank_name = $request->bank_name;
      $InstructorBank->account_number = $request->account_number;
      $InstructorBank->account_name = $request->account_name;
      $InstructorBank->bank_branch = $request->bank_branch;
      $InstructorBank->save();
      // NEW BANK ACCOUNT
    }else{
      // UPDATE BANK ACCOUNT
      $InstructorBank->bank_name = $request->bank_name;
      $InstructorBank->account_number = $request->account_number;
      $InstructorBank->account_name = $request->account_name;
      $InstructorBank->bank_branch = $request->bank_branch;
      $InstructorBank->save();
      // UPDATE BANK ACCOUNT
    }

    return redirect()->back();
  }

}
