<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, Cart;
use App\Transaction;
use App\AffiliateTransaction;
use App\AffiliateUser;
use App\AffiliateSetting;
use App\TransactionDetail;
use App\TransactionConfirmation;
use App\CourseUser;
use App\Promotion;
use App\PromotionUser;
use Cookie;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionInformation;

use App\Veritrans\Midtrans;

class SnapController extends Controller
{
  public function __construct()
  {
    Midtrans::$serverKey = 'SB-Mid-server-sQN1lKORkckM0TpWyn__k1cj';
    //set is production to true for production mode
    Midtrans::$isProduction = false;
  }

  public function snap()
  {
    return view('snap_checkout');
  }

  public function token()
  {
    //error_log('masuk ke snap token dri ajax');
    $midtrans = new Midtrans;

    $invoice = time();
    $user = Auth::user();
    $subtotal = 0;
    foreach (Cart::content() as $cart) {
      $subtotal += $cart->price;
    }

    // Populate items
    $items = [];


    foreach (Cart::content() as $cart) {
      array_push($items, [
        'id'        => $cart->id,
        'price'     => $cart->price,
        'quantity'  => $cart->qty,
        'name'      => $cart->name
      ]);
    }

    if (\Session::get('promotion')) {
      array_push($items, [

        'id'        => \Session::get('promotion')->id,
        'price'     => - ($subtotal * \Session::get('promotion')->discount) / 100,
        'quantity'  => 1,
        'name'      => "DISK " . \Session::get('promotion')->name

      ]);
    }

    if (\Session::get('promotion')) {
      $price_discount = ($subtotal * \Session::get('promotion')->discount) / 100;
      $subtotal = $subtotal - $price_discount;
    }

    $transaction_details = array(
      'order_id'      => $invoice,
      'gross_amount'  => $subtotal
    );

    // Populate customer's billing address
    $billing_address = array(
      'first_name'    => $user->name,
      'last_name'     => "",
      'address'       => "",
      'city'          => "",
      'postal_code'   => "",
      'phone'         => $user->phone,
      'country_code'  => 'IDN'
    );

    // Populate customer's shipping address
    $shipping_address = array(
      'first_name'    => $user->name,
      'last_name'     => "",
      'address'       => "",
      'city'          => "",
      'postal_code'   => "",
      'phone'         => $user->phone,
      'country_code'  => 'IDN'
    );

    // Populate customer's Info
    $customer_details = array(
      'first_name'      => $user->name,
      'last_name'       => "",
      'email'           => $user->email,
      'phone'           => $user->phone,
      // 'billing_address' => $billing_address,
      // 'shipping_address'=> $shipping_address
    );

    // Data yang akan dikirim untuk request redirect_url.
    $credit_card['secure'] = true;
    //ser save_card true to enable oneclick or 2click
    //$credit_card['save_card'] = true;

    $time = time();
    $custom_expiry = array(
      'start_time' => date("Y-m-d H:i:s O", $time),
      'unit'       => 'hour',
      'duration'   => 10
    );

    $enabled_payments =  [
      "credit_card",
      "mandiri_clickpay",
      "cimb_clicks",
      "bca_klikbca",
      "bca_klikpay",
      "bri_epay",
      "telkomsel_cash",
      "echannel",
      "indosat_dompetku",
      "mandiri_ecash",
      "permata_va",
      "bca_va",
      "bni_va",
      "other_va",
      "kioson",
      "indomaret",
      "gci",
      "danamon_online"
    ];

    $transaction_data = array(
      'transaction_details' => $transaction_details,
      'item_details'       => $items,
      'customer_details'   => $customer_details,
      'expiry'             => $custom_expiry,
      'credit_card'        => $credit_card,
      // 'enabled_payments'   => $enabled_payments,
    );

    try {
      $snap_token = $midtrans->getSnapToken($transaction_data);
      //return redirect($vtweb_url);
      echo $snap_token;
    } catch (Exception $e) {
      return $e->getMessage;
    }
  }

  public function finish(Request $request)
  {
    $result = $request->input('result_data');
    $result = json_decode($result);
    // echo $result->status_message . '<br>';
    // echo 'RESULT <br><pre>';
    // var_dump($result);
    // echo '</pre>' ;

    $payment_type = $result->payment_type;
    $status_code = $result->status_code; // 200 = success, 201 = pending,
    $pdf_url = isset($result->pdf_url) ? $result->pdf_url : '';
    $order_id = $result->order_id;
    $transaction_id = $result->transaction_id;
    $transaction_status = $result->transaction_status;

    $user = Auth::user();
    $invoice = $order_id;
    // $unique_number = rand(100,999);
    $cookie_affiliate = Cookie::get('affiliate');

    if ($cookie_affiliate) {
      $affiliate = '?aff=' . $cookie_affiliate;
      $AffiliateSetting = AffiliateSetting::where('status', '1')->orderBy('id', 'desc')->first();
      $AffiliateUser = AffiliateUser::where('url', $affiliate)->first();
      $AffiliateTransaction = new AffiliateTransaction;
      $AffiliateTransaction->affiliate_id = $AffiliateUser->id;
      $AffiliateTransaction->user_id = $user->id;
      $AffiliateTransaction->invoice = $invoice;
      $AffiliateTransaction->status = '0';
      $AffiliateTransaction->commission = $AffiliateSetting->commission;
      $AffiliateTransaction->save();
    }

    //insert transaction detail
    $subtotal = 0;
    foreach (Cart::content() as $cart) {
      $subtotal += $cart->price;
      $TransactionDetail = new TransactionDetail;
      $TransactionDetail->invoice = $invoice;
      if ($cart->options->type == 'course') {
        $TransactionDetail->course_id = $cart->id;
      } else {
        $TransactionDetail->program_id = $cart->id;
      }
      $TransactionDetail->total = $cart->price;
      $TransactionDetail->save();
    }
    //insert transaction detail

    if (Session::get('promotion')) {
      $Promotion = Session::get('promotion');
      $PromotionUser = new PromotionUser;
      $PromotionUser->promotion_id = $Promotion->id;
      $PromotionUser->user_id = $user->id;
      $PromotionUser->invoice = $invoice;
      $PromotionUser->save();

      $discount_price = ($subtotal * $Promotion->discount) / 100;
      //insert transaction
      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->user_id = $user->id;
      $Transaction->method = $payment_type;
      $Transaction->method_details = $pdf_url;
      $Transaction->total = ($subtotal - $discount_price);
      $Transaction->subtotal = ($subtotal - $discount_price);
      // $Transaction->unique_number = $unique_number;
      $Transaction->promotion = '1';
      $Transaction->promotion_id = $Promotion->id;
      $Transaction->discount_code = $Promotion->discount_code;
      $Transaction->status = $Promotion->discount == 100 ? '1' : '0';
      $Transaction->save();
      //insert transaction

      $request->session()->forget('promotion'); // delete session promo

      if ($Promotion->discount == 100) {

        foreach (Cart::content() as $cart) {
          if ($cart->options->type == 'course') {
            $TransactionDetail->course_id = $cart->id;
            $CourseUser = new CourseUser;
            $CourseUser->user_id = Auth::user()->id;
            $CourseUser->course_id = $cart->id;
            $CourseUser->save();
          }
        }

        Cart::destroy();
        \Session::flash('message', 'success');
        return redirect('my-course');
      }
    } else {
      //insert transaction
      $Transaction = new Transaction;
      $Transaction->invoice = $invoice;
      $Transaction->user_id = $user->id;
      $Transaction->method = $payment_type;
      $Transaction->method_details = $pdf_url;
      $Transaction->total = $subtotal;
      $Transaction->subtotal = $subtotal;
      // $Transaction->unique_number = $unique_number;
      $Transaction->promotion = '0';
      $Transaction->affiliate_id = $cookie_affiliate ? $AffiliateUser->id : NULL;
      $Transaction->status = '0';
      $Transaction->save();
      //insert transaction
    }

    // Mail::to($user->email)->send(new TransactionInformation($Transaction));
    Cart::destroy();

    if ($status_code == '200') {

      return redirect('transaction/success/information/v2/' . $invoice);
    } else {
      return redirect('transaction/information/v2/' . $invoice);
    }
  }

  public function notification()
  {
    $midtrans = new Midtrans;
    echo 'test notification handler';
    $json_result = file_get_contents('php://input');
    $result = json_decode($json_result);

    if ($result) {
      $notif = $midtrans->status($result->order_id);
    }

    error_log(print_r($result, TRUE));

    /*
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          if ($type == 'credit_card'){
            if($fraud == 'challenge'){
              // TODO set payment status in merchant's database to 'Challenge by FDS'
              // TODO merchant should decide whether this transaction is authorized or not in MAP
              echo "Transaction order_id: " . $order_id ." is challenged by FDS";
              }
              else {
              // TODO set payment status in merchant's database to 'Success'
              echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
              }
            }
          }
        else if ($transaction == 'settlement'){
          // TODO set payment status in merchant's database to 'Settlement'
          echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
          }
          else if($transaction == 'pending'){
          // TODO set payment status in merchant's database to 'Pending'
          echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
          }
          else if ($transaction == 'deny') {
          // TODO set payment status in merchant's database to 'Denied'
          echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }*/
  }
}
