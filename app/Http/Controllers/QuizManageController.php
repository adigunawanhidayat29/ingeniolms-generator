<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Section;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\QuizPageBreak;
use App\Course;
use App\CourseStudentGroup;
use App\GroupHasContent;
use Lang;

class QuizManageController extends Controller
{
  public function create($id){
    $section = Section::where('id', $id)->first();

    $data = [
      'action' => 'course/quiz/create_action/'.$section->id,
      'method' => 'post',
      'button' => Lang::get('front.content_create.create'),
      'id' => old('id'),
      'name' => old('name'),
      'description' => old('description'),
      'shuffle' => old('shuffle'),
      'time_start' => old('time_start'),
      'time_end' => old('time_end'),
      'duration' => old('duration','120'),
      'attempt' => old('attempt','1'),
      'section' => $section,
      'course' => Course::where('id', $section->id_course)->first(),
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get(),
      'group_has_content' => [],
    ];
    return view('course.quiz_form', $data);
  }

  public function create_action($section_id, Request $request){

    // insert Quiz
    $Quiz = new Quiz;

    $Quiz->name = $request->name;
    $Quiz->description = $request->description;
    $Quiz->slug = str_slug($request->name);
    // $Quiz->shuffle = $request->shuffle;
    if($request->time_start_required){
      $Quiz->time_start = $request->date_start ." ". $request->time_start;
      $Quiz->time_start_required = 1;
    }else {
      $Quiz->time_start = Date('Y-m-d') ." ". Date('H:i');
    }

    if($request->time_end_required){
      $Quiz->time_end = $request->date_end ." ". $request->time_end;
      $Quiz->time_end_required = 1;
    }else {
      $Quiz->time_end = Date('Y-m-d') ." ". Date('H:i');
    }

    if($request->duration_required){
      $Quiz->duration = $request->duration;
      $Quiz->duration_required = 1;
    }else {
      $Quiz->duration = 0;
    }

    if($request->grade_required){
      $Quiz->grade = $request->grade;
      $Quiz->grade_required = 1;
    }else {
      $Quiz->grade = 0;
    }
    $Quiz->attempt = $request->attempt;
    $Quiz->id_section = $section_id;
    $Quiz->status = $request->publish; // 1= publish, 2 =draft
    $Quiz->save();
    // insert Quiz

    // default page brak
    $QuizPageBreak = new QuizPageBreak;
    $QuizPageBreak->quiz_id = $Quiz->id;
    $QuizPageBreak->title = 'Page 1';
    $QuizPageBreak->save();
    // default page brak

    // insert grouphascontent if isset
    if (isset($request->course_student_group)) {
      foreach ($request->course_student_group as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $Quiz->id, 'table' => 'quizzes', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'quizzes';
          $GroupHasContent->table_id = $Quiz->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
    }
    // insert grouphascontent if isset

    \Session::flash('success', 'Create Quiz success');
    return redirect('course/quiz/question/manage/'.$Quiz->id);
    // return redirect()->back();
  }

  public function update($id){
    $Quiz = Quiz::where('id', $id)->first();
    $section = Section::where('id', $Quiz->id_section)->first();

    $data = [
      'action' => 'course/quiz/update_action/',
      'method' => 'put',
      'button' => 'Update',
      'id' => old('id', $Quiz->id),
      'name' => old('name', $Quiz->name),
      'description' => old('description', $Quiz->description),
      'shuffle' => old('shuffle', $Quiz->shuffle),
      'time_start' => old('time_start', $Quiz->time_start),
      'time_end' => old('time_end', $Quiz->time_end),
      'duration' => old('duration', $Quiz->duration),
      'attempt' => old('attempt', $Quiz->attempt),
      'section' => $section,
      'course' => Course::where('id', $section->id_course)->first(),
      'course_student_groups' => CourseStudentGroup::where('course_id', $section->id_course)->get(),
      'group_has_content' => GroupHasContent::where(['table' => 'quizzes', 'table_id' => $id])->get(),
    ];
    return view('course.quiz_form', $data);
  }

  public function update_action(Request $request){
    $Quiz = Quiz::where('id', $request->id)->first();
    $Section = Section::select('id_course')->where('id', $Quiz->id_section)->first();
    
    $Quiz->name = $request->name;
    $Quiz->slug = str_slug($request->name);
    $Quiz->description = $request->description;
    $Quiz->paginate_type = $request->paginate_type;
    $Quiz->paginate = $request->paginate;
    // $Quiz->shuffle = $request->shuffle;
    if($request->time_start_required){
      $Quiz->time_start = $request->date_start ." ". $request->time_start;
      $Quiz->time_start_required = 1;
    }else {
      $Quiz->time_start_required = 0;
    }

    if($request->time_end_required){
      $Quiz->time_end = $request->date_end ." ". $request->time_end;
      $Quiz->time_end_required = 1;
    }else {
      $Quiz->time_end_required = 0;
    }

    if($request->duration_required){
      $Quiz->duration = $request->duration;
      $Quiz->duration_required = 1;
    }else {
      $Quiz->duration_required = 0;
    }

    if($request->grade_required){
      $Quiz->grade = $request->grade;
      $Quiz->grade_required = 1;
    }else {
      $Quiz->grade_required = 0;
    }
    $Quiz->attempt = $request->attempt;
    $Quiz->save();

    // insert grouphascontent if isset
    if (isset($request->course_student_group)) {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $Quiz->id, 'table' => 'quizzes'])->delete();
      foreach ($request->course_student_group as $data) {
        $GroupHasContent = GroupHasContent::where(['table_id' => $Quiz->id, 'table' => 'quizzes', 'course_student_group_id' => $data])->first();
        if (!$GroupHasContent) {
          $GroupHasContent = new GroupHasContent;
          $GroupHasContent->table = 'quizzes';
          $GroupHasContent->table_id = $Quiz->id;
          $GroupHasContent->course_student_group_id = $data;
          $GroupHasContent->save();
        }
      }
    } else {
      $GroupHasContentDelete = GroupHasContent::where(['table_id' => $Quiz->id, 'table' => 'quizzes'])->delete();
    }
    // insert grouphascontent if isset

    \Session::flash('success', 'Update Quiz success');
    // return redirect('course/preview/'.$Section->id_course);
    return redirect()->back();
  }

  public function publish($quiz_id, $course_id){
    $Quiz = Quiz::where('id', $quiz_id)->first();
    $Section = Section::where('id', $Quiz->id_section)->first();
    $Quiz->status = '1';
    $Quiz->save();

    \Session::flash('success', 'Publish Quiz success');
    // return redirect('course/preview/'.$Section->id_course);
    return redirect()->back();
  }

  public function unpublish($quiz_id){
      $Quiz = Quiz::where('id', $quiz_id)->first();
      $Section = Section::where('id', $Quiz->id_section)->first();
      $Quiz->status = '0';
      $Quiz->save();

      \Session::flash('success', 'Unpublish Quiz success');
      return redirect()->back();
  }

  public function delete($course_id, $id){
    $Quiz = Quiz::where('id', $id);
    if($Quiz->first()){
      $QuizQuestion = QuizQuestion::where('quiz_id', $Quiz->first()->id);
        if($QuizQuestion->first()){
          $QuizQuestionAnswer = QuizQuestionAnswer::where('quiz_question_id', $QuizQuestion->first()->id);
          $QuizQuestionAnswer->delete();
        }
      $QuizQuestion->delete();
      $Quiz->delete();
      \Session::flash('success', 'Delete Quiz success');
    }else{
      \Session::flash('error', 'Delete Quiz failed');
    }
    return redirect('course/preview/'.$course_id);
  }

  public function update_sequence(Request $request){
    foreach($request->datas as $key => $value){
      $QuizPageBreak = QuizPageBreak::find($value);
      $QuizPageBreak->quiz_id = $request->quiz_id;
      $QuizPageBreak->sequence = $key;
      $QuizPageBreak->save();
    }
  }

  public function edit_page_break(Request $request){
    $QuizPageBreak = QuizPageBreak::find($request->id);
    $QuizPageBreak->title = $request->title;
    $QuizPageBreak->description = $request->description;
    $QuizPageBreak->save();
    return redirect()->back();
  }

  public function delete_page_break($id){
    $Question = QuizQuestion::where('quiz_page_break_id', $id)->delete();
    $QuizPageBreak = QuizPageBreak::find($id)->delete();
    return redirect()->back();
  }

}
