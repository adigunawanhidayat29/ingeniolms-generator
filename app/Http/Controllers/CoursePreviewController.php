<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\Content;
use App\Assignment;

class CoursePreviewController extends Controller
{
    public function detail($slug, $content_id){

        $courses = Course::where('slug', $slug)->first();
    
				$Content = Content::where('id', $content_id)->first();
    
				$data = [
					'course' => $courses,
					'content' => $Content,
				];

				return view('course.content_detail', $data);
		}

    public function detail_assignment($slug, $id){

        $courses = Course::where('slug', $slug)->first();
    
				$Content = Assignment::where('id', $id)->first();
    
				$data = [
					'course' => $courses,
					'content' => $Content,
					'assignment' => true
				];

				return view('course.content_detail', $data);
		}
}
