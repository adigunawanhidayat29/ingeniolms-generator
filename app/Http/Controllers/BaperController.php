<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\User;
use App\UserGroup;
use App\Quiz;
use App\QuizQuestionAnswer;
use App\QuizParticipant;
use App\QuizParticipantAnswer;
use App\QuizPageBreak;
use App\Section;
use Auth;
use DataTables;
use PDF;
use Session;

class BaperController extends Controller
{
  public function index(){
    // $baper_quiz = DB::table('baper_quizzes')->where('publish', '1')->first();
    // return view('baper.index', compact('baper_quiz'));

    return redirect('tesbaper/start/128');
  }

  public function session_clusters(Request $request){
    $data = rtrim(implode(',', $request->data), ',');
    Session::put('cluster'.$request->question_id, $data);
    echo $request->question_id;

    // $data1 = Session::get('cluster.642'); // 642,634,644 adalah question_id
    // $data2 = Session::get('cluster.643'); // 642,634,644 adalah question_id
    // $data3 = Session::get('cluster.644'); // 642,634,644 adalah question_id
    //
    // $data_all = [];
    // $data_array = [$data1, $data2, $data3];
    //
    // array_push($data_all, $data_array);
    // dd($data_array);


    // foreach($data_array as $value){
    //   return $implode_data = implode(',', $value);
      // foreach($value as $key => $data){
      // }
      // $answers_clusters = [];
      // DB::statement(DB::raw('set @rownum=0'));
      // $question_answers = DB::table('quiz_question_answers')
      // ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
      // ->whereIn('quiz_question_answers.id', explode(',', $data))
      // ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
      // ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
      // // ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, ".implode(',', $value).")"))
      // ->get();
      // foreach($question_answers as $index => $question_answer){
      //   array_push($answers_clusters, [
      //     'total' => $index + 1,
      //     'cluster' => $question_answer->cluster_name,
      //     'cluster_id' => $question_answer->cluster_id,
      //   ]);
      // }
    // }
    //
    // dd($answers_clusters);

  }

  public function step1(){
    $id = 128;
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->paginate(1);

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    $data = [
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $quiz_page_breaks,
    ];

    return view('baper.step1', $data);
  }
  public function step2(){

    $id = 128;
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->skip(1)->take(3)->get();

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    $data = [
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $quiz_page_breaks,
    ];

    return view('baper.step2', $data);
  }
  public function step3(){

    $id = 128;
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->skip(4)->take(4)->get();

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    $data1 = Session::get('cluster642'); // 642,634,644 adalah question_id
    $data2 = Session::get('cluster643'); // 642,634,644 adalah question_id
    $data3 = Session::get('cluster644'); // 642,634,644 adalah question_id

    $data_all = [];
    $data_array = [$data1, $data2, $data3];

    // array_push($data_all, $data_array);
    // dd($data_all);

    $answers_clusters = [];
    foreach($data_array as $value){
      // echo $value . '<br>';
      DB::statement(DB::raw('set @rownum=0'));
      $question_answers = DB::table('quiz_question_answers')
      ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
      ->whereIn('quiz_question_answers.id', explode(',', $value))
      ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
      ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
      ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $value)"))
      ->get();
      foreach($question_answers as $index => $question_answer){
        array_push($answers_clusters, [
          'total' => $index + 1,
          'cluster' => $question_answer->cluster_name,
          'cluster_id' => $question_answer->cluster_id,
        ]);
      }
    }
    // dd($answers_clusters);

    //===  grouping by cluster id
    $group_clusters = array();
    foreach($answers_clusters as $key => $item){
      $group_clusters[$item['cluster_id']][] = $item;
    }
    // dd($group_clusters);
    //===  grouping by cluster id

    // === totaling answer cluster
    $result_clusters = [];
    for($i=1; $i<=14; $i++){
      $total = 0;
      for($j=0; $j<=2; $j++){
        $total += $group_clusters[$i][$j]['total'];
      }
      array_push($result_clusters, [
        'cluster_id' => $group_clusters[$i][0]['cluster_id'],
        'cluster' => $group_clusters[$i][0]['cluster'],
        'total' => $total,
      ]);
    }
    // === totaling answer cluster

    //=== sort by total asc
    usort($result_clusters, function($first_value, $second_value) {
        return $first_value['total'] <=> $second_value['total'];
    });
    // dd($result_clusters);
    //=== sort by total asc

    // get data from clusters session
    $clusters_id = [];
    foreach($result_clusters as $index => $value){
      if($index < 4){
        array_push($clusters_id, $value['cluster_id']);
      }
    }
    // dd($clusters_id);
    $baper_cluster_questions = DB::table('baper_cluster_questions')->whereIn('cluster_id', $clusters_id)->get();
    $baper_cluster_question_id = [];
    foreach($baper_cluster_questions as $data){
      array_push($baper_cluster_question_id, $data->quiz_question_id);
    }
    // dd($baper_cluster_question_id);
    // get data from clusters session

    $quiz_page_breaks = QuizPageBreak::select('quiz_page_breaks.*')->where('quiz_page_breaks.quiz_id', $id)
      ->join('quiz_questions', 'quiz_questions.quiz_page_break_id', '=', 'quiz_page_breaks.id')
      ->whereIn('quiz_questions.id', $baper_cluster_question_id)
      ->with('quiz_questions')
      ->orderBy('quiz_page_breaks.sequence', 'asc')
      // ->skip(4)
      // ->take(4)
      ->get();
      // dd($quiz_page_breaks);

    $data = [
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $quiz_page_breaks,
      'baper_cluster_question_id' => $baper_cluster_question_id,
    ];

    return view('baper.step3', $data);
  }
  public function step4(){

    $id = 128;
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->skip(18)->take(8)->get();

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    $data = [
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $quiz_page_breaks,
    ];

    return view('baper.step4', $data);
  }

  public function start($id){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $section = Section::find($quiz->id_section);
    $all_quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->get();
    $quiz_page_breaks = QuizPageBreak::where('quiz_id', $id)->with('quiz_questions')->orderBy('sequence', 'asc')->paginate(1);

    $QuizParticipant_check = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();
    if(!$QuizParticipant_check){
      // insert quiz participant
      $QuizParticipant = new QuizParticipant;
      $QuizParticipant->quiz_id = $quiz->id;
      $QuizParticipant->user_id = $user->id;
      $QuizParticipant->time_start = date("Y-m-d H:i:s");
      $QuizParticipant->save();
      // insert quiz participant
      $quiz_time_start = $QuizParticipant->time_start;
    }
    elseif(QuizParticipantAnswer::where(array('quiz_participant_id' => $QuizParticipant_check->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{
      $quiz_time_start = $QuizParticipant_check->time_start;
    }

    $data = [
      'quiz' => $quiz,
      'all_quiz_page_breaks' => $all_quiz_page_breaks,
      'quiz_time_start' => $quiz_time_start,
      'quiz_page_breaks' => $quiz_page_breaks,
    ];
    // return view('baper.start', $data);

    return redirect('tesbaper/step-1');
  }

  public function finish($id, Request $request){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $quiz_participant = QuizParticipant::where(array('quiz_id' => $quiz->id, 'user_id' => $user->id))->first();

    if(QuizParticipantAnswer::where(array('quiz_participant_id' => $quiz_participant->id))->first()){
      return redirect('tesbaper/result/'.$id); // jika sudah mengikuti quiz redirect ke result
    }else{

      $BaperQuizCode = DB::table('baper_quiz_code')->insert([
        'code' => $request->BaperCode,
        'user_id' => $user->id,
        'baper_quiz_id' => '1',
      ]);

      // update time end
      $quiz_participant->time_end = date("Y-m-d H:i:s");
      $quiz_participant->save();
      // update time end

      $question = $request->questions;
      foreach($question as $index => $value){
        $QuizParticipantAnswer = new QuizParticipantAnswer;
        $QuizParticipantAnswer->quiz_id = $quiz->id;
        $QuizParticipantAnswer->quiz_participant_id = $quiz_participant->id;
        $QuizParticipantAnswer->quiz_question_id = $value;
        $QuizParticipantAnswer->quiz_question_answer_id = isset ($request->answers[$index]) ? rtrim($request->answers[$index], ',') : 0;
        if($request->question_type[$index] == '5'){
          $QuizParticipantAnswer->quiz_question_answer_id = 0;
          $QuizParticipantAnswer->answer_essay = $request->answers[$index];
        }
        if($request->question_type[$index] == '6'){
          $check_short_answer = QuizQuestionAnswer::where('answer', 'like', '%' . $request->answers[$index] . '%')->first();
          if($check_short_answer){
            $QuizParticipantAnswer->quiz_question_answer_id = $check_short_answer->id;
          }else{
            $QuizParticipantAnswer->quiz_question_answer_id = 0;
          }
          $QuizParticipantAnswer->answer_short_answer = $request->answers[$index];
        }
        $QuizParticipantAnswer->save();
      }
    }
  }

  public function result($id){
    $user = Auth::user();
    $quiz = Quiz::where('id', $id)->first();
    $QuizParticipant = QuizParticipant::where(['quiz_id' => $quiz->id, 'user_id' => $user->id])->first();
    $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participant_id', $QuizParticipant->id)
      ->where('baper_quizzes.quiz_id', $quiz->id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
      ->join('baper_quizzes','baper_quizzes.quiz_id','=','quiz_questions.quiz_id');

    $data = [
      'quiz' => $quiz,
      'quiz_participants' => $QuizParticipants->get(),
      'count_question' => $QuizParticipants->count(),
    ];
    return view('baper.result', $data);
  }

  public function report($user_id, $quiz_id){
    $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participants.quiz_id', $quiz_id)
      ->where('quiz_participants.user_id', $user_id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_participants','quiz_participants.id','=','quiz_participant_answers.quiz_participant_id')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
      ->join('baper_quizzes','baper_quizzes.quiz_id','=','quiz_questions.quiz_id')
      ->get();

    $Quiz = Quiz::find($QuizParticipants->first()->quiz_id);

    // === get data clusters
    $answers_clusters = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '3'){

          $sum_number = 0;
          DB::statement(DB::raw('set @rownum=0'));
          $question_answers = DB::table('quiz_question_answers')
            ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
            ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
            ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
            ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
            ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
            ->get();
            foreach($question_answers as $index => $question_answer){
              array_push($answers_clusters, [
                'total' => $index + 1,
                'cluster' => $question_answer->cluster_name,
                'cluster_id' => $question_answer->cluster_id,
              ]);
            }
      }
    }
    // dd($total_clusters);
    // === get data clusters

    //===  grouping by cluster id
    $group_clusters = array();
    foreach($answers_clusters as $key => $item){
      $group_clusters[$item['cluster_id']][] = $item;
    }
    // dd($group_clusters);
    //===  grouping by cluster id

    // === totaling answer cluster
    $result_clusters = [];
    for($i=1; $i<=14; $i++){
      $total = 0;
      for($j=0; $j<=2; $j++){
        $total += $group_clusters[$i][$j]['total'];
      }
      array_push($result_clusters, [
        'cluster_id' => $group_clusters[$i][0]['cluster_id'],
        'cluster' => $group_clusters[$i][0]['cluster'],
        'total' => $total,
      ]);
      // var_dump($arr[$i]);
    }
    // === totaling answer cluster

    //=== sort by total asc
    usort($result_clusters, function($first_value, $second_value) {
        return $first_value['total'] <=> $second_value['total'];
    });
    // dd($result_clusters);
    //=== sort by total asc

    $answers_majors = [];
    foreach($result_clusters as $index => $value){
      if($index < 4){
        foreach($QuizParticipants as $index => $quiz_participant){
          if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2'){
            $major = DB::table('baper_cluster_major_answers')
              ->select('baper_cluster_major_answers.id', 'baper_cluster_majors.major_name')
              ->join('baper_cluster_majors', 'baper_cluster_majors.id', '=', 'baper_cluster_major_answers.cluster_major_id')
              ->where('baper_cluster_majors.cluster_id', $value['cluster_id'])
              ->where('quiz_answer_id', $quiz_participant->quiz_question_answer_id)
              ->first();
              if($major){
                array_push($answers_majors, ['major_name' => $major->major_name]);
                // var_dump($major->major_name);
              }
          }
        }
      }
    }
    // dd($answers_majors);

    $answers_likert = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '7'){
        $multiple_intellegencies = DB::table('baper_multiple_intellegencies')->select('baper_multiple_intellegencies.*')
          ->join('baper_multiple_intellegency_questions', 'baper_multiple_intellegency_questions.multiple_intellegency_id', '=', 'baper_multiple_intellegencies.id')
          ->where('baper_multiple_intellegency_questions.question_id', $quiz_participant->quiz_question_id)
          ->groupBy('baper_multiple_intellegencies.id')
          ->first();
        if($multiple_intellegencies){
          array_push($answers_likert, [
            'multiple_intellegency_id' => $multiple_intellegencies->id,
            'multiple_intellegency_name' => $multiple_intellegencies->name,
            'answer' => $quiz_participant->answer,
            'answer_id' => $quiz_participant->quiz_question_answer_id,
            'question_id' => $quiz_participant->quiz_question_id
          ]);
          // var_dump($major->major_name);
        }
      }
    }
    // dd($answers_likert);
    // $sum_answers = 0;
    $group_likerts = array();
    foreach($answers_likert as $data){
      $group_likerts[$data['multiple_intellegency_id']][] = $data;
    }
    // dd($group_likerts);
    $result_likerts = [];
    for($i=1; $i<=8; $i++){
      // dd( $group_likerts[$i]);
      $total = 0;
      for($j=0; $j<=9; $j++){
        $total += $group_likerts[$i][$j]['answer'];
      }
      array_push($result_likerts, [
        'multiple_intellegency_id' => $group_likerts[$i][0]['multiple_intellegency_id'],
        'multiple_intellegency_name' => $group_likerts[$i][0]['multiple_intellegency_name'],
        'total' => $total,
      ]);
    }
    // dd($result_likerts);

    // get biodata
    $biodata = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '6'){
        array_push($biodata, [
          'data' => $quiz_participant->answer_short_answer
        ]);
      }
    }
    // get biodata

    $data = [
      'quiz_participants' => $QuizParticipants,
      'answers_majors' => $answers_majors,
      'result_clusters' => $result_clusters,
      'result_likerts' => $result_likerts,
      'biodata' => $biodata,
      'quiz' => $Quiz,
    ];

    return view('baper.report', $data);
  }

  public function report_pengajar($quiz_id){
    if(Auth::user()->email == 'baperplus@ingenio.co.id' || Auth::user()->email == 'admin@ingenio.co.id'){
      return view('baper.report_pengajar');
    }else{
      return abort(404);
    }

  }

  public function report_pengajar_serverside($quiz_id, Request $request){
    if($request->ajax()){

      $data = QuizParticipant::select('users.name', 'quiz_participants.created_at','quiz_participants.quiz_id', 'quiz_participants.quiz_id', 'quiz_participants.user_id', 'baper_quiz_code.code')
        ->where('baper_quizzes.quiz_id', $quiz_id)
        ->join('baper_quizzes', 'baper_quizzes.quiz_id', '=', 'quiz_participants.quiz_id')
        ->leftJoin('baper_quiz_code', 'baper_quiz_code.user_id', '=', 'quiz_participants.user_id')
        ->join('users', 'users.id', '=', 'quiz_participants.user_id')
        // ->groupBy('users.name', 'baper_quiz_code.code')
        ->orderBy('quiz_participants.created_at', 'desc')
        ->get();

      return DataTables::of($data)

      ->addColumn('action', function ($data)
      {
        return '
          <a href="/tesbaper/report/'.$data->user_id.'/'.$data->quiz_id.'">Lihat Hasil</a> |
          <a target="_blank" href="/tesbaper/download/report/'.$data->user_id.'/'.$data->quiz_id.'">Download</a>
        ';
      })->make(true);
    }else{
      exit("Not an AJAX request");
    }
  }

  public function report_download($user_id, $quiz_id){
    $QuizParticipants = QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participants.quiz_id', $quiz_id)
      ->where('quiz_participants.user_id', $user_id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_participants','quiz_participants.id','=','quiz_participant_answers.quiz_participant_id')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
      ->join('baper_quizzes','baper_quizzes.quiz_id','=','quiz_questions.quiz_id')
      ->get();

    $Quiz = Quiz::find($QuizParticipants->first()->quiz_id);

    // === get data clusters
    $answers_clusters = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '3'){

          $sum_number = 0;
          DB::statement(DB::raw('set @rownum=0'));
          $question_answers = DB::table('quiz_question_answers')
            ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
            ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
            ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
            ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
            ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
            ->get();
            foreach($question_answers as $index => $question_answer){
              array_push($answers_clusters, [
                'total' => $index + 1,
                'cluster' => $question_answer->cluster_name,
                'cluster_id' => $question_answer->cluster_id,
              ]);
            }
      }
    }
    // dd($total_clusters);
    // === get data clusters

    //===  grouping by cluster id
    $group_clusters = array();
    foreach($answers_clusters as $key => $item){
      $group_clusters[$item['cluster_id']][] = $item;
    }
    // dd($group_clusters);
    //===  grouping by cluster id

    // === totaling answer cluster
    $result_clusters = [];
    for($i=1; $i<=14; $i++){
      $total = 0;
      for($j=0; $j<=2; $j++){
        $total += $group_clusters[$i][$j]['total'];
      }
      array_push($result_clusters, [
        'cluster_id' => $group_clusters[$i][0]['cluster_id'],
        'cluster' => $group_clusters[$i][0]['cluster'],
        'total' => $total,
      ]);
      // var_dump($arr[$i]);
    }
    // === totaling answer cluster

    //=== sort by total asc
    usort($result_clusters, function($first_value, $second_value) {
        return $first_value['total'] <=> $second_value['total'];
    });
    // dd($result_clusters);
    //=== sort by total asc

    $answers_majors = [];
    foreach($result_clusters as $index => $value){
      if($index < 4){
        foreach($QuizParticipants as $index => $quiz_participant){
          if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2'){
            $major = DB::table('baper_cluster_major_answers')
              ->select('baper_cluster_major_answers.id', 'baper_cluster_majors.major_name')
              ->join('baper_cluster_majors', 'baper_cluster_majors.id', '=', 'baper_cluster_major_answers.cluster_major_id')
              ->where('baper_cluster_majors.cluster_id', $value['cluster_id'])
              ->where('quiz_answer_id', $quiz_participant->quiz_question_answer_id)
              ->first();
              if($major){
                array_push($answers_majors, ['major_name' => $major->major_name]);
                // var_dump($major->major_name);
              }
          }
        }
      }
    }
    // dd($answers_majors);

    $answers_likert = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '7'){
        $multiple_intellegencies = DB::table('baper_multiple_intellegencies')->select('baper_multiple_intellegencies.*')
          ->join('baper_multiple_intellegency_questions', 'baper_multiple_intellegency_questions.multiple_intellegency_id', '=', 'baper_multiple_intellegencies.id')
          ->where('baper_multiple_intellegency_questions.question_id', $quiz_participant->quiz_question_id)
          ->groupBy('baper_multiple_intellegencies.id')
          ->first();
        if($multiple_intellegencies){
          array_push($answers_likert, [
            'multiple_intellegency_id' => $multiple_intellegencies->id,
            'multiple_intellegency_name' => $multiple_intellegencies->name,
            'answer' => $quiz_participant->answer,
            'answer_id' => $quiz_participant->quiz_question_answer_id,
            'question_id' => $quiz_participant->quiz_question_id
          ]);
          // var_dump($major->major_name);
        }
      }
    }
    // dd($answers_likert);
    // $sum_answers = 0;
    $group_likerts = array();
    foreach($answers_likert as $data){
      $group_likerts[$data['multiple_intellegency_id']][] = $data;
    }
    // dd($group_likerts);
    $result_likerts = [];
    for($i=1; $i<=8; $i++){
      // dd( $group_likerts[$i]);
      $total = 0;
      for($j=0; $j<=9; $j++){
        $total += $group_likerts[$i][$j]['answer'];
      }
      array_push($result_likerts, [
        'multiple_intellegency_id' => $group_likerts[$i][0]['multiple_intellegency_id'],
        'multiple_intellegency_name' => $group_likerts[$i][0]['multiple_intellegency_name'],
        'total' => $total,
      ]);
    }
    // dd($result_likerts);

    // get biodata
    $biodata = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '6'){
        array_push($biodata, [
          'data' => $quiz_participant->answer_short_answer
        ]);
      }
    }
    // get biodata

    $data = [
      'quiz_participants' => $QuizParticipants,
      'answers_majors' => $answers_majors,
      'result_clusters' => $result_clusters,
      'result_likerts' => $result_likerts,
      'biodata' => $biodata,
      'quiz' => $Quiz,
    ];

    $pdf = PDF::loadView('baper.report_pdf', $data);
    // return $pdf->download('baper-report.pdf');
    return $pdf->stream('baper-report.pdf');
    // return view('baper.report_pdf', $data);

  }

  public function login(){

    return view('baper.login');
  }

  public function login_store(Request $request){

    Auth::attempt(['email' => $request['email'], 'password' => $request['password']]);
    return redirect('client/login?hostname=https://baperplus.co.id');
  }

  public function register(){

    return view('baper.register');
  }

  public function register_store(Request $request){

    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'phone' => 'required|string|min:8|max:20',
      'password' => 'required|string|min:6|confirmed',
    ]);

    $Register = User::create([
      'name' => $request['name'],
      'email' => $request['email'],
      'phone' => $request['phone'],
      'slug' => str_slug($request['name']),
      'password' => bcrypt($request['password']),
      'is_active' => '1',
      'auth_token' => str_random(60),
    ]);

    $UserGroup = new UserGroup;
    $UserGroup->user_id = $Register['id'];
    $UserGroup->level_id = '3';
    $UserGroup->save();

    Auth::attempt(['email' => $request['email'], 'password' => $request['password']]);

    return redirect('client/login?hostname=https://baperplus.co.id');
  }

  public function logout(){
    Auth::logout();
    return redirect('tesbaper/login');
  }

  public function report_pengajar_filter($quiz_id, Request $request){

    $Quiz = Quiz::find($quiz_id);
    $BaperQuizCodes = DB::table('baper_quiz_code')->where('code', $request->code)->get();

    $data = [
      'quiz' => $Quiz,
      'BaperQuizCodes' => $BaperQuizCodes,
    ];

    return view('baper.report_pengajar_filter', $data);
  }

}
