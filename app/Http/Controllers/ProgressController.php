<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Progress;
use Auth;

class ProgressController extends Controller
{
  public function reset(Request $request){
    $user = Auth::user();
    $Progress = Progress::where(['user_id' => $user->id, 'course_id' => $request->course_id])->update(['status' => '0']);    
  }
}
