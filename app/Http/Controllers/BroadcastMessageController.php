<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseBroadcastChat;
use App\Events\BroadcastMessageSent;
use Auth;

class BroadcastMessageController extends Controller
{
  public function save(Request $request){
    $User = Auth::user();
    $CourseChat = new CourseBroadcastChat;
    $CourseChat->course_id = $request->course_id;
    $CourseChat->broadcast_id = $request->broadcast_id;
    $CourseChat->user_id = $User->id;
    $CourseChat->body = $request->body;
    $CourseChat->save();

    broadcast(new BroadcastMessageSent($User, $CourseChat));
  }

  public function get($id){
    $CoursesChats = CourseBroadcastChat::where('course_broadcast_chats.broadcast_id', $id)->join('users','users.id','=','course_broadcast_chats.user_id')->orderby('course_broadcast_chats.id','asc')->get();
    $data = [
      'courses_chats' => $CoursesChats,
    ];
    return view('broadcast.message', $data);
  }
}
