<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use Plupload;
use App\Assignment;

class TestController extends Controller
{
    public function upload(Request $request){
        $assignment_id = 1;
        $User = Auth::user();
        $Assignment = Assignment::where('id', $assignment_id)->first();
        $section_id = $Assignment->id_section;

        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $size_file = $file->getSize(); // getting image extension
        $name_file = str_slug($file->getClientOriginalName()).'-'.rand(111,9999).'.'.$extension; // renameing image
        $folder = get_folderName_course($section_id); // get folder course by section id
        Storage::disk(env('APP_DISK'))->put('dynamic/uploads/' . $folder .'/'. $name_file, fopen($file, 'r+'), 'public'); //upload do
        $path_file = get_asset_newpath('dynamic/uploads/' . $folder, $name_file);

        return response()->json([
            'path' => config('filesystems.disks.'.env('APP_DISK').'.url') . "/" . $path_file,
        ]);
    }
}
