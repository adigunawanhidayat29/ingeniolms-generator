<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstructorGroup;
use App\Application;
use App\User;
use App\LibraryDirectory;
use App\LibraryDirectoryGroup;
use App\InstructorGroupLibrary;
use App\GroupDiscussion;
use App\GroupUpdate;
use App\GroupDiscussionAnswer;
use Auth;
use DB;
use DateTime;
use DateInterval;
use File;

class InstructorGroupController extends Controller
{

  public function index(){
    $data = [
      'instructor_groups' => InstructorGroup::where('user_id', 'LIKE', '%'.Auth::user()->id.'%')->get(),
    ];

    return view('instructor_group.index', $data);
  }

  public function discussion($id){
    $discussion = GroupDiscussion::find($id);

    if(!$discussion){
      return redirect()->back();
    }
    $discussion_list = GroupDiscussionAnswer::where('group_discussion_id', $discussion->id)->get();

    $data = [
      'discussion' => $discussion,
      'discussion_list' => $discussion_list,
    ];

    return view('instructor_group.discussion', $data);
  }

  public function group_update_submit(Request $request){
    $group_update = new GroupUpdate;
    $group_update->group_id = $request->group_id;
    $group_update->user_id = Auth::user()->id;
    $group_update->description = $request->body;
    if($request->parents){
      $group_update->level_1 = $request->parents;

    }

    $new_file = [];
    if($request->fileinput){
      $new_file = $request->fileinput;
    }
    $file = json_encode(array_values($new_file));

    $group_update->file = $file;
    $group_update->save();

    return redirect()->back();
  }

  public function show($id){
    $myLibrary = LibraryDirectory::with(['library' => function($query) { $query->with('content', 'quiz', 'assignment')->where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
      $query->where('user_id', Auth::user()->id);
    }); }])->get();
    $publicLibrary = LibraryDirectory::get();
    $publicLibrary = $publicLibrary[1];

    $folder = LibraryDirectoryGroup::where('user_id', Auth::user()->id)->orWhereHas('shared', function($query){
      $query->where('user_id', Auth::user()->id);
    })->get();

    $group = InstructorGroup::with('library')->whereIn('user_id', [Auth::user()->id])->where('id', '!=', $id)->get();

    $instructor_groups = InstructorGroup::find($id);
    $group_discussions = GroupDiscussion::where('group_id', $id)->get();

    $members = User::whereIn('id', explode(',', $instructor_groups->user_id))->get();
    // dd(explode(',', $instructor_groups->user_id));

    return view('instructor_group.detail', compact('myLibrary', 'folder', 'instructor_groups', 'publicLibrary', 'group', 'id', 'members', 'group_discussions'));
  }

  public function create(){
    $data = [
      'button' => 'Buat',
      'action' => '/instructor/groups/store',
      'title' => old('title'),
      'description' => old('description'),
      'user_id' => old('user_id'),
      'authors' => DB::table('users')->select('users.id', 'users.name', 'users.email')->join('instructors', 'instructors.user_id', '=', 'users.id')->where('users.id', '!=', Auth::user()->id)->get(),
    ];
    return view('instructor_group.form', $data);
  }

  public function add_resource(Request $request, $id){
    foreach ($request->content as $key => $value) {
      $content = explode('/', $value);
      if($content[1] == 'folder'){
        $library_group = InstructorGroupLibrary::where('instructor_group_id', $id)->where('folder_id', $content[0])->first();
        if(!$library_group){
          $library_group = new InstructorGroupLibrary;
          $library_group->instructor_group_id = $id;
          $library_group->folder_id = $content[0];
          $library_group->save();
        }
      }else {
        $library_group = InstructorGroupLibrary::where('instructor_group_id', $id)->where('content_id', $content[0])->first();
        if(!$library_group){
          $library_group = new InstructorGroupLibrary;
          $library_group->instructor_group_id = $id;
          $library_group->content_id = $content[0];
          $library_group->save();
        }
      }
    }

    return redirect()->back();
  }

  public function update_discussion(Request $request, $id){
    $group_discussion = GroupDiscussion::find($id);
    $group_discussion->title = $request->judul_diskusi;
    $group_discussion->description = $request->diskusi_description;
    $group_discussion->save();

    return redirect()->back()->with('message', 'Diskusi berhasil diperbarui');
  }

  public function delete_discussion($id){
    $group_discussion = GroupDiscussion::find($id);
    $group_discussion->answer()->delete();
    $group_discussion->delete();

    return redirect()->back();
  }

  public function update_discussion_detail(Request $request, $id){
    $group_discussion = GroupDiscussionAnswer::find($id);
    $group_discussion->body = $request->body;
    $group_discussion->save();

    return redirect()->back();
  }

  public function delete_discussion_detail($id){
    $group_discussion = GroupDiscussionAnswer::find($id);
    $group_discussion->delete();

    return redirect()->back();
  }

  public function add_discussion(Request $request, $id){
    $group_discussion = new GroupDiscussion;
    $group_discussion->group_id = $id;
    $group_discussion->user_id = Auth::user()->id;
    $group_discussion->title = $request->judul_diskusi;
    $group_discussion->description = $request->diskusi_description;
    $group_discussion->save();


    return redirect()->back();
  }

  public function discussion_list(Request $request, $id){
    $group_discussion = new GroupDiscussionAnswer;
    $group_discussion->group_discussion_id = $id;
    $group_discussion->user_id = Auth::user()->id;
    $group_discussion->body = $request->body;
    $group_discussion->save();


    return redirect()->back();
  }

  public function store(Request $request){
    $InstructorGroup = new InstructorGroup;
    $InstructorGroup->title = $request->title;
    $InstructorGroup->description = $request->description;
    $InstructorGroup->user_id = Auth::user()->id .','. implode(',', $request->user_id);
    $InstructorGroup->created_by = Auth::user()->id;
    $InstructorGroup->group_code = generateGroupCode();
    $InstructorGroup->save();

    // // Create Client Application
    // $date_now = new DateTime('now');
    // $date = new DateTime('now');
    // $expired_date = $date->add(new DateInterval('P1Y'));
    //
    // $Application = new Application;
    // $Application->name = $InstructorGroup->title;
    // $Application->id_instructor_group = $InstructorGroup->id;
    // $Application->created_by = Auth::user()->id;
    // $Application->api_key = str_random(20);
    // $Application->registered_at = $date_now->format('Y-m-d H:i:s');
    // $Application->expired_at = $expired_date->format('Y-m-d H:i:s');
    // $Application->save();

    return redirect('instructor/groups');
  }

  public function edit($id){
    $InstructorGroup = InstructorGroup::find($id);
    $data = [
      'button' => 'Edit',
      'action' => '/instructor/groups/update/' . $InstructorGroup->id,
      'title' => old('title', $InstructorGroup->title),
      'description' => old('description', $InstructorGroup->description),
      'user_id' => old('user_id', $InstructorGroup->user_id),
      'authors' => DB::table('users')->select('users.id', 'users.name', 'users.email')->join('instructors', 'instructors.user_id', '=', 'users.id')->where('users.id', '!=', Auth::user()->id)->get(),
    ];
    return view('instructor_group.form', $data);
  }

  public function update($id, Request $request){
    $InstructorGroup = InstructorGroup::find($id);
    $InstructorGroup->title = $request->title;
    $InstructorGroup->description = $request->description;
    $InstructorGroup->user_id = Auth::user()->id .','. implode(',', $request->user_id);
    $InstructorGroup->created_by = Auth::user()->id;
    $InstructorGroup->save();

    return redirect('instructor/groups');
  }

  public function join_group(Request $request){
    $group = InstructorGroup::where('group_code', $request->group_code)->first();
    $user_id = explode(',', $group->user_id);
    $search = array_search(Auth::user()->id,$user_id);
    if($search === false){
      array_push($user_id, "".Auth::user()->id);

      $group->user_id = implode($user_id, ',');
      $group->save();
    }

    return redirect('instructor/groups/detail/'.$group->id);
  }

  public function instructor_group_content_destroy($id){
    $group = InstructorGroupLibrary::find($id);
    $group->delete();

    return redirect()->back();
  }

  public function group_update_delete($id){
    $update = GroupUpdate::find($id);
    $update->reply_1()->delete();
    $update->delete();

    return redirect()->back();
  }
  
  public function remove_user($id, $user_id) {
    $group = InstructorGroup::where('id', $id)->first();
    $group_users = explode(',', $group->user_id);
    $fix_users = array_diff($group_users, [$user_id]);
    $group->user_id = implode(',', $fix_users);
    $group->save();

    return redirect()->back()->with('message', 'Peserta berhasil dihapus');
  }

}
