<?php

namespace App\Http\Controllers\Sso;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AuthenticatedUser;
use App\Application;
use App\User;
use Auth;
use Hash;
use DateTime, DateInterval;
use App\Http\Controllers\Controller;
use Session;

class LoginController extends Controller{
  public function sso_login(Request $request){
    if ($request->get('type')=='sso') {
      $Application = Application::where(['api_key' => $request->get('api_key'), 'redirect_url' => $request->get('redirect')])->first();
      if($Application){
        if (Auth::check()) {
          $authenticated_user = AuthenticatedUser::where('user_id', Auth::user()->id)->first();
          return redirect()->intended($request->get('redirect').'?auth='.$authenticated_user->token);
        }
        return redirect()->intended(url('login').'?type=sso&api_key='.$request->get('api_key').'&redirect='.$request->get('redirect'));
      }
    }
    return response()->json([
      'error' => 406,
      'message' => 'Invalid Operation, Please Try Again'
    ]);
  }

  public function sso_logout(){
    return response()->json([
      'users' => Auth::check(),
    ]);
    //return redirect()->intended(url('logout').'?type=sso');
  }
}


?>