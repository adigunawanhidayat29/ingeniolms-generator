<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use App\Vcon;
use App\VconWaiting;

class VconController extends Controller
{
    public function index(){
        $Vcons = Vcon::all();
        return view('vcon.index', compact('Vcons'));
    }

    public function waiting($id){
        $Vcons = VconWaiting::select('vcon_waiting.status as status', 'vcons.id as vcon_id', 'vcon_waiting.id as id', 'vcon_waiting.name', 'vcons.password', 'vcons.password_moderator')->where('vcon_waiting.id', $id)
          ->join('vcons', 'vcons.id', '=', 'vcon_waiting.vcon_id')
          ->first();

        if($Vcons->status == '0') {
          return view('vcon.waiting', compact('Vcons'));
        } else {

          $roomID = $Vcons->vcon_id . env('APP_URL', null);
          
          $bbb = new BigBlueButton();
  
          $joinMeetingParams = new JoinMeetingParameters($meetingID = $roomID, $name = $Vcons->name, $Vcons->password);
          $joinMeetingParams->setRedirect(true);
          $joinMeetingParams->setJoinViaHtml5(true);
          $url = $bbb->getJoinMeetingURL($joinMeetingParams);
      
          $data = [
            'url' => $url,
          ];
      
          return redirect($url);

        }
          
    }

    public function waiting_check($id){
        $Vcons = VconWaiting::select('vcon_waiting.status as status', 'vcons.id as vcon_id', 'vcon_waiting.id as id', 'vcon_waiting.name', 'vcons.password', 'vcons.password_moderator')->where('vcon_waiting.id', $id)
          ->join('vcons', 'vcons.id', '=', 'vcon_waiting.vcon_id')
          ->first();

        if($Vcons->status == '1') {
          $roomID = $Vcons->vcon_id . env('APP_URL', null);
          
          $bbb = new BigBlueButton();
  
          $joinMeetingParams = new JoinMeetingParameters($meetingID = $roomID, $name = $Vcons->name, $Vcons->password);
          $joinMeetingParams->setRedirect(true);
          $joinMeetingParams->setJoinViaHtml5(true);
          $url = $bbb->getJoinMeetingURL($joinMeetingParams);
      
          $data = [
            'url' => $url,
          ];
      
          return $url;
        } else {
          return '404';
        }
          
    }

    public function create($request){

      // dd($request);

        $meeting = explode('-', $request->meeting);
        $roomID = $meeting[0] . env('APP_URL');
        $Vcon = Vcon::where('id', $meeting[0])->first();
        // dd($roomID);

        $bbb = new BigBlueButton();
        $createMeetingParams = new CreateMeetingParameters($meetingID = $roomID, $meetingName = $meeting[1]);
        $createMeetingParams->setAttendeePassword($Vcon->password);
        $createMeetingParams->setModeratorPassword($Vcon->password_moderator);
        $createMeetingParams->setDuration("120");
        $createMeetingParams->setLogoutUrl(url('/vcon'));
        
        $createMeetingParams->setRecord(true);
        $createMeetingParams->setAllowStartStopRecording(true);
        $createMeetingParams->setAutoStartRecording(false);

        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED') {
            return 'Can\'t create room! please contact our administrator.';
        } else {
            // process after room created
        }
    }

  public function joinInstructor(Request $request){
    $meeting = explode('-', $request->meeting);
    $Vcon = Vcon::where('id', $meeting[0])->first();
    $roomID = $meeting[0] . env('APP_URL', null);
    // $Vcon->password_moderator;

    if($request->password == $Vcon->password_moderator || $request->password == $Vcon->password){

      // cek apakah vcon diharuskan menunggu approval
      if($Vcon->waiting == '1') {

        // cek apakah user menggunakan password atendee
        if($request->password == $Vcon->password) {

          // save data ke table vcon_waiting
          $vcon_waiting = new VconWaiting;
          $vcon_waiting->vcon_id = $Vcon->id;
          $vcon_waiting->name = $request->name;
          $vcon_waiting->status = 0;
          $vcon_waiting->save();

          return redirect('vcon/waiting/' . $vcon_waiting->id);
        }
      }
      
      $this->create($request);
  
      $bbb = new BigBlueButton();
  
      $joinMeetingParams = new JoinMeetingParameters($meetingID = $roomID, $name = $request->name, $request->password); // $moderator_password for moderator
      $joinMeetingParams->setRedirect(true);
      $joinMeetingParams->setJoinViaHtml5(true);
      $url = $bbb->getJoinMeetingURL($joinMeetingParams);
  
      $data = [
        'url' => $url,
      ];
  
      return redirect($url);
      // return view('meeting.index', $data);
    }else{
      return back()->with('message', 'Password tidak sesuai');
    }
  }

  public function join(Request $request){
    $meeting = explode('-', $request->meeting);
    $bbb = new BigBlueButton();
    $roomID = $meeting[0] . env('APP_URL', null);

    $joinMeetingParams = new JoinMeetingParameters($meetingID = $roomID, $name = $request->name, $password = $request->password); // $moderator_password for moderator
    $joinMeetingParams->setRedirect(true);
    $joinMeetingParams->setJoinViaHtml5(true);
    $url = $bbb->getJoinMeetingURL($joinMeetingParams);

    $data = [
      'url' => $url,
    ];

    // dd($data);

    return redirect($url);          
  }

}
