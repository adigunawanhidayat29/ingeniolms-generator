<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AssignmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $parent = parent::toArray($request);
        $data = [
            "type_text" => $this->type == "1" ? "file" : "text",
            "status_text" => $this->status == "1" ? "publish" : "draft",
        ];
        $merge = array_merge($parent, $data);
        return $merge;
    }
}
