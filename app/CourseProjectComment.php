<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseProjectComment extends Model
{
  public $table = 'course_project_comments';
}
