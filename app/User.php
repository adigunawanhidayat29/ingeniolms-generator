<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use Notifiable, Sluggable, HasApiTokens;

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  protected $fillable = [
    'name', 'email', 'password', 'phone', 'slug', 'activation_token', 'is_active', 'application_registered', 'auth_token', 'username', 'language'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'activation_token', 'auth_token'
  ];

  public function courses_users()
  {
    return $this->hasMany('App\CourseUser', 'user_id', 'id');
  }

  public function getPhotoAttribute()
  {
    // return $this->attributes['photo'] ? asset_url() . $this->attributes['photo'] : asset_url() . 'uploads/users/default.jpg';
    $photo = $this->attributes['photo'];
    $check_photo = explode('/', $photo);
    if ($photo == "") {
      $photo = '/user-default.png';
    }
    if (isset($check_photo[1]) && $check_photo[1] == 'uploads') {
      if ($check_photo[3] == 'default.png') {
        $photo = '/user-default.png';
      } else {
        $photo = $this->attributes['photo'] ? asset_url() . $this->attributes['photo'] : asset('user-default.png');
      }
    } else {
      $photo = $photo;
    }
    return $photo;
  }

  public function sendPasswordResetNotification($token)
  {
    // Your your own implementation.
    $this->notify(new ResetPasswordNotification($token));
  }

  public function total_courses()
  {
    return $this->hasMany('App\Course', 'id_author', 'id');
  }
}
