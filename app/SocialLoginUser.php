<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLoginUser extends Model
{
  protected $table = 'social_login_users';
}
