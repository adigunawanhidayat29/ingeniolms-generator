<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'communities';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'short_description', 'description', 'photo', 'status'];

    public function getPhotoAttribute(){
        return $this->attributes['photo'] ? asset_url($this->attributes['photo']) : asset_url('dynamic/uploads/default.jpg');
    }

    public function Instructors() {
        return $this->hasMany('App\Instructor', 'community_id')->join('users', 'users.id', '=', 'instructors.user_id'); //get all subs. NOT RECURSIVE
    }
}
