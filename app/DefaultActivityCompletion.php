<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class DefaultActivityCompletion extends Model
{

  protected $table = 'default_activity_completion';

}
