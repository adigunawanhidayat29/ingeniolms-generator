<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectory extends Model
{
    public function library(){

      return $this->hasMany('App\LibraryDirectoryContent', 'directory_id');
    }
}
