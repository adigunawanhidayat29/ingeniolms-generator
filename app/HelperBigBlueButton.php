<?php

use Illuminate\Support\Facades\DB;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;

function getRecording($meeting_schdule_id){
  $bbb = new BigBlueButton();
  $recordingParams = new GetRecordingsParameters();
  $recordingParams->setMeetingId($meeting_schdule_id); // get by meeting id
  $response = $bbb->getRecordings($recordingParams);

  if ($response->getReturnCode() == 'SUCCESS') {
    return $response->getRawXml()->recordings->recording;
  }else{
    return false;
  }
}
