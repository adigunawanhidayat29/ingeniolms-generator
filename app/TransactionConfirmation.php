<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionConfirmation extends Model
{
  protected $table = 'transactions_confirmations';
}
