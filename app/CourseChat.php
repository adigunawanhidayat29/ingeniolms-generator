<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseChat extends Model
{
  protected $table = 'courses_chats';
}
