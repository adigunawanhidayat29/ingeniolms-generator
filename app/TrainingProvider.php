<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingProvider extends Model
{
    protected $table = 'training_providers';
}
