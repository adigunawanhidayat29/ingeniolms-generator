<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\User;
use App\CourseUser;
use App\UserGroup;
use Hash;
use DB;

class CourseUserImport implements ToCollection, WithHeadingRow
{

    private $course_id;

    public function __construct(int $course_id) 
    {
        $this->course_id = $course_id;
    }

    public function collection(Collection $rows)
    {   

        foreach ($rows as $row) 
        {
            if($row->filter()->isNotEmpty()){
                
                $User = User::where('email', $row['email'])->first();
                if($User){
                    if(!CourseUser::where(['user_id' => $User->id, 'course_id' => $this->course_id])->first()){
                        CourseUser::create([
                            'user_id' => $User->id,
                            'course_id' => $this->course_id
                        ]);        
                    }
                }else{
                    User::create([
                        'username' => $row['username'],
                        'name'     => $row['name'],
                        'email'    => $row['email'], 
                        'is_active'=> '1', 
                        'password' => Hash::make($row['password']),            
                    ]);

                    $user_id = DB::getPdo()->lastInsertId();
        
                    UserGroup::create([
                        'level_id' => '3',
                        'user_id' => $user_id
                    ]);

                    CourseUser::create([
                        'user_id' => $user_id,
                        'course_id' => $this->course_id
                    ]);    
                }
                
            }
        }

        
    }
}
