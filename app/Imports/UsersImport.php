<?php

namespace App\Imports;

use App\User;
use App\UserGroup;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Hash;
use DB;

class UsersImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {     
        foreach ($rows as $row) 
        {
            if($row->filter()->isNotEmpty()){
                if(!User::where('email', $row['email'])->first()){
                    User::create([
                        'username' => $row['username'],
                        'name'     => $row['first_name'] . " " . $row['last_name'],
                        'email'    => $row['email'], 
                        'is_active'=> '1', 
                        'password' => Hash::make($row['password']),            
                    ]);
        
                    UserGroup::create([
                        'level_id' => '3',
                        'user_id' => DB::getPdo()->lastInsertId()
                    ]);
                }
            }
        }

        
    }
}
