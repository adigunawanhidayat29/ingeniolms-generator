<?php

namespace App\Imports;

use App\User;
use App\UserGroup;
use App\Cohort;
use App\CohortUser;
use App\CourseUser;
use App\Course;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Hash;
use DB;

class UsersImportAll implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {     
        foreach ($rows as $row) 
        {
            if($row->filter()->isNotEmpty()){
                if(!User::where('username', $row['username'])->first()){
                    User::create([
                        'username' => $row['username'],
                        'name'     => $row['name'],
                        'email'    => $row['email'],
                        'is_active'=> '1',
                        'password' => Hash::make($row['password']),            
                    ]);

                    $userId = DB::getPdo()->lastInsertId();
        
                    UserGroup::create([
                        'level_id' => '3',
                        'user_id' => $userId
                    ]);

                    // cohort
                    $dataCohort = explode(',', $row['cohort']);
                    foreach ($dataCohort as $data) {
                        $Cohort = Cohort::where('code', $data)->first();
                        if ($Cohort) {
                            $checkCohortUser = CohortUser::where(['cohort_id' => $Cohort->id, 'user_id' => $userId])->first();
                            if (!$checkCohortUser) {
                                $CohortUser = new CohortUser;
                                $CohortUser->cohort_id = $Cohort->id;
                                $CohortUser->user_id = $userId;
                                $CohortUser->save();
                            }
                        } else {
                            $newCohort = new Cohort;
                            $newCohort->name = $data;
                            $newCohort->code = $data;
                            $newCohort->status = '1';
                            $newCohort->save();

                            $newCohortUser = new CohortUser;
                            $newCohortUser->cohort_id = $newCohort->id;
                            $newCohortUser->user_id = $userId;
                            $newCohortUser->save();
                        }
                    }

                    // courses
                    $dataCourse = explode(',', $row['course']);
                    foreach ($dataCourse as $data) {
                        $Course = Course::where('shortname', $data)->first();
                        if ($Course) {
                            $checkCourseUser = CourseUser::where(['course_id' => $Course->id, 'user_id' => $userId])->first();
                            if (!$checkCourseUser) {
                                $CourseUser = new CourseUser;
                                $CourseUser->course_id = $Course->id;
                                $CourseUser->user_id = $userId;
                                $CourseUser->save();
                            }
                        }
                    }
                }
            }
        }

        
    }
}
