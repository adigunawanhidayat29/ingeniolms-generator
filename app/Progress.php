<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{

  protected $table = 'progresses';

  public function user() {
    return $this->hasOne('App\User', 'id', 'user_id');
  }
}
