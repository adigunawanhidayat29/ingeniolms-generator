<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentGrade extends Model
{
  protected $table = 'assignments_grades';
}
