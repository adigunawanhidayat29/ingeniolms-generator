<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerIdUser extends Model
{
    protected $table = 'player_id_users';
}
