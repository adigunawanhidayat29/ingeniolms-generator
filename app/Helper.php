<?php

use Illuminate\Support\Facades\DB;
// use DateTime;

function ArrayToJson($array)
{
  return json_decode(json_encode($array));
}

// Character Checking (For domain name checking)

function CharacterValidation($text)
{
  $forbiddenCharacters = ['.', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '=', '+', "'", '\"', '<', '>', ',', '/', '?', ';', ':'];
  $valid = true;
  $charLen = count($forbiddenCharacters);
  for ($i = 0; $i < $charLen; $i++) {
    if (strpos($text, $forbiddenCharacters[$i])) {
      $valid = false;
    }
  }
  return $valid;
}

function time_elapsed_string($datetime, $full = false)
{
  $now = new DateTime;
  $ago = new DateTime($datetime);
  $diff = $now->diff($ago);

  $diff->w = floor($diff->d / 7);
  $diff->d -= $diff->w * 7;

  $string = array(
    'y' => 'year',
    'm' => 'month',
    'w' => 'week',
    'd' => 'day',
    'h' => 'hour',
    'i' => 'minute',
    's' => 'second',
  );
  foreach ($string as $k => &$v) {
    if ($diff->$k) {
      $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    } else {
      unset($string[$k]);
    }
  }

  if (!$full) $string = array_slice($string, 0, 1);
  return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function ClientAssetUrl($path)
{
  return 'https://client-assets.ingenio.co.id/upload/' . $path;
}

function DomainValidation($text)
{
  $forbiddenCharacters = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '=', '+', "'", '\"', '<', '>', ',', '/', '?', ';', ':'];
  $valid = true;
  $charLen = count($forbiddenCharacters);
  for ($i = 0; $i < $charLen; $i++) {
    if (strpos($text, $forbiddenCharacters[$i])) {
      $valid = false;
    }
  }
  return $valid;
}

function dataContent($item)
{
  if ($item->content) {
    return $item->content;
  }

  if ($item->quiz) {
    return $item->quiz;
  }

  if ($item->assignment) {
    return $item->assignment;
  }
}

function generateCode($length = 5)
{

  $randomString = '';
  //number
  $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }

  return $randomString;
}

/*
  this function will return 3 different.
  value 0 -> 'Access Granted' code 'Success',
  value 1 -> 'API KEY Not Registered code 'Error',
  value 2 -> 'API KEY Expired' code 'Error,
  value 3 -> 'Your API KEY has Blocked by Administrator' code 'Error'

*/

function isApiRegistered($api_key)
{
  $credentials = DB::table('applications')->where(['api_key' => $api_key])->first();
  if ($credentials) {
    if ($credentials->blocked > 0) {
      return 3;
    }
    $date_now = new DateTime('now');
    $date_expired = new DateTime($credentials->expired_at);
    return $date_now < $date_expired ? 0 : 2;
  }
  return 1;
}

function api_base_url($any)
{
  return 'https://api.ingenio.co.id/api/' . $any;
}

function asset_path($any = null)
{
  return  base_path() . env('ASSET_PATH', '../../ingeniolms-assets') . '/' . $any;
}

function asset_static_path($any = null)
{
  return  env('ASSET_STATIC_PATH', '../../ingeniolms-assets') . '/' . $any;
}

function asset_url($any = null)
{
  // return "https://assets.ingeniolms.com/".$any;
  // if (env('APP_DISK') == 'do_spaces') {
  //   return env('ASSET_PATH_CLOUD', "https://assets.ingeniolms.com") . '/' .$any;
  // } else {
  //   return env('ASSET_PATH_LOCAL', "https://assets.ingeniolms.com") . '/' .$any;
  // }
  // dd(\Storage::disk('local'));
  return config('filesystems.disks.' . env('APP_DISK') . '.url') . '/' . $any;
}

function photo_url($any = null)
{
  $photo = $any;
  $check_photo = explode('/', $photo);
  if ($check_photo[1] == 'uploads') {
    $photo = $any ? asset_url() . $any : asset_url() . 'uploads/users/default.jpg';
  } else {
    $photo = $photo;
  }

  return $photo;
}

function content_type_icon($type)
{
  if ($type == "video" || $type == "url-video") {
    $icon = "fa fa-play-circle";
  } elseif ($type == "" || $type == null) {
    $icon = "fa fa-star";
  } elseif ($type == "discussion") {
    $icon = "fa fa-wechat";
  } elseif ($type == "folder") {
    $icon = "fa fa-folder";
  } else {
    $icon = "fa fa-file";
  }

  return $icon;
}

function formatSizeUnits($bytes)
{
  if ($bytes >= 1073741824) {
    $bytes = number_format($bytes / 1073741824, 2) . ' GB';
  } elseif ($bytes >= 1048576) {
    $bytes = number_format($bytes / 1048576, 2) . ' MB';
  } elseif ($bytes >= 1024) {
    $bytes = number_format($bytes / 1024, 2) . ' KB';
  } elseif ($bytes > 1) {
    $bytes = $bytes . ' bytes';
  } elseif ($bytes == 1) {
    $bytes = $bytes . ' byte';
  } else {
    $bytes = '0 bytes';
  }

  return $bytes;
}

function get_asset_path($folder_path, $filename)
{
  $dir = '/';
  $recursive = false; // Get subdirectories also?
  $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
  $files = collect(Storage::disk('google')->listContents($folder_path, false))
    ->where('type', '=', 'file');
  $filename = explode('.', $filename);
  foreach ($files as $file) {
    if ($file['filename'] == $filename[0]) {
      $path = explode('/', $file['path']);
    }
  }
  return $path[1];
}

function get_asset_newpath($folder_path, $filename)
{
  $dir = '/';
  $recursive = false; // Get subdirectories also?
  $contents = collect(Storage::disk(env('APP_DISK'))->listContents($dir, $recursive));
  $files = collect(Storage::disk(env('APP_DISK'))->listContents($folder_path, false));
  $filename = explode('.', $filename);
  $oath = '';
  foreach ($files as $file) {
    if ($file['filename'] == $filename[0]) {
      $path = $file['path'];
    }
  }
  return $path;
}

function get_asset_user_path($folder_path, $filename)
{
  $dir = '/';
  $recursive = false; // Get subdirectories also?
  $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
  $files = collect(Storage::disk('google')->listContents($folder_path, false))
    ->where('type', '=', 'file');
  $filename = explode('.', $filename);
  foreach ($files as $file) {
    if ($file['filename'] == $filename[0]) {
      $path = explode('/', $file['path']);
    }
  }
  return $path[1];
}

function get_folder_course($section_id)
{
  $course = DB::table('sections')
    ->where('sections.id', $section_id)
    ->join('courses', 'courses.id', '=', 'sections.id_course')
    ->first();
  return $course->folder_path;
}

// function get_foldername_course($section_id){
//   $course = DB::table('sections')
//             ->where('sections.id', $section_id)
//             ->join('courses', 'courses.id', '=', 'sections.id_course')
//             ->first();
//   return $course->folder;
// }

function get_folderName_course($section_id)
{
  $course = DB::table('sections')
    ->where('sections.id', $section_id)
    ->join('courses', 'courses.id', '=', 'sections.id_course')
    ->first();
  return $course->folder;
}

function get_folderName_user($user_id)
{
  return DB::table('users')->where(['id' => $user_id])->first()->user_folder_name;
}

function server_assets_path($folder = null)
{
  return '/var/www/html/ingenio-assets/uploads/videos/' . $folder;
}

function server_user_assets_path($folder = null)
{
  return '/var/www/html/ingenio-assets/uploads/videos/user-folder/' . $folder;
}

function server_assets()
{
  return 'http://159.69.35.86/ingenio-assets';
}

function get_folder_path_course($folder_name)
{
  $dir = '/';
  $recursive = false; // Get subdirectories also?
  $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
  $dir = $contents->where('type', '=', 'dir')
    ->where('filename', '=', $folder_name)
    ->first(); // There could be duplicate directory names!
  return $dir['path'];
}

function get_user_folder_path($user_id)
{
  $dir = '/';
  $user = DB::table('users')->where(['id' => $user_id])->first();
  $recursive = false; // Get subdirectories also?
  $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
  $dir = $contents->where('type', '=', 'dir')
    ->where('filename', '=', $user->user_folder_name)
    ->first(); // There could be duplicate directory names!
  return $dir['path'];
}

function rupiah($price = 0)
{
  // return "Rp" . number_format($price, 0, ',', '.');
  return "RM" . number_format($price, 0, ',', '.');
}

function isCreator($user_id)
{
  $Creator = DB::table('users_groups')->where(['user_id' => $user_id, 'level_id' => '2']);
  if ($Creator->first()) {
    return true;
  } else {
    return false;
  }
}

function isInstructor($user_id)
{
  $Instructor = DB::table('instructors')->where(['user_id' => $user_id, 'status' => '1']);
  if ($Instructor->first()) {
    return true;
  } else {
    return false;
  }
}

function isAdmin($user_id)
{
  $Users = DB::table('users_groups')->where(['user_id' => $user_id, 'level_id' => '1']);
  if ($Users->first()) {
    return true;
  } else {
    return false;
  }
}

function isEmployeer($user_id)
{
  $Users = DB::table('users_groups')->where(['user_id' => $user_id, 'level_id' => '6']);
  if ($Users->first()) {
    return true;
  } else {
    return false;
  }
}

function isTrainingProvider($user_id)
{
  $Users = DB::table('users_groups')->where(['user_id' => $user_id, 'level_id' => '7']);

  if ($Users->first()) {
    return true;
  } else {
    return false;
  }
}

function isTeacher($user_id)
{
  $Users = DB::table('users_groups')->where(['user_id' => $user_id, 'level_id' => '5']);
  if ($Users->first()) {
    return true;
  } else {
    return false;
  }
}

function isManager($user_id)
{
  $Manager = DB::table('managers')->where(['user_id' => $user_id, 'status' => '1']);
  if ($Manager->first()) {
    return true;
  } else {
    return false;
  }
}

function isAffiliate($user_id)
{
  $AffiliateUser = DB::table('affiliate_users')->where(['user_id' => $user_id, 'status' => '1']);
  if ($AffiliateUser->first()) {
    return $AffiliateUser->first();
  } else {
    return false;
  }
}

function avatar($photo)
{
  // if ($photo != null) {
  //   $check_photo = explode('/', $photo);
  //   if ($check_photo[0] == 'dynamic') {
  //     $photo = asset_url($photo);
  //   } else {
  //     $photo = $photo;
  //   }
  //   return $photo;
  // } else {
  //   return '';
  // }

  $check_photo = explode('/', $photo);
  if ($photo == "") {
    $photo = '/user-default.png';
  }
  if (isset($check_photo[1]) && $check_photo[1] == 'uploads') {
    if ($check_photo[3] == 'default.png') {
      $photo = '/user-default.png';
    } else {
      $photo = $photo ? asset_url() . $photo : asset('user-default.png');
    }
  } else {
    $photo = $photo;
  }
  return $photo;
}

function course_image_small($image)
{
  $explodeSource = explode('/', $image);
  $smallImage = 'small_' . $explodeSource[3];
  return $explodeSource[0] . "/" . $explodeSource[1] . "/" . $explodeSource[2] . "/" . $smallImage;
}

function course_image_medium($image)
{
  $explodeSource = explode('/', $image);
  $smallImage = 'medium_' . $explodeSource[3];
  return $explodeSource[0] . "/" . $explodeSource[1] . "/" . $explodeSource[2] . "/" . $smallImage;
}

function moota($url)
{
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, 'https://app.moota.co/api/v1/' . $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_HTTPHEADER, [
    'Accept: application/json',
    'Authorization: Bearer b0msRPDuV9q3XJOZyynkhoQ9JSUb1KldKIFCyj6ttxwqhkqQTO'
  ]);
  $response = curl_exec($curl);
  return $response;
}

function is_affiliate($user_id = null)
{
  $AffiliateUser = DB::table('affiliate_users')
    ->where('user_id', $user_id)
    // ->where('status', '1')
    ->first();

  if ($AffiliateUser) {
    return $AffiliateUser;
  } else {
    return false;
  }
}

function is_liveCourse($course_id)
{
  $MeetingSchedule = DB::table('meeting_schedules')
    ->where(['course_id' => $course_id, 'date' => date("Y-m-d")])
    ->first();

  $CourseLive = DB::table('course_lives')
    ->where('course_id', $course_id)
    ->where('start_date', '<=', date("Y-m-d"))
    ->where('end_date', '>=', date("Y-m-d"));
  if ($MeetingSchedule || $CourseLive->first()) {
    return true;
  } else {
    return false;
  }
}

function InstructorCompleteProfile()
{
  $User = Auth::user();
  $Instructor = DB::table('instructors')->where('user_id', $User->id)->first();
  $InstructorBank = DB::table('instructor_banks')->where('instructor_id', $Instructor->id);

  if ($User->phone == NULL || !$InstructorBank->first()) {
    return false;
  } else {
    return true;
  }
}

function getClientInfoGoogleDrive()
{
  $headers_a = array(
    'Content-type: application/x-www-form-urlencoded',
    'Accept: application/x-www-form-urlencoded',
  );
  $fields_a = 'client_secret=' . env('GOOGLE_DRIVE_CLIENT_SECRET') . '&grant_type=refresh_token&refresh_token=' . env('GOOGLE_DRIVE_REFRESH_TOKEN') . '&client_id=' . env('GOOGLE_DRIVE_CLIENT_ID');

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_a);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS,  $fields_a);
  $result = curl_exec($ch);
  curl_close($ch);
  $client_info = json_decode($result);
  // dd($client_info);

  return $client_info;
}

function getVideoGoogleDrive($path)
{

  $client_info = getClientInfoGoogleDrive();

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://www.googleapis.com/drive/v2/files/" . $path,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Authorization: Bearer " . $client_info->access_token
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  $data = json_decode($response);
  return $data;
}

function playContent($contentId)
{
  $headers_a = array(
    'Content-type: application/x-www-form-urlencoded',
    'Accept: application/x-www-form-urlencoded',
    /*'Authorization: Basic NjkwZGJmN2ItY2MyNy00NDM1LWE0MTMtNGI1OTg5Y2EyMmQz'*/
  );
  $fields_a = 'client_secret=' . env('GOOGLE_DRIVE_CLIENT_SECRET') . '&grant_type=refresh_token&refresh_token=' . env('GOOGLE_DRIVE_REFRESH_TOKEN') . '&client_id=' . env('GOOGLE_DRIVE_CLIENT_ID');

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/oauth2/v3/token');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_a);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS,  $fields_a);
  $result = curl_exec($ch);
  curl_close($ch);

  $client_info = json_decode($result);

  $headers_b = array(
    'Content-type: application/json',
    'Accept: application/json',
    'Authorization: Bearer ' . $client_info->access_token
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/drive/v2/files/' . $contentId);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_b);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  $content = curl_exec($ch);
  curl_close($ch);
  $url = json_decode($content);

  $playUrl = explode("&", $url->downloadUrl)[0] . '&' . explode("&", $url->downloadUrl)[2] . '&access_token=' . $client_info->access_token;
  $fileSize = $url->fileSize;
  return [
    'type_content' => 'video',
    'uri' => $playUrl,
    'fileSize' => $fileSize
  ];
}

function generateRandomString($length = 8)
{
  $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function generateGroupCode($length = 5)
{
  $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';

  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }

  $randomString .= '-';

  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }

  return $randomString;
}

function course_users($course_id)
{
  $Course = DB::table('courses_users')->where('course_id', $course_id)->get()->count();
  return $Course;
}

function sms($phone, $message = null)
{
  $userkey = "knp4il";
  $passkey = "k33psm1l3";
  $message = str_replace(" ", "%20", $message); // remove space to %20
  return file_get_contents("https://reguler.zenziva.net/apps/smsapi.php?userkey=" . $userkey . "&passkey=" . $passkey . "&nohp=" . $phone . "&pesan=" . $message);
}

function Setting($param)
{
  $setting = DB::table('settings')->where('name', $param)->first();
  if ($setting) {
    return $setting;
  } else {
    return '';
  }
}

function getYoutubeDuration($vid)
{
  $vid = explode('/', $vid);
  $vid = $vid[3];
  //$vid - YouTube video ID. F.e. LWn28sKDWXo
  $videoDetails = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=" . $vid . "&part=contentDetails,statistics&key=AIzaSyC1qnx101nz-Yr-kCQgrL93cWXX0F5OFww");
  $VidDuration = json_decode($videoDetails, true);
  foreach ($VidDuration['items'] as $vidTime) {
    $VidDuration = $vidTime['contentDetails']['duration'];
  }

  try {

    $pattern = '/PT(\d+)H(\d+)M(\d+)S/';
    preg_match($pattern, $VidDuration, $matches);

    // dd($matches);
    // $seconds=$matches[1]*60+$matches[2];

    $di = new DateInterval($VidDuration);
    $string = '';

    if ($di->h > 0) {
      $string .= $di->h . ':';
    }

    return $string . $di->i . ':' . $di->s;

    // return $seconds;

  } catch (\Exception $e) {

    return '';
  }
}

function group_has_content($table, $tableId)
{
  $GroupHasContent = \DB::table('group_has_contents')
    ->where(['table' => $table, 'table_id' => $tableId])
    ->join('course_student_groups', 'course_student_groups.id', '=', 'group_has_contents.course_student_group_id')
    ->get();

  $GroupHasContentName = [];
  foreach ($GroupHasContent as $data) {
    $GroupHasContentName[] = $data->name;
  }

  if (count($GroupHasContentName) > 0) {
    $data = [
      'name' => implode(',', $GroupHasContentName)
    ];

    return $data;
  } else {

    $data = ['name' => null];
    return $data;
  }
}

function get_courses($id_author)
{
    $courses = DB::table('courses')->join('training_provider_trainees', 'courses.id_author', '=', 'training_provider_trainees.user_id')
    ->join('users', 'training_provider_trainees.user_id', '=', 'users.id')
    ->where('courses.is_approved', null)
    ->where('courses.status', '0')
    ->where('courses.id_author', $id_author)
    ->select('courses.title', 'courses.id_author', 'courses.id', 'users.name')
    ->orderBy('courses.title')
    ->get();

    return $courses;
}

function get_courses_approved($id_author)
{
    $courses = DB::table('courses')->join('training_provider_trainees', 'courses.id_author', '=', 'training_provider_trainees.user_id')
    ->join('users', 'training_provider_trainees.user_id', '=', 'users.id')
    ->where('courses.is_approved', '1')
    ->where('courses.id_author', $id_author)
    ->select('courses.title', 'courses.id_author', 'courses.id', 'users.name')
    ->orderBy('courses.title')
    ->get();

    return $courses;
}

function is_inTrainingProvider($user_id)
{
  $training_provider_trainees = DB::table('training_provider_trainees')->where('user_id', $user_id)->count();

  return $training_provider_trainees;
}

function contents($idcourse, $idsection)
{

  $contents = DB::table('contents')->join('sections', 'contents.id_section', '=', 'sections.id')
  ->where('contents.id_section', $idsection)
  ->where('sections.id_course', $idcourse)
  ->select('sections.title', 'contents.title', 'contents.description', 'contents.type_content', 'contents.id')->get();

  return $contents;
}

function get_content($idcontent)
{
  $content = DB::table('contents')->where('id', $idcontent)->value('title');

  return $content;
}

function get_notifications($iduser)
{
  $notifications = DB::table('notifications_user')->where('user_id', $iduser)->get();

  return $notifications;
}

function get_criteria()
{
  $criterias = DB::table('badges_criteria')->get();

  return $criterias;
}

function get_contents($badge_id)
{
  $contents = DB::table('badges_content_assign')->join('contents', 'badges_content_assign.content_id', '=', 'contents.id')
  ->where('badges_content_assign.badge_id', $badge_id)
  ->select('contents.title as content_name')
  ->get();

  return $contents;
}

function get_contents_rule($leveling_rule_id)
{
  $contents = DB::table('leveling_rules_content')->join('contents', 'leveling_rules_content.content_id', '=', 'contents.id')
  ->where('leveling_rules_id', $leveling_rule_id)
  ->select('contents.title as content_name', 'leveling_rules_content.rules_criteria_id as rules_criteria_id')
  ->get();

  return $contents;
}

function get_rules_criteria($rules_criteria_id)
{
  $criterias = DB::table('rules_criteria')->where('id', $rules_criteria_id)->first();

  return $criterias;
}

function get_next_level($course_id, $user_level)
{
  $next_user_level = $user_level + 1;

  $data_level = DB::table('course_leveling')->where('course_id', $course_id)->where('level', $next_user_level)->first();

  if(!empty($data_level))
  {
    return $data_level;
  }

  else
  {
    $data_level = DB::table('course_leveling')->where('course_id', $course_id)->where('level', $user_level)->first();

    return $data_level;
  }
}

function get_current_level($course_id, $user_level)
{
  $data_level = DB::table('course_leveling')->where('course_id', $course_id)->where('level', $user_level)->first();

  return $data_level;
}
