<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SsoAuth extends Model
{
    protected $table = 'sso_auths';
}
