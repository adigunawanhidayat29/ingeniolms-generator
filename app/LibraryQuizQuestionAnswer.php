<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryQuizQuestionAnswer extends Model
{
    protected $table = 'library_quiz_question_answers';
}
