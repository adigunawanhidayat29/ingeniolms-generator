<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectoryContent extends Model
{
    public function content(){

      return $this->belongsTo('App\UserLibrary', 'content_id');
    }

    public function quiz(){

      return $this->belongsTo('App\LibraryQuiz', 'quiz_id');
    }

    public function assignment(){

      return $this->belongsTo('App\LibraryAssignment', 'assignment_id');
    }

    public function shared(){

      return $this->hasMany('App\LibraryDirectoryContentUser', 'library_directory_content_id');
    }
}
