<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseCohort extends Model
{
    protected $table = 'course_cohort';

    public function cohort () {
        return $this->hasMany('App\Cohort', 'id', 'cohort_id')->with('cohort_user');
    }
}
