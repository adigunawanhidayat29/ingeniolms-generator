<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteUserPage extends Model
{
    //
    protected $table = "site_users_pages";
}
