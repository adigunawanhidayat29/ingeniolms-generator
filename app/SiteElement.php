<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteElement extends Model
{
    protected $table = 'site_elements';
}
