<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryQuizQuestion extends Model
{
    protected $table = 'library_quiz_questions';

    public function quiz_question_answers()
    {
        return $this->hasMany('App\LibraryQuizQuestionAnswer', 'quiz_question_id');
    }
}
