<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
  protected $table = 'assignments';

  public function section(){

    return $this->belongsTo('App\Section', 'id_section');
  }

  public function setting(){

    return $this->hasOne('App\ActivityCompletion', 'assigment_id');
  }

  public function activity_order(){

    return $this->hasOne('App\ActivityOrder', 'assignment_id');
  }

  public function restrict(){

    return $this->hasMany('App\RestrictAccess', 'assignment_from_id');
  }

  public function progress_users(){
    return $this->hasMany('App\Progress', 'assignment_id')->with('user');
  }
}
