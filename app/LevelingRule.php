<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelingRule extends Model
{
    protected $table = 'leveling_rules';
}
