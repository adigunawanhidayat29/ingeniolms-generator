<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CohortUser extends Model
{
    protected $table= 'cohort_user';
}
