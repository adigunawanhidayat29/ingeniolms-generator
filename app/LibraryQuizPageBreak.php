<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryQuizPageBreak extends Model
{
    public function quiz_questions(){
      
      return $this->hasMany('App\LibraryQuizQuestion', 'quiz_page_break_id', 'id')->with('quiz_question_answers')->orderBy('sequence', 'asc');
    }
}
