<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStudentGroup extends Model
{
    public function course_student_group_user() {
        return $this->hasMany('App\CourseStudentGroupUser')->with('user');
    }
}
