<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentVideoUserQuizAnswer extends Model
{
  public $table = 'content_video_quiz_answers';
}
