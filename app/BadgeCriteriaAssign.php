<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadgeCriteriaAssign extends Model
{
    protected $table = 'badges_criteria_assign';
}
