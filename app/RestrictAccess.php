<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestrictAccess extends Model
{
    //

    public function self(){

      return $this->belongsTo('App\RestrictAccess', 'id');
    }

    public function to_content(){

      return $this->belongsTo('App\Content', 'content_to_id');
    }

    public function progress_content(){

      return $this->hasOne('App\Progress', 'content_id', 'content_to_id');
    }

    public function to_quizz(){

      return $this->belongsTo('App\Quiz', 'quizz_to_id');
    }

    public function progress_quizz(){

      return $this->hasOne('App\Progress', 'quiz_id', 'quizz_to_id');
    }

    public function to_assignment(){

      return $this->belongsTo('App\Assignment', 'assignment_to_id');
    }

    public function progress_assignment(){

      return $this->hasOne('App\Progress', 'assignment_id', 'assignment_to_id');
    }

    public function to(){
      $data = $this->self()->first();

      if($data->content_to_id){
        return $this->to_content();
      }elseif ($data->quizz_to_id) {
        return $this->to_quizz();
      }else {
        return $this->to_assignment();
      }
    }

    public function progress(){
      $data = $this->self()->first();

      if($data->content_to_id){
        return $this->progress_content();
      }elseif ($data->quizz_to_id) {
        return $this->progress_quizz();
      }else {
        return $this->progress_assignment();
      }
    }

}
