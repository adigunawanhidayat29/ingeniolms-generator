<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionAnswers extends Model
{
    //

    public function user(){

      return $this->belongsTo('App\User', 'user_id');
    }

    public function reply_1(){

      return $this->hasMany('App\DiscussionAnswers', 'level_1');
    }
}
