<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ActivityCompletion extends Model
{

  protected $table = 'activity_completion';

}
