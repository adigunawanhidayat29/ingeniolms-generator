<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupUpdate extends Model
{
    //

    public function user(){

      return $this->belongsTo('App\User');
    }

    public function reply_1(){

      return $this->hasMany('App\GroupUpdate', 'level_1');
    }
}
