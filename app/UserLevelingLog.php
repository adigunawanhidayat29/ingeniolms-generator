<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLevelingLog extends Model
{
    protected $table = 'users_leveling_logs';
}
