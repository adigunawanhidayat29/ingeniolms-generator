<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryDirectoryGroup extends Model
{
    public function contents(){

      return $this->hasMany('App\LibraryDirectoryGroupContent', 'group_id', 'id');
    }

    public function shared(){

      return $this->hasMany('App\LibraryDirectoryGroupUser', 'group_id');
    }

    public function question(){

      return $this->hasMany('App\QuestionBank', 'group_id');
    }
}
