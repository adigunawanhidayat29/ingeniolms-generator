<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAttendance extends Model
{
    protected $table = 'course_attendances';

    public function course_attendance_user () {
        return $this->hasMany('App\CourseAttendanceUser', 'course_attendances_id', 'id');
    }

    public function course_attendance_user_hasone () {
        return $this->hasOne('App\CourseAttendanceUser', 'course_attendances_id', 'id')->where('user_id', \Auth::user()->id);
    }
}
