<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizParticipantAnswer extends Model
{
  protected $table = 'quiz_participant_answers';

  public function type(){

    return $this->belongsTo('App\QuizType', 'quiz_type_id');
  }
}
