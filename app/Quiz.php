<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    //
    public function quiz_questions(){
      return $this->hasMany('App\QuizQuestion', 'quiz_id', 'id')->orderBy('sequence', 'asc');
    }

    public function patricipant(){
      return $this->hasMany('App\QuizParticipant', 'quiz_id', 'id');
    }

    public function pagebreak(){
      return $this->hasMany('App\QuizPageBreak', 'quiz_id', 'id')->orderBy('sequence', 'asc');
    }

    public function setting(){

      return $this->hasOne('App\ActivityCompletion', 'quizz_id');
    }

    public function activity_order(){

      return $this->hasOne('App\ActivityOrder', 'quizz_id');
    }

    public function restrict(){

      return $this->hasMany('App\RestrictAccess', 'quizz_from_id');
    }

    public function progress_users(){
      return $this->hasMany('App\Progress', 'quiz_id')->with('user');
    }
}
