var gulp = require("gulp");
var minifycss = require('gulp-clean-css');
var rename = require("gulp-rename");

gulp.task('minify-css', function(){
  return gulp.src(['./public/css/dq-style-2.css', './public/css/dq-style.css', './public/css/style.dq-light-blue.min.css'])
    .pipe(rename('app.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest('./public/css/'))
});
