<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandbox6e0b615be0ea445ca2a543d261ec74a1.mailgun.org',
        'secret' => 'key-b750f42e7c99245c54ccf0614cf9f0c5',
    ],

    'elastic_email' => [
    	'key' => env('ELASTIC_KEY'),
    	'account' => env('ELASTIC_ACCOUNT')
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


   /* 'facebook' => [
        'client_id' => '1753013895000791',
        'client_secret' => 'b5b98adfcd383ddb8326ea8d2f76b567',
        'redirect' => 'http://localhost:8000/auth/facebook/redirect',
    ],

    'google' => [
        'client_id' => '87331368189-t26t1f8a7f1c8bubmjvt5302nsitksma.apps.googleusercontent.com',
        'client_secret' => 'DqdcTVZ28NnF0ZG6JEELLG0_',
        'redirect' => 'http://localhost:8000/auth/google/redirect',
    ],*/


    'facebook' => [
        'client_id' => env('FACEBOOK_ID'),
        'client_secret' => env('FACEBOOK_SECRET'),
        'redirect' => env('APP_URL') . '/auth/facebook/redirect',
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('APP_URL') . '/auth/google/redirect',
    ],


];
