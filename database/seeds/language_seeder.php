<?php

use Illuminate\Database\Seeder;

class language_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            [
                'title' => 'Indonesia',
                'alias' => 'id',
                'status' => '1',
                'icon' => '',
            ],
            [
                'title' => 'English',
                'alias' => 'en',
                'status' => '0',
                'icon' => '',
            ],
        ]);
    }
}
