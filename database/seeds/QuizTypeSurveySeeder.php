<?php

use Illuminate\Database\Seeder;

class QuizTypeSurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('quiz_types')->insert([
        [
          'type' => 'Checkboxes'
        ],
        [
          'type' => 'Multiple Choice'
        ],
        [
          'type' => 'Date'
        ],
        [
          'type' => 'Time'
        ],
        [
          'type' => 'Dropdown'
        ],
        [
          'type' => 'File Upload'
        ],
        [
          'type' => 'Signature'
        ]
      ]);
    }
}
