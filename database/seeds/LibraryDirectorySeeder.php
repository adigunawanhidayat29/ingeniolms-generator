<?php

use Illuminate\Database\Seeder;

class LibraryDirectorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('library_directories')->insert([
        [
          'directory_name' => 'My Library',
        ],
        [
          'directory_name' => 'Public Library',
        ],
        [
          'directory_name' => 'Shared Library',
        ],
      ]);
    }
}
