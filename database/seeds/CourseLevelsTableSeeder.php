<?php

use Illuminate\Database\Seeder;

class CourseLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('course_levels')->insert([
        [
          'title' => 'Beginer',
        ],
        [
          'title' => 'Intermediate',
        ],
        [
          'title' => 'Expert',
        ],
        [
          'title' => 'All Levels',          
        ],
      ]);
    }
}
