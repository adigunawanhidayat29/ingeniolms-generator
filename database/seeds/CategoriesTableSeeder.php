<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
        [
          'title' => 'Business',
          'slug' => 'business',
          'description' => 'Business',
          'parent_category' => '0',
          'icon' => '',
        ],
        [
          'title' => 'Personal Development',
          'slug' => 'personal-development',
          'description' => 'Personal Development',
          'parent_category' => '0',
          'icon' => '',
        ],
        [
          'title' => 'Design',
          'slug' => 'design',
          'description' => 'Design',
          'parent_category' => '0',
          'icon' => '',
        ],
        [
          'title' => 'Lifestyle',
          'slug' => 'lifestyle',
          'description' => 'Lifestyle',
          'parent_category' => '0',
          'icon' => '',
        ],
      ]);
    }
}
