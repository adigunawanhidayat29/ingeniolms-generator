<?php

use Illuminate\Database\Seeder;

use App\Course;
use App\CourseSetting;
use App\DefaultActivityCompletion;

class CourseSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $course = Course::get();
        $content_type = ['assigment', 'quizz', 'text', 'file', 'video', 'discussion'];

        foreach ($course as $key => $value) {
          $course_setting = new CourseSetting;
          $course_setting->course_id = $value->id;
          $course_setting->save();

          foreach ($content_type as $type) {
            $default_activity_completion = new DefaultActivityCompletion;
            $default_activity_completion->course_id = $value->id;
            $default_activity_completion->type_content = $type;
            $default_activity_completion->required_view = 1;
            if($type == 'assigment'){
              $default_activity_completion->required_submit = 1;
            }

            $default_activity_completion->save();
          }
        }
    }
}
