<?php

use Illuminate\Database\Seeder;

class features_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert([
            [
                'title' => 'Anytime',
                'description' => 'Can be done at any time, 24 hours a day, 7 days a week',
                'status' => '1',
                'icon' => 'fa fa-clock-o',
            ],
            [
                'title' => 'Anywhere',
                'description' => 'Can be accessed from any location and is global. SMILE removes time and place restrictions with traditional class characteristics by using online communication modes such as email, online discussions',
                'status' => '1',
                'icon' => 'fa fa-globe',
            ],
            [
                'title' => 'Any Devices',
                'description' => 'Can be accessed through various devices, such as mobile phones, laptop, PC or tabs and provides interaction features such as live chat, discussion forums and others.',
                'status' => '1',
                'icon' => 'fa fa-desktop',
            ],
        ]);
    }
}
