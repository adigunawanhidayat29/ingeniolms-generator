<?php

use Illuminate\Database\Seeder;

class QuizQuestionAnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('quiz_question_answers')->insert([
        [
          'quiz_question_id' => '1',
          'answer' => 'Rasmus Lerdorf',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '1',
          'answer' => 'Taylor Otwell',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '1',
          'answer' => 'Mark Zuckerberg',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '1',
          'answer' => 'Larry Page',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '2',
          'answer' => '5.2',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '2',
          'answer' => '5.3',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '2',
          'answer' => '5.4',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '2',
          'answer' => '5.5',
          'answer_correct' => '1',
        ],

        [
          'quiz_question_id' => '3',
          'answer' => 'PHP',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '3',
          'answer' => 'HTML',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '3',
          'answer' => 'Python',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '3',
          'answer' => 'Ruby on Rails',
          'answer_correct' => '1',
        ],

        [
          'quiz_question_id' => '4',
          'answer' => 'Rasmus Lerdorf',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '4',
          'answer' => 'Taylor Otwell',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '4',
          'answer' => 'Mark Zuckerberg',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '4',
          'answer' => 'Larry Page',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '5',
          'answer' => 'Laracast',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '5',
          'answer' => 'Stack Overflow',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '5',
          'answer' => 'Medium',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '5',
          'answer' => 'Kaskus',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '6',
          'answer' => 'artisan make:model',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '6',
          'answer' => 'artisan make:controller',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '6',
          'answer' => 'artisan make:migration',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '6',
          'answer' => 'artisan make:seeder',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '7',
          'answer' => 'artisan make:model',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '7',
          'answer' => 'artisan make:controller',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '7',
          'answer' => 'artisan make:migration',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '7',
          'answer' => 'artisan make:seeder',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '8',
          'answer' => 'artisan make:model',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '8',
          'answer' => 'artisan make:controller',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '8',
          'answer' => 'artisan make:migration',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '8',
          'answer' => 'artisan make:seeder',
          'answer_correct' => '0',
        ],

        [
          'quiz_question_id' => '9',
          'answer' => 'artisan make:model',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '9',
          'answer' => 'artisan make:controller',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '9',
          'answer' => 'artisan make:migration',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '9',
          'answer' => 'artisan make:seeder',
          'answer_correct' => '1',
        ],

        [
          'quiz_question_id' => '10',
          'answer' => 'composer create-project --prefer-dist laravel/laravel blog',
          'answer_correct' => '1',
        ],
        [
          'quiz_question_id' => '10',
          'answer' => 'npm create-project --prefer-dist laravel/laravel blog',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '10',
          'answer' => 'bower create-project --prefer-dist laravel/laravel blog',
          'answer_correct' => '0',
        ],
        [
          'quiz_question_id' => '10',
          'answer' => 'composer create-project laravel/laravel blog',
          'answer_correct' => '0',
        ],
      ]);
    }
}
