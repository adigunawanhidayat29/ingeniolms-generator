<?php

use Illuminate\Database\Seeder;

class banners_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                'name' => '',
                'image' => '/img/iso-001.png',
                'status' => '1',
            ],
            [
                'name' => '',
                'image' => '/img/iso-002.png',
                'status' => '1',
            ],
            [
                'name' => '',
                'image' => '/img/iso-003.png',
                'status' => '1',
            ],
        ]);
    }
}
