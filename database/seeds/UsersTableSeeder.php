<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('users')->insert([
        [
          'name' => 'Administrator',
          'email' => 'admin@admin.com',
          'is_active' => '1',
          'slug' => 'administrator',
          'phone' => '08',
          'password' => bcrypt('12345678'),
        ],
      ]);
    }
}
