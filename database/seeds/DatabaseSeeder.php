<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(CourseLevelsTableSeeder::class);
        $this->call(UserLevelsTableSeeder::class);
        $this->call(QuizTypeSeeder::class);
        $this->call(UsersGroupsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(QuizzesTableSeeder::class);
        $this->call(QuizQuestionsTableSeeder::class);
        $this->call(QuizQuestionAnswersTableSeeder::class);
        $this->call(MeetingsTableSeeder::class);
        $this->call(ContentTableSeeder::class);
        $this->call(InstructorSeeder::class);
        $this->call(settings_seeder::class);
        $this->call(VconsTableSeeder::class);
        $this->call(LibraryDirectorySeeder::class);
        $this->call(CourseSettingSeeder::class);
        $this->call(QuizTypeSurveySeeder::class);
        $this->call(features_seeder::class);
        $this->call(language_seeder::class);
        $this->call(mail_config_seeder::class);
        $this->call(setting_userVerify_seeder::class);
    }
}
