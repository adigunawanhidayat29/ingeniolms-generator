<?php

use Illuminate\Database\Seeder;

class VconsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vcons')->insert([
            [
                'title' => 'Demo',
                'password' => 'password',
                'password_moderator' => 'moderator',
                'status' => '1',            
            ],
        ]);
    }
}
