<?php

use Illuminate\Database\Seeder;

class InstructorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('instructors')->insert([
        [
          'user_id' => '1',
          'status' => '1',
        ],
      ]);
    }
}
