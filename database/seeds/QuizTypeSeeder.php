<?php

use Illuminate\Database\Seeder;

class QuizTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('quiz_types')->insert([
        [
          'type' => 'Pilihan Ganda'
        ],
        [
          'type' => 'Benar atau Salah'
        ],
        [
          'type' => 'Mengurutkan'
        ],
        [
          'type' => 'Teks/Label'
        ],
        [
          'type' => 'Essay'
        ],
        [
          'type' => 'Short Answer'
        ],
        [
          'type' => 'Skala Likert'
        ]
      ]);
    }
}
