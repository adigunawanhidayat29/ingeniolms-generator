<?php

use Illuminate\Database\Seeder;

class mail_config_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
              'name' => 'MAIL_DRIVER',
              'value' => 'smtp',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_HOST',
              'value' => 'smtp.googlemail.com',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_PORT',
              'value' => '465',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_USERNAME',
              'value' => 'halloingenio@gmail.com',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_PASSWORD',
              'value' => 'dataquestsukses!321',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_ENCRYPTION',
              'value' => 'ssl',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_SENDER',
              'value' => 'no-reply',
              'type' => 'text',
            ],
            [
              'name' => 'MAIL_FROM',
              'value' => 'no-reply@ingeniolms.com',
              'type' => 'text',
            ],
        ]);
    }
}
