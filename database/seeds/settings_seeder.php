<?php

use Illuminate\Database\Seeder;

class settings_seeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('settings')->insert([
      [
        'name' => 'title',
        'value' => 'Your Application',
        'type' => 'text',
      ],
      [
        'name' => 'icon',
        'value' => '',
        'type' => 'file',
      ],
      [
        'name' => 'favicon',
        'value' => '',
        'type' => 'file',
      ],
      [
        'name' => 'footer',
        'value' => 'Ingeniolms 2019',
        'type' => 'text',
      ],
      [
        'name' => 'menu_category',
        'value' => 'Kategori',
        'type' => 'text',
      ],
      [
        'name' => 'menu_mycourse',
        'value' => 'Kelas Saya',
        'type' => 'text',
      ],
      [
        'name' => 'color_primary',
        'value' => '#0984e3',
        'type' => 'text',
      ],
      [
        'name' => 'color_secondary',
        'value' => '#74b9ff',
        'type' => 'text',
      ],
      [
        'name' => 'courses',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'video',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'degree',
        'value' => 'false',
        'type' => 'text',
      ],
      [
        'name' => 'program',
        'value' => 'false',
        'type' => 'text',
      ],
      [
        'name' => 'vcon',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'categories',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'menu_categories',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'menu_teacher',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'navbar_background_color',
        'value' => '#0984e3',
        'type' => 'text',
      ],
      [
        'name' => 'navbar_font_color',
        'value' => 'white',
        'type' => 'text',
      ],
      [
        'name' => 'navbar_font_color_hover',
        'value' => 'white',
        'type' => 'text',
      ],
      [
        'name' => 'login_with_facebook',
        'value' => 'false',
        'type' => 'text',
      ],
      [
        'name' => 'login_with_google',
        'value' => 'false',
        'type' => 'text',
      ],
      [
        'name' => 'banner',
        'value' => 'false',
        'type' => 'text',
      ],
      [
        'name' => 'copyright',
        'value' => 'Ingeniolms 2020. Powered by Dataquest',
        'type' => 'text',
      ],
      [
        'name' => 'registration',
        'value' => 'manual,self',
        'type' => 'text',
      ],
      [
        'name' => 'font_color_primary',
        'value' => '#000000',
        'type' => 'text',
      ],
      [
        'name' => 'font_color_secondary',
        'value' => '#bbbbbb',
        'type' => 'text',
      ],
      [
        'name' => 'navbar_background_hover',
        'value' => '#ffffff',
        'type' => 'text',
      ],
      [
        'name' => 'tabs_font_color',
        'value' => '#ffffff',
        'type' => 'text',
      ],
      [
        'name' => 'header_gradient',
        'value' => 'custom',
        'type' => 'text',
      ],

      // PURATAMA ADDED
      [
        'name' => 'tagline',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'custom_gradient_primary',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'custom_gradient_secondary',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'custom_gradient_rotate',
        'value' => '0',
        'type' => 'text',
      ],
      [
        'name' => 'banner_style',
        'value' => 'default',
        'type' => 'text',
      ],
      [
        'name' => 'footer_address',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_email',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_phone',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_instagram',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_facebook',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_twitter',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_youtube',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_linkedin',
        'value' => '',
        'type' => 'text',
      ],
      [
        'name' => 'footer_app_link',
        'value' => 'true',
        'type' => 'text',
      ],
      [
        'name' => 'app_store_link',
        'value' => '#',
        'type' => 'text',
      ],
      [
        'name' => 'google_play_link',
        'value' => '#',
        'type' => 'text',
      ],
      [
        'name' => 'features_thumbnail',
        'value' => null,
        'type' => 'text',
      ],
      [
        'name' => 'format_menu',
        'value' => 'category',
        'type' => 'text',
      ],
      [
        'name' => 'tnc_title',
        'value' => 'Syarat Dan Ketentuan',
        'type' => 'text',
      ],
      [
        'name' => 'tnc_value',
        'value' => 'Isi Syarat Dan Ketentuan',
        'type' => 'text',
      ],
      [
        'name' => 'privacy_policy_title',
        'value' => 'Kebijakan Privasi',
        'type' => 'text',
      ],
      [
        'name' => 'privacy_policy_value',
        'value' => 'Isi Kebijakan Privasi',
        'type' => 'text',
      ],
      [
        'name' => 'contact_us_title',
        'value' => 'Hubungi Kami',
        'type' => 'text',
      ],
      [
        'name' => 'contact_us_value',
        'value' => 'Isi Hubungi Kami',
        'type' => 'text',
      ],
      [
        'name' => 'catalog_column',
        'value' => '4',
        'type' => 'text',
      ],
      [
        'name' => 'max_catalog_column',
        'value' => '4',
        'type' => 'text',
      ],
      [
        'name' => 'enrollment',
        'value' => 'manual,self',
        'type' => 'text',
      ],
      [
        'name' => 'login_with',
        'value' => 'email',
        'type' => 'text',
      ],
      [
        'name' => 'feature_background_color',
        'value' => '#0984e3',
        'type' => 'text',
      ],
      [
        'name' => 'feature_text_color',
        'value' => '#ffffff',
        'type' => 'text',
      ],
      [
        'name' => 'footer_background_color',
        'value' => '#3D4349',
        'type' => 'text',
      ],
      [
        'name' => 'footer_text_color',
        'value' => '#ffffff',
        'type' => 'text',
      ],
    ]);
  }
}
