<?php

use Illuminate\Database\Seeder;

class UserLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('user_levels')->insert([
        [
          'name' => 'administrator',
        ],
        [
          'name' => 'creator',
        ],
        [
          'name' => 'user',
        ],
        [
          'name' => 'affiliate',
        ],
        [
          'name' => 'teacher',
        ],
      ]);
    }
}
