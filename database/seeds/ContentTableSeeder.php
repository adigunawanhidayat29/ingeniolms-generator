<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('contents')->insert([
      [
        'id_section' => '1',
        'title' => 'Mengapa E-Learning Gagal? | Ingenio Belajar Online',
        'description' => 'kenapa bisa ya?
        e-Learning kan teknologi tercanggih untuk sistem belajar masa kini.
        hal apa yang terjadi?
        Nonton sampai akhir yaa
        Ingenio 
        -Belajar Online-
        #inspirasisekitarkita​
        instagram : @ingenio.belajaronline
        www.ingenio.co.id',
        'type_content' => 'url-video',
        'path_file' => 'https://www.youtube.com/watch?v=4V6xLrZWSP0&t=212s',
        'full_path_file' => 'https://www.youtube.com/watch?v=4V6xLrZWSP0&t=212s',
      ],
    ]);
  }
}
