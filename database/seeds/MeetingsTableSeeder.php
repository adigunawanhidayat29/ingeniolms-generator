<?php

use Illuminate\Database\Seeder;

class MeetingsTableSeeder extends Seeder
{
    public function run()
    {
      DB::table('meetings')->insert([
        [
          'course_id' => '1',
          'name' => 'Learn Laravel',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '2',
          'name' => 'Learn Bootstrap',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '3',
          'name' => 'Learn Codeigniter',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '4',
          'name' => '	Learn Corel Draw',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '5',
          'name' => 'Learn Git',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '6',
          'name' => 'Learn Marketing',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '7',
          'name' => 'Learn Photoshop',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '8',
          'name' => 'Learn Vue JS',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '9',
          'name' => 'Learn Yii',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
        [
          'course_id' => '10',
          'name' => 'Learn React Native',
          'password' => rand(1000,9999),
          'password_instructor' => "instructor".rand(1000,9999),
        ],
      ]);
    }
}
