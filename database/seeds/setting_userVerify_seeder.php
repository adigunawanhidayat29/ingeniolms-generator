<?php

use Illuminate\Database\Seeder;

class setting_userVerify_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
              'name' => 'user_verify',
              'value' => '1',
              'type' => 'text',
            ],
        ]);
    }
}
