<?php

use Illuminate\Database\Seeder;

class QuizQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('quiz_questions')->insert([
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Siapakah penemu framework laravel?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Sudah versi berapakah laravel saat ini?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Berawal darimana framework Laravel ini?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Siapkah penemu bahasa pemograman PHP?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Forum official laravel apa namanya?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Perintah untuk membuat model?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Perintah untuk membuat controller?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Perintah untuk membuat migration?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Perintah untuk membuat seeder?',
          'weight' => '10',
        ],
        [
          'quiz_id' => '1',
          'quiz_type_id' => '1',
          'question' => 'Gimana cara install laravel?',
          'weight' => '10',
        ],
      ]);
    }
}
