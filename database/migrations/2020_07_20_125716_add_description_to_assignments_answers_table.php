<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToAssignmentsAnswersTable extends Migration
{
    public function up()
    {
        Schema::table('assignments_answers', function($table) {
            $table->longText('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignments_answers', function($table) {
            $table->dropColumn('description');
        });
    }
}
