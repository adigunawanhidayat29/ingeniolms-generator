<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContentVideoQuizAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('content_video_quiz_answers', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('content_video_quiz_id');
           $table->text('answer');
           $table->text('answer_description')->nullable();
           $table->enum('is_correct', ['0','1'])->default('0');
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('content_video_quiz_answers');
     }
}
