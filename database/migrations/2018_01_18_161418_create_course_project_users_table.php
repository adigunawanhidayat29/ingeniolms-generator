<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseProjectUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_project_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_project_id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('slug');
            $table->string('image');
            $table->longText('description');
            $table->integer('read')->default(0);
            $table->enum('status', ['0','1'])->default('1')->comment('0=nonactive, 1=active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_project_users');
    }
}
