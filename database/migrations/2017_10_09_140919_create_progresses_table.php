<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressesTable extends Migration
{
  public function up()
  {
    Schema::create('progresses', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('content_id');
      $table->integer('course_id');
      $table->integer('user_id');
      $table->enum('status', ['0', '1'])->comment('0 false, 1 true')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('progresses');
  }
}
