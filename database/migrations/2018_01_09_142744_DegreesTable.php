<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('degrees', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('degree_level_id');
         $table->string('password')->nullable();
         $table->string('title');
         $table->string('slug');
         $table->longText('description')->nullable();
         $table->string('image')->nullable();
         $table->string('introduction')->nullable();
         $table->longText('introduction_description')->nullable();
         $table->longText('benefits')->nullable();
         $table->longText('developer_team')->nullable();
         $table->longText('precondition')->nullable();
         $table->enum('status', ['0', '1'])->comment('0 nonactive, 1 active')->default('1');
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('degrees');
     }
}
