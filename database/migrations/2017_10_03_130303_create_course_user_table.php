<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseUserTable extends Migration
{
  public function up()
  {
    Schema::create('courses_users', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('course_id');
      $table->integer('user_id');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('courses_users');
  }
}
