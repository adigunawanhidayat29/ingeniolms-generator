<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('applications', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('image')->nullable();
        $table->string('hostname');
        $table->string('redirect_url')->nullable();
        $table->string('api_key');
        $table->string('email')->nullable();
        $table->integer('created_by');
        $table->datetime('registered_at');
        $table->datetime('expired_at');
        $table->integer('blocked');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('applications');
    }
}
