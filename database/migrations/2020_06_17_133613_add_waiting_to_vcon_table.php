<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaitingToVconTable extends Migration
{
    
    public function up()
    {
        Schema::table('vcons', function($table) {
            $table->enum('waiting', [0, 1])->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vcons', function($table) {
            $table->dropColumn('waiting');
        });
    }
}
