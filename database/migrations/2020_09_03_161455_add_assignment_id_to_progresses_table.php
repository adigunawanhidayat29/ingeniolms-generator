<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssignmentIdToProgressesTable extends Migration
{
    public function __construct()
    {
         DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('progresses', function (Blueprint $table) {
             $table->integer('content_id')->nullable()->change();
             $table->integer('assignment_id')->nullable();
             $table->integer('quiz_id')->nullable();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('contents', function (Blueprint $table) {
             $table->dropColumn('is_description_show');
         });
     }
}
