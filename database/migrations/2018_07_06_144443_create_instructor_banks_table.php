<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorBanksTable extends Migration
{
  public function up()
  {
      Schema::create('instructor_banks', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('instructor_id');
        $table->string('bank_name');
        $table->string('account_name')->nullable();
        $table->string('account_number')->nullable();
        $table->string('bank_branch')->nullable();
        $table->enum('status', ['0','1'])->default('1');
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('instructor_banks');
  }
}
