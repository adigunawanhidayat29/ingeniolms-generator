<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnswerOrderingGradeToQuizParticipantAnswersTable extends Migration
{
    
    public function up()
    {
        Schema::table('quiz_participant_answers', function($table) {
            $table->integer('answer_ordering_grade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_participant_answers', function($table) {
            $table->dropColumn('answer_ordering_grade');
        });
    }
}
