<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToCourseAttendancesTable extends Migration
{
    public function up()
    {
        Schema::table('course_attendances', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_attendances', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }
}
