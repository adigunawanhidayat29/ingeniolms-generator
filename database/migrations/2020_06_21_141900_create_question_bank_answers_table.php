<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionBankAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_bank_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_bank_id');
            $table->longText('answer')->nullable();
            $table->enum('answer_correct', ['0', '1'])->comment('0 false, 1 true');
            $table->integer('answer_ordering');
            $table->integer('sequence')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_bank_answers');
    }
}
