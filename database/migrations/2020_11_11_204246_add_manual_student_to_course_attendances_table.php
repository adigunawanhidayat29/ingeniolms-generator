<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManualStudentToCourseAttendancesTable extends Migration
{
    public function up()
    {
        Schema::table('course_attendances', function (Blueprint $table) {
            $table->string('manual_student')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_attendances', function (Blueprint $table) {
            $table->dropColumn('manual_student');
        });
    }
}
