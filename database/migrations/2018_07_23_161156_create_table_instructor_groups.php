<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstructorGroups extends Migration
{
  public function up()
  {
      Schema::create('instructor_groups', function (Blueprint $table) {
        $table->increments('id');
        $table->string('user_id');
        $table->string('title');
        $table->string('image', 500)->nullable();
        $table->string('slug');
        $table->longText('description')->nullable();
        $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('1');
        $table->integer('created_by');
        $table->timestamps();
      });
  }


  public function down()
  {
    Schema::dropIfExists('instructor_groups');
  }
}
