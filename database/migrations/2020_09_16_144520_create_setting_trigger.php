<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::unprepared('
        CREATE TRIGGER course_setting_trigger AFTER INSERT ON `courses` FOR EACH ROW
            BEGIN
                INSERT INTO course_settings (`course_id`)
                VALUES (NEW.id);
            END
        ');

        DB::unprepared('
        CREATE TRIGGER default_activity_completion_trigger AFTER INSERT ON `courses` FOR EACH ROW
            BEGIN
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`, `required_submit`) VALUES (NEW.id, "assigment", 1, 1);
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`) VALUES (NEW.id, "quizz", 1);
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`) VALUES (NEW.id, "text", 1);
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`) VALUES (NEW.id, "file", 1);
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`) VALUES (NEW.id, "video", 1);
                INSERT INTO default_activity_completion (`course_id`, `type_content`, `required_view`) VALUES (NEW.id, "discussion", 1);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared('DROP TRIGGER `course_setting_trigger`');
        DB::unprepared('DROP TRIGGER `default_activity_completion_trigger`');
    }
}
