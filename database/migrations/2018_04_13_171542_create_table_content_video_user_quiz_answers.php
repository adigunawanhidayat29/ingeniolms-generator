<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContentVideoUserQuizAnswers extends Migration
{
  public function up()
  {
    Schema::create('content_video_user_quiz_answers', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('content_video_quiz_id');
        $table->integer('content_video_quiz_answer_id');
        $table->integer('user_id');
        $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('content_video_user_quiz_answers');
  }
}
