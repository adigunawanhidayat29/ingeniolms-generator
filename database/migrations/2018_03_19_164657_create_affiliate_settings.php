<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateSettings extends Migration
{
  public function up()
  {
      Schema::create('affiliate_setting', function (Blueprint $table) {
        $table->increments('id');
        $table->string('commission')->default(0);
        $table->integer('discount')->default(0);
        $table->datetime('start_date')->nullable();
        $table->datetime('end_date')->nullable();
        $table->string('banner', 500)->nullable();
        $table->enum('status', ['0', '1'])->comment('0 nonactive, 1 active')->default('0');
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('affiliate_setting');
  }
}
