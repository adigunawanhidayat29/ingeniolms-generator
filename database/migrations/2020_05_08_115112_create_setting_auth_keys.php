<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingAuthKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_auth_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('google_client_id');
            $table->longText('google_client_secret');
            $table->longText('facebook_client_id');
            $table->longText('facebook_client_secret');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_auth_keys');
    }
}
