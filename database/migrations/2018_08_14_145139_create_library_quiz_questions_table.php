<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryQuizQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_quiz_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quiz_library_id');
            $table->integer('quiz_library_type_id');
            $table->longText('question')->nullable();
            $table->integer('weight')->default(0);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_quiz_questions');
    }
}
