<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizParticipantAnswersTable extends Migration
{
  public function up()
  {
    Schema::create('quiz_participant_answers', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('quiz_id');
        $table->integer('quiz_participant_id');
        $table->integer('quiz_question_id');
        $table->integer('quiz_question_answer_id');
        $table->datetime('time_start')->nullable();
        $table->datetime('time_end')->nullable();
        $table->integer('answer_ordering');
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quiz_participant_answers');
  }
}
