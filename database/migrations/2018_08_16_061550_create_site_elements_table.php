<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id');
            $table->string('site_logo')->default('site_assets/logo/default.png');
            $table->string('site_favicon')->default('site_assets/favicon/default.png');
            $table->string('site_bg')->default('site_assets/background/default.jpg');
            $table->string('site_bg_darkness')->default('0.1');
            $table->string('site_font')->default('Arial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_elements');
    }
}
