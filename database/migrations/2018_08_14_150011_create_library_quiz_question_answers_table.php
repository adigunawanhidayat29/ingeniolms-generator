<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryQuizQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_quiz_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quiz_question_id');
            $table->longText('answer')->nullable();
            $table->enum('answer_correct', ['0', '1'])->comment('0 false, 1 true');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_quiz_question_answers');
    }
}
