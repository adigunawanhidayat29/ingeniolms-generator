<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_broadcasts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('youtube_id');
            $table->string('stream_url');
            $table->string('stream_name');
            $table->longText('embed_html')->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('tags')->nullable();
            $table->enum('category', ['public','unlisted','private'])->default('unlisted');
            $table->string('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_broadcasts');
    }
}
