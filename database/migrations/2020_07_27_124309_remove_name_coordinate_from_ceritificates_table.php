<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNameCoordinateFromCeritificatesTable extends Migration
{
    public function up()
      {
          Schema::table('certificates', function($table) {
             $table->dropColumn('position');
             $table->dropColumn('x_coordinate');
             $table->dropColumn('y_coordinate');
          });
      }

    public function down()
    {
        Schema::table('certificates', function($table) {
            $table->integer('position');
            $table->integer('x_coordinate');
            $table->integer('y_coordinate');
        });
    }
}