<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToSectionsTable extends Migration
{
    public function up()
    {
        Schema::table('sections', function($table) {
            $table->boolean('status')->default(1);
        });
    }

    public function down()
    {
        Schema::table('sections', function($table) {
            $table->dropColumn('status');
        });
    }
}
