<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToSlidersTable extends Migration
{
    public function up()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->text('header')->nullable();
            $table->text('header_font_size')->nullable();
            $table->text('header_font_weight')->nullable();
            $table->text('header_font_color')->nullable();
            $table->text('subheader')->nullable();
            $table->text('subheader_font_size')->nullable();
            $table->text('subheader_font_weight')->nullable();
            $table->text('subheader_font_color')->nullable();
            $table->text('text_alignment')->nullable();
            $table->text('text_position')->nullable();
            $table->text('text_button')->nullable();
            $table->text('link_button')->nullable();
            $table->text('overlay_color')->nullable();
            $table->text('overlay_transparention')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->dropColumn('header');
            $table->dropColumn('header_font_size');
            $table->dropColumn('header_font_weight');
            $table->dropColumn('header_font_color');
            $table->dropColumn('subheader');
            $table->dropColumn('subheader_font_size');
            $table->dropColumn('subheader_font_weight');
            $table->dropColumn('subheader_font_color');
            $table->dropColumn('text_alignment');
            $table->dropColumn('text_position');
            $table->dropColumn('text_button');
            $table->dropColumn('link_button');
            $table->dropColumn('overlay_color');
            $table->dropColumn('overlay_transparention');
        });
    }
}
