<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssignmentAnswersTable extends Migration
{
  public function up()
  {
    Schema::create('assignments_answers', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('assignment_id');
        $table->integer('user_id');
        $table->longText('answer');
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('assignments_answers');
  }
}
