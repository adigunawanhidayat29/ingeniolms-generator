<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCourseLive extends Migration
{
  public function up()
  {
      Schema::create('course_lives', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('course_id');
        $table->integer('meeting_total')->default(1);
        $table->date('start_date')->nullable();
        $table->date('end_date')->nullable();
        $table->integer('participant_total')->nullable();        
        $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('1');
        $table->timestamps();
      });
  }


  public function down()
  {
    Schema::dropIfExists('course_lives');
  }
}
