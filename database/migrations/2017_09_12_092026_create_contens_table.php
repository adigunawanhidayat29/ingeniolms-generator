<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		    Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_section');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('type_content')->nullable();
            $table->string('name_file')->nullable();
            $table->string('path_file')->nullable();
            $table->string('full_path_file')->nullable();
            $table->string('full_path_file_resize')->nullable();
            $table->string('full_path_file_resize_720')->nullable();
            $table->string('full_path_original')->nullable();
            $table->string('full_path_original_resize')->nullable();
            $table->string('full_path_original_resize_720')->nullable();
            $table->string('file_status')->default(1);
            $table->integer('sequence')->default(0);
            $table->integer('file_size')->default(0);
            $table->string('video_duration')->default(0)->nullable();
            $table->enum('status', ['0','1'])->default(0)->comment('0=draft, 1=publish');
            $table->enum('preview', ['0','1'])->default(0)->comment('0=non preview, 1=preview');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		    Schema::dropIfExists('contents');
    }
}
