<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DegreeUsers extends Migration
{
  public function up()
  {
    Schema::create('degree_users', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('degree_id');
      $table->integer('user_id');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('degree_users');
  }
}
