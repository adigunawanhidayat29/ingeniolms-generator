<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssignmentsGrades extends Migration
{
  public function up()
  {
    Schema::create('assignments_grades', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('assignment_id');
      $table->integer('assignment_answer_id');
      $table->integer('grade');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('assignments_grades');
  }
}
