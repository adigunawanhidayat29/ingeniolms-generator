<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestrictAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restrict_accesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_from_id')->nullable();
            $table->integer('quizz_from_id')->nullable();
            $table->integer('assignment_from_id')->nullable();
            $table->integer('content_to_id')->nullable();
            $table->integer('quizz_to_id')->nullable();
            $table->integer('assignment_to_id')->nullable();
            $table->integer('grade')->nullable();
            $table->integer('required_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restrict_accesses');
    }
}
