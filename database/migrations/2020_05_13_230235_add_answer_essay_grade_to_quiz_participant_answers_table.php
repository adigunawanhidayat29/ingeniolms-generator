<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnswerEssayGradeToQuizParticipantAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('quiz_participant_answers', function($table) {
        $table->integer('answer_essay_grade')->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('quiz_participant_answers', function($table) {
        $table->dropColumn('answer_essay_grade');
    });
    }
}
