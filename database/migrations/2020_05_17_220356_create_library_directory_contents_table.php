<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryDirectoryContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_directory_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('directory_id');
            $table->integer('content_id')->nullable();
            $table->integer('quiz_id')->nullable();
            $table->integer('assignment_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->string('shared_code')->nullable();
            $table->enum('status', ['0','1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_directory_shareds');
    }
}
