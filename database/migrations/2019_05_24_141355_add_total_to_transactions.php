<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalToTransactions extends Migration
{
    public function up()
    {
        Schema::table('transactions', function($table) {
            $table->integer('total');
            $table->longText('method_details');
        });
    }

    public function down()
    {
        Schema::table('transactions', function($table) {
            $table->dropColumn('total');
            $table->dropColumn('method_details');
        });
    }
}
