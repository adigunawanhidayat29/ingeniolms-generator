<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultActivityCompletionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_activity_completion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('type_content')->nullable();
            $table->integer('completion_tracking')->default(0);
            $table->integer('required_view')->default(1);
            $table->float('required_grade')->default(0);
            $table->integer('required_submit')->default(0);
            $table->dateTime('expected_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_activity_completion');
    }
}
