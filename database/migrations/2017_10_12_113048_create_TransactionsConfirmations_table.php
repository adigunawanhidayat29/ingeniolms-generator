<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsConfirmationsTable extends Migration
{
  public function up()
  {
    Schema::create('transactions_confirmations', function (Blueprint $table) {
      $table->increments('id');
      $table->string('invoice', 50);
      $table->string('file');
      $table->string('account_name')->nullable();
      $table->string('account_number')->nullable();
      $table->string('bank_name')->nullable();
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('transactions_confirmations');
  }
}
