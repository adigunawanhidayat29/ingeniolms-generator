<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizParticipantsTable extends Migration
{
  public function up()
  {
    Schema::create('quiz_participants', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('quiz_id');
        $table->integer('user_id');
        $table->datetime('time_start')->nullable();
        $table->datetime('time_end')->nullable();
        $table->integer('grade')->default(0);
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quiz_participants');
  }
}
