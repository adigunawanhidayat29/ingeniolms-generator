<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPasswordModeratorToVconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vcons', function($table) {
            $table->longText('password_moderator');
        });
    }

    public function down()
    {
        Schema::table('vcons', function($table) {
            $table->dropColumn('password_moderator');
        });
    }
}
