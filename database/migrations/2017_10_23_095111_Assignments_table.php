<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssignmentsTable extends Migration
{
  public function up()
  {
    Schema::create('assignments', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_section');
        $table->string('title');
        $table->longText('description');
        $table->enum('type', ['0', '1'])->comment('0 text, 1 upload')->default('0');
        $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
        $table->integer('attempt')->default('1');
        $table->datetime('time_start')->nullable();
        $table->datetime('time_end')->nullable();
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('assignments');
  }
}
