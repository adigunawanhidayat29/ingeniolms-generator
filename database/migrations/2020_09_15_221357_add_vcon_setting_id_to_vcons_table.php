<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVconSettingIdToVconsTable extends Migration
{
    public function up()
    {
        Schema::table('vcons', function (Blueprint $table) {
            $table->string('vcon_setting_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vcons', function (Blueprint $table) {
            $table->dropColumn('vcon_setting_id');
        });
    }
}
