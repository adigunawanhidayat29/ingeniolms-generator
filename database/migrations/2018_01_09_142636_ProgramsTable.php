<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('programs', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->integer('price')->default(0);
        $table->string('slug');
        $table->longText('description')->nullable();
        $table->string('image')->nullable();
        $table->string('introduction')->nullable();
        $table->longText('introduction_description')->nullable();
        $table->longText('benefits')->nullable();
        $table->longText('developer_team')->nullable();
        $table->longText('precondition')->nullable();
        $table->string('password')->nullable();
        $table->enum('status', ['0', '1'])->comment('0 nonactive, 1 active')->default('1');
        $table->integer('author');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
