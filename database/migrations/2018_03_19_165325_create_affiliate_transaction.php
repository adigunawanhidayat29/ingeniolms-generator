<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTransaction extends Migration
{
  public function up()
  {
      Schema::create('affiliate_transactions', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->integer('affiliate_id');
        $table->string('invoice');
        $table->integer('commission')->default(0);
        $table->enum('status', ['0', '1'])->comment('0 nonactive, 1 active')->default('0');
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('affiliate_transactions');
  }
}
