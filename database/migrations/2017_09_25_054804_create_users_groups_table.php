<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersGroupsTable extends Migration
{

    public function up()
    {
      Schema::create('users_groups', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('level_id');
      });
    }

    public function down()
    {
      Schema::dropIfExists('users_groups');
    }
}
