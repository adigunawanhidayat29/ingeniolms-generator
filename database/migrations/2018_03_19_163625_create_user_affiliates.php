<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAffiliates extends Migration
{
  public function up()
  {
      Schema::create('affiliate_users', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('url')->nullable();
        $table->string('code')->nullable();
        $table->string('banner')->nullable();
        $table->string('bank')->nullable();
        $table->string('bank_account_number')->nullable();
        $table->string('bank_account_name')->nullable();
        $table->enum('status', ['0', '1'])->comment('0 nonactive, 1 active')->default('0');
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('affiliate_users');
  }
}
