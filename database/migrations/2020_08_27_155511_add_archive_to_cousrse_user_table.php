<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArchiveToCousrseUserTable extends Migration
{
    public function up()
    {
        Schema::table('courses_users', function (Blueprint $table) {
            $table->string('is_archive')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses_users', function (Blueprint $table) {
            $table->dropColumn('is_archive');
        });
    }
}
