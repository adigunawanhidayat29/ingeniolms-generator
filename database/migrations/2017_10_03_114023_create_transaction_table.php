<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
  public function up()
  {
    Schema::create('transactions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('invoice', 50);
      $table->integer('user_id');
      $table->string('method', 100);
      $table->integer('subtotal');
      $table->integer('unique_number');
      $table->enum('promotion', ['0','1'])->comment('0=false, 1=true')->default(0);
      $table->integer('promotion_id')->nullable();
      $table->string('discount_code')->nullable();
      $table->integer('affiliate_id')->nullable();
      $table->enum('status', ['0', '1'])->comment('0 pending, 1 success')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('transactions');
  }
}
