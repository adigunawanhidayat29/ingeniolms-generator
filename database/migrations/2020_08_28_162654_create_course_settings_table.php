<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('course_completion')->default(0);
            $table->string('activity_completion')->default('0,0,0');
            $table->integer('course_grade_enable')->default(0);
            $table->float('course_grade')->default(0);
            $table->integer('manual_self_completion')->default(0);
            $table->string('manual_other_completion')->default('0,0,0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_settings');
    }
}
