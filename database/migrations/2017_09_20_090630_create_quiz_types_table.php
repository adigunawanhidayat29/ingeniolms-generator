<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizTypesTable extends Migration
{
  public function up()
  {
    Schema::create('quiz_types', function (Blueprint $table) {
        $table->increments('id');
        $table->string('type', 50)->nullable();
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quiz_types');
  }
}
