<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
  public function up()
  {
    Schema::create('meetings', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('course_id');
      $table->string('name');
      $table->string('password');
      $table->string('password_instructor');
      $table->enum('publish', ['0', '1'])->comment('0 false, 1 true')->default('0');
      $table->longText('welcome_message')->nullable();
      $table->enum('waiting', ['0', '1'])->comment('0 false, 1 true')->default('0');
      $table->enum('recording', ['0', '1'])->comment('0 false, 1 true')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('meetings');
  }
}
