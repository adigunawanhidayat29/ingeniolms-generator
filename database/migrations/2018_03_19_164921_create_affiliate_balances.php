<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateBalances extends Migration
{
  public function up()
  {
      Schema::create('affiliate_balances', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('affiliate_id');
        $table->integer('credit')->default(0);
        $table->integer('debit')->default(0);
        $table->longText('information')->nullable();        
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('affiliate_balances');
  }
}
