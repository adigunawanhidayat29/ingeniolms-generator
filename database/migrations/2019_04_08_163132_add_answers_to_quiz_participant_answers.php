<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnswersToQuizParticipantAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_participant_answers', function($table) {
            $table->longText('answer_short_answer')->nullable();
            $table->longText('answer_essay')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_participant_answers', function($table) {
            $table->dropColumn('answer_short_answer');
            $table->dropColumn('answer_essay');
        });
    }
}
