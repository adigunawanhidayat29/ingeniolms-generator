<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ManagersTable extends Migration
{
  public function up()
  {
    Schema::create('managers', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');
      $table->enum('status', ['0', '1'])->comment('0 non active, 1 active')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('managers');
  }
}
