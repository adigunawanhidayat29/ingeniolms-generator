<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityCompletionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_completion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->nullable();
            $table->integer('assigment_id')->nullable();
            $table->integer('quizz_id')->nullable();
            $table->string('type_content')->nullable();
            $table->integer('completion_tracking')->default(0);
            $table->integer('required_view')->default(1);
            $table->integer('required_grade')->default(0);
            $table->integer('required_submit')->default(0);
            $table->dateTime('expected_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_completion');
    }
}
