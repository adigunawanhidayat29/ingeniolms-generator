<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeetingSchdule extends Migration
{
  public function up()
  {
    Schema::create('meeting_schedules', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('course_id');
      $table->string('name');
      $table->longText('description')->nullable();
      $table->date('date');
      $table->time('time')->nullable();
      $table->enum('recording', ['0', '1'])->comment('0 false, 1 true')->default('1');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('meeting_schedules');
  }
}
