<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommunityIdToInstructorsTable extends Migration
{
    public function up()
    {
        Schema::table('instructors', function($table) {
            $table->string('community_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('instructors', function($table) {
            $table->dropColumn('community_id');
        });
    }
}
