<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorsTable extends Migration
{
  public function up()
  {
    Schema::create('instructors', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id');
      $table->enum('status', ['0', '1'])->comment('0 non active, 1 active')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('instructors');
  }
}
