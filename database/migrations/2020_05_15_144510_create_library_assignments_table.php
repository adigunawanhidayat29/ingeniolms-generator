<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->enum('type', ['0', '1'])->comment('0 text, 1 upload')->default('0');
            $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
            $table->integer('attempt')->default('1');
            $table->datetime('time_start')->nullable();
            $table->datetime('time_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_assignments');
    }
}
