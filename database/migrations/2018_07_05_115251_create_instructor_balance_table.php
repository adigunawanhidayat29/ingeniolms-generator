<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorBalanceTable extends Migration
{
  public function up()
  {
      Schema::create('instructor_balances', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('instructor_id');
        $table->integer('credit');
        $table->integer('debit');
        $table->string('information', 500)->nullable();
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('instructor_balances');
  }
}
