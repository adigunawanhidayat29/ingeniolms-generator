<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProgramCoursesTable extends Migration
{
  public function up()
  {
    Schema::create('program_courses', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('program_id');
      $table->string('course_id');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('program_courses');
  }
}
