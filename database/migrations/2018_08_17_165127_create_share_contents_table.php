<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_lib_id');
            $table->integer('user_id');
            $table->integer('isWriteable')->default(0);
            $table->integer('isReadable')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_contents');
    }
}
