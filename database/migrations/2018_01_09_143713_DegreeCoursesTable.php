<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DegreeCoursesTable extends Migration
{
  public function up()
  {
    Schema::create('degree_courses', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('degree_id');
      $table->integer('course_id');
      $table->string('semester')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('degree_courses');
  }
}
