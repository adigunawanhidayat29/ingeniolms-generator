<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('promotions', function (Blueprint $table){
        $table->increments('id');
        $table->string('name')->nullable();
        $table->string('banner')->nullable();
        $table->longText('description')->nullable();
        $table->string('discount_code')->nullable();
        $table->integer('discount')->default(0);
        $table->datetime('start_date')->nullable();
        $table->datetime('end_date')->nullable();
        $table->enum('status', ['0', '1'])->comment('0 pending, 1 success')->default('0');
        $table->string('notes')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('promotions');
    }
}
