<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('goal')->nullable();
            $table->longText('description')->nullable();
            $table->string('slug', 255)->nullable();
            $table->string('folder', 255)->nullable();
            $table->string('folder_path', 255)->nullable();
            $table->string('image')->nullable();
            $table->integer('price')->default(0);
            $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
            $table->enum('archive', ['0', '1'])->comment('0 false, 1 true')->default('0');
            $table->enum('public', ['0', '1'])->comment('0 false, 1 true')->default('1');
            $table->enum('model', ['subscription','batch','live','private'])->default('subscription');
            $table->integer('id_category')->nullable();
            $table->integer('id_level_course')->nullable();
            $table->string('id_author')->nullable();
            $table->string('id_instructor_group')->nullable();
            $table->string('introduction')->nullable();
            $table->string('password')->nullable();
            $table->text('subtitle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('courses');
    }
}
