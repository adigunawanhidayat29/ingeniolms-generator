<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeritificateAttributes extends Migration
{
    public function up()
    {
        Schema::create('ceritificate_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ceritificate_id');
            $table->string('position')->nullable();
            $table->longText('x_coordinate')->nullable();
            $table->longText('y_coordinate')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ceritificate_attributes');
    }
}
