<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVconsJoinTables extends Migration
{
    public function up()
    {
        Schema::create('vcon_waiting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vcon_id');
            $table->string('name');
            $table->string('status')->default('0');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vcon_waiting');
    }
}
