<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizQuestionAnswersTable extends Migration
{
  public function up()
  {
    Schema::create('quiz_question_answers', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('quiz_question_id');
        $table->longText('answer')->nullable();
        $table->enum('answer_correct', ['0', '1'])->comment('0 false, 1 true');
        $table->integer('answer_ordering');
        $table->integer('sequence')->default(0);
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quiz_question_answers');
  }
}
