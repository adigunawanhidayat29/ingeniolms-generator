<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration
{

  public function up()
  {
    Schema::create('quizzes', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_section');
        $table->string('name', 255);
        $table->enum('shuffle', ['0', '1'])->comment('0 false, 1 true')->default('0');
        $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
        $table->longText('description')->nullable();
        $table->string('image', 255)->nullable()->default('quiz-default.png');
        $table->string('slug', 255)->nullable();
        $table->datetime('time_start')->nullable();
        $table->datetime('time_end')->nullable();
        $table->string('duration')->nullable()->default('60')->comment('default 60 minutes');
        $table->string('attempt')->default('-1')->nullable()->comment('-1 unlimited');
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quizzes');
  }
}
