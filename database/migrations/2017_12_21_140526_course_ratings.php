<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('course_ratings', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('course_id');
        $table->integer('user_id');
        $table->integer('rating')->default(0);
        $table->longText('comment');
        $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('1');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('course_ratings');
    }
}
