<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizPageBreakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('quiz_page_breaks', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('quiz_id');
         $table->string('title', 100)->nullable();
         $table->longText('description')->nullable();
         $table->integer('sequence')->default(0);
         $table->timestamps();
       });
     }

     public function down()
     {
       Schema::dropIfExists('quiz_page_breaks');
     }
}
