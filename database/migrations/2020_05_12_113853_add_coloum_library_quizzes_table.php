<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumLibraryQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('library_quizzes', function($table) {
            $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
            $table->longText('description')->nullable();
            $table->string('image', 255)->nullable()->default('quiz-default.png');
            $table->string('slug', 255)->nullable();
            $table->datetime('time_start')->nullable();
            $table->datetime('time_end')->nullable();
            $table->string('duration')->nullable()->default('60')->comment('default 60 minutes');
            $table->string('attempt')->default('-1')->nullable()->comment('-1 unlimited');
            $table->string('library_code')->nullable()->comment('library code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
