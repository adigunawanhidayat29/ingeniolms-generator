<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizQuestionsTable extends Migration
{
  public function up()
  {
    Schema::create('quiz_questions', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('quiz_id');
        $table->integer('quiz_type_id');
        $table->longText('question')->nullable();
        $table->integer('weight')->default(0);
        $table->integer('quiz_page_break_id')->nullable();
        $table->integer('sequence')->default(0);
        $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('quiz_questions');
  }
}
