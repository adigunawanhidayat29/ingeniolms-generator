<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseProjectCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_project_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_project_user_id');
            $table->integer('user_id');
            $table->longText('comment');
            $table->enum('status', ['0','1'])->default('1')->comment('0=nonactive, 1=active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_project_comments');
    }
}
