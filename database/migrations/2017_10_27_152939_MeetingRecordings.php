<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeetingRecordings extends Migration
{
  public function up()
  {
    Schema::create('meeting_recordings', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('meeting_id');
      $table->string('record');
      $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('meeting_recordings');
  }
}
