<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialLoginUsersTable extends Migration
{
  public function up()
  {
    Schema::create('social_login_users', function(Blueprint $table){
      $table->increments('id');
      $table->string('social_id');
      $table->string('email');
      $table->string('name');
      $table->string('avatar');
      $table->string('provider');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('social_login_users');
  }
}
