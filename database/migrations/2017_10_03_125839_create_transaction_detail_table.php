<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailTable extends Migration
{
  public function up()
  {
    Schema::create('transactions_details', function (Blueprint $table) {
      $table->increments('id');
      $table->string('invoice', 50);
      $table->integer('course_id')->nullable();
      $table->integer('program_id')->nullable();
      $table->integer('total');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('transactions_details');
  }
}
