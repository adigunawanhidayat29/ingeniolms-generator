<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscusionsTable extends Migration
{
  public function up()
  {
    Schema::create('discussions', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('course_id');
      $table->integer('user_id');
      $table->longText('body');
      $table->enum('status', ['0', '1'])->comment('0 draft, 1 publish')->default('0');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('discussions');
  }
}
