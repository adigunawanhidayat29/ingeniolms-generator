<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_broadcast_chats', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('course_id');
          $table->integer('broadcast_id');
          $table->integer('user_id');
          $table->longText('body');
          $table->enum('status', ['0', '1'])->comment('0 false, 1 true')->default('0');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_broadcast_chats');
    }
}
