<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContentVideoQuizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('content_video_quizes', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('content_id');
          $table->text('question');
          $table->string('seconds')->nullable();
          $table->enum('status', ['0','1'])->comment('0=draft, 1=publish');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('content_video_quizes');
    }
}
