<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorWithdrawalsTable extends Migration
{
  public function up()
  {
      Schema::create('instructor_withdrawals', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('instructor_id');
        $table->integer('amount');
        $table->enum('status', ['0','1'])->default('0');
        $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('instructor_withdrawals');
  }
}
