<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserlibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userlibraries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('type_content')->nullable();
            $table->string('name_file')->nullable();
            $table->string('path_file')->nullable();
            $table->string('full_path_file')->nullable();
            $table->string('full_path_file_resize')->nullable();
            $table->string('full_path_file_resize_720')->nullable();
            $table->string('full_path_original')->nullable();
            $table->string('full_path_original_resize')->nullable();
            $table->string('full_path_original_resize_720')->nullable();
            $table->string('file_status')->default(1);
            $table->integer('file_size')->default(0);
            $table->string('video_duration')->default(0)->nullable();
            $table->enum('status', ['0','1'])->default(0)->comment('0=draft, 1=publish');
            $table->enum('is_directory', ['0', '1'])->default(0)->comment('0=file, 1=directory');
            $table->string('parent')->default('.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userlibraries');
    }
}
