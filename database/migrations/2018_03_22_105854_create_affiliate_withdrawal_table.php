<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateWithdrawalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('affiliate_withdrawals', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('affiliate_id');
         $table->integer('request')->default(0);
         $table->enum('status', ['0', '1'])->comment('0 pending, 1 success')->default('0');
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('affiliate_withdrawals');
     }
}
