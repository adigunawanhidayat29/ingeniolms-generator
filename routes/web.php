<?php

// Degree
Route::get('degree/{any}', 'DegreeController@detail');
Route::get('degrees', 'DegreeController@index');
// Degree


Auth::routes();

// midtrans
Route::get('/vtweb', 'VtwebController@vtweb');
Route::get('/vtdirect', 'VtdirectController@vtdirect');
Route::post('/vtdirect', 'VtdirectController@checkout_process');
Route::get('/vt_transaction', 'PagesController@transaction');
Route::post('/vt_transaction', 'PagesController@transaction_process');
Route::post('/vt_notif', 'PagesController@notification');
Route::get('/pembayaran', 'SnapController@snap');
Route::get('/snaptoken', 'SnapController@token');
Route::post('/pembayaran/finish', 'SnapController@finish');
Route::get('/snapnotification', 'SnapController@notification');
// midtrans

Route::get('/password/reset', 'Auth\ForgotPasswordController@index');
Route::post('/password/reset', 'Auth\ForgotPasswordController@store');
Route::get('/password/reset/request/{token}', 'Auth\ResetPasswordController@getPassword');
Route::post('/password/reset/request', 'Auth\ResetPasswordController@updatePassword');

// vcon
Route::get('/vcon', 'VconController@index');
Route::get('/vcon/waiting/{id}', 'VconController@waiting');
Route::get('/vcon/waiting/{id}/check', 'VconController@waiting_check');
Route::post('/vcon/store', 'VconController@joinInstructor');
// vcon

// Route::get('app/notifications', 'BlastNotificationsController@getDataJson');
// Route::get('app/notifications/{id}', 'BlastNotificationsController@getDataDetailJson');

Route::get('video', 'VideoController@index');

Route::post('auth/check/email', 'UserController@_checkEmail');

Route::get('/sso/login', 'Sso\LoginController@sso_login');
Route::get('/sso/logout', 'Sso\LoginController@sso_logout');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
  // Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
  // Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
  Route::get('/', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\LfmController@show',
    'as' => 'show',
  ]);

  // display integration error messages
  Route::get('/errors', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\LfmController@getErrors',
    'as' => 'getErrors',
  ]);

  // upload
  Route::any('/upload', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload',
    'as' => 'upload',
  ]);

  // list images & files
  Route::get('/jsonitems', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@getItems',
    'as' => 'getItems',
  ]);

  Route::get('/move', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@move',
    'as' => 'move',
  ]);

  Route::get('/domove', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@domove',
    'as' => 'domove'
  ]);

  // folders
  Route::get('/newfolder', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\FolderController@getAddfolder',
    'as' => 'getAddfolder',
  ]);

  // list folders
  Route::get('/folders', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\FolderController@getFolders',
    'as' => 'getFolders',
  ]);

  // crop
  Route::get('/crop', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getCrop',
    'as' => 'getCrop',
  ]);
  Route::get('/cropimage', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getCropimage',
    'as' => 'getCropimage',
  ]);
  Route::get('/cropnewimage', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getNewCropimage',
    'as' => 'getCropnewimage',
  ]);

  // rename
  Route::get('/rename', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\RenameController@getRename',
    'as' => 'getRename',
  ]);

  // scale/resize
  Route::get('/resize', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\ResizeController@getResize',
    'as' => 'getResize',
  ]);
  Route::get('/doresize', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\ResizeController@performResize',
    'as' => 'performResize',
  ]);

  // download
  Route::get('/download', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\DownloadController@getDownload',
    'as' => 'getDownload',
  ]);

  // delete
  Route::get('/delete', [
    'uses' => '\UniSharp\LaravelFilemanager\Controllers\DeleteController@getDelete',
    'as' => 'getDelete',
  ]);

  // Route::get('/demo', 'DemoController@index');
});


Route::middleware(['auth'])->group(function () {

  Route::get('course/dashboard', 'CourseController@dashboard');
  Route::get('course/user/archive/{course_id}', 'CourseController@user_archive');
  Route::get('course/user/restore/{course_id}', 'CourseController@user_restore');

  // profile
  Route::get('logout', 'Auth\LoginController@logout');
  Route::get('profile', 'ProfileController@index');
  Route::post('profile/update', 'ProfileController@update');
  Route::post('profile/change-password', 'ProfileController@update_password');
  Route::post('profile/change-avatar', 'ProfileController@update_avatar');
  // profile

  // quiz participant
  Route::get('course/quiz/participant/serverside/{quiz_id}', 'QuizParticipantController@serverside');
  // quiz participant

  // quiz participant answer
  Route::get('course/quiz/participant/answer/{quiz_participant_id}/{quiz_id}', 'QuizParticipantAnswerController@index');
  Route::post('course/quiz/participant/answer/grade', 'QuizParticipantAnswerController@grade');
  Route::post('course/quiz/participant/answer/gradeOrder', 'QuizParticipantAnswerController@gradeOrder');
  Route::get('course/quiz/participant/download/answer/{quiz_participant_id}', 'QuizParticipantAnswerController@download');
  // route baru
  Route::post('course/quiz/participant/answer/grade/total', 'QuizParticipantAnswerController@grade_total');
  // quiz participant answer

  // suervey participant answer
  Route::get('course/survey/participant/serverside/{quiz_id}', 'SurveyParticipantController@serverside');
  Route::get('course/survey/participant/answer/{quiz_participant_id}/{quiz_id}', 'SurveyParticipantAnswerController@index');
  Route::post('course/survey/participant/answer/grade', 'SurveyParticipantAnswerController@grade');
  Route::post('course/survey/participant/answer/gradeOrder', 'SurveyParticipantAnswerController@gradeOrder');
  Route::get('course/survey/participant/download/answer/{quiz_participant_id}', 'SurveyParticipantAnswerController@download');
  Route::get('course/survey/participant/all/download/answer/{quiz_id}', 'SurveyParticipantAnswerController@downloadAll');
  // route baru
  Route::post('course/survey/participant/answer/grade/total', 'SurveyParticipantAnswerController@grade_total');
  // suervey participant answer

  // progress
  Route::post('progress/reset', 'ProgressController@reset');
  // progress

  // discussion
  Route::post('discussion/ask', 'DiscussionController@ask');
  Route::post('discussion/answer', 'DiscussionController@answer');
  Route::get('discussion/detail/{id}', 'DiscussionController@detail');
  // discussion

  // course user
  Route::post('course/rating/{course_id}', 'CourseController@rating');
  Route::get('course/enroll', 'CourseController@enroll');
  Route::post('course/check-password/{id}', 'CourseController@_checkPassword');
  Route::post('course/join-private-course', 'CourseController@joinPrivateCourse');
  Route::post('course/learn-content/finish', 'CourseController@learn_content_finish');
  Route::post('course/learn-content/unfinish', 'CourseController@learn_content_unfinish');
  Route::get('course/learn/content/{any}/{id}', 'CourseController@learn_content');
  Route::get('course/learn/content/{slug}/discussion/{id}', 'CourseController@learn_content_discussion');
  Route::get('course/learn/{any}', 'CourseController@learn');
  Route::post('course/learn/progress/finish/{id}', 'CourseController@course_finish');
  Route::get('course/streaming/{id}/{any}', 'CourseController@broadcast_streaming');
  Route::post('course/broadcast/message/save', 'BroadcastMessageController@save');
  Route::get('course/broadcast/message/get/{id}', 'BroadcastMessageController@get');
  Route::get('my-course', 'CourseController@my');
  Route::get('course/my/loadmore', 'CourseController@my_loadmore');
  Route::post('course/set-ceritifcated', 'CourseController@setCeritifcated');
  // course user

  //BIGBLEBUTTON
  Route::get('bigbluebutton/meeting/get', 'BigBlueButtonController@get');
  Route::get('bigbluebutton/meeting/create', 'BigBlueButtonController@create');
  Route::get('bigbluebutton/meeting/instructor/join/{course_id}/{meeting_schdule_id}', 'BigBlueButtonController@joinInstructor');
  Route::get('bigbluebutton/meeting/join/{course_id}/{meeting_schdule_id}', 'BigBlueButtonController@join');
  Route::get('bigbluebutton/meeting/close', 'BigBlueButtonController@close');
  Route::get('bigbluebutton/meeting/info', 'BigBlueButtonController@info');
  Route::get('bigbluebutton/meeting/upload/record/success', 'BigBlueButtonController@successUploadRecord');
  Route::get('bigbluebutton/meeting/upload/record/{course_id}', 'BigBlueButtonController@uploadRecord');
  Route::post('bigbluebutton/meeting/save/record/{course_id}', 'BigBlueButtonController@saveRecord');
  Route::get('bigbluebutton/recording/get', 'BigBlueButtonController@getRecording');
  Route::get('bigbluebutton/recording/delete', 'BigBlueButtonController@deleteRecording');
  Route::get('bigbluebutton/logout/{course_id}', 'BigBlueButtonController@logout');
  //BIGBLEBUTTON

  // message
  Route::post('message/save', 'MessageController@save');
  Route::get('message/get/{course_id}', 'MessageController@get');
  // message

  //QUIZ
  Route::get('course/learn/quiz/{slug}/{id}', 'QuizController@detail');
  Route::get('course/learn/quiz/{slug}/{id}/start', 'QuizController@start');
  Route::post('course/learn/quiz/{slug}/{id}/ajax', 'QuizController@start_ajax');
  Route::post('course/learn/quiz/{slug}/{id}/finish', 'QuizController@finish');
  Route::get('course/learn/quiz/{slug}/{id}/result/{id_participant}', 'QuizController@result');
  Route::post('course/learn/survey/upload', 'QuizController@upload');
  //QUIZ

  // ASSIGNMENT
  Route::get('course/learn/assignment/{slug}/{id}', 'AssignmentController@detail');
  Route::get('assignment/success/{id}', 'AssignmentController@success');
  Route::get('assignment/{id}', 'AssignmentController@detail')->middleware('course_user');
  Route::post('assignment/post/{id}', 'AssignmentController@post')->middleware('course_user');
  Route::post('assignment/upload/{id}', 'AssignmentController@upload')->middleware('course_user');
  Route::post('assignment/upload/success/{id}', 'AssignmentController@upload_success')->middleware('course_user');
  // ASSIGNMENT

  // announcement
  Route::post('course/announcement/comment', 'CourseAnnouncementController@comment');
  // announcement

  // course project user
  Route::get('course/project-user/{project_id}', 'CourseProjectController@create');
  Route::get('course/project-user/show/{slug}', 'CourseProjectController@show');
  Route::get('course/project-user/edit/{project_id}/{project_user_id}', 'CourseProjectController@edit');
  Route::post('course/project-user/store/{project_id}', 'CourseProjectController@store');
  Route::post('course/project-user/update/{project_user_id}', 'CourseProjectController@update');
  Route::post('course/project/comment/{id}', 'CourseProjectController@comment');
  Route::post('course/project/like/{id}', 'CourseProjectController@like');
  Route::post('course/update/submit', 'CourseController@update_submit');
  // course project user

  Route::middleware(['instructor'])->group(function () {

    // COURSE
    Route::get('course/advanced/setting/{id}', 'CourseController@setting');
    Route::post('course/advanced/setting/activity/completion/create/{id}/{type}', 'CourseController@activity_completion_create');
    Route::post('course/advanced/setting/restrict/order/create/{course_id}', 'CourseController@restrict_order_create');
    Route::post('course/advanced/setting/activity/order/generate/{id}', 'CourseController@activity_order_generate');
    Route::post('course/advanced/setting/activity/completion/destroy/{id}', 'CourseController@activity_completion_destroy');
    Route::get('course/serverside', 'CourseController@serverside');
    Route::get('course/show/{id}', 'CourseController@show');
    Route::get('course/create', 'CourseController@create');
    Route::post('course/create_action', 'CourseController@create_action');

    Route::get('course/manage/{id}', 'CourseController@manage');
    Route::put('course/update_action', 'CourseController@update_action');
    Route::get('course/delete/{id}', 'CourseController@delete');
    Route::post('course/publish', 'CourseController@publish');
    Route::get('course/publish_unpublish/{id}', 'CourseController@publish_unpublish');
    Route::get('course/participant/{course_id}', 'CourseController@participant');
    Route::get('course/preview/{course_id}', 'CourseController@preview');
    Route::post('course/editable/{id}', 'CourseController@editable');
    Route::get('course/duplicate/{course_id}', 'CourseController@duplicate');
    Route::get('course/archive/{course_id}', 'CourseController@archive');
    Route::get('course/restore/{course_id}', 'CourseController@restore');
    Route::get('course/archived', 'CourseController@archived');
    Route::get('course/atendee/{course_id}', 'CourseController@atendee');
    Route::post('courses/atendee/import/{course_id}', 'CourseImportController@atendee');
    Route::get('course/attendance/{course_id}', 'CourseAttendanceController@index');
    Route::post('course/attendance/store/{course_id}', 'CourseAttendanceController@store');
    Route::post('course/attendance/submit/{course_id}/{id}', 'CourseAttendanceController@submit');
    Route::post('course/attendance/submit/user/{course_id}/{id}', 'CourseAttendanceController@submit_user');
    Route::post('course/attendance/update/{id}', 'CourseAttendanceController@update');
    Route::get('course/attendance/delete/{id}', 'CourseAttendanceController@delete');
    Route::get('course/attendance/download/excel/{course_id}', 'CourseAttendanceController@download');

    Route::get('course/completion/{course_id}', 'CourseController@completion');
    Route::get('course/completion/{course_id}/export', 'ExportController@completion');
    Route::get('course/completion/detail/{course_id}/{user_id}', 'CourseController@completionDetail');
    Route::get('course/grades/{course_id}', 'CourseController@grades');
    Route::get('course/grades/{course_id}/export', 'ExportController@grades');
    Route::get('course/grades/{course_id}/user/{user_id}', 'CourseController@grades_user');
    Route::get('course/grades/{course_id}/quiz/{quiz_id}', 'CourseController@grades_quiz');
    Route::get('course/grades/{course_id}/assignment/{assignment_id}', 'CourseController@grades_assignment');
    Route::get('course/grades/setting/{course_id}', 'GradingController@setting');
    Route::post('course/grades/setting/save/{course_id}', 'GradingController@setting_save');
    Route::get('course/broadcast/{slug}', 'CourseController@broadcast_start');
    Route::get('instructor/library/old', 'CourseController@library');
    //Route::get('instructor/library/{folder_name}', 'CourseController@library_list');
    Route::post('library/create/folder', 'CourseController@library_create_folder');
    Route::post('upload/file/{user_id}', 'CourseController@library_upload_file');
    Route::post('course/preview/update-authors', 'CourseController@update_authors');
    Route::post('course/preview/update-instructor-groups', 'CourseController@update_instructor_groups');

    // CERTIFICATE
    Route::get('courses/certificate/{course_id}', 'CertificateController@index');
    Route::post('course/ceritificate/users/save/{course_id}', 'CertificateController@saveUsers');
    Route::get('courses/certificate/learn/{course_id}', 'CertificateController@learn');
    // CERTIFICATE

    // AKSES KONTEN
    Route::get('courses/access-content/{course_id}', 'CourseProgressController@index');
    // AKSES KONTEN

    // Route::get('instructor/library', 'LibraryController@show');

    // COURSE

    // transaction
    Route::get('instructor/transactions', 'InstructorTransactionController@index');
    Route::get('instructor/transactions/mutation', 'InstructorTransactionController@mutation');
    Route::post('instructor/transactions/withdrawal', 'InstructorTransactionController@withdrawal');
    Route::get('instructor/transactions/withdrawal/delete/{id}', 'InstructorTransactionController@withdrawal_delete');
    Route::post('instructor/banks/update', 'InstructorTransactionController@account_bank');
    // transaction

    // instructor group
    Route::get('instructor/groups/detail/{id}', 'InstructorGroupController@show');
    Route::post('instructor/groups/detail/{id}/discussion/store', 'InstructorGroupController@add_discussion');
    Route::post('instructor/groups/discussion/update/{id}', 'InstructorGroupController@update_discussion');
    Route::post('instructor/groups/discussion/delete/{id}', 'InstructorGroupController@delete_discussion');
    Route::post('instructor/groups/detail/{id}/add/resource', 'InstructorGroupController@add_resource');
    Route::get('instructor/groups/discussion/{id}', 'InstructorGroupController@discussion');
    Route::post('instructor/groups/discussion/{id}', 'InstructorGroupController@discussion_list');
    Route::post('instructor/groups/discussion/detail/update/{id}', 'InstructorGroupController@update_discussion_detail');
    Route::post('instructor/groups/discussion/detail/delete/{id}', 'InstructorGroupController@delete_discussion_detail');

    Route::get('instructor/groups', 'InstructorGroupController@index');
    Route::get('instructor/groups/create', 'InstructorGroupController@create');
    Route::post('instructor/groups/store', 'InstructorGroupController@store');
    Route::get('instructor/groups/edit/{id}', 'InstructorGroupController@edit');
    Route::post('instructor/groups/update/{id}', 'InstructorGroupController@update');
    Route::post('instructor/group/update/submit', 'InstructorGroupController@group_update_submit');
    Route::post('instructor/group/update/delete/{id}', 'InstructorGroupController@group_update_delete');
    // instructor group
    Route::get('instructor/groups/{id}/remove/user/{user_id}', 'InstructorGroupController@remove_user');
    // section
    Route::post('course/save_section', 'CourseController@save_section');
    Route::post('section/edit-section', 'SectionController@edit_section');
    Route::put('course/section/update_action/{id}', 'SectionController@update_action');
    Route::get('course/section/delete/{id_course}/{id}', 'SectionController@delete');
    Route::post('course/section/update-sequence', 'SectionController@update_sequence');
    Route::post('course/section/change-status', 'SectionController@change_status');
    // section

    // content
    Route::get('course/content/create/scorm/{id}', 'ContentController@create_scorm');
    Route::post('course/content/create/scorm/{id}', 'ContentController@store_scorm');
    Route::get('course/content/create/{id}', 'ContentController@create');
    Route::post('course/content/upload/{section_id}', 'ContentController@upload');
    Route::post('course/content/create_action/{id}', 'ContentController@create_action');
    Route::get('course/content/update/{id}', 'ContentController@update');
    Route::get('course/content/publish/{id}/{course_id}', 'ContentController@publish');
    Route::get('course/content/unpublish/{id}/{course_id}', 'ContentController@unpublish');
    Route::post('course/content/update_action', 'ContentController@update_action');
    Route::get('course/content/delete/{course_id}/{id}', 'ContentController@delete');
    Route::post('course/content/store-draft', 'ContentController@store_draft');
    Route::post('course/content/update-sequence', 'ContentController@update_sequence');
    Route::post('course/content/order/update', 'CourseController@update_order');
    Route::post('course/content/update-preview', 'ContentController@update_preview');
    Route::get('course/content/add/library/{id}/{course_id}', 'Library\LibraryController@create_action_course');
    Route::get('course/content/discussion/manage/{content_id}', 'ContentController@discussion');
    Route::get('course/content/discussion/manage/grade/{content_id}', 'ContentController@discussion_grade');
    Route::post('course/content/discussion/manage/grade/nilai/{siswa_id}/{content_id}', 'ContentController@discussion_grade_nilai');
    Route::get('course/content/folder/manage/{content_id}', 'ContentController@folder');
    Route::post('library/content/store-draft', 'Library\LibraryController@store_draft');
    Route::post('library/content/upload', 'Library\LibraryController@upload');
    // content

    // content video quiz
    Route::get('course/content/video/manage/{content_id}', 'ContentVideoQuizController@index');
    Route::get('course/content/video/delete/{content_id}', 'ContentVideoQuizController@delete');
    Route::post('content-video-quiz/store', 'ContentVideoQuizController@store');
    Route::post('content-video-quiz/check-answer', 'ContentVideoQuizController@check_answer');
    // content video quiz

    // Assignment
    Route::get('course/assignment/create/{id}', 'AssignmentController@create');
    Route::post('course/assignment/create_action/{id}', 'AssignmentController@create_action');
    Route::post('course/assignment/create_draft/{id}', 'AssignmentController@create_draft');
    Route::get('course/assignment/update/{id}', 'AssignmentController@update');
    Route::get('course/assignment/publish/{id}', 'AssignmentController@publish');
    Route::get('course/assignment/unpublish/{id}', 'AssignmentController@unpublish');
    Route::put('course/assignment/update_action', 'AssignmentController@update_action');
    Route::get('course/assignment/delete/{content_id}/{id}', 'AssignmentController@delete');
    Route::get('course/assignment/answer-preview/{id}', 'AssignmentController@previewAnswer');
    Route::post('course/assignment/save-grade', 'AssignmentController@saveGrade');
    Route::get('course/assignment/answer/{assignment_id}', 'AssignmentController@answer');
    Route::get('course/assignment/add/library/{id}/{course_id}', 'Library\LibraryController@create_action_assignment');
    // Assignment

    //survey
    Route::get('course/survey/create/{id}', 'SurveyManageController@create');
    Route::post('course/survey/create_action/{id}', 'SurveyManageController@create_action');
    Route::get('course/survey/update/{id}', 'SurveyManageController@update');
    Route::get('course/survey/publish/{id}/{course_id}', 'SurveyManageController@publish');
    Route::get('course/survey/unpublish/{id}/{course_id}', 'SurveyManageController@unpublish');
    Route::put('course/survey/update_action', 'SurveyManageController@update_action');
    Route::get('course/survey/delete/{course_id}/{id}', 'SurveyManageController@delete');
    Route::get('library/survey/{slug}', 'LibraryController@library_survey_redirect');
    Route::post('course/survey/page-break/update-sequence', 'SurveyManageController@update_sequence');
    Route::get('course/survey/page-break/delete/{id}', 'SurveyManageController@delete_page_break');
    Route::post('course/survey/pagebreak/update/', 'SurveyManageController@edit_page_break');
    Route::get('course/survey/add/library/{id}/{course_id}', 'Library\LibraryController@create_action_survey');
    //survey

    // quiz
    Route::get('course/quiz/create/{id}', 'QuizManageController@create');
    Route::post('course/quiz/create_action/{id}', 'QuizManageController@create_action');
    Route::get('course/quiz/update/{id}', 'QuizManageController@update');
    Route::get('course/quiz/publish/{id}/{course_id}', 'QuizManageController@publish');
    Route::get('course/quiz/unpublish/{id}/{course_id}', 'QuizManageController@unpublish');
    Route::put('course/quiz/update_action', 'QuizManageController@update_action');
    Route::get('course/quiz/delete/{course_id}/{id}', 'QuizManageController@delete');
    Route::get('library/quiz/{slug}', 'LibraryController@library_quiz_redirect');
    Route::post('course/quiz/page-break/update-sequence', 'QuizManageController@update_sequence');
    Route::get('course/quiz/page-break/delete/{id}', 'QuizManageController@delete_page_break');
    Route::post('course/quiz/pagebreak/update/', 'QuizManageController@edit_page_break');
    Route::get('course/quiz/add/library/{id}/{course_id}', 'Library\LibraryController@create_action_quiz');
    // quiz

    // survei Question
    Route::post('course/survey/create/pagebreak/{id}', 'SurveyQuestionController@create_pagebreak');
    Route::get('course/survey/question/manage/{id}', 'SurveyQuestionController@manage');
    Route::get('course/survey/question/preview/{id}', 'SurveyQuestionController@preview');
    Route::get('course/survey/question/answer/download/{id}', 'SurveyQuestionController@download');
    Route::get('course/survey/question/create/{id}', 'SurveyQuestionController@create');
    Route::post('course/survey/question/create_action/{id}', 'SurveyQuestionController@create_action');
    Route::get('course/survey/question/update/{quiz_id}/{id}', 'SurveyQuestionController@update');
    Route::put('course/survey/question/update_action/{quiz_id}', 'SurveyQuestionController@update_action');
    Route::get('course/survey/question/delete/{quiz_id}/{id}', 'SurveyQuestionController@delete');
    Route::get('course/survey/question/import/{id}', 'SurveyQuestionController@import');
    Route::post('course/survey/question/import_store/{id}', 'SurveyQuestionController@import_store');
    Route::post('course/survey/question/update-sequence', 'SurveyQuestionController@update_sequence');
    // quiz Question

    // quiz Question
    Route::post('course/quiz/create/pagebreak/{id}', 'QuizQuestionController@create_pagebreak');
    Route::get('course/quiz/question/manage/{id}', 'QuizQuestionController@manage');
    Route::get('course/quiz/question/preview/{id}', 'QuizQuestionController@preview');
    Route::get('course/quiz/question/answer/download/{id}', 'QuizQuestionController@download');
    Route::get('course/quiz/question/create/{id}', 'QuizQuestionController@create');
    Route::post('course/quiz/question/create_action/{id}', 'QuizQuestionController@create_action');
    Route::get('course/quiz/question/update/{quiz_id}/{id}', 'QuizQuestionController@update');
    Route::put('course/quiz/question/update_action/{quiz_id}', 'QuizQuestionController@update_action');
    Route::get('course/quiz/question/delete/{quiz_id}/{id}', 'QuizQuestionController@delete');
    Route::get('course/quiz/question/import/{id}', 'QuizQuestionController@import');
    Route::post('course/quiz/question/import_store/{id}', 'QuizQuestionController@import_store');
    Route::post('course/quiz/question/update-sequence', 'QuizQuestionController@update_sequence');
    // quiz Question

    // survey Question
    Route::get('course/survey/question/preview/{id}', 'SurveyQuestionController@preview');
    // quiz Question

    // quiz Question answer
    Route::post('course/quiz/question/answer/create', 'QuizQuestionAnswerController@create');
    Route::get('course/quiz/question/answer/delete/{quiz_id}/{id}', 'QuizQuestionAnswerController@delete');
    Route::post('course/quiz/question/answer/update_action', 'QuizQuestionAnswerController@update_action');
    // quiz Question answer

    // meeting
    Route::post('meeting/publish', 'MeetingController@publish');
    Route::post('meeting/schedule/store', 'MeetingController@store');
    Route::get('meeting/schedule/delete/{id}', 'MeetingController@destroy');
    Route::post('meeting/schedule/update', 'MeetingController@update');
    // meeting

    // announcements
    Route::get('course/announcement/{course_id}', 'CourseAnnouncementController@index');
    Route::get('course/announcement/create/{course_id}', 'CourseAnnouncementController@create');
    Route::post('course/announcement/store/{course_id}', 'CourseAnnouncementController@store');
    Route::get('course/announcement/edit/{id}', 'CourseAnnouncementController@edit');
    Route::post('course/announcement/update/{id}', 'CourseAnnouncementController@update');
    Route::delete('course/announcement/delete/{id}', 'CourseAnnouncementController@delete');
    // announcements

    //Report
    /*Route::get('report', 'ReportController@index');
    Route::get('report/get-content/{id}', 'ReportController@get_content');*/

    Route::get('report/summary/{course_id}', 'ReportController@summary');
    Route::get('report/summary/detail/{id}', 'ReportController@summary_detail');
    //Report

    //Grading
    // Route::get('grading', 'GradingController@index');
    // Route::get('grading/get-quiz/{id}', 'GradingController@get_quiz');
    //Grading

    Route::get('video-learning-studio', 'CreatorStudioController@index');
    Route::get('video-learning-studio/steps', 'CreatorStudioController@steps');

    // manager
    Route::get('become-a-manager', 'ManagerController@welcome');
    Route::get('become-a-manager/join', 'ManagerController@join');
    Route::get('become-a-manager/join/success', 'ManagerController@join_success');
    // manager

    // route only manager
    // Route::middleware(['manager'])->group(function () {
    //   // Program
    //   Route::get('program/dashboard', 'ProgramController@dashboard');
    //   Route::get('program/create', 'ProgramController@create');
    //   Route::post('program/store', 'ProgramController@store');
    //   Route::get('program/edit/{id}', 'ProgramController@edit');
    //   Route::put('program/update', 'ProgramController@update');
    //   Route::get('program/delete/{id}', 'ProgramController@destroy');
    //   Route::get('program/module/add/{id}', 'ProgramController@module_add');
    //   Route::post('program/module/store/{id}', 'ProgramController@module_store');
    //   Route::get('program/module/delete/{id}', 'ProgramController@module_delete');
    //   // Program
    // });
    // route only manager

    Route::post('courses/student/group/store/{id}', 'CourseStudentGroupController@store');
    Route::post('courses/student/group/add-user/{id}', 'CourseStudentGroupController@addUser');
  });
});

Route::get('download_badge/{image_name}', 'ProfileController@download_badge');
Route::get('badge_detail_user/{id}', 'ProfileController@detail_badge');

//badge fix

Route::get('{id}/criteria_fix', 'BadgeCriteriaController@create');
Route::post('{course_id}/{badge_id}/criteria_assign', 'BadgeCriteriaController@assign');
Route::post('activity_completion/store', 'BadgeCriteriaController@store');

Route::get('{course_id}/create_badge', 'BadgeFixController@create');
Route::get('{id}/list_badges', 'BadgeFixController@index');
Route::get('{id}/list_badges_user', 'BadgeFixController@index_user');
Route::post('store_badge', 'BadgeFixController@store');
Route::get('{course_id}/{badge_id}/badge_overview', 'BadgeFixController@overview');

Route::post('badge/publish/{id}', 'BadgeFixController@publish');
Route::post('badge/unpublish/{id}', 'BadgeFixController@unpublish');

Route::post('{badge_id}/complete_criteria/update', 'BadgeFixController@update');

//Course Leveling
Route::get('{course_id}/level_settings', 'CourseLevelingController@settings');
Route::post('{course_id}/update_levels', 'CourseLevelingController@update_levels');
Route::post('{course_id}/update_settings', 'CourseLevelingController@update_settings');
Route::post('{course_id}/update_visuals', 'CourseLevelingController@update_visuals');
Route::get('{course_id}/{user_id}/edit_reports', 'CourseLevelingController@edit_reports');
Route::post('{course_id}/{user_id}/update_reports', 'CourseLevelingController@update_reports');
Route::post('/update_rules', 'CourseLevelingController@update_rules');
Route::post('/update_image', 'CourseLevelingController@update_image');

Route::get('course/{course_id}/level_up_ladder', 'CourseController@getLadder');

// HOME
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('course/preview/content/{slug}/{id}', 'CoursePreviewController@detail');
Route::get('course/preview/content/{slug}/assignment/{id}', 'CoursePreviewController@detail_assignment');

Route::get('course/catalog', function () {
  return view('course.catalog');
});

// Route::get('/', 'HomeController@soon'); // soon page
Route::get('pengajar', 'HomeController@teacher');
Route::get('beta', 'HomeController@index');
Route::get('category/{any}', 'HomeController@category');
Route::get('projects', 'CourseProjectController@all');
Route::get('project/{any}', 'CourseProjectController@show');
Route::get('search', 'HomeController@search');
Route::post('change-language', ['before' => 'csrf', 'uses' => 'LanguageController@ChangeLanguage']);
Route::get('privacy-policy', 'HomeController@privacy_policy');
Route::get('term-condition', 'HomeController@tnc');
Route::get('contact-us', 'HomeController@contact_us');
// HOME

// CART
Route::get('cart', 'CartController@index');
Route::get('cart/get/{id}', 'CartController@get');
Route::get('cart/remove/{id}', 'CartController@remove');
Route::post('cart/add/{id}', 'CartController@add');
Route::post('cart/buy/{id}', 'CartController@buy');
Route::post('cart/add/program/{id}', 'CartController@add_program');
Route::post('cart/buy/program/{id}', 'CartController@buy_program');
Route::get('cart/checkout', 'CartController@checkout')->middleware('auth');
Route::get('cart/checkout/v2', 'CartController@checkout_v2')->middleware('auth');
Route::post('cart/promotion', 'CartController@promotion')->middleware('auth');
// CART

// TRANSACTION
Route::post('transaction/buy', 'TransactionController@process')->middleware('auth');
Route::get('transaction/information/{invoice}', 'TransactionController@information')->middleware('auth');
Route::get('transaction/information/v2/{invoice}', 'TransactionController@information_v2')->middleware('auth');
Route::get('transaction/success/information/v2/{invoice}', 'PaymentController@approve_transaction')->middleware('auth');
Route::get('transaction/payment/confirmation/{invoice}', 'TransactionController@confirmation')->middleware('auth');
Route::post('transaction/payment/confirmation_action/{invoice}', 'TransactionController@confirmation_action')->middleware('auth');
Route::get('transaction/payment/confirmation/{invoice}/information', 'TransactionController@confirmation_information')->middleware('auth');
// TRANSACTION

//Instructor
Route::get('instructor', 'InstructorController@index');
Route::get('instructor/join', 'InstructorController@join');
Route::post('instructor/join_action', 'InstructorController@join_action');
Route::get('instructor/waiting', 'InstructorController@waiting');
Route::get('instructor/redirect/facebook', 'InstructorController@redirect_facebook');
Route::get('instructor/redirect/google', 'InstructorController@redirect_google');
//Instructor

// social media login
Route::get('auth/facebook', 'SocialLoginController@Facebook');
Route::get('auth/facebook/redirect', 'SocialLoginController@FacebookCallback');
Route::get('auth/google', 'SocialLoginController@Google');
Route::get('auth/google/redirect', 'SocialLoginController@GoogleCallback');
// social media login

// activation user
Route::get('user/activation/success', 'ActivationController@success');
Route::get('user/activation/{token}', 'ActivationController@activate');
Route::get('user/resend/activation', 'ActivationController@resend');
// activation user

// COURSE
Route::get('course/{any}', 'CourseController@detail');
Route::get('courses', 'CourseController@index');
Route::get('courses-live', 'CourseController@live');
Route::post('course/content', 'ContentController@detail');
Route::post('course/content/folder/submit/{content_id}', 'ContentController@folder_submit');
Route::post('course/content/folder/item/delete/{item_id}', 'ContentController@folder_item_delete');
Route::post('course/content/discussion/submit/{content_id}', 'ContentController@discussion_submit');
Route::post('course/content/discussion/delete/{content_id}', 'ContentController@discussion_delete');
// COURSE

// Kategori
Route::get('categories', 'CategoryController@index');
// Kategori

// PROGRAMS
Route::post('program/check-password/{id}', 'ProgramController@_checkPassword')->middleware('auth');
Route::post('program/enroll/{id}', 'ProgramController@enroll')->middleware('auth');
Route::post('program/enroll-ajax/{id}', 'ProgramController@enroll_ajax')->middleware('auth');
Route::get('program/learn/{any}', 'ProgramController@learn')->middleware('auth');
Route::get('programs/dashboard', 'ProgramController@dashboard');
Route::get('program/{any}', 'ProgramController@detail');
Route::get('programs', 'ProgramController@index');
// PROGRAMS

// youtube
Route::get('youtube/streaming', 'YoutubeController@streaming');
Route::post('youtube/streaming/complete', 'YoutubeController@streaming_complete');
Route::get('youtube/screen-sharing', 'YoutubeController@screen_sharing');
// youtube

Route::get('user/{slug}', 'UserController@detail');

Route::get('m/profile/{id}', 'MobileProfileController@index');
Route::post('m/profile/update/{id}', 'MobileProfileController@update');
Route::post('m/profile/update/avatar/{id}', 'MobileProfileController@update_avatar');
Route::post('m/profile/update/password/{id}', 'MobileProfileController@update_password');

// AFFILIEATE
Route::get('affiliate', 'AffiliateController@index');
Route::post('affiliate/join', 'AffiliateController@join');
Route::post('affiliate/join/user', 'AffiliateController@join_user');
Route::post('affiliate/login', 'AffiliateController@login');
Route::get('affiliate/profile/edit', 'AffiliateController@profile_edit')->middleware('auth');
Route::post('affiliate/profile/update', 'AffiliateController@profile_update')->middleware('auth');
Route::post('affiliate/profile/edit-code', 'AffiliateController@edit_code')->middleware('auth');
Route::get('affiliate/transaction', 'AffiliateController@transaction')->middleware('auth');
Route::get('affiliate/balance', 'AffiliateController@balance')->middleware('auth');
Route::get('affiliate/withdrawal', 'AffiliateController@withdrawal')->middleware('auth');
Route::post('affiliate/withdrawal/request', 'AffiliateController@withdrawal_request')->middleware('auth');
Route::get('affiliate/links', 'AffiliateController@links')->middleware('auth');
// AFFILIEATE

//BIGBLUEBUTTON MOBILE
Route::get('bigbluebutton/meeting/join/{course_id}/{user_id}/{meeting_schedule_id}', 'BigBlueButtonController@join_mobile');
//BIGBLUEBUTTON MOBILE

//Delete file on library
Route::get('file/delete/{id}', 'CourseController@delete_lib');
//Delete file on library



// content detail
Route::post('course/content', 'ContentController@detail');
// content detail



// LIBRARY OlD
Route::get('instructor/library', 'Library\LibraryController@index');
Route::post('instructor/library/group/{id}', 'InstructorGroupController@instructor_group_content_destroy');
Route::post('instructor/group/join', 'InstructorGroupController@join_group');

//libraryedit
Route::get('library/course/content/update/{id}', 'Library\LibraryController@edit');
Route::get('library/course/survey/update/{id}', 'Library\LibraryController@edit_quiz');
Route::get('library/course/assignment/update/{id}', 'Library\LibraryController@edit_assignment');
Route::post('library/course/content/update/{id}', 'Library\LibraryController@update');
Route::post('library/course/survey/update/{id}', 'Library\LibraryController@update_quiz');
Route::post('library/course/assignment/update/{id}', 'Library\LibraryController@update_assignment');

// library destroy
Route::post('library/course/content/shared/destroy/{id}', 'Library\LibraryController@library_shared_destroy');
Route::post('library/course/content/destroy/{id}', 'Library\LibraryController@library_destroy');

//library rollback
Route::post('library/course/content/rollback/{id}', 'Library\LibraryController@rollback');

Route::get('library/course/content/create', 'Library\LibraryController@create');
Route::post('library/course/content/create_action', 'Library\LibraryController@create_action');

//library survey
Route::get('library/course/survey/create', 'Library\LibraryController@create_survey');
Route::post('library/course/survey/create_action', 'Library\LibraryController@create_survey_action');
Route::get('library/course/survey/manage/{id}', 'Library\SurveyQuestionController@manage');
Route::get('library/course/survey/question/delete/{quiz_id}/{id}', 'Library\SurveyQuestionController@delete');
Route::post('library/course/survey/create/pagebreak/{id}', 'Library\SurveyQuestionController@create_pagebreak');
Route::post('library/course/survey/page-break/update-sequence', 'Library\SurveyManageController@update_sequence');
Route::post('library/course/survey/question/update-sequence', 'Library\SurveyQuestionController@update_sequence');
Route::get('library/course/survey/question/update/{quiz_id}/{id}', 'Library\SurveyQuestionController@update');
Route::put('library/course/survey/question/update_action/{quiz_id}', 'Library\SurveyQuestionController@update_action');
Route::get('library/course/survey/question/create/{id}', 'Library\SurveyQuestionController@create');
Route::post('library/course/survey/question/create_action/{id}', 'Library\SurveyQuestionController@create_action');
Route::post('library/course/survey/pagebreak/update/', 'Library\SurveyManageController@edit_page_break');
Route::get('library/course/survey/page-break/delete/{id}', 'Library\SurveyManageController@delete_page_break');
Route::put('library/course/survey/update_action/{id}', 'Library\SurveyManageController@update_action');
//end

//library quiz
Route::get('library/course/quiz/create', 'Library\LibraryController@create_quiz');
Route::post('library/course/quiz/create_action', 'Library\LibraryController@create_quiz_action');
Route::get('library/course/quiz/manage/{id}', 'Library\QuizQuestionController@manage');
Route::get('library/course/quiz/question/delete/{quiz_id}/{id}', 'Library\QuizQuestionController@delete');
Route::post('library/course/quiz/create/pagebreak/{id}', 'Library\QuizQuestionController@create_pagebreak');
Route::post('library/course/quiz/page-break/update-sequence', 'Library\QuizManageController@update_sequence');
Route::post('library/course/quiz/question/update-sequence', 'Library\QuizQuestionController@update_sequence');
Route::get('library/course/quiz/question/update/{quiz_id}/{id}', 'Library\QuizQuestionController@update');
Route::put('library/course/quiz/question/update_action/{quiz_id}', 'Library\QuizQuestionController@update_action');
Route::get('library/course/quiz/question/create/{id}', 'Library\QuizQuestionController@create');
Route::post('library/course/quiz/question/create_action/{id}', 'Library\QuizQuestionController@create_action');
Route::post('library/course/quiz/pagebreak/update/', 'Library\QuizManageController@edit_page_break');
Route::get('library/course/quiz/page-break/delete/{id}', 'Library\QuizManageController@delete_page_break');
Route::put('library/course/quiz/update_action/{id}', 'Library\QuizManageController@update_action');
//end


Route::get('library/course/assignment/create/', 'Library\AssignmentController@create');
Route::post('library/course/assignment/create_action/', 'Library\AssignmentController@create_action');
Route::post('library/shared/{library_id}/{directory_id}/', 'Library\LibraryController@shared');
Route::post('library/public/{library_id}/shared', 'Library\LibraryController@share_add_public');
Route::post('library/shared/group/{library_id}/{directory_id}/', 'Library\LibraryController@group_shared');

// library group
Route::post('library/group/create/', 'Library\LibraryController@shared_group');
Route::post('library/group/shared/destroy/{id}', 'Library\LibraryController@group_shared_destroy');
Route::post('library/group/shared/rollback/{id}', 'Library\LibraryController@group_rollback');
Route::post('library/group/destroy/{id}', 'Library\LibraryController@group_destroy');
Route::post('library/group/add/content/{id}', 'Library\LibraryController@add_group');
Route::post('library/group/update/{id}', 'Library\LibraryController@edit_group');
Route::get('course/content/library/add/{course_id}/{section_id}', 'Library\LibraryController@add');
Route::post('course/content/library/add/{course_id}/{section_id}', 'LibraryController@add_content');
Route::post('course/content/library/course/add/{course_id}/{section_id}', 'LibraryController@add_content_course');
Route::post('course/content/library/add/share', 'Library\LibraryController@share_add');
Route::post('course/folder/library/add/share', 'Library\LibraryController@share_folder_add');
Route::post('course/content/library/group/share', 'Library\LibraryController@group_add');

//bank soal
Route::post('library/bank/soal/create/', 'Library\BankSoalController@store');
Route::get('library/bank/soal/show/{id}', 'Library\BankSoalController@show');
Route::get('library/bank/soal/get/{quiz_id}/{id}', 'Library\BankSoalController@get_question');
Route::post('library/bank/soal/get/{quiz_id}/{id}', 'Library\BankSoalController@get_question_create');
Route::post('library/bank/soal/destroy/{bank_soal_id}/{id}', 'Library\BankSoalController@destroy');
Route::get('library/course/survey/bank/question/create/{id}', 'Library\BankSoalController@create_question');
Route::post('library/course/survey/bank/question/create_action/{id}', 'Library\BankSoalController@create_action');
Route::get('library/course/quiz/bank/question/create/{id}', 'Library\BankSoalController@create_question');
Route::post('library/course/quiz/bank/question/create_action/{id}', 'Library\BankSoalController@create_action');
Route::get('course/quiz/get/{quiz_id}/{bank_id}', 'QuizQuestionController@show_quiz_library_course');
Route::post('course/quiz/get/{quiz_id}/{bank_id}', 'QuizQuestionController@get_question_create');

// END STUFF
Route::get('ckeditor/browser', 'CkeditorController@browser');
Route::post('ckeditor/uploader', 'CkeditorController@uploader');

// ROUTE ADMIN

Route::get('admin/login',  'Admin\Auth\LoginController@showLoginForm');
Route::post('admin/login', 'Admin\Auth\LoginController@login');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin', 'middleware' => 'IsAdmin'], function () {

  Route::get('/employeers', 'CorporatesController@index');
  Route::get('/employeers/create', 'CorporatesController@create');
  Route::post('/employeers/store', 'CorporatesController@store');

  Route::get('/corporate_users_employeer/create/{id}', 'CorporateUsersEmployeerController@create');
  Route::post('/corporate_users_employeer/store', 'CorporateUsersEmployeerController@store');

  Route::get('/training_providers', 'TrainingProvidersController@index');
  Route::get('/training_providers/create', 'TrainingProvidersController@create');
  Route::post('/training_providers/store', 'TrainingProvidersController@store');

  Route::get('/training_providers_trainer/create/{id}', 'TrainingProviderTrainersController@create');
  Route::post('/training_providers_trainer/store', 'TrainingProviderTrainersController@store');

  // Route::get('/training_providers', );

  Route::get('/', 'HomeController@index');
  Route::get('home', 'HomeController@index');
  Route::post('logout', 'Auth\LoginController@logout');

  // USER
  Route::get('users', 'UsersController@index');
  Route::get('users/activated/{id}', 'UsersController@activated');
  Route::get('users/non-activated/{id}', 'UsersController@non_activated');
  Route::get('users/serverside', 'UsersController@serverside');
  Route::get('users/create', 'UsersController@create');
  Route::post('users/create_action', 'UsersController@create_action');
  Route::get('users/password/update/{id}', 'UsersController@password_update');
  Route::post('users/password/update_action/{id}', 'UsersController@password_update_action');
  Route::get('users/update/{id}', 'UsersController@update');
  Route::put('users/update_action', 'UsersController@update_action');
  Route::get('users/delete/{id}', 'UsersController@delete');
  Route::get('users/import', 'UsersController@import');
  Route::post('users/import-action', 'UsersController@import_action');
  Route::get('users/import/all', 'UsersController@importAll');
  Route::post('users/all/import-action', 'UsersController@import_action_all');
  // USER

  // USER instruktor
  Route::get('instructor', 'InstructorController@index');
  Route::get('instructor/serverside', 'InstructorController@serverside');
  Route::get('instructor/create', 'InstructorController@create');
  // Route::get('instructor/detail/{id}', 'InstructorController@show');
  Route::post('instructor/create_action', 'InstructorController@create_action');
  Route::get('instructor/update/{id}', 'InstructorController@update');
  Route::put('instructor/update_action', 'InstructorController@update_action');
  Route::delete('instructor/delete/{id}', 'InstructorController@delete');
  Route::get('instructor/approve/{id}', 'InstructorController@approve');
  Route::get('instructor/block/{id}', 'InstructorController@block');
  // USER instruktor

  // USER manager
  Route::get('manager', 'ManagerController@index');
  Route::get('manager/serverside', 'ManagerController@serverside');
  Route::get('manager/create', 'ManagerController@create');
  Route::post('manager/create_action', 'ManagerController@create_action');
  Route::get('manager/update/{id}', 'ManagerController@update');
  Route::put('manager/update_action', 'ManagerController@update_action');
  Route::delete('manager/delete/{id}', 'ManagerController@delete');
  Route::get('manager/approve/{id}', 'ManagerController@approve');
  Route::get('manager/block/{id}', 'ManagerController@block');
  // USER manager

  // INSTRUCTOR TRANSACTION
  Route::get('instructor/withdrawals', 'InstructorController@withdrawals');
  Route::get('instructor/withdrawals/delete/{id}', 'InstructorController@withdrawal_delete');
  Route::get('instructor/withdrawals/approve/{id}', 'InstructorController@withdrawal_approve');
  Route::get('instructor/withdrawals/detail/{id}', 'InstructorController@withdrawal_detail');
  // INSTRUCTOR TRANSACTION

  // USER LEVEL
  Route::get('user-levels', 'UserLevelsController@index');
  Route::get('user-levels/serverside', 'UserLevelsController@serverside');
  Route::get('user-levels/create', 'UserLevelsController@create');
  Route::post('user-levels/create_action', 'UserLevelsController@create_action');
  Route::get('user-levels/update/{id}', 'UserLevelsController@update');
  Route::put('user-levels/update_action', 'UserLevelsController@update_action');
  Route::delete('user-levels/delete/{id}', 'UserLevelsController@delete');
  // USER LEVEL

  // COURSE
  Route::get('courses', 'CourseController@index');
  Route::get('course/serverside', 'CourseController@serverside');
  Route::get('course/show/{id}', 'CourseController@show');
  Route::get('course/create', 'CourseController@create');
  Route::post('course/create_action', 'CourseController@create_action');
  Route::get('course/manage/{id}', 'CourseController@preview');
  Route::put('course/update_action', 'CourseController@update_action');
  Route::delete('course/delete/{id}', 'CourseController@delete');
  Route::get('course/participant/{course_id}', 'CourseController@participant');
  Route::post('course/import', 'CourseController@import');
  Route::get('course/bulk/{id}', 'CourseController@bulk_enroll');
  Route::post('course/bulk/enroll/{id}', 'CourseController@bulk_enroll_action');
  Route::post('course/content/store-draft', 'ContentController@store_draft');
  Route::get('course/preview/content/update/{id}', 'ContentController@updatePreview');
  Route::get('course/edit/{id}', 'CourseController@edit');
  Route::post('course/edit_action', 'CourseController@edit_action');

  // Route::get('/{id}', 'ContentController@create');
  Route::post('course/content/upload/{section_id}', 'ContentController@upload');
  // Route::post('_action/{id}', 'ContentController@create_action');
  // Route::get('course/content/update/{id}', 'ContentController@update');
  // Route::put('course/content/update_action', 'ContentController@update_action');
  // Route::get('course/content/delete/{content_id}/{id}', 'ContentController@delete');

  // section
  Route::post('course/save_section', 'CourseController@save_section');
  Route::put('course/section/update_action/{id}', 'SectionController@update_action');
  Route::get('course/section/delete/{id_course}/{id}', 'SectionController@delete');
  // section

  // content
  Route::get('course/content/create/{id}', 'ContentController@create');
  Route::post('course/content/upload/{section_id}', 'ContentController@upload');
  Route::post('course/content/create_action/{id}', 'ContentController@create_action');
  Route::get('course/content/update/{id}', 'ContentController@update');
  Route::put('course/content/update_action', 'ContentController@update_action');
  Route::get('course/content/delete/{content_id}/{id}', 'ContentController@delete');
  // content

  //survey
  Route::get('course/survey/create/{id}', 'SurveyManageController@create');
  Route::post('course/survey/create_action/{id}', 'SurveyManageController@create_action');
  Route::get('course/survey/update/{id}', 'SurveyManageController@update');
  Route::put('course/survey/publish/{id}', 'SurveyManageController@publish');
  Route::put('course/survey/update_action', 'SurveyManageController@update_action');
  Route::get('course/survey/delete/{content_id}/{id}', 'SurveyManageController@delete');
  Route::get('course/survey/question/preview/{id}', 'SurveyQuestionController@preview');
  //survey

  // quiz
  Route::get('course/quiz/create/{id}', 'QuizManageController@create');
  Route::post('course/quiz/create_action/{id}', 'QuizManageController@create_action');
  Route::get('course/quiz/update/{id}', 'QuizManageController@update');
  Route::put('course/quiz/publish/{id}', 'QuizManageController@publish');
  Route::put('course/quiz/update_action', 'QuizManageController@update_action');
  Route::get('course/quiz/delete/{content_id}/{id}', 'QuizManageController@delete');
  Route::get('course/quiz/question/preview/{id}', 'QuizQuestionController@preview');
  // quiz

  // quiz Question
  Route::get('course/quiz/question/manage/{id}', 'QuizQuestionController@manage');
  Route::get('course/quiz/question/create/{id}', 'QuizQuestionController@create');
  Route::post('course/quiz/question/create_action/{id}', 'QuizQuestionController@create_action');
  Route::get('course/quiz/question/update/{quiz_id}/{id}', 'QuizQuestionController@update');
  Route::put('course/quiz/question/update_action/{quiz_id}', 'QuizQuestionController@update_action');
  Route::get('course/quiz/question/delete/{quiz_id}/{id}', 'QuizQuestionController@delete');
  // quiz Question

  // quiz Question answer
  Route::post('course/quiz/question/answer/create', 'QuizQuestionAnswerController@create');
  Route::get('course/quiz/question/answer/delete/{quiz_id}/{id}', 'QuizQuestionAnswerController@delete');
  Route::post('course/quiz/question/answer/update_action', 'QuizQuestionAnswerController@update_action');
  // quiz Question answer

  // quiz participant
  Route::get('course/quiz/participant/serverside/{quiz_id}', 'QuizParticipantController@serverside');
  Route::get('course/quiz/participant/create/{quiz_id}', 'QuizParticipantController@create');
  Route::post('course/quiz/participant/create_action/{quiz_id}', 'QuizParticipantController@create_action');
  // quiz participant

  // get library question
  Route::get('course/quiz/get/{quiz_id}/{bank_id}', 'QuizQuestionController@show_quiz_library_course');
  Route::post('course/quiz/get/{quiz_id}/{bank_id}', 'QuizQuestionController@get_question_create');

  // quiz participant answer
  // Route::get('course/quiz/participant/answer/{quiz_participant_id}', 'QuizParticipantAnswerController@index');
  // quiz participant answer

  // COURSE

  // Category
  Route::get('categories', 'CategoryController@index');
  Route::get('categories/serverside', 'CategoryController@serverside');
  Route::get('categories/create', 'CategoryController@create');
  Route::post('categories/create_action', 'CategoryController@create_action');
  Route::get('categories/update/{id}', 'CategoryController@update');
  Route::put('categories/update_action', 'CategoryController@update_action');
  Route::delete('categories/delete/{id}', 'CategoryController@delete');
  // Category

  // COURSE LEVEL
  Route::get('course-level', 'CourseLevelController@index');
  Route::get('course-level/serverside', 'CourseLevelController@serverside');
  Route::get('course-level/create', 'CourseLevelController@create');
  Route::post('course-level/create_action', 'CourseLevelController@create_action');
  Route::get('course-level/update/{id}', 'CourseLevelController@update');
  Route::put('course-level/update_action', 'CourseLevelController@update_action');
  Route::delete('course-level/delete/{id}', 'CourseLevelController@delete');
  // COURSE LEVEL

  // QUIZ TYPE
  Route::get('quiz-type', 'QuizTypeController@index');
  Route::get('quiz-type/serverside', 'QuizTypeController@serverside');
  Route::get('quiz-type/create', 'QuizTypeController@create');
  Route::post('quiz-type/create_action', 'QuizTypeController@create_action');
  Route::get('quiz-type/update/{id}', 'QuizTypeController@update');
  Route::put('quiz-type/update_action', 'QuizTypeController@update_action');
  Route::delete('quiz-type/delete/{id}', 'QuizTypeController@delete');
  // QUIZ TYPE

  // TRANSACTION
  Route::get('transaction', 'TransactionController@index');
  Route::get('transaction/serverside', 'TransactionController@serverside');
  Route::get('transaction/detail/{invoice}', 'TransactionController@detail');
  Route::get('transaction/approve/{invoice}', 'TransactionController@approve');
  Route::delete('transaction/delete/{id}', 'TransactionController@delete');
  Route::get('transaction/confirmation/{invoice}', 'TransactionController@confirmation');
  // TRANSACTION

  // Discussion
  Route::get('discussion', 'DiscussionController@index');
  Route::get('discussion/serverside', 'DiscussionController@serverside');
  Route::get('discussion/publish/{id}', 'DiscussionController@publish');
  Route::get('discussion/block/{id}', 'DiscussionController@block');
  Route::delete('discussion/delete/{id}', 'DiscussionController@delete');
  // Discussion

  // Discussion
  Route::get('discussion-answer', 'DiscussionAnswerController@index');
  Route::get('discussion-answer/serverside', 'DiscussionAnswerController@serverside');
  Route::get('discussion-answer/publish/{id}', 'DiscussionAnswerController@publish');
  Route::get('discussion-answer/block/{id}', 'DiscussionAnswerController@block');
  Route::delete('discussion-answer/delete/{id}', 'DiscussionAnswerController@delete');
  // Discussion

  // Assignment
  Route::get('course/assignment/create/{id}', 'AssignmentController@create');
  Route::post('course/assignment/create_action/{id}', 'AssignmentController@create_action');
  Route::get('course/assignment/update/{id}', 'AssignmentController@update');
  Route::put('course/assignment/publish/{id}', 'AssignmentController@publish');
  Route::put('course/assignment/update_action', 'AssignmentController@update_action');
  Route::get('course/assignment/delete/{content_id}/{id}', 'AssignmentController@delete');
  Route::get('course/assignment/answer/{assignment_id}', 'AssignmentController@answer');
  // Assignment

  // course project
  Route::get('course/project', 'CourseProjectController@index');
  Route::get('course/project/create/{course_id}', 'CourseProjectController@create');
  Route::post('course/project/store/{course_id}', 'CourseProjectController@store');
  Route::post('course/project/upload', 'CourseProjectController@upload');
  Route::get('course/project/show/{id}', 'CourseProjectController@show');
  Route::get('course/project/edit/{id}', 'CourseProjectController@edit');
  Route::post('course/project/update', 'CourseProjectController@update');
  Route::get('course/project/delete/{id}', 'CourseProjectController@destroy');
  // // course project

  // affiliate
  Route::get('affiliate/setting', 'AffiliateController@setting');
  Route::post('affiliate/setting/update', 'AffiliateController@setting_update');
  Route::post('affiliate/setting/create', 'AffiliateController@setting_create');
  Route::get('affiliate/user', 'AffiliateController@user');
  Route::get('affiliate/balance/{id}', 'AffiliateController@balance');
  Route::get('affiliate/transaction/{id}', 'AffiliateController@transaction');
  Route::get('affiliate/withdrawal/{id}', 'AffiliateController@withdrawal');
  Route::get('affiliate/withdrawal/approve/{id}', 'AffiliateController@withdrawal_approve');
  Route::get('affiliate/profile/{id}', 'AffiliateController@profile');
  Route::post('affiliate/profile/update/{id}', 'AffiliateController@profile_update');
  // affiliate

  // promotion
  Route::get('promotion/', 'PromotionController@index');
  Route::get('promotion/create', 'PromotionController@create');
  Route::post('promotion/store', 'PromotionController@store');
  Route::get('promotion/edit/{id}', 'PromotionController@edit');
  Route::post('promotion/update/{id}', 'PromotionController@update');
  Route::get('promotion/show/{id}', 'PromotionController@show');
  Route::get('promotion/delete/{id}', 'PromotionController@destroy');
  // promotion

  // Program
  Route::get('program', 'ProgramController@index');
  Route::get('program/create', 'ProgramController@create');
  Route::post('program/store', 'ProgramController@store');
  Route::get('program/edit/{id}', 'ProgramController@edit');
  Route::put('program/update', 'ProgramController@update');
  Route::get('program/delete/{id}', 'ProgramController@destroy');
  Route::get('program/module/add/{id}', 'ProgramController@module_add');
  Route::post('program/module/store/{id}', 'ProgramController@module_store');
  Route::get('program/module/delete/{id}', 'ProgramController@module_delete');
  // Program

  // Degree
  Route::get('degree', 'DegreeController@index');
  Route::get('degree/create', 'DegreeController@create');
  Route::post('degree/store', 'DegreeController@store');
  Route::get('degree/edit/{id}', 'DegreeController@edit');
  Route::put('degree/update', 'DegreeController@update');
  Route::get('degree/delete/{id}', 'DegreeController@destroy');
  Route::get('degree/module/remove/{id}', 'DegreeController@module_remove');
  Route::get('degree/module/add/{id}', 'DegreeController@module_add');
  Route::post('degree/module/store/{id}', 'DegreeController@module_store');
  // Degree

  // setting
  Route::get('setting/backup-db', 'SettingController@backup_db');
  // setting

  // blast email
  Route::get('blast', 'BlastController@all');
  // blast email

  // Application
  Route::get('app', 'ApplicationController@index');
  Route::get('app/serverside', 'ApplicationController@serverside');
  // Application

  // setting
  Route::get('settings', 'SettingController@index');
  Route::post('settings/action', 'SettingController@action');
  Route::post('settings/action/registration', 'SettingController@registration');
  Route::get('settings/apis', 'SettingController@apis');
  // setting

  // slider
  Route::get('sliders', 'SliderController@index');
  Route::post('sliders/action', 'SliderController@action');
  // slider

  // Route::get('features/', function() {
  //   return view('admin.features.index');
  // });
  // Route::get('features/create', function() {
  //   return view('admin.features.form');
  // });

  Route::get('videos/', 'VideoController@index');
  Route::get('videos/serverside', 'VideoController@serverside');
  Route::get('videos/create', 'VideoController@create');
  Route::post('videos/store', 'VideoController@store');
  Route::get('videos/edit/{id}', 'VideoController@edit');
  Route::post('videos/update', 'VideoController@update');
  Route::get('videos/delete/{id}', 'VideoController@delete');

  Route::get('sliders/', 'SliderController@index');
  Route::get('sliders/serverside', 'SliderController@serverside');
  Route::get('sliders/create', 'SliderController@create');
  Route::post('sliders/store', 'SliderController@store');
  Route::get('sliders/edit/{id}', 'SliderController@edit');
  Route::post('sliders/update', 'SliderController@update');
  Route::get('sliders/delete/{id}', 'SliderController@delete');

  Route::get('promotions/', 'PromotionController@index');
  Route::get('promotions/serverside', 'PromotionController@serverside');
  Route::get('promotions/create', 'PromotionController@create');
  Route::post('promotions/store', 'PromotionController@store');
  Route::get('promotions/edit/{id}', 'PromotionController@edit');
  Route::post('promotions/update', 'PromotionController@update');
  Route::get('promotions/delete/{id}', 'PromotionController@delete');

  Route::get('vcon_setting/delete/{id}', 'VconSettingController@delete');
  Route::get('vcon_setting/{id}', 'VconSettingController@index');
  Route::post('vcon_setting/store', 'VconSettingController@store');
  Route::post('vcon_setting/update', 'VconSettingController@update');
  Route::get('vcon_setting/set_default/{id}', 'VconSettingController@setDefault');
  Route::get('setting_auth_key', 'SettingAuthKeyController@index');
  Route::post('setting_auth_key/update', 'SettingAuthKeyController@update');
  Route::post('setting_auth_key/mail-setting-update', 'SettingAuthKeyController@mail_setting_update');

  Route::resource('blogs', 'BlogsController')->middleware('IsAdmin');
  Route::get('themes/activated/{id}', 'ThemeController@activated')->middleware('IsAdmin');
  Route::resource('themes', 'ThemeController')->middleware('IsAdmin');
  // ROUTE ADMIN


  Route::resource('vcons', 'VconsController');
  Route::get('vcons/recording/{id}', 'VconsController@recording');
  Route::get('vcons/waiting/{id}', 'VconsController@waiting');
  Route::get('vcons/waiting/approve/{id}', 'VconsController@approve');
  Route::resource('blast_notifications', 'BlastNotificationsController');
  Route::get('notifications/{id}', 'BlastNotificationsController@show');

  Route::resource('communities', 'CommunitiesController');
  Route::get('ceritificate', 'CertificateController@index');
  Route::get('ceritificate/lists', 'CertificateController@lists');
  Route::post('certificate/store', 'CertificateController@store');
  Route::post('certificate/update', 'CertificateController@update');
  Route::post('ceritificate/add-courses', 'CertificateController@addCourses');
  Route::post('courses/set-ceritifcated', 'CertificateController@setCeritifcated');

  Route::resource('cohorts', 'CohortsController');
  Route::post('cohorts/add-user/{cohort_id}', 'CohortsController@addUser');

  Route::get('course/atendee/{course_id}', 'CourseController@atendee');
  Route::get('course/user/delete/{course_id}/{user_id}', 'CourseController@deleteCourseUser');
  Route::post('courses/{id}/enroll/users', 'CourseController@addUser');
  Route::post('courses/{id}/enroll/cohort', 'CourseController@addCourseCohort');
  Route::post('courses/set-enrollment/{id}', 'CourseController@setEnrollment');

  // PERMISSIONS
  Route::resource('permissions', 'PermissionsController');
  Route::resource('level-has-permission', 'LevelHasPermissionController');
  // PERMISSIONS

  // BBB TESTS
  Route::get('test/bbb/meetingisrunning/{id}', 'VconsController@test_isrunning');
  Route::post('test/bbb/endmeeting', 'VconsController@test_endmeeting');
  Route::get('test/bbb/meetinginfo/{id}', 'VconsController@test_meetinginfo');
  Route::get('test/bbb/getmeetings/{server_id}', 'VconsController@test_getmeetings');
  Route::get('test/bbb/endmeetings/{server_id}', 'VconsController@test_endmeetings');
  // BBB TESTS

  // SETTINGS
  Route::get('setting/site', 'SettingController@site');
  Route::post('setting/site', 'SettingController@siteSubmit');
  Route::get('setting/auth', 'SettingController@auth');
  Route::post('setting/auth', 'SettingController@auth_post');
  Route::get('setting/vcon', 'SettingController@vcon');
  Route::get('setting/enrolment', 'SettingController@enrollment');
  Route::post('setting/enrollment', 'SettingController@enrollment_post');
  // SETTINGS

  // BANNER
  Route::resource('banner', 'BannerController');
  // BANNER

  // FEATURES
  Route::resource('features', 'FeaturesController');
  // FEATURES

  // LANGUAGES
  Route::resource('languages', 'LanguagesController');
  Route::post('languages/upload/{id}', 'LanguagesController@upload');
  Route::get('languages/download/{id}', 'LanguagesController@download');
  Route::get('languages/default/{id}', 'LanguagesController@setDefault');
  // LANGUAGES

});

// FOR USER LEVEL EMPLOYEER
Route::get('/employeer', 'Employeer\HomeController@index');
Route::get('/employeer/users', 'Employeer\UsersController@index');
Route::get('/employeer/users/create', 'Employeer\UsersController@create');
Route::post('/employeer/users/store', 'Employeer\UsersController@store');
// FOR USER LEVEL EMPLOYEER

Route::get('/training_provider', 'TrainingProvider\HomeController@index');
Route::get('/training_provider/users', 'TrainingProvider\UsersController@index');
Route::get('/training_provider/users/create', 'TrainingProvider\TrainingProviderTrainees@create');
Route::post('/training_provider/users/store', 'TrainingProvider\TrainingProviderTrainees@store');

Route::get('/training_provider/courses', 'TrainingProvider\CoursesController@index');
Route::get('/training_provider/courses/approved', 'TrainingProvider\CoursesController@index_approved');

Route::get('/training_provider/courses/approve/{id}', 'TrainingProvider\CoursesController@approve');

Route::get('/logout_custom', 'Auth\LoginController@logout_custom');

Route::get('tests/upload', function() {
  // $html = "<form method='post' enctype='multipart/form-data'>".csrf_field()."<input type='file' name='file'><input type='submit'></form>";
  // return $html;
  // echo config('filesystems.disks.'.env('APP_STATIC_DISK').'.static_url');
  echo asset('/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
});
Route::post('tests/upload', 'TestController@upload');
Route::post('uploadify/{page}', 'HomeController@uploadify')->name('uploadify');
Route::post('uploadify/{page}/cancel', 'HomeController@uploadify_cancel')->name('uploadify.cancel');

// Route::get('tests/email', function() {
//   $to_name = 'ingeniolms';
//   $to_email = 'adigunawanhidayat29@gmail.com';
//   $data = array('name'=>"Ogbonna", "body" => "A test mail");
//   \Mail::send('emails.test', $data, function($message) use ($to_name, $to_email) {
//   $message->to($to_email, $to_name)
//   ->subject("Bismillah Laravel Test Mail");
//   $message->from('halloingenio@gmail.com','Bismillah Test Mail');
//   });
// });
