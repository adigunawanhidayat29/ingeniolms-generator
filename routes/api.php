<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// API Route with subdomain prefix-=================================================================


Route::domain(env('APP_ENV', 'development') == 'development' ? 'localhost' : 'api.ingenio.co.id')->group(function () {
    Route::middleware(['thirdpartyaccess'])->group(function () {
        // List & Detail Course
        Route::get('course/all', 'Api\App\CourseController@index');
        Route::get('course/detail/{slug}', 'Api\App\CourseController@detail');

        // Authentication
        Route::post('/auth', 'Api\App\LoginController@login');
        Route::post('/token/check', 'Api\App\LoginController@check_token');

        Route::post('/sso/login/check', 'Api\App\LoginController@check_sso');

        // Registration
        Route::post('/user/register', 'Api\App\RegisterController@index');
        Route::post('/user/register/without-activation', 'Api\App\RegisterController@without_activation');

        // Social Media Credentials
        Route::post('/auth/social', 'Api\App\SocialLoginController@index');

        // To access this API, user must authenticated first
        Route::middleware(['authenticated'])->group(function () {
            // Get User Data
            Route::get('user', 'Api\App\UserController@index');

            // My Courses
            Route::get('my-courses', 'Api\App\CourseController@my_course');

            // Learn Course
            Route::get('course/learn/{slug}', 'Api\App\CourseController@learn');
            Route::get('course/learn/content/{slug}', 'Api\App\CourseController@learn_content');

            // Enroll Course
            Route::post('course/user/enroll', 'Api\App\CourseController@enroll');

            //Checking enrolled course
            Route::get('course/user/isenrolled/{course_id}', 'Api\App\CourseController@checkingEnrolled');

            // Enroll Private Course
            Route::post('course/user/join-private-course', 'Api\App\CourseController@joinPrivateCourse');

            // Finish Course
            Route::post('course/completion/finish', 'Api\App\CourseController@completion_finish');

            // Discussion
            Route::post('/course/discussion/ask', 'Api\App\DiscussionController@ask');
            Route::post('/course/discussion/answer', 'Api\App\DiscussionController@answer');
            Route::get('/course/discussion/detail/{id}', 'Api\App\DiscussionController@detail');

            // Transactions
            Route::post('/transaction/process', 'Api\App\TransactionController@process_transaction'); // Process the transaction
            Route::get('/transaction/information/{invoice}', 'Api\App\TransactionController@transaction_information'); // Get the detail of transaction
            Route::post('/transaction/confirm', 'Api\App\TransactionController@confirmation_transaction'); // Confirm the transaction
            Route::get('/transaction/history', 'Api\App\TransactionController@history_transaction'); // Get History Transaction
            Route::post('/transaction/promotion', 'Api\App\TransactionController@promotion'); // Checking promotion code
        });
    });
    //Route::get('/logout', 'Api\App\LoginController@logout');
});


//--=-=-=-=-=-======================================================-==-=-=-=======================---



// Route::get('/get/host', 'Api\ContentController@get_host')->middleware('thirdpartyaccess');
Route::get('/test/hostname', function () {
    return response()->json([
        'message' => parse_url('http://ingenio.co.id')['host'],
    ]);
});
//COURSE
Route::post('course/enroll', 'Api\CourseController@enroll');
Route::get('course/my_course/{id}', 'Api\CourseController@my_course');
Route::get('course/learn/{user_id}', 'Api\CourseController@learn');
Route::post('course/join-private-course', 'Api\CourseController@join_private_class');

//COURSE

//
Route::get('content/information/{contentId}', 'Api\ContentController@index');
Route::get('content/play/{fileId}', 'Api\ContentController@playContent');
//

//TRANSACTION
Route::post('transaction/process', 'Api\TransactionController@process');
Route::post('promotion/get', 'Api\CartController@promotion');
//TRANSACTION

//SOCIAL LOGIN

//SOCIAL LOGIN

Route::get('course/newest/list', 'Api\CourseController@n_course');
Route::get('course', 'Api\CourseController@index');
Route::get('course/category', 'Api\CourseController@category');
Route::get('course/find/{keyword}', 'Api\CourseController@search_course');
Route::get('course/{id}', 'Api\CourseController@show');
Route::get('course/category/{id_cat}', 'Api\CourseController@_category');

// LOGIN & REGISTER
Route::post('login', 'Api\LoginController@index')->middleware('mobileaccess');
Route::post('register', 'Api\RegisterController@index')->middleware('mobileaccess');
Route::post('social', 'Api\LoginController@social_login')->middleware('mobileaccess');
// LOGIN & REGISTER

//PROGRESS
Route::post('progress/reset', 'Api\ProgressController@reset');
Route::post('progress/set', 'Api\ProgressController@set');
Route::get('progress/check/{user_id}/{content_id}', 'Api\ProgressController@checkProgress');
//PROGRESS

//DISCUSSION
Route::post('discussion/ask', 'Api\DiscussionController@ask');
Route::post('discussion/answer', 'Api\DiscussionController@answer');
//DISCUSSION

//ANNOUNCEMENT
Route::post('announcement/comment/add', 'Api\AnnouncementController@comment');
//ANNOUNCEMEMT

//QUIZ
Route::get('quiz/detail/{id}', 'Api\QuizController@detail');
Route::post('quiz/start', 'Api\QuizController@start');
Route::post('quiz/finish', 'Api\QuizController@finish');
Route::post('quiz/result', 'Api\QuizController@result');
Route::post('quiz/download', 'Api\QuizController@download');
Route::post('quiz/sync', 'Api\QuizController@sync_handler');
Route::get('quiz/result/{user_id}', 'Api\QuizController@all_result');

Route::post('quiz/get', 'Api\QuizController@get_quiz');
//QUIZ

//CATEGORIES
Route::get('categories/list', 'Api\CourseController@show_category');

//CATEGORIES

//PROFILE
Route::get('{id}/profile', 'Api\ProfileController@index');
Route::post('profile/update', 'Api\ProfileController@update');
Route::post('profile/update/password', 'Api\ProfileController@update_password');
Route::post('profile/update/avatar', 'Api\ProfileController@update_avatar');
//PROFILE

//ONE SIGNAL
Route::post('notification', 'Api\OneSignalController@sendNotification');
Route::post('player/add', 'Api\OneSignalController@registerPlayer');
Route::post('player/remove', 'Api\OneSignalController@unregisterPlayer');
//ONE SIGNAL

Route::post('transaction/confirmation', 'Api\TransactionController@confirmation');

Route::get('/source/content/get/{content_id}/{course_id}', 'Api\ContentController@get_content');
Route::post('/source/content/update', 'Api\ContentController@update_content');

Route::get('/meeting/schedule/{course_id}', 'Api\CourseController@get_meeting_schedules');

Route::get('/live/newest/list', 'Api\CourseController@n_live_course');
Route::get('/live', 'Api\CourseController@live_course');

Route::get('/get/content/notfinished', 'Api\ContentController@get_all_content');

Route::get('/get/all/courses/{folder_name}', 'Api\ContentController@get_some_content');

Route::post('/instructor/join', 'Api\InstructorController@join');
Route::post('/reset/password', 'Api\RequestPassword@sendResetLinkEmail');

Route::post('upload/file/store-data', 'Api\CourseController@store_lib');

Route::post('library/get', 'Api\LibraryController@library_api');
Route::post('library/delete', 'Api\LibraryController@delete_lib_api');
Route::post('library/move', 'Api\LibraryController@move_lib_api');
Route::post('library/folder/create', 'Api\LibraryController@library_create_folder_api');

Route::get('course/content/{id_course}', 'Api\ContentController@detail');

Route::get('folder/create/{folder_name}', 'Api\LibraryController@folder_user_create');

Route::get('source/library/{lib_id}', 'Api\LibraryController@get_user_lib');

Route::post('upload/assets/image', 'Api\LibraryController@upload_image');

// Route for validation content where the content not compressed or uploaded yet
Route::get('/validation/content/{folder_name}', 'Api\ContentValidationController@index'); // Get all content not uploaded and compressed by folder name

// End of validation route

// V3
Route::group(['prefix' => 'v3', 'namespace' => 'Api\V3'], function () {
    Route::get('home', 'HomeController@index');
    Route::get('menus', 'SettingController@menu');
    Route::get('settings', 'SettingController@index');
    Route::get('settings/{name}', 'SettingController@detail');

    Route::get('courses/{id}', 'CourseController@detail');
    Route::get('courses/{id}/contents/{content_id}', 'CourseController@courseContent');
});
// V3

// API V2
Route::group(['prefix' => 'v2', 'namespace' => 'Api\V2'], function () {

    // home
    Route::get('home', 'HomeController@index');

    // courses
    Route::get('courses', 'CourseController@index');
    Route::get('courses/{id}', 'CourseController@detail');

    // auth
    Route::post('auth/register', 'AuthController@register');
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/facebook', 'AuthController@facebook');
    Route::post('auth/google', 'AuthController@google');

    // must authenticated
    Route::middleware('auth:api')->group(function () {

        // profile
        Route::get('user/profile', 'UserController@index');
        Route::post('user/profile/update', 'UserController@updateProfile');
        Route::post('user/profile/update-password', 'UserController@updatePassword');
        Route::get('user/courses', 'CourseController@my');

        // courses learn
        Route::post('courses/enroll/{id}', 'CourseController@enroll');
        Route::post('courses/enroll-password/{id}', 'CourseController@enroll_password');
        Route::get('courses/learn/{content_id}', 'ContentController@learn');
        Route::get('courses/learn/quiz/{quiz_id}', 'ContentController@quiz_learn');
        Route::post('courses/learn/quiz/{quiz_id}/start', 'ContentController@quiz_start');
        Route::get('courses/learn/quiz/{quiz_id}/questions', 'ContentController@quiz_questions');
        Route::post('courses/learn/quiz/finish', 'ContentController@quiz_finish');
        Route::get('courses/learn/assignment/{assignment_id}', 'ContentController@assignment_learn');
        Route::post('courses/learn/assignment/{assignment_id}/finish', 'ContentController@assignment_finish');
        Route::post('courses/discussion/{id}', 'CourseController@discussion');

        // teacher
        Route::group(['prefix' => 'teacher', 'namespace' => 'Teacher'], function () {
            // courses
            Route::get('courses', 'CourseController@index');
            Route::post('courses/store', 'CourseController@store');
            Route::delete('courses/delete', 'CourseController@destroy');
            Route::post('courses/update/{id}', 'CourseController@update');
            Route::post('courses/publish/{id}', 'CourseController@publish');
            Route::post('courses/unpublish/{id}', 'CourseController@unpublish');
            Route::post('courses/archive/{id}', 'CourseController@archive');
            Route::post('courses/restore/{id}', 'CourseController@restore');
            Route::post('courses/change-password/{id}', 'CourseController@changePassword');
            Route::post('courses/duplicate/{id}', 'CourseController@duplicate');
            Route::post('courses/announcement/{id}', 'CourseController@announcement');
            Route::post('courses/discussion/{id}', 'CourseController@discussion');
            Route::post('courses/atendee/{id}', 'CourseController@atendee');
            Route::post('courses/access-contents/{id}', 'CourseController@accessContents');
            Route::post('courses/{id}/atendee/group', 'CourseController@atendeeGroup');
            Route::post('courses/{id}/atendee/group/users', 'CourseController@atendeeGroupUsers');
            Route::get('courses/{id}/sections', 'ContentController@sectionGet');
            Route::post('courses/{id}/sections', 'ContentController@sectionStore');
            Route::post('courses/{id}/sections/{section_id}', 'ContentController@sectionUpdate');
            Route::delete('courses/{id}/sections/{section_id}', 'ContentController@sectionDestroy');
            Route::post('courses/attendance/{id}', 'CourseController@attendance');
            Route::post('courses/attendance/{id}/store', 'CourseController@attendanceStore');
            Route::post('courses/attendance/{course_id}/update/{id}', 'CourseController@attendanceUpdate');
            Route::post('courses/attendance/{course_id}/delete/{id}', 'CourseController@attendanceDelete');

            // sections & contents
            Route::get('sections/{id}/contents', 'ContentController@contentGet');
            Route::post('sections/{id}/contents', 'ContentController@contentStore');
            Route::post('sections/{id}/contents/{content_id}', 'ContentController@contentUpdate');
            Route::delete('sections/{id}/contents/{content_id}', 'ContentController@contentDestroy');
            Route::get('sections/{id}/assignments', 'ContentController@assignmentGet');
            Route::post('sections/{id}/assignments', 'ContentController@assignmentStore');
            Route::post('sections/{id}/assignments/{content_id}', 'ContentController@assignmentUpdate');
            Route::delete('sections/{id}/assignments/{content_id}', 'ContentController@assignmentDestroy');
            Route::get('sections/{id}/quizzes', 'ContentController@quizGet');
            Route::post('sections/{id}/quizzes', 'ContentController@quizStore');
            Route::post('sections/{id}/quizzes/{content_id}', 'ContentController@quizUpdate');
            Route::delete('sections/{id}/quizzes/{content_id}', 'ContentController@quizDestroy');
        });
    });
});
// API V2