<html>
<title>{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }} - Checkout</title>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style type="text/css">
  /* iframe{
    background: url("{{url('/img/wc-1.jpg')}}") !important;
  } */
</style>

{{-- prod --}}
{{-- <script type="text/javascript"
src="https://app.midtrans.com/snap/snap.js"
data-client-key="Mid-client-emewQEfUcP0dS5cK"></script> --}}

{{-- dev --}}
<script type="text/javascript"
src="https://app.sandbox.midtrans.com/snap/snap.js"
data-client-key="SB-Mid-client-OUIRJa6HOMTd_p8J"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<style media="screen">
.br-loading {
  content: '';
  display: block;
  position: fixed;
  z-index: 1000000000000000;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: table;
  background-color: rgba(255, 255, 255, 0.9);
  background: rgba(255, 255, 255, 0.9); }
  .br-loading .load-content {
    height: 100%;
    width: 100%;
    display: table-cell;
    text-align: center;
    vertical-align: middle; }
    .br-loading .load-content div {
      margin-top: -60px; }
      .br-loading .load-content div p {
        margin-top: 10px; }

      </style>
      <link rel="shotcut icon" href="{!! Setting('favicon')->value != '' ? asset_url(Setting('favicon')->value) : asset('/img/favicon.png') !!}">
    </head>
    <body>

      <div class="br-loading">
        <div class="load-content">
          <div>
            <img src="https://media.giphy.com/media/y1ZBcOGOOtlpC/200.gif" style="height:250px;">
            <p style="font-size: 17px;font-weight: 600;color: black;margin-top: -20px;">
              You are directed to the payment page
            </p>
          </div>
        </div>
      </div>


      <form id="payment-form" method="post" action="{{url('/pembayaran/finish')}}">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" name="result_type" id="result-type" value=""></div>
        <input type="hidden" name="result_data" id="result-data" value=""></div>
      </form>

      <script type="text/javascript">
      $.ajax({
        url: '{{url('/snaptoken')}}',
        cache: false,
        success: function(data) {
          //location = data;
          //console.log('token = '+data);

          $(".br-loading").hide();

          var resultType = document.getElementById('result-type');
          var resultData = document.getElementById('result-data');

          function changeResult(type,data){
            $("#result-type").val(type);
            $("#result-data").val(JSON.stringify(data));
            //resultType.innerHTML = type;
            //resultData.innerHTML = JSON.stringify(data);
          }

          snap.pay(data, {
            onSuccess: function(result){
              $(".br-loading").hide();
              changeResult('success', result);
              console.log(result.status_message);
              console.log(result);
              $("#payment-form").submit();
            },
            onPending: function(result){
              changeResult('pending', result);
              console.log(result.status_message);
              $("#payment-form").submit();
            },
            onError: function(result){
              changeResult('error', result);
              console.log(result.status_message);
              $("#payment-form").submit();
            },
            onClose: function(result){
              window.location = "{{url('/cart/checkout/v2')}}";
            }
          });
        }
      });

    </script>


  </body>
  </html>
