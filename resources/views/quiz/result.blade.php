@extends('quiz.template')

@section('quiz_title', $quiz->name)

@push('style')
  <style>
    .card-quiz {
      border-radius: 0 0 5px 5px;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }

    .card-quiz .card-header {
      padding: 15px;
      background: #f6f6f6;
      border-bottom: 1px solid #ececec;
      margin-bottom: 0;
    }

    .card-quiz .card-header .card-title {
      margin: 0;
      font-size: 16px;
      font-weight: 500;
      color: #333;
      display: contents;
    }

    .card-quiz .card-header .card-title.question {
      display: flex;
      align-items: flex-start;
    }

    .card-quiz .card-header .card-title.question .question-number {
      width: 40px;
      height: 40px;
      background: #222;
      font-size: 16px;
      font-weight: 400;
      color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 7.5px;
    }

    .card-quiz .card-header .card-title.question p {
      font-size: 16px;
    }

    .card-quiz .card-block {
      padding: 15px;
      margin-bottom: -1rem;
    }

    .card-quiz .card-block p {
      font-size: 16px;
    }

    .card-quiz .card-block .form-group {
      padding-bottom: 0;
      margin: 15px 0 0;
    }

    .card-quiz .card-block table p {
      margin-bottom: 0;
    }

    .quiz-panel--desktop {
      display: block;
    }

    .quiz-panel--mobile {
      display: none;
    }

    @media (max-width: 420px) {
      .card-quiz .card-header .card-title.question .question-number {
        width: 30px;
        height: 30px;
        font-size: 12px;
        border-radius: 5px;
      }

      .card-quiz .card-header .card-title.question p {
        font-size: 14px;
      }

      .card-quiz .card-block * {
          font-size: 14px;
      }

      #question_content {
        margin-bottom: 150px;
      }

      .quiz-panel--desktop {
        display: none;
      }

      .quiz-panel--mobile {
        position: fixed;
        background: #f9f9f9;
        width: 100%;
        display: flex;
        justify-content: space-between;
        border-top: 3px solid #eee;
        padding: 1rem;
        left: 0;
        bottom: 0;
      }

      .quiz-panel--mobile .panel-button-group {
        position: absolute;
        display: flex;
        justify-content: center;
        width: 100%;
        left: 0;
        top: -1.15rem;
      }

      .quiz-panel--mobile .panel-button-group .panel-button {
        background: #4ca3d9;
        width: 30px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        color: #fff;
        border-radius: 50%;
        margin-left: auto;
        margin-right: auto;
      }

      .quiz-panel--mobile .panel-time-group .time-title,
      .quiz-panel--mobile .panel-score-group .score-title {
        font-size: 12px;
        font-weight: 700;
        color: #ccc;
        letter-spacing: 1px;
        text-transform: uppercase;
        margin: 0;
      }

      .quiz-panel--mobile .panel-time-group .time-content,
      .quiz-panel--mobile .panel-score-group .score-content {
        font-size: 16px;
        font-weight: 500;
        margin: 0;
      }

      .quiz-panel--mobile .panel-score-group .score-content {
        font-size: 30px;
        line-height: 1;
      }

      .quiz-panel--mobile .btn {
        margin: 0;
      }
    }

    .feedback{
      border-top: 1px solid black;
      margin-top: 15px;
    }

    .specificfeedback{
      display: inline;
      padding: 0 .7em;
      background: #fff3bf;
    }
  </style>
@endpush

@section('quiz_content')
  <div class="container">
    <div class="row player-content-group">
      <div class="col-md-{{$quiz->quizz_type != 'survey' ? '8':'12'}}">
        <div class="">
          <div class="card-header">
            <h3 class="card-title">Hasil Jawaban {{$quiz->name}}</h3>
          </div>
          <div class="card-block nopadding">
            <div role="main">
              <span id="maincontent"></span>
              <div class="card-quiz" style="padding: 20px;">
                <table class="generaltable generalbox quizreviewsummary" style="width:100%">
                  <tbody>
                    <tr>
                      <th class="cell" scope="row">Started on</th>
                      <td class="cell">{{Date('l, d F Y h:i A', strtotime($quiz_participant_data->time_start))}}</td>
                    </tr>
                    <tr>
                      <th class="cell" scope="row">State</th>
                      <td class="cell">Finished</td>
                    </tr>
                    <tr>
                      <th class="cell" scope="row">Completed on</th>
                      <td class="cell">{{Date('l, d F Y h:i A', strtotime($quiz_participant_data->time_end))}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              @php $i =1; @endphp
              @foreach($quiz_participants as $index => $quiz_participant)
                @if($quiz_participant->quiz_type_id != '4')
                <div class="card-quiz" style="padding: 20px;">
                  <div class="que multichoice deferredfeedback">
                    <div class="info">
                      <h3 class="no">Question <span class="qno">{{$i}}</span></h3>
                    </div>
                    <div class="content">
                      <div class="formulation clearfix">
                        <div class="qtext" style="display:flex">
                          <i class="fa fa-question-circle" aria-hidden="true" style="    margin: 9px;margin-left: 0;"></i>
                          <div>
                            {!!$quiz_participant->question!!}
                          </div>
                        </div>
                        <div class="">
                          @if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '9')
                            @php
                              $question_answers = DB::table('quiz_question_answers')
                                ->whereIn('id', explode(',', $quiz_participant->quiz_question_answer_id))
                                ->orderByRaw(DB::raw("FIELD(id, $quiz_participant->quiz_question_answer_id)"))
                                ->get();

                              $question_origin = App\QuizQuestion::find($quiz_participant->quiz_question_id);
                              $char = 'A'; $char <= 'Z';
                            @endphp
                              <div class="answer">
                                @foreach($question_origin->quiz_question_answers as $question_answer)
                                <div class="r0">
                                  <input type="radio" disabled="disabled" {{$quiz_participant->quiz_question_answer_id == $question_answer->id ? 'checked' : ''}}>
                                  <label style="color: #8b8b8b;"><span>{{$char++}}. </span>{{$question_answer->answer}}</label>
                                  @if($quiz_participant->quiz_question_answer_id == $question_answer->id)
                                    @if($question_answer->answer_correct == 1)
                                    <i class="icon fa fa-check text-success fa-fw " title="Correct" aria-label="Correct"></i>
                                    @else
                                    <i class="icon fa fa-remove text-danger fa-fw " title="Incorrect" aria-label="Incorrect"></i>
                                    @endif
                                    <span class="specificfeedback">jawaban a</span>
                                  @endif
                                </div>
                                @endforeach
                              </div>
                          @elseif($quiz_participant->quiz_type_id == '3' || $quiz_participant->quiz_type_id == '9' || $quiz_participant->quiz_type_id == '12')
                            @php
                              $question_answers = DB::table('quiz_question_answers')
                                ->whereIn('id', explode(',', $quiz_participant->quiz_question_answer_id))
                                ->orderByRaw(DB::raw("FIELD(id, $quiz_participant->quiz_question_answer_id)"))
                                ->get();
                            @endphp
                            @foreach($question_answers as $index => $question_answer)
                              {{$question_answer->answer}} <br>
                            @endforeach
                          @elseif(in_array($quiz_participant->quiz_type_id, ['5', '10', '11']))
                            {!!$quiz_participant->answer_essay!!}
                          @elseif($quiz_participant->quiz_type_id == '13' )
                            @php
                              $file = explode(',', $quiz_participant->answer_essay);
                            @endphp
                            <a href="{{asset('files/learn/survey/'.$file[0])}}" download="{{$file[1]}}"><button class="btn btn-primary">Download File</button></a>
                          @elseif($quiz_participant->quiz_type_id == '8' )
                            @php
                              $question_answers_multi = DB::table('quiz_question_answers')
                                ->whereIn('id', explode(',', $quiz_participant->answer_essay))
                                ->get();
                            @endphp
                            @foreach($question_answers_multi as $index => $question_answer)
                              {{$question_answer->answer}} <br>
                            @endforeach
                          @elseif($quiz_participant->quiz_type_id == '14')
                          <img src="{{$quiz_participant->answer_essay}}" style="width:209px;border:1px solid #bbb5b5">
                          @elseif($quiz_participant->quiz_type_id == '6')
                            {!!$quiz_participant->answer_short_answer!!}
                          @endif
                        </div>
                        <br>
                        <div class="outcome clearfix">
                          @if($quiz_participant->feedback_required == 1)
                          <h4 class="accesshide">Feedback</h4>
                          <div class="feedback">
                            <p class="specificfeedback">{{$quiz_participant->feedback_question}}</p>
                          </div>
                          @endif
                          <!-- <div class="feedback">
                            <div class="specificfeedback">Your answer is correct.</div>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @php $i++ @endphp
                @endif
              @endforeach
            </div>
          </div>
        </div>
      </div>

      @if($quiz->quizz_type != 'survey')
      <div class="col-md-4 quiz-panel--desktop">
        <div class="card-quiz">
          <div class="card-header">
            <h3 class="card-title">Hasil Pekerjaan</h3>
          </div>
          <div class="card-block text-center">
            {{-- <p>total question : {{$count_question}}</p>
            <p>answer correct : {{$count_answer_correct}}</p>
            <p>answer incorrect : {{$count_answer_incorrect}}</p>
            <hr> --}}
            {{-- <p>Nilai Kamu <span class="badge">{{$score}}</span></p> --}}
            <p class="mb-0">Hasil yang anda peroleh</p>
            <div style="font-size: 50px; color: #4ca3d9; margin-bottom: 1rem;">
              {{number_format($score,2)}}
            </div>
          </div>
        </div>
        <a href="#" type="button" class="btn btn-primary btn-block btn-raised nextContent">Selanjutnya</a>
      </div>

      <div class="quiz-panel--mobile">
        <div class="panel-score-group">
          <p class="score-title">Nilai anda</p>
          <p class="score-content">{{$score}}</p>
        </div>
        <a href="#" class="btn btn-sm btn-raised btn-primary nextContent">Selanjutnya</a>
      </div>
      @endif
    </div>
  </div>
@endsection

@push('quiz_script')
  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
