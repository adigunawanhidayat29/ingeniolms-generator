@extends('quiz.template')

@section('quiz_title', $quiz->name)

@push('style')
  <style>
    .card-quiz {
        border-radius: 0 0 5px 5px;
        background: #f9f9f9;
        transition: all 0.3s;
        border: 1px solid #f5f5f5;
        margin-bottom: 2rem;
        margin-bottom: 20px;
    }

    .card-quiz .card-header {
        padding: 15px;
        background: #f6f6f6;
        border-bottom: 1px solid #ececec;
        margin-bottom: 0;
    }

    .card-quiz .card-header .card-title {
        margin: 0;
        font-size: 16px;
        font-weight: 500;
        color: #333;
        display: contents;
    }

    .card-quiz .card-block {
        padding: 15px;
        margin-bottom: -1rem;
    }

    .card-quiz .card-block p {
        font-size: 16px;
    }

    .card-quiz .card-block .form-group {
        padding-bottom: 0;
        margin: 15px 0 0;
    }

    @media (max-width: 420px) {
        .card-quiz .card-block p {
            font-size: 14px;
        }
    }
  </style>
@endpush

@section('quiz_content')
  <div class="container">
    <div class="row player-content-group">
      <div class="col-md-12">
        <h2 class="headline headline-md" style="margin-bottom: 0.5rem;"><span>{{$quiz->name}}</span></h2>
        @if($quiz->attempt < 0 || count($quiz_participant) < $quiz->attempt)
          @if($quiz->quizz_type == 'quiz')
            <a class="btn btn-primary btn-raised" href="{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/start')}}" style="margin-top: 0rem; margin-bottom: 1.5rem;">@lang('front.page_manage_preview_quiz.start_quiz')</a>
          @else
            <a class="btn btn-primary btn-raised" href="{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/start')}}" style="margin-top: 0rem; margin-bottom: 1.5rem;">@lang('front.page_manage_preview_quiz.start_survey')</a>
          @endif
        @else
          @php
            $participant = $quiz_participant->last();
          @endphp
          @if($participant->finish == 1)
          <p>@lang('front.page_manage_preview_quiz.attempts_out')</p>
          @else
            @if($quiz->quizz_type == 'quiz')
            <a class="btn btn-primary btn-raised" href="{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/start')}}" style="margin-top: 0rem; margin-bottom: 1.5rem;">@lang('front.page_manage_preview_quiz.start_quiz')</a>
            @else
            <a class="btn btn-primary btn-raised" href="{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/start')}}" style="margin-top: 0rem; margin-bottom: 1.5rem;">@lang('front.page_manage_preview_quiz.start_survey')</a>
            @endif
          @endif
        @endif
        {{-- @if(date("Y-m-d H:i:s") >= $quiz->time_start &&  date("Y-m-d H:i:s") <= $quiz->time_end)
          <a class="btn btn-primary btn-raised" href="{{url('course/learn/'.\Request::segment(3).'/quiz/'.$quiz->id.'/start')}}">{{$quiz->quizz_type == 'quiz' ? 'Mulai Quiz' : 'Mulai Survey'}}</a>
        @else
          Quiz belum siap.
        @endif --}}
        <div class="card-quiz">
          <div class="card-header">
            <h3 class="card-title">@lang('front.page_manage_preview_quiz.description')</h3>
          </div>
          <div class="card-block">
            {!! $quiz->description !!}
          </div>
        </div>
        @if(count($quiz_participant) > 0)
        <div class="result">
          <h3>@lang('front.page_manage_preview_quiz.summary_attempts')</h3>
          <table class="table">
            <thead>
              <tr>
                <th class="header c0" style="text-align:center;" scope="col">No</th>
                <th class="header c1" style="text-align:left;" scope="col">State</th>
                <th class="header c3" style="text-align:center;" scope="col">Grade / 10.00</th>
                <th class="header c4 lastcol" style="text-align:center;" scope="col">Review</th>
              </tr>
            </thead>
            <tbody>
              @foreach($quiz_participant as $index => $item)
                <tr class="">
                  <td class="cell c0" style="text-align:center;">{{$index+1}}</td>
                  <td class="cell c1" style="text-align:left;">{{$item->finish == 0 ? 'UnFinish' : 'Finish'}}<br><span class="statedetails">Submitted {{date('F, d M Y, H:s', strtotime($item->submitted_date))}}</span></td>
                  <td class="cell c3" style="text-align:center;">{{number_format((float)$item->grade, 2, '.', '')}}</td>
                  <td class="cell c4 lastcol" style="text-align:center;">
                    @if($item->finish == 1)
                    <a title="Review your responses to this attempt" href="/course/learn/quiz/kelas-admin/28/result/{{$item->id}}"><button class="btn btn-info btn-raised"><i class="fa fa-search"></i> @lang('front.page_manage_preview_quiz.review')</button></a>
                    @else
                    <a class="btn btn-primary btn-raised" href="{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/start')}}" style="margin-top: 0rem; margin-bottom: 1.5rem;">@lang('front.page_manage_preview_quiz.continue_quiz')</a>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @endif
      </div>
      <div class="col-md-4">

      </div>
    </div>
  </div>

@endsection
