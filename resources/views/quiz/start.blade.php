@extends('quiz.template')

@section('quiz_title', $quiz->name)

@push('quiz_style')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
    .goto-link p{
      color: #333;
      margin-left: 0.5rem;
      margin-bottom: 0;
      overflow: hidden;
      min-height: 20px;
      display: -webkit-inline-box !important;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 1;
      white-space: normal;
      line-height: 1.5rem;
      width: 90%;
    }
    .modal {
      z-index: 1200;
    }
    .modal .modal-dialog .modal-content .modal-body {
      padding: 1rem;
      margin-bottom: -1rem;
    }
  </style>

  <style>
    .card-quiz {
      border-radius: 0 0 5px 5px;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }

    .card-quiz .card-header {
      padding: 15px;
      background: #f6f6f6;
      border-bottom: 1px solid #ececec;
      margin-bottom: 0;
    }

    .card-quiz .card-header .card-title {
      margin: 0;
      font-size: 16px;
      font-weight: 500;
      color: #333;
      display: contents;
    }

    .card-quiz .card-header .card-title.question-group {
      display: flex;
      align-items: flex-start;
    }

    .card-quiz .card-header .card-title.question-group .question-number {
      width: 40px;
      height: 40px;
      background: #222;
      font-size: 16px;
      font-weight: 400;
      color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 7.5px;
    }

    .card-quiz .card-header .card-title.question-group .question {
      width: calc(100% - 40px);
      margin-left: 0.5rem;
    }

    .card-quiz .card-header .card-title.question-group .question * {
      font-size: 16px;
      margin-bottom: 0;
    }

    .card-quiz .card-block {
      padding: 15px;
      margin-bottom: -1rem;
    }

    .card-quiz .card-block p {
      font-size: 16px;
    }

    .card-quiz .card-block .form-group {
      padding-bottom: 0;
      margin: 15px 0 0;
    }

    .card-quiz .card-block table p {
      margin-bottom: 0;
    }

    .quiz-panel--desktop {
      display: block;
    }

    .quiz-panel--mobile {
      display: none;
    }

    @media (max-width: 420px) {
      .card-quiz .card-header .card-title.question-group .question-number {
        width: 30px;
        height: 30px;
        font-size: 12px;
        border-radius: 5px;
      }

      .card-quiz .card-header .card-title.question-group .question * {
        font-size: 14px;
      }

      .card-quiz .card-block * {
          font-size: 14px;
      }

      .quiz-panel--desktop {
        display: none;
      }

      .quiz-panel--mobile {
        position: fixed;
        background: #f9f9f9;
        width: 100%;
        display: flex;
        justify-content: space-between;
        border-top: 3px solid #eee;
        padding: 1rem;
        left: 0;
        bottom: 0;
      }

      .quiz-panel--mobile .panel-button-group {
        position: absolute;
        display: flex;
        justify-content: center;
        width: 100%;
        left: 0;
        top: -1.15rem;
      }

      .quiz-panel--mobile .panel-button-group .panel-button {
        background: #4ca3d9;
        width: 30px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        color: #fff;
        border-radius: 50%;
        margin-left: auto;
        margin-right: auto;
      }

      .quiz-panel--mobile .panel-time-group .time-title,
      .quiz-panel--mobile .panel-score-group .score-title {
        font-size: 12px;
        font-weight: 700;
        color: #ccc;
        letter-spacing: 1px;
        text-transform: uppercase;
        margin: 0;
      }

      .quiz-panel--mobile .panel-time-group .time-content,
      .quiz-panel--mobile .panel-score-group .score-content {
        font-size: 16px;
        font-weight: 500;
        margin: 0;
      }

      .quiz-panel--mobile .panel-score-group .score-content {
        font-size: 30px;
        line-height: 1;
      }

      .quiz-panel--mobile .btn {
        margin: 0;
      }
    }

    .signature{
      width: 100%;
      height: auto;
      border: 1px solid black;
    }
  </style>
@endpush

@section('quiz_content')
  <div class="container">
    <div class="row player-content-group">
      <div class="col-md-8">
        <!--Pagination Wrap Start-->
        {{-- <div class="text-center">
          {{$quiz_page_breaks->links('pagination.default')}}
        </div> --}}
        <!--Pagination Wrap End-->
        <div id="start_quiz">
          @if($quiz->paginate_type == 1)
            @include('quiz.start_quiz_question')
          @else
            @include('quiz.start_quiz_page_break')
          @endif
        </div>

        <!--Pagination Wrap Start-->
        <div class="text-center">
          {{$quiz_page_breaks->links('pagination.default')}}
        </div>
        <!--Pagination Wrap End-->
      </div>

      <div class="col-md-4 quiz-panel--desktop">
        <div class="card-quiz">
          <div class="card-header">
            {{-- <h3 class="card-title">Waktu Anda</h3> --}}
            <h3 class="card-title">Waktu Tersisa</h3>
          </div>
          <div class="card-block text-center">
            <div class="timer mb-3"></div>
          </div>
        </div>
        <div class="card-quiz">
          <div class="card-header">
            {{-- <h3 class="card-title">Go to Question</h3> --}}
            <h3 class="card-title">Pilih Soal</h3>
          </div>
          <div>
            <div class="list-group">
              <!--Pagination Wrap Start-->
              <div class="text-center">
                {{$quiz_page_breaks->links('pagination.default')}}
              </div>
              <!--Pagination Wrap End-->
            </div>
          </div>
        </div>
        {{-- <button id="finish_quiz" type="button" class="btn btn-primary btn-block btn-raised">Finish Quiz</button> --}}
        @if($userId != $authorId)
          <button id="finish_quiz" type="button" class="btn btn-primary btn-block btn-raised">Selesai</button>
        @else
          <button type="button" class="btn btn-danger btn-block btn-raised" disabled>Anda dalam moda pengajar</button>
        @endif

      </div>

      <div class="quiz-panel--mobile">
        <div class="panel-button-group">
          <a class="panel-button" href="#" data-toggle="modal" data-target="#quizPanel">
            <i class="fa fa-angle-up"></i>
          </a>
        </div>
        <div class="panel-time-group">
          <p class="time-title">Waktu tersisa</p>
          <p class="time-content timer">23 : 12 : 16</p>
        </div>
        <button id="finish_quiz--mobile" type="button" class="btn btn-sm btn-raised btn-primary">Selesai</button>
      </div>
    </div>
  </div>

  {{-- MODAL --}}
  <div class="modal" id="quizPanel" tabindex="-1" role="dialog" aria-labelledby="quizPanelLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="card-quiz">
            <div class="card-header">
              <h3 class="card-title">Pilih Soal</h3>
            </div>
            <div>
              <div class="list-group">
                <div class="text-center">
                  {{$quiz_page_breaks->links('pagination.default')}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('quiz_script')
  {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
  <script src="{{asset('js/jquery.paginate.min.js')}}"></script> --}}
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/jSignature/libs/jSignature.min.js')}}"></script>
  <script src="{{asset('plugins/jSignature/libs/modernizr.js')}}"></script>
  <script type="text/javascript" src="{{asset('plugins/jSignature/libs/flashcanvas.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  <!-- signature -->
  <script>
  $(document).on('click', '.page-link', function(event){
     event.preventDefault();
     var page = $(this).attr('href').split('page=')[1];
     var key1 = $(this).parents('li').index();
     $('.page-item').removeClass('active');
     $.each($('ul.pagination'), function(key, value){
       $.each($(this).find('li'), function(key2, value){
         if(key1 == key2){
           $(this).addClass('active');
         }
       })
     })

     fetch_data(page);
  });

  function fetch_data(page)
  {
    var data = allStorageItem();
    console.log(data)
    var _token = $("input[name=_token]").val();
    $.ajax({
        url:"{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/ajax')}}",
        method:"POST",
        data:{
          _token:_token,
          page:page,
          answer: data,
          quiz_participant_id : '{{$patricipant->id}}'
        },
        success:function(data)
        {
          console.log(data)
         $('#start_quiz').html(data);
        }
      });
  }

  function allStorageItem() {
    var answers = [];
    var questions = [];
    var question_type = [];

    @foreach($all_quiz_page_breaks as $quiz_page_break)
      @foreach($quiz_page_break->quiz_questions as $quiz_question)
        @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7' || $quiz_question->quiz_type_id == '9'|| $quiz_question->quiz_type_id == '12')
          var answer_checked = $('input[name=answer{{$quiz_question->id}}]:checked').val();
          var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          // check if answer is null
          if(answer_checked == null){
            answer_checked = 0;
          }else{
            answer_checked = answer_checked;
          }
          // check if answer is null
        @elseif($quiz_question->quiz_type_id == '3')
          var answer_checked = $('input[name=answer_ordering{{$quiz_question->id}}]').val();
          var session_value = get_session_answer('{{'questionOrdering'.$quiz_question->id}}');
        @elseif($quiz_question->quiz_type_id == '5')
          var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
          var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
        @elseif($quiz_question->quiz_type_id == '6' || $quiz_question->quiz_type_id == '10' || $quiz_question->quiz_type_id == '11')
          var answer_checked = $('input[name=answer{{$quiz_question->id}}]').val();
          var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
        @elseif($quiz_question->quiz_type_id == '13')
          var session_data = $('input[name=answer{{$quiz_question->id}}]');
          var formData = new FormData();
              formData.append('image', session_data[0].files[0]);
          var return_first = function () {
              var tmp = null;
              $.ajax({
                  url: "{{url('course/learn/survey/upload')}}",
                  type : "POST",
                  async: false,
                  data: formData,
                  processData: false,
                  contentType: false,
                  success : function(result){
                    tmp = result;
                  },
              });
              return tmp;
          }();
          var session_value = return_first;
        @elseif($quiz_question->quiz_type_id == '14')
        var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
        var session_value = answer_checked;
        @elseif($quiz_question->quiz_type_id == '8')
          var answer_checked = $('input[question-checkbox-type=checkbox{{$quiz_question->id}}]:checked');
          // $.each(answer_checked, function(key, value){
            // console.log(value);
          // });
          var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
        @endif

        // answers.push(answer_checked);
        answers.push(session_value);
        questions.push('{{$quiz_question->id}}');
        question_type.push('{{$quiz_question->quiz_type_id}}');
      @endforeach
    @endforeach

    return [answers, questions, question_type];
}
  // Initialize jSignature
  var $sigdiv = $(".signature").jSignature({'UndoButton':true});

  $(".signature").change(function(){
    // Get response of type image
    var data = $sigdiv.jSignature('getData', 'image');

    // Storing in textarea
    $(this).siblings('.signature-output').val("data:"+data);
    save_session_answer($(this).siblings('.signature-output')[0]);
  });

  $(".signature input").click(function(){
    // Get response of type image

    $(this).parents('.signature').siblings('.signature-output').val(null);
    save_session_answer($(this).parents('.signature').siblings('.signature-output')[0]);
  });

  </script>

  {{-- finish quiz --}}
  <script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    function allStorage() {

      var values = [],
          keys = Object.keys(localStorage),
          i = keys.length;

      while ( i-- ) {
          values.push( localStorage.getItem(keys[i]) );
      }

      return values;
    }

    $(".file-survey").change(function(){
      // console.log();
      $(this).siblings().val($(this).val().split('\\').pop());
    })

    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($all_quiz_page_breaks as $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions as $quiz_question)
          @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7' || $quiz_question->quiz_type_id == '9'|| $quiz_question->quiz_type_id == '12')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]:checked').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
            // check if answer is null
            if(answer_checked == null){
              answer_checked = 0;
            }else{
              answer_checked = answer_checked;
            }
            // check if answer is null
          @elseif($quiz_question->quiz_type_id == '3')
            var answer_checked = $('input[name=answer_ordering{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'questionOrdering'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '5')
            var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '6' || $quiz_question->quiz_type_id == '10' || $quiz_question->quiz_type_id == '11')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '13')
            var session_data = $('input[name=answer{{$quiz_question->id}}]');
            var formData = new FormData();
                formData.append('image', session_data[0].files[0]);
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    url: "{{url('course/learn/survey/upload')}}",
                    type : "POST",
                    async: false,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function(result){
                      tmp = result;
                    },
                });
                return tmp;
            }();
            var session_value = return_first;
          @elseif($quiz_question->quiz_type_id == '14')
          var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
          var session_value = answer_checked;
          @elseif($quiz_question->quiz_type_id == '8')
            var answer_checked = $('input[question-checkbox-type=checkbox{{$quiz_question->id}}]:checked');
            // $.each(answer_checked, function(key, value){
              // console.log(value);
            // });
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @endif

          // answers.push(answer_checked);
          answers.push(session_value);
          questions.push('{{$quiz_question->id}}');
          question_type.push('{{$quiz_question->quiz_type_id}}');
        @endforeach
      @endforeach

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/finish')}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          participant_id : '{{$patricipant->id}}',
          _token: token
        },
        success : function(result){
          //remove session answer
          if(result.status){
            @foreach($all_quiz_page_breaks as $quiz_page_break)
              @foreach($quiz_page_break->quiz_questions as $quiz_question)
                localStorage.removeItem('answer{{$quiz_question->id}}');
                localStorage.removeItem('questionOrdering{{$quiz_question->id}}');
              @endforeach
            @endforeach
            window.location.assign('{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/result')}}/'+result.id);
          }else {
            window.location.assign('{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/result')}}/'+result.id);
          }

          //remove session answer
          // window.location.assign('{{url('course/learn/quiz/'.\Request::segment(4).'/'.$quiz->id.'/result')}}/'+result);
        },
      });
    }

    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })

    $("#finish_quiz--mobile").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
  @foreach($all_quiz_page_breaks as $quiz_page_break)
    @foreach($quiz_page_break->quiz_questions as $quiz_question)
      var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');

      @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7'|| $quiz_question->quiz_type_id == '9')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}][value='"+session_value+"']").attr("checked","checked");
        }
      @elseif($quiz_question->quiz_type_id == '3')
        if(session_value !== null){
          $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '8')
        if(session_value !== null){
          var newData = session_value.split(",");
          $.each(newData, function(key, value){
            $("input[name=answer{{$quiz_question->id}}][value='"+value+"']").attr("checked","checked");
          });
          // $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '12')
        if(session_value !== null){
            $("select[name=answer{{$quiz_question->id}}] option[value='"+session_value+"']").prop('selected', true);
          // $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '5')
        if(session_value !== null){
          $("textarea[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @elseif($quiz_question->quiz_type_id == '6' || $quiz_question->quiz_type_id == '10' || $quiz_question->quiz_type_id == '11')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @endif
    @endforeach
  @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.

      if($(e).attr("question-checkbox-type")){
        if (localStorage.getItem(e.name) != null) {
          var newVal = localStorage.getItem(e.name);
          if(!Array.isArray(newVal)){
            newVal = [newVal];
          }
          newVal.push(val);
          val = newVal;
        }
      }

      // console.log(id, val);
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      // console.log(v, localStorage.getItem(v));
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}

  {{-- timer --}}
  <script>
  @if($quiz->duration_required == 1)
    // Set the date we're counting down to
    var countDownDate = new Date("{{date("Y-m-d H:i:s", strtotime($quiz_time_start) + $quiz->duration*60)}}").getTime();
    // Update the count down every 1 second
    var x = setInterval(function() {
        // Get todays date and time
        var now = new Date();
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Output the result in an element with id="demo"
        $(".timer").html(hours + " Jam, " + minutes + " Menit, " + seconds + " Detik Lagi")
        console.log((hours + " Jam, " + minutes + " Menit, " + seconds + " Detik Lagi"))
        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            $(".timer").html("Waktu Habis");
            quiz_finish();
        }
    }, 1000);
    @else
      $(".timer").html("Tidak ada batas waktu");
    @endif
  </script>
  {{-- timer --}}

  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script src="/jquery-ui/jquery.ui.touch-punch.min.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
