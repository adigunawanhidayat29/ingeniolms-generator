<div id="question_content">
  @php $i =1; @endphp
  @foreach($quiz_page_breaks as $quiz_question)
    @php
      $answer = $quiz_question->participants_answers($patricipant->id);
    @endphp
    <div class="card-quiz" id="question_{{$quiz_question->id}}">
      <div class="card-header">
        {{-- <div class="card-title d-flex"><span class="badge">{{$i++}}</span> {!!$quiz_question->question!!}</div> --}}
        <div class="card-title question-group">
          {{-- jika tipe label -> nomor tidak di nyalakan --}}
          @if($quiz_question->quiz_type_id != '4')
          <div class="question-number">{{$i++}}</div>
          @endif
          <div class="question">{!!$quiz_question->question!!}</div>
        </div>
      </div>
      <div class="card-block">

        {{-- jika tipe label -> nomor tidak di nyalakan --}}
        @if($quiz_question->quiz_type_id != '4')
        <label style="margin-bottom: 1rem;">Jawab pertanyaan dengan benar</label>
        @endif

        @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '9')
          @php
            $char = 'A'; $char <= 'Z';
            $answer_cheked = null;
            if($answer){
              $answer_cheked = $answer->quiz_question_answer_id;
            }
          @endphp
          @foreach($quiz_question->quiz_question_answers as $question_answer)
            <div>
              <label style="color:#19191a; margin-bottom: 1rem;">
                <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}" {{$answer_cheked == $question_answer->id ? 'checked' : ''}}> {{$char++}}. {!!$question_answer->answer!!}
              </label>
            </div>
          @endforeach
        @elseif($quiz_question->quiz_type_id == '7')
          @foreach($quiz_question->quiz_question_answers as $question_answer)
            <div>
              <label style="color:#19191a; margin-bottom: 1rem;" title="{{$question_answer->answer == '1' ? 'sangat tidak setuju' : ($question_answer->answer == '2' ? 'tidak setuju' : ($question_answer->answer == '3' ? 'netral' : ($question_answer->answer == '4' ? 'setuju' : 'sangat setuju')))}}">
                <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">
                {!! $question_answer->answer !!}
              </label>
            </div>
          @endforeach
        @elseif($quiz_question->quiz_type_id == '3')
          <div class="row">
            <div class="col-md-2">
              @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                <div style="cursor:pointer; background:#EEEEEE; padding:10px; margin: 7px 0; border-radius:5px">
                  {{$index+1}}
                </div>
              @endforeach
            </div>
            <div class="col-md-10">
              <div class="SectionSortable" question-id="{{$quiz_question->id}}">
                @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                  <li style="list-style:none; cursor:pointer; background:#EEEEEE; padding:10px; margin: 7px 0; border-radius:5px" data-id="{{$question_answer->id}}">
                    <p><i class="move fa fa-arrows-v"></i> {!!$question_answer->answer!!} ({{$index+1}})</p>
                  </li>
                @endforeach
                <input type="hidden" name="answer_ordering{{$quiz_question->id}}" value="" id="questionOrdering{{$quiz_question->id}}">
              </div>
            </div>
          </div>
        @elseif($quiz_question->quiz_type_id == '5')
          <textarea onkeyup="save_session_answer(this)" style="background: #fff; border: 1px solid #eee; width: 100%; height: 100px; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" class="form-control">{{$answer ? $answer->answer_essay : ''}}</textarea>
        @elseif($quiz_question->quiz_type_id == '6')
          <input onkeyup="save_session_answer(this)" style="background: #fff; border: 1px solid #eee; width: 100%; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="text" class="form-control">
        @elseif($quiz_question->quiz_type_id == '11')
          <input onchange="save_session_answer(this)" style="background: #fff; border: 1px solid #eee; width: 100%; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="time" class="form-control">
        @elseif($quiz_question->quiz_type_id == '10')
          <input onchange="save_session_answer(this)" style="background: #fff; border: 1px solid #eee; width: 100%; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="date" class="form-control">
        @elseif($quiz_question->quiz_type_id == '12')
          <select onchange="save_session_answer(this)" style="background: #fff; border: 1px solid #eee; width: 100%; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" class="form-control" required>
            <option value="">-Pilih Jawaban-</option>
            @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
            <option value="{{$question_answer->id}}">{{$question_answer->answer}}</option>
            @endforeach
          </select>
        @elseif($quiz_question->quiz_type_id == '13')
          <div class="form-group row justify-content-end">
            <label for="inputFile" class="col-lg-1 control-label" style="font-size:20px;line-height: 0.2;">File </label>
            <div class="col-lg-11">
              <input type="text" readonly="" class="form-control" style="background: #fff; border: 1px solid #eee; width: 100%; padding: 0.5rem; margin-bottom: 1rem;" placeholder="Browse...">
              <input onchange="save_session_answer(this)" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="file" class="form-control file-survey">
            </div>
          </div>
        @elseif($quiz_question->quiz_type_id == '14')
          <!-- Signature area -->
          <div class="signature"></div><br/>

          <textarea class="signature-output" onchange="save_session_answer(this)" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" style="display:none"></textarea>
        @elseif($quiz_question->quiz_type_id == '8')
          @php
            $char = 'A'; $char <= 'Z';
          @endphp
          @foreach($quiz_question->quiz_question_answers as $question_answer)
            <div>
              <label style="color:#19191a; margin-bottom: 1rem;">
                <input id="answer_id_{{$question_answer->id}}" question-checkbox-type="checkbox{{$question_answer->quiz_question_id}}" type="checkbox" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}"> {!!$question_answer->answer!!}
              </label>
            </div>
          @endforeach
        @endif
      </div>
    </div>
  @endforeach
</div>
