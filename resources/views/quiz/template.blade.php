@extends('layouts.app-static')

@section('title')
  @yield('quiz_title')
@endsection

@push('style')
  <link rel="stylesheet" href="/css/preload.min.css">
  <link rel="stylesheet" href="/css/plugins.min.css">
  <link rel="stylesheet" href="/css/content-dq-style.css">
  <link rel="stylesheet" href="/css/content-dq-layout.css">
  <link rel="stylesheet" href="/css/content-sidebar.css">
  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">
  <link rel="shotcut icon" href="{!! isset(Setting('icon')->value) ? Setting('icon')->value : '/img/ingenio-logo.png' !!}">

  <style media="screen">
    body {
      overflow-x: hidden;
      overflow-y: auto;
    }
    /* body{overflow: hidden}; */
    .vjs-play-progress{
      background: red !important;
      height: 8px;
    }
    .vjs-big-play-button {
      font-size: 3em;
      line-height: 2em !important;
      height: 2em !important;
      width: 3em !important;
      display: block;
      position: relative;
      top: 50% !important;
      left: 50% !important;
      padding: 0;
      cursor: pointer;
      opacity: 1;
      border: 0.06666em solid #fff;
      background-color: red !important;
      -webkit-border-radius: 0.3em;
      -moz-border-radius: 0.3em;
      border-radius: 0.3em;
      -webkit-transition: all 0.4s;
      -moz-transition: all 0.4s;
      -ms-transition: all 0.4s;
      -o-transition: all 0.4s;
      transition: all 0.4s;
    }
    .video-js .vjs-time-control{
      flex: none;
      font-size: 1em;
      line-height: 5.2em;
      min-width: 2em;
      width: auto;
      padding-left: 1em;
      padding-right: 1em;
    }
    .vjs-volume-bar .vjs-slider-horizontal{
      width: 5em;
      height: 1.3em;
    }
    .video-js .vjs-volume-bar{
      margin: 2.35em 0.45em;
      margin-top: 2.35em;
      margin-right: 0.45em;
      margin-bottom: 2.35em;
      margin-left: 0.45em;
    }
    .vjs-playback-rate .vjs-playback-rate-value{
      pointer-events: none;
      font-size: 1.5em;
      line-height: 3.3em;
      text-align: center;
    }
    .vjs-menu-button-popup .vjs-menu .vjs-menu-content{
      background-color: rgba(43, 51, 63, 0.7);
      position: absolute;
      width: 100%;
      bottom: 3.5em;
      max-height: 15em;
    }
    .video-js .vjs-control-bar{
      width: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 5.0em;
      background-color: rgba(43, 51, 63, 0.7);
    }
    .vjs-button > .vjs-icon-placeholder::before {
      font-size: 1.8em;
      line-height: 2.8em;
    }

    /* ASDAW */
    .btn-circle.btn-circle-raised.btn-circle-primary,
    .btn.btn-raised.btn-primary, 
    .btn.btn-fab.btn-primary, 
    .btn-group-raised .btn.btn-primary, 
    .input-group-btn .btn.btn-raised.btn-primary, 
    .input-group-btn .btn.btn-fab.btn-primary, 
    .btn-group-raised .input-group-btn .btn.btn-primary {
      background: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
      border: none;
    }

    .btn-circle.btn-circle-raised.btn-circle-primary:before,
    .btn.btn-raised:not(.btn-link):hover.btn-primary, 
    .btn.btn-raised:not(.btn-link):focus.btn-primary, 
    .btn.btn-raised:not(.btn-link).active.btn-primary, 
    .btn.btn-raised:not(.btn-link):active.btn-primary, 
    .btn-group-raised .btn:not(.btn-link):hover.btn-primary, 
    .btn-group-raised .btn:not(.btn-link):focus.btn-primary, 
    .btn-group-raised .btn:not(.btn-link).active.btn-primary, 
    .btn-group-raised .btn:not(.btn-link):active.btn-primary, 
    .input-group-btn .btn.btn-raised:not(.btn-link):hover.btn-primary, 
    .input-group-btn .btn.btn-raised:not(.btn-link):focus.btn-primary, 
    .input-group-btn .btn.btn-raised:not(.btn-link).active.btn-primary, 
    .input-group-btn .btn.btn-raised:not(.btn-link):active.btn-primary, 
    .btn-group-raised .input-group-btn .btn:not(.btn-link):hover.btn-primary, 
    .btn-group-raised .input-group-btn .btn:not(.btn-link):focus.btn-primary, 
    .btn-group-raised .input-group-btn .btn:not(.btn-link).active.btn-primary, 
    .btn-group-raised .input-group-btn .btn:not(.btn-link):active.btn-primary {
      /* background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' }}; */
      background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    }

    .pagination .page-item:hover .page-link,
    .pagination .page-item.active .page-link {
      background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
      border-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    }

    .pagination .page-item .page-link {
      width: 40px;
      height: 40px;
      display: flex;
      align-items: center;
      justify-content: center;
      color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
      transition: all ease .2s;
    }
    
    .pagination .page-item.active .page-link:hover, 
    .pagination .page-item.active .page-link:focus {
      background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
      box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.1);
    }
    /* ASDAW */
  </style>

  <style media="screen">
    .player-content--text:before {
      content: '';
      position: fixed;
      display: block;
      background: transparent; /* default linear-gradient(180deg, #333, transparent) */
      width: 100%;
      height: 100%;
      opacity: 0;
      top: 0;
      z-index: 0;
      transition: 0.3s ease-in-out;
    }

    .embed-responsive:before {
      content: '';
      position: relative;
      display: block;
      background: linear-gradient(180deg, #333, transparent);
      height: 100%;
      opacity: 0;
      z-index: 1; /* Jika quiz atau assignment z-index: 0; */
      transition: 0.3s ease-in-out;
    }

    .player-content--text:hover:before,
    .embed-responsive:hover:before {
      opacity: 1;
    }

    .player-content--text .player-navbar .content-toggle,
    .embed-responsive .player-navbar .content-toggle {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #222;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-title,
    .embed-responsive .player-navbar .content-title {
      display: none; /* default without display:none */
      color: #fff;
      line-height: 50px;
      padding: 0 1rem;
      margin: 0;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-close,
    .embed-responsive .player-navbar .content-close {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #e41f1f;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0
    }

    .player-content--text .player-navbar {
      position: sticky;
      margin-bottom: 1rem;
    }

    .player-content--text .player-navigate {
      position: sticky;
      top: calc(50% - 50px);
      z-index: 0;
    }

    .player-content--text .row.player-content-group {
      margin-top: -50px;
      margin-left: 0;
      margin-right: 0;
    }

    .player-content--text .player-attach {
      background: #eee;
      padding: 2rem 0;
      margin: 0;
    }

    .player-content--text .player-attach .attach-link {
      color: #007bff;
    }

    .player-content--text .player-attach .attach-link:hover {
      color: #004b9c;
    }

    .player-content--text:hover .player-navbar .content-toggle,
    .player-content--text:hover .player-navbar .content-title,
    .player-content--text:hover .player-navbar .content-close,
    .embed-responsive:hover .player-navbar .content-toggle,
    .embed-responsive:hover .player-navbar .content-title,
    .embed-responsive:hover .player-navbar .content-close {
      opacity: 1;
    }

    .embed-responsive:hover .vjs-big-play-centered .vjs-big-play-button,
    .embed-responsive:hover .video-js .vjs-control-bar {
      z-index: 2;
    }

    .player-content--text:hover .player-navbar .content-toggle:hover,
    .embed-responsive:hover .player-navbar .content-toggle:hover {
      background: #111;
      color: #fff;
    }

    .player-content--text:hover .player-navbar .content-close:hover,
    .embed-responsive:hover .player-navbar .content-close:hover {
      background: #cc3333;
      color: #fff;
    }

    .player-navbar {
      position: absolute;
      width: 100%;
      opacity: 1;
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      top: 0;
      z-index: 999;
      transition: 0.5s ease-in-out;
    }

    .player-navigate {
      position: absolute;
      display: flex;
      width: 100%;
      height: 100%;
      align-items: center;
      justify-content: space-between;
      top: 0;
    }

    .player-navigate .navigate {
      display: flex;
      height: auto;
      background: rgba(0, 0, 0, 0.25);
      align-items: center;
      justify-content: center;
      font-size: 26px;
      color: #fff;
      padding: 0.75rem;
      cursor: pointer;
      z-index: 3;
      transition: 0.3s ease-in-out;
    }

    .player-navigate .navigate:hover {
      background: rgba(0, 0, 0, 0.75);
    }

    #sidebar .player-content-headbox {
      position: sticky;
      display: flex;
      background: #f3f3f3;
      align-items: center;
      font-weight: 600;
      color: #333;
      top: 0;
      z-index: 10;
    }

    #sidebar .player-content-headbox .headbox-button {
      display: inline-flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      font-size: 20px;
      border-right: 2px solid #e3e3e3;
      cursor: pointer;
      padding: 1rem;
    }

    #sidebar .player-content-headbox .headbox-button.button-right {
      display: none;
      border-left: 2px solid #e3e3e3;
      border-right: none;
    }

    #sidebar .player-content-headbox .headbox-group {
      display: flex;
      align-items: center;
      width: calc(100% - 50px);
      height: 50px;
      padding: 1rem;
    }

    #sidebar .player-content-list {
      display: block;
      overflow-y: auto;
      height: calc(100% - 50px);
    }

    #sidebar .player-content-list .head-of-list {
      background: #ececec;
      display: block;
      padding: 0.75rem;
    }

    #sidebar .player-content-list .content-of-list {
      background: #f3f3f3;
      display: block;
      color: #333;
      padding: 0.75rem;
    }

    #sidebar .player-content-list li.active .content-of-list {
      background: #dae5eb;
    }

    #sidebar .player-content-list .content-of-list .list-row {
      display: flex;
      align-items: baseline;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-icon,
    #sidebar .player-content-list .content-of-list .list-row .list-text,
    #sidebar .player-content-list .content-of-list .list-row .list-check {
      padding-left: 5px;
      padding-right: 5px;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-check {
      margin-left: auto;
    }

    #content .player-content-headbox,
    #content .player-content-list {
      display: none;
    }

    @media (max-width: 768px) {
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }
      .embed-responsive-16by9::before{
        padding-top: 56.25%;
      }
      .player-navbar {
        display: none;
      }
    }

    @media (max-width: 420px) {
      body {
        overflow-x: hidden;
        overflow-y: auto;
      }
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }

      .player-content--text .player-navbar {
        display: flex;
      }

      .player-content--text .player-navigate {
        z-index: 1;
      }

      .player-content--text .row .content--text * {
        font-size: 14px;
      }

      .player-content--text .row.player-content-group .col-md-8 {
        margin-bottom: 25%;
      }

      .player-content--text .player-navbar .content-toggle,
      .player-content--text .player-navbar .content-close,
      .embed-responsive .player-navbar .content-toggle,
      .embed-responsive .player-navbar .content-close {
        width: 50px;
        height: 50px;
        opacity: 1
      }

      .player-content--text .player-navbar .content-title,
      .embed-responsive .player-navbar .content-title,
      .player-content--text .player-navbar .content-close {
        display: none;
      }

      .player-navigate {
        position: absolute;
        display: flex;
        width: 100%;
        height: 100%;
        align-items: center;
        justify-content: space-between;
        top: 0;
      }

      .player-navigate .navigate {
        font-size: 26px;
        padding: 0.5rem;
      }

      #sidebar .player-content-headbox .headbox-group {
        width: calc(100% - 100px);
      }

      #sidebar .player-content-headbox .headbox-button.button-right {
        display: inline-flex;
      }

      #sidebar .player-content-list {
        height: calc(100% + 20px);
      }

      #content .container-full.controlable {
        position: sticky;
        top: 0;
        z-index: 1;
      }

      #content .player-content-headbox {
        position: fixed; /* addon */
        width: 100%; /* addon */
        background: #f3f3f3;
        display: flex;
        align-items: center;
        z-index: 1; /* addon */
      }

      #content .player-content-headbox .headbox-group {
        width: calc(100% - 50px);
        padding: 0.75rem;
      }

      #content .player-content-headbox .headbox-button {
        width: 50px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        border-right: 2px solid #e3e3e3;
        padding: 0.75rem;
      }

      #content .player-content-list {
        background: #f9f9f9;
        display: block;
        padding-bottom: 0.5rem; /* addon */
        margin-top: 55px; /* addon */
      }

      #content .player-content-list .head-of-list {
        background: #ececec;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list .content-of-list {
        background: #f3f3f3;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list li.active .content-of-list {
        background: #dae5eb;
      }

      #content .player-content-list .content-of-list .list-row {
        display: flex;
        align-items: baseline;
      }

      #content .player-content-list .content-of-list .list-row .list-icon,
      #content .player-content-list .content-of-list .list-row .list-text,
      #content .player-content-list .content-of-list .list-row .list-check {
        padding-left: 5px;
        padding-right: 5px;
      }
    }
  </style>

  @stack('quiz_style')

@endpush

@section('content')
  <div id="wrapper" class="wrapper">
    <nav id="sidebar">
      <div class="player-content-headbox">
        <a class="headbox-button" href="/course/learn/{{ $course->slug }}" title="Back to course">
          <i class="zmdi zmdi-arrow-left"></i>
        </a>
        <div class="headbox-group">
          <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
        </div>
        <a id="sidebarCollapseOff" class="headbox-button button-right" href="#" title="Hide menu">
          <i class="zmdi zmdi-close"></i>
        </a>
      </div>
      <div class="player-content-list">
        <ul class="list-unstyled components">

          <?php $i=1; ?>
          @foreach($sections as $section)
            <li class="padding-10">
              <a class="head-of-list" href="#page{{$i}}" data-toggle="collapse" aria-expanded="false">
                <p class="fs-12 nopadding nomargin">Section {{$i}}:</p>
                <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
              </a>
              <ul class="collapse list-unstyled show in" id="page{{$i}}">
                @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                  @if($all->section_type == 'content')
                    @php $content = $all; @endphp
                    <li class="">
                      <a class="content-of-list" href="/course/learn/content/{{$course->slug}}/{{$content->id}}">
                        <div class="list-row">
                          <i class="list-icon {{content_type_icon($content->type_content)}}"></i>
                          <div class="list-text">{{$content->title}}</div>
                          <div class="list-check checkbox">

                          </div>
                        </div>
                      </a>
                    </li>
                  @endif

                  @if($all->section_type == 'quizz')
                    @php $quiz = $all; @endphp
                    <li class="{{ $quiz->id == \Request::segment(5) ? 'active' : ''}}">
                      <a class="content-of-list" href="{{'/course/learn/quiz/'.$course->slug.'/'. $quiz->id}}">
                        <div class="list-row">
                          <i class="list-icon {{$quiz->quizz_type == 'quiz' ? 'fa fa-star': 'fa fa-edit'}}"></i>
                          <div class="list-text">{{$quiz->name}}</div>
                          <div class="list-check checkbox">

                          </div>
                        </div>
                      </a>
                    </li>
                  @endif

                  @if($all->section_type == 'assignment')
                    @php $assignment = $all; @endphp
                    @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                      <li class="">
                        <a class="content-of-list" href="{{'/course/learn/assignment/'.$course->slug.'/'. $assignment->id}}">
                          <div class="list-row">
                            <i class="list-icon fa fa-tasks"></i>
                            <div class="list-text">{{$assignment->title}}</div>
                            <div class="list-check checkbox">

                            </div>
                          </div>
                        </a>
                      </li>
                    @endif
                  @endif
                @endforeach

              </ul>
            </li>
            <?php $i++; ?>
          @endforeach
        </ul>
      </div>
    </nav>

    <div id="content" style="background: white">
      <div id="contentBody">
        <div class="container-full">
          <div class="player-content--text">
            <nav class="player-navbar">
              <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                <i class="zmdi zmdi-menu"></i>
              </a>
              <p class="content-title text-truncate">{{$quiz->title}}</p>
              <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                <i class="zmdi zmdi-close"></i>
              </a>
            </nav>
            <div class="player-navigate">
              <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
              <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
            </div>

            @yield('quiz_content')

          </div>
        </div>
      </div>

    </div>
  </div>

@endsection

@push('script')
  <script src="/js/jquery.js"></script>
  <script src="/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $('#sidebar').removeClass('active');
    } else {
      $('#sidebar').addClass('active');
    }
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('.quiz-panel--mobile').hide();
            $('#sidebar').toggleClass('active');
        });
    });
    $(document).ready(function () {
        $('#sidebarCollapseOff').on('click', function () {
            $('#sidebar').toggleClass();
            $('.quiz-panel--mobile').show();
        });
    });
  </script>

  @php
  $contents = [];
  foreach($course->sections as $section_course){
    foreach($section_course->contents as $content){
      array_push($contents, [
        'type' => 'content',
        'id' => $content->id,
      ]);
    }
    foreach($section_course->quizzes as $quiz){
      array_push($contents, [
        'type' => 'quiz',
        'id' => $quiz->id,
      ]);
    }
    foreach($section_course->assignments as $assignment){
      array_push($contents, [
        'type' => 'assignment',
        'id' => $assignment->id,
      ]);
    }
  }
  // dd($contents);
  $current_content = !Request::get('quiz') && !Request::get('assignment') ? Request::segment(5) : (Request::get('quiz') ? Request::get('quiz') : Request::get('assignment')) ;
  $current_index = array_search($current_content, array_column($contents, 'id'));
  $next = $current_index + 1;
  $prev = $current_index - 1;
  @endphp

  @if($prev >= 0)
  <script type="text/javascript">
    @if($contents[$prev]['type'] == 'content')
      $("#backContent").attr("href", "/course/learn/content/{{$course->slug}}/{{$contents[$prev]['id']}}")
    @endif
    @if($contents[$prev]['type'] == 'quiz')
      $("#backContent").attr("href", "/course/learn/quiz/{{$course->slug}}/{{$contents[$prev]['id']}}")
    @endif
    @if($contents[$prev]['type'] == 'assignment')
      $("#backContent").attr("href", "/course/learn/assignment/{{$course->slug}}/{{$contents[$prev]['id']}}")
    @endif
  </script>
  @endif

  @if($next < count($contents))
  <script type="text/javascript">
    @if($contents[$next]['type'] == 'content')
      $("#nextContent").attr("href", "/course/learn/content/{{$course->slug}}/{{$contents[$next]['id']}}")
      $(".nextContent").attr("href", "/course/learn/content/{{$course->slug}}/{{$contents[$next]['id']}}")
    @endif
    @if($contents[$next]['type'] == 'quiz')
      $("#nextContent").attr("href", "/course/learn/quiz/{{$course->slug}}/{{$contents[$next]['id']}}")
      $(".nextContent").attr("href", "/course/learn/quiz/{{$course->slug}}/{{$contents[$next]['id']}}")
    @endif
    @if($contents[$next]['type'] == 'assignment')
      $("#nextContent").attr("href", "/course/learn/assignment/{{$course->slug}}/{{$contents[$next]['id']}}")
      $(".nextContent").attr("href", "/course/learn/assignment/{{$course->slug}}/{{$contents[$next]['id']}}")
    @endif
  </script>
  @endif

  @stack('quiz_script')
@endpush
