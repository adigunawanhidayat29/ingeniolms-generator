@extends('layouts.app')

@section('title', 'Meeting')

@section('content')
  <div class="row">
    <div class="col-md-9">
      {{-- <div class="alert alert-info">
        Dont forget to record this meeting for History course. <a data-toggle="modal" data-target="#modalStartRecording" href="#">Start Recording</a> or <a href="/bigbluebutton/meeting/upload/record/{{$course_id}}">Upload recording</a>
      </div> --}}
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="{{$url}}"></iframe>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Chat</h3>
        </div>
        <div class="panel-body" id="panel_chat" style="height:600px; overflow:auto;">
          <div id="MessageShow"></div>
        </div>
        <div class="panel-footer text-right">
          <div class="row">
            <div class="col-md-10">
              <div class="form-group">
                <textarea class="form-control" rows="2" cols="80" placeholder="Write something..." id="MessageBody"></textarea>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <button id="MessageSend" class="btn btn-default">Send</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalStartRecording" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Start Recording</h4>
        </div>
        <div class="modal-body">
          <p>Follow step to recording your live meeting :</p>
          <p>1. Install Extension Screencastify. <a href="https://chrome.google.com/webstore/detail/screencastify-screen-vide/mmeijimgabbpbgpdklnllpncmdofkcpn" target="_blank">Add Screencastify</a></p>
          <p>2. To start recording please press ALT + SHIFT + R</p>
          <p>3. Enjoy recording.</p>
          <p>4. Upload your recording so that student can watch again later or download your video recording.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('script')

  <script src="/pusher/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('6fe45cd1b03b7a9d8def', {
      cluster: 'ap1',
      encrypted: true
    });

    var channel = pusher.subscribe('chat-{{$course_id}}');
    channel.bind('get-chat', function(data) {

      var avatar = data.user.photo;
      var avatarSplit = avatar.split('/')
      if(avatarSplit[1] == 'uploads'){
        avatar = "{{asset_url()}}" + avatar;
      }else{
        avatar = avatar
      }

      var isUserSender = data.user.id;
      var isUserLoggedIn = "{{Auth::user()->id}}";

      if(isUserSender == isUserLoggedIn){
        var column_user = '<div class="row">'+
          '<div class="col-md-10">'+
            '<strong>'+data.user.name+'</strong><br>'+
            data.course_chat.body+
          '</div>'+
          '<div class="col-md-2">'+
            '<img class="img-responsive img-circle" style="height:30px" src="'+avatar+'">'+
          '</div>'+
        '</div>'+
        '<hr>';
      }else{
        var audio = new Audio('/sounds/notif.mp3');
        audio.play();
        var column_user = '<div class="row">'+
          '<div class="col-md-2">'+
            '<img class="img-responsive img-circle" style="height:30px" src="'+avatar+'">'+
          '</div>'+
          '<div class="col-md-10">'+
            '<strong>'+data.user.name+'</strong><br>'+
            data.course_chat.body+
          '</div>'+
        '</div>'+
        '<hr>';
      }

      $("#MessageShow").append(column_user);
    });
  </script>

  <script type="text/javascript">

    function updateScroll(){
      var element = document.getElementById("panel_chat");
      element.scrollTop = element.scrollHeight;
    }
    updateScroll();

    // setInterval(function(){
    //   MessageShow();
    // }, 5000);

    function MessageShow(){
      $.ajax({
        url : "{{url('message/get/'.$course_id)}}",
        type : "GET",
        success : function(result){
          $("#MessageShow").html(result);
        },
      });
    }
    MessageShow();
    // updateScroll();

    function MessageSend(){
      var course_id = '{{$course_id}}';
      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('message/save')}}",
        type : "POST",
        data : {
          body : $("#MessageBody").val(),
          course_id : course_id,
          _token: token
        },
        success : function(result){
          // MessageShow();
          // updateScroll();
          $("#MessageBody").val('');
        },
      });
    }

    $("#MessageSend").click(function(){
      MessageSend();
    });

    $("#MessageBody").keyup(function(e){
      var code = (e.keyCode ? e.keyCode : e.which);
      if(code == 13) { //Enter keycode
        MessageSend();
      }
    });
  </script>
@endpush
