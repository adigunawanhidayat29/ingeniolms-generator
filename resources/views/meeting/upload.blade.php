@extends('layouts.app')

@section('title', 'Upload Record')

@section('content')
  <div class="container">
    {{Form::open(['url'=>'/bigbluebutton/meeting/save/record/'.$meeting->id, 'method'=>'post', 'files'=>true])}}
    <div class="form-group">
      <label for="">Video</label>
      <input type="file" name="file" class="form-control" id="" placeholder="">
      <p class="help-block">Upload your video record.</p>
    </div>
    <div class="form-group">
      <button type="submit" name="submit" class="btn btn-default">Upload</button>
    </div>
    {{Form::close()}}
  </div>

@endsection

@push('script')

@endpush
