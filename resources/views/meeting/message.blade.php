@foreach($courses_chats as $course_chat)
  @if(Auth::user()->id === $course_chat->user_id)
    <div class="row">
      <div class="col-md-10">
        <strong>{{$course_chat->name}}</strong><br>
        {{$course_chat->body}}
      </div>
      <div class="col-md-2">
        <img class="img-responsive img-circle" style="height:30px" src="{{avatar($course_chat->photo)}}" alt="">
      </div>
    </div>
    <hr>
  @else
    <div class="row">
      <div class="col-md-2">
        <img class="img-responsive img-circle" style="height:30px" src="{{avatar($course_chat->photo)}}" alt="">
      </div>
      <div class="col-md-10">
        <strong>{{$course_chat->name}}</strong><br>
        {{$course_chat->body}}
      </div>
    </div>
    <hr>
  @endif
@endforeach
