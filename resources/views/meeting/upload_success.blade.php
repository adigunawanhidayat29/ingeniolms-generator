@extends('layouts.app')

@section('title')
	{{ 'Record Uploaded'}}
@endsection

@section('content')
	<div style="margin-top:120px;"></div>
	<div class="container">
		<div class="alert alert-success">
			<p>{!! Session::get('success') !!}</p>
		</div>
	</div>
@endsection
