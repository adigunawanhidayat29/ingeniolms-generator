<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>

<head>
  <title>Player</title>

  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/scorm/sscompat.js" type="text/javascript"></script>
  <script src="/js/scorm/sscorlib.js" type="text/javascript"></script>
  <script src="/js/scorm/ssfx.Core.js" type="text/javascript"></script>
  <script type="text/javascript" src="/js/scorm/API_BASE.js"></script>
  <script type="text/javascript" src="/js/scorm/API.js"></script>
  <script type="text/javascript" src="/js/scorm/Controls.js"></script>
  <script type="text/javascript" src="/js/scorm/Player.js"></script>

  <style type="text/css">
    * {
      margin: 0px;
    }

    html,
    body,
    iframe {
      height: 100%;
    }

    html,
    body {
      /*overflow-y:hidden;*/
      font-family: Helvetica, "Helvetica Neue", Arial;
      font-size: 12px;
      color: #000000;
    }

    iframe {
      display: block;
      width: 100%;
    }

    html,
    body,
    iframe,
    a,
    img,
    table,
    tbody,
    tr,
    td,
    table td,
    table th {
      border: 0px none;
      padding: 0px;
    }



    a:link {
      color: #0000FF;
    }

    a:visited {
      color: #0000FF;
    }

    a:hover {
      color: #000080;
    }

    a:active {
      color: #0000FF;
    }

    #btnExit {
      margin-left: 5px;
      display: none !important;
    }

    #btnAbandon {
      margin-left: 5px;
    }

    #btnSuspendAll {
      margin-left: 5px;
    }
  </style>

  <!--[if IE]>
<style type="text/css">
html, body { overflow-y:hidden; }
</style>
<![endif]-->

  <script type="text/javascript">
    function InitPlayer() {

      //  PlayerConfiguration.TreeMinusIcon = "images/minus.gif";
      //  PlayerConfiguration.TreePlusIcon = "images/plus.gif";
      //  PlayerConfiguration.TreeLeafIcon = "images/leaf.gif";
      //  PlayerConfiguration.TreeActiveIcon = "images/select.gif";

      //  PlayerConfiguration.BtnPreviousLabel = "Previous";
      //  PlayerConfiguration.BtnContinueLabel = "Continue";
      //  PlayerConfiguration.BtnExitLabel = "Exit";
      PlayerConfiguration.BtnExitAllLabel = "Exit All";
      //  PlayerConfiguration.BtnAbandonLabel = "Abandon";
      //  PlayerConfiguration.BtnAbandonAllLabel = "Abandon All";
      //  PlayerConfiguration.BtnSuspendAllLabel = "Suspend All";


      PlayerRun();

    }

    function PlayerRun() {
      var contentDiv = document.getElementById('placeholder_contentIFrame');
      contentDiv.innerHTML = "";
      Run.ManifestByURL('http://localhost:8000/uploads/scorm/1/imsmanifest.xml', true);
    }
  </script>

</head>

<body onload="InitPlayer()" style="background-color:#FFFFFF;">
  <div id="placeholder_contentIFrame" style="width:100%;height:100%;overflow:auto;-webkit-overflow-scrolling:touch;"></div>
</body>

</html>

<script>
  $("#btnExitAll").hide()
</script>