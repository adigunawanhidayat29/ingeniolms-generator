@extends('layouts.app')

@section('title')
	{{ 'Membuat Tugas ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.content_create.create') <span>@lang('front.content_create.assignment')</span></h2>
        </div>
      </div>
    </div>
  </div>
	<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
					<li><a href="#">@lang('front.content_create.teacher')</a></li>
					<li><a href="/course/dashboard">@lang('front.content_create.manage_course')</a></li>
					<li><a href="/course/preview/{{$section->id_course}}">@lang('front.content_create.preview')</a></li>
					<li>@lang('front.content_create.create') @lang('front.content_create.assignment') - {{$section->title}}</li>
        </ul>
      </div>
    </div>
  </div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
	            </center>

							<div class="card-md">
								{{--
							  <div class="panel-heading">
							    <h3 class="panel-title">{{$button}} Tugas</h3>
							  </div>
								--}}
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										<div class="form-group no-m">
											<label for="title">@lang('front.content_create.content_title_name')</label>
											<input placeholder="Masukan Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
										</div>

										<div class="form-group">
											<label for="title">@lang('front.content_create.description')</label>
											<textarea name="description" id="description" required>{{ $description }}</textarea>
										</div>
										<div class="form-group">
											<label for="title">@lang('front.content_create.assignment_type')</label><br>
											<select class="form-control selectpicker" name="type" requred>
												<option {{$type == '0' ? 'selected' : ''}} value="0">Online Text</option>
												<option {{$type == '1' ? 'selected' : ''}} value="1">File Upload</option>
											</select>
										</div>
										<div class="form-group" id="uploadfile">
											<label for="title">File (Docx, PDF, Txt.)</label>
											<input type="text" readonly="" class="form-control" placeholder="Pilih...">
											<input type="file" id="file" name="fileToUpload" class="form-control">
										</div>
										<div class="form-group">
											<label for="Attempt">@lang('front.content_create.opportunity_answer')</label>
											<input placeholder="@lang('front.content_create.opportunity_answer')" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
										</div>
										{{-- <input placeholder="Kesempatan Percobaan" type="hidden" class="form-control" name="attempt" value="{{ $attempt }}"> --}}
										<div class="form-group">
											<label for="name">@lang('front.page_manage_courses.overview_course_start_date')</label>
											<input placeholder="@lang('front.page_manage_courses.overview_course_start_date')" autocomplete="off" type="text" class="form-control form_datetime" name="time_start" value="{{ $time_start }}" required>
										</div>

										<div class="form-group">
											<label for="name">@lang('front.page_manage_courses.overview_course_end_date')</label>
											<input placeholder="@lang('front.content_create.end_date')" autocomplete="off" type="text" class="form-control form_datetime" name="time_end" value="{{ $time_end }}" required>
										</div>

										<div class="form-group" id="divDescription">
											<label for="title">@lang('front.content_create.content_for')</label>
											<select name="group_student_id" class="form-control selectpicker" id="group_student_id">
												<option value="all">@lang('front.content_create.content_for_all')</option>
												<option value="group" {{count($group_has_content) > 0 ? 'selected' : ''}}>@lang('front.content_create.content_for_group')</option>
											</select>
										</div>

										<div id="section_show_group">
											@foreach ($course_student_groups as $item)
												<label for="course_student_group_{{$item->id}}" style="color: black !important;">
													<input id="course_student_group_{{$item->id}}" name="course_student_group[]" class="course_student_group" type="checkbox" value="{{$item->id}}" @foreach($group_has_content as $data) {{$data->course_student_group_id == $item->id ? 'checked' : ''}} @endforeach> {{$item->name}}
												</label> <br>
											@endforeach
										</div>

										<div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit('Save' . " Tugas" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											{{-- @if(Request::segment(3) == 'create')
												<button class="btn btn-warning btn-raised" id="saveDraft" type="button">@lang('front.content_create.save_and_publish')</button>
											@endif --}}
											<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">@lang('front.content_create.cancel')</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	{{-- saving draft --}}
	@if(Request::segment(3) == 'create')
		<script>
				$('#saveDraft').click(function () {
						const title = $('input[name = "title"]').val();
						const description = $('#description').val();
						const timestart = $('input[name = "time_start"]').val();
						const timeend = $('input[name = "time_end"]').val();
						const attempt = $('input[name = "attempt"]').val();
						const type = $('.selectpicker').children('option:selected').val();
						// console.log(type);
						const section_id = '{{Request::segment(4)}}';

						$.ajax({
								url: "{{url('course/assignment/create_draft')}}/"+section_id,
								type: 'POST',
								data: {
										_token : '{{csrf_token()}}',
										title : title,
										description : description,
										type : type,
										time_start : timestart,
										time_end : timeend,
										attempt : attempt,
								},
								success: function(response){
										// alert('saved draft');
										location.assign("{{url('course/preview/'.$section->id_course)}}");
								}
						})
				})
		</script>
	@endif
	{{-- saving draft --}}

	<script>
		@if ($button == "Update")
			@if ($group_has_content->count() > 0)
				$("#section_show_group").show();
			@else
				$("#section_show_group").hide();
			@endif
		@else
			$("#section_show_group").hide();
		@endif

		$("#group_student_id").change(function() {
			if ($(this).val() == 'group') {
				$("#section_show_group").show();
				$("input[name^=course_student_group]").prop('checked', true)
			} else {
				$("#section_show_group").hide();
				$("input[name^=course_student_group]").prop('checked', false)
			}
		})
	</script>

@endpush
