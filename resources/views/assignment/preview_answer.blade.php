@extends('layouts.app')

@section('title')
	{{ 'Assignment Answer' }}
@endsection

@section('content')
	<div class="bg-page-title-negative">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="headline-md no-m">Jawaban <span>Peserta</span></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li><a href="/course/dashboard">Kelola Kelas</a></li>
          <li><a href="/course/preview/{{$section->course->id}}">{{$section->course->title}}</a></li>
          <li><a href="/course/assignment/answer/{{$AssignmentAnswer->assignments_id}}">{{$AssignmentAnswer->title}}</a></li>
          <li>Jawaban Peserta</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="">
				<center>
					@if(Session::has('success'))
						<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('success') !!}
						</div>
					@elseif(Session::has('error'))
						<div class="alert alert-error alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('error') !!}
						</div>
					@endif
				</center>
				@if($AssignmentAnswer->type == '1')
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="{{$AssignmentAnswer->answer}}"></iframe>
						<div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
					</div>
				@else
					{!!$AssignmentAnswer->answer!!}

					<p>Catatan: {{$AssignmentAnswer->description}}</p>
				@endif
				<a data-assignment-answer-id="{{$AssignmentAnswer->id}}" data-assignment-id="{{$AssignmentAnswer->assignment_id}}" id="giveGrade" class="btn btn-raised btn-primary" href="#">Berikan Nilai</a>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalGiveGrade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="form-group">
					  <label for="">Grade</label>
						<input type="hidden" id="assignment_id" value="">
						<input type="hidden" id="assignment_answer_id" value="">
					  <input type="number" class="form-control" id="inputGrade" placeholder="Grade">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Close<div class="ripple-container"></div></button>
					<button type="submit" class="btn btn-raised btn-primary" id="submitGrade">Submit</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#giveGrade',function(e){
				e.preventDefault();
					var assignment_answer_id = $(this).attr('data-assignment-answer-id');
					var assignment_id = $(this).attr('data-assignment-id');
					$("#assignment_id").val(assignment_id);
					$("#assignment_answer_id").val(assignment_answer_id);
					$("#modalGiveGrade").modal('show');
			});
		});

		$('#submitGrade').click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/assignment/save-grade')}}",
				type : "POST",
				data : {
					assignment_id : $("#assignment_id").val(),
					assignment_answer_id : $("#assignment_answer_id").val(),
					grade : $("#inputGrade").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		});

	</script>
@endpush
