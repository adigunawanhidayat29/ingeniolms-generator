@extends('assignment.template')

@section('assignment_title')
	{{ 'Assignment Answer' }}
@endsection

@push('style')
	<style>
		.card-assignment {
				border-radius: 0 0 5px 5px;
				background: #f9f9f9;
				transition: all 0.3s;
				border: 1px solid #f5f5f5;
				margin-bottom: 2rem;
				margin-bottom: 20px;
		}

		.card-assignment .card-header {
				padding: 15px;
				background: #f6f6f6;
				border-bottom: 1px solid #ececec;
				margin-bottom: 0;
		}

		.card-assignment .card-header .card-title {
				margin: 0;
				font-size: 16px;
				font-weight: 500;
				color: #333;
				display: contents;
		}

		.card-assignment .card-block {
				padding: 15px;
				margin-bottom: -1rem;
		}
		
		.card-assignment .card-block p {
				font-size: 16px;
		}
		
		.card-assignment .card-block table tr td p {
				margin-bottom: 0;
		}
		
		.card-assignment .card-block .form-group {
				padding-bottom: 0;
				margin: 15px 0 0;
		}

		@media (max-width: 420px) {
				.card-assignment .card-block p {
						font-size: 14px;
						margin-bottom: 0;
				}
		}
	</style>
@endpush

@section('assignment_content')
	<div class="container">
		<div class="row player-content-group">
			<div class="col-md-12">
				<center>
					@if(Session::has('success'))
						<div class="alert alert-success alert-dismissible border-0">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('success') !!}
						</div>
					@elseif(Session::has('error'))
						<div class="alert alert-error alert-dismissible border-0">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('error') !!}
						</div>
					@endif
				</center>

				<h2 class="headline headline-md" style="margin-bottom: 1.5rem;"><span>Jawaban Saya</span></h2>
				
				<div class="card-assignment">
					<div class="card-block nopadding">
						<table class="table table-striped table-no-border">
							@foreach($assignments_answers as $assignment_answer)
								<tr>
									<td>
										@if($assignment->type == '0')
											{!!$assignment_answer->answer!!}
										@else
											<div class="panel-body">
												<div class="">
													{{-- <iframe style="width:100% !important; height:600px" class="embed-responsive-item" src="{{$assignment_answer->answer}}"></iframe> --}}
													{{-- <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> --}}
													<a href="{{asset_url($assignment_answer->answer)}}" target="_blank" style="text-decoration: underline !important">Unduh Jawaban: {{asset_url($assignment_answer->answer)}}</a>
												</div>
											</div>
										@endif
										<strong>Catatan:</strong>
										{{ $assignment_answer->description }}
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('assignment_script')

@endpush
