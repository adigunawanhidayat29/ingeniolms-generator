@extends('layouts.app-static')

@section('title')
	{{ 'Assignment Answered'}}
@endsection

@section('content')
	<div class="container">
		<br>
		<div class="alert alert-success">
			<p>{!! Session::get('success') !!}</p>
		</div>
	</div>
@endsection
