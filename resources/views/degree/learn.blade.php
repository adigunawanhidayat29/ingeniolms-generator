@extends('layouts.app')

@section('title', 'Learn Degree ' . $degree->title)

@push('style')
  <style>
    .dq-project-bg{
      background-image: url(/img/project-bg.jpg);
      background-size: cover;
      background-position: top center;
      background-repeat: no-repeat;
    }
    .ms-collapse .card .card-header .card-title a.collapsed:after{
      content:none;
    }
    .ms-collapse .card .card-header .card-title a:after{
      content:none;
    }
    .dq-degree-bg{
      background:url(/img/unpad/perpustakaan1.jpg) no-repeat center;
      background-size: cover;
      width: 100%;
      height: 400px;
      position: relative;
      overflow: hidden;
    }
    .dq-overlay{
      position: absolute;
      top: 0;
      left:0;
      width:100%;
      height: 100%;
      background: rgba(0,0,0,0.7);
    }
  </style>

@endpush

@section('content')
  <div class="container mt-6">
    @if(Session::has('success'))
      <div class="alert alert-success">
        {{Session::get('success')}}
      </div>
    @endif

    <h1>Masters Degree {{count($courses_data)}} Module <i class="fa fa-caret-down"></i></h1>    

    <div class="row">
      @foreach($courses_data as $course)
      @php
        $num_contents = $course['num_contents'];
        //count percentage
        $num_progress = 0;
        $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = 100 / $percentage;
        //count percentage
      @endphp

        <div class="col-md-3">
          <div class="card">
            <a href="{{url('course/learn/'. $course['slug'])}}" class="zoom-img withripple">
              <img src="{{asset_url(course_image_small($course['image']))}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
            </a>
            <a href="{{url('course/'. $course['slug'])}}">
              <div class="card-block text-left">
                <h4 class="color-primary dq-max-title">{{$course['title']}}</h4>
                <p>
                  <i class="color-dark">{{ $course['name'] }}</i>
                </p>
                <div class="mt-2">
                <div class="progress">
                  <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
                </div>
                <span class="color-primary dq-span-border">
                  {{$percentage > 1 ? 'Lanjut Belajar' : 'Mulai Belajar'}}
                </span>
                <br><br>
                <span class="mr-2 pull-right">
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-dark"></i>
                </span>
                </div>
              </div>
            </a>
          </div>
        </div>
        @endforeach
      </div>
  </div>
@endsection

@push('script')
@endpush
