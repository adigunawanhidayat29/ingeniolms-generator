@extends('layouts.app')

@section('title', 'Degreee')

@section('content')
  <div class="container">
    <h1>Semua Degree</h1>
    <div class="row">
      @foreach($degrees as $degree)
        <div class="col-md-3">
          <div class="card">
            <a href="{{url('degree/'. $degree->slug)}}" class="zoom-img withripple">
              <img src="{{asset_url($degree->image)}}" alt="{{$degree->title}}" class="dq-image img-fluid">
            </a>
            <a href="{{url('degree/learn/'. $degree->slug)}}">
              <div class="card-block text-left">
                <h4 class="color-primary dq-max-title">{{$degree->title}}</h4>
              </div>
            </a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection
