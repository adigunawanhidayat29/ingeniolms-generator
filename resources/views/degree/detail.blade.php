@extends('layouts.app')

@section('title', $degree->title)

@push('style')
<style>
    .dq-project-bg{
      background-image: url(/img/project-bg.jpg);
      background-size: cover;
      background-position: top center;
      background-repeat: no-repeat;
    }
    .ms-collapse .card .card-header .card-title a.collapsed:after{
      content:none;
    }
    .ms-collapse .card .card-header .card-title a:after{
      content:none;
    }
    .dq-degree-bg{
      background:url(/img/unpad/perpustakaan1.jpg) no-repeat center;
      background-size: cover;
      width: 100%;
      height: 400px;
      position: relative;
      overflow: hidden;
    }
    .dq-overlay{
      position: absolute;
      top: 0;
      left:0;
      width:100%;
      height: 100%;
      background: rgba(0,0,0,0.7);
    }
  </style>

@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-7 color-dark">
        <h3>ONLINE DEGREE</h3>
        <h1 class="text-uppercase dq-title-caption">{{$degree->title}}</h1>
        <p style="font-size:20px;">
          {!!$degree->description!!}
        </p>
      </div>
      <div class="col-md-5 text-center mt-4">
        <a href="">
          <img src="/img/unpad/logo-unpad1.png" style="width:200px;">
        </a>
        <p class="mt-1"><a href="#" data-toggle="modal" data-target="{{Auth::check() ? '#DegreeCheckPassword' : '#ms-account-modal'}}" class="btn btn-raised btn-primary">Klik di sini</a></p>
      </div>
    </div>
  </div>

  <section class="mt-6" style="background:url({{asset_url($degree->image)}});background-position:center center;height:400px;background-size:100%;"></section>

  <div class="wrap">
    <div class="container mt-1">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <h1 class="text-left dq-title-caption color-dark">{{$degree->title}}</h1>
        </div>
        <div class="col-md-8 mx-auto">
          <div class="card">
            <div class="embed-responsive embed-responsive-16by9" style="position:relative;">
              @if($degree->introduction == "")
                <iframe class="embed-responsive-item" src="https://drive.google.com/file/d/1QWkD2cuhGojUgglivV25rgTB6jdRZfYe/preview" allowfullscreen="true"></iframe>
              @else
                <iframe class="embed-responsive-item" src="{{$degree->introduction.'/preview'}}" allowfullscreen="true"></iframe>
              @endif
              <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
            </div>
          </div>
          {{-- <video src="{{$degree->introduction}}"></video> --}}
          <div class="text-left mt-3 color-dark" style="font-size:20px;">
            <p>
              {!!$degree->introduction_description!!}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 mx-auto">
        <h1 class="text-center dq-title-caption color-dark">Kurikulum</h1>
        <div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="mb-0 card card-default">
            <div class="card-header" role="tab" id="headingOne">
              <h4 class="card-title ms-rotate-icon">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  <h1>Masters Degree</h1>
                  <h3 class="text-right">
                    {{count($courses)}} Module <i class="fa fa-caret-down"></i>
                  </h3>
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
              @foreach($courses as $index => $course)
                <div class="card-block color-dark">
                  <p>{{$index+1}}. {{$course->title}}</p>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="wrap mt-6" style="background:#f9f9f9;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="text-center dq-title-caption color-dark">Keunggulan Program</h1>
          {!!$degree->benefits!!}
        </div>
      </div>
    </div>
  </div>

  <div class="container-full dq-degree-bg">
    <div class="dq-overlay">
      <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <h1 class="text-center dq-title-caption color-white">Tim Pengembang</h1>
          <p class="color-white" style="font-size:20px;">
            {!!$degree->developer_team!!}
          </p>
          <div class="text-center">
            <img style="width:200px;" src="/img/unpad/logo-unpad1.png">
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 mx-auto color-dark mt-6 mb-6">
        <h1 class="text-center dq-title-caption">Prasyarat</h1>
        <p class="mt-3" style="font-size:20px;">
          {!!$degree->precondition!!}
        </p>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal" id="DegreeCheckPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id="DegreePassword" placeholder="Masukan Password">
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button id="CheckPassword" type="button" class="btn  btn-primary">Enroll</button>
              </div>
          </div>
      </div>
  </div>

@endsection

@push('script')
  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#DegreePassword").val();
      $.ajax({
        url : '/degree/check-password/{{$degree->id}}',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          if(response === 'true'){
            $.ajax({
              url : '/degree/enroll/{{$degree->id}}',
              type : 'post',
              data : {
                _token : '{{csrf_token()}}',
              },
              success : function(data){
                window.location.assign('/degree/learn/{{$degree->slug}}')
              }
            })
          }else{
            alert('wrong password');
          }
        }
      })
    })
  </script>
@endpush
