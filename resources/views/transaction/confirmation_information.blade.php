@extends('layouts.app')

@section('title', 'Confirmation Information')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title text-center">Payment Confirmation Information</h3>
      </div>
      <div class="panel-body text-center">
        <h3>Thank's</h3>
        <p class="alert alert-success">Thank you for making the payment, we will process your transaction for 2x24 hours.</p>
        <p>Please contact our customer service for further information.</p>
        <br><br>
        <a href="/" class="btn btn-primary btn-raised">Back to Courses</a>
    </div>
  </div>
@endsection
