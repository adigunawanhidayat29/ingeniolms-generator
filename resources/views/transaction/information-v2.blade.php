@extends('layouts.app')

@section('title', 'Payment Information')

@section('content')
  <div class="container">
    <h1>Please make a payment to complete the Transaction process.</h1>
    <strong>Your Order Number : {{$transaction->invoice}}</strong>
    <p>The amount you have to pay is equal <strong>{{rupiah($transaction->subtotal)}}</strong></p>
    <p> <a class="btn btn-danger btn-raised btn-sm" target="_blank" href="{{$transaction->method_details}}">Download payment instructions</a></p>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Item Detail</h3>
      </div>
      <div class="panel-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            @foreach($transactions_details as $transaction_detail)
              <tr>
                <td>
                  <p><strong>{{$transaction_detail->title}}</strong></p>
                  <p><img height="80" src="{{$transaction_detail->image}}" alt="{{$transaction_detail->title}}"></p>
                </td>
                <td>{{rupiah($transaction_detail->price)}}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <td>Subtotal</td>
              <td><p class="btn btn-primary btn-lg">{{rupiah($transaction->subtotal)}}</p></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <h3>Immediately do a payment confirmation if you have already made a payment.</h3>
    <a href="/transaction/payment/confirmation/{{$transaction->invoice}}" class="btn btn-primary btn-raised">Confirm Now</a>
  </div>
@endsection
