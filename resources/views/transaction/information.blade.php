@extends('layouts.app')

@section('title', 'Informasi Pembayaran')

@section('content')
  <div class="container">
    <h1>Silakan lakukan pembayaran untuk melengkapi proses Transaksi.</h1>
    <strong>Nomor Order Anda : {{$transaction->invoice}}</strong>
    <p>Jumlah yang harus Anda bayar adalah senilai <strong>{{rupiah($transaction->subtotal)}}</strong></p>
    <p>Silakan pilih bank yang akan Anda pakai di bawah ini: </p>
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <img height="30" src="/img/mandiri.png" alt="">
            <br><br>
            <p>No Rekening : 130-00-1331794-9</p>
            <p>Atas Nama : IDELEARNING</p>
          </div>
          {{-- <div class="col-md-4">
            <img height="30" src="https://upload.wikimedia.org/wikipedia/commons/9/97/Logo_BRI.png" alt="">
            <br><br>
            <p>No Rekening : 000 111 222 333</p>
            <p>Cabang : Kota Bandung</p>
            <p>Atas Nama : PT Dataquest</p>
          </div> --}}
          {{-- <div class="col-md-4"> --}}
            {{-- <img height="30" src="https://3.bp.blogspot.com/-e1fOq9uUk8M/V15O0WHiIMI/AAAAAAAAAJA/IpxPlLevxLsjisy2I625Yvz-eNzgc6xfgCKgB/s1600/Logo%2BBank%2BBNI%2BPNG.png" alt="">
            <br><br>
            <p>No Rekening : 000 111 222 333</p>
            <p>Cabang : Kota Bandung</p>
            <p>Atas Nama : PT Dataquest</p> --}}
          {{-- </div> --}}
          {{-- <div class="col-md-4"> --}}
            {{-- <img height="30" src="https://3.bp.blogspot.com/-ZK6W9UlA3lw/V15RGexr3yI/AAAAAAAAAJ4/nkyM9ebn_qg3_rQWyBZ1se5L_SSuuxcDACLcB/s1600/Bank_Central_Asia.png" alt="">
            <br><br>
            <p>No Rekening : 000 111 222 333</p>
            <p>Cabang : Kota Bandung</p>
            <p>Atas Nama : PT Dataquest</p> --}}
          {{-- </div> --}}
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Item Detail</h3>
      </div>
      <div class="panel-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Produk</th>
              <th>Harga</th>
            </tr>
          </thead>
          <tbody>
            @foreach($transactions_details as $transaction_detail)
              <tr>
                <td>
                  <p><strong>{{$transaction_detail->title}}</strong></p>
                  <p><img height="80" src="{{asset_url(course_image_small($transaction_detail->image))}}" alt="{{$transaction_detail->title}}"></p>
                </td>
                <td>{{rupiah($transaction_detail->price)}}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <td>Kode Pembayaran</td>
              <td><p class="btn btn-primary btn-lg">{{$transaction->unique_number}}</p></td>
            </tr>
            <tr>
              <td>Subtotal</td>
              <td><p class="btn btn-primary btn-lg">{{rupiah($transaction->subtotal)}}</p></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <h3>Segera lakukan konfirmasi pembayaran jika Anda sudah melakukan pembayaran.</h3>
    <a href="/transaction/payment/confirmation/{{$transaction->invoice}}" class="btn btn-primary btn-raised">Konfirmasi Sekarang</a>
  </div>
@endsection
