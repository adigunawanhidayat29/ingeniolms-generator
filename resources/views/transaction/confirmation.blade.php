@extends('layouts.app')

@section('title', 'Payment Confirmation')

@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Payment Confirmation</h3>
      </div>
      <div class="panel-body">
        {{Form::open(['url' => '/transaction/payment/confirmation_action/'.$invoice, 'method' => 'post' ,'files' => true])}}
          <div class="form-group">
            <label for="">Order Number</label>
            <input type="text" class="form-control" id="" name="invoice" placeholder="Order Number" value="{{$invoice}}" readonly>
          </div>
          <div class="form-group">
            <label for="">File/Image</label>
            <input type="file" class="form-control" id="" name="file" placeholder="File / Image" value="">
          </div>
          <div class="form-group">
            <label for="">Bank Name</label>
            <input type="text" class="form-control" id="" name="bank_name" placeholder="Bank Name" value="">
          </div>
          <div class="form-group">
            <label for="">Account Name</label>
            <input type="text" class="form-control" id="" name="account_name" placeholder="Account Name" value="">
          </div>
          <div class="form-group">
            <label for="">Account Number</label>
            <input type="text" class="form-control" id="" name="account_number" placeholder="Account Number" value="">
          </div>
          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary btn-raised" id="" value="Confirm Payment">
          </div>
        </div>
      {{Form::close()}}
    </div>
  </div>
@endsection
