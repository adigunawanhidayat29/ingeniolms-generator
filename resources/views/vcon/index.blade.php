@extends('layouts.app')

@section('title', Lang::get('front.vcon.title'))

@push('style')
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">  
@endpush

@push('meta')
  <meta name="description" content="Online Training bersama praktisinya Untuk Karir & Passionmu">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.vcon.header')</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.vcon.breadcrumb_home')</a></li>
          <li>@lang('front.vcon.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
  </div>
  <section class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img class="img-fluid" src="/img/group-video.svg" alt="">
        </div>
        <div class="col-md-6">
          @if(\Session::has('message'))
            <div class="alert alert-danger text-center">{{\Session::get('message')}}</div>  
          @endif

          <form class="form-horizontal" method="post" action="/vcon/store">
            {{csrf_field()}}
            <fieldset>
              <div class="form-group mt-0">
                <label class="control-label">@lang('front.vcon.select_room_label')</label>
                <select class="form-control selectpicker" name="meeting" required>
                  @foreach($Vcons as $Vcon)
                    <option value="{{$Vcon->id . "-" . $Vcon->title}}">{{$Vcon->title}}</option>                              
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="control-label">@lang('front.vcon.name_label')</label>
                <input type="text" class="form-control" id="inputEmail" name="name" placeholder="@lang('front.vcon.name_label')" required>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="control-label">@lang('front.vcon.password_label')</label>
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="@lang('front.vcon.password_label')" required>
              </div>
            </fieldset>
            <button type="submit" class="btn btn-raised btn-primary btn-block">@lang('front.vcon.join_button')</button>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection