@extends('layouts.app')

@section('title', 'Video Conference Menunggu Persetujuan')

@push('style')
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">  
@endpush

@push('meta')
  <meta name="description" content="Online Training bersama praktisinya Untuk Karir & Passionmu">
@endpush

@section('content')
    <div class="container mt-5" style="margin-top:120px !important">
        <div class="row justify-content-md-center">
          <div class="col-lg-6">
            <div class="card card-hero card-primary animated fadeInUp animation-delay-7">
              <div class="card-block">
                <h1 class="color-primary text-center">Video Conference - Menunggu Persetujuan</h1>

                  <div class="alert alert-warning text-center">
                    <strong>*Mohon untuk tidak menutup halaman ini*</strong>
                    <br>
                    Tunggu sebentar, Anda sedang dalam proses persetujuan, jika Anda sudah disetujui maka Halaman ini akan dialihkan secara otomatis.
                  </div>  

                  <div class="text-center">
                    <a href="" class="btn btn-primary btn-raised">Segarkan Halaman</a>
                  </div>

                         
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('script')
    <script>

        setInterval(function(){
          $.ajax({
                url : "/vcon/waiting/{{ \Request::segment(3) }}/check",
                type : "get",
                success : function(result){
                  console.log(result)
                  if (result != '404') {
                    window.location = result;
                  }
                },
              });
        }, 5000);
            
    </script>
@endpush