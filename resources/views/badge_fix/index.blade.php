@extends('layouts.app')

@section('title', Lang::get('front.badges.list_of_badges'))

@push('style')
    <style>
        .nav-tabs-ver-container .nav-tabs-ver {
            padding: 0;
            margin: 0;
        }

        .nav-tabs-ver-container .nav-tabs-ver:after {
            display: none;
        }
    </style>
@endpush

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="headline-md no-m">@lang('front.badges.list') <span>@lang('front.badges.badge')</span></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">@lang('front.badges.home')</a></li>
                    <li><a href="/course/dashboard">@lang('front.badges.manage_class')</a></li>
                    <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
                    <li>@lang('front.badges.list_of_badges')</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap pt-2 pb-2 mb-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-3 nav-tabs-ver-container">
					<img src="{{$course->image}}" alt="{{ $course->title }}" class="img-fluid mb-2">
					<div class="card no-shadow">
						<ul class="nav nav-tabs-ver" role="tablist">
							<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/atendee/{{$course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/attendance/{{$course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/grades/{{$course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
                            @if(isTeacher(Auth::user()->id) === true)
                                <li class="nav-item"><a class="nav-link active" href="/{{$course->id}}/list_badges"><i class="fa fa-shield"></i> @lang('front.badges.badge')</a></li>
                                @if($course->level_up == 1)
                                    <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/level_settings"><i class="fa fa-trophy"></i> @lang('front.badges.level')</a></li>
                                @endif
                            @endif
						</ul>
					</div>
				</div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active">
                            <a title="Badge" href="/{{$course->id}}/create_badge" class="btn btn-primary btn-raised no-m">
                                @lang('front.badges.create_badge')
                            </a>

                            <hr>

                            <table id="badgesData" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="width: 150px;">@lang('front.badges.badge_name')</th>
                                        {{-- <th>Description</th> --}}
                                        <th>@lang('front.badges.icon')</th>
                                        <th>@lang('front.badges.publish')</th>
                                        <th>@lang('front.badges.action')</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($badges as $no => $badge)
                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{$badge->name}}</td>
                                            {{-- <td>{!!$badge->description!!}</td> --}}
                                            <td><img src="{{ $badge->img != null ? '/storage/badge_content/'.$badge->img : '/img/inspiration.png' }}" style="width: 50px;"></td>
                                            <td>
                                                @if($badge->publish == 1)
                                                    <form method="post" action="/badge/unpublish/{{$badge->id}}">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="border: none;" onClick="return confirm('Unpublish this badge?')"><i class="fa fa-eye"></i></button>
                                                    </form>
                                                @else
                                                    <form method="post" action="/badge/publish/{{$badge->id}}">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="border: none;" onClick="return confirm('Publish this badge?')"><i class="fa fa-eye-slash"></i></button>
                                                    </form>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-raised btn-default m-0" data-toggle="modal" data-target="#badgeDetail{{$badge->id}}">@lang('front.badges.detail')</a>
                                                <a href="/{{$badge->course_id}}/{{$badge->id}}/badge_overview" class="btn btn-sm btn-raised btn-primary m-0">@lang('front.badges.overview')</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($badges as $no => $badge)
        <div id="badgeDetail{{$badge->id}}" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <p style="font-weight: 700; margin-bottom: 0;"><img src="{{ $badge->img != null ? '/storage/badge_content/'.$badge->img : '/img/inspiration.png' }}" style="height: 30px; margin-right: 1rem;"> {{ $badge->name }}</p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="zmdi zmdi-close"></i>
                                </span>
                            </button>
                        </div>
                        {!! $badge->description !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@push('script')
    <script>
        $(function () {
            $('#badgesData').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endpush