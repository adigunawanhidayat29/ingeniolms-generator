@extends('layouts.app')

@section('title', Lang::get('front.badges.overview'))

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="headline-md no-m">@lang('front.badges.badge') <span>@lang('front.badges.overview')</span></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">@lang('front.badges.home')</a></li>
                    <li><a href="/course/dashboard">@lang('front.badges.manage_class')</a></li>
                    <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
                    <li><a href="/{{$course->id}}/list_badges">@lang('front.badges.list_of_badges')</a></li>
                    <li>@lang('front.badges.overview') {{$badge->name}}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap pt-2 pb-2 mb-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="zmdi zmdi-close"></i>
                            </button>
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <div class="card-md">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#overview">@lang('front.badges.overview')</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#badgeCriteria">@lang('front.badges.badge_criteria')</a></li>
                        </ul>

                        <div class="card-block">
                            <div class="tab-content">
                                {{-- OVERVIEW --}}
                                <div id="overview" class="tab-pane fade active show">
                                    <div>
                                        {{-- <strong class="card-text">@lang('front.badges.badge_name'):</strong> --}}
                                        <p style="font-size: 24px; font-weight: 700; margin-bottom: 0;">{{$badge->name}}</p>
                                        {{-- <strong class="card-text">@lang('front.badges.badge_description'):</strong> --}}
                                        <p style="font-size: 16px; margin-bottom: 0;">{!!$badge->description!!}</p>
                                    </div>

                                    <hr style="margin: 2rem 0;">

                                    {{-- <div>
                                        <h4 class="card-title">@lang('front.badges.criteria')</h4>
                                    </div> --}}

                                    @if(count($badges_criteria) != 0)
                                        <p>
                                            @lang('front.badges.badge_awarded')
                                                @if($badge->complete_criteria == "any")
                                                    <strong>@lang('front.badges.any')</strong>
                                                @else
                                                    <p><strong>@lang('front.badges.all')</strong>
                                                @endif
                                            @lang('front.badges.of_criteria')
                                        </p>

                                        <h4>@lang('front.badges.criteria') :</h4>
                                        
                                        @foreach($badges_criteria as $no => $criteria)                                        

                                            <div style="margin-bottom: 2rem;">
                                                <p style="font-weight: 700; margin-bottom: 0;">{{$criteria->name}}</p>
                                                <p style="margin-bottom: 0;">{{$criteria->criteria_description}}</p>
                                            </div>

                                            @if($criteria->criteria_id == 2)
                                                @php
                                                    $contents = get_contents($criteria->badge_id);
                                                @endphp

                                                <div style="margin-bottom: 2rem;">
                                                    <p style="font-weight: 700; margin-bottom: 0;">@lang('front.badges.contents_on_completion')</p>                                                    
                                                    <ul style="margin-bottom: 0;">
                                                        @foreach($contents as $content)
                                                            <li>{{$content->content_name}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <p class="text-center mb-0">@lang('front.badges.criteria_not_set')</p>
                                    @endif
                                </div>

                                {{-- BADGE CRITERIA --}}
                                <div id="badgeCriteria" class="tab-pane fade">
                                    {{-- <h3>{{$badge->name}}</h3> --}}

                                    <form method="POST" action="/{{$course->id}}/{{$badge_id}}/criteria_assign" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div id="content-forms">
                                            <div class="form-group" style="margin: 0;">
                                                {{-- <label>Criteria</label> --}}
                                                <select class="form-control selectpicker" data-placeholder="@lang('front.badges.select_category')" name="criteria_id" style="width: 100%;">
                                                    @php
                                                        $criterias = get_criteria();
                                                    @endphp

                                                    <option>- @lang('front.badges.select_category') -</option>
                                                    @foreach($criterias as $criteria)
                                                        <option value="{{$criteria->id}}">{{$criteria->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-raised btn-primary">@lang('front.badges.submit')</button>
                                    </form>

                                    @if(count($badges_criteria) > 1)
                                        <hr style="margin: 2rem 0;">
                                        <p style="font-size: 16px; font-weight: 700;">@lang('front.badges.badge_awarded')</p>
                                        <form action="/{{$badge_id}}/complete_criteria/update" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group" style="margin: 0;">
                                                {{-- <label for="complete_criteria">@lang('front.badges.select_list'):</label> --}}
                                                <select class="form-control selectpicker" data-placeholder="@lang('front.badges.select_category')" id="complete_criteria" name="complete_criteria">
                                                    <option>- @lang('front.badges.select_type') -</option>
                                                    <option value="any">@lang('front.badges.any')</option>
                                                    <option value="all">@lang('front.badges.all')</option>
                                                </select>
                                            </div>

                                            <button class="btn btn-raised btn-primary" type="submit">@lang('front.badges.submit')</button>
                                        </form>
                                    @endif

                                    @if(count($badges_criteria) != 0)
                                        <hr style="margin: 2rem 0;">
                                        <p>
                                            @if($badge->complete_criteria == "any")
                                                <strong>@lang('front.badges.any')</strong>
                                            @else
                                                <strong>@lang('front.badges.all')</strong>
                                            @endif
                                            
                                            @lang('front.badges.of_criteria')
                                        </p>

                                        <h4>@lang('front.badges.criteria') :</h4>
                                    @endif

                                    
                                    @foreach($badges_criteria as $no => $criteria)
                                        <div>
                                            <div style="margin-bottom: 2rem;">
                                                <p style="font-weight: 700; margin-bottom: 0;">{{$criteria->name}}</p>
                                                <p style="margin-bottom: 0;">{{$criteria->criteria_description}}</p>
                                            </div>

                                            @if($criteria->criteria_id == 2)
                                                @php
                                                    $contents = get_contents($criteria->badge_id);
                                                @endphp

                                                <div style="margin-bottom: 2rem;">
                                                    <p style="font-weight: 700; margin-bottom: 0;">@lang('front.badges.contents_on_completion')</p>                                                    
                                                    <ul style="margin-bottom: 0;">
                                                        @foreach($contents as $content)
                                                            <li>{{$content->content_name}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach          
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection