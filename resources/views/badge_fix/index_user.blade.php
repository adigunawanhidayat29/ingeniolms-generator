@extends('layouts.app')

@section('title', 'List Badge')

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="headline-md no-m">@lang('front.badges.list') <span>@lang('front.badges.badge')</span></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">@lang('front.badges.home')</a></li>
                    <li><a href="/course/dashboard">@lang('front.badges.manage_class')</a></li>
                    <li><a href="/course/learn/{{$course->slug}}">{{$course->title}}</a></li>
                    <li>@lang('front.badges.list_of_badges')</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap pt-2 pb-2 mb-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table id="badgesData" class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>@lang('front.badges.badge_name')</th>
                                <th>@lang('front.badges.description')</th>
                                <th>@lang('front.badges.icon')</th>
                                <th>@lang('front.badges.action')</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($badges as $no => $badge)
                                <tr>
                                    <td>{{$no+1}}</td>
                                    <td>{{$badge->name}}</td>
                                    <td>{!!$badge->description!!}</td>
                                    <td>{{$badge->img}}</td>
                                    <td>
                                        <a href="/{{$badge->course_id}}/{{$badge->id}}/badge_overview" class="btn btn-raised btn-primary m-0">@lang('front.badges.overview')</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function () {
            $('#badgesData').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endpush