@extends('layouts.app')

@section('title', 'Create Badge Content')

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Create <span>Badge</span></h2>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/course/dashboard">Manage Class</a></li>
          <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
          <li>Create Badge</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="card-md">
            <div class="card-block">
              <form method="POST" action="/store_badge" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="badge_name">Badge Name</label>
                  <input type="text" class="form-control" name="name" id="badge_name">
                </div>
            
                <div class="form-group">
                  <label for="description">Description</label>
                  {{-- <input type="text" class="form-control" name="description" id="description"> --}}
                  <textarea name="description" id="description"></textarea>
                </div>
            
                <div class="form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img" id="badge_image">
                    <label class="custom-file-label" for="badge_image">Choose file</label>
                  </div>
                </div>
                
                <input type="hidden" name="course_id" value="{{$course_id}}">
            
                <button type="submit" class="btn btn-raised btn-primary">Save</button>
                <a href="{{ url('course/preview/5') }}" class="btn btn-raised btn-default">Cancel</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
  <script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    // CKEDITOR
    CKEDITOR.replace('description');
  </script>
@endpush