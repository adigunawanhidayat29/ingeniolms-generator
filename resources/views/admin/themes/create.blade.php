@extends('admin.layouts.app')

@section('title', 'theme Create')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>theme</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Create</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            
                            <a href="{{ url('/admin/themes') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br />
                            <br />

                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <form method="POST" action="{{ url('/admin/themes') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group {{ $errors->has('source') ? 'has-error' : ''}}">
                                    <div class="form-line">
                                        <label for="source" class="control-label">{{ 'Demo Preview (zip file)' }}</label>
                                        <input class="form-control" name="source" type="file" id="source" >
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('source_blade') ? 'has-error' : ''}}">
                                    <div class="form-line">
                                        <label for="source_blade" class="control-label">{{ 'Source Code (Blade Laravel)' }}</label>
                                        <input class="form-control" name="source_blade" type="file" id="source_blade" >
                                    </div>
                                </div>
                                
                                @include ('admin.themes.form', ['formMode' => 'create'])


                            </form>

                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

        </div>
        </div>
    </section>
    
@endsection
