<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="title" class="control-label">{{ 'Title' }}</label>
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($theme->title) ? $theme->title : ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="name" class="control-label">{{ 'Name' }}</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($theme->name) ? $theme->name : ''}}" >
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{-- <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="description" class="control-label">{{ 'Description' }}</label>
        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($theme->description) ? $theme->description : ''}}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}
{{-- <div class="form-group {{ $errors->has('thumbnail') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="thumbnail" class="control-label">{{ 'Thumbnail Theme' }}</label>
        <input class="form-control" name="thumbnail" type="file" id="thumbnail" value="{{ isset($theme->thumbnail) ? $theme->thumbnail : ''}}" >
        {!! $errors->first('thumbnail', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}
{{-- <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="status" class="control-label">{{ 'Status' }}</label>
        <input class="form-control" name="status" type="text" id="status" value="{{ isset($theme->status) ? $theme->status : ''}}" >
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
