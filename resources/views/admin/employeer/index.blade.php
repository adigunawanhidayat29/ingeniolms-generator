@extends('admin.layouts.app')

@section('title', 'Index Employeers')

@section('content')

<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>

			<div class="block-header">
				<h2>Employers</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div class="table-responsive">
								<div style="margin-bottom: 2rem;">
									<a href="{{url( \Request::route()->getPrefix() . '/employeers/create')}}" class="btn btn-primary">Add Employer</a>
								</div>

								<table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Nama Corporate</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($corporates as $no => $corporate)
                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{$corporate->name}}</td>
                                            <td><a href="/admin/corporate_users_employeer/create/{{$corporate->id}}" class="btn btn-primary">Add User</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection