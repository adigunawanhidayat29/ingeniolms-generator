@extends('admin.layouts.app')

@section('title')
    Promotions
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Promotions</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Lists</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <a href="{{url( \Request::route()->getPrefix() . '/promotions/create')}}" class="btn btn-primary">Create</a>
                                <br><br>
                                <center>
                                    @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        {!! Session::get('success') !!}
                                    </div>
                                    @elseif(Session::has('error'))
                                    <div class="alert alert-error alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        {!! Session::get('error') !!}
                                    </div>
                                    @endif
                                </center>
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                {{-- <th>Banner</th> --}}
                <th>Discount Code</th>
                <th>Discount</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

        </div>
        </div>
    </section>

@endsection

@push('script')
<script type="text/javascript">
    $(function () {
        var asset_url = '{{asset_url()}}';
        $("#data").DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url( \Request::route()->getPrefix() . "/promotions/serverside") }}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'discount_code', name: 'discount_code' },
                { data: 'discount', name: 'discount' },
                { data: 'start_date', name: 'start_date' },
                { data: 'end_date', name: 'end_date' },
                { data: 'status', name: 'status' },
                {
                    data: 'action',
                    'searchable': false,
                    'orderable': false
                }
            ]
        });
    });
</script>
@endpush