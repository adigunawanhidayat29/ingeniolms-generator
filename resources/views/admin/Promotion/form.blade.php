@extends('admin.layouts.app')

@section('title')
    Create Promotion
@endsection

@push('style')
    
@endpush

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Create Promotions</h2>
        </div>

        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        @php
                        if(Session::get('error')){
                        $errors = Session::get('error');
                        }
                        @endphp
                        {{ Form::open(array('url' => $action, 'method' => 'post', 'files' => true)) }}
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Name</label>
                            <input placeholder="" type="text" class="form-control" name="name" value="{{ $name }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Description</label>
                            <input placeholder="" type="text" class="form-control" name="description" value="{{ $description }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">File</label>
                            <input placeholder="" type="file" class="form-control" name="banner">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Discount Code</label>
                            <input placeholder="" type="text" class="form-control" name="discount_code" value="{{ $discount_code }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Discount</label>
                            <input placeholder="" type="text" class="form-control" name="discount" value="{{ $discount }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Start Date</label>
                            <input placeholder="" id="start_date" type="date" class="form-control" name="start_date" value="{{ $start_date }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                            <label for="">End Date</label>
                            <input placeholder="" id="end_date" type="date" class="form-control" name="end_date" value="{{ $end_date }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Status</label>
                            <select class="form-control" name="status" id="parent_Promotion">
                                <option {{$status == '1' ? 'selected' : ''}} value="1">Active</option>
                                <option {{$status == '0' ? 'selected' : ''}} value="0">Non Active</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Save">
                            <a href="{{ url('admin/promotion') }}" class="btn btn-default">Back</a>
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->
        </div>

    </div>
    </div>
</section>

@endsection

@push('script')
<script type="text/javascript" src="/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript">
    $("#start_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
    $("#end_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
  </script>
@endpush