@extends('admin.layouts.app')

@section('title', Lang::get('back.banner_default.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">

            <center>
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('success') !!}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('error') !!}
                </div>
                @endif
            </center>

            <div class="block-header">
                <h2>@lang('back.banner_default.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{url( \Request::route()->getPrefix() . '/banner/create')}}" class="btn btn-primary">@lang('back.banner_default.add_button')</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>@lang('back.banner_default.name_table')</th>
                                            <th>@lang('back.banner_default.image_table')</th>
                                            <th>@lang('back.banner_default.status_table')</th>
                                            <th>@lang('back.banner_default.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($banner as $item)
                                            <tr>
                                                {{-- <td>{{ $loop->iteration }}</td> --}}
                                                <td>{{ $item->name }}</td>
                                                <td><img src="{{ asset_url($item->image) }}" alt="" height="200"></td>
                                                <td>{{ $item->status == 1 ? Lang::get('back.banner_default.enabled_status') : Lang::get('back.banner_default.disabled_status')}}</td>
                                                <td>
                                                    {{-- <a href="{{ url('/admin/banner/' . $item->id) }}" class="btn btn-info btn-sm">
                                                        <i class="material-icons">info</i><span>Detail</span>
                                                    </a> --}}
                                                    <a href="{{ url('/admin/banner/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm">
                                                        <i class="material-icons">edit</i><span>@lang('back.banner_default.edit_button')</span>
                                                    </a>
                                                    <form method="POST" action="{{ url('/admin/banner' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="material-icons">delete</i><span>@lang('back.banner_default.delete_button')</span>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            {{-- <form method="GET" action="{{ url('/admin/banner') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="search"
                                                    placeholder="Search..." value="{{ request('search') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <button type="submit" class="btn btn-primary btn-sm m-l-15 waves-effect"><i
                                                class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>

                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>Name</th><th>Image</th><th>Status</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($banner as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td> <img src="{{ $item->image }}" alt="" height="200"> </td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <a href="{{ url('/admin/banner/' . $item->id) }}" title="View Banner"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                <a href="{{ url('/admin/banner/' . $item->id . '/edit') }}" title="Edit Banner"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                <form method="POST" action="{{ url('/admin/banner' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Banner" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $banner->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div> --}}

                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $(function () {        
            $("#data").DataTable();
        });
    </script>
@endpush