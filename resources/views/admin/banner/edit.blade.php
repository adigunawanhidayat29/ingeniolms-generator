@extends('admin.layouts.app')

@section('title', Lang::get('back.banner_default_form.update_title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.banner_default_form.update_header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <form method="POST" action="{{ url('/admin/banner/' . $banner->id) }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                @include ('admin.banner.form', ['formMode' => 'edit'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
