<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="name" class="control-label">@lang('back.banner_default_form.name_label')</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($banner->name) ? $banner->name : ''}}" placeholder="@lang('back.banner_default_form.name_label')">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="image" class="control-label">@lang('back.banner_default_form.image_label')</label>
        {{-- <input class="form-control" name="image" type="text" id="image" value="{{ isset($banner->image) ? $banner->image : ''}}" > --}}
        <input class="form-control" type="file" name="image" id="image">
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">@lang('back.banner_default_form.status_label')</label>
    @if( isset($banner->status) )
        <div class="form-group">
            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue" {{$banner->status == '1' ? 'checked' : ''}}>
            <label for="statusAktif">@lang('back.banner_default_form.enabled_radio')</label>
            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue" {{$banner->status == '0' ? 'checked' : ''}}>
            <label for="statusNonaktif">@lang('back.banner_default_form.disabled_radio')</label>
        </div>
    @else
        <div class="form-group">
            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue">
            <label for="statusAktif">@lang('back.banner_default_form.enabled_radio')</label>
            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue">
            <label for="statusNonaktif">@lang('back.banner_default_form.disabled_radio')</label>
        </div>
    @endif
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

{{-- <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="status" class="control-label">{{ 'Status' }}</label>
        <input class="form-control" name="status" type="text" id="status" value="{{ isset($banner->status) ? $banner->status : ''}}" >
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.banner_default_form.update_button') : Lang::get('back.banner_default_form.add_button') }}">
<a href="{{ url('admin/banner') }}" class="btn bg-blue-grey">@lang('back.banner_default_form.back_button')</a>
