<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="title" class="control-label">{{ 'Title' }}</label>
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($blast_notification->title) ? $blast_notification->title : ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="description" class="control-label">{{ 'Description' }}</label>
        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($blast_notification->description) ? $blast_notification->description : ''}}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="url" class="control-label">{{ 'Url' }}</label>
        <input class="form-control" name="url" type="text" id="url" value="{{ isset($blast_notification->url) ? $blast_notification->url : ''}}" >
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
