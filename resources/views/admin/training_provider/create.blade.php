@extends('admin.layouts.app')

@section('title')
    
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2></h2>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        @php
                            if(Session::get('error')){
                                $errors = Session::get('error');
                            }
                        @endphp
                        <form action="/admin/training_providers/store" method="post">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">Nama Training Provider</label>
                                    <input placeholder="Nama Training Provider" type="text" class="form-control" name="name" id="name" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection