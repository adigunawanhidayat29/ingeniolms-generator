@extends('admin.layouts.app')

@section('title', 'Index Employeers')

@section('content')

<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>

			<div class="block-header">
				<h2>Training Providers</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div class="table-responsive">
								<div style="margin-bottom: 2rem;">
									<a href="{{url( \Request::route()->getPrefix() . '/training_providers/create')}}" class="btn btn-primary">Add Training Provider</a>
								</div>

								<table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Nama Training Provider</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($training_providers as $no => $training_provider)
                                        <tr>
                                            <td>{{$no+1}}</td>
                                            <td>{{$training_provider->name}}</td>
                                            <td><a href="/admin/training_providers_trainer/create/{{$training_provider->id}}" class="btn btn-primary">Tambah User</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection