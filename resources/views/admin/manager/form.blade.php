@extends('admin.layouts.app')

@section('title')
	{{ 'manager' }}
@endsection


@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Manager </h2>
            </div>

						<div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Create</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
							@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => $action, 'method' => $method)) }}
							<div class="form-group">
								<label for="name">Name</label>
								<input placeholder="Name" type="text" class="form-control" name="name" value="{{ $name }}" required>
							</div>

							<div class="form-group">
								<label for="name">Email</label>
								<input placeholder="Email" type="email" class="form-control" name="email" value="{{ $email }}" required>
								@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
								@endif
							</div>

							@if($button == "Create")
								<div class="form-group">
									<label for="name">Password</label>
									<input placeholder="Password" type="password" class="form-control" name="password" value="{{ $password }}" required>
								</div>
							@endif

							<div class="form-group">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('manager') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

					</div>
			</div>
	</section>

@endsection
