@extends('admin.layouts.app')

@section('title', Lang::get('back.course_level_form.title'))

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
        <h2>@lang('back.course_level_form.header')</h2>
			</div>
			
			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp
							{{ Form::open(array('url' => $action, 'method' => $method)) }}
								<div class="form-group">
									<div class="form-line">
										<label for="title">@lang('back.course_level_form.name_label')</label>
										<input placeholder="@lang('back.course_level_form.name_label')" type="text" class="form-control" name="title" value="{{ $title }}" required>
									</div>
								</div>

								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button ? Lang::get('back.course_level_form.update_button') : '' , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/course-level') }}" class="btn bg-blue-grey">@lang('back.course_level_form.back_button')</a>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
