@extends('admin.layouts.app')

@section('title', Lang::get('back.course_level.title'))

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
        <h2>@lang('back.course_level.header')</h2>
      </div>

      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <center>
                @if(Session::has('success'))
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('success') !!}
                  </div>
                @elseif(Session::has('error'))
                  <div class="alert alert-error alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('error') !!}
                  </div>
                @endif
              </center>

              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                  <thead>
                    <tr>
                      <th></th>
                      <th>@lang('back.course_level.option_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('script')
  <script type="text/javascript">
    $(function(){
      $("#data").DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("admin/course-level/serverside") }}',
        columns: [
          { data: 'title', title: '@lang('back.course_level.name_table')' },
          { data: 'action', 'searchable': false, 'orderable':false }
        ]
      });
    });
  </script>
@endpush
