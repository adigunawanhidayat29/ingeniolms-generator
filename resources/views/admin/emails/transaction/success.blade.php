@component('mail::message')

  @slot('subheader')
    Pembayaran Berhasil #{{$Transaction->invoice}}
  @endslot

  <center>
  <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
    <tr>
      <td style="text-align:left;">
        <br>
        <strong>Hallo {{$Transaction->name}},</strong><br>
        <p>Pembayaran telah diterima. Selamat belajar dan nikmati fitur yang tersedia.</p>
      </td>
    </tr>
  </table>
  </center>

  <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
    <tr>
      <td style="text-align:left; padding:5px">
        <strong>Rincian transaksi:</strong><br>
      </td>
    </tr>
    @foreach($TransactionDetails as $TransactionDetail)
      <tr>
        <td style="text-align:left; padding:5px">
          <span>Produk</span>
        </td>
        <td style="text-align:left; padding:5px">
          <span>{{$TransactionDetail->title}}</span>
          <p><img height="60" src="{{asset_url(course_image_small($TransactionDetail->image))}}" alt="{{$TransactionDetail->title}}"></p>
          <p><a style="color: black" href="{{app_url('course/learn/' . $TransactionDetail->slug)}}">Mulai Belajar</a></p>
        </td>
      </tr>
      <tr>
        <td style="text-align:left; padding:5px">
          <span>Harga</span>
        </td>
        <td style="text-align:left; padding:5px">
          <span>{{rupiah($TransactionDetail->total)}}</span>
        </td>
      </tr>
    @endforeach
    <br>
    <tr style="border-top: 1px solid;">
      <td style="text-align:left; padding:5px">
        <span>Kode Pembayaran <small>(Hanya dibebankan kepada pembeli)</small></span>
      </td>
      <td style="text-align:right; padding:5px">
        <span>{{$Transaction->unique_number}}</span>
      </td>
    </tr>
    <tr>
      <td style="text-align:left;">
        <span>Total Pembayaran</span>
      </td>
      <td style="text-align:right; padding:5px">
        <span>{{rupiah($Transaction->subtotal)}}</span>
      </td>
    </tr>
  </table>

  <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
  <tr>
    <td style="text-align:center; margin:0 auto;">
      <br>
      <div>
        <a href="{{app_url('my-course')}}" style="background-color:#1E479D;color:#ffffff;display:inline-block;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;">MULAI BELAJAR</a></div>
      </td>
    </tr>
  </table>

@endcomponent
