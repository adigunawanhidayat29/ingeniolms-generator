@component('mail::message')

  @slot('subheader')
    Pengajar Ingenio
  @endslot

  <center>
  <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
    <tr>
      <td style="text-align:left;">
        <br>
        <strong>Hallo {{$Instructor->name}},</strong><br>
        <p>Selamat! Anda sudah terverifikasi menjadi pengajar di Ingenio. Silakan <a style="color:blue" href="https://ingenio.co.id/login">Masuk</a> untuk membuat kelas terbaik Anda.</p>
        <p>
          Untuk mempelajari langkah-langkah pembuatan kelas pertama Anda di Ingenio, kami rekomendasikan Anda untuk mengikuti kelas tutorial yang telah kami siapkan berikut ini : <br>
          <a style="color:blue" href="https://ingenio.co.id/course/persiapkan-karir-mengajar-anda-bersama-ingenio">https://ingenio.co.id/course/persiapkan-karir-mengajar-anda-bersama-ingenio</a>
          <br><br>
          Selamat Berkarya!
        </p>
      </td>
    </tr>
  </table>
  </center>
@endcomponent
