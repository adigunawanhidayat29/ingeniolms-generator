@component('mail::message')

  @slot('subheader')
    Aktivasi Akun
  @endslot

  <center>
  <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
    <tr>
      <td style="text-align:left;">
        <br>
        <strong>Hallo {{$name}},</strong><br>
        <p>Terimakasih sudah mendaftar. Segera lakukan aktivasi email Anda untuk bisa menikmati fitur yang tersedia.</p>
      </td>
    </tr>
  </table>
  </center>

  <table style="margin:0 auto;" cellspacing="0" cellpadding="10" width="100%">
  <tr>
    <td style="text-align:center; margin:0 auto;">
      <br>
      <div>
        <a href="{{url('/user/activation/'.$activation_token)}}"style="background-color:#1E479D;color:#ffffff;display:inline-block;font-family:'Source Sans Pro', Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;">Aktifkan Akun</a></div>
        <br>
      </td>
    </tr>
  </table>
@endcomponent
