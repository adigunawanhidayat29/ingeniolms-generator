@extends('admin.layouts.app')

@section('title')
	{{$button == 'Create' ? Lang::get('back.user_level_form.add_title') : Lang::get('back.user_level_form.update_title')}}
@endsection


@section('content')
	<section class="content">
		<div class="container-fluid">
            <div class="block-header">
                <h2>{{$button == 'Create' ? Lang::get('back.user_level_form.add_header') : Lang::get('back.user_level_form.update_header')}}</h2>
			</div>
			
			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp

							{{ Form::open(array('url' => $action, 'method' => $method)) }}
								<div class="form-group">
									<div class="form-line">
										<label for="name">Name</label>
										<input type="text" class="form-control" name="name" value="{{ $name }}" required>
										@if ($errors->has('name'))
											<span class="text-danger">{{ $errors->first('name') }}</span>
										@endif
									</div>
								</div>

								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button == 'Create' ? Lang::get('back.user_level_form.add_button') : Lang::get('back.user_level_form.update_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/user-levels') }}" class="btn bg-blue-grey">@lang('back.user_level_form.back_button')</a>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
