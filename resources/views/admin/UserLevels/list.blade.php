@extends('admin.layouts.app')

@section('title', Lang::get('back.user_level.title'))

@section('content')
  <section class="content">
    <div class="container-fluid">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>

      <div class="block-header">
        <h2>@lang('back.user_level.header')</h2>
      </div>

      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <div style="margin-bottom: 2rem;">
                <a href="{{url('admin/user-levels/create')}}" class="btn btn-primary">@lang('back.user_level.add_button') Level</a>
              </div>
  
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="data">
                  <thead>
                    <tr>
                      <th>@lang('back.user_level.name_table')</th>
                      <th>@lang('back.user_level.option_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('script')
  <script type="text/javascript">
    $(function(){
      $("#data").DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url("admin/user-levels/serverside") }}',
        columns: [
          { data: 'name', name: 'name' },
          { data: 'action', 'searchable': false, 'orderable':false }
        ]
      });
    });
  </script>
@endpush
