@extends('admin.layouts.app')

@section('title')
	{{$button == 'Create' ? Lang::get('back.user_teacher_form.add_user_title') : Lang::get('back.user_teacher_form.update_user_title')}}
@endsection

@push('style')
	<link rel="stylesheet" href="{{asset('select2/css/select2.min.css')}}">
	<link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>{{ $button == 'Create' ? Lang::get('back.user_teacher_form.add_user_header') : Lang::get('back.user_teacher_form.update_user_header') }}</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp
							{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
								<div class="form-group">
									<div class="form-line">
										<label for="name">@lang('back.user_teacher_form.username_label')</label>
										<input placeholder="@lang('back.user_teacher_form.username_label')" type="text" class="form-control" name="username" value="{{ $username }}" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="name">@lang('back.user_teacher_form.name_label')</label>
										<input placeholder="@lang('back.user_teacher_form.name_label')" type="text" class="form-control" name="name" value="{{ $name }}" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="name">@lang('back.user_teacher_form.email_label')</label>
										<input placeholder="@lang('back.user_teacher_form.email_label')" type="email" class="form-control" name="email" value="{{ $email }}" required>
										@if ($errors->has('email'))
											<span class="text-danger">{{ $errors->first('email') }}</span>
										@endif
									</div>
								</div>

								@if($button == "Create")
									<div class="form-group">
										<div class="form-line">
											<label for="name">@lang('back.user_teacher_form.password_label')</label>
											<input placeholder="@lang('back.user_teacher_form.password_label')" type="password" class="form-control" name="password" value="{{ $password }}" required>
										</div>
									</div>
								@endif

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.slogan_label')</label>
										<input placeholder="@lang('back.user_teacher_form.slogan_label')" type="text" class="form-control" name="tagline" value="{{ $tagline }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.short_description_label')</label>
										{{-- <textarea class="form-control" name="short_description">{{ $short_description }}</textarea> --}}
										{{-- <input placeholder="Deskripsi Pendek" type="text" class="form-control" name="short_description" value="{{ $short_description }}"> --}}
										<textarea class="summernote" cols="30" rows="10" name="short_description">{{ $short_description }}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.description_label')</label>
										<textarea class="form-control summernote" name="description">{{ $description }}</textarea>
										{{-- <input placeholder="Deskripsi" type="text" class="form-control" name="description" value="{{ $description }}"> --}}
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.facebook_label')</label>
										<input placeholder="@lang('back.user_teacher_form.facebook_label')" type="text" class="form-control" name="facebook" value="{{ $facebook }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.twitter_label')</label>
										<input placeholder="@lang('back.user_teacher_form.twitter_label')" type="text" class="form-control" name="twitter" value="{{ $twitter }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label>@lang('back.user_teacher_form.instagram_label')</label>
										<input placeholder="@lang('back.user_teacher_form.instagram_label')" type="text" class="form-control" name="instagram" value="{{ $instagram }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="name">@lang('back.user_teacher_form.image_label')</label>
										<input placeholder="@lang('back.user_teacher_form.image_label')" type="file" class="form-control" name="image">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="name">@lang('back.user_teacher_form.teacher_community_label')</label>
										<select class="form-control" name="community_id" id="community_id">
											<option value="">@lang('back.user_teacher_form.teacher_community_select')</option>
											@foreach($communities as $data)
												<option {{ $data->id == $community_id ? 'selected' : '' }} value="{{$data->id}}">{{$data->name}}</option>
											@endforeach()
										</select>
									</div>
								</div>

								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button == 'Create' ? Lang::get('back.user_teacher_form.add_user_button') : Lang::get('back.user_teacher_form.update_user_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/instructor') }}" class="btn bg-blue-grey">@lang('back.user_teacher_form.back_button')</a>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description', {
			filebrowserBrowseUrl: "{{asset('/ckeditor/browser?type=Images')}}",
      		filebrowserUploadUrl: "{{asset('/ckeditor/uploader?command=QuickUpload&type=Images')}}",
		});
		CKEDITOR.replace('short_description', {
			filebrowserBrowseUrl: "{{asset('/ckeditor/browser?type=Images')}}",
      		filebrowserUploadUrl: "{{asset('/ckeditor/uploader?command=QuickUpload&type=Images')}}",
		});
	</script>
	{{-- CKEDITOR --}}

	<script src="{{asset('summernote/summernote.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
            height: 300
        });
	});
	</script>

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{asset('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
		$("#community_id").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
