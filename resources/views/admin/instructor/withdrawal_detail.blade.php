@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Withdrawal Detail' }}
@endsection


@section('main-content')
  <div class="container-fluid spark-screen">
		<div class="box">
			<div class="box-body">
				<h3>Detail permintaan penarikan</h3>
		    <table class="table">
		      <tr>
		        <td>Pengajar</td><td>{{$withdrawal->name}}</td>
					</tr>
					<tr>
		        <td>Tanggal</td><td>{{$withdrawal->created_at}}</td>
					</tr>
					<tr>
		        <td>Nominal</td><td>{{$withdrawal->amount}}</td>
					</tr>
					<tr>
		        <td>Status</td><td>{{$withdrawal->status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
					</tr>
					<tr>
		        <td>Bank</td><td>{{$instructor_bank->bank_name}}</td>
					</tr>
					<tr>
		        <td>Atas Nama</td><td>{{$instructor_bank->account_name}}</td>
					</tr>
					<tr>
		        <td>Nomor Rekening</td><td>{{$instructor_bank->account_number}}</td>
					</tr>
					<tr>
		        <td>Bank Cabang</td><td>{{$instructor_bank->bank_branch}}</td>
					</tr>
		    </table>
				<br>
				<a class="btn btn-default" href="/instructor/withdrawals">Kembali</a>
			</div>
		</div>
  </div>
@endsection
