@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Withdrawal' }}
@endsection


@section('main-content')
  <div class="container-fluid spark-screen">

    <div class="box">
			<div class="box-body">

        @if(Session::get('message'))
          <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif

        <h3>Permintaan penarikan</h3>
        <table class="table">
          <tr>
            <th>Pengajar</th>
            <th>Tanggal</th>
            <th>Nominal</th>
            <th>Status</th>
            <th>#</th>
          </tr>
          @foreach($withdrawals as $withdrawal)
            <tr>
              <td>{{$withdrawal->name}}</td>
              <td>{{$withdrawal->created_at}}</td>
              <td>{{$withdrawal->amount}}</td>
              <td>{{$withdrawal->status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
              <td>
                {{-- <a onclick="return confirm('Yakin akan menghapus data?')" href="/instructor/transactions/withdrawal/delete/{{$withdrawal->id}}">Hapus</a> --}}
                @if($withdrawal->status == '0')
                  <a onclick="return confirm('Yakin akan menerima permintaan penarikan?')" href="/instructor/withdrawals/approve/{{$withdrawal->id}}">Terima</a>
                @endif
                  <a href="/instructor/withdrawals/detail/{{$withdrawal->id}}">Lihat detail</a>
              </td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
@endsection
