@extends('admin.layouts.app')

@section('title', Lang::get('back.user_teacher.title'))


@section('content')
  <section class="content">
    <div class="container-fluid">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>
      
      <div class="block-header">
        <h2>@lang('back.user_teacher.header')</h2>
      </div>

      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <div class="table-responsive">
                <div style="margin-bottom: 2rem;">
                  <a href="{{url( \Request::route()->getPrefix() . '/instructor/create')}}" class="btn btn-primary">@lang('back.user_teacher.add_user_button')</a>
                </div>
                
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.user_teacher.name_table')</th>
                      <th>@lang('back.user_teacher.email_table')</th>
                      <th>@lang('back.user_teacher.photo_table')</th>
                      <th>@lang('back.user_teacher.status_table')</th>
                      <th>@lang('back.user_teacher.option_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
@endsection

@push('script')
  <script type="text/javascript">
    $(function(){
      $("#data").DataTable({
        "order": [[ 0, "desc" ]],
        processing: true,
        serverSide: true,
        ajax: '{{ url("admin/instructor/serverside") }}',
        columns: [
          { data: 'id', name: 'id', "visible": false },
          { data: 'name', name: 'name' },
          { data: 'email', name: 'email' },
          {
            "data": "photo",
            "render": function(data, type, row) {
              return '<img src="'+data+'" width=\"100\"//>';
            },
          },
          { data: 'status', name: 'status' },
          { data: 'action', 'searchable': false, 'orderable':false }
        ]
      });
    });
  </script>
@endpush
