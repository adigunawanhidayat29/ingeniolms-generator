@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Promotion' }}
@endsection

@push('style')
	<link rel="stylesheet" href="/jquery-ui/jquery-ui.min.css">
@endpush

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Promotion Create</h3>
					</div>
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => $action, 'method' => 'post', 'files' => true)) }}
							<div class="form-group">
								<label for="">Name</label>
								<input placeholder="" type="text" class="form-control" name="name" value="{{ $name }}" required>
							</div>

							<div class="form-group">
								<label for="">Description</label>
								<input placeholder="" type="text" class="form-control" name="description" value="{{ $description }}">
							</div>

							<div class="form-group">
								<label for="">File</label>
								<input placeholder="" type="file" class="form-control" name="banner">
							</div>

							<div class="form-group">
								<label for="">Discount Code</label>
								<input placeholder="" type="text" class="form-control" name="discount_code" value="{{ $discount_code }}">
							</div>

							<div class="form-group">
								<label for="">Discount</label>
								<input placeholder="" type="text" class="form-control" name="discount" value="{{ $discount }}">
							</div>

							<div class="form-group">
								<label for="">Start Date</label>
								<input placeholder="" id="start_date" type="text" class="form-control" name="start_date" value="{{ $start_date }}">
							</div>

							<div class="form-group">
								<label for="">End Date</label>
								<input placeholder="" id="end_date" type="text" class="form-control" name="end_date" value="{{ $end_date }}">
							</div>


							<div class="form-group">
								<label for="">Status</label>
								<select class="form-control" name="status" id="parent_Promotion">
									<option {{$status == '1' ? 'selected' : ''}} value="1">Active</option>
									<option {{$status == '0' ? 'selected' : ''}} value="0">Non Active</option>
								</select>
							</div>

							<div class="form-group">
								<input type="submit" class="btn btn-primary" value="Save">
								<a href="{{ url('Promotion') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript" src="/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript">
    $("#start_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
    $("#end_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
  </script>

@endpush
