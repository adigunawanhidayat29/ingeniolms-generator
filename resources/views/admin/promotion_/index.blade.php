@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Promotion' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Promotion List</h3>
            <br><br>
            <a href="{{url('promotion/create')}}" class="btn btn-primary">Create</a>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <table class="table table-hover" id="data">
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Banner</th>
                <th>Discount Code</th>
                <th>Discount</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
							@foreach ($promotions as $index => $data)
								<tr>
									<td>{{$index + 1}}</td>
									<td>{{$data->name}}</td>
									<td> <img src="{{asset_url($data->banner)}}" alt=""> </td>
									<td>{{$data->discount_code}}</td>
									<td>{{$data->discount}}%</td>
									<td>{{$data->start_date}}</td>
									<td>{{$data->end_date}}</td>
									<td>{{$data->status == '1' ? 'Active' : 'Non Active'}}</td>
									<td>
										<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
												Pilihan
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="">
												<li> <a href="/promotion/edit/{{$data->id}}">Edit</a> </li>
												<li> <a onclick="retun confirm('Yakin?')" href="/promotion/delete/{{$data->id}}">Hapus</a> </li>
										  </ul>
										</div>
									</td>
								</tr>
							@endforeach
            </table>
						{{$promotions->render()}}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')

@endpush
