<div class="form-group {{ $errors->has('level_id') ? 'has-error' : ''}}">
    <label for="level_id" class="control-label">@lang('back.user_level_permission.level_label')</label>
    <div class="form-group">
        @foreach($levels as $level)
            <input class="with-gap radio-col-blue" name="level_id" type="radio" id="level{{$level->id}}" value="{{$level->id}}" {{$level->id == Request::get('level') ? 'checked' : ''}}>
            <label for="level{{$level->id}}">{{$level->name}}</label>
        @endforeach
    </div>
    {!! $errors->first('level_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('permission_id') ? 'has-error' : ''}}">
    <label for="permission_id" class="control-label">@lang('back.user_level_permission.permission_label')</label>
    <div class="form-group">
        @foreach($permissions as $permission)
            <input class="filled-in chk-col-light-blue" @foreach($LevelHasPermissions as $LevelHasPermission) {{$LevelHasPermission->permission_id == $permission->id ? 'checked' : ''}}  @endforeach type="checkbox" class="filled-in" id="permission{{$permission->id}}" name="permission_id[]" value="{{$permission->id}}">
            <label for="permission{{$permission->id}}">{{$permission->name}}</label>
        @endforeach
    </div>
    {!! $errors->first('permission_id', '<p class="help-block">:message</p>') !!}
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.user_level_permission.update_button') : Lang::get('back.user_level_permission.add_button') }}">
<a href="{{ url('/admin/user-levels') }}" class="btn bg-blue-grey">@lang('back.user_level_permission.back_button')</a>
