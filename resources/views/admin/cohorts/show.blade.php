@extends('admin.layouts.app')

@section('title', Lang::get('back.cohort_detail.title'))

@push('style')  
  <link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
@endpush

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.cohort_detail.header') - {{ $cohort->name }}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/cohorts') }}"><i class="material-icons">arrow_back</i><span>@lang('back.cohort_detail.back_button')</span></a>
                                <a class="btn btn-warning btn-sm" href="{{ url('/admin/cohorts/' . $cohort->id . '/edit') }}"><i class="material-icons">edit</i><span>@lang('back.cohort_detail.edit_button')</span></a>
                                <form method="POST" action="{{ url('admin/cohorts' . '/' . $cohort->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.cohort_detail.delete_button')</span></button>
                                </form>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 700; border-top: 0;">#</td>
                                            <td style="border-top: 0;">{{ $cohort->id }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.cohort_detail.name_table')</td>
                                            <td>{{ $cohort->name }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.cohort_detail.created_table')</td>
                                            <td>{{ $cohort->created_by }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700; border-bottom: 0;">@lang('back.cohort_detail.status_table')</td>
                                            <td style="border-bottom: 0;">{{ $cohort->status }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem">
                                <a href="#" data-toggle="modal" data-target="#addCohortUser{{ $cohort->id }}" class="btn btn-primary">@lang('back.cohort_detail.add_user_button')</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.cohort_detail.user_name_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cohort->cohort_user as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="addCohortUser{{ $cohort->id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">@lang('back.cohort_detail.add_user_modal') - {{ $cohort->name }}</h4>
                </div>
                <form action="/admin/cohorts/add-user/{{ $cohort->id }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <select name="user_id[]" id="duallist{{ $cohort->id }}" multiple>
                            @foreach ($users as $user)  
                                <option @foreach($cohort->cohort_user as $cohort_user) @if($user->id == $cohort_user->user_id)selected="selected"@endif @endforeach value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">@lang('back.cohort_detail.submit_button')</button>
                        <button type="button" class="btn bg-blue-grey" data-dismiss="modal">@lang('back.cohort_detail.close_button')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    
    <script>
        var demo1 = $('#duallist{{ $cohort->id }}').bootstrapDualListbox({
            nonSelectedListLabel: '@lang("back.cohort.not_selected_label")',
            selectedListLabel: '@lang("back.cohort.selected_label")',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true
        });

        $(function () {
            demo1.trigger('bootstrapDualListbox.refresh' , true);
        });
    </script>
@endpush