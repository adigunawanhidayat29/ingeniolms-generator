@extends('admin.layouts.app')

@section('title', Lang::get('back.cohort.title'))

@push('style')  
  <link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
@endpush

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.cohort.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/admin/cohorts/create') }}" class="btn btn-primary btn-sm" title="Add New Cohort">@lang('back.cohort.create_button')</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.cohort.name_table')</th>
                                            <th>@lang('back.cohort.code_table')</th>
                                            <th>@lang('back.cohort.status_table')</th>
                                            <th>@lang('back.cohort.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cohorts as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->status == '1' ? Lang::get('back.cohort.enabled_status') : Lang::get('back.cohort.disabled_status') }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#addCohortUser{{ $item->id }}"><i class="material-icons">add</i><span>@lang('back.cohort.add_user_button')</span></a>
                                                    <a class="btn btn-info btn-sm" href="{{ url('/admin/cohorts/' . $item->id) }}"><i class="material-icons">info</i><span>@lang('back.cohort.detail_button')</span></a>
                                                    <a class="btn btn-warning btn-sm" href="{{ url('/admin/cohorts/' . $item->id . '/edit') }}"><i class="material-icons">edit</i><span>@lang('back.cohort.edit_button')</span></a>

                                                    <form method="POST" action="{{ url('/admin/cohorts' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.cohort.delete_button')</span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $cohorts->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @foreach ($cohorts as $item)
        <div class="modal fade" id="addCohortUser{{ $item->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">@lang('back.cohort.add_user_modal') - {{ $item->name }}</h4>
                    </div>
                    <form action="/admin/cohorts/add-user/{{ $item->id }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <select name="user_id[]" id="duallist{{ $item->id }}" multiple>
                                @foreach ($users as $user)  
                                    <option @foreach($item->cohort_user as $cohort_user) @if($user->id == $cohort_user->user_id)selected="selected"@endif @endforeach value="{{ $user->id }}">{{ $user->name }}-{{ $user->email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">@lang('back.cohort.submit_button')</button>
                            <button type="button" class="btn bg-blue-grey" data-dismiss="modal">@lang('back.cohort.close_button')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@push('script')
    <script src="/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    
    @foreach ($cohorts as $item)
        <script>
            $('#duallist{{ $item->id }}').bootstrapDualListbox({
                nonSelectedListLabel: '@lang("back.cohort.not_selected_label")',
                selectedListLabel: '@lang("back.cohort.selected_label")',
                preserveSelectionOnMove: 'moved',
                moveOnSelect: false
            });
        </script>
    @endforeach

    <script type="text/javascript">
        $(function(){
            var asset_url = '{{asset_url()}}';
            $("#data").DataTable();
        });
    </script>
@endpush