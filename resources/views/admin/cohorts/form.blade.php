<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="name" class="control-label">@lang('back.cohort_form.name_label')</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($cohort->name) ? $cohort->name : ''}}" placeholder="@lang('back.cohort_form.name_label')">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="code" class="control-label">@lang('back.cohort_form.code_label')</label>
        <input class="form-control" name="code" type="text" id="code" value="{{ isset($cohort->code) ? $cohort->code : ''}}" placeholder="@lang('back.cohort_form.code_label')">
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{-- <div class="form-group {{ $errors->has('created_by') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="created_by" class="control-label">{{ 'Created By' }}</label>
        <input class="form-control" name="created_by" type="text" id="created_by" value="{{ isset($cohort->created_by) ? $cohort->created_by : ''}}" >
        {!! $errors->first('created_by', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="status" class="control-label">@lang('back.cohort_form.status_label')</label>
        <select name="status" class="form-control" id="status" >
            @foreach (json_decode('{"1": "'. Lang::get('back.cohort_form.enabled_select') .'", "0": "'. Lang::get('back.cohort_form.disabled_select') .'"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($cohort->status) && $cohort->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.cohort_form.update_button') : Lang::get('back.cohort_form.create_button') }}">
