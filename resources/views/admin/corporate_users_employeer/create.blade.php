@extends('admin.layouts.app')

@section('title')
    
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2></h2>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        @php
                            if(Session::get('error')){
                                $errors = Session::get('error');
                            }
                        @endphp
                        <form action="/admin/corporate_users_employeer/store" method="post">
                        {{ csrf_field() }}
                            <input type="hidden" class="form-control" name="idemployeer" value="{{$idemployeer}}" required>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">Nama User</label>
                                    <input placeholder="Nama User" type="text" class="form-control" name="name" id="name" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <label for="email">Email</label>
                                    <input placeholder="Email" type="email" class="form-control" name="email" id="email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <label for="username">Username</label>
                                    <input placeholder="Username" type="text" class="form-control" name="username" id="username" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <label for="password">Password</label>
                                    <input placeholder="Password" type="password" class="form-control" name="password" id="password" required>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection