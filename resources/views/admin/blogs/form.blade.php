@push('style')
	<!-- include summernote css/js -->
	<link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="title" class="control-label">@lang('back.blog_form.title_label')</label>
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($blog->title) ? $blog->title : ''}}" placeholder="@lang('back.blog_form.title_label')">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('body') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="body" class="control-label">@lang('back.blog_form.content_label')</label>
        <textarea class="form-control summernote" rows="5" name="body" type="textarea" id="body" >{{ isset($blog->body) ? $blog->body : ''}}</textarea>
        {!! $errors->first('body', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <div>
        <img height="100" src="{{ isset($blog->image) ? asset_url($blog->image) : ''}}" alt="">
    </div>
    <div class="form-line">
        <label for="image" class="control-label">@lang('back.blog_form.thumbnail_label')</label>
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($blog->image) ? $blog->image : ''}}" >
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.blog_form.update_button') : Lang::get('back.blog_form.create_button') }}">

@push('script')
	<script src="{{asset('summernote/summernote.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
            height: 300
        });
	});
	</script>
@endpush