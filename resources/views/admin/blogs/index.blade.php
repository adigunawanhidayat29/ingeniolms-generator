@extends('admin.layouts.app')

@section('title', Lang::get('back.blog.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <center>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('success') !!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-error alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('error') !!}
                    </div>
                @endif
            </center>

            <div class="block-header">
                <h2>@lang('back.blog.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/admin/blogs/create') }}" class="btn btn-primary btn-sm" title="Add New Blog">@lang('back.blog.create_button')</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.blog.title_table')</th>
                                            <th>@lang('back.blog.content_table')</th>
                                            <th>@lang('back.blog.thumbnail_table')</th>
                                            <th>@lang('back.blog.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($blogs as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td><td>{{ $item->body }}</td>
                                                <td> <img src="{{ asset_url($item->image) }}" alt="" height="50"> </td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" href="{{ url('/admin/blogs/' . $item->id) }}">
                                                        <i class="material-icons">info</i><span>@lang('back.blog.detail_button')</span>
                                                    </a>

                                                    <a class="btn btn-warning btn-sm" href="{{ url('/admin/blogs/' . $item->id . '/edit') }}">
                                                        <i class="material-icons">edit</i><span>@lang('back.blog.edit_button')</span>
                                                    </a>

                                                    <form method="POST" action="{{ url('/admin/blogs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="material-icons">delete</i><span>@lang('back.blog.delete_button')</span>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $blogs->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function(){
            var asset_url = '{{asset_url()}}';
            $("#data").DataTable();
        });
    </script>
@endpush
