@extends('admin.layouts.app')

@section('title', Lang::get('back.blog_detail.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.blog_detail.header') - {{ $blog->title }}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/blogs') }}"><i class="material-icons">arrow_back</i><span>@lang('back.blog_detail.back_button')</span></a>
                                <a class="btn btn-warning btn-sm" href="{{ url('/admin/blogs/' . $blog->id . '/edit') }}"><i class="material-icons">edit</i><span>@lang('back.blog_detail.edit_button')</span></a>
                                <form method="POST" action="{{ url('admin/blogs' . '/' . $blog->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.blog_detail.delete_button')</span></button>
                                </form>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover" style="margin-bottom: 0;">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 700; border-top: 0;">#</td>
                                            <td style="border-top: 0;">{{ $blog->id }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.blog_detail.title_table')</td>
                                            <td>{{ $blog->title }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.blog_detail.content_table')</td>
                                            <td>{{ $blog->body }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700; border-bottom: 0;">@lang('back.blog_detail.thumbnail_table')</td>
                                            <td style="border-bottom: 0;"><img src="{{ asset_url($blog->image) }}" alt="" height="250"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
