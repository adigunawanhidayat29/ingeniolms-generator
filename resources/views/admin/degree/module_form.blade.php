@extends('admin.layouts.app')

@section('title')
	Degree Add Module
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default bx -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Degree Add Module Create</h3>
					</div>
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => '/degree/module/store/'.Request::segment(4), 'method' => 'post')) }}

							{{-- <div class="form-group">
								<label for="">Semester</label>
								<input type="text" class="form-control" name="semester" value="">
							</div> --}}

							<div class="form-group">
								<label for="">Add Modules</label>
								<select class="form-control" name="course_id[]" id="course_id" multiple required>
									@foreach($courses as $course)
										<option value="{{$course->id}}">{{$course->title}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								{{  Form::submit('Save' , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('degree') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}

						<h3>Course On Degree</h3>
						<table class="table">
							<tr>
								<th>Course</th>
								{{-- <th>Semester</th> --}}
								<th>Option</th>
							</tr>
							@foreach($DegreeCourses as $DegreeCourse)
								<tr>
									<td>{{$DegreeCourse->title}}</td>
									{{-- <td>{{$DegreeCourse->semester}}</td> --}}
									<td>
										<a onclick="return confirm('Are you sure?')" href="/degree/module/remove/{{$DegreeCourse->degree_course_id}}">Remove Module</a>
									</td>
								</tr>
							@endforeach
						</table>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#course_id").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
