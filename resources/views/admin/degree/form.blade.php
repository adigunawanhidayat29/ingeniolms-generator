@extends('admin.layouts.app')

@section('title')
	{{ 'Degree' }}
@endsection

@section('content')

<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Courses</h2>
            </div>

						<div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Form</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">

                            {{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
						
							<div class="form-group">
								<label for="">Level</label>
								<select class="form-control" name="degree_level_id" required>
									<option value="">Choose Degree Level</option>
									@foreach($degree_levels as $degree_level)
										<option {{$degree_level->id == $degree_level_id ? 'selected' : ''}} value="{{$degree_level->id}}">{{$degree_level->title}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label for="">Title</label>
								<input placeholder="Title" type="text" class="form-control" name="title" value="{{ $title }}" required>
							</div>

							<div class="form-group">
								<label for="">Password</label>
								<input placeholder="Password" type="text" class="form-control" name="password" value="{{ $password }}" required>
							</div>

							<div class="form-group">
								<label for="title">Description</label>
								<textarea name="description" id="description">{!! $description !!}</textarea>
							</div>

							<div class="form-group">
								<label for="">Image</label>
								<input placeholder="Image" type="file" class="form-control" name="image">
							</div>

							<div class="form-group">
								<label for="">Video Introduction</label>
								<input placeholder="Video Introduction" type="file" class="form-control" name="introduction">
							</div>

							<div class="form-group">
								<label for="title">Introduction Description</label>
								<textarea name="introduction_description" id="introduction_description">{!! $introduction_description !!}</textarea>
							</div>

							<div class="form-group">
								<label for="title">Benefits</label>
								<textarea name="benefits" id="benefits">{!! $introduction_description !!}</textarea>
							</div>

							<div class="form-group">
								<label for="title">Developer Team</label>
								<textarea name="developer_team" id="developer_team">{!! $developer_team !!}</textarea>
							</div>

							<div class="form-group">
								<label for="title">Precondition</label>
								<textarea name="precondition" id="precondition">{!! $precondition !!}</textarea>
							</div>

							<div class="form-group">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('degree') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}
                            
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

					</div>
			</div>
	</section>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script type="text/javascript">
		CKEDITOR.replace('description');
		CKEDITOR.replace('introduction_description');
		CKEDITOR.replace('benefits');
		CKEDITOR.replace('developer_team');
		CKEDITOR.replace('precondition');
	</script>
	{{-- CKEDITOR --}}
@endpush
