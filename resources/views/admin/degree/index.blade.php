@extends('admin.layouts.app')

@section('title')
	{{ 'Degree' }}
@endsection


@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Degree</h2>
            </div>

						<div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Lists</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">																
																<a href="{{url( \Request::route()->getPrefix() . '/degree/create')}}" class="btn btn-primary">Create</a>
																<br><br>
																<center>
										              @if(Session::has('success'))
										                <div class="alert alert-success alert-dismissible">
										                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										                  {!! Session::get('success') !!}
										                </div>
										              @elseif(Session::has('error'))
										                <div class="alert alert-error alert-dismissible">
										                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										                  {!! Session::get('error') !!}
										                </div>
										              @endif
										            </center>
																<table class="table table-bordered table-striped table-hover">
																	<tr>
																		<th>Title</th>
																		<th>Description</th>
																		<th>Level</th>
																		<th>Action</th>
																	</tr>
																	@foreach($degrees as $degree)
																		<tr>
																			<td>{{$degree->title}}</td>
																			<td>{!!$degree->description!!}</td>
																			<td>{{$degree->level}}</td>
																			<td>
																				<div class="dropdown">
																					<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
																					<span class="caret"></span></button>
																					<ul class="dropdown-menu">
																						<li><a href="/degree/module/add/{{$degree->id}}">Add Modules</a></li>
																						<li class="divider"></li>
																						<li><a href="/admin/degree/edit/{{$degree->id}}">Edit</a></li>
																						<li><a href="/admin/degree/delete/{{$degree->id}}">Delete</a></li>
																					</ul>
																				</div>
																			</td>
																		</tr>
																	@endforeach
															</table>
															{{$degrees->render()}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

					</div>
			</div>
	</section>
@endsection

@push('script')

@endpush
