@extends('admin.layouts.app')

@section('title')
	{{ 'Membuat Tugas ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Courses</h2>
		</div>

		<div class="row clearfix">
			<!-- Task Info -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="header">
						<h2>Create Tugas</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<div style="padding:25px;margin-top:0px;">
							<div class="">
								<center>
		              @if(Session::has('success'))
		                <div class="alert alert-success alert-dismissible">
		                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                  {!! Session::get('success') !!}
		                </div>
		              @elseif(Session::has('error'))
		                <div class="alert alert-error alert-dismissible">
		                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                  {!! Session::get('error') !!}
		                </div>
		              @endif
		            </center>

								<div class="card-md">
									{{--
								  <div class="panel-heading">
								    <h3 class="panel-title">{{$button}} Tugas</h3>
								  </div>
									--}}
								  <div class="card-block">
										{{ Form::open(array('url' => $action, 'method' => $method)) }}

											<div class="form-group no-m">
												<label for="title">Judul</label>
												<input placeholder="Masukan Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
											</div>

											<div class="form-group">
												<label for="title">Deskripsi / Penjelasan</label>
												<textarea name="description" id="description" required>{{ $description }}</textarea>
											</div>
											<div class="form-group">
												<label for="title">Tipe Tugas</label><br>
												<select class="form-control selectpicker" name="type" requred>
													<option {{$type == '0' ? 'selected' : ''}} value="0">Online Text</option>
													<option {{$type == '1' ? 'selected' : ''}} value="1">File Upload</option>
												</select>
											</div>
											<div class="form-group">
												<label for="Attempt">Kesempatan Menjawab</label>
												<input placeholder="Kesempatan Percobaan" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
											</div>
											{{-- <input placeholder="Kesempatan Percobaan" type="hidden" class="form-control" name="attempt" value="{{ $attempt }}"> --}}
											<div class="form-group">
												<label for="name">Tanggal Mulai</label>
												<input placeholder="Tanggal Mulai" autocomplete="off" type="text" class="form-control form_datetime" name="time_start" value="{{ $time_start }}" required>
											</div>
											<div class="form-group">
												<label for="name">Tanggal Akhir</label>
												<input placeholder="Tanggal Akhir" autocomplete="off" type="text" class="form-control form_datetime" name="time_end" value="{{ $time_end }}" required>
											</div>
											<div class="form-group">
												{{  Form::hidden('id', $id) }}
												{{  Form::submit($button . " Tugas" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
												<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a>
											</div>
										{{ Form::close() }}
								  </div>
								</div>

							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}
@endpush
