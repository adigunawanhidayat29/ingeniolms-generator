@extends('assignment.template')

@section('assignment_title'){{ 'Assignment'}}
@endsection

@push('style') 
	<style>
		.form - group input[type = file]{
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			width: 100 %;
			height: 100 %;
			z - index: 100;
		}
    </style>
    
    <style>
        .card-assignment {
            border-radius: 0 0 5px 5px;
            background: #f9f9f9;
            transition: all 0.3s;
            border: 1px solid #f5f5f5;
            margin-bottom: 2rem;
            margin-bottom: 20px;
        }

        .card-assignment .card-header {
            padding: 15px;
            background: #f6f6f6;
            border-bottom: 1px solid #ececec;
            margin-bottom: 0;
        }

        .card-assignment .card-header .card-title {
            margin: 0;
            font-size: 16px;
            font-weight: 500;
            color: #333;
            display: contents;
        }

        .card-assignment .card-block {
            padding: 15px;
            margin-bottom: -1rem;
        }
        
        .card-assignment .card-block p {
            font-size: 16px;
        }
        
        .card-assignment .card-block .form-group {
            padding-bottom: 0;
            margin: 15px 0 0;
        }

        @media (max-width: 420px) {
            .card-assignment .card-block p {
                font-size: 14px;
            }
        }
    </style>
@endpush

@section('assignment_content') 
    <div class="container">
        <div class="row player-content-group">
            <div class="col-md-12">

                <center>
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!!Session::get('success')!!}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-error alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!!Session::get('error')!!}
                    </div>
                    @endif
                </center>

                <h2 class="headline headline-md" style="margin-bottom: 1.5rem;"><span>{{$assignment->title}}</span></h2>

                <div class="card-assignment">
                    {{-- <div class="card-header">
                        <h3 class="card-title">{{$assignment->title}}</h3>
                    </div> --}}
                    <div class="card-block">
                        {!!$assignment -> description !!}
                        <hr>
                        <p>Type : {{$assignment->type == '1' ? 'Upload File' : 'Online Text'}}</p>
                        <hr>
                        <p>Time : {{$assignment->time_start .' - '. $assignment->time_end}}</p>
                    </div>
                </div>

                <div class="card-assignment">
                    <div class="card-header">
                        <h3 class="card-title">Answer Assignment</h3>
                    </div>
                    <div class="card-block">
                        {{ Form::open(array('url' => '/assignment/post/'.$assignment->id, 'method' => 'post', 'files' => true)) }}

                        @if($assignment->type == '0')
                            <textarea name="answer" id="answer" required="required"></textarea>
                        @else
                            <input type="file" class="form-control" name="answer_file" id="file">

                            <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                            <div id="container"></div>

                            <div class="progress" style="display:none">
                                <div
                                    id="progressBar"
                                    class="progress-bar progress-bar-striped active"
                                    role="progressbar"
                                    aria-valuenow="0"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style="width: 0%">
                                    <span class="sr-only">0%</span>
                                </div>
                            </div>

                            <div id="uploadedMessageError">
                                <div id="uploadedMessage"></div>
                            </div>

                            {{-- <div class="form-group">
                            <label for="title">Upload File</label>
                            <input type="file" class="form-control" name="answer_file" required>
                            </div> --}}
                        @endif

                        <div class="form-group">
                            {{  Form::submit("Submit Answer" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection 
    
@push('script') 
			
    {{-- CKEDITOR --}}
    <script src="{{url('ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('answer');
    </script>
    {{-- CKEDITOR --}}

    <script type="text/javascript" src="/plupload/plupload.full.min.js"></script>

    <script type="text/javascript">
        var uploader = new plupload.Uploader({
            runtimes: 'html5,flash,silverlight,html4', browse_button: 'file', // you can pass an id...
            container: document.getElementById('container'), // ... or DOM Element itself
            url: '/assignment/upload/{{$assignment->id}}',
            flash_swf_url: '/plupload/Moxie.swf',
            silverlight_xap_url: '/plupload/Moxie.xap',
            chunk_size: '1mb',
            dragdrop: true,
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            filters: {
                mime_types: [
                    {
                        title: "Files",
                        extensions: "mp4,webm,avi,mkv,docx,doc,xlsx,pptx,pdf,xls,txt,jpg,jpeg,png"
                    }
                ]
            },

            init: {
                PostInit: function () {
                    document
                        .getElementById('filelist')
                        .innerHTML = '';
                },

                FilesAdded: function (up, files) {
                    plupload.each(files, function (file) {
                        document
                            .getElementById('filelist')
                            .innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(
                                file.size
                            ) + ') <b></b></div>';

                        uploader.start();
                        $("#contentSave").prop('disabled', true);
                        return false;
                    });
                },

                UploadProgress: function (up, file) {
                    // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML =
                    // '<span>' + file.percent + "%</span>";
                    $('.progress').show();
                    $('#progressBar')
                        .attr('aria-valuenow', file.percent)
                        .css('width', file.percent + '%')
                        .text(file.percent + '%');
                
                    $("#uploadedMessage").html('Upload masih dalam proses...');
                    $("#uploadedMessage").addClass('alert alert-info');
                },

                FileUploaded: function (up, file, info) {
                    // Called when file has finished uploading console.log('[FileUploaded] File:',
                    // file, "Info:", info);
                    var response = JSON.parse(info.response);
                    // console.log(response.result.original);
                    path_file = $("#path_file").val(response.result.original.path);

                    $.ajax({
                        url: '/assignment/upload/success/{{$assignment->id}}',
                        type: 'POST',
                        data: {
                            _token: '{{csrf_token()}}',
                            answer: response.result.original.path,
                        },
                        success: function (response) {
                            console.log(response);
                            // alert('saved draft');
                            // $("#contentId").val(response.id)
                        }
                    })

                },

                ChunkUploaded: function (up, file, info) {
                    // Called when file chunk has finished uploading console.log('[ChunkUploaded]
                    // File:', file, "Info:", info);
                },

                UploadComplete: function (up, files) {
                    // Called when all files are either uploaded or failed
                    // console.log('[UploadComplete]'); $("#saveContent").prop('disabled', false);
                    // console.log(up);
                    $('#progressBar')
                        .attr('aria-valuenow', 100)
                        .css('width', 100 + '%')
                        .text(100 + '%');
                    $("#uploadedMessage").html('Upload selesai');
                    $("#uploadedMessage").addClass('alert alert-success');
                    $("#contentSave").prop('disabled', false);
                },

                Error: function (up, err) {
                    // console.log(up);
                    console.log(err);
                    // document.getElementById('console').appendChild(document.createTextNode("\nError
                    // #" + err.code + ": " + err.message));
                    if (err.status == 500 || err.code == "-200") {
                        $("#uploadedMessage").html('');
                        $("#uploadedMessageError").html('Upload Gagal');
                        $("#uploadedMessageError").addClass('alert alert-danger');
                        console.log('Upload Gagal');
                    }
                },

                Destroy: function () {
                    console.log('error');
                }
            }
        });

        uploader.init();
    </script>

@endpush