@extends('layouts.app')

@section('title')
	{{ 'Assignment Answer' }}
@endsection

@section('content')
	<div class="container">
		<br>

			<div class="">
				<center>
					@if(Session::has('success'))
						<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('success') !!}
						</div>
					@elseif(Session::has('error'))
						<div class="alert alert-error alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! Session::get('error') !!}
						</div>
					@endif
				</center>
				@if($AssignmentAnswer->type == '1')
					<div class="panel panel-default">
		        <div class="panel-heading">
		          <h3 class="panel-title">Answer</h3>
		        </div>
		        <div class="panel-body">
		          <div class="embed-responsive embed-responsive-16by9">
		            <iframe class="embed-responsive-item" src="{{$AssignmentAnswer->answer}}"></iframe>
		            <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
		          </div>
		        </div>
		      </div>
				@else
					{!!$AssignmentAnswer->answer!!}
				@endif
				<br><br>
				<a data-assignment-answer-id="{{$AssignmentAnswer->id}}" data-assignment-id="{{$AssignmentAnswer->assignment_id}}" id="giveGrade" class="btn btn-warning" href="#">Give Grade</a>
				<br><br>
			</div>
	</div>

	<div class="modal fade" id="modalGiveGrade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="">Give Grade</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
					  <label for="">Grade</label>
						<input type="hidden" id="assignment_id" value="">
						<input type="hidden" id="assignment_answer_id" value="">
					  <input type="number" class="form-control" id="inputGrade" placeholder="Grade">
					</div>
					<div class="form-group">
					  <input type="button" value="Submit" class="btn btn-primary" id="submitGrade">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#giveGrade',function(e){
				e.preventDefault();
					var assignment_answer_id = $(this).attr('data-assignment-answer-id');
					var assignment_id = $(this).attr('data-assignment-id');
					$("#assignment_id").val(assignment_id);
					$("#assignment_answer_id").val(assignment_answer_id);
					$("#modalGiveGrade").modal('show');
			});
		});

		$('#submitGrade').click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/assignment/save-grade')}}",
				type : "POST",
				data : {
					assignment_id : $("#assignment_id").val(),
					assignment_answer_id : $("#assignment_answer_id").val(),
					grade : $("#inputGrade").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		});

	</script>
@endpush
