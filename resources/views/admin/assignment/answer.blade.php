@extends('admin.layouts.app')

@section('title')
	{{ 'Assignment Answer' }}
@endsection

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Jawaban</h2>
		</div>

		<center>
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{!! Session::get('success') !!}
				</div>
			@elseif(Session::has('error'))
				<div class="alert alert-error alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{!! Session::get('error') !!}
				</div>
			@endif
		</center>

		<div class="row clearfix">
			<!-- Task Info -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="header">
						<h2>Data Jawaban</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">
						<table class="table table-striped table-light">
							<thead class="thead-light">
								<tr>
									<th class="active">No</th>
									<th>Nama</th>
									<th>Jawaban</th>
									<th>Nilai</th>
									<th>Option</th>
									@php $i = 1 @endphp
								</tr>
							</thead>
							<tr>
							<tbody>
								@foreach($assignments_answers as $assignment_answer)
									<tr>
										<td>{{ $i++ }}</td>
										<td>{{$assignment_answer->name}}</td>
										<td>
											@if($assignment_answer->type == '1')
												<a target="_blank" href="/course/assignment/answer-preview/{{$assignment_answer->id}}">{{$assignment_answer->answer}}</a>
											@else
												{!!$assignment_answer->answer!!}
											@endif
										</td>
										<td>{{$assignment_answer->grade}}</td>
										<td>
											<a class="btn btn-default btn-sm btn-raised" href="/course/assignment/answer-preview/{{$assignment_answer->id}}">Lihat Jawaban</a>
											@if($assignment_answer->grade == null)
												<a data-assignment-answer-id="{{$assignment_answer->id}}" data-assignment-id="{{$assignment_answer->assignment_id}}" id="giveGrade" class="btn btn-warning btn-sm btn-raised" href="#">Berikan Nilai</a>
											@endif
										</td>
									</tr>
								@endforeach
							<tbody>
							</tr>
						</table>
					</div>

					<div class="modal fade" id="modalGiveGrade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									{{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> --}}
									<h4 class="modal-title" id="">Berikan Nilai</h4>
								</div>
								<div class="modal-body">
									<div class="form-group">
									  <label for="">Nilai</label>
										<input type="hidden" id="assignment_id" value="">
										<input type="hidden" id="assignment_answer_id" value="">
									  <input type="number" class="form-control" id="inputGrade" placeholder="Nilai">
									</div>
									<div class="form-group">
									  <input type="button" value="Submit" class="btn btn-primary btn-sm btn-raised" id="submitGrade">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default btn-sm btn-raised" data-dismiss="modal">Tutup</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('script')
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#giveGrade',function(e){
				e.preventDefault();
					var assignment_answer_id = $(this).attr('data-assignment-answer-id');
					var assignment_id = $(this).attr('data-assignment-id');
					$("#assignment_id").val(assignment_id);
					$("#assignment_answer_id").val(assignment_answer_id);
					$("#modalGiveGrade").modal('show');
			});
		});

		$('#submitGrade').click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/assignment/save-grade')}}",
				type : "POST",
				data : {
					assignment_id : $("#assignment_id").val(),
					assignment_answer_id : $("#assignment_answer_id").val(),
					grade : $("#inputGrade").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		});

	</script>
@endpush
