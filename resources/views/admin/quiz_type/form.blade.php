@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Quiz Type' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Quiz Type Create</h3>
					</div>
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => $action, 'method' => $method)) }}
							<div class="form-group">
								<label for="type">Type</label>
								<input placeholder="Type" type="text" class="form-control" name="type" value="{{ $type }}" required>
								@if ($errors->has('name'))
									<span class="text-danger">{{ $errors->first('type') }}</span>
								@endif
							</div>

							<div class="form-group">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('quiz-type') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
