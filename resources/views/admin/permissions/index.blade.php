@extends('admin.layouts.app')

@section('title', Lang::get('back.user_permission.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.user_permission.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/admin/permissions/create') }}" class="btn btn-primary">@lang('back.user_permission.create_button')</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.user_permission.name_table')</th>
                                            <th>@lang('back.user_permission.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($permissions as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/permissions/' . $item->id) }}" class="btn btn-info btn-sm"><i class="material-icons">info</i><span>@lang('back.user_permission.detail_button')</span></a>
                                                    <a href="{{ url('/admin/permissions/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm"><i class="material-icons">edit</i><span>@lang('back.user_permission.edit_button')</span></a>

                                                    <form method="POST" action="{{ url('/admin/permissions' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.user_permission.delete_button')</span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $permissions->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $(function () {
            $("#data").DataTable();
        });
    </script>
@endpush