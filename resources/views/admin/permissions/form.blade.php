<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="name" class="control-label">@lang('back.user_permission_form.name_label')</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($permission->name) ? $permission->name : ''}}" placeholder="@lang('back.user_permission_form.name_label')">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.user_permission_form.update_button') : Lang::get('back.user_permission_form.create_button') }}">
<a href="{{ url('/admin/permissions') }}" class="btn bg-blue-grey">@lang('back.user_permission_form.back_button')</a>
