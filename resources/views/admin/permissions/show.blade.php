@extends('admin.layouts.app')

@section('title', Lang::get('back.user_permission_detail.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.user_permission_detail.header') - {{ $permission->name }}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/permissions') }}"><i class="material-icons">arrow_back</i><span>@lang('back.user_permission_detail.back_button')</span></a>
                                <a class="btn btn-warning btn-sm" href="{{ url('/admin/permissions/' . $permission->id . '/edit') }}"><i class="material-icons">edit</i><span>@lang('back.user_permission_detail.edit_button')</span></a>
                                <form method="POST" action="{{ url('admin/permissions' . '/' . $permission->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.user_permission_detail.delete_button')</span></button>
                                </form>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover" style="margin-bottom: 0;">
                                    <tbody>
                                        <tr>
                                            <th>#</th>
                                            <td>{{ $permission->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>@lang('back.user_permission_detail.name_table')</th>
                                            <td>{{ $permission->name }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
