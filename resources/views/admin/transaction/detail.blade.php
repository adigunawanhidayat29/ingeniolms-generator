@extends('admin.layouts.app')

@section('title')
	{{ 'Transaction Detail' }}
@endsection


@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="panel panel-default">
					  <div class="panel-body">
							<table class="table table-hover">
			          <thead>
			            <tr>
			              <th>Produk</th>
			              <th>Price</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach($transactions_details as $transaction_detail)
			              <tr>
			                <td>
			                  <p><strong>{{$transaction_detail->title}}</strong></p>
			                  <p><img height="80" src="{{$transaction_detail->image}}"></p>
			                </td>
			                <td>{{rupiah($transaction_detail->price)}}</td>
			              </tr>
			            @endforeach
			          </tbody>
			        </table>
							<a class="btn btn-success" href="/admin/transaction/approve/{{Request::segment(4)}}">Approve</a>
					  </div>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
</section>
@endsection
