@extends('admin.layouts.app')

@section('title')
	{{ 'Transaction Detail' }}
@endsection


@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="panel panel-default">
					  <div class="panel-body">
							<table class="table table-hover">
			          <thead>
			            <tr>
			              <th>File</th>
										<th>Bank Name</th>
			              <th>Account Name</th>
										<th>Account Number</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach($transactions_confirmations as $transaction_confirmation)
			              <tr>
			                <td>
			                  <a href="#" id="file_detail" data-src="{{asset_url($transaction_confirmation->file)}}"><img height="80" src="{{asset_url($transaction_confirmation->file)}}"></a>
			                </td>
			                <td>{{$transaction_confirmation->bank_name}}</td>
											<td>{{$transaction_confirmation->account_name}}</td>
											<td>{{$transaction_confirmation->account_number}}</td>
			              </tr>
			            @endforeach
			          </tbody>
			        </table>
							<a class="btn btn-success" href="/admin/transaction/approve/{{Request::segment(4)}}">Approve</a>
					  </div>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	<div class="modal fade" id="file_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id=""></h4>
	      </div>
	      <div class="modal-body">
					<img id="confirmation_file" src="" class="img-responsive">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>	        
	      </div>
	    </div>
	  </div>
	</div>
</section>
@endsection

@push('script')
	<script type="text/javascript">

		$(function(){
		 $(document).on('click','#file_detail',function(e){
			 e.preventDefault();
				 var data_src = $(this).attr('data-src');
				 $("#confirmation_file").attr('src', data_src);
				 $("#file_modal").modal('show');
		 });
	 });
	</script>
@endpush
