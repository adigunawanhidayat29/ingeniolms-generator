@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Essayment Of' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{$button}} Essayment - {{$section->title}}</h3>
					</div>
					<div class="box-body">

						<center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						<div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title">{{$button}} Essayment</h3>
						  </div>
						  <div class="panel-body">
								{{ Form::open(array('url' => $action, 'method' => $method)) }}

									<div class="form-group">
										<label for="title">Title</label>
										<input placeholder="Title" type="text" class="form-control" name="title" value="{{ $title }}" required>
									</div>

									<div class="form-group">
										<label for="title">Description</label>
										<textarea name="description" id="description" required>{{ $description }}</textarea>
									</div>
									<div class="form-group">
										<label for="title">Type</label><br>
										<select class="form-control" name="type" requred>
											<option {{$type == '0' ? 'selected' : ''}} value="0">Online Text</option>
											<option {{$type == '1' ? 'selected' : ''}} value="1">File Upload</option>
										</select>
									</div>
									<div class="form-group">
										<label for="Attempt">Attempt</label>
										<input placeholder="Attempt" type="text" class="form-control" name="attempt" value="{{ $attempt }}" required>
									</div>
									<div class="form-group">
										<label for="name">Time Start</label>
										<input placeholder="Time Start" type="text" class="form-control form_datetime" name="time_start" value="{{ $time_start }}" required>
									</div>
									<div class="form-group">
										<label for="name">Time End</label>
										<input placeholder="Time End" type="text" class="form-control form_datetime" name="time_end" value="{{ $time_end }}" required>
									</div>
									<div class="form-group">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit($button . " Essayment" , array('class' => 'btn btn-primary', 'name' => 'button')) }}
										<a href="{{ url('courses/manage/'.$section->id_course) }}" class="btn btn-default">Back to Manage</a>
									</div>
								{{ Form::close() }}
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}
@endpush
