@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Assignments Answer' }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="box">
			<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th>Name</th>
						<th>Answer</th>
						@foreach($assignments_answers as $assignment_answer)
							<tr>
								<td>{{$assignment_answer->name}}</td>
								<td>
									@if($assignment_answer->type == '1')
										<a target="_blank" href="{{asset_url($assignment_answer->answer)}}">{{$assignment_answer->answer}}</a>
									@else
										{{$assignment_answer->answer}}
									@endif
								</td>
							</tr>
						@endforeach
					</tr>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}
@endpush
