@extends('admin.layouts.app') 

@section('title', Lang::get('back.user_teacher_community.title')) 

@section('content')
<section class="content">
    <div class="container-fluid">
        <center>
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('success') !!}
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! Session::get('error') !!}
                </div>
            @endif
        </center>

        <div class="block-header">
            <h2>@lang('back.user_teacher_community.header')</h2>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        <div style="margin-bottom: 2rem;">
                            <a href="{{ url('/admin/communities/create') }}" class="btn btn-primary btn-sm">@lang('back.user_teacher_community.create_button')</a>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('back.user_teacher_community.name_table')</th>
                                        {{-- <th>@lang('back.user_teacher_community.short_description_table')</th>
                                        <th>@lang('back.user_teacher_community.description_table')</th> --}}
                                        <th>@lang('back.user_teacher_community.image_table')</th>
                                        <th>@lang('back.user_teacher_community.option_table')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($communities as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            {{-- <td>{{ $item->short_description }}</td>
                                            <td>{{ $item->description }}</td> --}}
                                            <td><img src="{{ $item->photo }}" alt="" style="width: 100%; max-width: 250px;"></td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="{{ url('/admin/communities/' . $item->id) }}" title="View Community">
                                                    <i class="material-icons">info</i><span>@lang('back.user_teacher_community.option_detail_button')</span>
                                                </a>
                                                <a class="btn btn-warning btn-sm" href="{{ url('/admin/communities/' . $item->id . '/edit') }}">
                                                    <i class="material-icons">edit</i><span>@lang('back.user_teacher_community.option_update_button')</span>
                                                </a>

                                                <form method="POST" action="{{ url('/admin/communities' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="material-icons">delete</i><span>@lang('back.user_teacher_community.option_delete_button')</span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper">
                                {!! $communities->appends(['search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function(){
            var asset_url = '{{asset_url()}}';
            $("#data").DataTable();
        });
    </script>
@endpush