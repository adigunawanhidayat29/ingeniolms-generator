@push('style')
	<!-- include summernote css/js -->
	<link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="name" class="control-label">@lang('back.user_teacher_community_form.name_label')</label>
        <input required class="form-control" name="name" type="text" id="name" value="{{ isset($community->name) ? $community->name : ''}}" placeholder="@lang('back.user_teacher_community_form.name_label')">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('short_description') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="short_description" class="control-label">@lang('back.user_teacher_community_form.short_description_label')</label>
        <textarea class="form-control summernote" rows="5" name="short_description" type="textarea" id="short_description" >{{ isset($community->short_description) ? $community->short_description : ''}}</textarea>
        {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="description" class="control-label">@lang('back.user_teacher_community_form.description_label')</label>
        <textarea class="form-control summernote" rows="5" name="description" type="textarea" id="description" >{{ isset($community->description) ? $community->description : ''}}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="photo" class="control-label">@lang('back.user_teacher_community_form.image_label')</label>
        <input placeholder="photo" type="file" class="form-control" name="photo" value="">
        {{-- <textarea class="form-control" rows="5" name="photo" type="textarea" id="photo" >{{ isset($community->photo) ? $community->photo : ''}}</textarea> --}}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="status" class="control-label">@lang('back.user_teacher_community_form.status_label')</label>
        {{-- <input class="form-control" name="status" type="text" id="status" value="{{ isset($community->status) ? $community->status : ''}}" placeholder="@lang('back.user_teacher_community_form.status_label')"> --}}
        <input name="status" type="radio" id="active" value="1" {{isset($community->photo) &&$community->status == '1' ? 'checked' : ''}} />
        <label for="active">Aktif</label>
        <input name="status" type="radio" id="nonactive" value="0" {{isset($community->photo) && $community->status == '0' ? 'checked' : ''}} />
        <label for="nonactive">Tidak Aktif</label>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.user_teacher_community_form.update_button') : Lang::get('back.user_teacher_community_form.create_button') }}">

@push('script')
	<script src="{{asset('summernote/summernote.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
            height: 300
        });
	});
	</script>
@endpush