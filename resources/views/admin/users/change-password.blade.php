@extends('admin.layouts.app')

@section('title', Lang::get('back.user_all_password.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.user_all_password.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ Form::open(array('url' => \Request::route()->getPrefix() . '/users/password/update_action/'.Request::segment(5), 'method' => 'post')) }}
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="name">@lang('back.user_all_password.password_label')</label>
                                        <input placeholder="@lang('back.user_all_password.password_label')" type="password" class="form-control" name="password" value="" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="name">@lang('back.user_all_password.confirm_password_label')</label>
                                        <input placeholder="@lang('back.user_all_password.confirm_password_label')" type="password" class="form-control" name="password_confirmation" value="" required>
                                    </div>
                                </div>

                                {{  Form::submit(Lang::get('back.user_all_password.submit_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
                                <a href="{{ url('admin/users') }}" class="btn bg-blue-grey">@lang('back.user_all_password.back_button')</a>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
@endsection
