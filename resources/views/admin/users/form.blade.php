@extends('admin.layouts.app')

@section('title')
    {{$button == 'Create' ? Lang::get('back.user_all_form.add_user_title') : Lang::get('back.user_all_form.update_user_title')}}
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>{{$button == 'Create' ? Lang::get('back.user_all_form.add_user_header') : Lang::get('back.user_all_form.update_user_header')}}</h2>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        @php
                            if(Session::get('error')){
                                $errors = Session::get('error');
                            }
                        @endphp
                        {{ Form::open(array('url' => $action, 'method' => $method)) }}
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">@lang('back.user_all_form.username_label')</label>
                                    <input placeholder="@lang('back.user_all_form.username_label')" type="text" class="form-control" name="username" value="{{ $username }}" required>							
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">@lang('back.user_all_form.name_label')</label>
                                    <input placeholder="@lang('back.user_all_form.name_label')" type="text" class="form-control" name="name" value="{{ $name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">@lang('back.user_all_form.email_label')</label>
                                    <input placeholder="@lang('back.user_all_form.email_label')" type="email" class="form-control" name="email" value="{{ $email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>

                            @if($button == "Create")
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="name">@lang('back.user_all_form.password_label')</label>
                                        <input placeholder="@lang('back.user_all_form.password_label')" type="password" class="form-control" name="password" value="{{ $password }}" required>
                                    </div>
                                </div>
                            @endif

                            @if($button == "Create")
                                <div class="form-group">
                                    <label for="name">@lang('back.user_all_form.user_level_label')</label>
                                    <div class="demo-checkbox">
                                        @foreach($user_levels as $index => $user_level)
                                            <input type="checkbox" value="{{$user_level->id}}" id="basic_checkbox_{{$index+1}}" name="id_level_user[]" class="filled-in" checked />
                                            <label for="basic_checkbox_{{$index+1}}">{{$user_level->name}}</label>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="name">@lang('back.user_all_form.name_label')</label>
                                    <div class="demo-checkbox">
                                        @foreach($user_levels as $index => $user_level)
                                            <input {{in_array($user_level->id, $user_groups) ? 'checked' : ''}} value="{{$user_level->id}}" type="checkbox" id="basic_checkbox_{{$index+1}}" name="id_level_user[]" class="filled-in" />
                                            <label for="basic_checkbox_{{$index+1}}">{{$user_level->name}}</label>
                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            {{  Form::hidden('id', $id) }}
                            {{  Form::submit($button == 'Create' ? Lang::get('back.user_all_form.add_user_button') : Lang::get('back.user_all_form.update_user_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
                            <a href="{{ url('admin/users') }}" class="btn bg-blue-grey">@lang('back.user_all_form.back_button')</a>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection