@extends('admin.layouts.app')

@section('title', Lang::get('back.user_import.title'))

@push('style')
     <link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
@endpush


@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.user_import.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a target="_blank" class="btn btn-success" href="/imports/users/_sample/users.xlsx">@lang('back.user_import.download_button')</a>
                            </div>

                            <form action="{{url( \Request::route()->getPrefix() . '/users/import-action')}}" name='dropzone' method="post" enctype="multipart/form-data">
                                <div id='frmTarget' class="dropzone" style="margin-bottom: 2rem;">
                                    {{csrf_field()}}
                                    <div class="dz-message">
                                        <div class="drag-icon-cph">
                                            <i class="material-icons">touch_app</i>
                                        </div>
                                        <h3>@lang('back.user_import.upload_form')</h3>
                                        {{-- <em>(Masukkan file xlsx atau csv untuk impor data pengguna)</em> --}}
                                    </div>
                                    <div class="fallback">
                                        <input name="file" type="file" />
                                    </div>
                                </div>                                                                 
                                <button id="button" class="btn btn-primary" type="submit">@lang('back.user_import.submit_button')</button>
                                <a href="{{ url('admin/users') }}" class="btn bg-blue-grey">@lang('back.user_import.back_button')</a>
                            </form>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
@endsection

@push('script')

    <script src="/plugins/dropzone/dropzone.js"></script>

    <script>
        Dropzone.options.frmTarget = {
            autoProcessQueue: false,
            url: "{{url( \Request::route()->getPrefix() . '/users/import-action')}}",
            init: function () {

                var myDropzone = this;

                // Update selector to match your button
                $("#button").click(function (e) {
                    e.preventDefault();
                    myDropzone.processQueue();
                });

                this.on('sending', function(file, xhr, formData) {
                    // Append all form inputs to the formData Dropzone will POST
                    var data = $('#frmTarget').serializeArray();
                    $.each(data, function(key, el) {
                        formData.append(el.name, el.value);
                    });
                });

                this.on('success', function(file, xhr, formData) {
                    // Append all form inputs to the formData Dropzone will POST
                    window.location.assign("{{url( \Request::route()->getPrefix() . '/users')}}")
                });
            }
        }
    </script>
  
@endpush
