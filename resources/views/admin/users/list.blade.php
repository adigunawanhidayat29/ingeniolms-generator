@extends('admin.layouts.app')

@section('title', Lang::get('back.user_all.title'))

@section('content')
	<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>

			<div class="block-header">
				<h2>@lang('back.user_all.header')</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div class="table-responsive">
								<div style="margin-bottom: 2rem;">
									<a href="{{url( \Request::route()->getPrefix() . '/users/create')}}" class="btn btn-primary">@lang('back.user_all.add_user_button')</a>
									<a href="{{url( \Request::route()->getPrefix() . '/users/import')}}" class="btn btn-danger">@lang('back.user_all.import_user_button')</a>
								</div>

								<table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
									<thead>
										<tr>
											<th>@lang('back.user_all.name_table')</th>
											<th>@lang('back.user_all.email_table')</th>
											<th>@lang('back.user_all.status_table')</th>
											<th>@lang('back.user_all.created_table')</th>
											<th>@lang('back.user_all.option_table')</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('script')
  <script type="text/javascript">
  $(function(){
    $("#data").DataTable({
			"order": [[ 3, "desc" ]],
      processing: true,
      serverSide: true,
			dom: 'lBfrtip',
			responsive: true,
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
      ajax: '{{ url( \Request::route()->getPrefix() . "/users/serverside") }}',
      columns: [
        { data: 'name', name: 'name' },
        { data: 'email', name: 'email' },
        { data: 'is_active', name: 'is_active' },
        { data: 'created_at', name: 'created_at' },
        { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });

	// $(function () {
	//     $('.js-basic-example').DataTable({
	//         responsive: true
	//     });
	//
	//     //Exportable table
	//     $('.js-basic-example').DataTable({
	//         dom: 'Bfrtip',
	//         responsive: true,
	//         buttons: [
	//             'copy', 'csv', 'excel', 'pdf', 'print'
	//         ]
	//     });
	// });
  </script>
@endpush
