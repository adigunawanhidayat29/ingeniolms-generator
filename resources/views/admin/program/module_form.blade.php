@extends('admin.layouts.app')

@section('title')
	Program Add Module
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>
			<div class="block-header">
				<h2>TAMBAH MODUL PROGRAM</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp
	
							{{ Form::open(array('url' => '/admin/program/module/store/'.Request::segment(5), 'method' => 'post')) }}
								<div class="form-group">
									<label class="control-label" for="">Tambah Modules</label>
									<select class="form-control" name="course_id[]" id="course_id" multiple required>
										@foreach($courses as $course)
											<option value="{{$course->id}}">{{$course->title}}</option>
										@endforeach
									</select>
								</div>
	
								{{  Form::submit('Tambah' , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/program') }}" class="btn bg-blue-grey">Kembali</a>
							{{ Form::close() }}
						</div>
					</div>
	
					<div class="card">
						<div class="header">
							<h2>Course dalam program ini</h2>
						</div>
						<div class="body">
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th>Course</th>
											<th>Opsi</th>
										</tr>
									</thead>
									<tbody>
										@foreach($ProgramCourses as $ProgramCourse)
											<tr>
												<td>{{$ProgramCourse->title}}</td>
												{{-- <td><a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="/program/module/delete/{{$ProgramCourse->id}}">Hapus</a></td> --}}
												<td>
													<a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" href="/admin/program/module/delete/{{$ProgramCourse->id}}">
														<i class="material-icons">delete</i><span>Hapus</span>
													</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('script')
	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#course_id").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
