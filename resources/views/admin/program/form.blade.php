@extends('admin.layouts.app')

@section('title')
	Program Create
@endsection

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>{{$button == "Create" ? "BUAT" : "PERBARUI"}} PROGRAM</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp
							{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="">Nama Program</label>
										<input placeholder="Nama Program" type="text" class="form-control" name="title" value="{{ $title }}" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="">Harga Program</label>
										<input placeholder="Harga Program" type="text" class="form-control" name="price" value="{{ $price }}" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="">Password</label>
										<input placeholder="Password" type="text" class="form-control" name="password" value="{{ $password }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="title">Deskripsi Program</label>
										<textarea name="description" id="description">{!! $description !!}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="">Gambar</label>
										@if(Request::segment(2) == 'edit')
											<br>
											<img src="{{asset_url($image)}}" height="80">
											<br>
										@endif
										<input placeholder="Gambar" type="file" class="form-control" name="image">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="">Video Pengantar</label>
										<input placeholder="Video Pengantar" type="file" class="form-control" name="introduction">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="title">Deskripsi Pendahuluan</label>
										<textarea name="introduction_description" id="introduction_description">{!! $introduction_description !!}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="title">Manfaat Program</label>
										<textarea name="benefits" id="benefits">{!! $introduction_description !!}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="title">Tim Pengembang</label>
										<textarea name="developer_team" id="developer_team">{!! $developer_team !!}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label class="control-label" for="title">Precondition</label>
										<textarea name="precondition" id="precondition">{!! $precondition !!}</textarea>
									</div>
								</div>

								{{ Form::hidden('id', $id) }}
								{{ Form::submit($button == 'Create' ? 'Buat' : 'Perbarui' , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/program') }}" class="btn bg-blue-grey">Kembali</a>
							{{ Form::close() }}
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script type="text/javascript">
		CKEDITOR.replace('description');
		CKEDITOR.replace('introduction_description');
		CKEDITOR.replace('benefits');
		CKEDITOR.replace('developer_team');
		CKEDITOR.replace('precondition');
	</script>
	{{-- CKEDITOR --}}
@endpush