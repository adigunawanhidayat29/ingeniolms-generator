@extends('admin.layouts.app')

@section('title')
	Program
@endsection

@section('content')
	<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>
			<div class="block-header">
				<h2>PROGRAM</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">                        
						<div class="body">
							<div class="table-responsive">
								<div style="margin-bottom: 2rem;">
									<a href="{{url( \Request::route()->getPrefix() . '/program/create')}}" class="btn btn-primary">Buat Program</a>
								</div>
								
								<table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
									<tr>
										<th>Nama Program</th>
										<th>Deskripsi</th>
										<th>Gambar</th>
										<th>Opsi</th>
									</tr>
									@foreach($Programs as $Program)
										<tr>
											<td>{{$Program->title}}</td>
											<td>{!!$Program->description!!}</td>
											<td><img src="{{asset_url($Program->image)}}" alt=""></td>
											<td>
												<a class="btn btn-info btn-sm" href="/admin/program/module/add/{{$Program->id}}" title="View Community">
													<i class="material-icons">article</i><span>Tambahkan Modul</span>
												</a>
												<a class="btn btn-warning btn-sm" href="/admin/program/edit/{{$Program->id}}">
													<i class="material-icons">edit</i><span>Ubah</span>
												</a>
												<a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" href="/program/delete/{{$Program->id}}">
													<i class="material-icons">delete</i><span>Hapus</span>
												</a>
												
												{{-- <div class="dropdown">
													<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Option
													<span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="/admin/program/module/add/{{$Program->id}}">Add Modules</a></li>
														<li class="divider"></li>
														<li><a href="/admin/program/edit/{{$Program->id}}">Edit</a></li>
														<li><a onclick="return confirm('Are you sure?')" href="/program/delete/{{$Program->id}}">Delete</a></li>
													</ul>
												</div> --}}
											</td>
										</tr>
									@endforeach
								</table>
								{{$Programs->render()}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
@endsection

@push('script')
	<script>
		$(function(){
			$("#data").DataTable();
		});
	</script>		
@endpush