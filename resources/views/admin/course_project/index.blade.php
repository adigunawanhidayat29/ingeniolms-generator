@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Course Projects' }}
@endsection

@section('main-content')
  <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Course Project List</h3>
            <br><br>
            <a href="{{url('course/project/create')}}" class="btn btn-primary">Create</a>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                @foreach($CourseProjects as $CourseProject)
                  <tr>
                    <td>{{$CourseProject->title}}</td>
                    <td><img height="80" src="{{asset_url($CourseProject->image)}}" alt=""></td>
                    <td>{{$CourseProject->status == '1' ? 'Active' : 'Non Active'}}</td>
                    <td>
                      <a href="/course/project/show/{{$CourseProject->id}}">Show</a>
                      <a href="/course/project/edit/{{$CourseProject->id}}">Edit</a>
                      <a href="/course/project/delete/{{$CourseProject->id}}" onclick="return confirm('Are you Sure?')">Delete</a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
