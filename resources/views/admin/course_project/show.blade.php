@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'CourseProject' }}
@endsection

@section('main-content')
  <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="well well-sm">
				  <a href="/course/project" class="btn btn-default">Back</a>
				</div>

				<!-- Default box -->
				<div class="box">
					<div class="box-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <td>Title</td>
									<td>{{$CourseProject->title}}</td>
								</tr>
								<tr>
                  <td>Image</td>
									<td><img height="80" src="{{asset_url($CourseProject->image)}}" alt=""></td>
								</tr>
								<tr>
                  <td>Description</td>
									<td>{!!$CourseProject->description!!}</td>
								</tr>
								<tr>
                  <td>Status</td>
									<td>{{$CourseProject->status == '1' ? 'Active' : 'Non Active'}}</td>
								</tr>
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
