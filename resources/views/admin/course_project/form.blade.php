@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Course Project Form' }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Course Project Create</h3>
					</div>
					<div class="box-body">
						{{ Form::open(array('url' => $action, 'method' => 'post', 'files' => true)) }}

							<div class="form-group">
								<label for="">Title</label>
								<input placeholder="Title" type="text" class="form-control" name="title" value="{{ $title }}" required>
							</div>

							<div class="form-group">
								<label for="title">Description</label>
								<textarea name="description" id="description">{!! $description !!}</textarea>
							</div>

							<div class="form-group">
								<label for="">Image</label>
								<input placeholder="Image" type="file" class="form-control" name="image">
							</div>

							<div class="form-group">
								<label for="title">Status</label>
								<select class="form-control" name="status">
									<option {{$status == '1' ? 'selected' : ''}} value="1">Active</option>
									<option {{$status == '0' ? 'selected' : ''}} value="0">Non Active</option>
								</select>
							</div>

							<div class="form-group">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('course/project') }}" class="btn btn-default">Back</a>
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}
@endpush
