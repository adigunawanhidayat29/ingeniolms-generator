@extends('admin.layouts.app')

@section('title', Lang::get('back.lang_setting_detail.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.lang_setting_detail.header') - {{ $language->title }}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/languages') }}"><i class="material-icons">arrow_back</i><span>@lang('back.lang_setting_detail.back_button')</span></a>
                                <form method="POST" action="{{ url('admin/languages' . '/' . $language->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.lang_setting_detail.delete_button')</span></button>
                                </form>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover" style="margin-bottom: 0;">
                                    <tbody>
                                        <tr>
                                            <th>#</th>
                                            <td>{{ $language->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>@lang('back.lang_setting_detail.lang_table')</th>
                                            <td>{{ $language->title }}</td>
                                        </tr>
                                        <tr>
                                            <th>@lang('back.lang_setting_detail.alias_table')</th>
                                            <td>{{ $language->alias }}</td>
                                        </tr>
                                        <tr>
                                            <th>@lang('back.lang_setting_detail.status_table')</th>
                                            <td>{{ $language->status }}</td>
                                        </tr>
                                        <tr>
                                            <th>@lang('back.lang_setting_detail.icon_table')</th>
                                            <td>{{ $language->icon }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="block-header">
                        <h2>@lang('back.lang_setting_detail.upload_header')</h2>
                    </div>
                    <div class="card">
                        <div class="body">
                            <form method="POST" action="/admin/languages/upload/{{$language->id}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="form-line">
                                        <label>@lang('back.lang_setting_detail.upload_label')</label>
                                        <input type="file" name="files[]" multiple>
                                    </div>
                                </div>
                                <button class="btn btn-sm btn-primary" type="submit"><i class="material-icons">upload</i><span>@lang('back.lang_setting_detail.upload_button')</span></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block-header">
                        <h2>@lang('back.lang_setting_detail.download_header')</h2>
                    </div>
                    <div class="card">
                        <div class="body">
                            <p><b>@lang('back.lang_setting_detail.download_label')</b></p>
                            <a href="/admin/languages/download/{{$language->id}}" class="btn btn-info btn-sm">
                                <i class="material-icons">download</i><span>@lang('back.lang_setting_detail.download_button')</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
