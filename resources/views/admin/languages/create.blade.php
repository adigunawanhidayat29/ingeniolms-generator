@extends('admin.layouts.app')

@section('title', Lang::get('back.lang_setting_form.add_title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.lang_setting_form.add_header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <form method="POST" action="{{ url('/admin/languages') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @include ('admin.languages.form', ['formMode' => 'create'])
                                <a class="btn btn-sm bg-blue-grey" href="{{ url('/admin/languages') }}">@lang('back.user_teacher_community_form.back_button')</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
