@extends('admin.layouts.app')

@section('title', Lang::get('back.lang_setting.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@lang('back.lang_setting.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
								<a href="{{ url('/admin/languages/create') }}" class="btn btn-primary">@lang('back.lang_setting.add_button')</a>
							</div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.lang_setting.lang_table')</th>
                                            <th>@lang('back.lang_setting.alias_table')</th>
                                            <th>Status</th>
                                            {{-- <th>Icon</th> --}}
                                            <th>@lang('back.lang_setting.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($languages as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->alias }}</td>
                                                <td>{{ $item->status == '1' ? 'Default' : '' }}</td>
                                                {{-- <td>{{ $item->icon }}</td> --}}
                                                <td>
                                                    @if($item->status != '1')
                                                        <a href="{{ url('/admin/languages/default/' . $item->id) }}"><button class="btn btn-info btn-sm"><i class="material-icons">info</i><span>Set Default</span></button></a>
                                                    @endif
                                                    
                                                    <a href="{{ url('/admin/languages/' . $item->id) }}"><button class="btn btn-info btn-sm"><i class="material-icons">info</i><span>@lang('back.lang_setting.info_button')</span></button></a>
                                                    {{-- <a href="{{ url('/admin/languages/' . $item->id . '/edit') }}" title="Edit Language"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> --}}

                                                    <form method="POST" action="{{ url('/admin/languages' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>@lang('back.lang_setting.delete_button')</span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $languages->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $(function(){
            $("#data").DataTable();
        });
    </script>
@endpush