@extends('admin.layouts.app')

@section('title')
    {{$button == 'Create' ? Lang::get('back.video_form.add_title') : Lang::get('back.video_form.update_title')}}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{$button == 'Create' ? Lang::get('back.video_form.add_header') : Lang::get('back.video_form.update_header')}}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            @php
                                if(Session::get('error')){
                                    $errors = Session::get('error');
                                }
                            @endphp
                            {{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}                        
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">@lang('back.video_form.name_label')</label>
                                        <input placeholder="@lang('back.video_form.name_label')" type="text" class="form-control" name="title" value="{{$title}}" required>                                
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">@lang('back.video_form.url_label')</label>
                                        <input placeholder="@lang('back.video_form.url_label')" type="text" class="form-control" name="url" value="{{$url}}" required>                                
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">Deskripsi</label>
                                        <input placeholder="Deskripsi" type="text" class="form-control" name="description" value="{{$description}}" required>                                
                                    </div>
                                </div> --}}

                                <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                    <label for="status" class="control-label">@lang('back.video_form.status_label')</label>
                                    @if( isset($status) )
                                        <div class="form-group">
                                            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue" {{$status == '1' ? 'checked' : ''}}>
                                            <label for="statusAktif">@lang('back.video_form.enabled_radio')</label>
                                            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue" {{$status == '0' ? 'checked' : ''}}>
                                            <label for="statusNonaktif">@lang('back.video_form.disabled_radio')</label>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue">
                                            <label for="statusAktif">@lang('back.video_form.enabled_radio')</label>
                                            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue">
                                            <label for="statusNonaktif">@lang('back.video_form.disabled_radio')</label>
                                        </div>
                                    @endif
                                </div>

                                {{-- <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">Status</label>
                                        <select class="form-control" name="status">
                                            <option {{$status == '1' ? 'selected' : ''}} value="1">Aktif</option>
                                            <option {{$status == '0' ? 'selected' : ''}} value="0">Non Aktif</option>                                            
                                        </select>
                                    </div>
                                </div> --}}

                                {{ Form::hidden('id', $id) }}
                                {{ Form::submit($button == 'Create' ? Lang::get('back.video_form.add_button') : Lang::get('back.video_form.update_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
                                <a href="{{ url('admin/videos') }}" class="btn bg-blue-grey">@lang('back.video_form.back_button')</a>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection