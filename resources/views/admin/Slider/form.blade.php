@extends('admin.layouts.app')

@section('title')
    {{$button == 'Create' ? Lang::get('back.banner_slider_form.add_title') : Lang::get('back.banner_slider_form.update_title')}}
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{$button == 'Create' ? Lang::get('back.banner_slider_form.add_header') : Lang::get('back.banner_slider_form.update_header')}}</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            @php
                                if(Session::get('error')){
                                    $errors = Session::get('error');
                                }
                            @endphp
                            {{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

                                @if($button != 'Create')
                                    <img src="{{ asset_url($image) }}" style="width: 25%; margin-bottom: 2rem;">
                                @endif

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">@lang('back.banner_slider_form.image_label')</label>
                                        <input placeholder="@lang('back.banner_slider_form.image_label')" type="file" class="form-control" name="image" {{$button == 'Create' ? 'required' : '' }}>                                
                                    </div>
                                </div>                            

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="header">@lang('back.banner_slider_form.header_label')</label>
                                        <input placeholder="@lang('back.banner_slider_form.header_label')" type="text" class="form-control" name="header" value="{{$header}}" required>                                
                                    </div>
                                </div>
                                <div class="row" style="display: flex;">
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="header_font_size">@lang('back.banner_slider_form.font_size_label')</label>
                                            <input placeholder="@lang('back.banner_slider_form.default_text'): 36px" type="text" class="form-control" name="header_font_size" value="{{$header_font_size}}">                                
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="header_font_weight">@lang('back.banner_slider_form.font_weight_label')</label>
                                            <input placeholder="@lang('back.banner_slider_form.default_text'): 700" type="number" class="form-control" name="header_font_weight" value="{{$header_font_weight}}">                                
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="header_font_color">@lang('back.banner_slider_form.font_color_label')</label>
                                        <input placeholder="@lang('back.banner_slider_form.default_text'): #FFFFFF" type="text" class="form-control" name="header_font_color" value="{{$header_font_color}}">                                
                                        </div>
                                    </div>
                                </div>                            

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="subheader">@lang('back.banner_slider_form.sub_header_label')</label>
                                        <input placeholder="@lang('back.banner_slider_form.sub_header_label')" type="text" class="form-control" name="subheader" value="{{$subheader}}">                                
                                    </div>
                                </div>
                                <div class="row" style="display: flex;">
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="subheader_font_size">@lang('back.banner_slider_form.font_size_label')</label>
                                            <input placeholder="@lang('back.banner_slider_form.default_text'): 18px" type="text" class="form-control" name="subheader_font_size" value="{{$subheader_font_size}}">                                
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="subheader_font_weight">@lang('back.banner_slider_form.font_weight_label')</label>
                                            <input placeholder="@lang('back.banner_slider_form.default_text'): 300" type="number" class="form-control" name="subheader_font_weight" value="{{$subheader_font_weight}}">                                
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="form-line">
                                            <label for="subheader_font_color">@lang('back.banner_slider_form.font_color_label')</label>
                                            <input placeholder="@lang('back.banner_slider_form.default_text'): #FFFFFF" type="text" class="form-control" name="subheader_font_color" value="{{$subheader_font_color}}">                                
                                        </div>
                                    </div>
                                </div> 

                                {{-- <div class="form-group">
                                    <div class="form-line">
                                        <label for="title">Deskripsi</label>
                                        <input placeholder="Deskripsi" type="text" class="form-control" name="description" value="{{$description}}">                                
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label for="title">@lang('back.banner_slider_form.text_align_label')</label>
                                    <div class="form-group">
                                        <input name="text_alignment" type="radio" id="alignLeft" value="flex-start" class="with-gap radio-col-blue" {{ $text_alignment == 'flex-start' ? 'checked' : '' }}>
                                        <label for="alignLeft">@lang('back.banner_slider_form.align_left_label')</label>
                                        <input name="text_alignment" type="radio" id="alignCenter" value="center" class="with-gap radio-col-blue" {{ $text_alignment == 'center' ? 'checked' : '' }}>
                                        <label for="alignCenter">@lang('back.banner_slider_form.align_center_label')</label>
                                        <input name="text_alignment" type="radio" id="alignRight" value="flex-end" class="with-gap radio-col-blue" {{ $text_alignment == 'flex-end' ? 'checked' : '' }}>
                                        <label for="alignRight">@lang('back.banner_slider_form.align_right_label')</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="title">@lang('back.banner_slider_form.text_position_label')</label>
                                    <div class="form-group">
                                        <input name="text_position" type="radio" id="positionTop" value="flex-start" class="with-gap radio-col-blue" {{ $text_position == 'flex-start' ? 'checked' : '' }}>
                                        <label for="positionTop">@lang('back.banner_slider_form.position_top_label')</label>
                                        <input name="text_position" type="radio" id="positionMiddle" value="center" class="with-gap radio-col-blue" {{ $text_position == 'center' ? 'checked' : '' }}>
                                        <label for="positionMiddle">@lang('back.banner_slider_form.position_middle_label')</label>
                                        <input name="text_position" type="radio" id="positionBottom" value="flex-end" class="with-gap radio-col-blue" {{ $text_position == 'flex-end' ? 'checked' : '' }}>
                                        <label for="positionBottom">@lang('back.banner_slider_form.position_bottom_label')</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="">@lang('back.banner_slider_form.button_text_label')</label>
                                        <input placeholder="Link To" type="text" class="form-control" name="text_button" value="{{$text_button}}">                                
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="link_button">@lang('back.banner_slider_form.button_link_label')</label>
                                        <input placeholder="https://linkto.com" type="text" class="form-control" name="link_button" value="{{$link_button}}">                                
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="overlay_color">@lang('back.banner_slider_form.overlay_color_label')</label>
                                        <input placeholder="@lang('back.banner_slider_form.default_text'): #000000" type="text" class="form-control" name="overlay_color" value="{{$overlay_color}}">                                
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="overlay_transparention">@lang('back.banner_slider_form.overlay_transparency_label')</label>
                                        <select class="form-control show-tick" tabindex="-98" name="overlay_transparention">
                                            <option value="">-- @lang('back.banner_slider_form.select_text') --</option>
                                            <option value="0.1" {{ $overlay_transparention == 0.1 ? 'selected' : '' }}>0.1</option>
                                            <option value="0.2" {{ $overlay_transparention == 0.2 ? 'selected' : '' }}>0.2</option>
                                            <option value="0.3" {{ $overlay_transparention == 0.3 ? 'selected' : '' }}>0.3</option>
                                            <option value="0.4" {{ $overlay_transparention == 0.4 ? 'selected' : '' }}>0.4</option>
                                            <option value="0.5" {{ $overlay_transparention == 0.5 ? 'selected' : '' }}>0.5</option>
                                            <option value="0.6" {{ $overlay_transparention == 0.6 ? 'selected' : '' }}>0.6</option>
                                            <option value="0.7" {{ $overlay_transparention == 0.7 ? 'selected' : '' }}>0.7</option>
                                            <option value="0.8" {{ $overlay_transparention == 0.8 ? 'selected' : '' }}>0.8</option>
                                            <option value="0.9" {{ $overlay_transparention == 0.9 ? 'selected' : '' }}>0.9</option>
                                            <option value="1" {{ $overlay_transparention == 1 ? 'selected' : '' }}>1</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="title">@lang('back.banner_slider_form.status_label')</label>
                                    <div class="form-group">
                                        <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue" {{$status == '1' ? 'checked' : ''}}>
                                        <label for="statusAktif">@lang('back.banner_slider_form.enabled_radio')</label>
                                        <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue" {{$status == '0' ? 'checked' : ''}}>
                                        <label for="statusNonaktif">@lang('back.banner_slider_form.disabled_radio')</label>
                                    </div>
                                </div>

                                {{ Form::hidden('id', $id) }}
                                {{ Form::submit($button == 'Create' ? Lang::get('back.banner_slider_form.add_button') : Lang::get('back.banner_slider_form.update_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
                                <a href="{{ url('admin/sliders') }}" class="btn bg-blue-grey">@lang('back.banner_slider_form.back_button')</a>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection