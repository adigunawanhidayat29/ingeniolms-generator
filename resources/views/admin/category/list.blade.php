@extends('admin.layouts.app')

@section('title', Lang::get('back.category.title'))

@section('content')
  <section class="content">
    <div class="container-fluid">																
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>
      
      <div class="block-header">
        <h2>@lang('back.category.header')</h2>
      </div>

      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <div style="margin-bottom: 2rem;">
                <a href="{{url( \Request::route()->getPrefix() . '/categories/create')}}" class="btn btn-primary">@lang('back.category.create_button')</a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                  <thead>
                    <tr>
                      <th>@lang('back.category.name_table')</th>
                      <th>@lang('back.category.icon_table')</th>
                      <th>@lang('back.category.option_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
      </div>
    </div>
  </section>
@endsection

@push('script')
  <script type="text/javascript">
    $(function(){
      var asset_url = '{{asset_url()}}';
      $("#data").DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url( \Request::route()->getPrefix() . "/categories/serverside") }}',
        columns: [
          { data: 'title', name: 'title' },
          {
            "data": "icon",
            "render": function(data, type, row) {
              return '<img src="'+data+'" width=\"100\"//>';
            },
          },
          { data: 'action', 'searchable': false, 'orderable':false }
        ]
      });
    });
  </script>
@endpush
