@extends('admin.layouts.app')

@section('title')
	{{$button == "Create" ? Lang::get('back.category_form.create_title') : Lang::get('back.category_form.update_title')}}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>{{$button == "Create" ? Lang::get('back.category_form.create_header') : Lang::get('back.category_form.update_header')}}</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp
							{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
								<div class="form-group">
									<label for="">@lang('back.category_form.parent_category_label')</label>
									<select class="form-control" name="parent_category" id="parent_category">
										<option value="0">@lang('back.category_form.parent_category_select')</option>
										@foreach($categories as $category)
											<option {{ $category->id == $parent_category ? 'selected' : '' }} value="{{$category->id}}">{{$category->title}}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="">@lang('back.category_form.name_label')</label>
										<input placeholder="@lang('back.category_form.name_label')" type="text" class="form-control" name="title" value="{{ $title }}" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="">@lang('back.category_form.description_label')</label>
										<input placeholder="@lang('back.category_form.description_label')" type="text" class="form-control" name="description" value="{{ $description }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="">@lang('back.category_form.icon_label')</label>
										<input placeholder="Icon" type="file" class="form-control" name="icon" value="">
									</div>
								</div>

								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button == 'Create' ? Lang::get('back.category_form.create_button') : Lang::get('back.category_form.update_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/categories') }}" class="btn bg-blue-grey">@lang('back.category_form.back_button')</a>
							{{ Form::close() }}
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>
@endsection

@push('script')
	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#parent_category").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
