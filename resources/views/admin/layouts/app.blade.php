<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - Administrator - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</title>

    <!-- Favicon-->
    <link rel="shotcut icon" href="{!! Setting('favicon')->value != '' ? asset_url(Setting('favicon')->value) : asset('/img/favicon.png') !!}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{asset('/plugins/morrisjs/morris.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{asset('/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{asset('/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('/css/themes/all-themes.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('/font-awesome/css/font-awesome.min.css')}}"/>

    @stack('style')
    <style>
        .navbar-header {
            padding: 0;
        }

      .openNav {
          display: flex;
          align-items: center;
          height: 68px;
          font-size: 20px;
          color: #fff;
          padding: 1rem 2rem;
      }

      .openNav:hover {
        background-color: <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' ?>;
        text-decoration: none;
        color: #fff;
      }

      .openNav:focus {
        text-decoration: none;
        color: #fff;
      }

      .left-content-close{
        margin-left:15px !important;
      }

      .left-sidebar-close{
        width:0;
      }

      @media (max-width: 1168px){
        .openNav {
            display: none;
        }

        .left-sidebar-close{
          width:300px;
        }
      }
    </style>

    <style>
        .btn-primary,
        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary:focus {
            background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
        }

        .pagination li.active a {
            background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
        }

        .pagination > .active > a,
        .pagination > .active > span,
        .pagination > .active > a:hover,
        .pagination > .active > span:hover,
        .pagination > .active > a:focus,
        .pagination > .active > span:focus {
            background-color: <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' ?>;
        }

        .pagination > li > a:hover,
        .pagination > li > span:hover,
        .pagination > li > a:focus,
        .pagination > li > span:focus {
            color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
        }
    </style>
</head>
<body>

  <!-- Page Loader -->
  <div class="page-loader-wrapper">
      <div class="loader">
          <div class="preloader">
              <div class="spinner-layer pl-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
          <p>@lang('front.general.please_wait')</p>
      </div>
  </div>
  <!-- #END# Page Loader -->

  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <!-- #END# Overlay For Sidebars -->

  <!-- Search Bar -->
  <div class="search-bar">
      <div class="search-icon">
          <i class="material-icons">@lang('front.general.search')</i>
      </div>
      <input type="text" placeholder="@lang('front.general.start_typing')">
      <div class="close-search">
          <i class="material-icons">@lang('front.general.close')</i>
      </div>
  </div>
  <!-- #END# Search Bar -->

  @include('admin.layouts.header')
  @include('admin.layouts.sidebar')

  @yield('content')

  @include('admin.layouts.footer')

  @stack('script')

    {{-- <script>
        $.getJSON('/app/notifications', function(data){
            // alert(JSON.stringify(data));
            var datas = data.data.notifications.data
            $("#label-counter").html(data.data.notifications.total)

            var htmlNotifications = ''
            $.each(datas, function(index) {
                var item = datas[index]
                console.log(item.title);
                htmlNotifications += `
                    <li>
                        <a href="javascript:void(0);" class=" waves-effect waves-block">
                            <div class="icon-circle bg-light-green">
                                <i class="material-icons">notifications</i>
                            </div>
                            <div class="menu-info">
                                <h4>`+item.title+`</h4>
                                <p>
                                    <i class="material-icons">access_time</i> `+item.created_at+`
                                </p>
                            </div>
                        </a>
                    </li>
                `;
            });
            $("#notifications").append(htmlNotifications);
        });
    </script> --}}

    {{-- <script src="https://js.pusher.com/4.4/pusher.min.js"></script> --}}

    {{-- <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('073f0e4dcb17c5d4ff72', {
        cluster: 'ap1',
        forceTLS: true
        });

        var channel = pusher.subscribe('notifications-channel');
        channel.bind('notifications-event', function(data) {
            // console.log(data.BlastNotification.title);
            // alert(JSON.stringify(data));
            var notifications = data.BlastNotification;
            $("#notifications").prepend(`
                <li>
                    <a href="javascript:void(0);" class=" waves-effect waves-block">
                        <div class="icon-circle bg-light-green">
                            <i class="material-icons">notifications</i>
                        </div>
                        <div class="menu-info">
                            <h4>`+notifications.title+`</h4>
                            <p>
                                <i class="material-icons">access_time</i> `+notifications.created_at+`
                            </p>
                        </div>
                    </a>
                </li>
            `);
            var counter = $("#label-counter").text()
            counter = parseInt(counter) + 1;
            $("#label-counter").html(counter)

        });

        function openNav() {
          var sidebarWidth = $("#leftsidebar").width();
          if(sidebarWidth == 300){
            $("#leftsidebar").addClass('left-sidebar-close');
            $(".content").addClass('left-content-close');
          }else {
            $("#leftsidebar").removeClass('left-sidebar-close');
            $(".content").removeClass('left-content-close');
          }
        }

        /* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
        function closeNav() {
        }
    </script> --}}
</body>
</html>
