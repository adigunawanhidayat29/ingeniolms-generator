<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">@lang('back.sidebar.header')</li>

                <li class="{{ \Request::segment(2) == 'home' ? 'active' : ''}}">
                    <a href="{{Request::route()->getPrefix() . '/home'}}">
                        <i class="material-icons">dashboard</i>
                        <span>@lang('back.sidebar.dashboard')</span>
                    </a>
                </li>

                @if( isset(Setting('courses')->value) && Setting('courses')->value == 'true')
                    <li class="{{ \Request::segment(2) == 'courses' ? 'active' : ''}}">
                        <a href="{{Request::route()->getPrefix() . '/courses'}}">
                            <i class="material-icons">book</i>
                            <span>@lang('back.sidebar.course')</span>
                        </a>
                    </li>
                @endif

                <li class="{{ \Request::segment(2) == 'categories' ? 'active' : ''}}">
                    <a href="{{Request::route()->getPrefix() . '/categories'}}">
                        <i class="material-icons">label</i>
                        <span>@lang('back.sidebar.category')</span>
                    </a>
                </li>
                <li class="{{ \Request::segment(2) == 'transaction' ? 'active' : ''}}">
                    <a href="{{Request::route()->getPrefix() . '/transaction'}}">
                        <i class="material-icons">assignment</i>
                        <span>Transactions</span>
                    </a>
                </li>
                {{-- <li class="">
                    <a href="{{Request::route()->getPrefix() . '/discussion'}}">
                        <i class="material-icons">assignment</i>
                        <span>Diskusi</span>
                    </a>
                </li> --}}
                {{-- <li class="">
                    <a href="{{Request::route()->getPrefix() . 'affiliate'}}">
                        <i class="material-icons">assignment</i>
                        <span>Afiliasi</span>
                    </a>
                </li> --}}
                <li class="">
                    <a href="{{Request::route()->getPrefix() . '/promotion'}}">
                        <i class="material-icons">assignment</i>
                        <span>Promotions</span>
                    </a>
                </li>

                @if( isset(Setting('program')->value) && Setting('program')->value == 'true')
                    <li class="">
                        <a href="{{Request::route()->getPrefix() . '/program'}}">
                            <i class="material-icons">assignment</i>
                            <span>@lang('back.sidebar.program')</span>
                        </a>
                    </li>
                @endif

                @if( isset(Setting('degree')->value) && Setting('degree')->value == 'true')
                    <li class="">
                        <a href="{{Request::route()->getPrefix() . '/degree'}}">
                            <i class="material-icons">class</i>
                            <span>@lang('back.sidebar.degree')</span>
                        </a>
                    </li>
                @endif

                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>@lang('back.sidebar.user')</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/users'}}">
                                <span>@lang('back.sidebar.user_all')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/instructor'}}">
                                <span>@lang('back.sidebar.user_teacher')</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{Request::route()->getPrefix() . '/employeers'}}">
                                <i class="material-icons"></i>
                                <span>Employers</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{Request::route()->getPrefix() . '/training_providers'}}">
                                <i class="material-icons"></i>
                                <span>Training Provider</span>
                            </a>
                        </li>


                        <li>
                            <a href="{{Request::route()->getPrefix(). '/communities'}}">
                                <span>@lang('back.sidebar.user_teacher_community')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/users/import/all'}}">
                                <span>@lang('back.sidebar.user_import')</span>
                            </a>
                        </li>
                    </ul>
                </li>             
                
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">stars</i>
                        <span>@lang('back.sidebar.master_data')</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/user-levels'}}">
                                <span>@lang('back.sidebar.master_level')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/permissions'}}">
                                <span>@lang('back.sidebar.master_permission')</span>
                            </a>
                        </li>
                        {{-- <li>
                            <a href="{{Request::route()->getPrefix(). '/level-has-permission'}}">
                                <span>Level Has Permission</span>
                            </a>
                        </li> --}}
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/course-level'}}">
                                <span>@lang('back.sidebar.master_course_level')</span>
                            </a>
                        </li>
                    </ul>
                </li>        

                {{-- <li class="">
                    <a href="{{Request::route()->getPrefix() . '/sliders'}}">
                        <i class="material-icons">perm_media</i>
                        <span>@lang('back.sidebar.slider')</span>
                    </a>
                </li> --}}

                {{-- @if( isset(Setting('video')->value) && Setting('video')->value == 'true')
                    <li class="">
                        <a href="{{Request::route()->getPrefix() . '/videos'}}">
                            <i class="material-icons">theaters</i>
                            <span>Video</span>
                        </a>
                    </li>
                @endif --}}

                <li class="">
                    <a href="{{Request::route()->getPrefix() . '/blogs'}}">
                        <i class="material-icons">create</i>
                        <span>@lang('back.sidebar.blog')</span>
                    </a>
                </li>

                @if( isset(Setting('vcon')->value) && Setting('vcon')->value == 'true')
                    <li class="">
                        <a href="{{Request::route()->getPrefix() . '/vcons'}}">
                            <i class="material-icons">play_circle_outline</i>
                            <span>@lang('back.sidebar.vcon')</span>
                        </a>
                    </li>
                @endif

                <li class="">
                    <a href="{{Request::route()->getPrefix() . '/ceritificate/lists'}}">
                        <i class="material-icons">book</i>
                        <span>@lang('back.sidebar.certificate')</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{Request::route()->getPrefix() . '/cohorts'}}">
                        <i class="material-icons">group</i>
                        <span>@lang('back.sidebar.cohort')</span>
                    </a>
                </li>

                <li class="header"></li>

                {{-- <li class="">
                    <a href="{{Request::route()->getPrefix() . '/settings'}}">
                        <i class="material-icons">settings</i>
                        <span>Pengaturan</span>
                    </a>
                </li> --}}

                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings</i>
                        <span>@lang('back.sidebar.setting')</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{'/admin/setting/site'}}">
                                <span>@lang('back.sidebar.site_setting')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{'/admin/setting/auth'}}">
                                <span>@lang('back.sidebar.auth_setting')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/setting/enrolment'}}">
                                <span>@lang('back.sidebar.enrol_setting')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/setting/vcon'}}">
                                <span>@lang('back.sidebar.vcon_setting')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Request::route()->getPrefix(). '/languages'}}">
                                <span>@lang('back.sidebar.lang_setting')</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a target="_blank" href="{{'/'}}">
                        <i class="material-icons">public</i>
                        <span>@lang('back.sidebar.webpage')</span>
                    </a>
                </li>

                {{-- <li class="">
                    <a href="{{Request::route()->getPrefix() . '/settings/apis'}}">
                        <i class="material-icons">code</i>
                        <span>@lang('back.sidebar.api_setting')</span>
                    </a>
                </li> --}}

                {{-- <li class="header">LABELS</li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons col-red">donut_large</i>
                        <span>Important</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons col-amber">donut_large</i>
                        <span>Warning</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons col-light-blue">donut_large</i>
                        <span>Information</span>
                    </a>
                </li> --}}
            </ul>
        </div>
        <!-- #Menu -->

        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 - {{date("Y")}} <a href="javascript:void(0);">Ingeniolms.com - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }} </a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
            <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                <ul class="demo-choose-skin">
                    <li data-theme="red" class="active">
                        <div class="red"></div>
                        <span>Red</span>
                    </li>
                    <li data-theme="pink">
                        <div class="pink"></div>
                        <span>Pink</span>
                    </li>
                    <li data-theme="purple">
                        <div class="purple"></div>
                        <span>Purple</span>
                    </li>
                    <li data-theme="deep-purple">
                        <div class="deep-purple"></div>
                        <span>Deep Purple</span>
                    </li>
                    <li data-theme="indigo">
                        <div class="indigo"></div>
                        <span>Indigo</span>
                    </li>
                    <li data-theme="blue">
                        <div class="blue"></div>
                        <span>Blue</span>
                    </li>
                    <li data-theme="light-blue">
                        <div class="light-blue"></div>
                        <span>Light Blue</span>
                    </li>
                    <li data-theme="cyan">
                        <div class="cyan"></div>
                        <span>Cyan</span>
                    </li>
                    <li data-theme="teal">
                        <div class="teal"></div>
                        <span>Teal</span>
                    </li>
                    <li data-theme="green">
                        <div class="green"></div>
                        <span>Green</span>
                    </li>
                    <li data-theme="light-green">
                        <div class="light-green"></div>
                        <span>Light Green</span>
                    </li>
                    <li data-theme="lime">
                        <div class="lime"></div>
                        <span>Lime</span>
                    </li>
                    <li data-theme="yellow">
                        <div class="yellow"></div>
                        <span>Yellow</span>
                    </li>
                    <li data-theme="amber">
                        <div class="amber"></div>
                        <span>Amber</span>
                    </li>
                    <li data-theme="orange">
                        <div class="orange"></div>
                        <span>Orange</span>
                    </li>
                    <li data-theme="deep-orange">
                        <div class="deep-orange"></div>
                        <span>Deep Orange</span>
                    </li>
                    <li data-theme="brown">
                        <div class="brown"></div>
                        <span>Brown</span>
                    </li>
                    <li data-theme="grey">
                        <div class="grey"></div>
                        <span>Grey</span>
                    </li>
                    <li data-theme="blue-grey">
                        <div class="blue-grey"></div>
                        <span>Blue Grey</span>
                    </li>
                    <li data-theme="black">
                        <div class="black"></div>
                        <span>Black</span>
                    </li>
                </ul>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="settings">
                <div class="demo-settings">
                    <p>GENERAL SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Report Panel Usage</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Email Redirect</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                    <p>SYSTEM SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Notifications</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Auto Updates</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                    <p>ACCOUNT SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Offline</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Location Permission</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <!-- #END# Right Sidebar -->
</section>
