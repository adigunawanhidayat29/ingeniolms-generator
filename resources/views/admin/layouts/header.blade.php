<style>
    .container-fluid:before,
    .container-fluid:after {
        display: none;
    }

    .container-fluid.navbar-fluid {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .navbar-header {
        float: none;
    }

    .admin-custom-nav {
        background-color: <?= isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : '#4ca3d9' ?>;
    }

    .admin-custom-nav .navbar-header {
        display: flex;
        align-items: center;
    }

    .admin-custom-nav .navbar-header .navbar-brand {
        display: flex;
        align-items: center;
        padding: 0;
        margin-left: 0;
    }

    .admin-custom-nav .navbar-header .navbar-brand img {
        height: 50px;
    }

    .admin-custom-nav ul li {
        position: relative;
    }

    .admin-custom-nav ul li a.nav-item {
        display: flex;
        align-items: center;
        height: 68px;
        color: <?= isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' ?>;
        padding: 1rem 2rem;
        margin: 0;
    }

    .admin-custom-nav ul li a.nav-item:hover,
    .admin-custom-nav ul li ul.dropdown-canvas li.dropdown-list a.dropdown-item:hover {
        background-color: <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' ?>;
    }

    .admin-custom-nav ul li a.nav-item img {
        width: 30px;
        height: 30px;
        object-fit: cover;
    }

    .admin-custom-nav ul li ul.dropdown-canvas {
        display: none;
        position: absolute;
        min-width: 150px;
        background-color: <?= isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : '#4ca3d9' ?>;
        list-style: none;
        padding: 0;
        top: 67.5px;
        right: 0;
    }

    .admin-custom-nav ul li:hover ul.dropdown-canvas {
        display: block;
    }

    .admin-custom-nav ul li ul.dropdown-canvas li.dropdown-list a.dropdown-item {
        display: block;
        text-decoration: none;
        color: <?= isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' ?>;
        padding: 1rem 2rem;
    }

    /* .nav-tabs > li.active > a,
    .nav-tabs > li.active > a:hover,
    .nav-tabs > li.active > a:focus {
        background-color: unset;
    } */

    /* .nav-tabs > li {
        top: 0;
        margin-bottom: -2px;
    } */

    /* .nav-tabs > li > a:before {
        border-bottom: 2px solid <?= isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : '#4ca3d9' ?>;
        bottom: 0;
    } */

    .nav.nav-tabs {
        display: flex;
        background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
        border-bottom: 1px solid <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    }

    .nav.nav-tabs.nav-tabs-full {
        display: table;
    }

    .nav.nav-tabs li {
        display: table-cell;
        width: auto;
        float: none;
        top: 0;
        left: 0;
    }

    .nav.nav-tabs li a {
        color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?> !important;
        opacity: 0.75;
    }

    .nav.nav-tabs li.active a {
        background-color: rgba(0, 0, 0, 0.1);
        color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?> !important;
        opacity: 1;
    }

    .nav.nav-tabs li.active a:before {
        background-color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : '#333' ?>;
        height: 3px;
        border: none;
        margin-bottom: -3px;
    }

    /* .nav.nav-tabs {
        background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    }

    .nav.nav-tabs-transparent.indicator-primary li a.active {
        background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' }};
        color: #fff !important;
    }

    .nav-tabs-ver-container .nav-tabs-ver li a.active {
        background-color: #eee;
        color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    }

    .nav-tabs-ver-container .nav-tabs-ver li a:hover {
        color: #fff;
    }

    .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary {
        background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    }

    .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary li a {
        color: #fff;
    }

    .nav.nav-tabs-transparent li a {
        color: <?= isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' ?> !important;
    }

    .nav.nav-tabs-transparent.indicator-info li a.active {
        color: <?= isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' ?> !important;
    }

    .nav.nav-tabs.indicator-info .ms-tabs-indicator {
        background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : 'black' ?>;
    } */

    .nav-item>a:hover:before {
        height: 0 !important;
    }

    .nav-item.nav-item-category {
        margin-left: 0;
        margin-right: auto;
    }

    .nav-item>a:hover,
    .navbar .navbar-nav>li>a:hover,
    .navbar .navbar-nav>li>a:focus,
    .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary li a.active {
        background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' }};
    }

    .mobile-dropdown {
        display: none;
    }

    .list-group a,
    .list-group-item div.item-content {
        color: #424242;
    }

    @media (max-width: 768px) {
        .admin-custom-nav .navbar-header {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            width: 100%;
            height: 68px;
            margin: 0;
        }

        .admin-custom-nav .navbar-header .bars {
            color: <?= isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' ?>;
        }

        .admin-custom-nav .navbar-header .navbar-brand {
            display: none;
        }

        .admin-custom-nav ul li a.nav-item {
            padding: 1rem;
        }

        .admin-custom-nav ul li a.nav-item:focus,
        .admin-custom-nav ul li a.nav-item:hover {
            text-decoration: none;
        }

        .mobile-dropdown {
            display: flex;
            align-items: center;
            list-style: none;
            margin: 0;
        }
    }

    @media(max-width: 576px) {
        .nav.nav-tabs li a {
            padding: 1.5rem 1rem;
        }
    }
</style>

<!-- Top Bar -->
<nav class="navbar admin-custom-nav">
    <div class="container-fluid navbar-fluid">
        <div class="navbar-header">
            {{-- <a href="javascript:void(0);" class="bars"></a> --}}
            {{-- <a href="javascript:void(0);" class="openNav" onclick="openNav()">&#9776;</a> --}}
            {{-- <a class="navbar-brand" href="index.html">Administrator {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</a> --}}
            <a class="navbar-brand" href="/admin"><img src="{!! Setting('icon')->value != '' ? asset_url(Setting('icon')->value) : asset('/img/ingenio-logo.png') !!}" alt="" style="width: auto; max-width: 150px; height: auto; max-height: 50px;"></a>
            <ul class="mobile-dropdown">
                <li>
                    <a class="nav-item" href="{{url('/')}}"><i class="material-icons">public</i></a>
                </li>
                {{-- <li>
                    <a href="javascript:void(0);" class="nav-item js-search" data-close="true"><i class="material-icons">search</i></a>
                </li> --}}
                <li>
                    <a class="nav-item" href="#"><img src="https://assets.ingeniolms.com/uploads/users/default.png" alt="" style="width: 30px; height: 30px; object-fit: cover;"></a>
                    <ul class="dropdown-canvas">
                        <li class="dropdown-list">
                            <a class="dropdown-item" href="{{url('profile')}}">Profil</a>
                        </li>
                        <li class="dropdown-list">
                            <a class="dropdown-item" href="{{url('logout')}}">@lang('back.navbar.profile_logout')</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{url('/')}}" class="nav-item"><i class="material-icons" style="margin-right: 0.5rem;">public</i> @lang('back.navbar.webpage')</a>
                </li>
                {{-- <li>
                    <a href="javascript:void(0);" class="nav-item js-search" data-close="true" title="@lang('back.navbar.search')"><i class="material-icons">search</i></a>
                </li> --}}
                <li>
                    <a class="nav-item" href="#" title="@lang('back.navbar.profile')"><img src="https://assets.ingeniolms.com/uploads/users/default.png" alt=""></a>
                    <ul class="dropdown-canvas">
                        {{-- <li class="dropdown-list">
                            <a class="dropdown-item" href="{{url('profile')}}">@lang('back.navbar.profile_link')</a>
                        </li> --}}
                        <li class="dropdown-list">
                            <a class="dropdown-item" href="{{url('logout')}}">@lang('back.navbar.profile_logout')</a>
                        </li>
                    </ul>
                </li>
                {{-- <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li> --}}
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
