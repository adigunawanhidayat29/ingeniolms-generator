@extends('admin.layouts.app')

@section('title')
	{{ 'Course' }}
@endsection


@section('content')
  <section class="content">
    <div class="container-fluid">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>
      <div class="block-header">
        <h2>PESERTA KELAS - {{$courses->title}}</h2>
      </div>

      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <div style="margin-bottom: 2rem;">
                <a class="btn bg-blue-grey" href="{{url('admin/course/show/'. $courses->id)}}">Kembali</a>
              </div>

              <div class="table-responsive">                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $no = 1 @endphp
                    @foreach($course_users as $course_user)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>{{$course_user->name}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>                     
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>

@endsection
