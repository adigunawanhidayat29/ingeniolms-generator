@extends('admin.layouts.app')

@section('title', 'Peserta - ' . $Course->title)

@push('style')
	<style>
		.nav-tabs-ver-container .nav-tabs-ver {
			padding: 0;
			margin: 0;
		}

		.nav-tabs-ver-container .nav-tabs-ver:after {
			display: none;
		}
	</style>
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
	<link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Daftar Peserta</h2>
		</div>

		<div class="">
			<div class="row">
				<div class="">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li><a href="/admin/courses">Kelola Kelas</a></li>
						<li><a href="/admin/course/manage/{{$Course->id}}">{{ $Course->title }}</a></li>
						<li>Daftar Peserta</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row clearfix">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="header">
							<h2>Daftar Peserta</h2>
							<ul class="header-dropdown m-r--5">
									<li class="dropdown">
											<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
													role="button" aria-haspopup="true" aria-expanded="false">
													<i class="material-icons">more_vert</i>
											</a>
											<ul class="dropdown-menu pull-right">
													<li><a href="javascript:void(0);" data-toggle="modal" data-target="#modalSetupEnrollment">Pengaturan Enrollment</a></li>
											</ul>
									</li>
							</ul>
					</div>
					<div class="body">
						<div class="row">
							<div class="col-md-3">
								<img src="{{$Course->image}}" alt="{{ $Course->title }}" class="img-responsive">
								<div class="card no-shadow">
									<ul class="nav nav-tabs-ver" role="tablist">
										<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$Course->id}}"><i class="fa fa-chevron-circle-left"></i> Kembali Ke Kelas</a></li>
										<li class="nav-item"><a class="nav-link active" href="/course/atendee/{{$Course->id}}"><i class="fa fa-users"></i> Daftar Peserta</a></li>
										<li class="nav-item"><a class="nav-link" href="/course/grades/{{$Course->id}}"><i class="fa fa-address-book-o"></i> Grade Book</a></li>              
										<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$Course->id}}"><i class="fa fa-certificate"></i> Sertifikat</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-9">
									{{-- Discussions --}}
										<div class="d-flex align-items-center justify-content-between mb-2">
											<h3 class="headline headline-sm mt-0 mb-0">Daftar Peserta</h3>
											<div class="text-right">
												<a href="/course/completion/{{$Course->id}}/export" target="_blank" class="btn btn-sm btn-raised btn-success mt-0 mb-0">Ekspor</a>
												<a href="javascript:void(0);" data-toggle="modal" data-target="#modalSetupEnrollment" class="btn btn-sm btn-raised btn-default mt-0 mb-0">Pengaturan Enrollment</a>
												<div class="btn-group" role="group">
														<button type="button" class="btn btn-primary waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																Enroll Peserta
																<span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
																<li><a data-toggle="modal" data-target="#modalEnrollUser" href="javascript:void(0);">Manual</a></li>
																<li><a data-toggle="modal" data-target="#modalEnrollUserCohort" href="javascript:void(0);">Cohort</a></li>
														</ul>
												</div>
											</div>
										</div>
	
										<div class="body table-responsive">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>No</th>
														<th>Nama</th>
														<th>Persentase</th>
														<th class="text-right">Pilihan</th>
													</tr>
												</thead>
												
												<tbody>
													@foreach($completions as $index => $completion)
														<tr>
															<td>{{$index + 1}}</td>
															<td>{{$completion['name']}}</td>
															<td>{{round($completion['percentage'], 2)}}%</td>
															<td class="text-right">
																<a class="btn-circle btn-circle-primary btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#userProgressDetail{{$completion['user_id']}}" title="Detail pembelajaran"><i class="fa fa-info-circle"></i></a>
																<a onclick="return confirm('Yakin akan menghapus peserta ini?')" class="btn-circle btn-circle-danger btn-circle-raised btn-circle-sm" href="/admin/course/user/delete/{{Request::segment(4)}}/{{$completion['user_id']}}" title="Hapus peserta"><i class="fa fa-trash"></i></a>
															</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
								
							</div>
						</div>

					</div>
				</div>
			</div>

			<!-- Modal -->
			@foreach($completions as $index => $completion)
				<div class="modal" id="userProgressDetail{{$completion['user_id']}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
					<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="d-flex align-items-center justify-content-between mb-2">
									<h3 class="headline headline-sm m-0">Detail Pembelajaran <span>{{$completion['name']}}</span></h3>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">
											<i class="zmdi zmdi-close"></i>
										</span>
									</button>
								</div>
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr style="background-color: #e6e6e6;">
												<th>Section</th>
												<th>Content</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											{{-- <tr style="{{$content_progress ? 'background-color: #eee; color: #999;' : ''}}"> --}}
											
											@foreach($completion['section_data'] as $section)
												@foreach($section['section_contents'] as $content)
													@php
														$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => $completion['user_id'], 'status' => '1'])->first();
													@endphp
													<tr style="{{$content_progress ? 'background-color: #eee; color: #999;' : ''}}">
														<td>{{$section['title']}}</td>
														<td>{{$content->title}}</td>
														<td>
															<span class="badge {{ $content_progress ? 'badge-success' : 'badge-danger' }}">{{ $content_progress ? 'Selesai' : 'Belum selesai' }}</span>
														</td>
													</tr>
												@endforeach
											@endforeach
											

											{{-- <tr style="background-color: #eee; color: #999;">
												<td>"Section Name 2"</td>
												<td>"Content Name 2"</td>
												<td>
													<span class="badge badge-success">Selesai</span>
												</td>
											</tr>
											<tr>
												<td>"Section Name 3"</td>
												<td>"Content Name 3"</td>
												<td>
													<span class="badge badge-danger">Belum selesai</span>
												</td>
											</tr> --}}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach

		</div>

	</div>
</section>

<div class="modal fade" id="modalEnrollUser" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
					<div class="modal-header">
							<h4 class="modal-title" id="largeModalLabel">Enroll Peserta</h4>
					</div>
					<form action="/admin/courses/{{ $Course->id }}/enroll/users" method="post">
						{{ csrf_field() }}
						<div class="modal-body">
								<select name="user_id[]" id="duallist" multiple>
										@foreach ($users as $user)  
												<option @foreach($Course->course_user as $course_user) @if($user->id == $course_user->user_id)selected="selected"@endif @endforeach value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
										@endforeach
								</select>
						</div>
						<div class="modal-footer">
								<button type="submit" class="btn btn-link waves-effect">SIMPAN</button>
								<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
						</div>
				</form>
			</div>
	</div>
</div>

<div class="modal fade" id="modalEnrollUserCohort" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
					<div class="modal-header">
							<h4 class="modal-title" id="largeModalLabel">Enroll Peserta Dengan Cohort</h4>
					</div>
					<form action="/admin/courses/{{ $Course->id }}/enroll/cohort" method="post">
						{{ csrf_field() }}
						<div class="modal-body">
								<select name="cohort_id[]" id="cohortduallist" multiple>
										@foreach ($cohorts as $cohort)  
												<option @foreach($Course->course_cohort as $course_cohort) @if($cohort->id == $course_cohort->cohort_id)selected="selected"@endif @endforeach value="{{ $cohort->id }}">{{ $cohort->name }}</option>
										@endforeach
								</select>
						</div>
						<div class="modal-footer">
								<button type="submit" class="btn btn-link waves-effect">SIMPAN</button>
								<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
						</div>
				</form>
			</div>
	</div>
</div>

<div class="modal fade" id="modalSetupEnrollment" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
					<div class="modal-header">
							<h4 class="modal-title" id="largeModalLabel">Pengaturan Enrollment Kelas</h4>
					</div>
					<form action="{{url(\Request::route()->getPrefix() . '/courses/set-enrollment/' . $Course->id)}}" method="post" enctype="multipart/form-data" >
							{{csrf_field()}}
							<div class="modal-body">
									<label for="">Default Enrollment?</label>
									<div class="form-group">
											<div class="form-line">
													@php
														$enrollmentSelected = [];
														if ($Course->enrollment_type != null) {
															$enrollmentSelected = explode(',', $Course->enrollment_type);
														} else {
															$enrollmentSelected = explode(',', Setting('enrollment')->value);
														}
													@endphp
													
													<input type="checkbox" class="filled-in" id="manual" name="enrollment_type[]" {{ in_array("manual", $enrollmentSelected) ? 'checked' : '' }} value="manual">
													<label for="manual">Manual</label>
													<input type="checkbox" class="filled-in" id="self" name="enrollment_type[]" {{ in_array("self", $enrollmentSelected) ? 'checked' : '' }} value="self">                                         
													<label for="self">Self Enrollment</label>
													<input type="checkbox" class="filled-in" id="cohort" name="enrollment_type[]" {{ in_array("cohort", $enrollmentSelected) ? 'checked' : '' }} value="cohort">                                            
													<label for="cohort">Cohort</label>
													
											</div>
									</div>
							<button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
					</form>
			</div>
	</div>
</div>

@endsection

@push('script')
    <script src="/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    
    <script>
        var demo1 = $('#duallist').bootstrapDualListbox({
            nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
						refresh: true
        });

        var demo2 = $('#cohortduallist').bootstrapDualListbox({
            nonSelectedListLabel: 'Non-selected',
            selectedListLabel: 'Selected',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: true,
						refresh: true
        });

				$(function () {
					demo1.trigger('bootstrapDualListbox.refresh' , true);
					demo2.trigger('bootstrapDualListbox.refresh' , true);
				});
    </script>
@endpush