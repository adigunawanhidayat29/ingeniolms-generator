@extends('admin.layouts.app')

@section('title')
	{{$button == "Create" ? Lang::get('back.course_form.create_title') : Lang::get('back.course_form.update_title')}}
@endsection

@push('style')
	<link rel="stylesheet" href="{{asset('select2/css/select2.min.css')}}">
	<link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>{{$button == "Create" ? Lang::get('back.course_form.create_header') : Lang::get('back.course_form.update_header')}}</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">

							{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
								<div class="form-group">
									<div class="form-line">
										<label for="title">@lang('back.course_form.name_label')</label>
										<input placeholder="@lang('back.course_form.name_label')" type="text" class="form-control" name="title" value="{{ $title }}" required>
										@if ($errors->has('title'))
											<span class="text-danger">{{ $errors->first('title') }}</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="title">@lang('back.course_form.sub_name_label')</label>
										<input placeholder="@lang('back.course_form.sub_name_label')" type="text" class="form-control" name="subtitle" value="{{ $subtitle }}">
										@if ($errors->has('subtitle'))
											<span class="text-danger">{{ $errors->first('subtitle') }}</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<label for="title">@lang('back.course_form.description_label')</label>
									<textarea name="description" id="description" class="summernote">{{ $description }}</textarea>
								</div>

								<div class="form-group">
									<label for="name">@lang('back.course_form.category_label')</label>
									<select class="form-control" name="id_category" id="id_category" required>
										<option value="">@lang('back.course_form.category_select')</option>
										@foreach($categories as $category)
											<option {{ $category->id == $id_category ? 'selected' : '' }} value="{{$category->id}}">{{$category->title}}</option>
										@endforeach()
									</select>
								</div>

								<div class="form-group">
									@if($button == "Update")
										<img src="{{$image}}" height="80">
									@endif
									<div class="form-line">
										<label for="title">@lang('back.course_form.image_label')</label>
										<input type="file" class="form-control" name="image">
										<input type="hidden" name="old_image" value="{{ $image }}">
									</div>
								</div>

								<div class="form-group">
									<label for="name">@lang('back.course_form.author_label')</label>
									<select class="form-control" name="id_author" id="id_author" required>
										<option value="">@lang('back.course_form.author_select')</option>
										@foreach($authors as $author)
											<option {{ $author->id == $id_author ? 'selected' : '' }} value="{{$author->id}}">{{$author->name}}</option>
										@endforeach()
									</select>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="price">@lang('back.course_form.price_label')</label>
										<input placeholder="@lang('back.course_form.price_label')" type="number" class="form-control" name="price" value="{{ $price }}">
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<label for="password">@lang('back.course_form.code_label')</label>
										<input id="password" placeholder="@lang('back.course_form.code_label')" type="text" class="form-control" name="password" value="{{ $password }}">
									</div>
								</div>

								<div class="form-group">
									<label for="name">@lang('back.course_form.course_level_label')</label>
									<select class="form-control" name="id_level_course" id="id_level_course" required>
										<option value="">@lang('back.course_form.course_level_select')</option>
										@foreach($course_levels as $course_level)
											<option {{ $course_level->id == $id_level_course ? 'selected' : '' }} value="{{$course_level->id}}">{{$course_level->title}}</option>
										@endforeach()
									</select>
								</div>

								<div class="form-group">
									<label for="name">@lang('back.course_form.course_publish_label')</label>
									<div class="switch">
										<label>@lang('back.course_form.no')<input type="checkbox" {{$public == '1' ? 'checked' : ''}} name="public"><span class="lever"></span>@lang('back.course_form.yes')</label>
									</div>
								</div>

								<input type="hidden" id="model" name="model" value="subscription">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button == "Create" ? Lang::get('back.course_form.create_button') : Lang::get('back.course_form.update_button') , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('admin/courses') }}" class="btn bg-blue-grey">@lang('back.course_form.back_button')</a>
							{{ Form::close() }}

						</div>
					</div>
					<!-- #END# Task Info -->
				</div>
			</div>
		</div>
	</section>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script type="text/javascript">
		CKEDITOR.replace('description');
		CKEDITOR.replace('section_description');
	</script> --}}
	<script src="{{asset('summernote/summernote.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
            height: 300
        });
	});
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{asset('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#id_category").select2();
		$("#id_level_course").select2();
		$("#id_author").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
