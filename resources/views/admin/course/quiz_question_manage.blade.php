@extends('admin.layouts.app')

@section('title')
	{{ 'Kelola Kuis ' . $quiz->name }}
@endsection

@push('style')

	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<style media="screen">
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			background: #ccc;
			color: #333!important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:active {
			background: none;
			color: black!important;
		}
		select.form-control:not([size]):not([multiple]){
			height: auto !important;
		}
	</style>
	<style media="screen">
		.dq-input-group{
			width: 100%;
			display: flex;
		}
			.dq-input-group i{
				position: absolute;
				left: 0;
				padding: 0.5rem 1rem;
				align-self: center;
				z-index: 1;
				font-size: 18px;
				border-right: 1px solid #ccc;
			}
			.dq-input-group .form{
				position: relative;
				border: 1px solid #ccc;
				border-radius: 2px;
				background: #f9f9f9;
				box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
			}
			.dq-input-group span{
				align-self: center;
				margin: 0 0.5rem;
			}
			.date{
				width: 100%;
				padding: 0.5rem 0rem 0.5rem 4.5rem;
			}
			.select{
				width: auto;
				padding: 0.5rem 1rem;
			}
			.QuestionSortable{
	      border:0.2px solid #dfe6e9;
	      min-height:60px;
	      /* padding:10px; */
	    }

			.nav {
			    padding-left: 10px;
			    margin-bottom: 0;
			    list-style: none;
			}

			.panel-group .panel .panel-heading a {
			    display: inline-flex;
			    padding: 10px 15px;
			}
	</style>
@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<ul class="breadcrumb">
				<li><a href="/admin"><h2>Home</h2></a></li>
				<li><a href="/admin/courses">Kelas</a></li>
				<li><a href="/admin/course/manage/{{$course->id}}">{{$course->title}}</a></li>
				<li>Manage Kuis - {{$quiz->name}}</li>
			</ul>
		</div>

			<!-- Task Info -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">

				</div>
			</div>
		</div>
		<div class="">
			<!-- <br> -->
			<div class="row">
				<div class="col-md-3">
					<img width="100%" src="{{$course->image}}" alt="{{$course->title}}">
				</div>
				<div class="col-md-9">

					<div class="card">
					    <!-- Nav tabs -->
					    <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
					        <li class="nav-item active"><a class="nav-link withoutripple active" href="#question" aria-controls="question" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Pertanyaan</span></a></li>
					        <li class="nav-item"><a class="nav-link withoutripple" href="#setting" aria-controls="setting" role="tab" data-toggle="tab" id="button-nav-pengaturan"><span class="d-none d-sm-inline">Pengaturan</span></a></li>
					        <li class="nav-item"><a class="nav-link withoutripple" href="#preview" aria-controls="preview" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Preview</span></a></li>
					        <li class="nav-item"><a class="nav-link withoutripple" href="#result" aria-controls="result" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Hasil</span></a></li>
					    </ul>

					    <div class="card-block">
					        <!-- Tab panes -->
					        <div class="tab-content" style="padding: 20px;">
					            <div role="tabpanel" class="tab-pane fade active active in" id="question">
												<div class="btn-group" style="margin-bottom:20px;">
											    <button type="button" class="btn btn btn-primary dropdown-toggle btn-raised" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										        Tambah Pertanyaan <i class="zmdi zmdi-chevron-down right"></i>
											    </button>
											    <ul class="dropdown-menu">
														{{-- <li><a href="{{url('admin/course/quiz/question/create/'.$quiz->id)}}">Pilihan Ganda</a></li>
														<li><a href="{{url('admin/course/quiz/question/create/'.$quiz->id)}}">Benar Salah</a></li>
														<li role="separator" class="dropdown-divider"></li> --}}
														@foreach($quiz_types as $quiz_type)
															<li><a href="{{url('admin/course/quiz/question/create/'.$quiz->id.'?quiz_type='.$quiz_type->id)}}">{{$quiz_type->type}}</a></li>
														@endforeach()
														<li><a data-toggle="modal" data-target="#modalPageBreak" href="#/">Page Break</a></li>
														<li role="separator" class="dropdown-divider"></li>
										        {{-- <li><a href="{{url('admin/course/quiz/question/create/'.$quiz->id)}}">Buat Baru</a></li> --}}
														<li><a href="#" data-toggle="modal" data-target="#ModalShowBankSoal">Ambil Dari Bank</a></li>
										        <li><a href="{{url('admin/course/quiz/question/import/'.$quiz->id)}}">Import</a></li>
											    </ul>
												</div>

												<div class="panel-group PageBreakSortable" id="accordion">

													@if(count($quiz_questions_without_page_breaks) > 0)
														<div class="QuestionSortable mt-2 mb-2" id="">
															@foreach($quiz_questions_without_page_breaks as $quiz_questions_without_page_break)
																<li style="background:white;list-style:none;border: 1px solid #bbb; margin: 5px; padding: 10px;" id="{{$quiz_questions_without_page_break->id}}">
																	{{$quiz_questions_without_page_break->question}}
																	<div class="text-right">
																		<a class="btn btn-default btn-sm btn-raised" href="{{url('admin/course/quiz/question/update/'.$quiz->id.'/'.$quiz_questions_without_page_break->id)}}">Edit</a>
																		<a onclick="return confirm('Are You sure?')" class="btn btn-danger btn-sm btn-raised" href="{{url('admin/course/quiz/question/delete/'.$quiz->id.'/'.$quiz_questions_without_page_break->id)}}">Hapus</a>
																	</div>
																</li>
															@endforeach
														</div>
													@endif

													@foreach($quiz_page_breaks as $quiz_page_break)
														<div class="panel panel-default" id="section{{$quiz_page_break->id}}" data-id="{{$quiz_page_break->id}}" style="background:white">
															<a data-toggle="collapse" data-parent="#accordion{{$quiz_page_break->id}}" href="#collapse{{$quiz_page_break->id}}">
													      <div class="panel-heading">
													        <h4 class="panel-title">{{$quiz_page_break->title}}</h4>
																	<p class="mt-2" style="color:black">{{$quiz_page_break->description}}</p>
																	<div class="text-right">
																		<a href="#/" id="editPageBreak" data-id="{{$quiz_page_break->id}}" data-title="{{$quiz_page_break->title}}" data-description="{{$quiz_page_break->description}}">Edit</a>
																		<a onclick="return confirm('PERINGATAN! jika Anda menghapus page break ini secara langsung maka pertanyaan juga akan langsung terhapus. Diharapkan untuk memindahkan pertanyaan terlebih dulu. Setuju?')" href="/course/quiz/page-break/delete/{{$quiz_page_break->id}}">Hapus</a>
																	</div>
													      </div>
															</a>
												      <div id="collapse{{$quiz_page_break->id}}" class="panel-collapse collapse show">
												        <div class="panel-body">
																	<div class="QuestionSortable" id="{{ $quiz_page_break->id }}">
																		@foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
																			<li style="background:#EEEEEE;list-style:none;border: 1px solid #EEEEEE; border-radius:3px; margin: 5px; padding: 10px" id="{{$quiz_question->id}}" data-section="{{$quiz_question->page_break_id}}">
																				{!!$quiz_question->question!!}
																				<div class="text-right">
																					<a class="btn btn-default btn-sm btn-raised" href="{{url('admin/course/quiz/question/update/'.$quiz->id.'/'.$quiz_question->id . '?quiz_type=' . $quiz_question->quiz_type_id)}}">Edit</a>
																					<a onclick="return confirm('Are You sure?')" class="btn btn-danger btn-sm btn-raised" href="{{url('admin/course/quiz/question/delete/'.$quiz->id.'/'.$quiz_question->id)}}">Hapus</a>
																				</div>
																			</li>
																		@endforeach
																	</div>
																</div>
												      </div>
												    </div>
													@endforeach
												</div>
												@if($quiz->status == '0')
													<button type="button" class="btn btn btn-primary btn-raised" onclick="window.location.href='{{url('course/quiz/publish/'.$quiz->id.'/'.$course->id)}}'">Publikasikan Kuis</button>
												@else
													<button type="button" class="btn btn btn-danger btn-raised" onclick="window.location.href='{{url('course/quiz/unpublish/'.$quiz->id.'/'.$course->id)}}'">Batalkan Publikasi Kuis</button>
												@endif
					            </div>

											<div role="tabpanel" class="tab-pane fade" id="preview">
												<a href="/admin/course/quiz/question/preview/{{$quiz->id}}" class="btn btn-raised btn-primary">Preview Kuis</a>
											</div>
					            <div role="tabpanel" class="tab-pane fade" id="setting">
												{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
													@if (Session::has('error'))
														<div class="alert alert-warning alert-dismissible">
															<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
															{!! Session::get('error') !!}
														</div>
													@endif
													<div class="form-group">
														<div class="form-line">
															<label for="name">Judul Kuis</label>
															<input placeholder="Judul Kuis" type="text" class="form-control" name="name" value="{{ $name }}" required>
														</div>
													</div>

													<div class="form-group">
														<label for="title">Deskripsi / Penjelasan</label>
														<textarea name="description" id="description">{{ $description }}</textarea>
													</div>
													{{-- <div class="form-group">
														<label for="title">Acak?</label><br>
														<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
														<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
													</div> --}}

													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<label for="name">Tanggal Mulai</label>
																<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : '' }}" required>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label for="name">Jam Mulai</label>
																<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ isset($time_start) ? date("H:i:s", strtotime($time_start)) : '' }}" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<label for="name">Tanggal Selesai</label>
																<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}" required>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label for="name">Jam Selesai</label>
																<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ isset($time_end) ? date("H:i:s", strtotime($time_end)) : '' }}" required>
															</div>
														</div>
													</div>
													{{-- <div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<label>Tanggal Mulai</label>
																<div class="dq-input-group">
																	<i class="fa fa-calendar"></i>
																	<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form date datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : '' }}" required>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label>Jam Mulai</label>
																<div class="dq-input-group">
																	<select class="form select" name="">
																		<option>-</option>
																		<option value="01">01</option>
																		<option value="02">02</option>
																		<option value="03">03</option>
																		<option value="04">04</option>
																		<option value="05">05</option>
																		<option value="06">06</option>
																		<option value="07">07</option>
																		<option value="08">08</option>
																		<option value="09">09</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																	</select>
																	<span>:</span>
																	<select class="form select" name="">
																		<option>-</option>
																		<option value="00">00</option>
																		<option value="15">15</option>
																		<option value="30">30</option>
																		<option value="45">45</option>
																	</select>
																	<span></span>
																	<select class="form select" name="">
																		<option value="AM">AM</option>
																		<option value="PM">PM</option>
																	</select>
																</div>
															</div>
														</div>
													</div> --}}

													{{-- <div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<label>Tanggal Selesai</label>
																<div class="dq-input-group">
																	<i class="fa fa-calendar"></i>
																	<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form date datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}" required>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label>Jam Selesai</label>
																<div class="dq-input-group">
																	<select class="form select" name="">
																		<option>-</option>
																		<option value="01">01</option>
																		<option value="02">02</option>
																		<option value="03">03</option>
																		<option value="04">04</option>
																		<option value="05">05</option>
																		<option value="06">06</option>
																		<option value="07">07</option>
																		<option value="08">08</option>
																		<option value="09">09</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																	</select>
																	<span>:</span>
																	<select class="form select" name="">
																		<option>-</option>
																		<option value="00">00</option>
																		<option value="15">15</option>
																		<option value="30">30</option>
																		<option value="45">45</option>
																	</select>
																	<span></span>
																	<select class="form select" name="">
																		<option value="AM">AM</option>
																		<option value="PM">PM</option>
																	</select>
																</div>
															</div>
														</div>
													</div> --}}

													<div class="form-group">
														<label for="name">Durasi (Menit)</label>
														<input placeholder="60" type="number" class="form-control" name="duration" value="{{ $duration }}" required>
													</div>
													<div class="form-group">
														<label for="name">Kesempatan / Percobaan</label>
														<input placeholder="3" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
													</div>
													<div class="form-group">
														{{  Form::hidden('id', $id) }}
														{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
														{{-- <a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a> --}}
													</div>
												{{ Form::close() }}
					            </div>
					            <div role="tabpanel" class="tab-pane fade" id="result">
												<div class="table-responsive">
													<table class="table table-hover" width="100%" id="data">
														<thead>
															<tr>
																<th>Nama</th>
																<th>Pilihan</th>
															</tr>
														</thead>
													</table>
												</div>
					            </div>
					        </div>
					    </div>
					</div> <!-- card -->

				</div>
			</div>
		</div>

		{{-- add bank soal --}}
		<div class="modal" id="ModalShowBankSoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">Tambah Konten Folder</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
							<div class="form-group">
								<div class="table-responsive table-cs">
									<table class="table table-striped d-table edit-group">
										<thead>
											<tr>
												<th>Nama Konten</th>
												<th>Tanggal Dibuat</th>
												<th class="text-center">Directory</th>
												<th class="text-center">Tipe</th>
											</tr>
										</thead>
										<tbody>
											@foreach($group as $groups)
												<tr style="color: #4ca3d9;">
													<td>
														@if($groups->question_bank == 1)
														<a href="{{url('admin/course/quiz/get', [$quiz->id, $groups->id])}}"><i class="fa fa-folder mr-1"></i> {{$groups->group_name}}</a>
														@else
														<a href="#"><i class="fa fa-folder-open mr-1"></i> {{$groups->group_name}}</a>
														@endif
													</td>
													<td>{{$groups->created_at}}</td>
													<td class="text-center">
														@if($groups->user_id == Auth::user()->id)
															@if($groups->group_code == null && $groups->status == '0')
																My Library
															@elseif($groups->status == '1')
																Public Library
															@else
																Shared Library
															@endif
														@else
															Shared Library
														@endif
													</td>
													<td class="text-center">{{$groups->question_bank == 1 ? 'Bank Soal' : 'Folder'}}</td
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
		{{-- add bank soal --}}

		{{-- add page break --}}
		<div class="modal fade" id="modalPageBreak" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
		    <div class="modal-content">
					<form class="" action="/course/quiz/create/pagebreak/{{$quiz->id}}" method="post">
						{{csrf_field()}}
			      <div class="modal-header">
			        <h4 class="modal-title" id="">Tambah Page Break</h4>
			      </div>
			      <div class="modal-body">
							<div class="form-group">
								<label for="title">Judul</label>
								<input type="text" name="title" value="" class="form-control">
							</div>
							<div class="form-group">
								<label for="title">Deskripsi</label>
								<textarea name="description" class="form-control" rows="8" cols="80"></textarea>
							</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			        <button type="submit" class="btn btn-primary btn-raised">Simpan</button>
			      </div>
					</form>
		    </div>
		  </div>
		</div>
		{{-- add page break --}}

		{{-- edit page break --}}
		<div class="modal fade" id="modalEditPageBreak" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
		    <div class="modal-content">
					<form class="" action="/course/quiz/pagebreak/update" method="post">
						{{csrf_field()}}
						<input type="hidden" name="id" id="pageBreakId" value="">
			      <div class="modal-header">
			        <h4 class="modal-title" id="">Edit Page Break</h4>
			      </div>
			      <div class="modal-body">
							<div class="form-group">
								<label for="title">Judul</label>
								<input type="text" id="pageBreakTitle" name="title" value="" class="form-control">
							</div>
							<div class="form-group">
								<label for="title">Deskripsi</label>
								<textarea name="description" id="pageBreakDescription" class="form-control" rows="8" cols="80"></textarea>
							</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			        <button type="submit" class="btn btn-primary btn-raised">Simpan</button>
			      </div>
					</form>
		    </div>
		  </div>
		</div>
		{{-- edit page break --}}

		{{-- edit answer --}}
		<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="">Edit Pilihan</h4>
		      </div>
		      <div class="modal-body">
						<div class="form-group">
							<label for="title">Pilihan</label>
							<input type="text" id="edit_text_answer" value="" class="form-control">
						</div>

						<div class="form-group">
							<label for="title">Status Pilihan ?</label>
							<select class="form-control selectpicker" id="edit_text_answer_correct">
								<option value="0">Salah</option>
								<option value="1">Benar</option>
							</select>
						</div>
						<input type="hidden" id="edit_text_answer_id" value="">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
		        <button type="button" class="btn btn-primary btn-raised" id="save_edit_answer">Simpan</button>
		      </div>
		    </div>
		  </div>
		</div>
		{{-- edit answer --}}
	</div>
</section>
@endsection

@push('script')

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('admin/course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}

	{{-- participant list --}}
	<script src="{{url('js/jquery.dataTables.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script> <!-- Datatable -->
	<script type="text/javascript">
	  $(function(){
			var asset_url = '{{asset_url()}}';
	    $($("#data").DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: '{{ url("course/quiz/participant/serverside/".$quiz->id) }}',
	      columns: [
					{ data: 'name', name: 'name' },
	        { data: 'action', 'searchable': false, 'orderable':false }
	      ],
				"oLanguage": {
					"sLengthMenu": "Tampilkan _MENU_",
					"sSearch": "Cari: ",
		      "oPaginate": {
						"sPrevious": "<",
						"sNext": ">",
					}
		    },
	    }).table().container()).removeClass( 'form-inline' );
	  });
  </script>

	{{-- participant list --}}

	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>

<script type="text/javascript">
var js = document.createElement("script");
js.type = "text/javascript";
js.src = "WIRISplugins.js?viewer=image";
document.head.appendChild(js);
</script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

{{-- sortable --}}
<script src="/jquery-ui/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".PageBreakSortable" ).sortable({
      axis: 'y',
      connectWith: '.PageBreakSortable',
      helper: 'clone',
      out: function(event, ui) {
        var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
        console.log(itemOrder)

        $.ajax({
          url : '/course/quiz/page-break/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'quiz_id' : '{{$quiz->id}}',
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    });
    $( ".PageBreakSortable" ).disableSelection();
  });
</script>

<script>
  $( function() {
    var quiz_page_break_id = ''
    $( ".QuestionSortable" ).sortable({
      axis: 'y',
      connectWith: '.QuestionSortable',
      helper: 'clone',
      placeholder: "placeholder",
      over:function(event,ui){
        quiz_page_break_id = $('.placeholder').parent().attr('id');
      },
      out: function(event, ui) {
        var sequence = ui.item.index();
        var question_id = ui.item.attr('id');
        var itemOrder = $(this).sortable('toArray', { attribute: 'id' });
        console.log(itemOrder);
        // console.log(question_id);
        console.log(quiz_page_break_id);
        $.ajax({
          url : '/admin/course/quiz/question/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'quiz_page_break_id' : quiz_page_break_id,
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    }).disableSelection();
  });
</script>
{{-- sortable --}}

{{-- edit pagebreak  --}}
<script type="text/javascript">
	$(function(){
		$(document).on('click','#editPageBreak',function(e){
			e.preventDefault();
			var data_id = $(this).attr('data-id');
			var data_title = $(this).attr('data-title');
			var data_description = $(this).attr('data-description');
			$("#pageBreakId").val(data_id);
			$("#pageBreakTitle").val(data_title);
			$("#pageBreakDescription").val(data_description);
			$("#modalEditPageBreak").modal('show');
		});
	});
</script>
{{-- edit pagebreak  --}}

{{-- preview trigger --}}
@if (Session::has('error'))
	<script>
		$('#button-nav-pengaturan').trigger('click');
	</script>
@endif
@endpush
