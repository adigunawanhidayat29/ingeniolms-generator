@extends('admin.layouts.app')

@section('title')
@lang('back.course.title_preview')
@endsection

@push('style')
	<link rel="stylesheet" href="{{asset('/css/style.dq-light-blue.min.css')}}">
	<link rel="stylesheet" href="{{asset('/css/dq-style.css')}}">
	<link rel="stylesheet" href="{{asset('/css/dq-style-2.css')}}">
	<link rel="stylesheet" href="{{asset('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('/css/bootstrap-editable.css')}}">
  <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <style media="screen">
    .editableform .form-group{
      margin: 0px !important;
      padding: 0px !important;
    }
    .editableform .form-group .form-control{
      height: 33px;
      padding: 0px;
      margin-top: -1.3rem;
      margin-bottom: 0px;
      line-height: auto;
    }
    .editable-input .form-control{
      color: white !important;
      width: auto;
      font-size: 2.4rem;
      font-weight: bold;
    }
    .editable-buttons{
      display: none;
    }
  </style>
  <style>
    /* Style the tab */
    .tab {
			float: left;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
			width: 30%;
			height: auto;
    }

    /* Style the buttons inside the tab */
    .tab button {
			display: block;
			background-color: inherit;
			color: black;
			padding: 22px 16px;
			width: 100%;
			border: none;
			outline: none;
			text-align: left;
			cursor: pointer;
			transition: 0.3s;
			font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
			background-color: #ddd;
    }

    /* Create an active/current "tab button" class */
    .tab button.active {
			background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
    }

    .placeholder{
			background-color:white;
			height:18px;
    }

    .move:hover{
      cursor: move;
    }

    .lh-3{
      line-height: 3;
    }

    .ContentSortable{
      /* border:0.2px solid #dfe6e9; */
      min-height:60px;
      /* padding:10px; */
    }

    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
      width: 120px;
      height: 120px;
    }

    .togglebutton label .toggle{
      margin-left: 15px;
      margin-right: 15px;
    }

    .preview-group{
      display: inline-flex;
    }

		.preview-group .preview{
			text-align: center;
		}

		.preview-group .status{
			margin-right: 1rem;
		}

    .date-time span{
      display: inline-block;
      margin-bottom: 0;
      margin-right: 1rem;
    }

		.container {
			width: 100%;
		}

		@media (min-width: 1200px) {
			.container {
				max-width: 1170px;
			}
		}

		/* @media (min-width: 992px) {
			.container {
				max-width: 970px;
			}
		} */

    @media (max-width: 768px){
			.container {
				max-width: 750px;
			}

      .preview-group{
        display: block;
      }

			.preview-group .duration{
				text-align: right;
			}

			.preview-group .preview{
				display: inline-flex;
				margin-right: 1rem;
			}

			.preview-group .status{
				text-align: right;
				margin-right: 0;
			}

			.togglebutton label .toggle{
				margin-left: 10px;
				margin-right: 0;
			}

      .date-time span{
        display: block;
        margin-bottom: 0.5rem;
        margin-right: 0;
      }
    }
  </style>
  <style media="screen">
    .eyeCheckbox > input{
			display: none;
		}

		.eyeCheckbox input[type=checkbox]{
			opacity:0;
			position: absolute;
		}

		.eyeCheckbox i{
			cursor: pointer;
		}

		.btn-opsi-preview button {
      line-height: 1;
    }

		.panel-group .panel .panel-heading a {
			display: initial;
			padding: 10px 15px;
		}

		.list-group{
      border: 0;
			margin: 0;
    }

    .list-group li {
      list-style: none;
    }

    .list-group li .list-group-item{
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      border: 1px solid #eee;
      margin-bottom: -1px;
    }

    .list-group li .list-group-item.disabled,
    .list-group li .list-group-item.disabled .item-content {
      background: #eee;
      color: #999;
    }

    .item-content {
      width: 100%;
      display: flex;
      align-items: baseline;
    }

    .item-content i {
      margin-right: 1rem;
    }

    #importLibrary .nav-tabs-ver-container .nav-tabs-ver {
			padding: 0;
			margin: 0;
    }

		.btn-opsi-preview .dropdown-menu{
			right: 0;
	    left: auto;
	    z-index: 9999999;
	    cursor: auto;
	    top: 95px;
			padding: 0 !important;
		}

		.btn-opsi-preview .dropdown-menu .dropdown-item{
			color: black;
			cursor: pointer;
		}

		.btn-opsi-preview .dropdown-menu li{
	    padding: 3px 10px;
		}

		.btn-opsi-preview .dropdown-menu li:hover{
	    background: #b0c2c7;
		}

		[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
			position: absolute;
			left: unset;
			opacity: 1;
			cursor: pointer;
		}
	</style>
	<style>
		.btn-circle.btn-circle-raised.btn-circle-primary,
		.btn.btn-raised.btn-primary,
		.btn.btn-fab.btn-primary,
		.btn-group-raised .btn.btn-primary,
		.input-group-btn .btn.btn-raised.btn-primary,
		.input-group-btn .btn.btn-fab.btn-primary,
		.btn-group-raised .input-group-btn .btn.btn-primary {
			background: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
		}

		.btn-circle.btn-circle-raised.btn-circle-primary:before,
		.btn.btn-raised:not(.btn-link):hover.btn-primary,
		.btn.btn-raised:not(.btn-link):focus.btn-primary,
		.btn.btn-raised:not(.btn-link).active.btn-primary,
		.btn.btn-raised:not(.btn-link):active.btn-primary,
		.btn-group-raised .btn:not(.btn-link):hover.btn-primary,
		.btn-group-raised .btn:not(.btn-link):focus.btn-primary,
		.btn-group-raised .btn:not(.btn-link).active.btn-primary,
		.btn-group-raised .btn:not(.btn-link):active.btn-primary,
		.input-group-btn .btn.btn-raised:not(.btn-link):hover.btn-primary,
		.input-group-btn .btn.btn-raised:not(.btn-link):focus.btn-primary,
		.input-group-btn .btn.btn-raised:not(.btn-link).active.btn-primary,
		.input-group-btn .btn.btn-raised:not(.btn-link):active.btn-primary,
		.btn-group-raised .input-group-btn .btn:not(.btn-link):hover.btn-primary,
		.btn-group-raised .input-group-btn .btn:not(.btn-link):focus.btn-primary,
		.btn-group-raised .input-group-btn .btn:not(.btn-link).active.btn-primary,
		.btn-group-raised .input-group-btn .btn:not(.btn-link):active.btn-primary {
			background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' }};
		}

		a,
		.dropdown-menu li a:hover {
			color: <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : 'black' ?>;
			transition: all ease .1s;
		}
	</style>

	<style>
		.btn.btn-raised.btn-primary, 
		.btn.btn-fab.btn-primary, 
		.btn-group-raised .btn.btn-primary, 
		.input-group-btn .btn.btn-raised.btn-primary, 
		.input-group-btn .btn.btn-fab.btn-primary, 
		.btn-group-raised .input-group-btn .btn.btn-primary {
			display: inline-flex;
			align-items: center;
			background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
			border-radius: 5px;
		}

		.btn-circle.btn-circle-raised.btn-circle-primary:before, 
		.btn.btn-raised:not(.btn-link):hover.btn-primary, 
		.btn.btn-raised:not(.btn-link):focus.btn-primary, 
		.btn.btn-raised:not(.btn-link).active.btn-primary, 
		.btn.btn-raised:not(.btn-link):active.btn-primary, 
		.btn-group-raised .btn:not(.btn-link):hover.btn-primary, 
		.btn-group-raised .btn:not(.btn-link):focus.btn-primary, 
		.btn-group-raised .btn:not(.btn-link).active.btn-primary, 
		.btn-group-raised .btn:not(.btn-link):active.btn-primary, 
		.input-group-btn .btn.btn-raised:not(.btn-link):hover.btn-primary, 
		.input-group-btn .btn.btn-raised:not(.btn-link):focus.btn-primary, 
		.input-group-btn .btn.btn-raised:not(.btn-link).active.btn-primary, 
		.input-group-btn .btn.btn-raised:not(.btn-link):active.btn-primary, 
		.btn-group-raised .input-group-btn .btn:not(.btn-link):hover.btn-primary, 
		.btn-group-raised .input-group-btn .btn:not(.btn-link):focus.btn-primary, 
		.btn-group-raised .input-group-btn .btn:not(.btn-link).active.btn-primary, 
		.btn-group-raised .input-group-btn .btn:not(.btn-link):active.btn-primary {
			background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
		}

		.btn-circle {
			left: 0;
		}

		.btn:not(.btn-link):not(.btn-circle) span {
			top: 0;
			margin-left: 0;
			margin-right: 1rem;
		}

		.btn:not(.btn-link):not(.btn-circle) i {
			font-size: 16px;
			top: 0;
			margin: 0;
		}

		.edit-image-button {
			position: absolute;
			top: 1rem;
			right: 3rem;
			background-color: #f9f9f9;
			padding: .25rem 1rem;
			border-radius: 5px;
			transition: all 0.3s;
		}

		.edit-image-button:hover {
			background-color: #ddd;
		}

		.edit-image-button i {
			color: #333;
		}

		.d-inline {
			display: inline!important;
		}

		.d-none {
			display: none!important;
		}

		@media (max-width: 767px) {
			body{
				overflow-x: hidden;
			}

			.img-responsive {
				margin-bottom: 2rem;
			}

			section.content {
				margin-top: 85px;
			}

			.content .container-fluid {
				margin-left: -15px;
				margin-right: -15px;
			}

			.content .container-fluid .block-header {
				padding-left: 15px;
				padding-right: 15px;
			}
		}

		@media (min-width: 576px) {
			.d-sm-inline {
				display: inline!important;
			}

			.d-sm-none {
				display: none!important;
			}
		}
	</style>
@endpush

@section('content')
<section class="content">
	<div class="container-fluid">

		<center>
				@if(Session::has('success') || Session::has('message'))
				<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
						{!! Session::get('message') !!}
				</div>
				@elseif(Session::has('error'))
				<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
				</div>
				@endif
		</center>
		
		<div class="block-header">
			<h2>KELOLA KELAS</h2>
		</div>

		<div class="row clearfix">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div style="background:#393939; padding:25px;margin-top:0px;">
							<div class="container">
								<div class="row">
									<div class="col-md-5">
										@if($course->introduction != NULL)
											<div class="card">
												<div class="embed-responsive embed-responsive-16by9" style="position:relative;">
													<iframe class="embed-responsive-item" src="{{$course->introduction.'/preview'}}" allowfullscreen="true"></iframe>
													<div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">
														&nbsp;
													</div>
												</div>
											</div>
										@else
											<img class="img-responsive" src="{{$course->image}}" alt="{{$course->title}}">
											<a href="#" data-target="#modalSetImage" data-toggle="modal" class="edit-image-button"><i class="fa fa-pencil"></i></a>
										@endif
									</div>
									<div class="col-md-7">
										<h3 class="no-m" style="font-weight:bold;">
											<a class="color-white fs-30" href="#" id="CourseTitle" data-type="text">
												{{$course->title}} <i class="fa fa-pencil color-white"></i>
											</a>
										</h3>
										<h4 class="mt-1" style="font-weight:bold;">
											<a class="color-white fs-20" href="#" id="CourseSubTitle" data-type="text">
												{{$course->subtitle}} <i class="fa fa-pencil color-white"></i>
											</a>
										</h4>
										<p>
											<span>
												<a href="#">
													@for($i=1;$i<=$rate;$i++) <i class="fa fa-star color-warning"></i> @endfor
													@for($i=$rate;$i<5;$i++) <i class="fa fa-star color-white"></i> @endfor
													Beri penilaian
												</a>
											</span>
										</p>
										<p style="color:white">
											<strong>{{$num_progress}}</strong> dari <strong>{{$num_contents}}</strong> Modul selesai <a
												href="#">Reset Progres</a>
											<div class="progress">
												<div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0"
													aria-valuemax="100" style="width:{{$percentage}}%">
													{{ceil($percentage)}}%
												</div>
											</div>
										</p>
										<div style="display: flex; align-items: center; justify-content: space-between;">
											<a href="/course/learn/{{$course->slug}}" class="btn btn-primary btn-raised"><span>Buka Pratinjau</span><i class="material-icons">arrow_forward</i></a>
											<a href="#" class="btn-circle btn-circle-sm btn-circle-primary btn-circle-raised" title="Pengaturan Lanjutan"><i class="material-icons" style="left: 0;">settings</i></a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="card card-flat">
										<ul class="nav nav-tabs nav-tabs-full" role="tablist">
											<li class="nav-item">
												<a class="nav-link withoutripple" href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
													<i class="fa fa-eye"></i>
													<span class="d-none d-sm-inline">Ikhtisar</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link withoutripple" href="#update" aria-controls="update" role="tab" data-toggle="tab">
													<i class="fa fa-list"></i>
													<span class="d-none d-sm-inline">Update</span>
												</a>
											</li>
											<li class="nav-item active">
												<a class="nav-link withoutripple" href="#course-content" aria-controls="course-content" role="tab" data-toggle="tab">
													<i class="fa fa-graduation-cap"></i>
													<span class="d-none d-sm-inline">Konten</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link withoutripple" href="#webinar" aria-controls="webinar" role="tab" data-toggle="tab">
													<i class="fa fa-users"></i>
													<span class="d-none d-sm-inline">Webinar</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
													<i class="fa fa-question-circle-o"></i>
													<span class="d-none d-sm-inline">Diskusi</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
													<i class="fa fa-bullhorn"></i>
													<span class="d-none d-sm-inline">Pengumuman</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link withoutripple" href="{{url('admin/course/atendee/'.$course->id)}}">
													<i class="fa fa-circle-o-notch"></i>
													<span class="d-none d-sm-inline">Administrasi</span>
												</a>
											</li>
										</ul>
									</div>

									<div class="card-block no-pl no-pr">
										<div class="tab-content">

											<!-- TAB OVERVIEW -->
											<div role="tabpanel" class="tab-pane fade" id="overview">
												<div class="row">

													<!--RECENT ACTIVITY-->
													<div class="col-md-12">
														<h2 class="headline-sm headline-line">Aktifitas <span>Terbaru</span></h2>
													</div>
													<div class="col-md-6">
														<div class="card">
															<ul class="list-group">
																<li class="list-group-item">
																	<b>Pertanyaan Terbaru</b>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-md-6">
														<div class="card">
															<ul class="list-group">
																<li class="list-group-item">
																	<b>Pengumuman Pengajar Terbaru</b>
																</li>
															</ul>
														</div>
													</div>

													<!--ABOUT COURSE-->
													<div class="col-md-12">
														<h2 class="headline-sm headline-line">Tentang <span>Kelas Ini</span></h2>
													</div>
													<div class="col-md-12">
														<div class="card card-info">
															<div class="card-block">
																<div class="row">
																	<div class="col-md-2">
																		<b>Dengan Nomor</b>
																	</div>
																	<ul class="col-md-3">
																		<li class="dq-list-item">Modul: {{count($sections)}}</li>
																		<li class="dq-list-item">Kategori:
																			<a href="#" id="CourseCategory" data-type="select">
																				{{$course->category}} <i class="fa fa-pencil"></i>
																			</a>
																			</li>
																		<li class="dq-list-item">Skill level:
																			<a href="#" id="CourseLevel" data-type="select">
																				{{$course->level}} <i class="fa fa-pencil"></i>
																			</a>
																		</li>
																		Harga:
																		<a href="#" id="CoursePrice" data-type="text">
																			{{$course->price}} <i class="fa fa-pencil"></i>
																		</a>
																	</ul>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-2">
																		<b>Penjelasan</b>
																	</div>
																	<ul class="col-md-8">
																		<div id="recentDescription">
																			{!! $course->description !!}
																		</div>
																		<a href="#" id="getEditorDescription" data-target="#modalSetEditorDescription" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
																	</ul>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-2">
																		<b>Tujuan</b>
																	</div>
																	<ul class="col-md-8">
																			<div id="recentGoal">
																				{!! $course->goal !!}
																			</div>
																			<a href="#" id="getEditorGoal" data-target="#modalSetEditorGoal" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
																	</ul>
																</div>
																@if($course->model == 'private')
																	<hr>
																	<div class="row">
																		<div class="col-md-2">
																			<b>Password / Kode</b>
																		</div>
																		<ul class="col-md-8">
																				<div id="recentGoal">
																					{{ $course->password }}
																				</div>
																		</ul>
																	</div>
																@endif
																@if($course->model == 'live')
																	@php
																		$course_live = DB::table('course_lives')->where('course_id', $course->id)->first();
																	@endphp
																	<hr>
																	<div class="row">
																		<div class="col-md-2">
																			<b>Tanggal Mulai</b>
																		</div>
																		<ul class="col-md-8">
																				<div id="recentGoal">
																					{{$course_live->start_date}}
																				</div>
																		</ul>
																	</div>
																	<hr>
																	<div class="row">
																		<div class="col-md-2">
																			<b>Tanggal Akhir</b>
																		</div>
																		<ul class="col-md-8">
																				<div id="recentGoal">
																					{{$course_live->end_date}}
																				</div>
																		</ul>
																	</div>
																	<hr>
																	<div class="row">
																		<div class="col-md-2">
																			<b>Total Live</b>
																		</div>
																		<ul class="col-md-8">
																				<div id="recentGoal">
																					{{$course_live->meeting_total}}
																				</div>
																		</ul>
																	</div>
																	<hr>
																	<div class="row">
																		<div class="col-md-2">
																			<b>Total Peserta</b>
																		</div>
																		<ul class="col-md-8">
																				<div id="recentGoal">
																					{{$course_live->participant_total}}
																				</div>
																		</ul>
																	</div>
																@endif
																<hr>
																<div class="row">
																	<div class="col-md-2">
																		<b>Pengajar</b>
																	</div>
																	<div class="col-md-10">
																		<div class="row">
																			<div class="col-md-2">
																				<img src="{{avatar($course->photo)}}" class="img-circle">
																			</div>
																			<div class="col-md-6">
																				<h3 class="color-dark">{{$course->author}}</h3>
																			</div>
																		</div>
																	</div>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-2">
																		<b>Mendapatkan Sertifikat?</b>
																	</div>
																	<div class="col-md-10">
																		<div class="row">
																			<div class="col-md-12">
																				{{ $course->get_ceritificate == '0' ? 'Tidak Ada Sertifikat' : ($course->get_ceritificate == '1' ? 'Persentase' : ($course->get_ceritificate == '2' ? 'Grade Book' : 'Manual')) }}
																				<p><a href="#" data-target="#modalEditGetCeritificate" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a></p>
																			</div>
																		</div>
																	</div>
																</div>
																<hr>
																<div class="row">
																	<div class="col-md-2">
																		<b>Publikasikan Kelas?</b>
																	</div>
																	<div class="col-md-10">
																		<div class="row">
																			<div class="col-md-12">
																				@php $status_checked = $course->status == '1' ? 'checked' : ''; @endphp
																				<div class="form-group" style="margin: 0;">
																					<div class="togglebutton">
																						<div class="switch">
																							<label>
																								<span class="fs-14 d-none d-sm-inline">Unpublish</span>
																								<input type="checkbox" {{$status_checked}} name="status{{$course->id}}">
																								<span class="lever switch-col-blue"></span>
																								<span class="fs-14 d-none d-sm-inline">Publish</span>
																							</label>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- TAB Update -->
											<div role="tabpanel" class="tab-pane fade" id="update">
												<div class="row">
					
													<!--RECENT ACTIVITY-->
													<div class="col-md-12">
														<h2 class="headline-sm headline-line">@lang('front.page_manage_courses.update_newest_title')</h2>
														<div class="form-group no-m">
															<form action="{{url('course/update/submit')}}" method="post">
																{{csrf_field()}}
																<input type="hidden" name="course_id" value="{{$course->id}}">
																<textarea name="body" id="answer-update" placeholder="Berikan Komentar Anda untuk membantu penanya menemukan jawaban terbaik." class="form-control" rows="8" cols="80"></textarea>
																<div class="upload-page" style="background: #f8f8f8;padding: 0px 10px;">
																	<!-- The file upload form used as target for the file upload widget -->
																	<div id="fileupload-discussion">
																		<!-- Redirect browsers with JavaScript disabled to the origin page -->
																		<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
																		<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
																		<div class="row fileupload-buttonbar">
																			<div class="col-lg-7">
																				<!-- The fileinput-button span is used to style the file input field as button -->
																				<span class="btn btn-success fileinput-button btn-raised">
																					<i class="glyphicon glyphicon-file"></i>
																					<span>@lang('front.page_manage_courses.update_add_files')</span>
																					<input type="file" name="files[]" multiple id="add-file-jquery-upload" />
																				</span>
																				<!-- The global file processing state -->
																				<span class="fileupload-process"></span>
																			</div>
																			<!-- The global progress state -->
																			<div class="col-lg-5 fileupload-progress fade">
																				<!-- The global progress bar -->
																				<div
																					class="progress progress-striped active"
																					role="progressbar"
																					aria-valuemin="0"
																					aria-valuemax="100"
																				>
																					<div
																						class="progress-bar progress-bar-success"
																						style="width: 0%;"
																					></div>
																				</div>
																				<!-- The extended global progress state -->
																				<div class="progress-extended">&nbsp;</div>
																			</div>
																		</div>
																		<!-- The table listing the files available for upload/download -->
																		<table role="presentation" class="table table-striped table-files-upload" style="margin:0">
																			<tbody class="files"></tbody>
																		</table>
																	</div>
																</div>
					
																<div class="form-group no-m">
																	<input type="submit" class="btn btn-primary btn-raised" name="submit" value="Kirim Update">
																</div>
															</form>
														</div>
														@foreach($update->where('level_1', null) as $answer)
															<div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
																@if($answer->user_id == Auth::user()->id)
																<div class="groups-dropdown">
																	<div class="groups-dropdown-group">
																		<i class="fa fa-ellipsis-v"></i>
																		<ul class="groups-dropdown-menu">
																			<!-- <li>
																				<a class="groups-dropdown-item" href="#">
																					<i class="fa fa-pencil"></i> Ubah
																				</a>
																			</li> -->
																			<li>
																				<a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$answer->id}}').submit()">
																					<i class="fa fa-trash"></i> @lang('front.page_manage_courses.update_answer_delete')
																				</a>
																				<form action="{{url('/course/content/discussion/delete/'.$answer->id)}}" id="form-koment-delete-{{$answer->id}}" method="post" style="display:none;">
																					{{csrf_field()}}
																				</form>
																			</li>
																		</ul>
																	</div>
																</div>
																@endif
																<div class="card-block">
																	<a class="groups-title" href="#">{{$answer->user->name}}</a>
																	<p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($answer->created_at))}}&nbsp;&middot;&nbsp;</p>
																	<p class="groups-description">{!!$answer->description!!}</p>
																	@if($answer->file)
																	<?php $files = explode(',',$answer->file); ?>
																	<p class="fs-14" style="color: #999;">@lang('front.page_manage_courses.update_answer_attachment')</p>
																		@foreach($files as $file)
																		<?php
																				$ext = explode('.',$file);
																				$ext = $ext[count($ext)-1];
																				$img = asset('files/learn/course_update/'.$file);
																				if($ext == 'pdf'){
																					$img = asset('images/pdf.png');
																				}elseif ($ext == 'docx') {
																					$img = asset('images/word.png');
																				}
																		 ?>
																		<div class="img-attachment">
																			<img src="{{$img}}" style="width:50px;margin-right:20px;">
																			<a class="groups-title" href="{{asset('files/learn/update/'.$file)}}" download="{{substr($file,strlen(Auth::user()->id)+12,(strlen($file)-(strlen(Auth::user()->id)+12)))}}">{{substr($file,strlen(Auth::user()->id)+12,(strlen($file)-(strlen(Auth::user()->id)+12)))}}</a>
																		</div>
																		@endforeach
																	@endif
																	<div class="reply-comment">
																		<hr style="border-color:#bbb">
																		<p class="reply-btn" style="cursor:pointer">@lang('front.page_manage_courses.update_answer_reply')</p>
																		<div class="text-reply">
																			<form method="POST" action="{{url('course/update/submit')}}" accept-charset="UTF-8" enctype="multipart/form-data">
																				{{csrf_field()}}
																				<input type="hidden" name="course_id" value="{{$course->id}}">
																				<input name="parents" type="hidden" value="{{$answer->id}}">
																				<textarea name="body" id="discussion-reply-{{$answer->id}}" class="form-control discussion-reply" required="required"></textarea>
																				<input class="btn btn-primary btn-raised" name="button" type="submit" value="@lang('front.page_manage_courses.update_submit_comment')">
																			</form>
																		</div>
																		<div class="reply_1-page">
																			@if($answer->reply_1)
																				@foreach($answer->reply_1 as $reply_1)
																				<div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
																					@if($reply_1->user_id == Auth::user()->id)
																					<div class="groups-dropdown">
																						<div class="groups-dropdown-group">
																							<i class="fa fa-ellipsis-v"></i>
																							<ul class="groups-dropdown-menu">
																								<!-- <li>
																									<a class="groups-dropdown-item" href="#">
																										<i class="fa fa-pencil"></i> Ubah
																									</a>
																								</li> -->
																								<li>
																									<a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$reply_1->id}}').submit()">
																										<i class="fa fa-trash"></i> @lang('front.page_manage_courses.update_answer_delete')
																									</a>
																									<form action="{{url('/course/content/discussion/delete/'.$reply_1->id)}}" id="form-koment-delete-{{$reply_1->id}}" method="post" style="display:none;">
																										{{csrf_field()}}
																									</form>
																								</li>
																							</ul>
																						</div>
																					</div>
																					@endif
																					<div class="card-block">
																						<a class="groups-title" href="#">{{$reply_1->user->name}}</a>
																						<p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($reply_1->created_at))}}&nbsp;&middot;&nbsp;</p>
																						<p class="groups-description">{!!$reply_1->description!!}</p>
																					</div>
																				</div>
																				@endforeach
																			@endif
																		</div>
																	</div>
																</div>
															</div>
														@endforeach
													</div>
												</div>
											</div>

											<!-- TAB COURSE CONTENT -->
											<div role="tabpanel" class="tab-pane fade in active" id="course-content">
												<input type="hidden" id="content-id-value" value="" />
												<div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
													<?php $i=1; ?>

													<div class="panel-group SectionSortable" id="accordion fade in active">
														@foreach($sections as $section)
															<div class="panel panel-default" id="section{{$section['id']}}" data-id="{{$section['id']}}">
																{{-- <div class="panel-heading" style="background: #e9e9e9; display: flex; align-items: flex-start; justify-content: space-between;"> --}}
																<div class="panel-heading" style="background: #e9e9e9; display: flex; align-items: center; justify-content: space-between; padding: 15px;">
																	<a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}" style="width: 100%; padding: 0;">
																		<div style="display: flex; align-items: baseline;">
																			<i class="move fa fa-arrows-alt mr-1" style="color: #333;"></i>
																			<div>
																				<h4 class="panel-title fw-500">{{ $section['title'] }}</h4>
																				<p style="color: #aaa; margin-bottom: 0;">{{ $section['description'] }}</p>
																			</div>
																		</div>
																	</a>
																	<div style="width: 65px; display: flex; align-items: center; justify-content: space-between;">
																		{{-- <i class="fa fa-circle fs-20 {!! $section['status'] == '1' ? 'color-success' : 'color-dark' !!}"></i> --}}
																		<i class="fa fa-circle fs-20 color-success"></i>
																		<div class="btn-group m-0" style="box-shadow: none; margin: 0;">
																			<button type="button" class="btn btn-raised btn-white no-shadow dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: flex; align-items: center; justify-content: center; border: 1px solid #4ca3d9; border-radius: 5px;">
																				<i class="material-icons" style="font-size: 14px;">settings</i>
																			</button>
																			<ul class="dropdown-menu">
																				{{-- <li><a class="dropdown-item" href="#/" data-id="{{ $section['id'] }}" data-title="{{ $section['title'] }}" data-description="{{ $section['description'] }}" data-status="{{ $section['status'] }}"><i class="fa fa-pencil"></i> Edit</a></li> --}}
																				<li><a class="dropdown-item" href="/course/section/delete/{{ $section['id_course'] }}/{{ $section['id'] }}" onclick="return confirm('Topik ini memiliki beberapa konten, Anda yakin ingin menghapusnya?')"><i class="fa fa-trash"></i> Hapus</a></li>
																			</ul>
																		</div>
																	</div>
																</div>

																<div id="content{{$i}}" class="panel-collapse collapse show">
																	<div class="panel-body" style="background:white;">
																		<div class="mb-2" style="display: flex; align-items: center; justify-content: space-between;">
																			<a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" class="btn btn-primary btn-raised m-0 d-none d-sm-inline">Tambahkan Materi</a> <!-- For desktop view -->
																			<a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" class="btn-circle btn-circle-sm btn-circle-primary btn-circle-raised m-0 d-inline d-sm-none"><i class="material-icons">add</i></a> <!-- For mobile view -->
																			{{-- <div class="togglebutton">
																				<label>
																					<span class="d-none d-sm-inline">Non-aktif </span><input class="sectionActivated" id-section="{{ $section['id'] }}" type="checkbox" {{ $section['status'] == '1' ? 'checked' : '' }}><span class="d-none d-sm-inline"> Aktif</span>
																				</label>
																			</div> --}}
																			<div class="switch">
																				<label><span class="d-none d-sm-inline">Nonaktif</span><input type="checkbox" class="sectionActivated" id-section="{{ $section['id'] }}" type="checkbox" {{ $section['status'] == '1' ? 'checked' : '' }}><span class="lever switch-col-blue"></span><span class="d-none d-sm-inline">Aktif</span></label>
																			</div>
																		</div>

																		<div class="list-group">
																			<div class="ContentSortable" id="{{ $section['id'] }}">
																				@foreach($section['section_contents'] as $content)
																					<li id="{{$content->id}}" data-section="{{$content->id_section}}">
																						@php
																							$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
																						@endphp

																						<div class="list-group-item list-group-item-action withripple {{$content->status == '0' ? 'disabled' : ''}}">
																							@if($content->type_content == 'label')
																								<div class="item-content">
																									<i class="move fa fa-arrows-alt mr-1"></i>
																									<div>
																										{{ $content->title ? $content->title : strip_tags($content->description) }}
																									</div>
																								</div>
																							@else
																								<a class="item-content" href="/course/learn/content/{{$course->slug}}/{{$content->id}}?preview=true">
																									<i class="move fa fa-arrows-alt mr-1"></i>
																									<div>
																										<i class="{{content_type_icon($content->type_content)}}"></i> {{ $content->title ? $content->title : strip_tags($content->description) }}
																									</div>
																								</a>
																							@endif

																							<div style="display: flex; align-items: center;">
																								@if($content->type_content == 'video')
																									<div class="duration fs-12 fw-600 color-dark d-none d-sm-inline">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</div>
																								@endif
																								@if($course->public == '1')
																									@if($content->preview == '1')
																										<i title="Preview" class="fa fa-eye fs-14 ml-1"></i>
																									@endif
																								@endif
																								<i title="{{$content->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$content->status == '0' ? '' : 'color-success'}}"></i>
																								<div class="btn-opsi-preview ml-1">
																									<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																										<i class="fa fa-ellipsis-v"></i>
																									</button>
																									<ul class="dropdown-menu" role="menu" aria-labelledby="">
																										@if($content->type_content == 'discussion')
																											<li><a class="dropdown-item" href="{{url('course/content/discussion/manage/'.$content->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
																										@elseif($content->type_content == 'folder')
																											<li><a class="dropdown-item" href="{{url('course/content/folder/manage/'.$content->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
																										@endif
																										<li><a class="dropdown-item status" href="#"><i class="fa fa-circle {{$content->status == '0' ? '' : 'color-success'}}"></i> Atur sbg konten {{$content->status == '0' ? 'publish' : 'draft'}}</a></li>
																										<li class="eyeCheckbox">
																											<a class="dropdown-item" id="previewEye{{$content->id}}" href="/admin/course/preview/content/update/{{$content->id}}"><i class="fa fa-eye"></i> Atur sbg {{ $content->preview == '1' ? 'non-preview' : 'preview' }}</a>
																											<input type="checkbox" id="preview{{$content->id}}" class="preview_content" value="{{$content->id}}" {{$content->preview == '1' ? 'checked' : ''}}>
																										</li>
																										<li>
																											<a class="dropdown-item" href="{{url('course/content/add/library/'.$content->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> Tambahkan Sebagai Library</a>
																										</li>
																										<li><a class="dropdown-item" href="{{url('course/content/update/'.$content->id. '?content=' . $content->type_content)}}"><i class="fa fa-pencil"></i> Edit</a></li>
																										<li><a class="dropdown-item" href="#" onclick="if(confirm('sure?')){ return location.href = '{{url('course/content/delete/'.$course->id.'/'.$content->id)}}' }"><i class="fa fa-trash-o"></i> Hapus</a></li>
																										@if($content->type_content == 'video')
																											<li><a class="dropdown-item" href="{{url('course/content/video/manage/'.$content->id)}}"><i class="fa fa-cog"></i> Manage Video</a></li>
																										@endif
																									</ul>
																								</div>
																							</div>
																						</div>
																					</li>
																				@endforeach
																			</div>
																		</div>

																		<div class="list-group">
																			@foreach($section['section_quizzes'] as $quiz)
																				<li>
																					<div class="list-group-item list-group-item-action withripple {{$quiz->status == '0' ? 'disabled' : ''}}">
																						<a class="item-content" href="#">
																							<i class="fa fa-star"></i> {{$quiz->name}}
																						</a>

																						<div style="display: flex; align-items: center;">
																							<i title="{{$quiz->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$quiz->status == '0' ? '' : 'color-success'}}"></i>

																							<div class="btn-opsi-preview ml-1">
																								<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																									<i class="fa fa-ellipsis-v"></i>
																								</button>
																								<ul class="dropdown-menu" role="menu" aria-labelledby="">
																									<li><a class="dropdown-item" href="{{url('course/quiz/question/manage/'.$quiz->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
																									<li><a class="dropdown-item" href="{{$quiz->status == '1' ? url('course/quiz/unpublish/'.$quiz->id.'/'.$course->id) : url('course/quiz/publish/'.$quiz->id.'/'.$course->id)}}"><i class="fa fa-circle {{$quiz->status == '1' ? 'color-success' : ''}}"></i> Atur sbg {{$quiz->status == '1' ? 'draft' : 'publish'}}</a></li>
																									<li><a class="dropdown-item" id="previewEye{{$quiz->id}}" href="{{url('course/quiz/add/library/'.$quiz->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> Tambahkan Sebagai Library</a></li>
																									<li><a class="dropdown-item" href="{{url('course/quiz/update/'.$quiz->id)}}"><i class="fa fa-pencil"></i> Edit</a></li>
																									<li><a class="dropdown-item" href="#" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('course/quiz/delete/'.$course->id.'/'.$quiz->id)}}'"><i class="fa fa-trash-o"></i> Hapus</a></li>
																								</ul>
																							</div>
																						</div>
																					</div>
																				</li>
																			@endforeach
																		</div>

																		<div class="list-group">
																			@foreach($section['section_assignments'] as $assignment)
																				<li>
																					<div class="list-group-item list-group-item-action withripple {{$assignment->status == '0' ? 'disabled' : ''}}">
																						<a class="item-content" href="#">
																							<i class="fa fa-tasks"></i> {{$assignment->title}}
																						</a>

																						<div style="display: flex; align-items: center;">
																							<i title="{{$assignment->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$assignment->status == '0' ? '' : 'color-success'}}"></i>

																							<div class="btn-opsi-preview ml-1">
																								<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																									<i class="fa fa-ellipsis-v"></i>
																								</button>
																								<ul class="dropdown-menu" role="menu" aria-labelledby="">
																									<li><a class="dropdown-item" href="{{url('course/assignment/answer/'.$assignment->id)}}"><i class="fa fa-paste"></i> Lihat Jawaban</a></li>
																									<li><a class="dropdown-item" href="{{$assignment->status == '1' ? url('course/assignment/unpublish/'.$assignment->id) : url('course/assignment/publish/'.$assignment->id)}}"><i class="fa fa-circle {{$assignment->status == '1' ? 'color-success' : ''}}"></i> Atur sbg {{$assignment->status == '1' ? 'draft' : 'publish'}}</a></li>
																									<li><a class="dropdown-item" id="previewEye{{$assignment->id}}" href="{{url('course/assignment/add/library/'.$assignment->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> Tambahkan Sebagai Library</a></li>
																									<li><a class="dropdown-item" href="{{url('course/assignment/update/'.$assignment->id)}}"><i class="fa fa-pencil"></i> Edit</a></li>
																									<li><a class="dropdown-item" href="#" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('course/assignment/delete/'.$course->id.'/'.$assignment->id)}}'"><i class="fa fa-trash-o"></i> Hapus</a></li>
																								</ul>
																							</div>
																						</div>
																					</div>
																				</li>
																			@endforeach
																		</div>
																	</div>
																</div>
															</div>
															<?php $i++; ?>
														@endforeach
													</div>

													{{-- <div class="panel-group SectionSortable" id="accordion fade in active">
														@foreach($sections as $section)
															<div class="panel panel-default" id="section{{$section['id']}}" data-id="{{$section['id']}}">
																<a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
																	<div class="panel-heading" style="background:#e9e9e9">
																		<h4 class="panel-title">
																			<i class="move fa fa-arrows-alt"></i>
																			<strong style="color:#2e2e2e">{{ $section['title'] }}</strong>
																			<div class="pull-right">
																				<a class="color-primary" href="#/" id="EditSection" data-id="{{ $section['id'] }}" data-title="{{ $section['title'] }}" data-description="{{ $section['description'] }}">Edit <i class="fa fa-pencil"></i></a>
																				<a class="color-danger" href="/course/section/delete/{{ $section['id_course'] }}/{{ $section['id'] }}" onclick="return confirm('Topik ini memiliki beberapa konten, Anda yakin ingin menghapusnya?')">Hapus <i class="fa fa-trash"></i></a>
																			</div>
																		</h4>
																		<br>
																		<p>{{ $section['description'] }}</p>
																	</div>
																</a>
																<div id="content{{$i}}" class="panel-collapse collapse show">
																	<div class="panel-body" style="background:white;">
																		<a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" da class="btn btn-primary btn-raised">Tambah Konten / Kuis</a>

																		<div class="list-group">
																			<div class="ContentSortable" id="{{ $section['id'] }}">
																				@foreach($section['section_contents'] as $content)
																					<li style="list-style:none" id="{{$content->id}}" data-section="{{$content->id_section}}">
																						@php
																							$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
																						@endphp
																						<a href="#" class="list-group-item list-group-item-action withripple {{$content->status == '0' ? 'disabled' : ''}}" style="align-items: center;justify-content: space-between;">
																							<div style="display: flex; align-items: center;">
																								<i class="move fa fa-arrows-alt mr-1"></i>
																								<div onclick="location.href = '/course/learn/content/{{$course->slug}}/{{$content->id}}?preview=true'">
																									<i class="{{content_type_icon($content->type_content)}}"></i> {{ $content->title ? substr($content->title,0,50) : strip_tags(substr($content->description,0,100)).'...' }}
																								</div>
																							</div>

																							<div style="display: flex; align-items: center;">
																								@if($content->type_content == 'video')
																									<div class="duration fs-12 fw-600 color-dark d-none d-sm-inline">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</div>
																								@endif
																								@if($course->public == '1')
																									@if($content->preview == '1')
																										<i title="Preview" class="fa fa-eye fs-14 ml-1"></i>
																									@endif
																								@endif
																								<i title="{{$content->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$content->status == '0' ? '' : 'color-success'}}"></i>
																								<div class="btn-opsi-preview ml-1">
																									<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																										<i class="fa fa-ellipsis-v"></i>
																									</button>
																									<ul class="dropdown-menu" role="menu" aria-labelledby="">
																										<li><span class="dropdown-item" onclick="location.href = '{{$content->status == '1' ? url('course/content/unpublish/'.$content->id.'/'.$course->id) : url('course/content/publish/'.$content->id.'/'.$course->id)}}'"><i class="fa fa-circle {{$content->status == '1' ? 'color-success' : ''}}"></i> Atur sbg {{$content->status == '1' ? 'draft' : 'publish'}}</span></li>
																										<li class="eyeCheckbox">
																											<span class="dropdown-item" id="previewEye{{$content->id}}" onclick="location.href = '#'"><i class="fa fa-eye"></i> Atur sbg {{ $content->preview == '1' ? 'non-preview' : 'preview' }}</span>
																											<input type="checkbox" id="preview{{$content->id}}" class="preview_content" value="{{$content->id}}" {{$content->preview == '1' ? 'checked' : ''}}>
																										</li>
																										<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/content/update/'.$content->id. '?content=' . $content->type_content)}}'"><i class="fa fa-pencil"></i> Edit</span></li>
																										<li><span class="dropdown-item" onclick="if(confirm('sure?')){ return location.href = '{{url('admin/course/content/delete/'.$course->id.'/'.$content->id)}}' }"><i class="fa fa-trash-o"></i> Hapus</span></li>
																										@if($content->type_content == 'video')
																											<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/content/video/manage/'.$content->id)}}'"><i class="fa fa-cog"></i> Manage Video</span></li>
																										@endif
																									</ul>
																								</div>
																							</div>
																						</a>
																				</li>
																				@endforeach
																			</div>
																		</div>

																		<div class="list-group">
																			@foreach($section['section_quizzes'] as $quiz)
																				<li>
																					<a href="#" class="list-group-item list-group-item-action withripple {{$quiz->status == '0' ? 'disabled' : ''}}">
																						<div>
																							<i class="fa fa-star"></i> {{$quiz->name}}
																						</div>

																						<div style="display: flex; align-items: center;">
																							<i title="{{$quiz->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$quiz->status == '0' ? '' : 'color-success'}}"></i>

																							<div class="btn-opsi-preview ml-1">
																								<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																									<i class="fa fa-ellipsis-v"></i>
																								</button>
																								<ul class="dropdown-menu" role="menu" aria-labelledby="">
																									<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/quiz/question/manage/'.$quiz->id)}}'"><i class="fa fa-cog"></i> Manage</span></li>
																									<li><span class="dropdown-item" onclick="location.href = '{{$quiz->status == '1' ? url('course/quiz/unpublish/'.$quiz->id.'/'.$course->id) : url('course/quiz/publish/'.$quiz->id.'/'.$course->id)}}'"><i class="fa fa-circle {{$quiz->status == '1' ? 'color-success' : ''}}"></i> Atur sbg {{$quiz->status == '1' ? 'draft' : 'publish'}}</span></li>
																									<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/quiz/update/'.$quiz->id)}}'"><i class="fa fa-pencil"></i> Edit</span></li>
																									<li><span class="dropdown-item" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('admin/course/quiz/delete/'.$course->id.'/'.$quiz->id)}}'"><i class="fa fa-trash-o"></i> Hapus</span></li>
																								</ul>
																							</div>
																						</div>
																					</a>
																				</li>
																			@endforeach
																		</div>

																		<div class="list-group">
																			@foreach($section['section_assignments'] as $assignment)
																				<li>
																					<a href="#" class="list-group-item list-group-item-action withripple {{$assignment->status == '0' ? 'disabled' : ''}}">
																						<div>
																							<i class="fa fa-tasks"></i> {{$assignment->title}}
																						</div>

																						<div style="display: flex; align-items: center;">
																							<i title="{{$assignment->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$assignment->status == '0' ? '' : 'color-success'}}"></i>

																							<div class="btn-opsi-preview ml-1">
																								<button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
																									<i class="fa fa-ellipsis-v"></i>
																								</button>
																								<ul class="dropdown-menu" role="menu" aria-labelledby="">
																									<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/assignment/answer/'.$assignment->id)}}'"><i class="fa fa-paste"></i> Lihat Jawaban</span></li>
																									<li><span class="dropdown-item" onclick="location.href = '{{$assignment->status == '1' ? url('course/assignment/unpublish/'.$assignment->id) : url('course/assignment/publish/'.$assignment->id)}}'"><i class="fa fa-circle {{$assignment->status == '1' ? 'color-success' : ''}}"></i> Atur sbg {{$assignment->status == '1' ? 'draft' : 'publish'}}</span></li>
																									<li><span class="dropdown-item" onclick="location.href = '{{url('admin/course/assignment/update/'.$assignment->id)}}'"><i class="fa fa-pencil"></i> Edit</span></li>
																									<li><span class="dropdown-item" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('admin/course/assignment/delete/'.$course->id.'/'.$assignment->id)}}'"><i class="fa fa-trash-o"></i> Hapus</span></li>
																								</ul>
																							</div>
																						</div>
																					</a>
																				</li>
																			@endforeach
																		</div>
																	</div>
																</div>
															</div>
														@endforeach
													</div> --}}

													<div id="place_new_section"></div>
													<input type="hidden" value="0" id="section_row">
													<button type="button" class="btn btn-raised btn-block btn-lg" id="add_new_section">Tambah Topik</button>
												</div>
											</div>

											<!-- TAB WEBINAR -->
											<div role="tabpanel" class="tab-pane fade" id="webinar">
												<a href="#" data-toggle="modal" data-target="#modalWebinarAddSchedule" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.schedule_title') {{is_liveCourse($course->id) == true ? Lang::get('front.page_manage_courses.horizontal_tab_live') : Lang::get('front.page_manage_courses.horizontal_tab_webinar')}}</a>
					
												<div class="modal fade" id="modalWebinarAddSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title" id="">@lang('front.page_manage_courses.schedule_title') {{is_liveCourse($course->id) == true ? Lang::get('front.page_manage_courses.horizontal_tab_live') : Lang::get('front.page_manage_courses.horizontal_tab_webinar')}}</h4>
															</div>
															<form class="" action="/meeting/schedule/store" method="post">
																{{csrf_field()}}
																<div class="modal-body">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_title')</label>
																				<input type="text" name="name" class="form-control" id="" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_description')</label>
																				<textarea name="description" class="form-control" rows="8" cols="80"></textarea>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_date')</label>
																				<input type="date" name="date" class="form-control" id="" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_time')</label>
																				<input type="time" name="time" class="form-control" id="" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_recording')</label>
																				<select class="form-control selectpicker" name="recording">
																					<option value="1">@lang('front.page_manage_courses.schedule_new_recording_yes')</option>
																					<option value="0">@lang('front.page_manage_courses.schedule_new_recording_no')</option>
																				</select>
																			</div>
																			<input type="hidden" name="course_id" value="{{$course->id}}">
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.page_manage_courses.button_close')</button>
																	<button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_save')</button>
																</div>
															</form>
														</div>
													</div>
												</div>
					
												<div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
													@foreach($meeting_schedules as $index => $meeting_schedule)
														<div class="mb-0 card card-default">
															<div class="card-header" role="tab" id="headingWebinar{{$index}}">
																<h4 class="card-title">
																	<a class="collapsed dq-none-rotation" role="button" data-toggle="collapse" data-parent="#accordion" href="#webinar{{$index}}" aria-expanded="false" aria-controls="collapse{{$index}}">
																		<span class="dq-section-auto pull-right">
																			<p class="date-time color-info">
																				<span>
																					<i class="fa fa-calendar"></i> {{$meeting_schedule->date}}
																				</span>
																				<span>
																					<i class="fa fa-clock-o"></i> {{$meeting_schedule->time}}
																				</span>
																			</p>
																		</span>
																		<h2 class="headline-sm">{{$meeting_schedule->name}}</h2>
																	</a>
																</h4>
															</div>
															<div id="webinar{{$index}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingWebinar{{$index}}">
																<div class="card-block">
																	<p class="pull-right">
																		<b class="color-dark">@lang('front.page_manage_courses.schedule_new_recording'):</b>
																		&nbsp;
																		{{$meeting_schedule->recording == '1' ? Lang::get('front.page_manage_courses.text_yes') : Lang::get('front.page_manage_courses.text_no')}}
																	<h3 class="headline-sm fs-20 mt-0"><span>@lang('front.page_manage_courses.schedule_new_description')</span></h3>
																	<p>{{$meeting_schedule->description}}</p>
																	<hr>
																	@if($meeting_schedule->date == date("Y-m-d"))
																		<a title="Start Meeting" href="{{url('bigbluebutton/meeting/instructor/join/'.$course->id. '/' . $meeting_schedule->id )}}" class="btn btn-raised btn-success">@lang('front.page_manage_courses.schedule_start')</a>
																	@endif
																	<a id="webinarEditSchedule" data-id="{{$meeting_schedule->id}}" data-name="{{$meeting_schedule->name}}" data-description="{{$meeting_schedule->description}}" data-date="{{$meeting_schedule->date}}" data-time="{{$meeting_schedule->time}}" data-recording="{{$meeting_schedule->recording}}" href="#/" class="btn btn-raised btn-warning">@lang('front.page_manage_courses.text_edit')</a>
																	<a onclick="return confirm('Yakin?')" class="btn btn-raised btn-danger" href="/meeting/schedule/delete/{{$meeting_schedule->id}}">@lang('front.page_manage_courses.text_delete')</a>
																</div>
															</div>
														</div>
													@endforeach
												</div>
					
												<div class="modal fade" id="modalWebinarEditSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title" id="">@lang('front.page_manage_courses.schedule_webinar_title_edit')</h4>
															</div>
															<form class="" action="/meeting/schedule/update" method="post">
																{{csrf_field()}}
																<div class="modal-body">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_title')</label>
																				<input type="text" name="name" class="form-control" id="webinarName" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_description')</label>
																				<textarea name="description" class="form-control" id="webinarDescription" rows="8" cols="80"></textarea>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_date')</label>
																				<input type="date" name="date" id="webinarDate" class="form-control" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_time')</label>
																				<input type="time" name="time" class="form-control" id="webinarTime" placeholder="">
																			</div>
																			<div class="form-group">
																				<label for="">@lang('front.page_manage_courses.schedule_new_recording')</label>
																				<select class="form-control selectpicker" id="webinarRecording" name="recording">
																					<option value="1">@lang('front.page_manage_courses.schedule_new_recording_yes')</option>
																					<option value="0">@lang('front.page_manage_courses.schedule_new_recording_no')</option>
																				</select>
																			</div>
																			<input type="hidden" name="course_id" value="{{$course->id}}">
																			<input type="hidden" name="id" id="webinarId" value="{{$course->id}}">
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.page_manage_courses.button_close')</button>
																	<button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_save')</button>
																</div>
															</form>
														</div>
													</div>
												</div>
					
											</div>

											<!-- TAB Q&A -->
											<div role="tabpanel" class="tab-pane fade" id="QandA">
												<div class="card">
													<ul class="list-group">
														@foreach($discussions as $discussion)
															<a href="/discussion/detail/{{$discussion->id}}">
																<li class="list-group-item card-block">
																	<span class="text-center dq-pl">
																		<img src="{{avatar($discussion->photo)}}" class="dq-frame-round-xs" alt="{{$discussion->name}}">
																	</span>
																	<span class="col-lg-10 dq-max">
																		<b>{{$discussion->name}}</b>
																		<p>{{$discussion->body}}</p>
																	</span>
																	<span class="text-center dq-pl dq-pr">
																		<b>{{(DB::table('discussions_answers')->where('discussion_id', $discussion->id))->count()}}</b>
																		<p>responses</p>
																	</span>
																</li>
															</a>
														@endforeach
													</ul>
												</div>
												<div class="text-center">
													{{-- <a href="" class="btn btn-raised btn-info">
														Load More
													</a> --}}
												</div>
											</div>

											<!-- TAB ANNOUNCEMENTS -->
											<div role="tabpanel" class="tab-pane fade" id="announcements">
												<a title="Pengumuman" href="{{url('course/announcement/create/'.$course->id)}}" class="btn btn-primary btn-raised">
													Buat Pengumuman
												</a>
												@foreach($announcements as $announcement)
													<div class="card">
														<div class="card-block-big">
															<p>
																<div class="row">
																	<img src="{{avatar($announcement['photo'])}}" class="dq-image-user" style="margin-left:15px">
																	<div class="col-md-8">
																		<div>
																			{{$announcement['name']}}
																		</div>
																		<div>
																			<span>{{$announcement['created_at']}}</span>
																		</div>
																	</div>
																</div>
															</p>
															<h3>{{$announcement['title']}}</h3>
															<p>
																{!!$announcement['description']!!}
															</p>
															<form name="delete" action="/course/announcement/delete/{{$announcement['id']}}" method="post">
																{{csrf_field()}}
																<input type="hidden" name="_method" value="DELETE">
																<button type="submit" class="btn btn-danger btn-raised btn-sm" onclick="return confirm('Yakin akan menghapus pengumuman?')">Delete</button>
																<a class="btn btn-warning btn-raised btn-sm" href="/course/announcement/edit/{{$announcement['id']}}">Edit</a>
															</form>
															<!--FLOATING FORM-->
															<p>
																<form class="form-group label-floating"  method="post" action="#">
																	{{csrf_field()}}
																	<div class="input-group">
																		<span class="input-group-addon">
																			<img src="{{avatar(Auth::user()->photo)}}" class="dq-image-user dq-ml">
																		</span>
																		<label class="control-label" for="addon2">Enter your comment</label>
																		<input type="text" id="addon2" class="form-control" name="comment">
																		<input type="hidden" name="course_announcement_id" value="{{$announcement['id']}}">
																		<span class="input-group-btn">
																			<button type="submit" class="btn btn-raised btn-primary">
																				Kirim
																			</button>
																		</span>
																	</div>
																</form>
															</p>

															<p>
																<div class="ms-collapse no-margin" id="accordion{{$announcement['id']}}" role="tablist" aria-multiselectable="true">
																	<div class="mb-0">
																		<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{$announcement['id']}}" href="#collapse{{$announcement['id']}}" aria-expanded="false" aria-controls="collapse{{$announcement['id']}}">
																				Comments({{count($announcement['announcement_comments'])}})
																		</a>
																		<div id="collapse{{$announcement['id']}}" class="card-collapse collapse" role="tabpanel">
																			@foreach($announcement['announcement_comments'] as $announcement_comment)
																			<div class="dq-padding-comment">
																				<span>
																					<div class="row">
																						<img src="{{avatar($announcement_comment->photo)}}" class="dq-image-user dq-ml">
																						<div class="col-md-8">
																							<div>
																								<span>
																									{{$announcement_comment->name}}
																								</span>
																								.
																								<span>{{$announcement_comment->created_at}}</span>
																								{{-- .
																								<span>
																									<a href="" data-toggle="tooltip" data-placement="top" title="Report Abuse">
																										<i class="fa fa-flag"></i>
																									</a>
																								</span> --}}
																							</div>
																							<div>
																								<span>
																									{{$announcement_comment->comment}}
																								</span>
																							</div>
																						</div>
																					</div>
																				</span>
																			</div>
																			@endforeach
																		</div>
																	</div>
																</div>
															</p>
														</div>
													</div>
												@endforeach
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal" id="modalEditGetCeritificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog animated zoomIn animated-3x" role="document">
									<div class="modal-header">
										<h4 class="modal-title">Pengaturan Sertifikat</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
									<div class="modal-content" style="padding: 20px;">
										<form action="/admin/courses/set-ceritifcated" method="POST">
											{{ csrf_field() }}
												<label for="">Mendapatkan Sertifikat Dengan Cara</label>
												<div class="demo-radio-button">
														<input name="get_ceritificate" type="radio" id="radio_1" value="0" {{ $course->get_ceritificate == '0' ? 'checked' : '' }} />
														<label for="radio_1">Tidak Ada</label>
														<input name="get_ceritificate" type="radio" id="radio_4" value="3" {{ $course->get_ceritificate == '3' ? 'checked' : '' }} />
														<label for="radio_4">Manual</label>
														<input name="get_ceritificate" type="radio" id="radio_2" value="1" {{ $course->get_ceritificate == '1' ? 'checked' : '' }} />
														<label for="radio_2">Persentase</label>
														<input name="get_ceritificate" type="radio" id="radio_3" value="2" {{ $course->get_ceritificate == '2' ? 'checked' : '' }} />
														<label for="radio_3">Grade Book</label>
												</div>
												<input type="hidden" name="id" value="{{ $course->id }}">
												<button type="submit" class="btn btn-primary">Simpan</button>
										</form>
									</div>
							</div>
						</div>

						<div class="modal" id="modalSetEditorDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
								<div class="modal-content">
									<textarea name="editorDescription" id="editorDescription"></textarea>
									<input type="button" id="saveEditorDescription" value="Save" class="btn btn-primary btn-raised">
								</div>
							</div>
						</div>

						<div class="modal" id="modalSetEditorGoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
								<div class="modal-content">
									<textarea name="editorGoal" id="editorGoal"></textarea>
									<input type="button" id="saveEditorGoal" value="Save" class="btn btn-primary btn-raised">
								</div>
							</div>
						</div>

						<div class="modal" id="modalSetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog animated zoomIn animated-3x" role="document">
								<div class="modal-content text-center" style="border-radius: .3rem;">
									<img class="img-fluid" src="{{$course->image}}" alt="{{$course->title}}" style="width: 100%; margin-bottom: 2rem;">
									<input type="file" id="editorImage" value="">
									<input type="button" id="saveEditorImage" value="Simpan" class="btn btn-primary btn-raised">
								</div>
							</div>
						</div>

						<div class="modal" id="modalEditSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
								<div class="modal-content">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Edit Section</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="">Judul</label>
												<input type="text" class="form-control" id="setSectionTitle">
											</div>

											<div class="form-group">
												<label for="">Deskripsi / Penjelasan</label>
												<textarea id="setSectionDescription" class="form-control" rows="6"></textarea>
											</div>
											<input type="hidden" id="setSectionId">
											<input type="button" id="saveEditSection" value="Save" class="btn btn-primary btn-raised">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal" id="modalAddContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
								<div class="modal-content">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title" id="contentForm"></h3>
										</div>
										<div class="panel-body">
											<form id="formAddContent" action="" method="post">
												{{csrf_field()}}

												<div class="form-group">
													<label for="title">Judul</label>
													<input placeholder="Judul" type="text" class="form-control" id="contentTitle" name="title" value="" required>
												</div>

												<div class="form-group">
													<label for="title">Deskripsi / Penjelasan</label>
													<textarea name="description" id="contentDescription"></textarea>
												</div>

												<div class="form-group">
													<label for="title">Tipe Konten</label>
													<select class="form-control selectpicker" id="contentTypeContent" name="type_content" required>
														<option value="">Pilih Tipe Konten</option>
														<option value="text">Text</option>
														<option value="video">Video</option>
														<option value="file">File</option>
													</select>
												</div>

												<div class="form-group">
													<label for="title">Lampiran (Dokumen / Video)</label>
													<input type="file" id="file" class="form-control">
													<input type="hidden" id="path_file" name="path_file">
													<input type="hidden" id="name_file" name="name_file">
													<input type="hidden" id="file_size" name="file_size">
													<input type="hidden" id="video_duration" name="video_duration">
												</div>

												<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
												<div id="container"></div>

												<div class="progress" style="display:none">
													<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
														<span class="sr-only">0%</span>
													</div>
												</div>
												<div id="uploadedMessage"></div>

												<div class="form-group">
													<label for="title">Urutan</label>
													<input placeholder="Urutan" id="contentSequence" type="text" class="form-control" name="sequence" value="" required>
												</div>

												<input type="hidden" name="id" id="contentId" value="">
												<div class="form-group">
													{{  Form::submit("Save Content" , array('class' => 'btn btn-primary', 'name' => 'button', 'id' => 'saveContent')) }}
												</div>
											{{ Form::close() }}
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal" id="modalAddQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
								<div class="modal-content">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title" id="quizForm"></h3>
										</div>
										<div class="panel-body">
											<form id="formAddQuiz" action="" method="post">
												{{csrf_field()}}

												<div class="form-group">
													{{-- <label for="name">Judul Kuis</label> --}}
													<input placeholder="Judul Kuis" type="text" class="form-control" name="name" id="quizname" required>
												</div>

												<div class="form-group">
													<label for="title">Deskripsi / Penjelasan</label>
													<textarea name="description" id="Quizdescription"></textarea>
												</div>
												<div class="form-group">
													<label for="title">Acak kuis?</label> &nbsp;
													<input type="radio" name="shuffle" checked value="0">Tidak &nbsp;
													<input type="radio" name="shuffle" value="1">Ya
												</div>
												<div class="form-group">
													{{-- <label for="name">Waktu Mulai</label> --}}
													<input placeholder="Waktu Mulai" type="text" class="form-control form_datetime" name="time_start" id="Quiztime_start" required>
												</div>
												<div class="form-group">
													{{-- <label for="name">Waktu Akhir</label> --}}
													<input placeholder="Waktu Akhir" type="text" class="form-control form_datetime" name="time_end" id="Quiztime_end" required>
												</div>
												<div class="form-group">
													<label for="name">Durasi Menit </label>
													<input placeholder="(Contoh: 60)" type="number" class="form-control" name="duration" id="Quizduration" required>
												</div>

												<input type="hidden" name="id" id="QuizId" value="">
												<div class="form-group">
													{{  Form::submit("Simpan Kuis" , array('class' => 'btn btn-raised btn-primary', 'name' => 'button', 'id' => 'saveQuiz')) }}
												</div>
											{{ Form::close() }}
										</div>
									</div>
								</div>
							</div>
						</div>

					{{-- </div> --}}
				{{-- </div> --}}
			</div>
		</div>
	</div>

	<div class="modal" id="ModalMenuContent{{ $section['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3>Tambah Konten / Kuis</h3>
							<div class="tab">
								<button class="tablinks" onclick="openCity(event, 'Label{{ $section['id'] }}')" id="defaultOpen"><i class="fa fa-tags"></i> Label</button>
								<button class="tablinks" onclick="openCity(event, 'Video{{ $section['id'] }}')"><i class="fa fa-video-camera"></i> Konten Video</button>
								<button class="tablinks" onclick="openCity(event, 'Teks{{ $section['id'] }}')"><i class="fa fa-text-height"></i> Konten Teks</button>
								<button class="tablinks" onclick="openCity(event, 'File{{ $section['id'] }}')"><i class="fa fa-file-o"></i> Konten File</button>
								<button class="tablinks" onclick="openCity(event, 'Kuis{{ $section['id'] }}')"><i class="fa fa-question"></i> Kuis</button>
								<button class="tablinks" onclick="openCity(event, 'Tugas{{ $section['id'] }}')"><i class="fa fa-tasks"></i> Tugas</button>
								<button class="tablinks" onclick="openCity(event, 'Library{{ $section['id'] }}')"><i class="fa fa-archive"></i> Library</button>
							</div>

							<div id="Label{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Label</h3>
								<p>
									Tambahkan label untuk informasi konten lebih jelas
								</p>

								<a href="/admin/course/content/create/{{ $section['id'] }}?content=label" class="btn btn-primary btn-raised">Tambah Label</a>

								<br>
							</div>

							<div id="Video{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Konten Video</h3>
								<p>
									Tambahkan konten video agar proses belajar menjadi lebih interaktif
								</p>
								{{-- <a href="/course/content/library/{{ $section['id'] }}?content=video" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<a href="/admin/course/content/create/{{ $section['id'] }}?content=video" class="btn btn-primary btn-raised">Upload Video</a>
								<a href="/admin/course/content/create/{{ $section['id'] }}?content=url&urlType=video" class="btn btn-primary btn-raised">Tambah URL Youtube</a>

								<br>
							</div>

							<div id="Teks{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Konten Teks</h3>
								<p>
									Anda bisa menambahkan konten dengan hanya teks saja. bisa digunakan seperti membuat pendahuluan dan lain-lain.
								</p>
								<a href="/admin/course/content/create/{{ $section['id'] }}?content=text" class="btn btn-primary btn-raised">Tambah Konten Teks</a>
								{{-- <a href="/course/content/library/{{ $section['id'] }}?content=text" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<br>
							</div>

							<div id="File{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Konten File</h3>
								<p>
									Anda bisa menambahkan konten file / berkas. bisa digunakan seperti membuat file pdf, power point dan lain-lain.
								</p>
								<a href="/admin/course/content/create/{{ $section['id'] }}?content=file" class="btn btn-primary btn-raised">Tambah Konten File</a>
								{{-- <a href="/course/content/library/{{ $section['id'] }}?content=file" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<a href="/admin/course/content/create/{{ $section['id'] }}?content=url&urlType=file" class="btn btn-primary btn-raised">Tambah Dari URL</a>

								<br>
							</div>

							<div id="Kuis{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Kuis</h3>
								<p>
									Tambahkan kuis untuk menguji peserta sejauhmana mereka belajar
								</p>
								<a href="/admin/course/quiz/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">Tambah Kuis</a>
								{{-- <a href="/admin/course/content/library/{{ $section['id'] }}?content=quiz" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<br>
							</div>

							<div id="Tugas{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Tugas</h3>
								<p>
									Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.
								</p>
								<a href="/admin/course/assignment/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">Tambah Tugas</a>
								<br>
							</div>

							<div id="Library{{ $section['id'] }}" class="tabcontent">
								<h3>Tambah Content Library</h3>
								<p>
									Anda dapat menambhakan kontent yang sudah tersedia di library.
								</p>
								<a href="#" data-toggle="modal" data-target="#importLibrary" class="btn btn-primary btn-raised">Import Dari Library</a>
								<br>
								<div class="modal" id="importLibrary" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
									<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
										<div class="modal-content">
											<div class="modal-body">
												<div class="nav-tabs-ver-container">
													<div class="row" style="display: flex;flex-wrap: wrap;">
														<div class="col-lg-2" style="    flex: 0 0 12.666667%;max-width: 12.666667%;">
															<ul class="nav nav-tabs-ver nav-tabs-ver-primary" role="tablist" style="height:100%">
																<li class="nav-item">
																	<a class="nav-link active" style="text-align:center" href="#library-tab" aria-controls="home3" role="tab" data-toggle="tab">
																		<i class="fa fa-building" style="margin:0"></i><br>Library
																	</a>
																</li>
																<li class="nav-item">
																	<a class="nav-link" href="#group-tab" style="text-align:center"  aria-controls="profile3" role="tab" data-toggle="tab">
																		<i class="fa fa-users" style="margin:0"></i><br>Group
																	</a>
																</li>
															</ul>
														</div>
														<div class="col-lg-10 nav-tabs-ver-container-content" style="flex: 0 0 87.333333%;max-width: 87.333333%;padding-right: 0px;">
															<div class="card-block" style="padding:0px;">
																<div class="tab-content">
																	<div role="tabpanel" class="tab-pane active" id="library-tab">
																		<div class="nav-tabs-ver-container">
																			<div class="row" style="display: flex;flex-wrap: wrap;">
																				<div class="col-lg-3" style="flex: 0 0 30%;max-width: 30%;padding-left:0">
																					<ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%;width:100%">
																						<li class="nav-item">
																							<a class="nav-link active" href="#my-library-tab" aria-controls="my-library-tab" role="tab" data-toggle="tab">
																								<i class="fa fa-building"></i> My Library
																							</a>
																						</li>
																						<li class="nav-item">
																							<a class="nav-link" href="#shared-library-tab" aria-controls="shared-library-tab" role="tab" data-toggle="tab">
																								<i class="fa fa-share-alt"></i> Shared Library
																							</a>
																						</li>
																						<li class="nav-item">
																							<a class="nav-link" href="#public-library-tab" aria-controls="profile3" role="tab" data-toggle="tab">
																								<i class="fa fa-users"></i> Public Library
																							</a>
																						</li>
																					</ul>
																				</div>
																				<div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%;max-width: 70%;">
																					<div class="card-block">
																						<div class="tab-content">
																							<div role="tabpanel" class="tab-pane active" id="my-library-tab">
																								<form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
																									{{csrf_field()}}
																									<div class="table-responsive table-cs">
																										<table id="example-1" class="display" style="width:100%">
																											<thead>
																												<tr>
																													<th></th>
																													<th>Nama</th>
																													<th class="text-center">Tipe</th>
																												</tr>
																											</thead>
																											<tbody>
																												@foreach($folder->where('user_id', Auth::user()->id) as $item)
																												<tr>
																													<td class="text-center"></td>
																													<td>
																														<a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
																															<i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
																														</a>
																														<div class="collapse" id="collapseExample-{{$item->id}}">
																															<div class="card card-body">
																																<table>
																																	<thead>
																																		<tr>
																																			<th></th>
																																			<th>Nama</th>
																																			<th class="text-center">Tipe</th>
																																		</tr>
																																	</thead>
																																	<tbody>
																																		@foreach($item->contents as $item_folder)
																																			<?php
																																				$data = dataContent($item_folder->library);
																																				$icon = content_type_icon($data->type_content);
																																				$type = 'Quiz';
																																				if($data->type_content){
																																					$type = $data->type_content;
																																				}else if($data->type == "0"){
																																					$type = 'Tugas';
																																					$icon = "fa fa-tasks";
																																				}
																																			?>
																																			<tr>
																																				<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																																				<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																																				<td class="text-center">{{$type}}</td>
																																			</tr>
																																		@endforeach
																																	</tbody>
																																</table>
																															</div>
																														</div>
																													</td>
																													<td class="text-center">Folder</td>
																												</tr>
																												@endforeach
																												@foreach($myLibrary[0]->library as $item)
																													<?php
																														$data = dataContent($item);
																														$icon = content_type_icon($data->type_content);
																														$type = 'Quiz';
																														if($data->type_content){
																															$type = $data->type_content;
																														}else if($data->type == "0"){
																															$type = 'Tugas';
																															$icon = "fa fa-tasks";
																														}
																													?>
																													<tr>
																														<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																														<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																														<td class="text-center">{{$type}}</td>
																													</tr>
																												@endforeach
																											</tbody>
																										</table>
																									</div>

																									<div class="modal-footer" style="padding-right: 0;">
																										<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
																										<button type="submit" class="btn btn-raised btn-primary" style="margin: 0px;">Tambah</button>
																									</div>
																								</form>
																							</div>
																							<div role="tabpanel" class="tab-pane" id="shared-library-tab">
																								<form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
																									{{csrf_field()}}
																									<div class="table-responsive table-cs">
																										<table id="example-2" class="display" style="width:100%">
																											<thead>
																												<tr>
																													<th></th>
																													<th>Nama</th>
																													<th class="text-center">Tipe</th>
																												</tr>
																											</thead>
																											<tbody>
																												@foreach($folder->where('user_id', '!=', Auth::user()->id) as $item)
																												<tr>
																													<td class="text-center"></td>
																													<td>
																														<a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExampleShared-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
																															<i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
																														</a>
																														<div class="collapse" id="collapseExampleShared-{{$item->id}}">
																															<div class="card card-body">
																																<table>
																																	<thead>
																																		<tr>
																																			<th></th>
																																			<th>Nama</th>
																																			<th class="text-center">Tipe</th>
																																		</tr>
																																	</thead>
																																	<tbody>
																																		@foreach($item->contents as $item_folder)
																																			<?php
																																				$data = dataContent($item_folder->library);
																																				$icon = content_type_icon($data->type_content);
																																				$type = 'Quiz';
																																				if($data->type_content){
																																					$type = $data->type_content;
																																				}else if($data->type == "0"){
																																					$type = 'Tugas';
																																					$icon = "fa fa-tasks";
																																				}
																																			?>
																																			<tr>
																																				<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																																				<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																																				<td class="text-center">{{$type}}</td>
																																			</tr>
																																		@endforeach
																																	</tbody>
																																</table>
																															</div>
																														</div>
																													</td>
																													<td class="text-center">Folder</td>
																												</tr>
																												@endforeach
																												@foreach($myLibrary[2]->library as $item)
																													<?php
																														$data = dataContent($item);
																														$icon = content_type_icon($data->type_content);
																														$type = 'Quiz';
																														if($data->type_content){
																															$type = $data->type_content;
																														}else if($data->type == "0"){
																															$type = 'Tugas';
																															$icon = "fa fa-tasks";
																														}
																													?>
																													<tr>
																														<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																														<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																														<td class="text-center">{{$type}}</td>
																													</tr>
																												@endforeach
																											</tbody>
																										</table>
																									</div>

																									<div class="modal-footer" style="padding-right: 0;">
																										<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
																										<button type="submit" class="btn btn-raised btn-primary" style="margin: 0px;">Tambah</button>
																									</div>
																								</form>
																							</div>
																							<div role="tabpanel" class="tab-pane" id="public-library-tab">
																								<form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
																									{{csrf_field()}}
																									<div class="table-responsive table-cs">
																										<table id="example-3" class="display" style="width:100%">
																											<thead>
																												<tr>
																													<th></th>
																													<th>Nama</th>
																													<th class="text-center">Tipe</th>
																												</tr>
																											</thead>
																											<tbody>
																												@foreach($folder->where('status', '1') as $item)
																													<tr>
																														<td class="text-center"></td>
																														<td>
																															<a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExamplePublic-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
																																<i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
																															</a>
																															<div class="collapse" id="collapseExamplePublic-{{$item->id}}">
																																<div class="card card-body">
																																	<table>
																																		<thead>
																																			<tr>
																																				<th></th>
																																				<th>Nama</th>
																																				<th class="text-center">Tipe</th>
																																			</tr>
																																		</thead>
																																		<tbody>
																																			@foreach($item->contents as $item_folder)
																																			<?php
																																				$data = dataContent($item_folder->library);
																																				$icon = content_type_icon($data->type_content);
																																				$type = 'Quiz';
																																				if($data->type_content){
																																					$type = $data->type_content;
																																				}else if($data->type == "0"){
																																					$type = 'Tugas';
																																					$icon = "fa fa-tasks";
																																				}
																																			?>
																																			<tr>
																																				<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																																				<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																																				<td class="text-center">{{$type}}</td>
																																			</tr>
																																			@endforeach
																																		</tbody>
																																	</table>
																																</div>
																															</div>
																														</td>
																														<td class="text-center">Folder</td>
																													</tr>
																												@endforeach

																												@foreach($publicLibrary->library as $item)
																													<?php
																														$data = dataContent($item);
																														$icon = content_type_icon($data->type_content);
																														$type = 'Quiz';
																														if($data->type_content){
																															$type = $data->type_content;
																														}else if($data->type == "0"){
																															$type = 'Tugas';
																															$icon = "fa fa-tasks";
																														}
																													?>
																													<tr>
																														<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																														<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																														<td class="text-center">{{$type}}</td>
																													</tr>
																												@endforeach
																											</tbody>
																										</table>
																									</div>

																									<div class="modal-footer" style="padding-right: 0;">
																										<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
																										<button type="submit" class="btn btn-raised btn-primary" style="margin: 0px;">Tambah</button>
																									</div>
																								</form>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div role="tabpanel" class="tab-pane" id="group-tab">
																		<div class="nav-tabs-ver-container">
																			<div class="row" style="display: flex;flex-wrap: wrap;">
																				<div class="col-lg-3" style="flex: 0 0 30%;max-width: 30%;padding-left:0">
																					<ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%">
																						@foreach($group as $index => $item)
																							<li class="nav-item">
																								<a class="nav-link {{$index == 0 ? 'active' : ''}}" href="#my-library-tab-{{$item->group_code}}" aria-controls="my-library-tab-{{$item->group_code}}" role="tab" data-toggle="tab">
																									<i class="fa fa-building"></i> {{$item->title}}
																								</a>
																							</li>
																						@endforeach
																					</ul>
																				</div>
																				<div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%;max-width: 70%;">
																					<div class="card-block">
																						<div class="tab-content">
																							@foreach($group as $index => $item)
																								<div role="tabpanel" class="tab-pane {{$index == 0 ? 'active' : ''}}" id="my-library-tab-{{$item->group_code}}">
																									<form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
																										{{csrf_field()}}
																										<div class="table-responsive table-cs">
																											<table id="example-group-{{$item->group_code}}" class="display" style="width:100%">
																												<thead>
																													<tr>
																														<th></th>
																														<th>Nama</th>
																														<th class="text-center">Tipe</th>
																													</tr>
																												</thead>
																												<tbody>
																													@foreach($item->library as $content)
																														@if($content->folder)
																															@if($content->folder->question_bank == '0')
																															<tr>
																																<td class="text-center"></td>
																																<td>
																																	<a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-Group-{{$content->folder->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-Group-{{$content->folder->id}}">
																																		<i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$content->folder->group_name}}
																																	</a>
																																	<div class="collapse" id="collapseExample-Group-{{$content->folder->id}}">
																																		<div class="card card-body">
																																			<table>
																																				<thead>
																																					<tr>
																																						<th></th>
																																						<th>Nama</th>
																																						<th class="text-center">Tipe</th>
																																					</tr>
																																				</thead>
																																				<tbody>
																																					@foreach($content->folder->contents as $item_folder)
																																						<?php
																																							$data = dataContent($item_folder->library);
																																							$icon = content_type_icon($data->type_content);
																																							$type = 'Quiz';
																																							if($data->type_content){
																																								$type = $data->type_content;
																																							}else if($data->type == "0"){
																																								$type = 'Tugas';
																																								$icon = "fa fa-tasks";
																																							}
																																						?>
																																						<tr>
																																							<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																																							<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																																							<td class="text-center">{{$type}}</td>
																																						</tr>
																																					@endforeach
																																				</tbody>
																																			</table>
																																		</div>
																																	</div>
																																</td>
																																<td class="text-center">Folder</td>
																															</tr>
																															@endif
																														@endif
																													@endforeach

																													@foreach($item->library as $content)
																														@if($content->content)
																															<?php
																																$data = dataContent($content->content);
																																$icon = content_type_icon($data->type_content);
																																$type = 'Quiz';
																																if($data->type_content){
																																	$type = $data->type_content;
																																}else if($data->type == "0"){
																																	$type = 'Tugas';
																																	$icon = "fa fa-tasks";
																																}
																															?>
																															<tr>
																																<td class="text-center"><input type="checkbox" name="content[]" style="position: inherit;" value="{{$data->id}}/{{$type}}"></td>
																																<td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
																																<td class="text-center">{{$type}}</td>
																															</tr>
																														@endif
																													@endforeach
																												</tbody>
																											</table>
																										</div>

																										<div class="modal-footer" style="padding-right: 0;">
																											<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
																											<button type="submit" class="btn btn-raised btn-primary" style="margin: 0px;">Tambah</button>
																										</div>
																									</form>
																								</div>
																							@endforeach
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<br><br><br><br> &nbsp;
							{{-- <a href="#" class="btn btn-default btn-raised">Batal</a> --}}
						</div>
					</div>
				</div>
		</div>
	</div>
</section>

@endsection

@push('script')
  <script type="text/javascript" src="{{asset('/js/bootstrap-editable.min.js')}}"></script>
  <script src="{{asset('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

  <script>
    $.fn.editable.defaults.mode = 'inline';

    $(document).ready(function() {

      $('#CourseTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'title',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseSubTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'subtitle',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        },
      });

      $('#CoursePrice').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'price',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseLevel').editable({
        value: {{$course->level_id}},
        source: [
          @php $course_levels = DB::table('course_levels')->get(); @endphp
          @foreach($course_levels as $course_level)
            {value: {{$course_level->id}}, text: '{{$course_level->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_level_course',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseCategory').editable({
        value: {{$course->id_category}},
        source: [
          @php $categories = DB::table('categories')->get(); @endphp
          @foreach($categories as $category)
            {value: {{$category->id}}, text: '{{$category->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_category',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

    });
</script>

{{-- CKEDITOR --}}
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script>
  const textDescription = `{!! $course->description !!}`;
  const textGoal = `{!! $course->goal !!}`;
  CKEDITOR.replace('editorDescription').setData(textDescription);
  CKEDITOR.replace('editorGoal').setData(textGoal);
	CKEDITOR.replace('contentDescription');
	CKEDITOR.replace('Quizdescription');
</script>
{{-- CKEDITOR --}}

<script>
  $("#saveEditorDescription").click(function(){
    var new_description = CKEDITOR.instances.editorDescription.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'description',
        value : new_description,
      },
      success : function(data){
        $("#modalSetEditorDescription").modal('hide')
        $("#recentDescription").html(new_description);
      }
    })
  })

  $("#saveEditorGoal").click(function(){
    var new_goal = CKEDITOR.instances.editorGoal.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'goal',
        value : new_goal,
      },
      success : function(data){
        $("#modalSetEditorGoal").modal('hide')
        $("#recentGoal").html(new_goal);
      }
    })
  })

  var fileName = '';
  $("#editorImage").change(function(e){
    fileName = e.target.files[0];
  })

  $("#saveEditorImage").click(function(e){
    var formData = new FormData();
    formData.append('value', fileName);
    formData.append('name', 'image');

    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,
      headers: {
				'X-CSRF-TOKEN': '{{csrf_token()}}'
			},	
      success : function(data){
        location.reload();
      }
    })
  })
</script>

{{-- add new section --}}
<script type="text/javascript">
  $("#add_new_section").click(function(){

    $("#section_row").val(parseInt($("#section_row").val())+1)
    var section_add_row = $("#section_row").val();

    $("#place_new_section").append(
      '<div class="panel panel-default" id="section_row_'+section_add_row+'">'+
        '<div class="panel-heading">'+
          '<h3 class="panel-title">Tambah Topik</h3>'+
        '</div>'+
        '<div class="panel-body">'+
          '<div class="form-group">'+
            '<input placeholder="Judul Topik" type="text" class="form-control" id="new_section_title_'+section_add_row+'" value="" required>'+
          '</div>'+
            '<button type="button" id="save_new_section" data-id="'+section_add_row+'" class="btn btn-primary btn-raised">Simpan</button> '+
            '<button type="button" id="remove_new_section" data-id="'+section_add_row+'" class="btn btn-danger btn-raised">Batal</button>'+
        '</div>'+
      '</div>'
    );
  });
</script>
{{-- add new section --}}

{{-- save new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#save_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var new_section_title = $("#new_section_title_"+data_id).val();

        var id_course = '{{$course->id}}';
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('course/save_section')}}",
          type : "POST",
          data : {
            title : new_section_title,
            id_course : id_course,
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save new section --}}

{{-- remove new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#remove_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        $("#section_row_"+data_id).remove();
    });
  });
</script>
{{-- remove new section --}}

{{-- set edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#EditSection',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var data_title = $(this).attr('data-title');
        var data_description = $(this).attr('data-description');

        $("#setSectionId").val(data_id)
        $("#setSectionTitle").val(data_title)
        $("#setSectionDescription").val(data_description)
        $("#modalEditSection").modal('show');
    });
  });
</script>
{{-- set edit section --}}

{{-- save edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#saveEditSection',function(e){
      e.preventDefault();
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('section/edit-section')}}",
          type : "POST",
          data : {
            title : $("#setSectionTitle").val(),
            description : $("#setSectionDescription").val(),
            id : $("#setSectionId").val(),
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save edit section --}}


<script type="text/javascript" src="{{asset('/plupload/plupload.full.min.js')}}"></script>

{{-- set sectionid on add content --}}
<script type="text/javascript">
  var data_section = '';
  $(function(){
    $(document).on('click','#AddContent',function(e){
      e.preventDefault();
        $("#contentForm").html('Tambah Konten');
        data_section = $(this).attr('data-section');
        $("#formAddContent").attr('action', '/admin/course/content/create_action/'+data_section)
        $("#modalAddContent").modal('show');

        // Custom example logic
        var path_file = $("#path_file").val();
        var name_file = $("#name_file").val();
        var file_size = $("#file_size").val();
        var video_duration = $("#video_duration").val();
        var section_id = data_section;
        // $("#saveContent").prop('disabled', true);

        var uploader = new plupload.Uploader({
          runtimes : 'html5,flash,silverlight,html4',
          browse_button : 'file', // you can pass an id...
          container: document.getElementById('container'), // ... or DOM Element itself
          url : '/course/content/upload/'+section_id,
          flash_swf_url : '/plupload/Moxie.swf',
          silverlight_xap_url : '/plupload/Moxie.xap',
          chunk_size: '1mb',
          dragdrop: true,
          headers: {
             'X-CSRF-TOKEN': '{{csrf_token()}}'
          },
          filters : {
            mime_types: [
              {title : "Document files", extensions : "docs,xlsx,pptx"},
              {title : "Video files", extensions : "mp4,webm"},
            ]
          },

          init: {
            PostInit: function() {
              document.getElementById('filelist').innerHTML = '';
            },

            FilesAdded: function(up, files) {
              plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                uploader.start();
                return false;
              });
            },

            UploadProgress: function(up, file) {
              // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
              $('.progress').show();
              $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
              $("#uploadedMessage").html('Upload dalam proses...');
            },

            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                // console.log('[FileUploaded] File:', file, "Info:", info);
                var response = JSON.parse(info.response);
                // console.log(response.result.original.path);
                path_file = $("#path_file").val(response.result.original.path);
                name_file = $("#name_file").val(response.result.original.name);
                file_size = $("#file_size").val(response.result.original.size);
                video_duration = $("#video_duration").val(response.result.original.video_duration);
            },

            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                // console.log('[ChunkUploaded] File:', file, "Info:", info);
            },

            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                // console.log('[UploadComplete]');
                // $("#saveContent").prop('disabled', false);
                $("#uploadedMessage").html('Upload selesai');
            },

            Error: function(up, err) {
              document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
          }
        });

        uploader.init();
    });
  });
</script>
{{-- set sectionid on add content --}}

<script>
// edit content
$(function(){
  $(document).on('click','#EditContent',function(e){
    e.preventDefault();
    $("#contentForm").html('Edit Content');
    $("#formAddContent").attr('action', '/course/content/update_action');
    var data_id = $(this).attr('data-id');
    var data_title = $(this).attr('data-title');
    var data_description = $(this).attr('data-description');
    var data_type_content = $(this).attr('data-type-content');
    var data_sequence = $(this).attr('data-sequence');
    var data_section = $(this).attr('data-section');
    $("#contentId").val(data_id);
    $("#contentTitle").val(data_title);
    CKEDITOR.instances['contentDescription'].setData(data_description)
    $("#contentTypeContent").val(data_type_content).change();
    $("#contentSequence").val(data_sequence);
    $("#modalAddContent").modal('show');

    // Custom example logic
    var path_file = $("#path_file").val();
    var name_file = $("#name_file").val();
    var file_size = $("#file_size").val();
    var video_duration = $("#video_duration").val();
    var section_id = data_section;
    // $("#saveContent").prop('disabled', true);

    var uploader = new plupload.Uploader({
      runtimes : 'html5,flash,silverlight,html4',
      browse_button : 'file', // you can pass an id...
      container: document.getElementById('container'), // ... or DOM Element itself
      url : '/course/content/upload/'+section_id,
      flash_swf_url : '/plupload/Moxie.swf',
      silverlight_xap_url : '/plupload/Moxie.xap',
      chunk_size: '1mb',
      dragdrop: true,
      headers: {
         'X-CSRF-TOKEN': '{{csrf_token()}}'
      },
      filters : {
        mime_types: [
          {title : "Document files", extensions : "docs,xlsx,pptx"},
          {title : "Video files", extensions : "mp4,webm"},
        ]
      },

      init: {
        PostInit: function() {
          document.getElementById('filelist').innerHTML = '';
        },

        FilesAdded: function(up, files) {
          plupload.each(files, function(file) {
            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            uploader.start();
            return false;
          });
        },

        UploadProgress: function(up, file) {
          // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
          $('.progress').show();
          $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
          $("#uploadedMessage").html('Upload dalam proses...');
        },

        FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            // console.log('[FileUploaded] File:', file, "Info:", info);
            var response = JSON.parse(info.response);
            // console.log(response.result.original.path);
            path_file = $("#path_file").val(response.result.original.path);
            name_file = $("#name_file").val(response.result.original.name);
            file_size = $("#file_size").val(response.result.original.size);
            video_duration = $("#video_duration").val(response.result.original.video_duration);
        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
            // $("#saveContent").prop('disabled', false);
            $("#uploadedMessage").html('Upload selesai');
        },

        Error: function(up, err) {
          document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
        }
      }
    });

    uploader.init();

  })
})
// edit content
</script>

{{-- datetime picker --}}
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript">
  $('.form_datetime').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
  });
</script>
{{-- datetime picker --}}

<script>
// add quiz
  var data_section = '';
  $(function(){
    $(document).on('click','#AddQuiz',function(e){
      $("#quizForm").html('Tambah Kuis');
      data_section = $(this).attr('data-section');
      $("#formAddQuiz").attr('action', '/course/quiz/create_action/'+data_section)
      $("#modalAddQuiz").modal('show');
    })
  })
  // add quiz
</script>

<script type="text/javascript">
  $("[name=publish{{$course->id}}]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
    var token = "{{csrf_token()}}";
    if(state === true){
      $.ajax({
        url : "/meeting/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          publish : 1,
          _token: token
        },
        success : function(result){

        },
      });
    }else{
      $.ajax({
        url : "/meeting/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          publish : 0,
          _token: token
        },
        success : function(result){

        },
      });
    }
  });
</script>


<script>
// trigger modal add konten / quiz
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

  // trigger modal add konten / quiz
</script>

<script>
$(function(){
  $(document).on('click','#webinarEditSchedule',function(e){
    $("#webinarId").val($(this).attr('data-id'));
    $("#webinarName").val($(this).attr('data-name'));
    $("#webinarDescription").val($(this).attr('data-description'));
    $("#webinarDate").val($(this).attr('data-date'));
    $("#webinarTime").val($(this).attr('data-time'));
    $("#webinarRecording").val($(this).attr('data-recording')).change();
    $("#modalWebinarEditSchedule").modal('show');
  })
})
</script>

{{-- sortable --}}
<script src="{{asset('/jquery-ui/jquery-ui.js')}}"></script>

<script>
  $( function() {
    $( ".SectionSortable" ).sortable({
      axis: 'y',
      connectWith: '.SectionSortable',
      helper: 'clone',
      out: function(event, ui) {
        var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
        console.log(itemOrder)

        $.ajax({
          url : '/course/section/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'course_id' : '{{$course->id}}',
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    });
    $( ".SectionSortable" ).disableSelection();
  });
</script>

<script>
  $( function() {
    var section_id = ''
    $( ".ContentSortable" ).sortable({
      axis: 'y',
      connectWith: '.ContentSortable',
      helper: 'clone',
      placeholder: "placeholder",
      over:function(event,ui){
        section_id = $('.placeholder').parent().attr('id');
      },
      out: function(event, ui) {
        var sequence = ui.item.index();
        var content_id = ui.item.attr('id');
        var itemOrder = $(this).sortable('toArray', { attribute: 'id' });
        console.log(itemOrder);
        $.ajax({
          url : '/course/content/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'section_id' : section_id,
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    }).disableSelection();
  });
</script>

{{-- sortable --}}

<script type="text/javascript">
  $("[class=preview]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
    var token = "{{csrf_token()}}";
    var content_id = $(this)[0].value;
    if(state === true){
      // alert(state);
      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 1,
          _token: token
        },
        success : function(result){
          // alert(result.message);
          if(result.alert == 'error'){
            $("[id=preview"+content_id+"]").bootstrapSwitch('state', false)
          }
          swal(result.title, result.message, result.alert);
        },
      });
    }else{
      // alert(state);
      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 0,
          _token: token
        },
        success : function(result){
          // swal(result.title, result.message, result.alert);
        },
      });
    }
  });
</script>

<script type="text/javascript">
  // $(function(){
  //   $(document).ready(function(){
  //     $('.get-from-library').on('click', function(){
  //       $('#content-id-value').val($('.get-from-library').attr('content-id'));
  //       alert($('#content-id-value').val());
  //     })
  //   })
  // })
</script>

<script type="text/javascript">
	$(function(){
		$(document).on('click','#preview_content',function(e){
			alert()
		})
	})
  
	// $(".preview_content").click(function(){
  //   var data_id = $(this).attr('value');
  //   var token = "{{csrf_token()}}";
  //   var content_id = $(this).attr('value');
  //   // alert($(this).prop('checked'))
  //   if($(this).prop('checked') == true){

  //     $.ajax({
  //       url : "/course/content/update-preview",
  //       type : "POST",
  //       data : {
  //         id : content_id,
  //         course_id : '{{$course->id}}',
  //         preview : 1,
  //         _token: token
  //       },
  //       success : function(result){
  //         // alert(result.message);
  //         swal(result.title, result.message, result.alert);
  //         if(result.alert == 'error'){
  //           $("#previewEye"+data_id).removeClass('color-success');
  //           $("#previewEye"+data_id).addClass('color-dark');
  //         }else{
  //           $("#previewEye"+data_id).removeClass('color-dark');
  //           $("#previewEye"+data_id).addClass('color-success');
  //         }
  //       },
  //     });

  //   }else{

  //     $.ajax({
  //       url : "/course/content/update-preview",
  //       type : "POST",
  //       data : {
  //         id : content_id,
  //         course_id : '{{$course->id}}',
  //         preview : 0,
  //         _token: token
  //       },
  //       success : function(result){
  //         // swal(result.title, result.message, result.alert);
  //         $("#previewEye"+data_id).removeClass('color-success');
  //         $("#previewEye"+data_id).addClass('color-dark');
  //       },
  //     });

  //   }
  // })
</script>

{{-- instructor n group --}}
<script type="text/javascript" src="{{asset('select2/js/select2.min.js')}}"></script>
<script type="text/javascript">
  $("#id_author").select2({
     placeholder: "Pilih pengajar"
  });
  $("#id_instructor_group").select2({
     placeholder: "Pilih group"
  });
</script>
{{-- instructor n group --}}

<script type="text/javascript">
  $("#saveAuthor").click(function(){
    $.ajax({
      url : "/course/preview/update-authors",
      type : "POST",
      data : {
        course_id : '{{$course->id}}',
        authors : $("#id_author").val(),
        _token: '{{csrf_token()}}'
      },
      success : function(result){
        console.log(result);
        window.location.reload();
      },
    });
  })

  $("#saveInstructorGroup").click(function(){
    $.ajax({
      url : "/course/preview/update-instructor-groups",
      type : "POST",
      data : {
        course_id : '{{$course->id}}',
        id_instructor_group : $("#id_instructor_group").val(),
        _token: '{{csrf_token()}}'
      },
      success : function(result){
        // console.log(result);
        window.location.reload();
      },
    });
  })
</script>

<script>
	$('#example-1').DataTable();
	$('#example-2').DataTable();
	$('#example-3').DataTable();
	@foreach($group as $index => $item)
	$('#example-group-{{$item->group_code}}').DataTable();
	@endforeach

	$("#example-1_wrapper .dataTables_length").html("My Library");
	$("#example-2_wrapper .dataTables_length").html("Shared Library");
	$("#example-3_wrapper .dataTables_length").html("Public Library");
	@foreach($group as $index => $item)
	$("#example-group-{{$item->group_code}}_wrapper .dataTables_length").html("{{$item->title}}");
	@endforeach
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<style>
	.table-cs .dataTables_length{
	background: #4ca3d9;
	color: white !important;
	padding: 0px 10px;
	border-radius: 10px;
	}

	.table-cs input{
	box-shadow: 0px 5px 18px -10px;
	border-radius: 10px;
	border: 1px solid #00000014;
	padding-left: 15px;
	}

	.table-cs thead>tr>th{
	border-bottom: none;
	padding: 8px 10px;
	}

	.table-cs tbody>tr>td{
	background: white !important;
	}

	.table-cs tbody>tr>td{
	background: white !important;
	}

	.table-cs .dataTables_paginate .paginate_button{
	}

	.table-cs .dataTables_paginate span a.paginate_button
	{
	padding: 0;
	/* background: #4cd137; */
	background: none;
	border-radius: 50px;
	border:none;
	}

	.table-cs .dataTables_paginate span a.paginate_button.current{
	background: #4cd137;
	color:white !important;
	}

	.table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after{
		display: none !important
	}

	.table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child{
		width: 0.1px !important;
		padding: 0;
	}

	.table-cs .dataTables_info{
	display: none;
	}

	.dataTables_filter{
	margin-bottom: 15px;
	}
</style>

<script type="text/javascript">
  $("[name=status{{$course->id}}]").on("change", function() {
    var state = $("[name=status{{$course->id}}]").is(":checked")
    var token = "{{csrf_token()}}";
    if(state === true){
      $.ajax({
        url : "/course/publish",
        type : "POST",
        data : {
          id : '{{$course->id}}',
          status : 1,
          live : {{is_liveCourse($course->id) == true ? 'true' : 'false'}},
          _token: token
        },
        success : function(result){
          // console.log(result);
          if(result.status != 200){
            swal({
              type: 'warning',
              title: 'Gagal...',
              html: result.message,
            })
          }else{
            swal("Berhasil!", "Kursus Anda telah aktif dan dapat diakses", "success");
          }
        },
      });
    }else{
      $.ajax({
        url : "/course/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          status : 0,
          _token: token,
          live : '{{is_liveCourse($course->id) == true ? 'true' : 'false'}}',
        },
        success : function(result){

        },
      });
    }
  });
</script>

<script>
	$(".sectionActivated").change(function(){
		var sectionStatus = $(this).prop('checked')
		// alert($(this).attr('id-section'))
		$.ajax({
			url: "/course/section/change-status",
			type: "post",
			data: {
				section_id: $(this).attr('id-section'),
				status: sectionStatus,
				_token: "{{csrf_token()}}"
			},
			success: function(){
				// alert()
			}
		})
	})
</script>
@endpush
