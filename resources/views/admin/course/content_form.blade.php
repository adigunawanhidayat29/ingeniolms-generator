@extends('admin.layouts.app')

@section('title')
	{{ 'Kelola Konten ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{asset('select2/css/select2.min.css')}}">

	<!-- <style>
		ul.breadcrumb {
				padding: 10px 16px;
				list-style: none;
				background-color: #eee;
		}
		ul.breadcrumb li {
				display: inline;
				font-size: 18px;
		}
		ul.breadcrumb li+li:before {
				padding: 8px;
				color: black;
				content: "/\00a0";
		}
		ul.breadcrumb li a {
				color: #0275d8;
				text-decoration: none;
		}
		ul.breadcrumb li a:hover {
				color: #01447e;
				text-decoration: underline;
		}
	</style> -->

@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Courses</h2>
		</div>

		<div class="row clearfix">
			<!-- Task Info -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="header">
						<h2>Create Kontent</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<div style="padding:25px;margin-top:0px;">
							<div class="box-body">
								<center>
								@if(Session::has('success'))
									<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
									</div>
								@elseif(Session::has('error'))
									<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
									</div>
								@endif
								</center>
								<div class="card-md">
								  <!-- <div class="panel-heading">
								    <h3 class="panel-title">{{$button}} Konten</h3>
								  </div> -->
								  <div class="card-block">
										{{ Form::open(array('url' => $action, 'method' => $method)) }}
										@if(Request::get('content') == 'video')
											<div class="form-group" id="FileVideo">
												<label for="title">Pilih file untuk diupload</label>
												<input type="text" readonly="" class="form-control" placeholder="Pilih...">
												<input type="file" id="file" class="form-control">
												<input type="hidden" id="path_file" name="path_file">
												<input type="hidden" id="name_file" name="name_file">
												<input type="hidden" id="file_size" name="file_size">
												<input type="hidden" id="video_duration" name="video_duration">
												<input type="hidden" id="full_path_original" name="full_path_original">
												<input type="hidden" id="resize_path" name="resize_path">
												@if($button == "Update")
													<i class="{{content_type_icon($type_content)}}"></i> <a href="#">{{$title}}</a>
												@endif

												<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
												<div id="container"></div>

												<div class="progress" style="display:none">
													<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
														<span class="sr-only">0%</span>
													</div>
												</div>

												<div id="uploadedMessageError">
													<div id="uploadedMessage"></div>
												</div>
											</div>
										@endif

											@if(Request::get('content') == 'file')
												<div class="form-group" id="FileFile">
													<label for="title">File (Docx, PDF, xlsx, etc.)</label>
													<input type="text" readonly="" class="form-control" placeholder="Pilih...">
													<input type="file" id="file" class="form-control">
													<input type="hidden" id="path_file" name="path_file">
													<input type="hidden" id="name_file" name="name_file">

													<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
													<div id="container"></div>

													<div class="progress" style="display:none">
														<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
															<span class="sr-only">0%</span>
														</div>
													</div>

													<div id="uploadedMessageError">
														<div id="uploadedMessage"></div>
													</div>
												</div>
											@endif

	                    @if(Request::get('content') == 'url')

	                      @if(Request::get('urlType')=='video')
	                        <div class="form-group" id="YoutubeVids">
	                          <label for="title">Cari Video Youtube (Copy Link untuk mendapatkan URL)</label>
	                          <br/>
	                          <input type="text" id="search" placeholder="Masukkan kata kunci video" autocomplete="off" class="form-control" />
	                          <button class="btn btn-primary btn-raised" type="button" class="form-control btn btn-primary w100" onclick="searchVideo()">Cari Video</button>
	                          <div id="YouLib" style="display: none;">
	                            <div class="card card-body" style="background: #F5F5F5;">
	                              <table class="table table-no-border">
	                              </table>
	                            </div>
	                          </div>
	                        </div>
	                      @endif

												<div class="form-group" id="FileURL">
													<label for="title">URL</label>
													<input id="contentFullPathFile" placeholder="Masukan URL" type="text" class="form-control" name="full_path_file" value="" required>
	                      </div>


											@endif

											@if(Request::get('content') != 'label')
												<div class="form-group" id="divTitle">
													<label for="title">Judul</label>
													<input id="contentTitle" placeholder="Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
													@if ($errors->has('title'))
														<span class="text-danger">{{ $errors->first('title') }}</span>
													@endif
												</div>
											@endif

											<div class="form-group" id="divDescription">
												<label for="title">Deskripsi / Penjelasan</label>
												<textarea name="description" id="description">{{ $description }}</textarea>
											</div>

											<div class="form-group">
												<input name="id" type="hidden" id="contentId" value="{{$id}}">
												{{-- {{  Form::submit($button . " Content" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button', 'id' => 'saveContent')) }} --}}
												{{-- <button type="button" id="btnSaveDraft" class="btn btn-primary btn-raised">Simpan Sebagai Draft</button> --}}
												<button onclick="afterClick()" type="button" id="contentSave" class="btn btn-success btn-raised">Simpan dan Terbitkan</button>
												<a href="{{ url('admin/course/manage/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a>
											</div>
										{{ Form::close() }}
								  </div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('script')

	<script>
		function afterClick(){
			$("#contentSave").removeClass('btn-success');
			$("#contentSave").removeClass('btn-raised');
			$("#contentSave").html('<i class="fa fa-circle-o-notch fa-spin"></i>Menyimpan');
		}
	</script>

	{{-- CKEDITOR --}}
	{{-- <script src="{{asset('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	<script type="text/javascript">
		@if(Request::get('content') == 'video')
			$("#divTitle").hide()
			$("#divDescription").hide()
			$("#contentSave").hide()
			// $("#divSequence").hide()
		@endif
	</script>

	<script type="text/javascript" src="{{asset('/plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript">
		// Custom example logic
		var path_file = $("#path_file").val();
		var name_file = $("#name_file").val();
		var file_size = $("#file_size").val();
		var video_duration = $("#video_duration").val();
		var full_path_original = $("#full_path_original").val();
		var resize_path = $("#resize_path").val();
		// $("#saveContent").prop('disabled', true);
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'file', // you can pass an id...
			container: document.getElementById('container'), // ... or DOM Element itself
			url : '/admin/course/content/upload/{{$section->id}}',
			flash_swf_url : '/plupload/Moxie.swf',
			silverlight_xap_url : '/plupload/Moxie.xap',
			chunk_size: '1mb',
			dragdrop: true,
			headers: {
				'X-CSRF-TOKEN': '{{csrf_token()}}'
			},
			filters : {
				mime_types: [
					@if(Request::get('content') == 'video')
						{title : "Video files", extensions : "mp4,webm,avi,mkv"},
					@else
						{title : "Document files", extensions : "docx,doc,xlsx,pptx,pdf,xls,txt"},
					@endif
				]
			},

			init: {
				PostInit: function() {
					document.getElementById('filelist').innerHTML = '';
				},

				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						$("#divTitle").show()
						$("#divDescription").show()
						$("#contentTitle").val(file.name)
						$("#contentSave").show()

						$.ajax({
							url: '/admin/course/content/store-draft',
							type: 'POST',
							data: {
								_token : '{{csrf_token()}}',
								field: 'title',
								value: file.name,
								section_id: '{{Request::segment(5)}}',
								type_content: '{{Request::get('content') ? Request::get('content') : $type_content}}',
								id: $("#contentId").val()
							},
							success: function(response){
								// alert('saved draft');
								$("#contentId").val(response.id)
							}
						})

						uploader.start();
						$("#contentSave").prop('disabled', true);
						return false;
					});
				},

				UploadProgress: function(up, file) {
					// document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
					$('.progress').show();
					if(file.percent < 97){
						$('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
					}
					$("#uploadedMessage").html('Upload masih dalam proses...');
					$("#uploadedMessage").addClass('alert alert-info');
				},

				FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            // console.log('[FileUploaded] File:', file, "Info:", info);
						var response = JSON.parse(info.response);
						// console.log(response.result.original);
						path_file = $("#path_file").val(response.result.original.path);
						name_file = $("#name_file").val(response.result.original.name);
						file_size = $("#file_size").val(response.result.original.size);
						video_duration = $("#video_duration").val(response.result.original.video_duration);
						full_path_original = $("#full_path_original").val(response.result.original.full_path_original);
						resize_path = $("#resize_path").val(response.result.original.resize_path);

						$.ajax({
							url: '/admin/course/content/store-draft',
							type: 'POST',
							data: {
								_token : '{{csrf_token()}}',
								file: true,
								path_file: response.result.original.path,
								name_file: response.result.original.name,
								file_size: response.result.original.size,
								video_duration: response.result.original.video_duration,
								full_path_original: response.result.original.full_path_original,
								resize_path: response.result.original.resize_path,
								section_id: '{{Request::segment(5)}}',
								type_content: '{{Request::get('content') ? Request::get('content') : $type_content}}',
								id: $("#contentId").val()
							},
							success: function(response){
								// alert('saved draft');
								$("#contentId").val(response.id)
							}
						})

        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
						// $("#saveContent").prop('disabled', false);
						// console.log(up);
						$('#progressBar').attr('aria-valuenow', 100).css('width', 100 + '%').text(100 + '%');
						$("#uploadedMessage").html('Upload selesai');
						$("#uploadedMessage").addClass('alert alert-success');
						$("#contentSave").prop('disabled', false);
        },

				Error: function(up, err) {
					// console.log(up);
					console.log(err);
					// document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
					if(err.status == 500 || err.code == "-200"){
						$("#uploadedMessage").html('');
						$("#uploadedMessageError").html('Upload Gagal');
						$("#uploadedMessageError").addClass('alert alert-danger');
						console.log('Upload Gagal');
					}
				},

				Destroy : function(){
					console.log('error');
				}
			}
		});

		uploader.init();
	</script>

	<script type="text/javascript">
		// kondisi type koenten
		var type_content = '{{ Request::get('content') ? Request::get('content') : $type_content }}'
		@if(Request::get('urlType'))
			type_content = 'url-' + '{{Request::get('urlType')}}'
		@endif
		// kondisi type koenten

		$("#contentFullPathFile").change(function(){
			$.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'full_path_file',
					value: $(this).val(),
					section_id: '{{Request::segment(5)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		})

		$("#contentTitle").change(function(){
			$.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'title',
					value: $(this).val(),
					section_id: '{{Request::segment(5)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					//console.log('Changed')
					$("#contentId").val(response.id)
				}
			})
		})

		CKEDITOR.instances.description.on('change', function() {
			$.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'description',
					value: CKEDITOR.instances['description'].getData(),
					section_id: '{{Request::segment(5)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		});

		$("#contentType").change(function(){
			$.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'type_content',
					value: $(this).val(),
					section_id: '{{Request::segment(5)}}',
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		})

		// $("#contentSequence").change(function(){
		// 	$.ajax({
		// 		url: '/admin/course/content/store-draft',
		// 		type: 'POST',
		// 		data: {
		// 			_token : '{{csrf_token()}}',
		// 			field: 'sequence',
		// 			value: $(this).val(),
		// 			section_id: '{{Request::segment(5)}}',
		// 			type_content: $('#contentType option:selected').val(),
		// 			id: $("#contentId").val()
		// 		},
		// 		success: function(response){
		// 			// alert('saved draft');
		// 			$("#contentId").val(response.id)
		// 		}
		// 	})
		// })

		// save draft
		$("#btnSaveDraft").click(function(){
			window.location.assign('/admin/course/preview/{{$section->course_id}}')
		})

		// save n publish
		$("#contentSave").click(function(){
			$.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'status',
					value: '1',
					section_id: '{{Request::segment(5)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					window.location.assign('/admin/course/manage/{{$section->id_course}}')
				}
			})
		})

  </script>

  <script>
		function searchVideo(){
			init();
			let keyword = $('#search').val();
			var request = gapi.client.youtube.search.list({
				part: "snippet",
				type: "video",
				q: encodeURIComponent(keyword).replace(/%20/g, "+"),
				maxResults: 10,
				order: "viewCount"
			});
			// execute the request
			request.execute(function(response) {
				var results = response.result;
				$("#results").html("");
				$('table').empty();
				$.each(results.items, function(index, item) {
					// console.log(index);
					console.log(item);
					$('table').append('<tr>' +
							'<td><img src="'+item.snippet.thumbnails.default.url+'"></td>' +
							'<td><h4>'+item.snippet.title+'</h4> <br/></td>' +
							'<td><button class="btn btn-danger btn-raised" type="button" onclick="window.open(\'https://www.youtube.com/watch?v='+item.id.videoId+'\',\'_blank\')" title="Preview video in new tab">Preview</button>' +
							// '<button class="btn btn-primary btn-raised" type="button" onclick="copyClipboard(this)" value="https://www.youtube.com/watch?v='+item.id.videoId+'" title="Copy link into clipboard">Copy Link</button></td>' +
							'<button class="btn btn-success btn-raised" type="button" id="setYoutubeVideo" data-title="'+item.snippet.title+'" data-description="'+item.snippet.description+'" data-url="https://www.youtube.com/watch?v='+item.id.videoId+'" value="https://www.youtube.com/watch?v='+item.id.videoId+'" title="Pilih vidio ini">Pilih</button></td>' +
							'</tr>');
				});
				// resetVideoHeight();
				$('#YouLib').css('display','block');
			});
			// $(window).on("resize", resetVideoHeight);
		}

		function copyClipboard(btnObj) {
			/* Get the text field */
			var copyText = btnObj.value;
			var dummy = $('<input>').val(copyText).appendTo('body').select();
			document.execCommand("copy");
		}

    $(document).on('click', "#setYoutubeVideo" , function() {
        var url = $(this).attr('data-url');
        var title = $(this).attr('data-title');
        var description = $(this).attr('data-description');
        // alert(description)

        $("#contentFullPathFile").val(url);
        $("#contentTitle").val(title);
        changeValues(title, url, description);

        // CKEDITOR.instances['description'].setData(description);
    });

    function changeValues(title, url, description) {

      $.ajax({
				url: '/admin/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'title',
					value: title,
					section_id: '{{Request::segment(5)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
          $.ajax({
            url: '/admin/course/content/store-draft',
            type: 'POST',
            data: {
              _token : '{{csrf_token()}}',
              field: 'full_path_file',
              value: url,
              section_id: '{{Request::segment(5)}}',
              type_content: type_content,
              id: response.id
            },
            success: function(response){
              // alert('saved draft');
              // $("#contentId").val(response.id)
            }
          })
				}
			})
    }

		function init() {
			gapi.client.setApiKey("AIzaSyC1qnx101nz-Yr-kCQgrL93cWXX0F5OFww");
			gapi.client.load("youtube", "v3", function() {
				// yt api is ready
			});
		}
	</script>
	<script src="https://apis.google.com/js/client.js?onload=init"></script>
@endpush
