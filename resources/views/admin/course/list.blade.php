@extends('admin.layouts.app')

@section('title', Lang::get('back.course.title'))

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>@lang('back.course.header')</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div style="margin-bottom: 2rem;">
								<a href="{{url( \Request::route()->getPrefix() . '/course/create')}}" class="btn btn-primary">@lang('back.course.create_button')</a>
								<a href="#" data-toggle="modal" data-target="#modalImport" class="btn btn-success">@lang('back.course.import_button')</a>
							</div>
							<div class="table-responsive">
								<center>
									@if(Session::has('success'))
										<div class="alert alert-success alert-dismissible">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											{!! Session::get('success') !!}
										</div>
									@elseif(Session::has('error'))
										<div class="alert alert-error alert-dismissible">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											{!! Session::get('error') !!}
										</div>
									@endif
								</center>
								<table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
									<thead>
										<tr>
											<th>#</th>
											<th>@lang('back.course.name_table')</th>
											<th>@lang('back.course.image_table')</th>
											<th>@lang('back.course.code_table')</th>
											<th>@lang('back.course.created_table')</th>
											<th>@lang('back.course.option_table')</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">@lang('back.course.import_modal')</h4>
	      </div>
	      <div class="modal-body">
					<form action="/admin/course/import" enctype="multipart/form-data" method="post">
						{{csrf_field()}}
						<div class="form-group">
							<div class="form-line">
								<label for="">@lang('back.course.file_label')</label>
								<input type="file" name="file" class="form-control">
							</div>
						</div>
						<a class="btn btn-primary" href="/excel/format/course.xlsx">@lang('back.course.download_modal')</a>
						<input type="submit" class="btn btn-success" value="@lang('back.course.submit_button')">
					</form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection

@push('script')
  <script type="text/javascript">
  $(function(){
		var asset_url = '{{asset_url()}}';
    $("#data").DataTable({
			"order": [[ 3, "desc" ]],
      processing: true,
      serverSide: true,
      ajax: '{{ url( \Request::route()->getPrefix() . "/course/serverside") }}',
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'title', name: 'title' },
				{
					"data": "image",
					"render": function(data, type, row) {
						return '<img src="'+data+'" width=\"100\"//>';
					},
				},
				{ data: 'password', name: 'password' },
				{ data: 'created_at', name: 'created_at' },
        { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
  </script>
@endpush
