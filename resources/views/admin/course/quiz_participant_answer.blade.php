@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Answer' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Answer List</h3>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>Question</th>
                  <th>Answer</th>
									<th>Is Correct</th>
                </tr>
                @foreach($quiz_participants as $quiz_participant)
									<tr class="{{$quiz_participant->answer_correct == '1' ? 'success' : 'danger'}}">
										<td>{!!$quiz_participant->question!!}</td>
										<td>{!!$quiz_participant->answer!!}</td>
										<td>{{$quiz_participant->answer_correct == '1' ? 'Correct' : 'Incorrect'}}</td>
									</tr>
								@endforeach
              </table>
            </div>						
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
