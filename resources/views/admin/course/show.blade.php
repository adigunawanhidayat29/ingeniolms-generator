@extends('admin.layouts.app')

@section('title')
	{{ 'Course' }}
@endsection


@section('content')
	<section class="content">
		<div class="container-fluid">
			<center>
				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('success') !!}
					</div>
				@elseif(Session::has('error'))
					<div class="alert alert-error alert-dismissible">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{!! Session::get('error') !!}
					</div>
				@endif
			</center>
			<div class="block-header">
				<h2>DETAIL KELAS - {{$courses->title}}</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
              {{-- <div style="margin-bottom: 2rem;">
                <a class="btn bg-blue-grey" href="{{url('admin/courses')}}">Kembali</a>
							</div> --}}

							<div style="margin-bottom: 2rem;">
								<a class="btn bg-blue-grey" href="{{ url('admin/courses') }}"><i class="material-icons">arrow_back</i><span>Kembali</span></a>
								<a class="btn btn-warning btn-sm" href="{{ url('admin/course/manage/' . $courses->id) }}"><i class="material-icons">pending</i><span>Kelola</span></a>
								<form method="POST" action="{{ url('admin/course/delete/' . $courses->id) }}" accept-charset="UTF-8" style="display:inline">
									{{ method_field('DELETE') }}
									{{ csrf_field() }}
									<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i><span>Hapus</span></button>
								</form>
							</div>
							
							<div class="table-responsive">
								<table class="table table-hover" style="margin-bottom: 0;">
									<tbody>
										<tr>
											<td style="font-weight: 700; border-top: 0;">Judul</td>
											<td style="border-top: 0;">{{$courses->title}}</td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Gambar</td>
											<td><img src="{{asset_url($courses->image)}}" alt="{{$courses->title}}" style="max-width: 500px;" /></td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Tujuan</td>
											<td>{!!$courses->goal!!}</td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Deskripsi</td>
											<td>{!!$courses->description!!}</td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Kategori</td>
											<td>{{$courses->category}}</td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Level</td>
											<td>{{$courses->level}}</td>
										</tr>
										<tr>
											<td style="font-weight: 700;">Penulis</td>
											<td>{{$courses->author}}</td>
										</tr>
										<tr>
											<td style="font-weight: 700; border-bottom: 0;">Peserta</td>
											<td style="border-bottom: 0;">
												{{$course_user}} peserta 
												<a href="{{url( \Request::route()->getPrefix() . '/course/participant/'. $courses->id)}}" style="display: block; font-size: 12px; text-decoration: underline;">Daftar Peserta</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
@endsection
