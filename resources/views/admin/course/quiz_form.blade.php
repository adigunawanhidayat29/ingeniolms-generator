@extends('admin.layouts.app')

@section('title')
	{{ 'Kelola Konten ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">

	<!-- <style>
		ul.breadcrumb {
				padding: 10px 16px;
				list-style: none;
				background-color: #eee;
		}
		ul.breadcrumb li {
				display: inline;
				font-size: 18px;
		}
		ul.breadcrumb li+li:before {
				padding: 8px;
				color: black;
				content: "/\00a0";
		}
		ul.breadcrumb li a {
				color: #0275d8;
				text-decoration: none;
		}
		ul.breadcrumb li a:hover {
				color: #01447e;
				text-decoration: underline;
		}
	</style> -->

@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Courses</h2>
		</div>

		<div class="row clearfix">
			<!-- Task Info -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="header">
						<h2>Create Quiz</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<div style="padding:25px;margin-top:0px;">
							<div class="">
								<center>
		              @if(Session::has('success'))
		                <div class="alert alert-success alert-dismissible">
		                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                  {!! Session::get('success') !!}
		                </div>
		              @elseif(Session::has('error'))
		                <div class="alert alert-error alert-dismissible">
		                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                  {!! Session::get('error') !!}
		                </div>
		              @endif
		            </center>

								<div class="card-md">
								  <div class="card-block">
										{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

											<div class="form-group">
												<label for="name">Judul Kuis</label>
												<input placeholder="Judul Kuis" type="text" class="form-control" name="name" value="{{ $name }}" required>
											</div>

											<div class="form-group">
												<label for="title">Deskripsi / Penjelasan</label>
												<textarea name="description" id="description">{{ $description }}</textarea>
											</div>
											{{-- <div class="form-group">
												<label for="title">Acak?</label><br>
												<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
												<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
											</div> --}}
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="name">Tanggal Mulai</label>
														<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : date("Y-m-d") }}" required>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="name">Jam Mulai</label>
														<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ isset($time_start) ? date("H:i:s", strtotime($time_start)) : '08:00' }}" required>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="name">Tanggal Selesai</label>
														<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}" required>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="name">Jam Selesai</label>
														<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ isset($time_end) ? date("H:i:s", strtotime($time_end)) : '08:00' }}" required>
													</div>
												</div>
											</div>


											<div class="form-group">
												<label for="name">Durasi (Menit)</label>
												<input placeholder="60" type="number" class="form-control" name="duration" value="{{ $duration }}" required>
											</div>
											{{-- <div class="form-group">
												<label for="name">Kesempatan / Percobaan</label>
												<input placeholder="3" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
											</div> --}}
											<input placeholder="3" type="hidden" class="form-control" name="attempt" value="{{ $attempt }}">
											<div class="form-group">
												{{  Form::hidden('id', $id) }}
												{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
												<a href="{{ url('admin/course/manage/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a>
											</div>
										{{ Form::close() }}
								  </div>
								</div>

							</div>
							<!-- /.box-body -->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>
@endpush
