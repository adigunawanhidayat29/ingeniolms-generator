@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Discussion Answer' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Discussion Answer List</h3>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
              <table class="table table-hover" id="data">
                <thead>
                  <tr>
                    <th>Discussion</th>
										<th>Body</th>
										<th>Status</th>
										<th>Created At</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
  <script type="text/javascript">
  $(function(){
    $("#data").DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ url("discussion-answer/serverside") }}',
      columns: [
        { data: 'discussion', name: 'discussion' },
				{ data: 'body', name: 'body' },
				{ data: 'status', name: 'status' },
				{ data: 'created_at', name: 'created_at' },
        { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
  </script>
@endpush
