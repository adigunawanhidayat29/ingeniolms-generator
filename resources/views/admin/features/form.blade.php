<div class="form-group">
    <div class="form-line">
        <label for="icon" class="control-label">@lang('back.feature_form.icon_label')</label>
        <input placeholder="@lang('back.feature_form.icon_label')" type="text" class="form-control" id="icon" name="icon" value="{{ isset($feature->icon) ? $feature->icon : ''}}">
        <div class="help-info"><a href="http://zavoloklom.github.io/material-design-iconic-font/icons.html">@lang('back.feature_form.reference_text')</a>. <b>@lang('back.feature_form.example_text'): zmdi zmdi-favorite</b></div>
        {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="title" class="control-label">@lang('back.feature_form.name_label')</label>
        <input placeholder="@lang('back.feature_form.name_label')" type="text" class="form-control" name="title" id="title" value="{{ isset($feature->title) ? $feature->title : ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="description" class="control-label">@lang('back.feature_form.description_label')</label>
        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($feature->description) ? $feature->description : ''}}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">@lang('back.feature_form.status_label')</label>
    @if( isset($feature->status) )
        <div class="form-group">
            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue" {{$feature->status == '1' ? 'checked' : ''}}>
            <label for="statusAktif">@lang('back.feature_form.enabled_radio')</label>
            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue" {{$feature->status == '0' ? 'checked' : ''}}>
            <label for="statusNonaktif">@lang('back.feature_form.disabled_radio')</label>
        </div>
    @else
        <div class="form-group">
            <input name="status" type="radio" id="statusAktif" value="1" class="with-gap radio-col-blue">
            <label for="statusAktif">@lang('back.feature_form.enabled_radio')</label>
            <input name="status" type="radio" id="statusNonaktif" value="0" class="with-gap radio-col-blue">
            <label for="statusNonaktif">@lang('back.feature_form.disabled_radio')</label>
        </div>
    @endif
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

{{-- <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="status" class="control-label">{{ 'Status' }}</label>
        <input class="form-control" name="status" type="text" id="status" value="{{ isset($feature->status) ? $feature->status : ''}}" >
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}

{{-- <div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="icon" class="control-label">{{ 'Icon' }}</label>
        <input class="form-control" name="icon" type="text" id="icon" value="{{ isset($feature->icon) ? $feature->icon : ''}}" >
        {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.feature_form.update_button') : Lang::get('back.feature_form.add_button') }}">
<a href="{{ url('admin/features') }}" class="btn bg-blue-grey">@lang('back.feature_form.back_button')</a>
