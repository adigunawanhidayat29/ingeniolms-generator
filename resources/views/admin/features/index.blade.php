@extends('admin.layouts.app')

@section('title', Lang::get('back.feature.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">

            <center>
                    @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {!! Session::get('success') !!}
                            </div>
                    @elseif(Session::has('error'))
                            <div class="alert alert-error alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {!! Session::get('error') !!}
                            </div>
                    @endif
            </center>
            
            <div class="block-header">
                <h2>@lang('back.feature.header')</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/admin/features/create') }}" class="btn btn-primary">@lang('back.feature.add_button')</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>@lang('back.feature.icon_table')</th>
                                            <th>@lang('back.feature.name_table')</th>
                                            <th>@lang('back.feature.description_table')</th>
                                            <th>@lang('back.feature.status_table')</th>
                                            <th>@lang('back.feature.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($features as $item)
                                            <tr>
                                                {{-- <td>{{ $loop->iteration }}</td> --}}
                                                <td>{{ $item->icon }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>{{ $item->status == 1 ? Lang::get('back.feature.enabled_status') : Lang::get('back.feature.disabled_status') }}</td>
                                                <td>
                                                    {{-- <a href="{{ url('/admin/features/' . $item->id) }}" class="btn btn-info btn-sm">
                                                        <i class="material-icons">info</i><span>Detail</span>
                                                    </a> --}}
                                                    <a href="{{ url('/admin/features/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm">
                                                        <i class="material-icons">edit</i><span>@lang('back.feature.edit_button')</span>
                                                    </a>
                                                    <form method="POST" action="{{ url('/admin/features' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="material-icons">delete</i><span>@lang('back.feature.delete_button')</span>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            {{-- <a href="{{ url('/admin/features/create') }}" class="btn btn-primary btn-sm" title="Add New Feature">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            <br>
                            <form method="GET" action="{{ url('/admin/features') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="search"
                                                    placeholder="Search..." value="{{ request('search') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <button type="submit" class="btn btn-primary btn-sm m-l-15 waves-effect"><i
                                                class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>Title</th><th>Description</th><th>Status</th><th>Icon</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($features as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->title }}</td><td>{{ $item->description }}</td><td>{{ $item->status }}</td><td>{{ $item->icon }}</td>
                                            <td>
                                                <a href="{{ url('/admin/features/' . $item->id) }}" title="View Feature"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                <a href="{{ url('/admin/features/' . $item->id . '/edit') }}" title="Edit Feature"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                <form method="POST" action="{{ url('/admin/features' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Feature" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $features->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div> --}}

                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

        </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $(function () {        
            $("#data").DataTable();
        });
    </script>
@endpush