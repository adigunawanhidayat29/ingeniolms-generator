@extends('admin.layouts.app')

@section('title', Lang::get('back.vcon_setting.title'))

@push('style')
	<link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
	<style>
		.config-notify {
			display: flex; 
			align-items: center; 
			justify-content: center; 
			min-height: 200px;
		}

		.config-notify .wrap .notify-title {
			font-size: 24px; 
			text-align: center;
		}

		.config-notify .wrap .notify-description,
		.config-notify .wrap .notify-switch {
			text-align: center;
		}
	</style>
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">

			<center>
					@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
							</div>
					@elseif(Session::has('error'))
							<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
							</div>
					@endif
			</center>
			
			<div class="block-header">
				<h2>@lang('back.vcon_setting.header')</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					{{-- <div class="card">
						<div class="body">
							<div class="config-notify">
								<div class="wrap">
									<p class="notify-title">VCON nonaktif</p>
									<p class="notify-description">Aktifkan VCON dengan klik tombol di bawah ini</p>
									<div class="switch notify-switch">
										<label><input type="checkbox"><span class="lever"></span></label>
									</div>
									<div id="buttonFrame" class="notify-switch">
										<button id="activate" class="btn btn-primary">Aktifkan</button>
										<button id="nonActive" class="btn btn-danger">Nonaktifkan</button>
									</div>
								</div>
							</div>
						</div>
					</div> --}}

					<div id="vconConfiguration" class="card">
						<div class="body">
							{{-- <div style="display: flex; align-items: center; justify-content: space-between; margin-bottom: 2rem;">
								<a href="#" data-target="#modalAddServerVcon" data-toggle="modal" class="btn btn-primary">Tambah Server</a>
								<form action="#">
									<div class="switch notify-switch">
										<label><input type="checkbox" checked><span class="lever"></span></label>
									</div>
								</form>
							</div> --}}
							<div style="margin-bottom: 2rem;">
								<a href="#" data-target="#modalAddServerVcon" data-toggle="modal" class="btn btn-primary">@lang('back.vcon_setting.add_button')</a>
							</div>
							@foreach($vcon_setting as $data)
								<table class="table">
									<tr>
										<td style="width:10%">#</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$data->id}}</td>
									</tr>
									<tr>
										<td style="width:10%">@lang('back.vcon_setting.base_url_table')</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$data->base_url}}</td>
									</tr>
									<tr>
										<td style="width:10%">@lang('back.vcon_setting.salt_table')</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$data->salt}}</td>
									</tr>
									<tr>
										<td style="width:10%">@lang('back.vcon_setting.option_table')</td>
										<td style="width:5%">:</td>
										<td style="width:85%">
											<a class="btn btn-info btn-sm" target="_blank" href="/admin/test/bbb/getmeetings/{{$data->id}}"><i class="material-icons">info</i><span>@lang('back.vcon_setting.info_button')</span></a>
											<a class="btn btn-primary btn-sm" href="/admin/vcon_setting/set_default/{{$data->id}}"><i class="material-icons">favorite</i><span>{{$data->status == '1' ? Lang::get('back.vcon_setting.default_button') : Lang::get('back.vcon_setting.set_default_button')}}</span></a>
											<a class="btn btn-warning btn-sm" href="/admin/vcon_setting/{{$data->id}}"><i class="material-icons">edit</i><span>@lang('back.vcon_setting.update_button')</span></a>
											<a onclick="return confirm('Yakin akan menghapus data?')" class="btn btn-danger btn-sm" href="/admin/vcon_setting/delete/{{$data->id}}"><i class="material-icons">delete</i><span>@lang('back.vcon_setting.delete_button')</span></a>
										</td>
									</tr>
								</table>
							@endforeach							
						</div>
					</div>
				</div>
			</div>
			<!-- #END# Task Info -->
		</div>

		<div class="modal fade" id="modalAddServerVcon" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="largeModalLabel">@lang('back.vcon_setting.modal_header')</h4>
					</div>
					<form action="{{url(\Request::route()->getPrefix() . '/vcon_setting/store')}}" method="post" enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="modal-body">
							<div class="form-group">
								<div class="form-line">
									<label for="">@lang('back.vcon_setting.base_url_label')</label>
									<input type="text" class="form-control" name="base_url" placeholder="@lang('back.vcon_setting.base_url_label')" />
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="">@lang('back.vcon_setting.salt_label')</label>
									<input type="text" class="form-control" name="salt" placeholder="@lang('back.vcon_setting.salt_label')" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" id="editId" value="">
							<input type="hidden" name="action" value="update">
							<button type="submit" name="submit" class="btn btn-primary waves-effect">@lang('back.vcon_setting.submit_button')</button>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">@lang('back.vcon_setting.close_button')</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('script')
	<script>
		$('#nonActive').hide();
		$('#vconConfiguration').show();

		$('#activate').click(function() {
			$(this).hide();
			$('#nonActive').show();
			$('#vconConfiguration').show();
		})
		
		$('#nonActive').click(function() {
			$(this).hide();
			$('#activate').show();
			$('#vconConfiguration').hide();
		})
	</script>
	<script>
		$("#type").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#value").attr('type', 'file')
			} else {
				$("#value").attr('type', 'text')
			}
		})

		$("#editType").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#editValue").attr('type', 'file')
			} else {
				$("#editValue").attr('type', 'text')
			}
		})

		if($("#registration_self").is(":checked")) {
			$("#showSocialMedia").show();
		} else {
			$("#showSocialMedia").hide();
		}

		$("#registration_self").change(function() {
			if($("#registration_self").is(":checked")) {
				$("#showSocialMedia").show();
			} else {
				$("#showSocialMedia").hide();
			}
		})

	</script>

	<script>
		$(function () {
			$(document).on('click', '#editSetting', function (e) {
				e.preventDefault();
				var data_type = $(this).attr('data-type');
				var data_value = $(this).attr('data-value');
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');

				$("#editId").val(data_id)
				$("#editName").val(data_name)
				$("#editType").val(data_type)
				$("#editValue").val(data_value)

				$("#modalFormEdit").modal('show');
			});
		});
	</script>
@endpush