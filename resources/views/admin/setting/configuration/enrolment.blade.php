@extends('admin.layouts.app')

@section('title', Lang::get('back.enrol_setting.title'))

@push('style')
	<link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
	<style>
		.config-notify {
			display: flex; 
			align-items: center; 
			justify-content: center; 
			min-height: 200px;
		}

		.config-notify .wrap .notify-title {
			font-size: 24px; 
			text-align: center;
		}

		.config-notify .wrap .notify-description,
		.config-notify .wrap .notify-switch {
			text-align: center;
		}
	</style>
@endpush

@section('content')
	@php
	$enrollmentSelected = [];
	if (isset(Setting('enrollment')->value)) {
		$enrollmentSelected = explode(',', Setting('enrollment')->value);
	}
	@endphp

	<section class="content">
		<div class="container-fluid">

			<center>
					@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
							</div>
					@elseif(Session::has('error'))
							<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
							</div>
					@endif
			</center>
			
			<div class="block-header">
				<h2>@lang('back.enrol_setting.header')</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					{{-- <div class="card">
						<div class="body">
							<div class="config-notify">
								<div class="wrap">
									<p class="notify-title">Enrolment Nonaktif</p>
									<p class="notify-description">Aktifkan / nonaktifkan konfigurasi enrolment dengan klik tombol di bawah ini</p>
									<div id="buttonFrame" class="notify-switch">
										<button id="customization" class="btn btn-primary">Aktifkan</button>
										<button id="backToDefault" class="btn btn-danger">Nonaktifkan</button>
									</div>
								</div>
							</div>
						</div>
					</div> --}}

					<div id="customConfiguration" class="row" style="display: flex;">
						<div class="col-md-8" style="margin-left: auto; margin-right: auto;">
							<div class="card">
								<div class="header">
									<h2>@lang('back.enrol_setting.enrol_config_header')</h2>
								</div>
								<div class="body">
									<form action="/admin/setting/enrollment" method="POST">
										@csrf
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.enrol_setting.manual_enrol_label')
												<small>* Enrol manual adalah enrollment yang diberikan oleh admin secara manual</small>
											</h2>
											<div class="switch">
												<label><input name="enrollment[]" value="manual" type="checkbox" {{ isset(Setting('enrollment')->value) && in_array("manual", $enrollmentSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.enrol_setting.self_enrol_label')
												<small>* Self enrolment adalah enrollment yang dilakukan mandiri oleh peserta</small>
											</h2>
											<div class="switch">
												<label><input type="checkbox" value="self" name="enrollment[]" {{ isset(Setting('enrollment')->value) && in_array("self", $enrollmentSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.enrol_setting.cohort_enrol_label')
												<small>* Enrol melalui cohort adalah enrollment yang dilakukan oleh admin dengan mengelompokan beberapa peserta dalam suatu grub atau kelompok</small>
											</h2>
											<div class="switch">
												<label><input type="checkbox" value="cohort" name="enrollment[]" {{ isset(Setting('enrollment')->value) && in_array("cohort", $enrollmentSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<input type="submit" class="btn btn-raised btn-primary" value="@lang('back.enrol_setting.submit_button')">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>
@endsection

@push('script')
	<script>
		$('#backToDefault').hide();
		$('#customConfiguration').show();
	
		$('#customization').click(function() {
			$(this).hide();
			$('#backToDefault').show();
			$('#customConfiguration').show();
		})
		
		$('#backToDefault').click(function() {
			$(this).hide();
			$('#customization').show();
			$('#customConfiguration').hide();
		})
	</script>
	<script>
		$("#type").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#value").attr('type', 'file')
			} else {
				$("#value").attr('type', 'text')
			}
		})

		$("#editType").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#editValue").attr('type', 'file')
			} else {
				$("#editValue").attr('type', 'text')
			}
		})

		if($("#registration_self").is(":checked")) {
			$("#showSocialMedia").show();
		} else {
			$("#showSocialMedia").hide();
		}

		$("#registration_self").change(function() {
			if($("#registration_self").is(":checked")) {
				$("#showSocialMedia").show();
			} else {
				$("#showSocialMedia").hide();
			}
		})

	</script>

	<script>
		$(function () {
			$(document).on('click', '#editSetting', function (e) {
				e.preventDefault();
				var data_type = $(this).attr('data-type');
				var data_value = $(this).attr('data-value');
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');

				$("#editId").val(data_id)
				$("#editName").val(data_name)
				$("#editType").val(data_type)
				$("#editValue").val(data_value)

				$("#modalFormEdit").modal('show');
			});
		});
	</script>
@endpush