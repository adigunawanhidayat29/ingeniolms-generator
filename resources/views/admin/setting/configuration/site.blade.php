@extends('admin.layouts.app')

@section('title', Lang::get('back.site_setting.title'))

@push('style')
	<link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

	<style>
		.config-notify {
			display: flex;
			align-items: center;
			justify-content: center;
			min-height: 200px;
		}

		.config-notify .wrap .notify-title {
			font-size: 24px;
			text-align: center;
		}

		.config-notify .wrap .notify-description,
		.config-notify .wrap .notify-switch {
			text-align: center;
		}
	</style>

	<!-- include summernote css/js -->
	<link href="/summernote/summernote.min.css" rel="stylesheet">
@endpush

@section('content')
	<section class="content">
		<div class="container-fluid">

			<center>
					@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
							</div>
					@elseif(Session::has('error'))
							<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
							</div>
					@endif
			</center>

			<div class="block-header">
				<h2>@lang('back.site_setting.header')</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<form class="form-horizontal" action="/admin/setting/site" method="POST" enctype="multipart/form-data">
								{{csrf_field()}}

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.logo_label') (100 x 50 px)</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												@if(isset(Setting('icon')->value))
													<img height=50 src={{asset_url(Setting('icon')->value)}}>
												@endif
												<input class="form-control" type="file" name="icon" id="">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.icon_label') (60 x 60 px)</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												@if(isset(Setting('favicon')->value))
													<img height=50 src={{asset_url(Setting('favicon')->value)}}>
												@endif
												<input class="form-control" type="file" name="favicon" id="">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.name_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="title" value="{{isset(Setting('title')->value) ? Setting('title')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.tagline_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="tagline" value="{{isset(Setting('tagline')->value) ? Setting('tagline')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.column_list_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="catalog_column" type="radio" id="templateKolomDefault" value="4" class="with-gap radio-col-blue" checked>
											<label for="templateKolomDefault">@lang('back.site_setting.default_radio')</label>
											<input name="catalog_column" type="radio" id="templateKolomTiga" value="3" class="with-gap radio-col-blue">
											<label for="templateKolomTiga">@lang('back.site_setting.three_column_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.max_list_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="number" class="form-control" name="max_catalog_column" value="{{isset(Setting('max_catalog_column')->value) ? Setting('max_catalog_column')->value : '3'}}">
												<div class="help-info">@lang('back.site_setting.default_text'): 3</div>
											</div>
										</div>
									</div>
								</div>

								{{-- <div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Deskripsi Site</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="description" value="{{isset(Setting('description')->value) ? Setting('description')->value : ''}}">
											</div>
										</div>
									</div>
								</div> --}}

								{{-- <div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Tagline Site</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="tagline" value="{{isset(Setting('tagline')->value) ? Setting('tagline')->value : ''}}">
											</div>
										</div>
									</div>
								</div> --}}

								{{-- <div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Atur Warna Site</label>
									</div>
									<div class="col-md-7 padding-0">
										<div class="row clearfix" style="width: 100%;">
											<div class="col-md-6" style="margin-bottom: 0;">
												<b>Color Primary</b>
												<div class="input-group colorpicker" style="margin-bottom: 0;">
													<div class="form-line">
														<input type="text" class="form-control" value="#00AABB">
													</div>
													<span class="input-group-addon">
														<i></i>
													</span>
												</div>
											</div>
											<div class="col-md-6" style="margin-bottom: 0;">
												<b>Color Secondary</b>
												<div class="input-group colorpicker" style="margin-bottom: 0;">
													<div class="form-line">
														<input type="text" class="form-control" value="rgba(0,0,0,0.7)">
													</div>
													<span class="input-group-addon">
														<i></i>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div> --}}

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.site_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="row clearfix">
											<div class="col-md-6" style="margin-bottom: 0;">
												<div class="form-group form-float">
													<div class="form-line">
														<input type="text" class="form-control" name="color_primary" value="{{isset(Setting('color_primary')->value) ? Setting('color_primary')->value : ''}}">
														<label class="form-label">@lang('back.site_setting.primary_color_label')</label>
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
											<div class="col-md-6" style="margin-bottom: 0;">
												<div class="form-group form-float">
													<div class="form-line">
														<input type="text" class="form-control" name="color_secondary" value="{{isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : ''}}">
														<label class="form-label">@lang('back.site_setting.secondary_color_label')</label>
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.font_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="row clearfix">
											<div class="col-md-6" style="margin-bottom: 0;">
												<div class="form-group form-float">
													<div class="form-line">
														<input type="text" class="form-control" name="font_color_primary" value="{{isset(Setting('font_color_primary')->value) ? Setting('font_color_primary')->value : ''}}">
														<label class="form-label">@lang('back.site_setting.primary_font_color_label')</label>
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
											<div class="col-md-6" style="margin-bottom: 0;">
												<div class="form-group form-float">
													<div class="form-line">
														<input type="text" class="form-control" name="font_color_secondary" value="{{isset(Setting('font_color_secondary')->value) ? Setting('font_color_secondary')->value : ''}}">
														<label class="form-label">@lang('back.site_setting.secondary_font_color_label')</label>
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.navbar_background_label')</label>
									</div>
									<div class="col-md-7">
										{{-- <div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="navbar_background_color" value="{{isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div> --}}
										<div class="form-group" style="padding-top: .5rem;">
											<input name="navbar_background_color" value="{{isset(Setting('color_primary')->value) ? Setting('color_primary')->value : ''}}" type="radio" id="navbarPrimer" class="with-gap radio-col-blue" {{Setting('color_primary')->value == Setting('navbar_background_color')->value ? 'checked' : '' }}>
											<label for="navbarPrimer">@lang('back.site_setting.primary_color_radio')</label>
											<input name="navbar_background_color" value="{{isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : ''}}" type="radio" id="navbarSekunder" class="with-gap radio-col-blue" {{Setting('color_secondary')->value == Setting('navbar_background_color')->value ? 'checked' : '' }}>
											<label for="navbarSekunder">@lang('back.site_setting.secondary_color_radio')</label>
											<input name="navbar_background_color" value="lainnya" type="radio" id="navbarLainnya" class="with-gap radio-col-blue" {{Setting('color_primary')->value != Setting('navbar_background_color')->value && Setting('color_secondary')->value != Setting('navbar_background_color')->value ? 'checked' : '' }}>
											<label for="navbarLainnya">@lang('back.site_setting.other_color_radio')</label>
										</div>
									</div>
								</div>
								<div class="row clearfix" id="navbarBackgroundForm" style="{{Setting('color_primary')->value != Setting('navbar_background_color')->value && Setting('color_secondary')->value != Setting('navbar_background_color')->value ? '' : 'display: none;' }}">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.set_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="navbar_background_color_another" class="form-control" placeholder="@lang('back.site_setting.default_text'): #4ca3d9" value="{{isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.navbar_link_background_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="navbar_background_hover" value="{{isset(Setting('color_primary')->value) ? Setting('color_primary')->value : ''}}" type="radio" id="navbarLinkPrimer" class="with-gap radio-col-blue" {{Setting('color_primary')->value == Setting('navbar_background_hover')->value ? 'checked' : '' }}>
											<label for="navbarLinkPrimer">@lang('back.site_setting.primary_color_radio')</label>
											<input name="navbar_background_hover" value="{{isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : ''}}" type="radio" id="navbarLinkSekunder" class="with-gap radio-col-blue" {{Setting('color_secondary')->value == Setting('navbar_background_hover')->value ? 'checked' : '' }}>
											<label for="navbarLinkSekunder">@lang('back.site_setting.secondary_color_radio')</label>
											<input name="navbar_background_hover" value="lainnya" type="radio" id="navbarLinkLainnya" class="with-gap radio-col-blue" {{Setting('color_primary')->value != Setting('navbar_background_hover')->value && Setting('color_secondary')->value != Setting('navbar_background_hover')->value ? 'checked' : '' }}>
											<label for="navbarLinkLainnya">@lang('back.site_setting.other_color_radio')</label>
										</div>
									</div>
								</div>
								<div class="row clearfix" id="navbarLinkBackgroundForm" style="{{Setting('color_primary')->value != Setting('navbar_background_hover')->value && Setting('color_secondary')->value != Setting('navbar_background_hover')->value ? '' : 'display: none;' }}">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.set_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="navbar_background_hover_another" class="form-control" placeholder="@lang('back.site_setting.default_text'): #4ca3d9" value="{{isset(Setting('navbar_background_hover')->value) ? Setting('navbar_background_hover')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.navbar_font_color_label')</label>
									</div>
									<div class="col-md-7">
										{{-- <div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="navbar_font_color" value="{{isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div> --}}
										<div class="form-group" style="padding-top: .5rem;">
											<input name="navbar_font_color" type="radio" id="navbarFontDark" class="with-gap radio-col-blue" value="#333333" {{Setting('navbar_font_color')->value == '#333333' ? 'checked' : '' }}>
											<label for="navbarFontDark">@lang('back.site_setting.dark_color_radio')</label>
											<input name="navbar_font_color" type="radio" id="navbarFontLight" class="with-gap radio-col-blue" value="#ffffff" {{Setting('navbar_font_color')->value == '#ffffff' ? 'checked' : '' }}>
											<label for="navbarFontLight">@lang('back.site_setting.light_color_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.navbar_font_hover_color_label')</label>
									</div>
									<div class="col-md-7">
										{{-- <div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="navbar_font_color_hover" value="{{isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div> --}}
										<div class="form-group" style="padding-top: .5rem;">
											<input name="navbar_font_color_hover" type="radio" id="navbarFontHoverDark" class="with-gap radio-col-blue" value="#333333" {{Setting('navbar_font_color_hover')->value == '#333333' ? 'checked' : '' }}>
											<label for="navbarFontHoverDark">@lang('back.site_setting.dark_color_radio')</label>
											<input name="navbar_font_color_hover" type="radio" id="navbarFontHoverLight" class="with-gap radio-col-blue" value="#ffffff" {{Setting('navbar_font_color_hover')->value == '#ffffff' ? 'checked' : '' }}>
											<label for="navbarFontHoverLight">@lang('back.site_setting.light_color_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.horizontal_tabs_font_color_label')</label>
									</div>
									<div class="col-md-7">
										{{-- <div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="tabs_font_color" value="{{isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div> --}}
										<div class="form-group" style="padding-top: .5rem;">
											<input name="tabs_font_color" type="radio" id="navtabsFontDark" class="with-gap radio-col-blue" value="#333333" {{Setting('tabs_font_color')->value == '#333333' ? 'checked' : '' }}>
											<label for="navtabsFontDark">@lang('back.site_setting.dark_color_radio')</label>
											<input name="tabs_font_color" type="radio" id="navtabsFontLight" class="with-gap radio-col-blue" value="#ffffff" {{Setting('tabs_font_color')->value == '#ffffff' ? 'checked' : '' }}>
											<label for="navtabsFontLight">@lang('back.site_setting.light_color_radio')</label>
										</div>
									</div>
								</div>

								<hr>

								<div class="row clearfix">
									<div class="col-md-7 col-md-offset-4">
										<h3 style="margin-left: -15px;">@lang('back.site_setting.banner_header')</h3>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.banner_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="banner" type="radio" id="bannerAktif" value="true" class="with-gap radio-col-blue" {{Setting('banner')->value == 'true' ? 'checked' : '' }}>
											<label for="bannerAktif">@lang('back.site_setting.enabled_radio')</label>
											<input name="banner" type="radio" id="bannerNonaktif" value="false" class="with-gap radio-col-blue" {{Setting('banner')->value == 'false' ? 'checked' : '' }}>
											<label for="bannerNonaktif">@lang('back.site_setting.disabled_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix" id="bannerStyle" style="{{Setting('banner')->value == 'true' ? '' : 'display: none;' }}">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.banner_style_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="banner_style" type="radio" id="bannerStyleDefault" value="default" class="with-gap radio-col-blue" {{Setting('banner_style')->value == 'default' ? 'checked' : '' }}>
											<label for="bannerStyleDefault">@lang('back.site_setting.default_radio')</label>
											<input name="banner_style" type="radio" id="bannerStyleSlider" value="slider" class="with-gap radio-col-blue" {{Setting('banner_style')->value == 'slider' ? 'checked' : '' }}>
											<label for="bannerStyleSlider">@lang('back.site_setting.slider_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.banner_config_label')</label>
									</div>
									@if( Setting('banner_style')->value == 'default' )
										<div class="col-md-7">
											<a href="{{Request::route()->getPrefix() . '/banner'}}" class="btn bg-grey btn-sm" style="margin-left: -15px;">
												<span>@lang('back.site_setting.set_image_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
											</a>
										</div>
									@elseif( Setting('banner_style')->value == 'slider' )
										<div class="col-md-7">
											<a href="{{Request::route()->getPrefix() . '/sliders'}}" class="btn bg-grey btn-sm" style="margin-left: -15px;">
												<span>@lang('back.site_setting.set_slider_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
											</a>
										</div>
									@endif
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.banner_gradation_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="header_gradient" type="radio" id="gradasiDefault" value="default" class="with-gap radio-col-blue" {{Setting('header_gradient')->value == 'default' ? 'checked' : '' }}>
											<label for="gradasiDefault">@lang('back.site_setting.default_radio')</label>
											<input name="header_gradient" type="radio" id="gradasiCustom" value="custom" class="with-gap radio-col-blue" {{Setting('header_gradient')->value == 'custom' ? 'checked' : '' }}>
											<label for="gradasiCustom">@lang('back.site_setting.custom_radio')</label>
											<input name="header_gradient" type="radio" id="gradasiNonaktif" value="false" class="with-gap radio-col-blue" {{Setting('header_gradient')->value == 'false' ? 'checked' : '' }}>
											<label for="gradasiNonaktif">@lang('back.site_setting.disabled_radio')</label>
										</div>
										<div class="row clearfix" id="gradasiForm" style="{{Setting('header_gradient')->value == 'custom' ? '' : 'display: none;' }}">
											<div class="col-md-5" style="margin-bottom: 0;">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="form-control" name="custom_gradient_primary" placeholder="Primary gradient color" value="{{isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : ''}}">
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
											<div class="col-md-5" style="margin-bottom: 0;">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="form-control" name="custom_gradient_secondary" placeholder="Secondary gradient color" value="{{isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : ''}}">
														<div class="help-info">@lang('back.site_setting.example_color_text')</div>
													</div>
												</div>
											</div>
											<div class="col-md-2" style="margin-bottom: 0;">
												<div class="form-group">
													<div class="form-line">
														<input type="number" class="form-control" name="custom_gradient_rotate" placeholder="Rotate" value="{{isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : ''}}">
														<div class="help-info">@lang('back.site_setting.default_text'): 0</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.banner_font_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="banner_font_color" type="radio" id="bannerFontDark" class="with-gap radio-col-blue" value="#333333" {{Setting('banner_font_color')->value == '#333333' ? 'checked' : '' }}>
											<label for="bannerFontDark">@lang('back.site_setting.dark_color_radio')</label>
											<input name="banner_font_color" type="radio" id="bannerFontLight" class="with-gap radio-col-blue" value="#ffffff" {{Setting('banner_font_color')->value == '#ffffff' ? 'checked' : '' }}>
											<label for="bannerFontLight">@lang('back.site_setting.light_color_radio')</label>
										</div>
									</div>
								</div>

								<hr>

								<div class="row clearfix">
									<div class="col-md-7 col-md-offset-4">
										<h3 style="margin-left: -15px;">@lang('back.site_setting.feature_header')</h3>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.feature_background_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="feature_background_color" value="{{isset(Setting('color_primary')->value) ? Setting('color_primary')->value : ''}}" type="radio" id="FeatureBackgroundPrimer" class="with-gap radio-col-blue" {{Setting('color_primary')->value == Setting('feature_background_color')->value ? 'checked' : '' }}>
											<label for="FeatureBackgroundPrimer">@lang('back.site_setting.primary_color_radio')</label>
											<input name="feature_background_color" value="{{isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : ''}}" type="radio" id="FeatureBackgroundSekunder" class="with-gap radio-col-blue" {{Setting('color_secondary')->value == Setting('feature_background_color')->value ? 'checked' : '' }}>
											<label for="FeatureBackgroundSekunder">@lang('back.site_setting.secondary_color_radio')</label>
											<input name="feature_background_color" value="lainnya" type="radio" id="FeatureBackgroundLainnya" class="with-gap radio-col-blue" {{Setting('color_primary')->value != Setting('feature_background_color')->value && Setting('color_secondary')->value != Setting('feature_background_color')->value ? 'checked' : '' }}>
											<label for="FeatureBackgroundLainnya">@lang('back.site_setting.other_color_radio')</label>
										</div>
									</div>
								</div>

								<div class="row clearfix" id="FeatureBackgroundForm" style="{{Setting('color_primary')->value != Setting('feature_background_color')->value && Setting('color_secondary')->value != Setting('feature_background_color')->value ? '' : 'display: none;' }}">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.set_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="feature_background_color_another" class="form-control" placeholder="@lang('back.site_setting.default_text'): #4ca3d9" value="{{isset(Setting('feature_background_color')->value) ? Setting('feature_background_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix"">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.feature_text_color_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="feature_text_color" class="form-control" placeholder="@lang('back.site_setting.default_text'): #4ca3d9" value="{{isset(Setting('feature_text_color')->value) ? Setting('feature_text_color')->value : ''}}">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div>
									</div>
								</div>

								@if(isset(Setting('features_thumbnail')->value))
									<div class="row clearfix">
										<div class="col-md-7 col-md-offset-4">
											<img height="200px" src="{{ asset_url(Setting('features_thumbnail')->value) }}" style="margin-left: -15px;">
										</div>
									</div>
								@endif

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.feature_thumbnail_label') {{ Setting('title')->value }}</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input class="form-control" type="file" name="features_thumbnail">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.feature_label') {{ Setting('title')->value }}</label>
									</div>
									<div class="col-md-7">
										<a href="{{Request::route()->getPrefix() . '/features'}}" class="btn bg-grey btn-sm" style="margin-left: -15px;">
											<span>@lang('back.site_setting.set_content_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
										</a>
									</div>
								</div>

								<hr>

								<div class="row clearfix">
									<div class="col-md-7 col-md-offset-4">
										<h3 style="margin-left: -15px;">@lang('back.site_setting.video_header')</h3>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.video_label') {{ Setting('title')->value }}</label>
									</div>
									<div class="col-md-7">
										<a href="{{Request::route()->getPrefix() . '/videos'}}" class="btn bg-grey btn-sm" style="margin-left: -15px;">
											<span>@lang('back.site_setting.set_video_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
										</a>
									</div>
								</div>

								{{-- <div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Warna Latar Belakang Section Landing Page</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="landingPageSectionBackground" type="radio" id="landingPagePrimer" class="with-gap radio-col-blue" checked>
											<label for="landingPagePrimer">Warna Primer</label>
											<input name="landingPageSectionBackground" type="radio" id="landingPageSekunder" class="with-gap radio-col-blue">
											<label for="landingPageSekunder">Warna Sekunder</label>
											<input name="landingPageSectionBackground" type="radio" id="landingPageLainnya" class="with-gap radio-col-blue">
											<label for="landingPageLainnya">Warna Lainnya</label>
										</div>
									</div>
								</div>
								<div class="row clearfix" id="landingPageSectionBackgroundForm" style="display: none;">
									<div class="col-md-4 form-control-label">
										<label>Atur Warna</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="" placeholder="@lang('back.site_setting.default_text'): #4ca3d9" value="">
												<div class="help-info">@lang('back.site_setting.example_color_text')</div>
											</div>
										</div>
									</div>
								</div> --}}

								{{-- <div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Format Menu</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<input name="group5" id="catalog" type="radio" class="with-gap radio-col-blue" checked>
											<label for="catalog">Gunakan Katalog</label>
										</div>
										<div class="form-group">
											<input name="group5" id="category" type="radio" class="with-gap radio-col-blue">
											<label for="category">Gunakan Kategori</label>
										</div>
									</div>
								</div> --}}

								<hr>

								<div class="row clearfix">
									<div class="col-md-7 col-md-offset-4">
										<h3 style="margin-left: -15px;">@lang('back.site_setting.footer_header')</h3>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.footer_background_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_background_color" value="{{isset(Setting('footer_background_color')->value) ? Setting('footer_background_color')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.footer_text_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_text_color" value="{{isset(Setting('footer_text_color')->value) ? Setting('footer_text_color')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.footer_address_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_address" value="{{isset(Setting('footer_address')->value) ? Setting('footer_address')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.footer_email_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_email" value="{{isset(Setting('footer_email')->value) ? Setting('footer_email')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.footer_phone_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_phone" value="{{isset(Setting('footer_phone')->value) ? Setting('footer_phone')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Instagram</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_instagram" value="{{isset(Setting('footer_instagram')->value) ? Setting('footer_instagram')->value : ''}}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Facebook</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_facebook" value="{{isset(Setting('footer_facebook')->value) ? Setting('footer_facebook')->value : ''}}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Twitter</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_twitter" value="{{isset(Setting('footer_twitter')->value) ? Setting('footer_twitter')->value : ''}}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>Youtube</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_youtube" value="{{isset(Setting('footer_youtube')->value) ? Setting('footer_youtube')->value : ''}}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>LinkedIn</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="footer_linkedin" value="{{isset(Setting('footer_linkedin')->value) ? Setting('footer_linkedin')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.terms_condition_label')</label>
									</div>
									<div class="col-md-7">
										<a href="#" class="btn bg-grey btn-sm" data-toggle="modal" data-target="#syaratKetentuan" style="margin-left: -15px;">
											<span>@lang('back.site_setting.set_content_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
										</a>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.privacy_policy_label')</label>
									</div>
									<div class="col-md-7">
										<a href="#" class="btn bg-grey btn-sm" data-toggle="modal" data-target="#kebijakanPrivasi" style="margin-left: -15px;">
											<span>@lang('back.site_setting.set_content_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
										</a>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.contact_label')</label>
									</div>
									<div class="col-md-7">
										<a href="#" class="btn bg-grey btn-sm" data-toggle="modal" data-target="#hubungiKami" style="margin-left: -15px;">
											<span>@lang('back.site_setting.set_content_button')</span><i class="material-icons" style="margin-left: 1rem;">arrow_forward</i>
										</a>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.apps_download_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group" style="padding-top: .5rem;">
											<input name="footer_app_link" type="radio" id="aplikasiAktif" value="true" class="with-gap radio-col-blue" {{Setting('footer_app_link')->value == 'true' ? 'checked' : '' }}>
											<label for="aplikasiAktif">@lang('back.site_setting.enabled_radio')</label>
											<input name="footer_app_link" type="radio" id="aplikasiNonaktif" value="false" class="with-gap radio-col-blue" {{Setting('footer_app_link')->value == 'false' ? 'checked' : '' }}>
											<label for="aplikasiNonaktif">@lang('back.site_setting.disabled_radio')</label>
										</div>
									</div>
								</div>
								<div id="aplikasiForm" style="{{Setting('footer_app_link')->value == 'true' ? '' : 'display: none;' }}">
									<div class="row clearfix" style="">
										<div class="col-md-4 form-control-label">
											<label>@lang('back.site_setting.app_store_label')</label>
										</div>
										<div class="col-md-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" name="app_store_link" class="form-control" placeholder="" value="{{isset(Setting('app_store_link')->value) ? Setting('app_store_link')->value : ''}}">
													{{-- <div class="help-info">@lang('back.site_setting.example_color_text')</div> --}}
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix" style="">
										<div class="col-md-4 form-control-label">
											<label>@lang('back.site_setting.google_play_label')</label>
										</div>
										<div class="col-md-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" name="google_play_link" class="form-control" placeholder="" value="{{isset(Setting('google_play_link')->value) ? Setting('google_play_link')->value : ''}}">
													{{-- <div class="help-info">@lang('back.site_setting.example_color_text')</div> --}}
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-4 form-control-label">
										<label>@lang('back.site_setting.copyright_label')</label>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control" name="copyright" value="{{isset(Setting('copyright')->value) ? Setting('copyright')->value : ''}}">
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-offset-4">
										<button type="submit" class="btn btn-primary m-t-15 waves-effect">@lang('back.site_setting.submit_button')</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>


	<div class="modal fade in" id="syaratKetentuan" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">@lang('back.site_setting.terms_condition_modal')</h4>
				</div>
				<form action="/admin/setting/site" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_title_label')</label>
								<input type="text" class="form-control" name="tnc_title" value="{{isset(Setting('tnc_title')->value) ? Setting('tnc_title')->value : 'Syarat & Ketentuan'}}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_description_label')</label>
								<textarea name="tnc_value" cols="30" rows="5" class="form-control summernote">{{isset(Setting('tnc_value')->value) ? Setting('tnc_value')->value : 'isi Syarat & Ketentuan'}}</textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary waves-effect">@lang('back.site_setting.submit_button')</button>
						<button type="button" class="btn bg-grey waves-effect" data-dismiss="modal">@lang('back.site_setting.close_button')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade in" id="kebijakanPrivasi" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">@lang('back.site_setting.privacy_policy_modal')</h4>
				</div>
				<form action="/admin/setting/site" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_title_label')</label>
								<input type="text" class="form-control" name="privacy_policy_title" value="{{isset(Setting('privacy_policy_title')->value) ? Setting('privacy_policy_title')->value : 'Syarat & Ketentuan'}}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_description_label')</label>
								<textarea name="privacy_policy_value" id="" cols="30" rows="5" class="form-control summernote">{{isset(Setting('privacy_policy_value')->value) ? Setting('privacy_policy_value')->value : 'isi kebijakan privasi'}}</textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary waves-effect">@lang('back.site_setting.submit_button')</button>
						<button type="button" class="btn bg-grey waves-effect" data-dismiss="modal">@lang('back.site_setting.close_button')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade in" id="hubungiKami" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">@lang('back.site_setting.contact_modal')</h4>
				</div>
				<form action="/admin/setting/site" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_title_label')</label>
								<input type="text" class="form-control" name="contact_us_title" value="{{isset(Setting('contact_us_title')->value) ? Setting('contact_us_title')->value : 'Syarat & Ketentuan'}}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-line">
								<label>@lang('back.site_setting.modal_description_label')</label>
								<textarea name="contact_us_value" id="" cols="30" rows="10" class="form-control summernote">{{isset(Setting('contact_us_value')->value) ? Setting('contact_us_value')->value : 'isi hubungi kami'}}</textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary waves-effect">@lang('back.site_setting.submit_button')</button>
						<button type="button" class="btn bg-grey waves-effect" data-dismiss="modal">@lang('back.site_setting.close_button')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script src="/summernote/summernote.min.js"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
        height: 300
      });
	});
	</script>
	<script src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script>
		$("#type").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#value").attr('type', 'file')
			} else {
				$("#value").attr('type', 'text')
			}
		})

		$("#editType").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#editValue").attr('type', 'file')
			} else {
				$("#editValue").attr('type', 'text')
			}
		})

		if($("#registration_self").is(":checked")) {
			$("#showSocialMedia").show();
		} else {
			$("#showSocialMedia").hide();
		}

		$("#registration_self").change(function() {
			if($("#registration_self").is(":checked")) {
				$("#showSocialMedia").show();
			} else {
				$("#showSocialMedia").hide();
			}
		})

	</script>

	<script>
		$(function () {
			$(document).on('click', '#editSetting', function (e) {
				e.preventDefault();
				var data_type = $(this).attr('data-type');
				var data_value = $(this).attr('data-value');
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');

				$("#editId").val(data_id)
				$("#editName").val(data_name)
				$("#editType").val(data_type)
				$("#editValue").val(data_value)

				$("#modalFormEdit").modal('show');
			});
		});

		$("input[name='navbar_background_color']").click(function() {
			if ($("#navbarLainnya").is(":checked")) {
				$("#navbarBackgroundForm").fadeIn(600);
			} else {
				$("#navbarBackgroundForm").fadeOut(300);
			}
		});

		$("input[name='navbar_background_hover']").click(function() {
			if ($("#navbarLinkLainnya").is(":checked")) {
				$("#navbarLinkBackgroundForm").fadeIn(600);
			} else {
				$("#navbarLinkBackgroundForm").fadeOut(300);
			}
		});

		$("input[name='feature_background_color']").click(function() {
			if ($("#FeatureBackgroundLainnya").is(":checked")) {
				$("#FeatureBackgroundForm").fadeIn(600);
			} else {
				$("#FeatureBackgroundForm").fadeOut(300);
			}
		});

		$("input[name='banner']").click(function() {
			if ($("#bannerAktif").is(":checked")) {
				$("#bannerStyle").fadeIn(600);
			} else {
				$("#bannerStyle").fadeOut(300);
			}
		});

		$("input[name='header_gradient']").click(function() {
			if ($("#gradasiCustom").is(":checked")) {
				$("#gradasiForm").fadeIn(600);
			} else {
				$("#gradasiForm").fadeOut(300);
			}
		});

		$("input[name='landingPageSectionBackground']").click(function() {
			if ($("#landingPageLainnya").is(":checked")) {
				$("#landingPageSectionBackgroundForm").fadeIn(600);
			} else {
				$("#landingPageSectionBackgroundForm").fadeOut(300);
			}
		});

		$("input[name='footer_app_link']").click(function() {
			if ($("#aplikasiAktif").is(":checked")) {
				$("#aplikasiForm").fadeIn(600);
			} else {
				$("#aplikasiForm").fadeOut(300);
			}
		});
	</script>
@endpush
