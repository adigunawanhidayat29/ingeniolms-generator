@extends('admin.layouts.app')

@section('title', Lang::get('back.auth_setting.title'))

@push('style')
	<link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
	<style>
		.config-notify {
			display: flex; 
			align-items: center; 
			justify-content: center; 
			min-height: 200px;
		}

		.config-notify .wrap .notify-title {
			font-size: 24px; 
			text-align: center;
		}

		.config-notify .wrap .notify-description,
		.config-notify .wrap .notify-switch {
			text-align: center;
		}
	</style>
@endpush

@section('content')
	@php
	$registrationSelected = [];
	if (isset(Setting('registration')->value)) {
		$registrationSelected = explode(',', Setting('registration')->value);
	}

	$loginWith = [];
	if (isset(Setting('login_with')->value)) {
		$loginWith = explode(',', Setting('login_with')->value);
	}
	@endphp

	<section class="content">
		<div class="container-fluid">

			<center>
					@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
							</div>
					@elseif(Session::has('error'))
							<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
							</div>
					@endif
			</center>
			
			<div class="block-header">
				<h2>@lang('back.auth_setting.header')</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					{{-- <div class="card">
						<div class="body">
							<div class="config-notify">
								<div class="wrap">
									<p class="notify-title">Autentikasi Kustomisasi</p>
									<p class="notify-description">Klik tombol di bawah ini untuk mengatur autentikasi sesuai kebutuhan anda</p>
									<div id="buttonFrame" class="notify-switch">
										<button id="customization" class="btn btn-primary">Kustomisasi</button>
										<button id="backToDefault" class="btn btn-danger">Kembali ke default</button>
									</div>
								</div>
							</div>
						</div>
					</div> --}}

					<div id="customConfiguration" class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="header">
									<h2>@lang('back.auth_setting.auth_config_header')</h2>
								</div>
								<div class="body">
									<form action="/admin/setting/auth" method="POST">
										@csrf
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.auth_setting.manual_registration_label')
												<small>* Registrasi manual adalah peserta didaftarkan secara manual oleh admin atau petugas</small>
											</h2>
											<div class="switch">
												<label><input name="registration[]" value="manual" type="checkbox" {{ isset(Setting('registration')->value) && in_array("manual", $registrationSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.auth_setting.self_registration_label')
												<small>* Self registration adalah peserta mendaftar secara mandiri bisa menggunakan email atau sosial media seperti facebook atau google</small>
											</h2>
											<div class="switch">
												<label><input name="registration[]" type="checkbox" value="self" {{ isset(Setting('registration')->value) && in_array("self", $registrationSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<div class="form-group">
											<h2 class="card-inside-title">
												Verifikasi Register
												<small>* Verifikasi register adalah untuk memvalidasi user supaya melakukan verifikasi emaiil atau tidak</small>
											</h2>
											<div class="switch">
												<label><input name="user_verify" value="on" type="checkbox" {{ isset(Setting('user_verify')->value) && Setting('user_verify')->value == '1' ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										{{-- <div class="form-group">
											<h2 class="card-inside-title">
												Registrasi Pihak Ketiga
												<small>* Registrasi pihak ketiga adalah blablabla</small>
											</h2>
											<div class="switch">
												<label><input type="checkbox" {{ isset(Setting('registration')->value) && in_array("third", $registrationSelected) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div> --}}
										<input type="submit" class="btn btn-primary btn-raised" value="@lang('back.auth_setting.submit_button')">
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="header">
									<h2>@lang('back.auth_setting.auth_config_advanced_header')</h2>
								</div>
								<div class="body">
									<form action="/admin/setting/auth" method="POST">
										@csrf
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.auth_setting.login_email_label')
												<small>* Login Email adalah login menggunakan email dan password</small>
											</h2>
											<div class="switch">
												<label><input name="login_with[]" value="email" type="checkbox" {{ isset(Setting('login_with')->value) && in_array("email", $loginWith) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.auth_setting.login_facebook_label')
												<small>* Login Facebook adalah masuk dengan menggunakan akun facebook</small>
											</h2>
											<div class="switch">
												<label><input name="login_with[]" value="facebook" type="checkbox" {{ isset(Setting('login_with')->value) && in_array("facebook", $loginWith) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										{{-- <div class="card" style="background: #f9f9f9; border: 1px solid #eee; box-shadow: none;">
											<div class="body">
												<form action="">
													@csrf
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" placeholder="Facebook Client ID" style="background-color: #f9f9f9;">
														</div>
													</div>
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" placeholder="Facebook Client Secret" style="background-color: #f9f9f9;">
														</div>
													</div>
													<button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
												</form>
											</div>
										</div> --}}
										<div class="form-group">
											<h2 class="card-inside-title">
												@lang('back.auth_setting.login_google_label')
												<small>* Login google adalah masuk dengan menggunakan akun google</small>
											</h2>
											<div class="switch">
												<label><input name="login_with[]" value="google" type="checkbox" {{ isset(Setting('login_with')->value) && in_array("google", $loginWith) ? 'checked' : '' }}><span class="lever"></span></label>
											</div>
										</div>
										{{-- <div class="card" style="background: #f9f9f9; border: 1px solid #eee; box-shadow: none;">
											<div class="body">
												<form action="">
													@csrf
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" placeholder="Google Client ID" style="background-color: #f9f9f9;">
														</div>
													</div>
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" placeholder="Google Client Secret" style="background-color: #f9f9f9;">
														</div>
													</div>
													<button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
												</form>
											</div>
										</div> --}}
										<input type="submit" value="@lang('back.auth_setting.submit_button')" class="btn btn-primary btn-raised">
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h2>@lang('back.auth_setting.api_config_header')</h2>
								</div>
								<div class="body">
									<form action="{{url(\Request::route()->getPrefix() . '/setting_auth_key/update')}}" method="post" enctype="multipart/form-data" >
										{{csrf_field()}}
										<div class="form-group">
											<div class="form-line">
												<label for="">@lang('back.auth_setting.google_client_id_label')</label>
												<input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->google_client_id}}" name="google_client_id" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">@lang('back.auth_setting.google_client_secret_label')</label>
												<input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->google_client_secret}}" name="google_client_secret" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">@lang('back.auth_setting.facebook_client_id_label')</label>
												<input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->facebook_client_id}}" name="facebook_client_id" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">@lang('back.auth_setting.facebook_client_secret_label')</label>
												<input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->facebook_client_secret}}" name="facebook_client_secret"  />
											</div>
										</div>
										<input type="hidden" name="action" value="{{!$SettingAuthKey ? 'store' : 'update'}}">
										<button type="submit" name="submit" class="btn btn-primary btn-raised">@lang('back.auth_setting.submit_button')</button>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="header">
									<h2>Mail Konfigurasi</h2>
								</div>
								<div class="body">
									<form action="{{url(\Request::route()->getPrefix() . '/setting_auth_key/mail-setting-update')}}" method="post" enctype="multipart/form-data" >
										{{csrf_field()}}
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Driver</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_DRIVER')->value) ? Setting('MAIL_DRIVER')->value : ''}}" name="MAIL_DRIVER" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Host</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_HOST')->value) ? Setting('MAIL_HOST')->value : ''}}" name="MAIL_HOST" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Port</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_PORT')->value) ? Setting('MAIL_PORT')->value : ''}}" name="MAIL_PORT" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Username</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_USERNAME')->value) ? Setting('MAIL_USERNAME')->value : ''}}" name="MAIL_USERNAME" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Password</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_PASSWORD')->value) ? Setting('MAIL_PASSWORD')->value : ''}}" name="MAIL_PASSWORD" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Encryption</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_ENCRYPTION')->value) ? Setting('MAIL_ENCRYPTION')->value : ''}}" name="MAIL_ENCRYPTION" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL Sender</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_SENDER')->value) ? Setting('MAIL_SENDER')->value : ''}}" name="MAIL_SENDER" />
											</div>
										</div>
										<div class="form-group">
											<div class="form-line">
												<label for="">MAIL From</label>
												<input type="text" class="form-control" value="{{isset(Setting('MAIL_FROM')->value) ? Setting('MAIL_FROM')->value : ''}}" name="MAIL_FROM" />
											</div>
										</div>
										<button type="submit" name="submit" class="btn btn-primary btn-raised">@lang('back.auth_setting.submit_button')</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
			</div>
		</div>
	</section>
@endsection

@push('script')
	<script>
		$('#backToDefault').hide();
		// $('#customConfiguration').hide();
		$('#customConfiguration').show();
	
		$('#customization').click(function() {
			$(this).hide();
			$('#backToDefault').show();
			$('#customConfiguration').show();
		})
		
		$('#backToDefault').click(function() {
			$(this).hide();
			$('#customization').show();
			$('#customConfiguration').hide();
		})
	</script>
	<script>
		$("#type").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#value").attr('type', 'file')
			} else {
				$("#value").attr('type', 'text')
			}
		})

		$("#editType").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#editValue").attr('type', 'file')
			} else {
				$("#editValue").attr('type', 'text')
			}
		})

		if($("#registration_self").is(":checked")) {
			$("#showSocialMedia").show();
		} else {
			$("#showSocialMedia").hide();
		}

		$("#registration_self").change(function() {
			if($("#registration_self").is(":checked")) {
				$("#showSocialMedia").show();
			} else {
				$("#showSocialMedia").hide();
			}
		})

	</script>

	<script>
		$(function () {
			$(document).on('click', '#editSetting', function (e) {
				e.preventDefault();
				var data_type = $(this).attr('data-type');
				var data_value = $(this).attr('data-value');
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');

				$("#editId").val(data_id)
				$("#editName").val(data_name)
				$("#editType").val(data_type)
				$("#editValue").val(data_value)

				$("#modalFormEdit").modal('show');
			});
		});
	</script>
@endpush