@extends('admin.layouts.app')

@section('title', 'Setting Kunci Autentikasi')


@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Setting Autentikasi</h2>
            </div>

						<div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Konfigurasi Auth Key</h2>
                        </div>
                        <div class="body">
                            <form action="{{url(\Request::route()->getPrefix() . '/setting_auth_key/update')}}" method="post" enctype="multipart/form-data" >
                              {{csrf_field()}}
                              <div class="modal-body">
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label for="">GOOGLE CLIENT ID</label>
                                          <input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->google_client_id}}" name="google_client_id" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label for="">GOOGLE CLIENT SECRET</label>
                                          <input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->google_client_secret}}" name="google_client_secret" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label for="">FACEBOOK CLIENT ID</label>
                                          <input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->facebook_client_id}}" name="facebook_client_id" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <label for="">FACEBOOK CLIENT SECRET</label>
                                          <input type="text" class="form-control" value="{{!$SettingAuthKey ? '' : $SettingAuthKey->facebook_client_secret}}" name="facebook_client_secret"  />
                                      </div>
                                  </div>
                              </div>
                              <input type="hidden" name="action" value="{{!$SettingAuthKey ? 'store' : 'update'}}">
                              <button type="submit" name="submit" class="btn btn-primary">SAVE</button>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

					</div>
            </div>

	</section>
@endsection

@push('script')

@endpush
