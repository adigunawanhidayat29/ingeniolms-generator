@extends('admin.layouts.app')

@section('title', 'Setting')

@push('style')
	<link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
@endpush


@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>PENGATURAN</h2>
			</div>

			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div style="margin-bottom: 2rem;">							
								<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary" href="#/">Tambah Pengaturan</a>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover">
									<tr>
										<th>Nama Item</th>
										<th>Value</th>
										<th>Tipe</th>
										<th>Opsi</th>
									</tr>
									@foreach ($settings as $data)
										<tr>
											<td>{{$data->name}}</td>
											<td>{!! $data->type == 'file' ? '<img height=50 src='.asset_url($data->value).'>' : $data->value!!}</td>
											<td>{{$data->type}}</td>
											<td>
												<a id="editSetting" class="btn btn-warning btn-sm" href="#/" data-type="{{$data->type}}" data-value="{{$data->value}}" data-name="{{$data->name}}" data-id="{{$data->id}}">
													<i class="material-icons">edit</i><span>Ubah</span>
												</a>
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>

					{{-- <div class="card">
						<div class="header">
							<h2>Tema & Tampilan</h2>
						</div>
						<div class="body">
							<a href="/admin/themes" class="btn btn-primary">Manage Tema</a> <br><br>
							<table class="table">
								<tr>
									<th>Title</th>
									<th>Name</th>
									<th>Preview</th>
									<th>Status</th>
									<th>Pilihan</th>
								</tr>
								@foreach ($themes as $item)
								<tr>
									<td>{{$item->title}}</td>
									<td>{{$item->name}}</td>
									<td> <img height="200" src="{{$item->thumbnail}}" alt=""> </td>
									<td>{{$item->status == 1 ? 'Active' : 'Non Active'}}</td>
									<td>
										<a target="_blank" class="btn btn-sm btn-info" href="{{$item->description}}">Preview Demo</a>
										@if($item->status !== 1)
										<a onclick="return confirm('Yakin akan mengubah tema?')" class="btn btn-sm btn-primary"
											href="/admin/themes/activated/{{$item->id}}">Set Theme</a>
										@endif
									</td>
								</tr>
								@endforeach
							</table>

						</div>
					</div> --}}

					<div class="card">
						<div class="header">
							<h2>Konfigurasi Vcon</h2>
						</div>
						<div class="body">
							<div style="margin-bottom: 2rem;">
								<a href="#" data-target="#modalAddServerVcon" data-toggle="modal" class="btn btn-primary">Tambah Server</a>
							</div>
							{{-- <a href="/admin/vcon_setting" class="btn btn-primary">Setting Vcon</a> <br><br> --}}
							@foreach($vcon_setting as $data)
							<table class="table">
								<tr>
									<td style="width:10%">ID</td>
									<td style="width:5%">:</td>
									<td style="width:85%">{{$data->id}}</td>
								</tr>
								<tr>
									<td style="width:10%">Base Url</td>
									<td style="width:5%">:</td>
									<td style="width:85%">{{$data->base_url}}</td>
								</tr>
								<tr>
									<td style="width:10%">Salt</td>
									<td style="width:5%">:</td>
									<td style="width:85%">{{$data->salt}}</td>
								</tr>
								<tr>
									<td style="width:10%">Opsi</td>
									<td style="width:5%">:</td>
									<td style="width:85%">
										<a class="btn btn-info btn-sm" target="_blank" href="/admin/test/bbb/getmeetings/{{$data->id}}">Lihat List Meetings</a>
										<a class="btn btn-primary btn-sm" href="/admin/vcon_setting/set_default/{{$data->id}}"><i class="material-icons">favorite</i><span>{{$data->status == '1' ? 'Default' : 'Set Default'}}</span></a>
										<a class="btn btn-warning btn-sm" href="/admin/vcon_setting/{{$data->id}}"><i class="material-icons">edit</i><span>Ubah</span></a>
										<a onclick="return confirm('Yakin akan menghapus data?')" class="btn btn-danger btn-sm" href="/admin/vcon_setting/delete/{{$data->id}}"><i class="material-icons">delete</i><span>Hapus</span></a>
									</td>
								</tr>
							</table>
							@endforeach

						</div>
					</div>

					<div class="card">
						<div class="header">
							<h2>Konfigurasi Autentikasi</h2>
						</div>
						<div class="body">
							<div style="margin-bottom: 2rem;">
								<a href="/admin/setting_auth_key" class="btn btn-primary">Atur Autentikasi</a>
							</div>
							@if($setting_auth_keys)
								<table class="table">
									<tr>
										<td style="width:10%">Google Client</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$setting_auth_keys->google_client_id}}</td>
									</tr>
									<tr>
										<td style="width:10%">Google Client Secret</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$setting_auth_keys->google_client_secret}}</td>
									</tr>
									<tr>
										<td style="width:10%">Facebook CLiend ID</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$setting_auth_keys->facebook_client_id}}</td>
									</tr>
									<tr>
										<td style="width:10%">Facebook Client Secret</td>
										<td style="width:5%">:</td>
										<td style="width:85%">{{$setting_auth_keys->facebook_client_secret}}</td>
									</tr>
								</table>
							@endif
						</div>
					</div>

					<div class="card">
						<div class="header">
							<h2>Konfigurasi Enrollment Kelas</h2>
						</div>
						<div class="body">
							<form action="{{url(\Request::route()->getPrefix() . '/settings/action')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								<div class="form-group">
									<label for="">Default Enrollment?</label>
									<div class="form-line">
										<input type="hidden" value="enrollment" name="name">
										<input type="hidden" value="text" name="type">
										<input type="hidden" name="action"
											value="{{ isset(Setting('enrollment')->value) ? 'update' : 'store' }}">
										@if(isset(Setting('enrollment')->value))
										<input type="hidden" name="id" value="{{ Setting('enrollment')->id }}">

										@php
										$enrollmentSelected = explode(',', Setting('enrollment')->value);
										@endphp
										@endif

										<input type="checkbox" class="filled-in" id="manual" name="value[]"
											{{ isset(Setting('enrollment')->value) && in_array("manual", $enrollmentSelected) ? 'checked' : '' }}
											value="manual">
										<label for="manual">Manual</label>
										<input type="checkbox" class="filled-in" id="self" name="value[]"
											{{ isset(Setting('enrollment')->value) && in_array("self", $enrollmentSelected) ? 'checked' : '' }}
											value="self">
										<label for="self">Self Enrollment</label>
										<input type="checkbox" class="filled-in" id="cohort" name="value[]"
											{{ isset(Setting('enrollment')->value) && in_array("cohort", $enrollmentSelected) ? 'checked' : '' }}
											value="cohort">
										<label for="cohort">Cohort</label>
									</div>
								</div>
								<button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
							</form>
						</div>
					</div>

					<div class="card">
						<div class="header">
							<h2>Konfigurasi Registrasi</h2>
						</div>
						<div class="body">
							<form action="{{url(\Request::route()->getPrefix() . '/settings/action/registration')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								<div class="form-group">
									<label for="">Default Registrasi?</label>
									<div class="form-line">
										@if(isset(Setting('registration')->value))
											<input type="hidden" name="id" value="{{ Setting('registration')->id }}">
											@php
											$registrationSelected = explode(',', Setting('registration')->value);
											@endphp
										@endif
	
										<input type="checkbox" class="filled-in" id="registration_manual" name="registration[]"
											{{ isset(Setting('registration')->value) && in_array("manual", $registrationSelected) ? 'checked' : '' }}
											value="manual">
										<label for="registration_manual">Manual</label>
	
										<input type="checkbox" class="filled-in" id="registration_self" name="registration[]"
											{{ isset(Setting('registration')->value) && in_array("self", $registrationSelected) ? 'checked' : '' }}
											value="self">
										<label for="registration_self">Self Registration</label>
	
										<input type="checkbox" class="filled-in" id="registration_third" name="registration[]"
											{{ isset(Setting('registration')->value) && in_array("third", $registrationSelected) ? 'checked' : '' }}
											value="third">
										<label for="registration_third">Third Party</label>
	
										@if(isset(Setting('login_with')->value))
											<input type="hidden" name="id" value="{{ Setting('login_with')->id }}">
											@php
												$login_withSelected = explode(',', Setting('login_with')->value);
											@endphp
										@endif
	
										<div id="showSocialMedia" class="mt-3">
											<input type="checkbox" class="filled-in" id="registration_self_email" name="login_with[]" value="email" {{ isset(Setting('login_with')->value) && in_array("email", $login_withSelected) ? 'checked' : '' }}>
											<label for="registration_self_email">Email</label>
											<input type="checkbox" class="filled-in" id="registration_self_facebook" name="login_with[]" value="facebook" {{ isset(Setting('login_with')->value) && in_array("facebook", $login_withSelected) ? 'checked' : '' }}>
											<label for="registration_self_facebook">Facebook</label>
											<input type="checkbox" class="filled-in" id="registration_self_google" name="login_with[]" value="google" {{ isset(Setting('login_with')->value) && in_array("google", $login_withSelected) ? 'checked' : '' }}>
											<label for="registration_self_google">Google</label>
										</div>										
									</div>
								</div>
								<button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- #END# Task Info -->
		</div>

		<div class="modal fade" id="modalForm" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="largeModalLabel">TAMBAH PENGATURAN</h4>
					</div>
					<form action="{{url(\Request::route()->getPrefix() . '/settings/action')}}" method="post"
						enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="modal-body">
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Nama Item</label>
									<input type="text" class="form-control" name="name" placeholder="Nama Item" />
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Tipe Item</label>
									<select class="form-control" id="type" name="type">
										<option value="text">Text</option>
										<option value="file">File</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Value</label>
									<input type="text" class="form-control" id="value" name="value" placeholder="Value" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="action" value="store">
							<button type="submit" name="submit" class="btn btn-primary waves-effect">Simpan</button>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalFormEdit" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="largeModalLabel">TAMBAH PENGATURAN</h4>
					</div>
					<form action="{{url(\Request::route()->getPrefix() . '/settings/action')}}" method="post"
						enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="modal-body">
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Nama Item</label>
									<input type="text" class="form-control" name="name" placeholder="Nama Item" id="editName" />
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Tipe Item</label>
									<select class="form-control" id="editType" name="type">
										<option value="text">Text</option>
										<option value="file">File</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="" class="control-label">Value</label>
									<input type="text" class="form-control" id="editValue" name="value" placeholder="Value" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" id="editId" value="">
							<input type="hidden" name="action" value="update">
							<button type="submit" name="submit" class="btn btn-primary waves-effect">Simpan</button>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalAddServerVcon" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="largeModalLabel">TAMBAH SERVER VCON</h4>
					</div>
					<form action="{{url(\Request::route()->getPrefix() . '/vcon_setting/store')}}" method="post" enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="modal-body">
							<div class="form-group">
								<div class="form-line">
									<label for="">Base URL</label>
									<input type="text" class="form-control" name="base_url" placeholder="Base URL" />
								</div>
							</div>
							<div class="form-group">
								<div class="form-line">
									<label for="">Salt</label>
									<input type="text" class="form-control" name="salt" placeholder="Salt" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" id="editId" value="">
							<input type="hidden" name="action" value="update">
							<button type="submit" name="submit" class="btn btn-primary waves-effect">Simpan</button>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('script')
	<script>
		$("#type").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#value").attr('type', 'file')
			} else {
				$("#value").attr('type', 'text')
			}
		})

		$("#editType").change(function () {
			var type = $(this).val();
			if (type == 'file') {
				$("#editValue").attr('type', 'file')
			} else {
				$("#editValue").attr('type', 'text')
			}
		})

		if($("#registration_self").is(":checked")) {
			$("#showSocialMedia").show();
		} else {
			$("#showSocialMedia").hide();
		}

		$("#registration_self").change(function() {
			if($("#registration_self").is(":checked")) {
				$("#showSocialMedia").show();
			} else {
				$("#showSocialMedia").hide();
			}
		})

	</script>

	<script>
		$(function () {
			$(document).on('click', '#editSetting', function (e) {
				e.preventDefault();
				var data_type = $(this).attr('data-type');
				var data_value = $(this).attr('data-value');
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');

				$("#editId").val(data_id)
				$("#editName").val(data_name)
				$("#editType").val(data_type)
				$("#editValue").val(data_value)

				$("#modalFormEdit").modal('show');
			});
		});
	</script>
@endpush