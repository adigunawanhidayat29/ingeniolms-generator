@extends('admin.layouts.app')

@section('title', 'Setting')


@section('content')
<section class="content">
	<div class="container-fluid">
			<div class="block-header">
					<h2>Setting Vcon</h2>
			</div>

			<div class="row clearfix">
					<!-- Task Info -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="card">
									<div class="header">
											<h2>Konfigurasi Vcon</h2>
									</div>
									<div class="body">
											<form action="{{url(\Request::route()->getPrefix() . '/vcon_setting/update')}}" method="post" enctype="multipart/form-data" >
												{{csrf_field()}}
												<div class="modal-body">
														<div class="form-group">
																<div class="form-line">
																		<label for="">BASE URL</label>
																		<input type="text" class="form-control" value="{{!$vcon_setting ? '' : $vcon_setting->base_url}}" name="base_url" placeholder="Base URL" />
																</div>
														</div>
														<div class="form-group">
																<div class="form-line">
																		<label for="">SALT</label>
																		<input type="text" class="form-control" value="{{!$vcon_setting ? '' : $vcon_setting->salt}}" name="salt" placeholder="Salt" />
																</div>
														</div>
												</div>
												<input type="hidden" name="id" value="{{$vcon_setting->id}}">
												<button type="submit" name="submit" class="btn btn-primary">SAVE</button>
										</form>
									</div>
							</div>
					</div>
					<!-- #END# Task Info -->
			</div>

		</div>
			</div>

</section>
@endsection

@push('script')

@endpush
