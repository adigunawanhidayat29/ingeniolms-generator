@extends('admin.layouts.app')

@section('title', 'Setting')

@push('style')
     <link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
@endpush


@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>API</h2>
            </div>

						<div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>API</h2>
                        </div>
                        <div class="body">
                            <pre>
                                <h3>Home</h3>
                                <p>
                                    Url: /api/v3/home
                                </p>
                            </pre>
                            <pre>
                                <h3>Courses</h3>
                                <p>
                                    Url: /api/v3/courses
                                </p>
                            </pre>
                            <pre>
                                <h3>Settings</h3>
                                <p>
                                    Url: /api/v3/settings
                                </p>
                            </pre>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>

					</div>
            </div>
            

	</section>
@endsection

@push('script')

<script>
    $("#type").change(function(){
        var type = $(this).val();
        if(type == 'file'){
            $("#value").attr('type', 'file')
        }else{
            $("#value").attr('type', 'text')

        }
    })

    $("#editType").change(function(){
        var type = $(this).val();
        if(type == 'file'){
            $("#editValue").attr('type', 'file')
        }else{
            $("#editValue").attr('type', 'text')

        }
    })
</script>

<script>
    $(function(){
	    $(document).on('click','#editSetting',function(e){
	      e.preventDefault();
	        var data_type = $(this).attr('data-type');
	        var data_value = $(this).attr('data-value');
            var data_name = $(this).attr('data-name');
            var data_id = $(this).attr('data-id');

            $("#editId").val(data_id)
            $("#editName").val(data_name)
            $("#editType").val(data_type)
            $("#editValue").val(data_value)
            
	        $("#modalFormEdit").modal('show');
	    });
	  });
</script>
  
@endpush
