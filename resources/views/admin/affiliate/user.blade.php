@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Affiliate List' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Affiliate List</h3>
            <br><br>
            {{-- <a href="{{url('affiliate/create')}}" class="btn btn-primary">Create New Affiliate</a> --}}
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="">
              <table class="table table-hover" id="data">
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Url</th>
                  <th>Code</th>
                  <th>Banner</th>
                  <th>Action</th>
                </tr>
								@foreach($AffiliateUsers as $index => $AffiliateUser)
									<tr>
										<td>{{$index+1}}</td>
										<td>{{$AffiliateUser->name}}</td>
										<td>{{$AffiliateUser->url}}</td>
										<td>{{$AffiliateUser->code}}</td>
										<td><img src="{{asset_url($AffiliateUser->banner)}}"></td>
										<td>
											<div class="dropdown">
											  <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
													Action
											    <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu" role="menu" aria-labelledby="">
													<li><a href="/affiliate/balance/{{$AffiliateUser->id}}">Balance</a></li>
													<li><a href="/affiliate/transaction/{{$AffiliateUser->id}}">Transaction</a></li>
													<li><a href="/affiliate/withdrawal/{{$AffiliateUser->id}}">Withdrawal</a></li>
													<li><a href="/affiliate/profile/{{$AffiliateUser->id}}">Profile</a></li>
											  </ul>
											</div>
										</td>
									</tr>
								@endforeach
              </table>
							{{$AffiliateUsers->render()}}
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
