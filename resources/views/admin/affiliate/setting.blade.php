@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Setting Affiliate
@endsection

@push('style')
	<link rel="stylesheet" href="/jquery-ui/jquery-ui.min.css">
@endpush

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-body">

						@if(Session::get('success'))
							<div class="alert alert-success">{{Session::get('success')}}</div>
						@endif

						@if($affiliate_setting)
							<form class="" action="/affiliate/setting/update" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								<div class="form-group">
								  <label for="">Komisi</label>
								  <input type="number" class="form-control" name="commission" value="{{$affiliate_setting->commission}}" placeholder="Komisi">
								</div>
								<div class="form-group">
								  <label for="">Diskon</label>
								  <input max="100" type="number" class="form-control" name="discount" value="{{$affiliate_setting->discount}}" placeholder="Diskon">
								</div>
								<div class="form-group">
								  <label for="">Tanggal Mulai</label>
								  <input type="text" class="form-control" id="start_date" name="start_date" value="{{$affiliate_setting->start_date}}" placeholder="Tanggal Mulai">
								</div>
								<div class="form-group">
								  <label for="">Tanggal Akhir</label>
								  <input type="text" class="form-control" id="end_date" name="end_date" value="{{$affiliate_setting->end_date}}" placeholder="Tanggal Akhir">
								</div>
								<div class="form-group">
								  <label for="">Banner</label>
									@if($affiliate_setting->banner)
										<br>
										<img src="{{asset_url($affiliate_setting->banner)}}" height="100">
										<br>
									@endif
								  <input type="file" name="banner">
								</div>
								<input type="submit" class="btn btn-primary" value="Simpan">
								{{-- <a class="btn btn-default" href="/affiliate/setting">Batal</a> --}}
							</form>
						@else
							<a class="btn btn-primary" data-toggle="modal" data-target="#modalSettingCreate" href="#">Create Setting</a>
						@endif
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	<div class="modal fade" id="modalSettingCreate" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id=""></h4>
	      </div>
	      <div class="modal-body">
					<form class="" action="/affiliate/setting/create" method="post" enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="form-group">
							<label for="">Komisi</label>
							<input type="number" class="form-control" name="commission" value="" placeholder="Komisi">
						</div>
						<div class="form-group">
							<label for="">Diskon</label>
							<input max="100" type="number" class="form-control" name="discount" value="" placeholder="Diskon">
						</div>
						<div class="form-group">
							<label for="">Tanggal Mulai</label>
							<input type="text" class="form-control" id="start_date" name="start_date" value="" placeholder="Tanggal Mulai">
						</div>
						<div class="form-group">
							<label for="">Tanggal Akhir</label>
							<input type="text" class="form-control" id="end_date" name="end_date" value="" placeholder="Tanggal Akhir">
						</div>
						<div class="form-group">
							<label for="">Banner</label>
							<input type="file" name="banner">
						</div>
						<input type="submit" class="btn btn-primary" value="Simpan">
					</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
@endsection

@push('script')
	<script type="text/javascript" src="/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript">
    $("#start_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
    $("#end_date").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true,
      // minDate : '2018-03-27'
    });
  </script>

@endpush
