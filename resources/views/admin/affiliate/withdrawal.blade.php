@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Affiliate Withdrawal' }}
@endsection


@section('main-content')
  <div class="container-fluid spark-screen">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">History Penarikan</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>No</td>
                      <td>Jumlah</td>
                      <td>Status</td>
                      <td>Pilihan</td>
                    </tr>
                    @foreach($affiliate_withdrawals as $index => $affiliate_withdrawal)
                      <tr>
                        <td>{{$index+1}}</td>
                        <td>{{rupiah($affiliate_withdrawal->request)}}</td>
                        <td>{{$affiliate_withdrawal->status == '1' ? 'Success' : 'Pending'}}</td>
                        <td>
                          @if($affiliate_withdrawal->status == '0')
                            <a onclick="return confirm('Yakin?')" href="/affiliate/withdrawal/approve/{{$affiliate_withdrawal->id}}">Approve</a>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                {{$affiliate_withdrawals->render()}}
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal" id="modalWithdrawalRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title color-primary" id="myModalLabel">Permintaan Penarikan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
              <form class="" action="/affiliate/withdrawal/request" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  {{-- <label for="">Jumlah</label> --}}
                  <input type="number" min="20000" class="form-control" name="amount" placeholder="Jumlah">
                </div>
                <input type="submit" value="Kirim" class="btn btn-primary btn-raised">
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
      </div>
  </div>
@endsection
