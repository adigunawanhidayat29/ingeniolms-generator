@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Affiliate Balance')

@section('main-content')
  <div class="container-fluid spark-screen">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-primary btn-raised">Saldo: {{rupiah($balance)}}</button>
        <br><br>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Affiliate Balance</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <table class="table">
                    <tr>
                      <td>No</td>
                      <td>Informasi</td>
                      <td>Debit</td>
                      <td>Kredit</td>
                    </tr>
                    @foreach($affiliate_balances as $index => $affiliate_balance)
                      <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$affiliate_balance->information}}</td>
                        <td>{{rupiah($affiliate_balance->debit)}}</td>
                        <td>{{rupiah($affiliate_balance->credit)}}</td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                {{$affiliate_balances->render()}}
              </div>

            </div>
          </div>
        </div>
        <a href="/affiliate/user" class="btn btn-default">Back To List</a>
      </div>
    </div>
  </div>
@endsection
