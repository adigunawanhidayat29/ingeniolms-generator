@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Users' }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Application List</h3>
            <br><br>
            <a href="{{url('users/create')}}" class="btn btn-primary">Create</a>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
              <table class="table table-hover" id="data">
                <thead>
                  <tr>
                    <th>Site Name</th>
                    <th>Domain Name</th>
                    <th>App ID</th>
                    <th>Secret App ID</th>
                    <th>Email</th>
                    <th>Created By</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
  <script type="text/javascript">
  $(function(){
    $("#data").DataTable({
			"order": [[ 3, "desc" ]],
      processing: true,
      serverSide: true,
      ajax: '{{ url("app/serverside") }}',
      columns: [
        { data: 'site_title', name: 'site_title' },
        { data: 'hostname', name: 'hostname' },
        { data: 'app_id', name: 'app_id' },
        { data: 'app_secret', name: 'app_secret' },
        { data: 'email', name: 'email' },
        { data: 'name', name: 'name' },
        { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
  </script>
@endpush
