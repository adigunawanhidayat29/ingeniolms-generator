@extends('admin.layouts.app')

@section('title', Lang::get('back.certificate_form.title'))

@push('style')
	<style>
		text { 
			cursor: move; 
			font-size: '12px'
		}
	</style>
@endpush


@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>{{ Lang::get('back.certificate_form.header') }}</h2>
			</div>

			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
						<div class="body">
							<div class="d-flex align-items-center justify-content-between">
								<h3 class="headline headline-sm" style="margin-top: 0; margin-bottom: 2rem;">{{ Lang::get('back.certificate_form.headline') }}</h3>
							</div>

							@if(!$certificate)
								<a class="btn btn-raised btn-primary" href="#" data-toggle="modal" data-target="#newCertificate">{{ Lang::get('back.certificate_form.create_button') }}</a>   
							@else
								<a class="btn btn-raised btn-primary" href="#" data-toggle="modal" data-target="#editCertificate">{{ Lang::get('back.certificate_form.edit_button') }}</a>

								<div class="d-block">
									<svg width="600" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<image href="{{ $certificate->image }}" height="600" width="600" />
										@foreach ( $certificate->certificate_attributes as $data )	
											<text style="font-size: 20px; text-align: center;" x="{{ $data->x_coordinate }}" y="{{ $data->y_coordinate }}">{{ $data->position }}</text>
										@endforeach
									</svg>
								</div>
							@endif
									
							@if(!$certificate)												
								<div class="modal" id="newCertificate" tabindex="-1" role="dialog" aria-labelledby="gradesDetailLabel">
									<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
										<div class="modal-content">
											<form method="post" action="/admin/certificate/store" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="modal-body">
													<div class="d-flex align-items-center justify-content-between mb-2">
														<h3 class="headline headline-sm" style="margin-top: 0; margin-bottom: 2rem;">{{ Lang::get('back.certificate_form.create_certificate_header') }}</h3>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">
																<i class="zmdi zmdi-close"></i>
															</span>
														</button>
													</div>
													<div class="table-responsive">
														<div class="">
															<label for="">{{ Lang::get('back.certificate_form.image_label') }}</label> <br>
															<input type="file" id="imgInp" name="image">

															<select name="" id="attrPosition">
																<option value="{{ Lang::get('back.certificate_form.participant_label') }}">{{ Lang::get('back.certificate_form.participant_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.class_label') }}">{{ Lang::get('back.certificate_form.class_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.date_label') }}">{{ Lang::get('back.certificate_form.date_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.certificate_id_label') }}">{{ Lang::get('back.certificate_form.certificate_id_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.signature_label') }}">{{ Lang::get('back.certificate_form.signature_label') }}</option>
															</select>
															<button type="button" id="addAttribute">{{ Lang::get('back.certificate_form.attribute_button') }}</button>

															<div id="newAttribute"></div>
					
															<div class="mt-2 text-center">
																<svg id="cvs" width="600" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																	<image id="blah" href="#" height="600" width="600" />
																</svg>
															</div>
					
															{{-- <input type="submit" name="submit" value="Simpan" class="btn btn-primary btn-raised"> --}}
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-primary">{{ Lang::get('back.certificate_form.submit_button') }}</button>
													<button type="button" class="btn bg-blue-grey" data-dismiss="modal">{{ Lang::get('back.certificate_form.close_button') }}</button>
												</div>
											</form>
										</div>
									</div>
								</div>												
							@else												
								<div class="modal" id="editCertificate" tabindex="-1" role="dialog" aria-labelledby="gradesDetailLabel">
									<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
										<div class="modal-content">
											<form method="post" action="/admin/certificate/update" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="modal-body">
													<div class="d-flex align-items-center justify-content-between mb-2">
														<h3 class="headline headline-sm" style="margin-top: 0; margin-bottom: 2rem;">{{ Lang::get('back.certificate_form.edit_certificate_header') }}</h3>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">
																<i class="zmdi zmdi-close"></i>
															</span>
														</button>
													</div>
													<div class="table-responsive">
														<div class="">
															<label for="">{{ Lang::get('back.certificate_form.image_label') }}</label> <br>
															<input type="file" id="imgInp" name="image">
															<input type="hidden" value="{{ $certificate->id }}" name="id">
															<select name="" id="attrPosition">
																<option value="{{ Lang::get('back.certificate_form.participant_label') }}">{{ Lang::get('back.certificate_form.participant_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.class_label') }}">{{ Lang::get('back.certificate_form.class_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.date_label') }}">{{ Lang::get('back.certificate_form.date_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.certificate_id_label') }}">{{ Lang::get('back.certificate_form.certificate_id_label') }}</option>
																<option value="{{ Lang::get('back.certificate_form.signature_label') }}">{{ Lang::get('back.certificate_form.signature_label') }}</option>
															</select>
															<button type="button" id="addAttribute">{{ Lang::get('back.certificate_form.attribute_button') }}</button>

															<div id="newAttribute"></div>

															@foreach ( $certificate->certificate_attributes as $data )	
																<input type="hidden" id="x_coordinate{{ $data->id }}" value="{{ $data->x_coordinate }}" name="x_coordinate[]">
																<input type="hidden" id="y_coordinate{{ $data->id }}" value="{{ $data->y_coordinate }}" name="y_coordinate[]">
																<input type="hidden" value="{{ $data->position }}" name="position[]">
															@endforeach
					
															<div class="mt-2 text-center">
																<svg id="cvs" width="600" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																	<image id="blah" href="{{ $certificate->image }}" height="600" width="600" />
																	@foreach ( $certificate->certificate_attributes as $data )
																		<text data-id="{{ $data->id }}" id="textAttribute{{ $data->id }}" style="font-size: 20px; text-align: center;" x="{{ $data->x_coordinate }}" y="{{ $data->y_coordinate }}">{{ $data->position }}</text>
																	@endforeach
																</svg>
															</div>
					
															{{-- <input type="submit" id="submitAttribute" name="submit" value="Simpan" class="btn btn-primary btn-raised"> --}}
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-primary">{{ Lang::get('back.certificate.submit_button') }}</button>
													<button type="button" class="btn bg-blue-grey" data-dismiss="modal">{{ Lang::get('back.certificate.close_button') }}</button>
												</div>
											</form>
										</div>
									</div>
								</div>												
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>  
@endsection

@push('script')
	<script src="/js/pablo.min.js"></script>
	<script>
		@if(!$certificate)
			$("#blah").hide();
		@else
			$("#blah").show();
		@endif

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('#blah').attr('href', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]); // convert to base64 string
			}
		}

		$("#imgInp").change(function() {
			$("#blah").show();
			readURL(this);
		});
	</script>

	<script>
		var selectedElement = null;
		var prevElement = null;
		var currentX = 0;
		var currentY = 0;

		$(document).ready(function() {				
			function handleDragStart(e) {
				log("handleDragStart");                
				this.style.opacity = '0.4';  // this ==> e.target is the source node.
			};
			
			var registercb = function () {
				$("#cvs > text").mousedown(function (e) {
					// save the original values
					currentX = e.clientX;
					currentY = e.clientY;
					selectedElement = e.target;
				}).mousemove(function (e) {    
					// if there is an active element, move it around            
					if (selectedElement) {
						var dx = parseInt(selectedElement.getAttribute("x")) + e.clientX - currentX;
						var dy = parseInt(selectedElement.getAttribute("y")) + e.clientY - currentY;
						currentX = e.clientX;
						currentY = e.clientY;
						selectedElement.setAttribute("x", dx);
						selectedElement.setAttribute("y", dy);

						var textEl = selectedElement.getAttribute("data-id");
						// alert(textEl)
						$("#x_coordinate"+textEl).val(dx);
						$("#y_coordinate"+textEl).val(dy);
					}
				}).mouseup(function (e) {
					// deactivate element after setting it into its new location
					selectedElement = null;  
				});
			};
			
			function log() {
					if (window.console && window.console.log)
							console.log('[XXX] ' + Array.prototype.join.call(arguments, ' '));
			};
			
			registercb();

			$("#addAttribute").click(function() {
				var attrPosition = $("#attrPosition").val();
				var randomNumber = 'new' + Math.floor(Math.random() * 10);
				$("#newAttribute").append(`
					<input type="hidden" id="x_coordinate`+randomNumber+`" value="250" name="x_coordinate[]">
					<input type="hidden" id="y_coordinate`+randomNumber+`" value="365" name="y_coordinate[]">
					<input type="hidden" value="${attrPosition}" name="position[]">
				`);
				Pablo("#cvs").append('<text data-id="'+randomNumber+'" id="textAttributenew'+randomNumber+'" style="font-size: 20px; text-align: center;" x="250" y="365">'+ attrPosition +'</text>'); 
				registercb();
			})
		}); 
	</script>
@endpush
