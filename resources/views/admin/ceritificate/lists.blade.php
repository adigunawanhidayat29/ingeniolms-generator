@extends('admin.layouts.app')

@section('title', Lang::get('back.certificate.title'))

@push('style')  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
@endpush

@section('content')
  <section class="content">
    <div class="container-fluid">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>

      <div class="block-header">
        <h2>{{ Lang::get('back.certificate.header') }}</h2>
      </div>

      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="body">
              <div class="table-responsive">
                <div style="margin-bottom: 2rem;">
                  <a href="{{url( \Request::route()->getPrefix() . '/ceritificate')}}" class="btn btn-primary">{{ Lang::get('back.certificate.create_button') }}</a>
                </div>

                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                  <thead>
                    <tr>
                      <th>{{ Lang::get('back.certificate.number_table') }}</th>
                      <th>{{ Lang::get('back.certificate.design_table') }}</th>
                      <th>{{ Lang::get('back.certificate.certificate_id_table') }}</th>
                      <th>{{ Lang::get('back.certificate.option_table') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($ceritificates as $index => $data )
                      <tr>
                        <td>{{ $index+1 }}</td>
                        <td> <img src="{{ asset_url($data->image) }}" height="200" alt=""></td>
                        <td>
                          @php
                            $courses_data = \DB::table('courses')->whereIn('id', explode(',', $data->course_id))->get()
                          @endphp
                          @foreach ($courses_data as $course)
                            <ul>
                              <li>{{ $course->title }}</li>
                            </ul>
                          @endforeach
                        </td>
                        <td>
                          <a class="btn btn-warning btn-sm" href="/admin/ceritificate?id={{ $data->id }}">
                            <i class="material-icons">edit</i><span>{{ Lang::get('back.certificate.edit_button') }}</span>
                          </a>
                          <a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#modalAddCourse{{ $data->id }}">
                            <i class="material-icons">add</i><span>{{ Lang::get('back.certificate.add_class_button') }}</span>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @foreach ($ceritificates as $index => $data )
    <div class="modal fade" id="modalAddCourse{{ $data->id }}" role="dialog">
      <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">{{ Lang::get('back.certificate.add_class_header') }}</h4>
          </div>
          <form action="/admin/ceritificate/add-courses" method="POST">    
            {{ csrf_field() }}                  
            <div class="modal-body">
              <div class="form-group">
                <select name="course_id[]" class="chosen-select" multiple>
                  @foreach ($courses as $item)  
                    <option {{ in_array($item->id, explode(',', $data->course_id)) ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->title }}</option>
                  @endforeach
                </select>
              </div>
              <input type="hidden" name="id" value="{{ $data->id }}">
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">{{ Lang::get('back.certificate.submit_button') }}</button>
              <button type="button" class="btn bg-blue-grey" data-dismiss="modal">{{ Lang::get('back.certificate.close_button') }}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  @endforeach
@endsection

@push('script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
  <script>
    $(".chosen-select").chosen({width: "100%"}); 
  </script>
@endpush