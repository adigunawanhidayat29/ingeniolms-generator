@extends('admin.layouts.app')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<style>
		[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
		    position: absolute;
		    left: unset;
		    opacity: 1;
		    cursor: pointer;
		}
	</style>
@endpush

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Tambah Dari Bank SOal</h2>
		</div>

			<!-- Task Info -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">

				</div>
			</div>
		</div>

		<form action="{{url('admin/course/quiz/get', [$quiz->id, $bank_soal->id])}}" method="post">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="row">
			</div>
      <div class="btn-group" style="margin-bottom:20px;">
        <button type="submit" class="btn btn btn-primary dropdown-toggle btn-raised">
          Tambah Pertanyaan
        </button>
      </div>

			<div class="card card-library">
				<div class="card-block">
					<div class="" style="padding:20px;">
						<div class="table-responsive table-cs">
							<table id="example" class="table table-striped d-table">
								<thead>
									<tr>
	                  <th></th>
										<th style="min-width: 25vh;">Title</th>
										<th>Tipe</th>
										<th class="text-center">Bobot Nilai</th>
									</tr>
								</thead>
								<tbody>
	                @foreach($bank_soal->question as $index => $item)
	                <tr>
	                  <td style="    width: 20px;"><input type="checkbox" name="bank_question_id[]" value="{{$item->id}}"></td>
	                  <td>{{strip_tags($item->question)}}</td>
	                  <td>{{$item->type->type}}</td>
	                  <td class="text-center">{{$item->weight}}</td>
	                </tr>
	                @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection

@push('script')
	<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
	<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="{{asset('js/sweetalert.min.js')}}"></script>
	<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script>

		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();

		// trigger modal add konten / quiz

		$(document).ready(function() {
			$('#example').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
		});
	</script>
@endpush
