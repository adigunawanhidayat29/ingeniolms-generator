@extends('layouts.app')

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">

	<!-- <style>
		ul.breadcrumb {
				padding: 10px 16px;
				list-style: none;
				background-color: #eee;
		}
		ul.breadcrumb li {
				display: inline;
				font-size: 18px;
		}
		ul.breadcrumb li+li:before {
				padding: 8px;
				color: black;
				content: "/\00a0";
		}
		ul.breadcrumb li a {
				color: #0275d8;
				text-decoration: none;
		}
		ul.breadcrumb li a:hover {
				color: #01447e;
				text-decoration: underline;
		}
	</style> -->

@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Membuat <span>Konten</span></h2>
        </div>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="headline-sm headline-line">Membuat Library Konten</h3>
				</div>
				<div class="col-md-12">
					<!-- Default box -->
					<div class="box">
						<div class="box-body">
							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>
							<div class="card-md">
							  <!-- <div class="panel-heading">
							    <h3 class="panel-title">{{$button}} Konten</h3>
							  </div> -->
							  <div class="card-block">
									<form action="{{url($action)}}" method="post" enctype="multipart/form-data">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										@if($content->type_content == 'file')
											<div class="form-group" id="FileFile">
												<label for="title">File (Docx, PDF, xlsx, etc.)</label>
												<input type="text" readonly="" class="form-control" placeholder="Pilih...">
												<input type="file" id="file" name="file"class="form-control">
												<i>* Kosongkan bila tidak akan merubah file</i>
											</div>
										@endif

                    @if($content->type_content == 'url')

											<div class="form-group" id="FileURL">
												<label for="title">URL</label>
												<input id="contentFullPathFile" placeholder="Masukan URL" type="text" class="form-control" name="full_path_file" value="" required>
												<i>* Kosongkan bila tidak akan merubah url</i>
                      </div>


										@endif

											<div class="form-group" id="divTitle">
												<label for="title">Judul</label>
												<input id="contentTitle" placeholder="Judul" type="text" class="form-control" name="title" value="{{ $content->title }}" required>
												@if ($errors->has('title'))
													<span class="text-danger">{{ $errors->first('title') }}</span>
												@endif
											</div>

										@if($content->type_content == 'video')
											<div class="form-group" id="FileVideo">
												<label for="title">Pilih file untuk diupload</label>
												<input type="text" readonly="" class="form-control" placeholder="Pilih...">
												<input type="file" id="file" name="video_file" class="form-control">

												<div class="progress" style="display:none">
													<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
														<span class="sr-only">0%</span>
													</div>
												</div>

												<div id="uploadedMessageError">
													<div id="uploadedMessage"></div>
												</div>
												<i>* Kosongkan bila tidak akan merubah video</i>
											</div>
										@endif


										<div class="form-group" id="divDescription">
											<label for="title">Deskripsi / Penjelasan</label>
											<textarea name="description" id="description">{{ $content->description }}</textarea>
										</div>

										<div class="form-group">
											<!-- <button onclick="afterClick()" type="button" id="contentSave" class="btn btn-success btn-raised">Simpan dan Terbitkan</button> -->
											<button type="submit" class="btn btn-success btn-raised">Simpan dan Terbitkan</button>
											<a href="{{ url('instructor/library') }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									</form>
							  </div>
							</div>
						</div>
						<!-- /.box-body-->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')

	<script>
		function afterClick(){
			$("#contentSave").removeClass('btn-success');
			$("#contentSave").removeClass('btn-raised');
			$("#contentSave").html('<i class="fa fa-circle-o-notch fa-spin"></i>Menyimpan');
		}
	</script>

	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	<script type="text/javascript">
		@if(Request::get('content') == 'video')
			// $("#divTitle").hide()
			// $("#divDescription").hide()
			// $("#contentSave").hide()
			// $("#divSequence").hide()
		@endif
	</script>

	<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>

	<script src="https://apis.google.com/js/client.js?onload=init"></script>
@endpush
