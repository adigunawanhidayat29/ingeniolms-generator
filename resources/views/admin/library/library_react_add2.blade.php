@extends('layouts.app_nation')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>
	<style>
		/* Style the tab */
		.tab {
				float: left;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
				width: 30%;
				height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
				display: block;
				background-color: inherit;
				color: black;
				padding: 22px 16px;
				width: 100%;
				border: none;
				outline: none;
				text-align: left;
				cursor: pointer;
				transition: 0.3s;
				font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
				background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
				background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
				float: left;
				padding: 0px 12px;
				border: 1px solid #ccc;
				width: 70%;
				border-left: none;
				height: auto;
		}

		.placeholder{
				background-color:white;
				height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}
	</style>
@endpush

@section('content')
<div class="container">
	<h2>Tambah Kontent Library</h2>
	<div class="table-responsive card" style="padding:20px;">
		<form action="{{url('course/content/library/add/'.$course_id.'/'.$section_id)}}" method="post">
			{{ csrf_field() }}
			<button type="submit" class="btn btn-primary btn-raised">Tambah Kontent / Kuis</button>
			<table id="example" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th></th>
	                <th>Nama Kontent</th>
									<th>Tanggal Dibuat</th>
									<th>Ukuran</th>
									<th>Tipe</th>
	            </tr>
	        </thead>
	        <tbody>
						@foreach($content as $index => $item)
	            <tr>
									<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$item->id}}"></td>
	                <td>{{$item->title}}</td>
	                <td>{{$item->created_at}}</td>
	                <td class="text-center">-</td>
									<th>{{$item->type_content}}</th>
	            </tr>
						@endforeach
						@foreach($quiz_library as $index => $item)
							<tr>
									<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$item->id}}"></td>
									<td>{{$item->name}}</td>
									<td>{{$item->created_at}}</td>
									<td class="text-center">-</td>
									<th>quiz</th>
							</tr>
						@endforeach
						@foreach($assignment_library as $index => $item)
							<tr>
									<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$item->id}}"></td>
									<td>{{$item->title}}</td>
									<td>{{$item->created_at}}</td>
									<td class="text-center">-</td>
									<th>tugas</th>
							</tr>
						@endforeach
	        </tbody>
	    </table>
		</form>
	</div>
</div>

@endsection

@push('script')
		<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
		<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
		<script src="{{asset('js/sweetalert.min.js')}}"></script>
		<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script>
			$(document).ready(function() {
			    $('#example').DataTable();
			});
		</script>
@endpush
