@extends('admin.layouts.app')

@section('title', Lang::get('back.vcon_waiting.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <center>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('success') !!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-error alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('error') !!}
                    </div>
                @endif
            </center>
            
            <div class="block-header">
                <h2>@lang('back.vcon_waiting.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/vcons') }}"><i class="material-icons">arrow_back</i><span>@lang('back.vcon_waiting.back_button')</span></a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.vcon_waiting.name_table')</th>
                                            <th>@lang('back.vcon_waiting.status_table')</th>
                                            <th>@lang('back.vcon_waiting.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($vcon_waitings as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->status == '1' ? 'Disetujui' : 'Menunggu' }}</td>
                                                <td>
                                                    @if($item->status == '0')
                                                        <a class="btn btn-success btn-sm" href="{{ url('/admin/vcons/waiting/approve/' . $item->id) }}">
                                                            <i class="material-icons">check</i><span>@lang('back.vcon_waiting.approve_button')</span>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function(){
            var asset_url = '{{asset_url()}}';
            $("#data").DataTable();
        });
    </script>
@endpush