@extends('admin.layouts.app')

@section('title', 'Vcon Show')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Lists Meetings</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/settings') }}"><i class="material-icons">arrow_back</i><span>Kembali</span></a>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 700;">Nama Server</td>
                                            <td>{{ $VconSetting->base_url }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">Salt</td>
                                            <td>{{ $VconSetting->salt }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <h3>List Meetings</h3>

                            @if(count($meetings) > 0) 
                                <a href="/admin/test/bbb/endmeetings/{{ $VconSetting->id }}" class="btn btn-sm btn-danger" onclick="return confirm('Yakin akan menghentikan semua meeting?')">Akhiri Semua Meetings</a>
                                <table class="table table-hover" style="margin-bottom: 0;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Meeting</th>
                                            <th>Nama Meeting</th>
                                            <th>Meeting Created Date</th>
                                            <th>Is Running</th>
                                            <th>Atendees</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $index = 1 @endphp
                                        @foreach($meetings as $data)
                                            <tr>
                                                <td>{{ $index++ }}</td>
                                                <td>{{ $data->meetingID }}</td>
                                                <td>{{ $data->meetingName }}</td>
                                                <td>{{ $data->createDate }}</td>
                                                <td>{{ $data->running }}</td>
                                                <td>{{ $data->participantCount }}</td>
                                                <td>
                                                    <form action="/admin/test/bbb/endmeeting" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="salt" value="{{ $VconSetting->salt }}">
                                                        <input type="hidden" name="base_url" value="{{ $VconSetting->base_url }}">
                                                        <input type="hidden" name="id" value="{{ $data->meetingID }}">
                                                        <input type="hidden" name="password" value="{{ $data->moderatorPW }}">
                                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin akan menghentikan meeting ini?')">Akhiri Meeting Ini</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-info" style="margin-bottom: 0;">
                                    Tidak ada meeting
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
