@extends('admin.layouts.app')

@section('title', Lang::get('back.vcon_recording.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <center>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('success') !!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-error alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('error') !!}
                    </div>
                @endif
            </center>
            
            <div class="block-header">
                <h2>@lang('back.vcon_recording.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a class="btn bg-blue-grey" href="{{ url('/admin/vcons') }}"><i class="material-icons">arrow_back</i><span>@lang('back.vcon_recording.back_button')</span></a>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 700; border-top: 0;">#</td>
                                            <td style="border-top: 0;">{{ $vcon->id }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.vcon_recording.room_table')</td>
                                            <td>{{ $vcon->title }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.vcon_recording.password_table')</td>
                                            <td>{{ $vcon->password }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700;">@lang('back.vcon_recording.moderator_password_table')</td>
                                            <td>{{ $vcon->password_moderator }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 700; border-bottom: 0;">@lang('back.vcon_recording.waiting_table')</td>
                                            <td style="border-bottom: 0;">{{ $vcon->waiting == '1' ? Lang::get('back.vcon_recording.enabled_waiting') : Lang::get('back.vcon_recording.disabled_waiting') }}</td>
                                        </tr>
                                        {{-- <tr><th> Status </th><td> {{ $vcon->status }} </td></tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="body">
                            @if(isset($recordings->recording)) 
                                <table class="table table-striped table-hover" style="margin-bottom: 0;">
                                    <thead>
                                        <tr>
                                            <th>@lang('back.vcon_recording.record_name_table')</th>
                                            <th>@lang('back.vcon_recording.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($recordings as $data)
                                            <tr>
                                                <td>{{ $data->recording->name }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-primary" target="_blank" href="{{ $data->recording->playback->format->url }}">@lang('back.vcon_recording.open_button')</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-info text-center" style="margin-bottom: 0;">
                                    @lang('back.vcon_recording.not_available_notify')
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
