<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="title" class="control-label">@lang('back.vcon_form.room_label')</label>
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($vcon->title) ? $vcon->title : ''}}" placeholder="@lang('back.vcon_form.room_label')">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="password" class="control-label">@lang('back.vcon_form.password_label')</label>
        <input class="form-control" name="password" type="text" id="password" value="{{ isset($vcon->password) ? $vcon->password : ''}}" placeholder="@lang('back.vcon_form.password_label')">
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password_moderator') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="password_moderator" class="control-label">@lang('back.vcon_form.moderator_password_label')</label>
        <input class="form-control" name="password_moderator" type="text" id="password_moderator" value="{{ isset($vcon->password_moderator) ? $vcon->password_moderator : ''}}" placeholder="@lang('back.vcon_form.moderator_password_label')">
        {!! $errors->first('password_moderator', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="form-control" name="status" type="hidden" id="status" value="1" >

<div class="form-group {{ $errors->has('waiting') ? 'has-error' : ''}}">
    <div class="form-line">
        <label for="waiting" class="control-label">@lang('back.vcon_form.waiting_label')</label>
        <select name="waiting" id="" class="form-control">
            <option {{ isset($vcon->waiting) ? ($vcon->waiting == '0' ? 'selected' : '') : ''}} value="0">@lang('back.vcon_form.disabled_select')</option>
            <option {{ isset($vcon->waiting) ? ($vcon->waiting == '1' ? 'selected' : '') : ''}} value="1">@lang('back.vcon_form.enabled_select')</option>
        </select>
        {!! $errors->first('waiting', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="form-line">
        <label for="waiting" class="control-label">@lang('back.vcon_form.server_label')</label>
        <select name="vcon_setting_id" id="" class="form-control">
            @foreach($vcon_setting as $data)
                <option {{ isset($vcon->waiting) ?? $data->id == $vcon->vcon_setting_id ? 'selected' : ''}} value="{{$data->id}}">{{$data->base_url}}</option>
            @endforeach
        </select>
    </div>
</div>

<input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? Lang::get('back.vcon_form.update_button') : Lang::get('back.vcon_form.create_button') }}">
