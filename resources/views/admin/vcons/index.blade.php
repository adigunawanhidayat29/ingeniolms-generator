@extends('admin.layouts.app')

@section('title', Lang::get('back.vcon.title'))

@section('content')
    <section class="content">
        <div class="container-fluid">
            <center>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('success') !!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-error alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! Session::get('error') !!}
                    </div>
                @endif
            </center>
            
            <div class="block-header">
                <h2>@lang('back.vcon.header')</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/admin/vcons/create') }}" class="btn btn-primary btn-sm">@lang('back.vcon.create_button')</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('back.vcon.room_table')</th>
                                            <th>@lang('back.vcon.password_table')</th>
                                            <th>@lang('back.vcon.moderator_password_table')</th>
                                            <th>@lang('back.vcon.waiting_table')</th>
                                            <th>@lang('back.vcon.server_table')</th>
                                            <th>@lang('back.vcon.option_table')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($vcons as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->password }}</td>
                                                <td>{{ $item->password_moderator }}</td>
                                                <td>{{ $item->waiting == '1' ? Lang::get('back.vcon.enabled_waiting') : Lang::get('back.vcon.disabled_waiting') }}</td>
                                                <td>{{ $item->vcon_setting_id }}</td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" href="{{ url('/admin/vcons/recording/' . $item->id) }}">
                                                        <i class="material-icons">save</i><span>@lang('back.vcon.recording_button')</span>
                                                    </a>
                                                    <a class="btn btn-warning btn-sm" href="{{ url('/admin/vcons/' . $item->id . '/edit') }}">
                                                        <i class="material-icons">edit</i><span>@lang('back.vcon.edit_button')</span>
                                                    </a>

                                                    <form method="POST" action="{{ url('/admin/vcons' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i class="material-icons">delete</i><span>@lang('back.vcon.delete_button')</span>
                                                        </button>
                                                    </form>

                                                    @if($item->waiting == '1')
                                                        <a class="btn btn-sm bg-blue" href="{{ url('/admin/vcons/waiting/' . $item->id) }}">
                                                            <i class="material-icons">access_time</i><span>@lang('back.vcon.waiting_button')</span>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $vcons->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function(){
            var asset_url = '{{asset_url()}}';
            $("#data").DataTable();
        });
    </script>
@endpush