@extends('layouts.app')

@section('title', 'Menjadi Pengajar')

@section('content')
  <div class="container">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Pemberitahuan</h3>
      </div>
      <div class="panel-body">
        <p>Terimakasih sudah mendaftar menjadi pengajar di Ingenio. Mohon untuk menunggu proses verifikasi oleh admin selama kurang lebih 2 x 24 jam.</p>
      </div>
    </div>
  </div>
@endsection
