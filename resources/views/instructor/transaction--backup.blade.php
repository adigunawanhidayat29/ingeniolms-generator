@extends('layouts.app')

@section('title', 'Transaksi')

@push('style')

@endpush

@section('content')
  <div class="container">
    <h3>Transaksi</h3>saldo: {{rupiah($balance)}}
    <br>
    <a href="/instructor/transactions/mutation">Lihat Mutasi</a>
    <table class="table">
      <tr>
        <th>Invoice</th>
        <th>Course</th>
        <th>Customer</th>
        <th>Total</th>
        <th>Status</th>
      </tr>
      @foreach($transactions as $index => $transaction)
        <tr>
          <td>{{$transaction->invoice}}</td>
          <td>{{$transaction->title}}</td>
          <td>{{$transaction->name}}</td>
          <td>{{rupiah($transaction->total)}}</td>
          <td>{{$transaction->transaction_status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection
