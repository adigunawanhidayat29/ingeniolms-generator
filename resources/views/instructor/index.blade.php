@extends('layouts.app')

@section('title', 'Menjadi Pengajar')

@push('style')
<style media="screen">
    .header-instructor{
      background: url('/img/ingenio/img1.png') top no-repeat;
      background-size: cover;
      background-position: center center;
      min-height: 450px;
      margin-top: -40px;
    }
    .need-help{
      background: url('/img/ingenio/img2.jpg') top no-repeat;
      background-size: cover;
      background-attachment: fixed;
      color: white;
    }
    .need-help .dark-transparent{
      width: 100%;
      padding-top: 5rem;
      padding-bottom: 5rem;
      background-color: #00000080;
    }
  </style>

@endpush

@section('content')
  <div class="header-instructor">
        <div class="container">
          <div class="row">
            <div class="col-md-5 color-dark">
              <br><br><br>
              <h1 style="font-weight:bold">Berbagi Pengetahuan Anda</h1>
              <p>Kami membuka kesempatan untuk siapapun menjadi bagian penting dari perkembangan ilmu pengetahuan dan keterampilan di Indonesia. Bagi Anda yang memiliki pengetahuan dan keterampilan khusus dapat mengajukan diri menjadi instruktur di platform Ingenio</p>
              <br>
              <div class="">
                <a href="/instructor/join" class="btn btn-danger btn-raised btn-lg">Ajukan Diri Jadi Instruktur</a>
              </div>
            </div>
            <div class="col-md-7">

            </div>
          </div>
        </div>
      </div>

      <div class="container color-dark">
        <br>
        <h2 class="text-center" style="font-weight: bold;">Kesempatan emas menjadi pengajar</h2>
        <br>
        <div class="row">
          <div class="col-md-4 text-center">
            <img src="img/inspiration.png" height="100" alt="">
            <br><br>
            <strong style="font-size:20px">Menginspirasi</strong>
            <br><br>
            <p>
              Anda dapat membantu banyak orang dalam menguasai pengetahuan dan keterampilan yang Anda miliki saat ini. Jadilah seorang inspirator bagi ribuan orang di Indonesia.
            </p>
          </div>
          <div class="col-md-4 text-center">
            <img src="img/comunity.png" height="100" alt="">
            <br><br>
            <strong style="font-size:20px">Membangun Komunitas</strong>
            <br><br>
            <p>
              Dengan berbagi pengetahuan dan keterampilan yang Anda miliki, Anda dapat dengan mudah bertemu dengan orang-orang yang memiliki passion dan hobby yang sama sehingga memudahkan Anda membangun komunitas yang berharga.
            </p>
          </div>
          <div class="col-md-4 text-center">
            <img src="img/money.png" height="100" alt="">
            <br><br>
            <strong style="font-size:20px">Mendapatkan Uang</strong>
            <br><br>
            <p>
              Anda dapat membuat sumber pendapatan baru yang tidak terbatas dengan membagikan pengetahuan dan keterampilan di platform ini.
            </p>
          </div>
        </div>
      </div>

      <br><br>

      <div class="need-help">
        <div class="dark-transparent">
          <div class="container">
            <br>
            <h2 class="text-center" style="font-weight: bold;">Bagaimana cara memulai?</h2>
            <br>
            <div class="row">
              <div class="col-md-4 text-center">
                <div class="mb-2">
                  <img src="/img/ingenio/numb-1.png" alt="" width="75px">
                </div>
                <strong>Tentukan Judul</strong>
                <p>
                  Materi Tentukan judul materi yang akan Anda buat, bagi menjadi beberapa topik. Ribuan orang menunggu untuk belajar bersama Anda tentang topik-topik tersebut.
                </p>
              </div>
              <div class="col-md-4 text-center">
                <div class="mb-2">
                  <img src="/img/ingenio/numb-2.png" alt="" width="75px">
                </div>
                <strong>Buat Kelas</strong>
                <p>
                  Buat kelas di platform kami: rekam video Anda, unggah file-file pendukung & pelajari tips dan trik membuat kelas yang menarik
                </p>
              </div>
              <div class="col-md-4 text-center">
                <div class="mb-2">
                  <img src="/img/ingenio/numb-3.png" alt="" width="75px">
                </div>
                <strong>Bagikan dan Hasilkan Uang</strong>
                <p>
                  Bagikan kelas online Anda ke keluarga terdekat, rekan kerja, dan lingkungan terdekat Anda lainnya. Kelas yang berkualitas pada akhirnya akan dicari dan diikuti oleh banyak orang dan Anda dapat menghasilkan uang darinya
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container text-center color-dark">
        <br>
        <h2 class="text-center" style="font-weight: bold;">Kami Siap Membantu</h2>
        <br>
        <p>Anda masih memiliki kesulitan dalam memulai kelas online pertama Anda? silakan klik halaman ini dan tim kami akan dengan senang hati membantu mewujudkan kelas online pertama Anda bersama Ingenio</p>
      </div>

      <div class="container text-center">
        <br>
        <div class="">
          <a href="/instructor/join" class="btn btn-danger btn-raised btn-lg">Daftar Pengajar Hari ini</a>
        </div>
        <br><br>
      </div>


@endsection
