@extends('layouts.app')

@section('title', 'Mutasi')

@push('style')

@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>Mutasi</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Pengajar</a></li>
          <li><a href="#">Transaksi</a></li>
          <li>Mutasi</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      @if(Session::get('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
      @endif
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-sm no-m">Saldo: <span>{{rupiah($balance)}}</span></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
        <a data-target="#modalWithdrawalRequest" data-toggle="modal" class="btn btn-primary btn-raised" href="#">Buat Permintaan</a>
        <!-- <table class="table">
          <tr>
            <th>Tanggal</th>
            <th>Debit</th>
            <th>Kredit</th>
            <th>Informasi</th>
          </tr>
          @foreach($mutations as $index => $mutation)
            <tr>
              <td>{{$mutation->created_at}}</td>
              <td>{{$mutation->debit}}</td>
              <td>{{$mutation->credit}}</td>
              <td>{{$mutation->information}}</td>
            </tr>
          @endforeach
        </table>

        <hr>

        <h3>Riwayat permintaan penarikan</h3>
        <table class="table">
          <tr>
            <th>Tanggal</th>
            <th>Nominal</th>
            <th>Status</th>
            <th>#</th>
          </tr>
          @foreach($withdrawals as $withdrawal)
            <tr>
              <td>{{$withdrawal->created_at}}</td>
              <td>{{$withdrawal->amount}}</td>
              <td>{{$withdrawal->status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
              <td><a onclick="return confirm('Yakin akan menghapus data?')" href="/instructor/transactions/withdrawal/delete/{{$withdrawal->id}}">Hapus</a></td>
            </tr>
          @endforeach
        </table> -->

        <table class="table table-striped table-no-border mt-2">
          <tr>
            <td>
              <p class="no-m">Pembayaran untuk tagihan ABC</p>
              <b class="fs-12 fw-600" style="color:#ccc;">10 september 2018 10.59</b>
            </td>
            <td class="text-right fw-600">
              <p class="no-m">Rp 150.000</p>
              <b class="fs-12 fw-600" style="color:#ccc;">Saldo: Rp 50.000</b>
            </td>
          </tr>
          <tr>
            <td>
              <p class="no-m">Pembayaran untuk tagihan DEF</p>
              <b class="fs-12 fw-600" style="color:#ccc;">10 september 2018 10.59</b>
            </td>
            <td class="text-right fw-600">
              <p class="no-m">Rp 150.000</p>
              <b class="fs-12 fw-600" style="color:#ccc;">Saldo: Rp 50.000</b>
            </td>
          </tr>
          <tr>
            <td>
              <p class="no-m">Pembayaran untuk tagihan ABC</p>
              <b class="fs-12 fw-600" style="color:#ccc;">10 september 2018 10.59</b>
            </td>
            <td class="text-right fw-600">
              <p class="no-m">Rp 150.000</p>
              <b class="fs-12 fw-600" style="color:#ccc;">Saldo: Rp 50.000</b>
            </td>
          </tr>
          <tr>
            <td>
              <p class="no-m">Pembayaran untuk tagihan DEF</p>
              <b class="fs-12 fw-600" style="color:#ccc;">10 september 2018 10.59</b>
            </td>
            <td class="text-right fw-600">
              <p class="no-m">Rp 150.000</p>
              <b class="fs-12 fw-600" style="color:#ccc;">Saldo: Rp 50.000</b>
            </td>
          </tr>
        </table>

      </div>
    </div>
  </div>
</div>

  <div class="modal" id="modalWithdrawalRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title color-primary" id="myModalLabel">Permintaan Penarikan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
              <form class="" action="/instructor/transactions/withdrawal" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  {{-- <label for="">Jumlah</label> --}}
                  <input type="number" min="20000" max="{{$balance}}" class="form-control" name="amount" placeholder="Jumlah">
                </div>
                <input type="submit" value="Kirim" class="btn btn-primary btn-raised">
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
      </div>
  </div>
@endsection
