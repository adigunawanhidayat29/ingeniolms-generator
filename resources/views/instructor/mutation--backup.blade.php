@extends('layouts.app')

@section('title', 'Mutasi')

@push('style')

@endpush

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <h3>Mutasi</h3>saldo: {{rupiah($balance)}}
    <br>
    <a data-target="#modalWithdrawalRequest" data-toggle="modal" class="btn btn-primary btn-raised" href="#">Buat Permintaan</a>
    <table class="table">
      <tr>
        <th>Tanggal</th>
        <th>Debit</th>
        <th>Kredit</th>
        <th>Informasi</th>
      </tr>
      @foreach($mutations as $index => $mutation)
        <tr>
          <td>{{$mutation->created_at}}</td>
          <td>{{$mutation->debit}}</td>
          <td>{{$mutation->credit}}</td>
          <td>{{$mutation->information}}</td>
        </tr>
      @endforeach
    </table>

    <hr>

    <h3>Riwayat permintaan penarikan</h3>
    <table class="table">
      <tr>
        <th>Tanggal</th>
        <th>Nominal</th>
        <th>Status</th>
        <th>#</th>
      </tr>
      @foreach($withdrawals as $withdrawal)
        <tr>
          <td>{{$withdrawal->created_at}}</td>
          <td>{{$withdrawal->amount}}</td>
          <td>{{$withdrawal->status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
          <td><a onclick="return confirm('Yakin akan menghapus data?')" href="/instructor/transactions/withdrawal/delete/{{$withdrawal->id}}">Hapus</a></td>
        </tr>
      @endforeach
    </table>
  </div>

  <div class="modal" id="modalWithdrawalRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title color-primary" id="myModalLabel">Permintaan Penarikan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
              <form class="" action="/instructor/transactions/withdrawal" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  {{-- <label for="">Jumlah</label> --}}
                  <input type="number" min="20000" max="{{$balance}}" class="form-control" name="amount" placeholder="Jumlah">
                </div>
                <input type="submit" value="Kirim" class="btn btn-primary btn-raised">
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
      </div>
  </div>
@endsection
