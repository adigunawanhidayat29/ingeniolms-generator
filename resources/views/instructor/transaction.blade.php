@extends('layouts.app')

@section('title', 'Transaksi')

@push('style')

@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>Transaksi</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Pengajar</a></li>
          <li>Transaksi</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-sm no-m">Saldo: <span>{{rupiah($balance)}}</span></h2>
        </div>
      </div>
      <a class="btn btn-primary btn-raised" href="/instructor/transactions/mutation">Lihat Mutasi</a>
      <table class="table table-striped table-no-border mt-2">
        <tr class="bg-primary">
          <th>Invoice</th>
          <th>Course</th>
          <th>Customer</th>
          <th>Total</th>
          <th>Status</th>
        </tr>
        @foreach($transactions as $index => $transaction)
          <tr>
            <td class="fs-14">{{$transaction->invoice}}</td>
            <td class="fs-14">{{$transaction->title}}</td>
            <td class="fs-14">{{$transaction->name}}</td>
            <td class="fs-14">{{rupiah($transaction->total)}}</td>
            <td class="fs-14">{{$transaction->transaction_status == '1' ? 'Berhasil' : 'Menunggu'}}</td>
          </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
