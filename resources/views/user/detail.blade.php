@extends('layouts.app')

@section('title', $user->name)

@push('style')
  <style media="screen">
    h1, h2, h3, h4, h5, h6 {
      margin: 0;
    }

    hr {
      margin: 3rem 0;
    }

    .user-bg {
      background: #192a56;
      padding: 7px;
      min-height: 150px;
      color: white;
      margin-top: 0px;
    }

    .fa-socmed {
      padding: 7px;
      font-size: 26px;
    }

    .img-circle {
      border-radius: 50%;
    }

    .card .card-img-top {
      height: 170px;
      object-fit: cover;
    }

    .contributor {
      justify-content: center;
    }

    .headline-wrap>span {
      font-weight: 700; 
      color: #ccc;
      text-transform: uppercase;
    }

    .headline-wrap h1 {
      font-size: 30px;
      font-weight: 300;
      color: #424242;
      margin: 0;
    }

    .headline-wrap h1>span {
      font-weight: 700;
    }

    .profile-wrap,
    .courses-wrap {
      margin-top: 3rem;
    }

    .profile-wrap h2 {
      font-size: 20px; 
      font-weight: 700; 
      margin-bottom: 1rem;
    }

    .profile-wrap p {
      font-size: 16px; 
      margin-bottom: 0;
    }

    .courses-wrap h2 {
      font-size: 20px; 
      font-weight: 700; 
      margin-bottom: 2rem;
    }

    .courses-wrap .card-paper {
      margin-bottom: 2rem;
    }

    .profile-pict-wrap {
      width: 250px; 
      margin-left: auto; 
      text-align: center;
    }

    .profile-pict-wrap img {
      display: block; 
      width: 250px; 
      height: 250px; 
      object-fit: cover; 
      border-radius: 50%; 
      box-shadow: 1px 1px 20px rgb(0 0 0 / 10%); 
      margin-bottom: 2rem;
    }

    @media (max-width: 767px) {
      .contributor {
        flex-direction: column-reverse;
      }

      .courses-wrap .row {
        margin-left: 0;
        margin-right: 0;
      }

      .profile-pict-wrap {
        width: 150px; 
        margin-left: auto; 
        margin-right: auto;
        margin-bottom: 2rem;
      }

      .profile-pict-wrap img {
        width: 150px; 
        height: 150px; 
      }
    }
  </style>
  <link rel="stylesheet" href="{{asset('/sweetalert2/sweetalert2.min.css')}}">
@endpush

@section('content')
  {{-- <div class="user-bg">
    <div class="container">
      <br>
      <div class="row">
        <div class="col-md-2">
          <div class="text-center">
            <img class="dq-frame-round-md" height="130px" src="{{avatar($user->photo)}}"/>
            <a href="{{$user->facebook}}" class="btn-circle btn-facebook color-white"><i class="zmdi zmdi-facebook"></i></a>
            <a href="{{$user->twitter}}" class="btn-circle btn-twitter color-white"><i class="zmdi zmdi-twitter"></i></a>
            <a href="{{$user->instagram}}" class="btn-circle btn-instagram color-white"><i class="zmdi zmdi-instagram"></i></a>
            @if(Auth::check())
              @if(isAffiliate(Auth::user()->id) != false)
                <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy color-white" data-clipboard-text="{{ \Request::url() . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
              @endif
            @endif
          </div>
        </div>
        <div class="col-md-10">
          <h2 class="headline-md color-white no-m"><span>{{$user->name}}</span></h2>
          <p class="fs-18 fw-300 color-white">{!! $user->short_description !!}</p>
          <a data-toggle="modal" data-target="#modalPreviewDescription" class="btn btn-raised btn-primary btn-sm">@lang('front.contributor.btn_description_detail')</a>
        </div>
      </div>
    </div>
  </div> --}}

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/pengajar">Contributor</a></li>
          <li>{{$user->name}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap mb-2 bg-white">
    <div class="container">
      <div class="row contributor">
        <div class="col-md-6">
          <div class="headline-wrap">
            <span>Contributor</span>
            <h1><span>{{$user->name}}</span></h1>
          </div>

          <div class="profile-wrap">
            <h2>About</h2>
            {!! $user->short_description !!}
            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur faucibus euismod. Phasellus vel tincidunt felis. Sed sagittis odio at ipsum posuere tincidunt vel sit amet magna. Nam placerat ipsum mi, id consectetur augue pulvinar eu. Ut sem nulla, hendrerit id elit vel, aliquet convallis risus. Aenean urna augue, condimentum ac pellentesque id, sagittis aliquet erat. In eu quam a orci facilisis bibendum ut vel mauris. Etiam tincidunt fermentum risus ut scelerisque.</p> --}}
          </div>

          <div class="courses-wrap">
            <h2>Courses (2)</h2>
            <div class="row">
              @foreach($courses as $course)
                <div class="col-md-6 text-center">
                  <div class="card card-paper">
                    <img class="card-img-top" src="{{$course->image}}" alt="{{ $course->title }}">
                    <div class="card-body">
                      <h3 class="headline-sm text-truncate fs-16"><span>{{$course->title}}</span></h3>
                      <a class="btn btn-raised btn-primary" href="{{url('course/'. $course->slug)}}">
                        @lang('front.contributor.btn_detail') <i class="zmdi zmdi-arrow-right after-text"></i>
                      </a>
                    </div>
                  </div>
                </div>

                {{-- <div class="col-md-3 mb-2 {{$course->public == '1' ? 'public-course' : 'private-course'}}">
                  <div class="card-sm">
                    <a href="{{url('course/'. $course->slug)}}">
                      <img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
                    </a>
                    <a href="{{url('course/'. $course->slug)}}">
                      <div class="card-block text-left">
                        <span class="badge badge-primary">Lifetime</span>
                        <span class="text-right" style="background: orange; color: white; padding: 4px; font-weight: bold; border-radius: 32px; font-size: 12px">
                          {{$course->price == 0 ? 'Rp.0 (Gratis)' : "Rp. " . number_format($course->price)}}
                        </span>
                        <h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
                        <p>
                            <i class="fa fa-star color-warning"></i>
                            <i class="fa fa-star color-warning"></i>
                            <i class="fa fa-star color-warning"></i>
                            <i class="fa fa-star color-warning"></i>
                            <i class="fa fa-star color-warning"></i>
                          </p>
                        <p class="section-body-xs">
                          <span class="dq-max-subtitle">{{ $course->name }}</span>
                        </p>
                        <div class="m-0">
                          <button class="btn btn-raised btn-primary btn-block">@lang('front.contributor.btn_detail')</button>
                        </div>
                      </div>
                    </a>
                  </div>
                </div> --}}
              @endforeach
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="profile-pict-wrap">
            <img src="{{avatar($user->photo)}}" alt="">
            <a href="{{$user->facebook}}" class="btn-circle btn-circle-sm btn-circle-raised btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
            <a href="{{$user->twitter}}" class="btn-circle btn-circle-sm btn-circle-raised btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
            <a href="{{$user->instagram}}" class="btn-circle btn-circle-sm btn-circle-raised btn-instagram"><i class="zmdi zmdi-instagram"></i></a>
          </div>
        </div>
      </div>

      <!--Pagination Wrap Start-->
      <div class="text-center">
        {{$courses->links('pagination.default')}}
      </div>
      <!--Pagination Wrap End-->

      <!-- preview deskriopsi user -->
      <div class="modal" id="modalPreviewDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
              <div class="modal-content" style="background:linear-gradient(90deg, {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }} 0%, {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' }} 50%);">
                  <div class="modal-header">
                      <h3 class="modal-title color-white">@lang('front.contributor.about') {{$user->name}}</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="text-center">
                          <img class="dq-frame-round-md" height="130px" src="{{avatar($user->photo)}}"/>
                          <a href="{{$user->facebook}}" class="btn-circle btn-facebook color-white"><i class="zmdi zmdi-facebook"></i></a>
                          <a href="{{$user->twitter}}" class="btn-circle btn-twitter color-white"><i class="zmdi zmdi-twitter"></i></a>
                          <a href="{{$user->instagram}}" class="btn-circle btn-instagram color-white"><i class="zmdi zmdi-instagram"></i></a>
                          @if(Auth::check())
                            @if(isAffiliate(Auth::user()->id) != false)
                              <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy color-white" data-clipboard-text="{{ \Request::url() . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
                            @endif
                          @endif
                        </div>
                      </div>
                      <div class="col-md-10" style="color: white">
                        <h2 class="headline-md color-white no-m"><span>{{$user->name}}</span></h2>
                        <p class="fs-18 fw-300 color-white">{!!$user->short_description!!}</p>
                        <p class="fs-18 fw-300 color-white">{!!$user->description!!}</p>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- preview deskriopsi user -->

    </div>
  </div>
@endsection

@push('script')
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('clipboard/clipboard.min.js')}}"></script>

	<script type="text/javascript">
		var clipboard = new ClipboardJS('.btn-copy');
		clipboard.on('success', function(e) {
	    swal("Berhasil!", "Silakan bagikan link ini untuk referal Anda: " + e.text, "success");
	    e.clearSelection();
		});
	</script>
@endpush
