<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
  <div class="table-responsive">
    <h4>
      Data <b> {{$quiz->name}}</b>
    </h4>
    @if($quiz->quiz_questions)
    @php
      $questions = $quiz->quiz_questions;
    @endphp
    <table class="table table-hover">
      <tr class="bg-primary">
        <th rowspan="2">Peserta</th>
        <th colspan="{{count($questions)}}" style="text-align:center">Pertanyaan</th>
      </tr>
      <tr class="bg-primary">
        @foreach($questions as $question)
          @if(!in_array($question->quiz_type_id, ['14','13','4']))
            <th>{!!$question->question!!}</th>
          @endif
        @endforeach
      </tr>
      @if($quiz->patricipant)
      @php
        $participants = $quiz->patricipant;
      @endphp
      @foreach($participants as $participant)
        <tr class="bg-primary">
          <th>{{$participant->user->name}}</th>
          @foreach($questions as $quiz_participant)
            @if(!in_array($quiz_participant->quiz_type_id, ['14','13','4']))
              @php
               $answer = $participant->answer->where('quiz_question_id', $quiz_participant->id)->first();
              @endphp
              <th>
                @if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '7')
                  {!!$quiz_participant->answer!!}
                @elseif($quiz_participant->quiz_type_id == '3' || $quiz_participant->quiz_type_id == '9' || $quiz_participant->quiz_type_id == '12')
                  @php
                    $question_answers = DB::table('quiz_question_answers')
                      ->whereIn('id', explode(',', $answer->quiz_question_answer_id))
                      ->orderByRaw(DB::raw("FIELD(id, $answer->quiz_question_answer_id)"))
                      ->get();
                  @endphp
                  @foreach($question_answers as $index => $question_answer)
                    {{$question_answer->answer}} <br>
                  @endforeach
                @elseif(in_array($quiz_participant->quiz_type_id, ['5', '10', '11']))
                  {!!$answer->answer_essay!!}
                @elseif($quiz_participant->quiz_type_id == '8' )
                  @php
                    $question_answers_multi = DB::table('quiz_question_answers')
                      ->whereIn('id', explode(',', $answer->answer_essay))
                      ->get();
                  @endphp

                  <ul>
                    @foreach($question_answers_multi as $index => $question_answer)
                      <li>{{$question_answer->answer}}</li>
                    @endforeach
                  </ul>
                @elseif($quiz_participant->quiz_type_id == '6')
                  {!!$answer->answer_short_answer!!}
                @endif
              </th>
            @endif
          @endforeach
        </tr>
        @endforeach
      @endif
    </table>
  </div>
  @endif

</body>
</html>
