@extends('layouts.app')

@section('title', 'Course Grades')

@push('style')
  <style media="screen">
    .link-grade{
      text-decoration: underline;
    }
    .answer-correct{
      background: green;
      color: white;
    }
    .answer-incorrect{
      background: red;
      color: white;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Nilai <span>Peserta</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Pengajar</a></li>
          <li><a href="/course/preview/{{Request::segment(3)}}">Progress Peserta</a></li>
          <li><a href="/course/grades/{{Request::segment(3)}}">Buku Nilai</a></li>
          <li>Report Kuis</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Buku Nilai</h3>
			  </div>
			  <div class="panel-body" style="background:white">
					<table class="table table-bordered">
						<tr>
							<th>No</th>
							<th>Nama</th>
              @foreach($quiz_questions as $index => $quiz_question)
  							<th>Q{{$index+1}}</th>
              @endforeach
              <th>Total</th>
						</tr>

            @foreach($course_users as $index => $course_user)

              <tr>
                <td>{{$index +1}}</td>
                <td>{{$course_user->name}}</td>

                @foreach($quiz_questions as $quiz_question)
                  @php
                    $quiz_participant_answer = \DB::table('quiz_participant_answers')
                      ->join('quiz_participants', 'quiz_participants.id', '=', 'quiz_participant_answers.quiz_participant_id')
                      ->join('quiz_question_answers', 'quiz_question_answers.id', '=', 'quiz_participant_answers.quiz_question_answer_id')
                      ->where([
                        'quiz_participants.quiz_id' => Request::segment(5),
                        'quiz_participants.user_id' => $course_user->user_id,
                        'quiz_question_answers.quiz_question_id' => $quiz_question->id
                      ])
                      ->first();
                  @endphp
                  @if($quiz_participant_answer)
                    <td>{{$quiz_participant_answer->answer_correct == '1' ? 'v' : 'x'}}</td>
                  @else
                    <td>-</td>
                  @endif
                @endforeach

                @php
                  $quiz_participant = DB::table('quiz_participants')->where(['quiz_id' => Request::segment(5), 'user_id' => $course_user->user_id])->first();
                @endphp
                @if($quiz_participant)
                  <td>{{$quiz_participant->grade}}</td>
                @else
                  <td>0</td>
                @endif
              </tr>
            @endforeach

					</table>
			  </div>
			</div>
		</div>
	</div>

@endsection
