@extends('layouts.app')

@section('title')
	{{ 'Answer' }}
@endsection


@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Answer List</h3>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>Question</th>
                  <th>Answer</th>
									<th>Is Correct</th>
                </tr>
                @foreach($quiz_participants as $quiz_participant)
									<tr class="{{$quiz_participant->answer_correct == '1' ? 'success' : 'danger'}}">
										<td>{!!$quiz_participant->question!!}</td>
										<td>{!!$quiz_participant->answer!!}</td>
										<td>{{$quiz_participant->answer_correct == '1' ? 'Correct' : 'Incorrect'}}</td>
									</tr>
								@endforeach
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
@endpush
