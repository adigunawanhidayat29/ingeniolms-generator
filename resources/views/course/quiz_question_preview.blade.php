@extends('layouts.app')

@section('title', Lang::get('front.page_manage_preview_quiz.title') ." - ". $quiz->name)

@push('style')
  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
    .goto-link p{
      color: #333;
      margin-left: 0.5rem;
      margin-bottom: 0;
      overflow: hidden;
      min-height: 20px;
      display: -webkit-inline-box !important;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 1;
      white-space: normal;
    }

    .signature{
      width: 100%;
      height: auto;
      border: 1px solid black;
    }

    .list-group {
      border-left: none;
      border-right: none;
    }

    .card-quiz {
      border-radius: 0 0 5px 5px;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }

    .card-quiz .card-header {
      padding: 15px;
      background: #f6f6f6;
      border-bottom: 1px solid #ececec;
      margin-bottom: 0;
    }

    .card-quiz .card-header .card-title {
      margin: 0;
      font-size: 16px;
      font-weight: 400; /* Default 500 */
      color: #333;
      display: contents;
    }

    .card-quiz .card-header .card-title.question-group {
      display: flex;
      align-items: flex-start;
    }

    .card-quiz .card-header .card-title.question-group .question-number {
      width: 40px;
      height: 40px;
      background: #222;
      font-size: 16px;
      font-weight: 400;
      color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 7.5px;
    }

    .card-quiz .card-header .card-title.question-group .question {
      width: calc(100% - 40px);
      margin-left: 0.5rem;
    }

    .card-quiz .card-header .card-title.question-group .question * {
      font-size: 16px;
      margin-bottom: 0;
    }

    .card-quiz .card-block {
      padding: 15px;
      margin-bottom: -1rem;
    }

    .card-quiz .card-block p {
      font-size: 16px;
    }

    .card-quiz .card-block .form-group {
      padding-bottom: 0;
      margin: 15px 0 0;
    }

    .card-quiz .card-block table p {
      margin-bottom: 0;
    }

    .quiz-panel--desktop {
      display: block;
    }

    .quiz-panel--mobile {
      display: none;
    }
  </style>
@endpush

@section('content')
  <div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">@lang('front.page_manage_preview_content.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_preview_content.breadcumb_manage_courses')</a></li>
				  <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
				  <li><a href="/course/quiz/question/manage/{{$quiz->id}}">{{$quiz->name}}</a></li>
				  <li>@lang('front.page_manage_preview_quiz.title')</li>
				</ul>
			</div>
		</div>
	</div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          @foreach($quiz_page_breaks as $quiz_page_break)
            <h2 class="headline headline-md" style="margin-bottom: 1rem;"><span>{{$quiz_page_break->title}}</span></h2>
            <p class="fs-16">{{$quiz_page_break->description}}</p>
            <div id="question_content">
              @php $i =1; @endphp
              @foreach($quiz_page_break->quiz_questions as $key => $quiz_question)
                <div class="card-quiz" id="question_{{$quiz_question->id}}">
                  <div class="card-header">
                    <div class="card-title question-group">
                      @if($quiz_question->quiz_type_id != '4')
                        <div class="question-number">{{$key+1}}</div>
                      @endif
                      <div class="question">{!!$quiz_question->question!!}</div>
                    </div>
                  </div>
                  <div class="card-block">
                    @if($quiz_question->quiz_type_id != '4')
                      <label style="margin-bottom: 1rem;">@lang('front.page_manage_preview_quiz.answer_the_question_text')</label>
                    @endif
                    @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '9')
                      @php
                        $char = 'A'; $char <= 'Z';
                      @endphp
                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                        <div class="radio radio-primary">
                          <label style="color:#19191a">
                            <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">{{$char++}}. {!!$question_answer->answer!!}
                          </label>
                        </div>
                      @endforeach
                    @elseif($quiz_question->quiz_type_id == '7')
                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                        <div class="radio radio-primary form-inline mr-2">
                          <label class="pl-5" style="color:#19191a" title="{{$question_answer->answer == '1' ? 'sangat tidak setuju' : ($question_answer->answer == '2' ? 'tidak setuju' : ($question_answer->answer == '3' ? 'netral' : ($question_answer->answer == '4' ? 'setuju' : 'sangat setuju')))}}">
                            <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">
                            {!! $question_answer->answer !!}
                          </label>
                        </div>
                      @endforeach
                    @elseif($quiz_question->quiz_type_id == '3')
                      <div class="row">
                        <div class="col-md-2">
                          @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                            <div class="" style="cursor:pointer; background:#EEEEEE; padding:10px; margin: 7px 0; border-radius:5px">
                              {{$index+1}}
                            </div>
                          @endforeach
                        </div>
                        <div class="col-md-10">
                          <div class="SectionSortable" question-id="{{$quiz_question->id}}">
                            @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                              <li style="list-style:none; cursor:pointer; background:#EEEEEE; padding:10px; margin: 7px 0; border-radius:5px" data-id="{{$question_answer->id}}">
                                <p><i class="move fa fa-arrows-v"></i> {!!$question_answer->answer!!} ({{$index+1}})</p>
                              </li>
                            @endforeach
                            <input type="hidden" name="answer_ordering{{$quiz_question->id}}" value="" id="questionOrdering{{$quiz_question->id}}">
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '5')
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group m-0">
                            <textarea onkeyup="save_session_answer(this)" style="background: #fff; width: 100%; height: 100px; border: 1px solid #eee; border-radius: .5rem; padding: 7px" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '6')
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group m-0">
                            <input onkeyup="save_session_answer(this)" style="background: #fff; width: 100%; border: 1px solid #eee; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '10')
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group m-0">
                            <input onchange="save_session_answer(this)" style="background: #fff; width: 100%; border: 1px solid #eee; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="date" class="form-control">
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '11')
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group m-0">
                            <input onchange="save_session_answer(this)" style="background: #fff; width: 100%; border: 1px solid #eee; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="time" class="form-control">
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '12')
                      <select onchange="save_session_answer(this)" style="height: 40px; background: #fff; width: 100%; border: 1px solid #eee; padding: 0.5rem; margin-bottom: 1rem;" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" class="form-control" required>
                        <option value="">-@lang('front.page_manage_preview_quiz.select_answer')-</option>
                        @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                        <option value="{{$question_answer->id}}">{{$question_answer->answer}}</option>
                        @endforeach
                      </select>
                    @elseif($quiz_question->quiz_type_id == '13')
                      <div class="form-group m-0 row justify-content-end">
                        <label for="inputFile" class="col-lg-1 control-label" style="font-size:20px;line-height: 0.2;">File </label>
                        <div class="col-lg-11">
                          <input type="text" readonly="" class="form-control" style="background: #fff; width: 100%; border: 1px solid #eee; padding: 0.5rem; margin-bottom: 1rem;" placeholder="Browse...">
                          <input onchange="save_session_answer(this)" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="file" class="form-control file-survey">
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '14')
                      <!-- Signature area -->
                      <div class="signature"></div>
                      <br/>
                      <textarea class="signature-output" onchange="save_session_answer(this)" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" style="display:none"></textarea>
                    @elseif($quiz_question->quiz_type_id == '8')
                      @php
                        $char = 'A'; $char <= 'Z';
                      @endphp
                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                        <div style="margin:10px">
                          <label style="color:#19191a; margin-bottom: 1rem;">
                            <input id="answer_id_{{$question_answer->id}}" question-checkbox-type="checkbox{{$question_answer->quiz_question_id}}" type="checkbox" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}"> {!!$question_answer->answer!!}
                          </label>
                        </div>
                      @endforeach
                    @endif
                  </div>
                </div>
              @endforeach
            </div>
          @endforeach
        </div>

        <div class="col-md-4">
          <div class="card-quiz">
            <div class="card-header">
              <h3 class="card-title">@lang('front.page_manage_preview_quiz.select_question')</h3>
            </div>
            <div>
              <div class="list-group">
                <div class="text-center">
                  {{$quiz_page_breaks->links('pagination.default')}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script-preview')
  {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
  <script src="{{asset('js/jquery.paginate.min.js')}}"></script> --}}
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/jSignature/libs/jSignature.min.js')}}"></script>
  <script src="{{asset('plugins/jSignature/libs/modernizr.js')}}"></script>
  <script type="text/javascript" src="{{asset('plugins/jSignature/libs/flashcanvas.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  <!-- signature -->
  <script>
  // Initialize jSignature
  var $sigdiv = $(".signature").jSignature({'UndoButton':true});
  console.log($sigdiv);
  $(".signature").change(function(){
    // Get response of type image
    var data = $sigdiv.jSignature('getData', 'image');

    // Storing in textarea
    $(this).siblings('.signature-output').val("data:"+data);
    save_session_answer($(this).siblings('.signature-output')[0]);
  });

  $(".signature input").click(function(){
    // Get response of type image

    $(this).parents('.signature').siblings('.signature-output').val(null);
    save_session_answer($(this).parents('.signature').siblings('.signature-output')[0]);
  });

  </script>

  {{-- finish quiz --}}
  {{-- <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($quiz_questions_answers as $quiz_question_answer)
        @if($quiz_question_answer['quiz_type_id'] !=3)
          var answer_checked = $('input[name=answer{{$quiz_question_answer['id']}}]:checked').val();
          // check if answer is null
          if(answer_checked == null){
            answer_checked = 0;
          }else{
            answer_checked = answer_checked;
          }
          // check if answer is null
        @else
          var answer_checked = $('input[name=answer_ordering{{$quiz_question_answer['id']}}]').val();
        @endif

        answers.push(answer_checked);
        questions.push('{{$quiz_question_answer['id']}}');
        question_type.push('{{$quiz_question_answer['quiz_type_id']}}');
      @endforeach

      console.log(answers);

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('quiz/'.$quiz->id.'/finish')}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          _token: token
        },
        success : function(result){
          //remove session answer
          @foreach($quiz_questions_answers as $quiz_question_answer)
            localStorage.removeItem('answer{{$quiz_question_answer['id']}}');
            localStorage.removeItem('questionOrdering{{$quiz_question_answer['id']}}');
          @endforeach
          //remove session answer

          window.location.assign('{{url('quiz/'.$quiz->id.'/result')}}');
        },
      });
    }
    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script> --}}
  {{-- finish quiz --}}

  {{-- save session answer --}}

  <script type="text/javascript">
  @foreach($quiz_page_breaks as $quiz_page_break)
    @foreach($quiz_page_break->quiz_questions as $quiz_question)
      var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');

      @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}][value='"+session_value+"']").attr("checked","checked");
        }
      @elseif($quiz_question->quiz_type_id == '3')
        if(session_value !== null){
          $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '5')
        if(session_value !== null){
          $("textarea[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @elseif($quiz_question->quiz_type_id == '6')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @endif
    @endforeach
  @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}


  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script src="/jquery-ui/jquery.ui.touch-punch.min.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
