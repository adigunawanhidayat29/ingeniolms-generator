@extends('layouts.app')

@section('title', 'Kelas Saya')

@push('style')
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">
  <style media="screen">
    .swal2-container {
      z-index: 10000;
    }
		.filter button{
      width: 250px !important;
      display: block;
		}
    @media (min-width:768px) and (max-width:1023px){
      .filter button{
        width: 129px !important;
        display: block;
        /* color: #ffffff !important; */
      }
    }
    @media (max-width:767px){
      .filter button{
        width: 250px !important;
        display: block;
        /* color: #ffffff !important; */
      }
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Kelas <span>Saya</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li>Kelas Saya</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-right">
            <a href="#/" class="btn btn-primary btn-primary btn-raised" data-toggle="modal" data-target="#modalPrivateCourse"><i class="fa fa-plus"></i> Ikuti Kelas Private</a>
          </div>
          <div class="card-md radius-0">
            <div class="card-block">
              <div class="row">
                <div class="col-md-5">
                  <form class="form-group mt-1" action="" method="get">
                    <input value="{{Request::get('q')}}" type="text" name="q" class="form-control" placeholder="Search...">
                    {!!Request::get('q') ? '<a href="/my-course">Reset searcing</a>' : ''!!}
                  </form>
                </div>
                <div class="col-md-7">
                  <form class="form-inline" action="" method="get">
                    <div class="form-group mt-1">
                      <select class="form-control selectpicker no-m filter" name="filter">
                        <option value="">Filter by</option>
                        <option {{Request::get('filter') == 'title' ? 'selected' : ''}} value="title">Judul</option>
                        <option {{Request::get('filter') == 'created_at' ? 'selected' : ''}} value="created_at">date</option>
                      </select>
                    </div>
                    <div class="form-group mt-1">
                      <select class="form-control selectpicker no-m filter" name="sort">
                        <option value="">Urutkan</option>
                        <option {{Request::get('sort') == 'asc' ? 'selected' : ''}} value="asc">A-Z (Ascending)</option>
                        <option {{Request::get('sort') == 'desc' ? 'selected' : ''}} value="desc">Z-A (Descending)</option>
                      </select>
                    </div>
                    <input type="submit" class="btn btn-sm btn-primary btn-raised" value="Filter">
                  </form>
                </div>
              </div>
            </div>
          </div>

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link withoutripple active" href="#all" id="tabAllCourses" aria-controls="all" role="tab" data-toggle="tab">
								<i class="fa fa-list"></i>
								<span class="d-none d-sm-inline">Semua Kelas</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link withoutripple" href="#open-class" id="tabPublicCourses" aria-controls="open-class" role="tab" data-toggle="tab">
								<i class="fa fa-folder-open"></i>
								<span class="d-none d-sm-inline">Kelas Terbuka</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link withoutripple" href="#close-class" id="tabPrivateCourses" aria-controls="close-class" role="tab" data-toggle="tab">
								<i class="fa fa-folder"></i>
								<span class="d-none d-sm-inline">Kelas Tertutup</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link withoutripple" href="#archive" aria-controls="archive" role="tab" data-toggle="tab">
								<i class="fa fa-archive"></i>
								<span class="d-none d-sm-inline">Arsip</span>
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<!-- TAB SEMUA KELAS -->
						<div role="tabpanel" class="tab-pane active show" id="all">
              <br>
              <div class="row" id="appendAllCourse">
                @foreach($courses_datas as $course)
                  @php
                    $num_contents = $course['num_contents'];
                    //count percentage
                    $num_progress = 0;
                    $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
                    $percentage = 0;
                    $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
                    $percentage = $percentage == 0 ? 1 : $percentage ;
                    $percentage = 100 / $percentage;
                    //count percentage
                  @endphp
                  <div class="col-md-3 mb-2 {{$course['public'] == '1' ? 'public-course' : 'private-course'}}">
                    <div class="card-sm">
                      <a href="{{url('course/learn/'. $course['slug'])}}">
                        <img src="{{$course['image']}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
                      </a>
                      <a href="{{url('course/learn/'. $course['slug'])}}">
                        <div class="card-block text-left">
                          @if(is_liveCourse($course['id']) == true) <span class="badge badge-danger">Live</span> @endif
  		                    @if($course['public'] == '0') <span class="badge badge-warning">Tertutup</span> @endif
  		                    @if($course['model'] == 'batch') <span class="badge badge-success">Terjadwal</span> @endif
  												@if($course['model'] == 'subscription') <span class="badge badge-primary">Lifetime</span> @endif
                          <h4 class="headline-xs dq-max-title">{{$course['title']}}</h4>
                          <p class="section-body-xs">
                            <span class="dq-max-subtitle">{{ $course['name'] }}</span>
                          </p>
                          <div class="m-0">
                            <div class="progress">
                              <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
                            </div>
                            <span class="dq-span-border">
                              {{$percentage > 1 ? 'LANJUTKAN' : 'MULAI'}}
                            </span>
                            <br><br>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                @endforeach
              </div>
              @if(count($courses_datas) > 8)
                <div class="text-center">
                  <button type="button" class="btn btn-raised btn-primary" data-title="allCourse" data-offset="" id="loadMoreAllCourse">Load More</button>
                </div>
              @endif
            </div>

            <!-- TAB Kelas Terbuka -->
            <div role="tabpanel" class="tab-pane" id="open-class">
              <br>
              <div class="row" id="publicCourses">

              </div>
            </div>

            <!-- TAB Kelas Tertutup -->
            <div role="tabpanel" class="tab-pane" id="close-class">
              <br>
              <div class="row" id="privateCourses">

              </div>
            </div>

            <!-- TAB Kelas Arsip -->
            <div role="tabpanel" class="tab-pane" id="archive">
              <br>
              <div class="row">
                @foreach($course_archives as $course)

                @php
                  $num_contents = $course['num_contents'];
                  //count percentage
                  $num_progress = 0;
                  $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
                  $percentage = 0;
                  $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
                  $percentage = $percentage == 0 ? 1 : $percentage ;
                  $percentage = 100 / $percentage;
                  //count percentage
                @endphp

                <div class="col-md-3 mb-2">
                  <div class="card-sm">
                    <a href="{{url('course/learn/'. $course['slug'])}}">
                      <img src="{{$course['image']}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
                    </a>
                    <a href="{{url('course/'. $course['slug'])}}">
                      <div class="card-block text-left">
                        @if(is_liveCourse($course['id']) == true) <span class="badge badge-danger">Live</span> @endif
		                    @if($course['public'] == '0') <span class="badge badge-warning">Tertutup</span> @endif
		                    @if($course['model'] == 'batch') <span class="badge badge-success">Terjadwal</span> @endif
												@if($course['model'] == 'subscription') <span class="badge badge-primary">Lifetime</span> @endif
                        <h4 class="headline-xs dq-max-title">{{$course['title']}}</h4>
                        <p class="section-body-xs">
                          <span class="dq-max-subtitle">{{ $course['name'] }}</span>
                        </p>
                        <div class="m-0">
                          <div class="progress">
                            <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
                          </div>
                          <span class="color-primary dq-span-border">
                            {{$percentage > 1 ? 'LANJUTKAN' : 'MULAI'}}
                          </span>
                          <br><br>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal" id="modalPrivateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password / Kode</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password / Kode</label>
                  <input type="text" class="form-control" id="CoursePassword" placeholder="Masukan Password / Kode">
                </div>
              </div>
              <div class="modal-footer">
                <button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">Gabung</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->
@endsection

@push('script')
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/course/join-private-course',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          console.log(response);
          if(response.code == 200){
            $.ajax({
              url : '/course/enroll',
              type : 'get',
              data : {
                id : response.data.id,
              },
              success : function(data){
                swal({
                  type: 'success',
                  title: 'Berhasil...',
                  html: response.message,
                }).then(function() {
                    window.location.assign('/course/learn/' + response.data.slug)
                });
              }
            })
          }else{
            swal({
              type: 'error',
              title: 'Gagal!',
              html: response.message,
            })
          }
        }
      })
    })
  </script>

  <script type="text/javascript">
    var allCourseLimited = 4;
    var allCourseTotal = {{$courses_datas_count}}; // total data
    var allCourseLoaded = 12; // limit data
    $("#loadMoreAllCourse").attr('data-offset', allCourseLoaded);

    $("#loadMoreAllCourse").click(function(){
      var offset = $(this).attr('data-offset')
      // alert(offset);
      $.ajax({
        url: '/course/my/loadmore',
        type: 'get',
        data: {
          offset: offset
        },
        beforeSend: function(){
          $("#loadMoreAllCourse").html('<i class="fa fa-circle-o-notch fa-spin"></i> Memuat');
        },
        success: function(response){
          $("#loadMoreAllCourse").html('Load More')
          $("#appendAllCourse").append(response);
          $("#loadMoreAllCourse").attr('data-offset', parseInt(offset) + allCourseLimited)
          parseInt(allCourseLoaded += allCourseLimited);

          if(allCourseLoaded >= allCourseTotal){
            $("#loadMoreAllCourse").hide()
          }

        }
      })
    })
  </script>

  <script type="text/javascript">
    $("#tabAllCourses").click(function(){
      $( ".public-course" ).show()
      $( ".private-course" ).show()
    })

    $("#tabPublicCourses").click(function(){
      $( ".public-course" ).show()
      $( ".private-course" ).hide()

      $("#publicCourses").html('');
      $( ".public-course" ).clone().appendTo("#publicCourses")
    })

    $("#tabPrivateCourses").click(function(){
      $( ".public-course" ).hide()
      $( ".private-course" ).show()

      $("#privateCourses").html('');
      $( ".private-course" ).clone().appendTo("#privateCourses")
    })
  </script>

  @if(\Session::has('success'))
    <script type="text/javascript">
      swal({
        type: 'success',
        title: 'Berhasil',
        html: '{{\Session::get('success')}}',
      })
    </script>
  @endif
@endpush
