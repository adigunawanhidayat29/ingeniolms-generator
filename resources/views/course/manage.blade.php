@extends('layouts.app')

@section('title', 'Course Dashboard')

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Manage Course - {{$title}}</h3>
					</div>
					<div class="box-body">
						<center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						<div class="panel panel-primary">
						  <div class="panel-heading">
						    <h3 class="panel-title">Course - {{$title}}</h3>
						  </div>
						  <div class="panel-body" style="background:white">
								{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

									<div class="form-group">
										<label for="title">Title</label>
										<input placeholder="Title" type="text" class="form-control" name="title" value="{{ $title }}" required>
									</div>

									<div class="form-group">
										<label for="title">Goal</label>
										<textarea name="goal" id="goal">{{ $goal }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Description</label>
										<textarea name="description" id="description">{{ $description }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Image</label>
										<br>
										<img src="{{asset_url($image)}}" alt="{{$title}}" height="90" />
										<br><br>
										<input type="text" readonly="" class="form-control" placeholder="Browse...">
										<input type="file" name="image">
										<input type="hidden" name="old_image" value="{{ $image }}">
									</div>

									<div class="form-group">
										<label for="title">Video Introduction</label>
										@if($introduction != NULL)
											<br>
			                <iframe class="embed-responsive-item" src="{{$introduction.'/preview'}}" height="200"></iframe>
			                <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
											<br>
										@endif
										<input type="text" readonly="" class="form-control" placeholder="Browse...">
										<input type="file" name="introduction">
										<input type="hidden" name="old_introduction" value="{{ $introduction }}">
									</div>

									<div class="form-group">
										<label for="price">Price</label>
										<input placeholder="Price" type="number" class="form-control" name="price" value="{{ $price }}" required>
									</div>

									<div class="form-group">
										<label for="price">Model</label>
										<select class="form-control selectpicker" name="model" required>
											<option {{ $model == 'subscription' ? 'selected' : '' }} value="subscription">Subscription</option>
											<option {{ $model == 'batch' ? 'selected' : '' }} value="batch">Batch</option>
											<option {{ $model == 'live' ? 'selected' : '' }} value="live">Live</option>
										</select>
									</div>

									<div class="form-group">
										<label for="name">Category</label>
										<select class="form-control" name="id_category" id="id_category" required>
											<option value="">Choose Category</option>
											@foreach($categories as $category)
												<option {{ $category->id == $id_category ? 'selected' : '' }} value="{{$category->id}}">{{$category->title}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										<label for="name">Level</label>
										<select class="form-control selectpicker" name="id_level_course"  required>
											<option value="">Choose Level</option>
											@foreach($course_levels as $course_level)
												<option {{ $course_level->id == $id_level_course ? 'selected' : '' }} value="{{$course_level->id}}">{{$course_level->title}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit("Save Course" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
										{{-- <a href="/course/duplicate/{{$id}}" class="btn btn-info">Duplicate Course</a> --}}
										<a href="{{ url('course/dashboard') }}" class="btn btn-default">Back</a>
									</div>

								{{ Form::close() }}
						  </div>
						</div>

						<div id="place_new_section">
							<div class="panel-group" id="sortable">
								@foreach($sections as $section)
									<li style="list-style:none" id="{{$section['id']}}">
										<div class="panel panel-info">
											<div class="panel-heading">
												<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$section['id']}}">
														<h4 class="panel-title">
																{{$section['title']}}
														</h4>
												</a>
											</div>
											<div id="collapse{{$section['id']}}" class="panel-collapse collapse">
												<div class="panel-body" style="background:white">
													{{ Form::open(array('url' => '/course/section/update_action/'.$section['id'], 'method' => 'put')) }}
													<div class="form-group">
														<label for="title">Title</label>
														<input placeholder="Section Title" type="text" class="form-control" name="section_title" value="{{$section['title']}}" required>
													</div>
													<div class="form-group">
														<label for="description">Description</label>
														<textarea name="section_description" class="form-control" rows="8" cols="40">{{$section['description']}}</textarea>
													</div>
													<div class="form-group">
														<label for="title">Sequence</label>
														<input placeholder="Section Sequence" type="number" class="form-control" name="section_sequence" value="{{$section['sequence']}}" required>
													</div>
													<input type="submit" name="submit" value="Update Section" class="btn btn-warning btn-raised">
													<a onclick="return confirm('are you sure?')" class="btn btn-danger btn-raised" href="{{url('course/section/delete/'.$id.'/'.$section['id'])}}">Delete {{$section['title']}}</a>
													{{ Form::close() }}

													<br>
														<div class="panel panel-default">
															<div class="panel-heading">
																<h3 class="panel-title">Content</h3>
															</div>
															<div class="panel-body">
																<a href="{{url('course/content/create/'. $section['id'])}}" class="btn btn-default">Add New Content</a>
																<br><br>
																<div id="">
																	@foreach($section['section_contents'] as $content)
																		<a id="{{$content->type_content == 'document' ? 'show_document' : 'show_video' }}" style="cursor:pointer" data-source="{{$content->full_path_file.'/preview'}}" data-title="{{$content->title}}">
																			<i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}
																		</a> |
																		<a class="text-warning" href="{{url('course/content/update/'.$content->id)}}">Edit</a> |
																		<a onclick="return confirm('are you sure?')" class="text-danger" href="{{url('course/content/delete/'.$id.'/'.$content->id)}}">Delete</a>
																		<br>
																	@endforeach
																</div>
															</div>
														</div>

														<br>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h3 class="panel-title">Quiz</h3>
																</div>
																<div class="panel-body">
																	<a href="{{url('course/quiz/create/'. $section['id'])}}" class="btn btn-primary btn-raised">Add New Quiz</a>
																	<br>
																	<table class="table">
																		@foreach($section['section_quizzes'] as $quiz)
																			<tr>
																				<td><a class="badge" href="#">{{$quiz->name}}</a></td>
																				<td>
																					{{ Form::open(['url' => 'course/quiz/publish/'.$quiz->id, 'method' => 'put']) }}
																						<a class="btn-info btn-sm btn" href="{{url('course/quiz/question/manage/'.$quiz->id)}}">Manage Quiz</a>
																						<input onclick="return confirm('are you sure?')" value="{{$quiz->status == '1' ? 'Published' : 'Publish Quiz'}}" type="submit" class="btn-success btn-sm btn">
																						@if($quiz->status != '1')
																							<a class="btn-warning btn-sm btn" href="{{url('course/quiz/update/'.$quiz->id)}}">Edit</a>
																							<a onclick="return confirm('are you sure?')" class="btn-danger btn-sm btn" href="{{url('course/quiz/delete/'.$id.'/'.$quiz->id)}}">Delete</a>
																						@endif()
																					{{ Form::close() }}
																				</td>
																			</tr>
																		@endforeach
																	</table>
																</div>
															</div>

															<br>
																<div class="panel panel-default">
																	<div class="panel-heading">
																		<h3 class="panel-title">Assignment</h3>
																	</div>
																	<div class="panel-body">
																		<a href="{{url('course/assignment/create/'. $section['id'])}}" class="btn btn-default">Add New Assignment</a>
																		<br><br>
																		<table class="table">
																			@foreach($section['section_assignments'] as $Assignment)
																				<tr>
																					<td><a class="badge" href="#">{{$Assignment->title}}</a></td>
																					<td>
																						{{ Form::open(['url' => 'course/assignment/publish/'.$Assignment->id, 'method' => 'put']) }}
																							<input onclick="return confirm('are you sure?')" value="{{$Assignment->status == '1' ? 'Published' : 'Publish Assignment'}}" type="submit" class="btn-success btn-sm btn">
																							<a class="btn btn-default btn-sm" href="/course/assignment/answer/{{$Assignment->id}}">View Answer</a>
																							@if($Assignment->status != '1')
																								<a class="btn-warning btn-sm btn" href="{{url('course/assignment/update/'.$Assignment->id)}}">Edit</a>
																								<a onclick="return confirm('are you sure?')" class="btn-danger btn-sm btn" href="{{url('course/assignment/delete/'.$id.'/'.$Assignment->id)}}">Delete</a>
																							@endif()
																						{{ Form::close() }}
																					</td>
																				</tr>
																			@endforeach
																		</table>
																	</div>
																</div>
												</div>
											</div>
										</div>
									</li>
								@endforeach
							</div>

						</div>

						<button type="button" id="add_new_section" class="btn btn-info btn-raised">Add New Section</button>
						<input type="hidden" name="name" value="0" id="section_row">
						<br><br>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	{{-- show video --}}
  <div class="modal fade" id="modal_show_video" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="video_title"></h4>
        </div>
        <div class="modal-body">
          <iframe id="iframe_src" src="" style="width:100%; height:500px;"></iframe>
          {{-- <video id="video" style="width:100%; height:100%;" controls>
            <source id="iframe_src" src="" type="video/mp4">
          </video> --}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  {{-- show video --}}

  {{-- show document --}}
  <div class="modal fade" id="modal_show_document" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="document_title"></h4>
        </div>
        <div class="modal-body">
          {{-- <object id="document_src" type="application/pdf" data="" style="width:100%; height:500px;"></object> --}}
					<iframe id="document_src" src="" style="width:100%; height:500px;"></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  {{-- show document --}}

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
	  CKEDITOR.replace('goal');
		CKEDITOR.replace('description');
		CKEDITOR.replace('section_description');
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#id_category").select2();
		$("#id_author").select2();
	</script>
	{{-- SELECT2 --}}

	{{-- add new section --}}
	<script type="text/javascript">
		$("#add_new_section").click(function(){

			$("#section_row").val(parseInt($("#section_row").val())+1)
			var section_add_row = $("#section_row").val();

			$("#place_new_section").append(
				'<div class="panel panel-info" id="section_row_'+section_add_row+'">'+
					'<div class="panel-heading">'+
						'<h3 class="panel-title">Add Section</h3>'+
					'</div>'+
					'<div class="panel-body">'+
						'<div class="form-group">'+
							'<label for="title">Title</label>'+
							'<input placeholder="Section Title" type="text" class="form-control" id="new_section_title_'+section_add_row+'" value="" required>'+
						'</div>'+
							'<button type="button" id="save_new_section" data-id="'+section_add_row+'" class="btn btn-primary">Save Section</button> '+
							'<button type="button" id="remove_new_section" data-id="'+section_add_row+'" class="btn btn-danger">Remove Section</button>'+
					'</div>'+
				'</div>'
			);
		});
	</script>
	{{-- add new section --}}

	{{-- save new section --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#save_new_section',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					var new_section_title = $("#new_section_title_"+data_id).val();

					var id_course = '{{$id}}';
					var token = '{{ csrf_token() }}';
					$.ajax({
            url : "{{url('course/save_section')}}",
            type : "POST",
            data : {
							title : new_section_title,
							id_course : id_course,
							_token: token
            },
            success : function(result){
              location.reload();
            },
          });
			});
		});
	</script>
	{{-- save new section --}}

	{{-- remove new section --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#remove_new_section',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					$("#section_row_"+data_id).remove();
			});
		});
	</script>
	{{-- remove new section --}}

	{{-- show content --}}
	  <script type="text/javascript">
	  // show video
	  $(function(){
	    $(document).on('click','#show_video',function(e){
	      e.preventDefault();
	        var data_source = $(this).attr('data-source');
					var data_title = $(this).attr('data-title');
	        $("#iframe_src").attr('src', data_source);
	        $("#modal_show_video").modal('show');
					$("#video_title").html(data_title);
	    });
	  });

	  $('#modal_show_video').modal({
	      show: false
	    }).on('hidden.bs.modal', function(){
	      $("#modal_show_video iframe").attr("src", $("#modal_show_video iframe").attr("src"));
	    });
	  // show video

	  // show document
	  $(function(){
	    $(document).on('click','#show_document',function(e){
	      e.preventDefault();
	        var data_source = $(this).attr('data-source');
					var data_title = $(this).attr('data-title');
	        $("#document_src").attr('src', data_source);
	        $("#modal_show_document").modal('show');
					$("#document_title").html(data_title);
	    });
	  });
	  // show document
	  </script>
		{{-- show content --}}

		{{-- sortable --}}
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
		  $( function() {
		    $( "#sortable" ).sortable({
					axis: 'y',
	        update: function(event, ui) {
						var sequence = ui.item.index();
						var section_id = $('#sortable li:nth-child(' + (ui.item.index() + 1) + ')')[0].id;
						$.ajax({
							url : '/course/section/update-sequence',
							type : 'POST',
							data : {
								'sequence' : sequence,
								'section_id' : section_id,
								'_token' : '{{csrf_token()}}'
							},
							success : function(){
							}
						})
	        }
				});
		  } );
	  </script>

		<script>
		  $( function() {
		    $( "#sortableContent" ).sortable({});
		  } );
	  </script>
		{{-- sortable --}}

@endpush
