@extends('layouts.app')

@section('title', $quiz->name)

@push('style')
  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
    .goto-link p{
      color: #333;
      margin-left: 0.5rem;
      margin-bottom: 0;
      overflow: hidden;
      min-height: 20px;
      display: -webkit-inline-box !important;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 1;
      white-space: normal;
    }
  </style>
@endpush

@section('content')
  <div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="#/">Pengajar</a></li>
				  <li><a href="/course/dashboard">Kelola Kelas</a></li>
				  <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
				  <li><a href="/course/quiz/question/manage/{{$quiz->id}}">Manage {{$quiz->name}}</a></li>
				  <li>Preview Kuis - {{$quiz->name}}</li>
				</ul>
			</div>
		</div>
	</div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div id="question_content">
            @php $i =1; @endphp
            @foreach($quiz_questions_answers as $quiz_question_answer)
              <div class="card-quiz" id="question_{{$quiz_question_answer['id']}}">
                <div class="card-header">
                  <h3 class="card-title"><span class="badge">{{$i++}}</span> {!!$quiz_question_answer['question']!!}</h3>
                </div>
                <div class="card-block">
                  @if($quiz_question_answer['quiz_type_id'] != '3')
                    @php
                      $char = 'A'; $char <= 'Z';
                    @endphp
                    @foreach($quiz_question_answer['question_answers'] as $question_answer)
                      <div class="radio radio-primary">
                        <label style="color:#19191a">
                          <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">{{$char++}}. {!!$question_answer->answer!!}
                        </label>
                      </div>
                    @endforeach
                  @else
                    <div class="SectionSortable" question-id="{{$quiz_question_answer['id']}}">
                      @foreach($quiz_question_answer['question_answers'] as $question_answer)
                        <li style="list-style:none; cursor:pointer; background:#EEEEEE; padding:10px; margin: 7px 0; border-radius:5px" data-id="{{$question_answer->id}}">
                          <p><i class="move fa fa-arrows-v"></i> {!!$question_answer->answer!!}</p>
                        </li>
                      @endforeach
                      <input type="hidden" name="answer_ordering{{$quiz_question_answer['id']}}" value="" id="questionOrdering{{$quiz_question_answer['id']}}">
                    </div>
                  @endif
                </div>
              </div>
            @endforeach
          </div>

          {{-- <ul class="pagination" id="question_content-pagination">
            <li><a id="question_content-previous" href="#">&laquo; Previous</a></li>
            <li><a id="question_content-next" href="#">Next &raquo;</a></li>
          </ul> --}}

        </div>
        <div class="col-md-4">
          <div class="">
            <div class="list-group">
              <a href="#" class="list-group-item active">
                Go to Question
              </a>
              @php $i =1; @endphp
              @foreach($quiz_questions_answers as $quiz_question_answer)
                <a href="#question_{{$quiz_question_answer['id']}}" class="list-group-item goto-link"><span class="badge">{{$i++}}</span> <p>{!!$quiz_question_answer['question']!!}</p></a>
              @endforeach
            </div>
            {{-- <button id="finish_quiz" type="button" class="btn btn-primary btn-lg btn-block btn-raised">Finish Quiz</button> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
  <script src="{{asset('js/jquery.paginate.min.js')}}"></script> --}}
  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  {{-- finish quiz --}}
  <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($quiz_questions_answers as $quiz_question_answer)
        @if($quiz_question_answer['quiz_type_id'] !=3)
          var answer_checked = $('input[name=answer{{$quiz_question_answer['id']}}]:checked').val();
          // check if answer is null
          if(answer_checked == null){
            answer_checked = 0;
          }else{
            answer_checked = answer_checked;
          }
          // check if answer is null
        @else
          var answer_checked = $('input[name=answer_ordering{{$quiz_question_answer['id']}}]').val();
        @endif

        answers.push(answer_checked);
        questions.push('{{$quiz_question_answer['id']}}');
        question_type.push('{{$quiz_question_answer['quiz_type_id']}}');
      @endforeach

      console.log(answers);

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('quiz/'.$quiz->id.'/finish')}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          _token: token
        },
        success : function(result){
          //remove session answer
          @foreach($quiz_questions_answers as $quiz_question_answer)
            localStorage.removeItem('answer{{$quiz_question_answer['id']}}');
            localStorage.removeItem('questionOrdering{{$quiz_question_answer['id']}}');
          @endforeach
          //remove session answer

          window.location.assign('{{url('quiz/'.$quiz->id.'/result')}}');
        },
      });
    }
    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
    @foreach($quiz_questions_answers as $quiz_question_answer)
      @foreach($quiz_question_answer['question_answers'] as $question_answer)
        var session_value = get_session_answer('{{'answer'.$quiz_question_answer['id']}}');

        @if($quiz_question_answer['quiz_type_id'] != 3)
          if(session_value !== null){
            $("input[name=answer{{$quiz_question_answer['id']}}][value='"+session_value+"']").attr("checked","checked");
          }
        @else
          if(session_value !== null){
            $("#questionOrdering{{$quiz_question_answer['id']}}").val(get_session_answer("questionOrdering{{$quiz_question_answer['id']}}"));
          }
        @endif
      @endforeach
    @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}


  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
