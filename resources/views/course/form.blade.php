@extends('layouts.app')

@section('title', 'Buat Kelas Baru')

@push('style')
	<link rel="stylesheet" href="{{asset('select2/css/select2.min.css')}}">
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
	<style>
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input {
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}

	.onoff input{
		margin: 6px;
	  margin-right: 5px;
	}

	.onoff input:last-child{
		margin: 6px;
		margin-right: 5px;
	  margin-left: 20px;
	}
</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Buat <span>Kelas</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Pengajar</a></li>
          <li>Buat Kelas</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">
							@php
								if(Session::get('error')){
									$errors = Session::get('error');
								}
							@endphp

							@if(Session::get('message'))
								<div class="alert alert-warning"><i class="fa fa-info-circle"></i> Kelas privat terbatas. <a style="text-decoration:underline" href="#"><strong>Upgrade akun Anda</strong></a> untuk mendapatkan kelas tanpa batas</div>
							@endif

							<div class="card-md">
							  <!-- <div class="panel-heading">
							    <h3 class="panel-title">Buat Kelas {{Request::get('live') ? 'Live' : ''}}</h3>
							  </div> -->
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true, 'id' => 'formCreate')) }}

										<div class="form-group">
											<label for="title">Judul</label>
											<input placeholder="Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
											@if ($errors->has('title'))
												<span class="text-danger">{{ $errors->first('title') }}</span>
											@endif
                    </div>

                    <div class="form-group">
											<label for="title">Sub Judul</label>
											<input placeholder="Sub Judul" type="text" class="form-control" name="subtitle" value="{{ $subtitle }}">
											@if ($errors->has('subtitle'))
												<span class="text-danger">{{ $errors->first('subtitle') }}</span>
											@endif
										</div>

                    <div class="form-group">
											<label for="name">Kategori</label>
											<select class="form-control" name="id_category" id="id_category" required>
												<option value="">Pilih Kategori</option>
												@foreach($categories as $category)
                          @php
                            $ParentCategory = \DB::table('categories')->where('id', $category->parent_category)->first();
                          @endphp
													<option {{ $category->id == $id_category ? 'selected' : '' }} value="{{$category->id}}">{{isset($ParentCategory) ? $ParentCategory->title . ' > ' : ''}} {{$category->title}}</option>
												@endforeach()
											</select>
                    </div>

                    <div class="form-group">
											<label for="title">Gambar</label>
											<input type="text" readonly="" class="form-control" placeholder="Pilih...">
											<input type="file" class="form-control" name="image" accept="image/*">
											<input type="hidden" name="old_image" value="{{ $image }}">
                    </div>

                    {{-- <div class="form-group">
											<label for="name">Pilih Pengajar</label>
											<select class="form-control" name="id_author[]" id="id_author" multiple>
												@foreach($authors as $author)
													<option {{ $author->id == $id_author ? 'selected' : '' }} value="{{$author->id}}">{{$author->name}}</option>
												@endforeach()
											</select>
										</div> --}}

										{{-- <div class="form-group">
					            <label class="">Terjadwal</label>
											<div class="radio radio-primary">
												<label>
													<input type="radio" name="is_batch" value="1"> Ya
												</label>
											</div>
											<div class="radio radio-primary">
												<label>
													<input type="radio" name="is_batch" value="0" checked=""> Tidak
												</label>
											</div>
						        </div> --}}

										{{-- <div id="div_is_batch">
											<div class="form-group">
												<label for="name">Jumlah Pertemuan</label>
												<select class="form-control selectpicker" name="meeting_total">
													<option value="">Pilih Pertemuan</option>
													@for ($i=1; $i <= 20; $i++)
														<option value="{{$i}}">{{$i}}</option>
													@endfor
												</select>
											</div>
											<div class="form-group">
												<label for="name">Kuota Peserta</label>
												<select class="form-control selectpicker" name="participant_total">
													<option value="">Pilih</option>
													<option value="5">5</option>
													<option value="10">10</option>
													<option value="25">25</option>
													<option value="50">50</option>
													<option value="100">100</option>
												</select>
											</div>
											<div class="form-group">
												<label for="title">Tanggal Mulai</label>
												<input placeholder="Tanggal Mulai" type="text" id="datePicker1" class="form-control" name="start_date">
											</div>
											<div class="form-group">
												<label for="title">Tanggal Akhir</label>
												<input placeholder="Tanggal Akhir" type="text" id="datePicker2" class="form-control" name="end_date">
											</div>
											<div class="form-group">
						            <label class="">Live</label>
												<div class="radio radio-primary">
													<label>
														<input type="radio" name="is_live" value="1"> Ya
													</label>
												</div>
												<div class="radio radio-primary">
													<label>
														<input type="radio" name="is_live" value="0" checked=""> Tidak
													</label>
												</div>
							        </div>
										</div> --}}

										{{-- <div class="form-group">
											<label for="title">Tujuan Pembelajaran</label>
											<textarea name="goal" id="goal">{{ $goal }}</textarea>
										</div> --}}

										<div class="form-group">
											<label for="title">Deskripsi</label>
											<textarea name="description" id="description">{{ $description }}</textarea>
										</div>

										<div class="form-group">
											<label for="title">Tipe Kelas</label>
											<br>
											<div class="onoff" style="display:flex">
												<input type="radio" name="public" value="1" checked> Terbuka
											  <input type="radio" name="public" value="0"> Tertutup
											</div>
										</div>

										<div class="form-group">
											<label for="title">Publish Kelas</label>
											<br>
											<input type="checkbox" name="status">
										</div>

										{{-- <div class="form-group">
											<label for="price">Harga</label>
											<input placeholder="Price" type="number" class="form-control" name="price" value="{{ $price }}" required>
										</div> --}}
										<input placeholder="Price" type="hidden" class="form-control" name="price" value="{{ $price }}">

										{{-- <div class="form-group">
											<label for="name">Tingkatan</label>
											<select class="form-control" name="id_level_course" id="id_level_course"  required>
												<option value="">Pilih Tingkatan</option>
												@foreach($course_levels as $course_level)
													<option {{ $course_level->id == $id_level_course ? 'selected' : '' }} value="{{$course_level->id}}">{{$course_level->title}}</option>
												@endforeach()
											</select>
										</div> --}}
										<input type="hidden" name="id_level_course" value="4">

										{{-- <div class="form-group">
											<label for="name">Share dengan Group</label>
											<select class="form-control" name="id_instructor_group[]" id="id_instructor_group" multiple>
												@foreach($instructor_groups as $instructor_group)
													<option {{ $instructor_group->id == $id_instructor_group ? 'selected' : '' }} value="{{$instructor_group->id}}">{{$instructor_group->title}}</option>
												@endforeach()
											</select>
										</div> --}}

										{{-- @if(Request::get('model') == 'private')
											<div class="form-group">
												<label for="title">Password / Kode Akses</label>
												<input placeholder="Masukan Password / Kode Akses" type="text" class="form-control" name="password" value="{{ generateRandomString(8) }}" required>
												@if ($errors->has('password'))
													<span class="text-danger">{{ $errors->first('password') }}</span>
												@endif
											</div>
										@endif --}}

										<div class="form-group">
											<input type="hidden" id="model" name="model" value="subscription">
											{{  Form::hidden('id', $id) }}
											{{-- {{  Form::submit($button . " Kelas" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button', 'id' => 'contentSave')) }} --}}
											<button class="btn btn-primary btn-raised" name="button" id="contentSave" type="submit">{{$button}} Kelas</button>
											<a href="{{ url('course/dashboard') }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')
	<script src="{{asset('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<!-- <script src="{{asset('ckeditor/ckeditor.js')}}"></script> -->

	{{-- CKEDITOR --}}
	<script type="text/javascript">
		$("[name=status]").bootstrapSwitch()
	  // CKEDITOR.replace('goal', {
		// 	filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
    //   filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		// });
		CKEDITOR.replace('description', {
			filebrowserBrowseUrl: "{{asset('/ckeditor/browser?type=Images')}}",
      		filebrowserUploadUrl: "{{asset('/ckeditor/uploader?command=QuickUpload&type=Images')}}",
		});

		// CKEDITOR.replace('section_description', {
		// 	filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
      	// filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		// });
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{asset('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#id_category").select2();
		$("#id_level_course").select2();
		$("#id_author").select2({
		   placeholder: "Pilih pengajar"
		});
		$("#id_instructor_group").select2({
		   placeholder: "Pilih group"
		});
	</script>
	{{-- SELECT2 --}}

	<script type="text/javascript">
		$("#div_is_batch").hide()
		$('input[type=radio][name=is_batch]').change(function() {
			if (this.value == '1') {
				$("#div_is_batch").show()
				$("#model").val('batch')
			}else{
				$("#div_is_batch").hide()
				$("#model").val('subscription')
			}
		});
	</script>

	<script type="text/javascript">
		$('input[type=radio][name=is_live]').change(function() {
			if (this.value == '1') {
				$("#model").val('live')
			}else{
				$("#model").val('batch')
			}
		});
	</script>

	<script type="text/javascript">
		$("#datePicker1").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
		$("#datePicker2").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>

	<script>
		$("#formCreate").submit(function(){

				$("#contentSave").html('<i class="fa fa-circle-o-notch fa-spin"></i>Menyimpan');
				$("#contentSave").removeClass('btn-primary');
				$("#contentSave").removeClass('btn-raised');
				$("#contentSave").attr('disabled', 'disabled');

		})
	</script>
@endpush
