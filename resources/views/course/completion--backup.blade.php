@extends('layouts.app')

@section('title', 'Course Completions ' . $Course->title)

@section('content')
	<div class="container">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Course Completion - {{$Course->title}}</h3>
		  </div>
		  <div class="panel-body" style="background:white">
				<table class="table table-bordered">
					<tr>
						<th>Name</th>
						<th>Progress</th>
						<th></th>
					</tr>
					@foreach($completions as $completion)
						<tr>
							<td>{{$completion['name']}}</td>
							<td>{{$completion['percentage']}}%</td>
							<td><a class="btn btn-primary btn-raised btn-sm" href="/course/completion/detail/{{Request::segment(3)}}/{{$completion['user_id']}}">Detail</a></td>
						</tr>
					@endforeach
				</table>
		  </div>
		</div>
	</div>

@endsection
