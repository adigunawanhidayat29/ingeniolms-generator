@foreach($courses as $course)
  @php
    $num_contents = $course['num_contents'];
    //count percentage
    $num_progress = 0;
    $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
    $percentage = 0;
    $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
    $percentage = $percentage == 0 ? 1 : $percentage ;
    $percentage = 100 / $percentage;
    //count percentage
  @endphp
  <div class="col-md-3 mb-2 {{$course['public'] == '1' ? 'public-course' : 'private-course'}}">
    <div class="card-sm">
      <a href="{{url('course/learn/'. $course['slug'])}}">
        <img src="{{$course['image']}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
      </a>
      <a href="{{url('course/'. $course['slug'])}}">
        <div class="card-block text-left">
          @if(is_liveCourse($course['id']) == true) <span class="badge badge-danger">Live</span> @endif
          @if($course['public'] == '0') <span class="badge badge-warning">Tertutup</span> @endif
          @if($course['model'] == 'batch') <span class="badge badge-success">Terjadwal</span> @endif
          @if($course['model'] == 'subscription') <span class="badge badge-primary">Lifetime</span> @endif
          <h4 class="headline-xs dq-max-title">{{$course['title']}}</h4>
          <p class="section-body-xs">
            <span class="dq-max-subtitle">{{ $course['name'] }}</span>
          </p>
          <div class="m-0">
            <div class="progress">
              <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
            </div>
            <span class="color-primary dq-span-border">
              {{$percentage > 1 ? 'LANJUTKAN' : 'MULAI'}}
            </span>
            <br><br>
          </div>
        </div>
      </a>
    </div>
  </div>
@endforeach
