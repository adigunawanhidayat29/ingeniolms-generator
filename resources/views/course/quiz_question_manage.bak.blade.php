@extends('layouts.app')

@section('title')
	{{ 'Kelola Kuis ' . $quiz->name }}
@endsection

@push('style')

	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<style media="screen">
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			background: #ccc;
			color: #333!important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:active {
			background: none;
			color: black!important;
		}
		select.form-control:not([size]):not([multiple]){
			height: auto !important;
		}
	</style>
	<style media="screen">
		.dq-input-group{
			width: 100%;
			display: flex;
		}
			.dq-input-group i{
				position: absolute;
				left: 0;
				padding: 0.5rem 1rem;
				align-self: center;
				z-index: 1;
				font-size: 18px;
				border-right: 1px solid #ccc;
			}
			.dq-input-group .form{
				position: relative;
				border: 1px solid #ccc;
				border-radius: 2px;
				background: #f9f9f9;
				box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
			}
			.dq-input-group span{
				align-self: center;
				margin: 0 0.5rem;
			}
			.date{
				width: 100%;
				padding: 0.5rem 0rem 0.5rem 4.5rem;
			}
			.select{
				width: auto;
				padding: 0.5rem 1rem;
			}
	</style>
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="#/">Pengajar</a></li>
				  <li><a href="/course/dashboard">Kelola Kelas</a></li>
				  <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
				  <li>Manage Kuis - {{$quiz->name}}</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container">
		<!-- <br> -->
		<div class="row">
			<div class="col-md-3">
				<img width="100%" src="{{asset_url(course_image_small($course->image))}}" alt="{{$course->title}}">
			</div>
			<div class="col-md-9">

				<div class="card">
				    <!-- Nav tabs -->
				    <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
				        <li class="nav-item"><a class="nav-link withoutripple active" href="#question" aria-controls="question" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Pertanyaan</span></a></li>
				        <li class="nav-item"><a class="nav-link withoutripple" href="#setting" aria-controls="setting" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Pengaturan</span></a></li>
				        <li class="nav-item"><a class="nav-link withoutripple" href="#preview" aria-controls="preview" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Preview</span></a></li>
				        <li class="nav-item"><a class="nav-link withoutripple" href="#result" aria-controls="result" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">Hasil</span></a></li>
				    </ul>

				    <div class="card-block">
				        <!-- Tab panes -->
				        <div class="tab-content">
				            <div role="tabpanel" class="tab-pane fade active show" id="question">
											<div class="btn-group">
										    <button type="button" class="btn btn btn-primary dropdown-toggle btn-raised" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									        Tambah Pertanyaan <i class="zmdi zmdi-chevron-down right"></i>
										    </button>
										    <ul class="dropdown-menu">
													{{-- <li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Pilihan Ganda</a></li>
													<li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Benar Salah</a></li>
													<li role="separator" class="dropdown-divider"></li> --}}
													@foreach($quiz_types as $quiz_type)
														<li><a  href="{{url('course/quiz/question/create/'.$quiz->id.'?quiz_type='.$quiz_type->id)}}">{{$quiz_type->type}}</a></li>
													@endforeach()
													<li role="separator" class="dropdown-divider"></li>
									        <li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Buat Baru</a></li>
									        <li><a href="{{url('course/quiz/get/'.$quiz->id)}}">Ambil Dari Bank</a></li>
									        <li><a href="{{url('course/quiz/question/import/'.$quiz->id)}}">Import</a></li>
										    </ul>
											</div>

											<div class="panel-group" id="accordion">
												@php $no = 1 @endphp
												@foreach($quiz_questions_answers as $quiz_question_answer)
													<div class="panel panel-default">
														<a data-toggle="collapse" data-parent="#accordion{{$no}}" href="#collapse{{$no}}">
												      <div class="panel-heading">
																<span>No. {{$no}}</span>
												        <h4 class="panel-title">
												          {!!$quiz_question_answer['question']!!}
												        </h4>
																<small>
																	{{$quiz_question_answer['type']}} - {!!$quiz_question_answer['weight']!!} poin
																</small>
																<div class="text-right">
																	<a class="btn btn-warning btn-sm btn-raised" href="{{url('course/quiz/question/update/'.$quiz->id.'/'.$quiz_question_answer['id'])}}">Edit</a>
																	<a onclick="return confirm('Are You sure?')" class="btn btn-danger btn-sm btn-raised" href="{{url('course/quiz/question/delete/'.$quiz->id.'/'.$quiz_question_answer['id'])}}">Hapus</a>
																</div>
												      </div>
														</a>
											      <div id="collapse{{$no}}" class="panel-collapse collapse">
											        <div class="panel-body">
																<table class="table">
																	@foreach($quiz_question_answer['question_answers'] as $index => $question_answer)
																	<tr>
																		<td>Pilihan {{$index + 1}}</td>
																		<td class="{{$question_answer->answer_correct == '1' ? 'success' : 'danger'}}">
																			@if($quiz_question_answer['quiz_type_id'] == '1' || $quiz_question_answer['quiz_type_id'] == '2')
																				{!!$question_answer->answer!!} - {{$question_answer->answer_correct == '1' ? '(Benar)' : '(Salah)'}}
																			@else
																				{!!$question_answer->answer!!} - {{$question_answer->answer_ordering}}
																			@endif
																			<br>
																			<a href="#/" id="edit_answer" data-id="{{$question_answer->id}}" data-answer="{{$question_answer->answer}}" data-answer-correct="{{$question_answer->answer_correct}}"><i class="fa fa-pencil"></i> Edit</a>
																			&nbsp;
																			<a onclick="return confirm('Are You sure?')" href="{{url('course/quiz/question/answer/delete/'.$quiz->id.'/'.$question_answer->id)}}"><i class="fa fa-trash"></i> Hapus</a>
																		</td>
																	</tr>
																	@endforeach
																</table>
															</div>
											      </div>
											    </div>
												@php $no++ @endphp
												@endforeach
											</div>

				            </div>
										<div role="tabpanel" class="tab-pane fade" id="preview">
											<a href="/course/quiz/question/preview/{{$quiz->id}}" class="btn btn-raised btn-primary">Preview Kuis</a>
										</div>
				            <div role="tabpanel" class="tab-pane fade" id="setting">
											{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

												<div class="form-group">
													<label for="name">Judul Kuis</label>
													<input placeholder="Judul Kuis" type="text" class="form-control" name="name" value="{{ $name }}" required>
												</div>

												<div class="form-group">
													<label for="title">Deskripsi / Penjelasan</label>
													<textarea name="description" id="description">{{ $description }}</textarea>
												</div>
												<div class="form-group">
													<label for="title">Acak?</label><br>
													<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
													<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
												</div>
												{{--
												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
															<label for="name">Tanggal Mulai</label>
															<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : '' }}" required>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label for="name">Jam Mulai</label>
															<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ isset($time_start) ? date("H:i:s", strtotime($time_start)) : '' }}" required>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
															<label for="name">Tanggal Selesai</label>
															<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}" required>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label for="name">Jam Selesai</label>
															<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ isset($time_end) ? date("H:i:s", strtotime($time_end)) : '' }}" required>
														</div>
													</div>
												</div>
												--}}

												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
															<label>Tanggal Mulai</label>
															<div class="dq-input-group">
																<i class="fa fa-calendar"></i>
																<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form date datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : '' }}" required>
															</div>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label>Jam Mulai</label>
															<div class="dq-input-group">
																<select class="form select" name="">
																	<option>-</option>
																	<option value="01">01</option>
																	<option value="02">02</option>
																	<option value="03">03</option>
																	<option value="04">04</option>
																	<option value="05">05</option>
																	<option value="06">06</option>
																	<option value="07">07</option>
																	<option value="08">08</option>
																	<option value="09">09</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																</select>
																<span>:</span>
																<select class="form select" name="">
																	<option>-</option>
																	<option value="00">00</option>
																	<option value="15">15</option>
																	<option value="30">30</option>
																	<option value="45">45</option>
																</select>
																<span></span>
																<select class="form select" name="">
																	<option value="AM">AM</option>
																	<option value="PM">PM</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
															<label>Tanggal Selesai</label>
															<div class="dq-input-group">
																<i class="fa fa-calendar"></i>
																<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form date datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}" required>
															</div>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label>Jam Selesai</label>
															<div class="dq-input-group">
																<select class="form select" name="">
																	<option>-</option>
																	<option value="01">01</option>
																	<option value="02">02</option>
																	<option value="03">03</option>
																	<option value="04">04</option>
																	<option value="05">05</option>
																	<option value="06">06</option>
																	<option value="07">07</option>
																	<option value="08">08</option>
																	<option value="09">09</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																</select>
																<span>:</span>
																<select class="form select" name="">
																	<option>-</option>
																	<option value="00">00</option>
																	<option value="15">15</option>
																	<option value="30">30</option>
																	<option value="45">45</option>
																</select>
																<span></span>
																<select class="form select" name="">
																	<option value="AM">AM</option>
																	<option value="PM">PM</option>
																</select>
															</div>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="name">Durasi (Menit)</label>
													<input placeholder="60" type="text" class="form-control" name="duration" value="{{ $duration }}" required>
												</div>
												<div class="form-group">
													<label for="name">Kesempatan / Percobaan</label>
													<input placeholder="3" type="text" class="form-control" name="attempt" value="{{ $attempt }}" required>
												</div>
												<div class="form-group">
													{{  Form::hidden('id', $id) }}
													{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
													{{-- <a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a> --}}
												</div>
											{{ Form::close() }}
				            </div>
				            {{-- <div role="tabpanel" class="tab-pane fade" id="preview">
				              Preview
				            </div> --}}
				            <div role="tabpanel" class="tab-pane fade" id="result">
											<div class="table-responsive">
												<table class="table table-hover" width="100%" id="data">
													<thead>
														<tr>
															<th>Nama</th>
															<th>Pilihan</th>
														</tr>
													</thead>
												</table>
											</div>
				            </div>
				        </div>
				    </div>
				</div> <!-- card -->

			</div>
		</div>
	</div>

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="">Edit Pilihan</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Pilihan</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">Status Pilihan ?</label>
						<select class="form-control selectpicker" id="edit_text_answer_correct">
							<option value="0">Salah</option>
							<option value="1">Benar</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	        <button type="button" class="btn btn-primary btn-raised" id="save_edit_answer">Simpan</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}
@endsection

@push('script')

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}

	{{-- participant list --}}
	<script src="{{url('js/jquery.dataTables.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script> <!-- Datatable -->
	<script type="text/javascript">
	  $(function(){
			var asset_url = '{{asset_url()}}';
	    $($("#data").DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: '{{ url("course/quiz/participant/serverside/".$quiz->id) }}',
	      columns: [
					{ data: 'name', name: 'name' },
	        { data: 'action', 'searchable': false, 'orderable':false }
	      ],
				"oLanguage": {
					"sLengthMenu": "Tampilkan MENU",
					"sSearch": "Cari: ",
		      "oPaginate": {
						"sPrevious": "<",
						"sNext": ">",
					}
		    },
	    }).table().container()).removeClass( 'form-inline' );
	  });
  </script>

	{{-- participant list --}}

	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>

<script type="text/javascript">
var js = document.createElement("script");
js.type = "text/javascript";
js.src = "WIRISplugins.js?viewer=image";
document.head.appendChild(js);
</script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
@endpush
