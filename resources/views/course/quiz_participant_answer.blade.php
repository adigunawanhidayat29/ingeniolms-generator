@extends('layouts.app')

@section('title')
	Jawaban {{$quiz->name}}
@endsection


@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Daftar <span>Jawaban</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
					<li><a href="/">Home</a></li>
				  <li><a href="/course/dashboard">Kelola Kelas</a></li>
				  <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
				  <li><a href="/course/quiz/question/manage/{{$quiz->id}}">{{$quiz->name}}</a></li>
				  <li>Daftar Jawaban</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<!-- <div class="box-header with-border">
							<h3 class="box-title">Answer List</h3>
						</div> -->

						<div class="box-body">

	            <center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

	            <div class="table-responsive">
                <h4>
                  Nama Peserta: <b>{{$user->name}}</b>
                </h4>
                <h4>
                  {{-- Nilai Peserta: <b class="labelNilaiAkhirPeserta">{{ $quiz->grade }}</b> --}}
                </h4>
	              <table class="table table-hover">
	                <tr class="bg-primary">
	                  <th>No</th>
	                  <th>Tipe Pertanyaan</th>
	                  <th>Pertanyaan</th>
	                  <th>Jawaban</th>
	                  <th>Bobot</th>
										<th>Nilai</th>
                  </tr>
                  
                  @php 
                    $nilaiTotal = 0;
                    $nilaiPGTotal = 0;
                    $nilaiEssayTotal = 0;
                    $nilaiOrderingTotal = 0;
                  @endphp

                  @foreach($quiz_participants as $index => $quiz_participant)

                    {{-- TIPE KUIS PILIHAN GANDA / BENAR SALAH --}}
										@if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '7' )
											<tr class="">
                        <td>{{$index+1}}</td>
                        <td>Pilihan Ganda</td>
												<td>{!!$quiz_participant->question!!}</td>
                        <td>{!!$quiz_participant->answer!!}</td>
                        <td>
                          {{$quiz_participant->weight}}
                          <input type="number" class="grade" hidden value="{{$quiz_participant->answer_correct}}">
                        </td>
												<td>
                          @php
                              if($quiz_participant->answer_correct == "1"){
                                echo "Benar";
                              }else{
                                echo "Salah";
                              }
                          @endphp
                        </td>
                      </tr>

                      @php
                        $nilaiPGTotal += $quiz_participant->answer_correct == "1" ? $quiz_participant->weight : 0;
                      @endphp
                      
                    {{-- TIPE KUIS MENGURUTKAN /ORDERING --}}
										@elseif($quiz_participant->quiz_type_id == '3')
											@php
												$question_answers = DB::table('quiz_question_answers')
													->whereIn('id', explode(',', $quiz_participant->quiz_question_answer_id))
													->orderByRaw(DB::raw("FIELD(id, $quiz_participant->quiz_question_answer_id)"))
													->get();
											@endphp
											<tr class="">
                        <td>{{$index+1}}</td>
                        <td>Mengurutkan</td>
												<td>{!!$quiz_participant->question!!}</td>
												<td>
													@foreach($question_answers as $index => $question_answer)
														{!!$question_answer->answer!!} <br> <br>
													@endforeach
                        </td>
                        <td>
                          {{$quiz_participant->weight}}
                          @php
                              $sortGrade = intval($quiz_participant->answer_ordering_grade);
                              $sortGrade = $sortGrade * intval($quiz_participant->weight) / 100;
                          @endphp
                          <input type="number" class="grade" hidden value="{{$sortGrade}}">
                        </td>
												<td>
                            @if($quiz_participant->answer_ordering_grade == null)
                                <a href="#" id="orderingGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Berikan Nilai Mengurutkan</a>
                            @else
                                {{$quiz_participant->answer_ordering_grade}} <a href="#" id="orderingGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Edit Nilai</a>
                            @endif
                        </td>
                      </tr>

                      @php
                        $nilaiOrderingTotal += $sortGrade ;
                      @endphp
                      
                    {{-- TIPE KUIS ESSAY --}}
										@elseif($quiz_participant->quiz_type_id == '5')
											<tr class="">
                        <td>{{$index+1}}</td>
                        <td>Essay</td>
												<td>{!!$quiz_participant->question!!}</td>
												<td>
													{!!$quiz_participant->answer_essay!!}
                        </td>
                        <td>
                          {{$quiz_participant->weight}}
                          @php
                              $essayGrade = intval($quiz_participant->answer_essay_grade);
                              $essayGrade = $essayGrade * intval($quiz_participant->weight) / 100;
                          @endphp
                          <input type="number" class="grade" hidden value="{{$essayGrade}}">
                        </td>
												<td>
                          @if($quiz_participant->answer_essay_grade == null)
                            <a href="#" class="essayGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Berikan Nilai Essay</a>
                          @else
                            {{$quiz_participant->answer_essay_grade}} <a href="#" class="essayGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Edit Nilai</a>
                          @endif
                        </td>
                      </tr>
                      
                      @php
                        $nilaiEssayTotal += $essayGrade ;
                      @endphp

                    {{-- TIPE KUIS JAWABAN SINGKAT/SHORT ANSWER --}}
										@elseif($quiz_participant->quiz_type_id == '6')
											<tr class="">
                        <td>{{$index+1}}</td>
                        <td>Short Answer</td>
												<td>{!!$quiz_participant->question!!}</td>
												<td>
													{!!$quiz_participant->answer_short_answer!!}
												</td>
												<td>
                          {{$quiz_participant->weight}}
                        </td>
												<td>
                          
                        </td>
											</tr>

                    @endif
                    
                  @endforeach
                  
                  @php
                    $nilaiTotal += ($nilaiPGTotal + $nilaiEssayTotal + $nilaiOrderingTotal);
                  @endphp

                </table>

                <div class="text-right">
                  <strong class="btn btn-sm btn-success btn-raised">Nilai Total: {{ $nilaiTotal }}</strong>
                </div>

                @php
                  // update nilai dengan nilai total
                  DB::table('quiz_participants')->where(['user_id' => $user->id, 'quiz_id' => $quiz->id])->update(['grade' => $nilaiTotal]);
                @endphp
                
	            </div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
  </div>
  
  <div class="modal" id="modalBeriNilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div style="padding: 16px">
                  <h1>Berikan Nilai</h1>
                  <form action="">
                      <div>
                          <input type="hidden" id="quiz_participant_answer_id">
                          <input id="grade" placeholder="Masukan nilai disini dari 0 sampai 100" type="number" class="form-control">
                      </div>
                      <div class="text-right">
                          <label class="label-input-salah text-danger"></label>
                          <input type="button" class="btn btn-raised btn-primary" value="Kirim" id="sendGrade">
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
  <div class="modal" id="modalNilaiOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div style="padding: 16px">
                  <h1>Berikan Nilai</h1>
                  <form action="">
                      <div>
                          <input type="hidden" id="answer_id">
                          <input id="gradeOrder" placeholder="Masukan nilai disini dari 0 sampai 100" type="number" class="form-control">
                      </div>
                      <div class="text-right">
                        <label class="label-input-salah text-danger"></label>
                        <input type="button" class="btn btn-raised btn-primary" value="Kirim" id="sendOrderGrade">
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>

@endsection

@push('script')
	<script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>

  <script>
    //variable global;
    let nilaiAkhir = 0;
  </script>

  <script>
    $(".essayGrade").click(function(e){
      e.preventDefault();
      var quiz_participant_answer_id = $(this).attr('data-id');
      $("#quiz_participant_answer_id").val(quiz_participant_answer_id);
      $("#modalBeriNilai").modal('show')
    });
    $("#orderingGrade").click(function(e){
      e.preventDefault();
        var quiz_participant_answer_id = $(this).attr('data-id');
        $("#answer_id").val(quiz_participant_answer_id);
        $("#modalNilaiOrder").modal('show')
    });
  </script>

  <script>
    $("#sendGrade").click(function(){
      var quiz_participant_answer_id = $("#quiz_participant_answer_id").val();
      var grade = $("#grade").val();
      var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('course/quiz/participant/answer/grade')}}",
          type : "POST",
          data : {
            id : quiz_participant_answer_id,
            grade : grade,
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });      
    });

    $("#sendOrderGrade").click(function(){
        var answer_id = $("#answer_id").val();
        var grade = $("#gradeOrder").val();
        // alert(answer_id)
        $.ajax({
            url : "{{url('course/quiz/participant/answer/gradeOrder')}}",
            type : "POST",
            data : {
                id : answer_id,
                grade : grade,
                _token: '{{ csrf_token() }}'
            },
            success : function(result){
                location.reload();
            },
        });
    });
  </script>

  {{-- cek input 0 sampai 100 --}}
  <script>
    $("#grade").on('keyup', function(params) {
      if(($(this).val() > -1) && ($(this).val() < 101)){
        $("#sendGrade").attr('disabled', false);
        $('.label-input-salah').html('');
      }else{
        $("#sendGrade").attr('disabled', true);
        $('.label-input-salah').html('masukan nilai dengan rentang 0 sampai 100');
      }
    });
    $("#gradeOrder").on('keyup', function(params) {
      if(($(this).val() > -1) && ($(this).val() < 101)){
        $("#sendOrderGrade").attr('disabled', false);
        $('.label-input-salah').html('');
      }else{
        $("#sendOrderGrade").attr('disabled', true);
        $('.label-input-salah').html('masukan nilai dengan rentang 0 sampai 100');
      }
    });
  </script>

  {{-- <script>
    function kalkulasiTotalGrade() {
      let loopTr = $('tr').length;
      let totalBobot = 0;
      for (let index = 1; index < loopTr; index++) {
        totalBobot += parseFloat($('tr')[index].children[4].innerText);
      }
      
      let totalBobotUser = 0;
      $('.grade').each(function(){
        totalBobotUser += parseFloat($(this).val());
      });
      
      nilaiAkhir = totalBobotUser/totalBobot * 100;
      nilaiAkhir = nilaiAkhir.toFixed(2);
      $('.labelNilaiAkhirPeserta').html(nilaiAkhir);
      // @if($quiz_participant->quiz_type_id == '5' && $quiz_participant->answer_essay_grade == null)
        // $('.labelNilaiAkhirPeserta').append(' "nilai sementara, karena beberapa soal essay belum anda nilai"')
      // @endif
    }
    kalkulasiTotalGrade();
  </script> --}}

  <script>
    let gradeTotal = nilaiAkhir;
    function simpanGradeTotal() {
      $.ajax({
        url : "{{url('course/quiz/participant/answer/grade/total')}}",
        type : "POST",
        data : {
          id : $("#quiz_participant_answer_id").val(),
          grade : gradeTotal,
          _token: '{{ csrf_token() }}'
        },
        success : function(result){
          
        },
      });
    }
    simpanGradeTotal();
  </script>
@endpush
