@extends('layouts.app_nation')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
  <style>
    .table-cs .dataTables_length {
      font-size: 20px;
    }

    .table-cs input {
      width: 100%;
      font-size: 14px;
      border: 1px solid #ccc;
      border-radius: 5px;
      box-shadow: none;
      padding: 0.75rem 1.5rem;
      margin: 0;
    }

    .table-cs thead>tr>th {
      border-bottom: none;
      padding: 8px 10px;
    }

    .table-cs tbody>tr>td {
      background: white !important;
    }

    .table-cs .dataTables_paginate .paginate_button {
    }

    .table-cs .dataTables_paginate span a.paginate_button {
      border-radius: 3px;
      padding: 0.25rem 0.75rem;
    }

    .table-cs .dataTables_paginate span a.paginate_button.current {
      border: 0px;
      background: #4ca3d9;
      color: #fff !important;
    }

    .table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after {
			display: none !important
    }

    .table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child {
			width: 0.1px !important;
			padding: 0;
    }

    .table-cs .dataTables_info {
      display: none;
    }

		.table-cs .table td, .table-cs .table th {
			vertical-align: middle;
		}

    .dataTables_filter {
      /* margin-bottom: 15px; */
    }
  </style>
	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>
	<style>
		/* Style the tab */
		.tab {
			float: left;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
			width: 30%;
			height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
			display: block;
			background-color: inherit;
			color: black;
			padding: 22px 16px;
			width: 100%;
			border: none;
			outline: none;
			text-align: left;
			cursor: pointer;
			transition: 0.3s;
			font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
			background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
			background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.placeholder{
			background-color:white;
			height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}

		/* .library-direc{
			display: flex;
			padding: 20px;
			font-size: 19px;
    	font-family: cursive;
		}

		.library-direc span{
			color: #4ca3d9;
	    font-size: 25px;
	    margin-right: 20px;
		}

		.direc:hover{
			background:  #4ca3d9;
			color: white !important;
		}

		.direc:hover .library-direc{
			color: white !important;
		}

		.direc:hover .library-direc span{
			color: white !important;
		} */

		.nav-tabs-ver-container-content{
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.tab-content-share .tab{
			min-height: 180px;
			margin-bottom: 25px;
		}

		.tab-content-share li{
			width: 100%;
		}

		.tab-content-share li a.active{
			background: #ccc;
		}

		.tab-content-share .nav-tabs-ver-container-content{
			min-height: 180px;
		}

		.tab-content-share .nav-tabs-ver-container-content .tab-pane{
			background: white;
    	color: black;
		}

		.tab-content-share .nav-tabs-ver-container-content .card-block{
			padding: 0;
		}

		.tab-content-share li a{
			display: block;
	    background-color: inherit;
	    color: black;
	    padding: 22px 16px;
	    width: 100%;
	    border: none;
	    outline: none;
	    text-align: left;
	    cursor: pointer;
	    transition: 0.3s;
	    font-size: 17px;
		}

		.dropdown-menu.dropdown-menu-primary li a i{
			padding: 5px;
			border-radius: 5px;
			width: 25px;
			text-align: center;
		}

		.active, .site-dashboard-panel__nav:hover {
			background-color: #7c7c7c;
			background: none;
			color: black;
		}
	</style>
	<style>
		.card.card-library {
			background: #f9f9f9;
			color: #424242;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
		}

		.card.card-library.card-library-hover:hover {
			background: #f3f3f3;
		}

		.upload-page{
			background: #f8f8f8;
			border: 1px solid #d1d1d1;
			padding: 10px 15px;
			padding-bottom: 5px;
		}

		.upload-page input{
			/* z-index: 999; */
			/* display: none; */
		}
	</style>

	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload.css" />
	<link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui.css" />
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript
		><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-noscript.css"
	/></noscript>
	<noscript
		><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui-noscript.css"
	/></noscript>
	<style>
		.fileinput-button input {
				position: absolute;
				top: 0;
				right: 0;
				margin: 0;
				height: 100%;
				opacity: 0;
				filter: alpha(opacity=0);
				font-size: 200px !important;
				direction: ltr;
				cursor: pointer;
				width: 180px;
		}
	</style>
@endpush

@section('content')
	<div class="bg-page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					{{-- <h2 class="headline-md no-m">Libraries (Beta)</h2> --}}
						<h2 class="headline-md no-m">{{$folder->title}}</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li><a href="{{url('/instructor/library')}}">Course</a></li>
					<li>Manage Folder</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					@if($errors->any())
							{!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
					@endif

					{{ Form::open(array('url' => '/course/content/folder/submit/'.$folder->id, 'method' => 'post', 'enctype' => 'multipart/form-data')) }}

					<div class="upload-page">
						<!-- The file upload form used as target for the file upload widget -->
						<div id="fileupload-folder">
							<!-- Redirect browsers with JavaScript disabled to the origin page -->
							<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
							<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
							<div class="row fileupload-buttonbar">
								<div class="col-lg-7">
									<!-- The fileinput-button span is used to style the file input field as button -->
									<span class="btn btn-success fileinput-button btn-raised">
										<i class="glyphicon glyphicon-file"></i>
										<span>Tambah File</span>
										<input type="file" name="files[]" multiple id="add-file-jquery-upload" />
									</span>
									<!-- The global file processing state -->
									<span class="fileupload-process"></span>
								</div>
								<!-- The global progress state -->
								<div class="col-lg-5 fileupload-progress fade">
									<!-- The global progress bar -->
									<div
										class="progress progress-striped active"
										role="progressbar"
										aria-valuemin="0"
										aria-valuemax="100"
									>
										<div
											class="progress-bar progress-bar-success"
											style="width: 0%;"
										></div>
									</div>
									<!-- The extended global progress state -->
									<div class="progress-extended">&nbsp;</div>
								</div>
							</div>
							<!-- The table listing the files available for upload/download -->
							<table role="presentation" class="table table-striped table-files-upload" style="margin:0;    display: inline-table;">
								<tbody class="files"></tbody>
							</table>
						</div>
					</div>

					<div class="form-group" style="margin: 15px 0px 0;">
							{{  Form::submit("Submit File" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
					</div>
					{{ Form::close() }}
				</div>
			</div>
			<div class="card card-library">
				<div class="card-block">
					<div class="row clearfix">

						<div class="col-sm-12">
							<div class="table-responsive table-cs">
								<table id="example" class="table table-striped d-table">
									<thead>
										<tr>
		                  <th>No</th>
											<th style="min-width: 25vh;">File</th>
											<th>Tipe</th>
											<th class="text-center">Size</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($folder->files as $index => $item)
										<?php
											$file_name = explode('-',$item->file_name);
											unset($file_name[0]);
											$file_name = implode('-',$file_name);
										 ?>
											<tr>
												<td>{{$index+1}}</td>
												<td style="min-width: 25vh;">
													@if($item->file_type == 'pdf')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/pdf.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@elseif($item->file_type == 'docx')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/word.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@elseif($item->file_type == 'mp4')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/mp4.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@else
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/file.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@endif
												</td>
												<td>{{$item->file_type}}</td>
												<td class="text-center">{{formatSizeUnits($item->file_size)}}</td>
												<td class="text-center">
													<form method="post" action="{{url('/course/content/folder/item/delete/'.$item->id)}}">
														{{csrf_field()}}
														<button type="submit" class="btn btn-danger btn-raised"><i class="fa fa-trash"></i></button>
													</form>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal" id="ModalMenuContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-body">

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
	<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="{{asset('js/sweetalert.min.js')}}"></script>
	<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script>

		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();

		// trigger modal add konten / quiz

		$(document).ready(function() {
			$('#example').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
		});
	</script>

	<script id="template-upload" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
				<tr class="template-upload {%=o.options.loadImageFileTypes.test(file.type)?' image':''%}">
						<td>
								<span class="preview"></span>
						</td>
						<td>
								<p class="name">{%=file.name%}</p>
								<strong class="error text-danger"></strong>
						</td>
						<td>
								<p class="size">Processing...</p>
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
						</td>
						<td>
								{% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
									<button class="btn btn-success edit" data-index="{%=i%}" disabled>
											<i class="glyphicon glyphicon-edit"></i>
											<span>Edit</span>
									</button>
								{% } %}
								{% if (!i && !o.options.autoUpload) { %}
										<button class="btn btn-primary start" disabled>
												<i class="glyphicon glyphicon-upload"></i>
												<span>Start</span>
										</button>
								{% } %}
								{% if (!i) { %}
										<button class="btn btn-warning cancel cancel-template-td">
												<i class="glyphicon glyphicon-ban-circle"></i>
												<span>Cancel</span>
										</button>
								{% } %}
						</td>
				</tr>
		{% } %}
	</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
			{% if (file.name.substring(0,{{strlen(Auth::user()->id)}}) == {{Auth::user()->id}}) { %}
				<tr class="template-download {%=file.thumbnailUrl?' image':''%}">
						<td>
								<span class="preview">
										{% if (file.name.split(".")[file.name.split(".").length-1] == 'pdf') { %}
												<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{{asset('images/pdf.png')}}" style="width: 50px;"></a>
										{% }else if (file.name.split(".")[file.name.split(".").length-1] == 'docx') { %}
												<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{{asset('images/word.png')}}" style="width: 50px;"></a>
										{% }else if (file.name.split(".")[file.name.split(".").length-1] == 'mp4') { %}
											<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{{asset('images/mp4.png')}}" style="width: 50px;"></a>
										{% } %}

										{% if (file.thumbnailUrl) { %}
												<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
										{% } %}
								</span>
						</td>
						<td>
								<p class="name">
										{% if (file.url) { %}
												<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">{%=file.name.substring({{strlen(Auth::user()->id)}}+1)%}</a>
										{% } else { %}
												<span>{%=file.name%}</span>
										{% } %}
								</p>
								{% if (file.error) { %}
										<div><span class="label label-danger">Error</span> {%=file.error%}</div>
								{% } %}
						</td>
						<td>
								<span class="size">{%=o.formatFileSize(file.size)%}</span>
						</td>
						<td>
								{% if (file.deleteUrl) { %}
										<button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl+'&page=folder&id={{Auth::user()->id}}'%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
												<i class="glyphicon glyphicon-trash"></i>
												<span>Delete</span>
										</button>
								{% } else { %}
										<button class="btn btn-warning cancel">
												<i class="glyphicon glyphicon-ban-circle"></i>
												<span>Cancel</span>
										</button>
								{% } %}
						</td>
				</tr>
			{% } %}
		{% } %}
	</script>
	<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="/jquery-upload/js/vendor/jquery.ui.widget.js"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="/jquery-upload/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="/jquery-upload/js/jquery.fileupload.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-process.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-image.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-audio.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-video.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-validate.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="/jquery-upload/js/jquery.fileupload-ui.js"></script>
	<!-- The main application script -->
	<script src="/jquery-upload/js/demo.js"></script>
	<script>
	// Initialize the jQuery File Upload widget:
	$('#fileupload-folder').fileupload({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: '/jquery-upload/server/php?page=folder&id='+{{Auth::user()->id}},
		autoUpload:true
	});

	// Load existing files:
	$('#fileupload-folder').addClass('fileupload-processing');
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $('#fileupload-folder').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#fileupload-folder')[0]
	})
		.always(function () {
			$(this).removeClass('fileupload-processing');
		})
		.done(function (result) {
			$(this)
				.fileupload('option', 'done')
				// eslint-disable-next-line new-cap
				.call(this, $.Event('done'), { result: result });
		});
	</script>
@endpush
