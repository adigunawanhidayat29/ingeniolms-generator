@extends('layouts.app')

@section('title')
	{{ 'Kelola Kuis ' . $quiz->name }}
@endsection

@push('style')

	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<style media="screen">
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			background: #ccc;
			color: #333!important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:active {
			background: none;
			color: black!important;
		}
		select.form-control:not([size]):not([multiple]){
			height: auto !important;
		}
	</style>
	<style media="screen">
		.dq-input-group{
			width: 100%;
			display: flex;
		}
			.dq-input-group i{
				position: absolute;
				left: 0;
				padding: 0.5rem 1rem;
				align-self: center;
				z-index: 1;
				font-size: 18px;
				border-right: 1px solid #ccc;
			}
			.dq-input-group .form{
				position: relative;
				border: 1px solid #ccc;
				border-radius: 2px;
				background: #f9f9f9;
				box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
			}
			.dq-input-group span{
				align-self: center;
				margin: 0 0.5rem;
			}
			.date{
				width: 100%;
				padding: 0.5rem 0rem 0.5rem 4.5rem;
			}
			.select{
				width: auto;
				padding: 0.5rem 1rem;
			}
			.QuestionSortable{
	      border:0.2px solid #dfe6e9;
	      min-height:60px;
	      /* padding:10px; */
	    }

			#accordionExample p{
				margin: 10px;
			}
			#accordionExample .form-group{
				margin-top: 0
			}

			#accordionExample input:disabled,
			#accordionExample input[disabled]{
			  border: 1px solid #999999;
			  background-color: #cccccc;
			  color: #666666;
			}

			#accordionExample .form-control[disabled], #accordionExample .form-group .form-control[disabled] {
				border: 1px solid #999999;
			  background-color: #cccccc;
			  color: #666666;
			}
	</style>
@endpush

@section('content')
	<div class="bg-page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="headline-md no-m">@lang('front.page_manage_preview_quiz.manage') <span>@lang('front.page_manage_preview_quiz.quiz')</span></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">@lang('front.page_manage_preview_quiz.home')</a></li>
				  <li><a href="/course/dashboard">@lang('front.page_manage_preview_quiz.manage_class')</a></li>
				  <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
				  <li>{{$quiz->name}}</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<img width="100%" src="{{$course->image}}" alt="{{$course->title}}">
				</div>
				<div class="col-md-9">

					<div class="card card-flat">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-tabs-full nav-tabs-4" role="tablist">
							<li class="nav-item"><a class="nav-link withoutripple active" href="#question" aria-controls="question" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">@lang('front.page_manage_preview_quiz.questions')</span></a></li>
							<li class="nav-item"><a class="nav-link withoutripple" href="#setting" aria-controls="setting" role="tab" data-toggle="tab" id="button-nav-pengaturan"><span class="d-none d-sm-inline">@lang('front.page_manage_preview_quiz.settings')</span></a></li>
							<li class="nav-item"><a class="nav-link withoutripple" href="#preview" aria-controls="preview" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">@lang('front.page_manage_preview_quiz.preview')</span></a></li>
							<li class="nav-item"><a class="nav-link withoutripple" href="#result" aria-controls="result" role="tab" data-toggle="tab"><span class="d-none d-sm-inline">@lang('front.page_manage_preview_quiz.result')</span></a></li>
						</ul>
					</div>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active show" id="question">
							<div class="btn-group">
								<button type="button" class="btn btn btn-primary dropdown-toggle btn-raised" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									@lang('front.page_manage_preview_quiz.add_question') <i class="zmdi zmdi-chevron-down right"></i>
								</button>
								<ul class="dropdown-menu">
									{{-- <li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Pilihan Ganda</a></li>
									<li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Benar Salah</a></li>
									<li role="separator" class="dropdown-divider"></li> --}}
									@foreach($quiz_types->where('id', '<=', 7) as $quiz_type)
										<li><a href="{{url('course/quiz/question/create/'.$quiz->id.'?quiz_type='.$quiz_type->id)}}">{{$quiz_type->type}}</a></li>
									@endforeach()
									<li><a data-toggle="modal" data-target="#modalPageBreak" href="#/">@lang('front.page_manage_preview_quiz.page_break')</a></li>
									<li role="separator" class="dropdown-divider"></li>
									{{-- <li><a href="{{url('course/quiz/question/create/'.$quiz->id)}}">Buat Baru</a></li> --}}
									<li><a href="#" data-toggle="modal" data-target="#ModalShowBankSoal">@lang('front.page_manage_preview_quiz.get_from_bank')</a></li>
									<li><a href="{{url('course/quiz/question/import/'.$quiz->id)}}">@lang('front.page_manage_preview_quiz.import')</a></li>
								</ul>
							</div>

							<div class="panel-group PageBreakSortable" id="accordion">

								@if(count($quiz_questions_without_page_breaks) > 0)
									<div class="QuestionSortable mt-2 mb-2" id="">
										@foreach($quiz_questions_without_page_breaks as $quiz_questions_without_page_break)
											<li style="background:white;list-style:none;border: 1px solid #bbb; margin: 5px; padding: 10px" id="{{$quiz_questions_without_page_break->id}}">
												{{$quiz_questions_without_page_break->question}}
												<div class="text-right">
													<a class="btn btn-default btn-sm btn-raised" href="{{url('course/quiz/question/update/'.$quiz->id.'/'.$quiz_questions_without_page_break->id)}}">@lang('front.page_manage_preview_quiz.edit')</a>
													<a onclick="return confirm('@lang('front.page_manage_preview_quiz.delete_alert')')" class="btn btn-danger btn-sm btn-raised" href="{{url('course/quiz/question/delete/'.$quiz->id.'/'.$quiz_questions_without_page_break->id)}}">@lang('front.page_manage_preview_quiz.delete')</a>
												</div>
											</li>
										@endforeach
									</div>
								@endif

								@foreach($quiz_page_breaks as $quiz_page_break)
									<div class="panel panel-default" id="section{{$quiz_page_break->id}}" data-id="{{$quiz_page_break->id}}" style="background:white">
										<a data-toggle="collapse" data-parent="#accordion{{$quiz_page_break->id}}" href="#collapse{{$quiz_page_break->id}}">
											<div class="panel-heading">
												<h4 class="panel-title">{{$quiz_page_break->title}}</h4>
												<p class="mt-2" style="color:black">{{$quiz_page_break->description}}</p>
												<div class="text-right">
													<a href="#/" id="editPageBreak" data-id="{{$quiz_page_break->id}}" data-title="{{$quiz_page_break->title}}" data-description="{{$quiz_page_break->description}}">@lang('front.page_manage_preview_quiz.edit')</a>
													<a onclick="return confirm('@lang('front.page_manage_preview_quiz.page_break_delete_alert')')" href="/course/quiz/page-break/delete/{{$quiz_page_break->id}}">@lang('front.page_manage_preview_quiz.delete')</a>
												</div>
											</div>
										</a>
										<div id="collapse{{$quiz_page_break->id}}" class="panel-collapse collapse show">
											<div class="panel-body">
												<div class="QuestionSortable" id="{{ $quiz_page_break->id }}">
													@foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
														<li style="background:#EEEEEE;list-style:none;border: 1px solid #EEEEEE; border-radius:3px; margin: 5px; padding: 10px" id="{{$quiz_question->id}}" data-section="{{$quiz_question->page_break_id}}">
															{!!$quiz_question->question!!}
															<div class="text-right">
																<a class="btn btn-default btn-sm btn-raised" href="{{url('course/quiz/question/update/'.$quiz->id.'/'.$quiz_question->id . '?quiz_type=' . $quiz_question->quiz_type_id)}}">@lang('front.page_manage_preview_quiz.edit')</a>
																<a onclick="return confirm('@lang('front.page_manage_preview_quiz.delete_alert')')" class="btn btn-danger btn-sm btn-raised" href="{{url('course/quiz/question/delete/'.$quiz->id.'/'.$quiz_question->id)}}">@lang('front.page_manage_preview_quiz.delete')</a>
															</div>
														</li>
													@endforeach
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
							@if($quiz->status == '0')
								<button type="button" class="btn btn btn-primary btn-raised" onclick="window.location.href='{{url('course/quiz/publish/'.$quiz->id.'/'.$course->id)}}'">@lang('front.page_manage_preview_quiz.publish_quiz')</button>
							@else
								<button type="button" class="btn btn btn-danger btn-raised" onclick="window.location.href='{{url('course/quiz/unpublish/'.$quiz->id.'/'.$course->id)}}'">@lang('front.page_manage_preview_quiz.cancel_publish_quiz')</button>
							@endif
						</div>

						<div role="tabpanel" class="tab-pane fade" id="preview">
							<a href="/course/quiz/question/preview/{{$quiz->id}}" class="btn btn-raised btn-primary">@lang('front.page_manage_preview_quiz.preview_quiz')</a>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="setting">
							{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}
								@if (Session::has('error'))
									<div class="alert alert-warning alert-dismissible">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										{!! Session::get('error') !!}
									</div>
								@endif
								<div class="form-group">
									<label for="name">@lang('front.page_manage_preview_quiz.quiz_title')</label>
									<input placeholder="@lang('front.page_manage_preview_quiz.quiz_title')" type="text" class="form-control" name="name" value="{{ $name }}" required>
								</div>

								<div class="form-group">
									<label for="title">@lang('front.page_manage_preview_quiz.description')</label>
									<textarea name="description" id="description">{{ $description }}</textarea>
								</div>
								{{-- <div class="form-group">
									<label for="title">Acak?</label><br>
									<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
									<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
								</div> --}}

								<div class="accordion" id="accordionExample">
									<div class="">
										<div class="" id="headingOne">
											<h2 class="mb-0">
												<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													<i class="fa fa-th-list" aria-hidden="true"></i> @lang('front.page_manage_preview_quiz.timing')
													<hr style="margin-top:10px;margin-bottom:0">
												</button>
											</h2>
										</div>

										<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
											<div class="card-body">
												<div class="row">
													<div class="col-md-3">
														<p for="name">@lang('front.page_manage_preview_quiz.start_time')</p>
													</div>
													<div class="col-md-3 input_disabled">
														<div class="form-group">
															<input placeholder="@lang('front.page_manage_preview_quiz.start_date')" type="text" autocomplete="off" class="form-control datePicker input-required" name="date_start" value="{{date("Y-m-d", strtotime($quiz->time_start))}}" required {{$quiz->time_start_required == 0 ? 'disabled' : ''}}>
														</div>
													</div>
													<div class="col-md-3 input_disabled">
														<div class="form-group">
															<input placeholder="@lang('front.page_manage_preview_quiz.start_hour')" type="time" autocomplete="off" class="form-control input-required" name="time_start" value="{{date("H:i:s", strtotime($quiz->time_start))}}" required {{$quiz->time_start_required == 0 ? 'disabled' : ''}}>
														</div>
													</div>
													<div class="col-sm-2">
														<p>
															<input type="checkbox" value="1" name="time_start_required" class="activated_input" {{$quiz->time_start_required == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.activate')
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<p for="name">@lang('front.page_manage_preview_quiz.end_time')</p>
													</div>
													<div class="col-md-3 input_disabled">
														<div class="form-group">
															<input placeholder="@lang('front.page_manage_preview_quiz.end_date')" type="text" autocomplete="off" class="form-control datePicker input-required" name="date_end" value="{{date("Y-m-d", strtotime($quiz->time_end))}}" {{$quiz->time_end_required == 0 ? 'disabled' : ''}}>
														</div>
													</div>
													<div class="col-md-3 input_disabled">
														<div class="form-group">
															<input placeholder="@lang('front.page_manage_preview_quiz.end_hour')" type="time" autocomplete="off" class="form-control input-required" name="time_end" value="{{date("H:i:s", strtotime($quiz->time_end))}}" required {{$quiz->time_end_required == 0 ? 'disabled' : ''}}>
														</div>
													</div>
													<div class="col-sm-2">
														<p>
															<input type="checkbox" value="1" name="time_end_required" class="activated_input" {{$quiz->time_end_required == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.activate')
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<p for="name">@lang('front.page_manage_preview_quiz.duration') (@lang('front.page_manage_preview_quiz.minute'))</p>
													</div>
													<div class="col-md-2 input_disabled">
														<div class="form-group">
															<input placeholder="60" type="number" class="form-control input-required" name="duration" value="{{ $quiz->duration }}" required {{$quiz->duration_required == 0 ? 'disabled' : ''}}>
														</div>
													</div>
													<div class="col-sm-2">
														<p>
															<input type="checkbox" value="1" name="duration_required" class="activated_input" {{$quiz->duration_required == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.activate')
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="">
											<div class="" id="grading">
												<h2 class="mb-0 mt-0">
													<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapsegrading" aria-expanded="true" aria-controls="collapseOne">
														<i class="fa fa-th-list" aria-hidden="true"></i> @lang('front.page_manage_preview_quiz.grade')
														<hr style="margin-top:10px;margin-bottom:0">
													</button>
												</h2>
											</div>

											<div id="collapsegrading" class="collapse" aria-labelledby="grading" data-parent="#accordionExample">
												<div class="card-body">
													<div class="row">
														<div class="col-md-2">
															<p for="name">@lang('front.page_manage_preview_quiz.grade')</p>
														</div>
														<div class="col-md-2 input_disabled">
															<div class="form-group">
																<input placeholder="60" type="number" class="form-control input-required" name="grade" value="{{$quiz->grade}}" required {{$quiz->grade_required == 0 ? 'disabled' : ''}}>
															</div>
														</div>
														<div class="col-sm-2">
															<p>
																<input type="checkbox" value="1" name="grade_required" class="activated_input" {{$quiz->grade_required == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.activate')
															</p>
														</div>
													</div>
												</div>
											</div>
									</div>
									<div class="">
											<div class="" id="access">
												<h2 class="mb-0 mt-0">
													<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapseaccess" aria-expanded="true" aria-controls="collapseOne">
														<i class="fa fa-th-list" aria-hidden="true"></i> @lang('front.page_manage_preview_quiz.access')
														<hr style="margin-top:10px;margin-bottom:0">
													</button>
												</h2>
											</div>

											<div id="collapseaccess" class="collapse" aria-labelledby="access" data-parent="#accordionExample">
												<div class="card-body">
													<div class="row">
														<div class="col-md-2">
															<p for="name">@lang('front.page_manage_preview_quiz.publish')</p>
														</div>
														<div class="col-md-2">
															<div class="form-group">
																<p><input type="radio" value="1" name="publish" class="time_end_required" {{$quiz->status == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.yes')</p>
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-group">
																<p><input type="radio" value="2" name="publish" class="time_end_required" {{$quiz->status == 2 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.no')</p>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-2">
															<p for="name">@lang('front.page_manage_preview_quiz.attempt')</p>
														</div>
														<div class="col-md-6 input_disabled">
															<div class="form-group">
																<select class="form-control" name="attempt" style="padding: 5px 10px;" required>
																	<option value="-1" {{$quiz->attempt == -1 ? 'selected' : ''}}>@lang('front.page_manage_preview_quiz.unlimited')</option>
																	@for($i = 1; $i<(11);$i++)
																	<option value="{{$i}}" {{$quiz->attempt == $i ? 'selected' : ''}}>{{$i}}</option>
																	@endfor
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
									</div>
									<div class="">
											<div class="" id="page">
												<h2 class="mb-0 mt-0">
													<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapsepage" aria-expanded="true" aria-controls="collapseOne">
														<i class="fa fa-th-list" aria-hidden="true"></i> @lang('front.page_manage_preview_quiz.pagination')
														<hr style="margin-top:10px;margin-bottom:0">
													</button>
												</h2>
											</div>

											<div id="collapsepage" class="collapse" aria-labelledby="access" data-parent="#accordionExample">
												<div class="card-body">
													<div class="row">
														<div class="col-md-3">
															<p for="name">@lang('front.page_manage_preview_quiz.pagination_by')</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<p><input type="radio" value="0" name="paginate_type" {{$quiz->paginate_type == 0 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.page_break')</p>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<p><input type="radio" value="1" name="paginate_type" {{$quiz->paginate_type == 1 ? 'checked' : ''}}> @lang('front.page_manage_preview_quiz.questions')</p>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-3">
															<p for="name">@lang('front.page_manage_preview_quiz.page_show')</p>
														</div>
														<div class="col-md-3 input_disabled">
															<div class="form-group">
																<select class="form-control" name="paginate" style="padding: 5px 10px;" required>
																	@for($i = 1; $i<(11);$i++)
																	<option value="{{$i}}" {{$quiz->paginate == $i ? 'selected' : ''}}>{{$i}}</option>
																	@endfor
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
									</div>
								</div>

								<div class="form-group">
									{{  Form::hidden('id', $id) }}
									{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
									{{-- <a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a> --}}
								</div>
							{{ Form::close() }}
						</div>
						<div role="tabpanel" class="tab-pane fade" id="result">
							<div class="table-responsive">
								<table class="table table-hover" width="100%" id="data">
									<thead>
										<tr>
											<th>@lang('front.page_manage_preview_quiz.name')</th>
											<th>@lang('front.page_manage_preview_quiz.options')</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	{{-- add bank soal --}}
	<div class="modal" id="ModalShowBankSoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="d-flex align-items-center justify-content-between mb-2">
						<h3 class="headline headline-sm m-0">@lang('front.page_manage_preview_quiz.add_content_folder')</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								<i class="zmdi zmdi-close"></i>
							</span>
						</button>
					</div>
						<div class="form-group">
							<div class="table-responsive table-cs">
								<table class="table table-striped d-table edit-group">
									<thead>
										<tr>
											<th>@lang('front.page_manage_preview_quiz.content_name')</th>
											<th>@lang('front.page_manage_preview_quiz.date_created')</th>
											<th class="text-center">@lang('front.page_manage_preview_quiz.directory')</th>
											<th class="text-center">@lang('front.page_manage_preview_quiz.type')</th>
										</tr>
									</thead>
									<tbody>
										@foreach($group as $groups)
											<tr style="color: #4ca3d9;">
												<td>
													@if($groups->question_bank == 1)
													<a href="{{url('course/quiz/get', [$quiz->id, $groups->id])}}"><i class="fa fa-folder mr-1"></i> {{$groups->group_name}}</a>
													@else
													<a href="#"><i class="fa fa-folder-open mr-1"></i> {{$groups->group_name}}</a>
													@endif
												</td>
												<td>{{$groups->created_at}}</td>
												<td class="text-center">
													@if($groups->user_id == Auth::user()->id)
														@if($groups->group_code == null && $groups->status == '0')
															@lang('front.page_manage_preview_quiz.my_library')
														@elseif($groups->status == '1')
															@lang('front.page_manage_preview_quiz.public_library')
														@else
															@lang('front.page_manage_preview_quiz.shared_library')
														@endif
													@else
														@lang('front.page_manage_preview_quiz.shared_library')
													@endif
												</td>
												<td class="text-center">{{$groups->question_bank == 1 ? 'Bank Soal' : 'Folder'}}</td
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	{{-- add bank soal --}}

	{{-- add page break --}}
	<div class="modal fade" id="modalPageBreak" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
	    <div class="modal-content">
				<form class="" action="/course/quiz/create/pagebreak/{{$quiz->id}}" method="post">
					{{csrf_field()}}
		      <div class="modal-header">
		        <h4 class="modal-title" id="">@lang('front.page_manage_preview_quiz.add_page_break')</h4>
		      </div>
		      <div class="modal-body">
						<div class="form-group">
							<label for="title">@lang('front.page_manage_preview_quiz.page_break_title')</label>
							<input type="text" name="title" value="" class="form-control">
						</div>
						<div class="form-group">
							<label for="title">@lang('front.page_manage_preview_quiz.page_break_description')</label>
							<textarea name="description" class="form-control" rows="8" cols="80"></textarea>
						</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('front.page_manage_preview_quiz.cancel')</button>
		        <button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_preview_quiz.save')</button>
		      </div>
				</form>
	    </div>
	  </div>
	</div>
	{{-- add page break --}}

	{{-- edit page break --}}
	<div class="modal fade" id="modalEditPageBreak" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
	    <div class="modal-content">
				<form class="" action="/course/quiz/pagebreak/update" method="post">
					{{csrf_field()}}
					<input type="hidden" name="id" id="pageBreakId" value="">
		      <div class="modal-header">
		        <h4 class="modal-title" id="">@lang('front.page_manage_preview_quiz.edit_page_break')</h4>
		      </div>
		      <div class="modal-body">
						<div class="form-group">
							<label for="title">@lang('front.page_manage_preview_quiz.page_break_title')</label>
							<input type="text" id="pageBreakTitle" name="title" value="" class="form-control">
						</div>
						<div class="form-group">
							<label for="title">@lang('front.page_manage_preview_quiz.page_break_description')</label>
							<textarea name="description" id="pageBreakDescription" class="form-control" rows="8" cols="80"></textarea>
						</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('front.page_manage_preview_quiz.cancel')</button>
		        <button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_preview_quiz.save')</button>
		      </div>
				</form>
	    </div>
	  </div>
	</div>
	{{-- edit page break --}}

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg animated zoomIn animated-3x">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="">@lang('front.page_manage_preview_quiz.edit_option')</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">@lang('front.page_manage_preview_quiz.options')</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">@lang('front.page_manage_preview_quiz.option_status') ?</label>
						<select class="form-control selectpicker" id="edit_text_answer_correct">
							<option value="0">@lang('front.page_manage_preview_quiz.correct')</option>
							<option value="1">@lang('front.page_manage_preview_quiz.incorrect')</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('front.page_manage_preview_quiz.close')</button>
	        <button type="button" class="btn btn-primary btn-raised" id="save_edit_answer">@lang('front.page_manage_preview_quiz.save')</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}
@endsection

@push('script')

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}

	{{-- participant list --}}
	<script src="{{url('js/jquery.dataTables.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script> <!-- Datatable -->
	<script type="text/javascript">
	  $(function(){
			var asset_url = '{{asset_url()}}';
	    $($("#data").DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: '{{ url("course/quiz/participant/serverside/".$quiz->id) }}',
	      columns: [
					{ data: 'name', name: 'name' },
	        { data: 'action', 'searchable': false, 'orderable':false }
	      ],
				"oLanguage": {
					"sLengthMenu": "@lang('front.page_manage_preview_quiz.show') _MENU_",
					"sSearch": "@lang('front.page_manage_preview_quiz.search'): ",
		      "oPaginate": {
						"sPrevious": "<",
						"sNext": ">",
					}
		    },
	    }).table().container()).removeClass( 'form-inline' );
	  });
  </script>

	{{-- participant list --}}

	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>

<script type="text/javascript">
var js = document.createElement("script");
js.type = "text/javascript";
js.src = "WIRISplugins.js?viewer=image";
document.head.appendChild(js);
</script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script type="text/javascript">
$(".activated_input").change(function(){
	var cheked = $(this).prop('checked');
	if(cheked){
		$.each($(this).parents('.row').children('.input_disabled'), function(){
			$(this).find('.input-required').prop('disabled', false);
		})
	}else {
		$.each($(this).parents('.row').children('.input_disabled'), function(){
			$(this).find('.input-required').prop('disabled', true);
		})
	}

});
</script>

{{-- sortable --}}
<script src="/jquery-ui/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".PageBreakSortable" ).sortable({
      axis: 'y',
      connectWith: '.PageBreakSortable',
      helper: 'clone',
      out: function(event, ui) {
        var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
        console.log(itemOrder)

        $.ajax({
          url : '/course/quiz/page-break/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'quiz_id' : '{{$quiz->id}}',
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    });
    $( ".PageBreakSortable" ).disableSelection();
  });
</script>

<script>
  $( function() {
    var quiz_page_break_id = ''
    $( ".QuestionSortable" ).sortable({
      axis: 'y',
      connectWith: '.QuestionSortable',
      helper: 'clone',
      placeholder: "placeholder",
      over:function(event,ui){
        quiz_page_break_id = $('.placeholder').parent().attr('id');
      },
      out: function(event, ui) {
        var sequence = ui.item.index();
        var question_id = ui.item.attr('id');
        var itemOrder = $(this).sortable('toArray', { attribute: 'id' });
        console.log(itemOrder);
        // console.log(question_id);
        console.log(quiz_page_break_id);
        $.ajax({
          url : '/course/quiz/question/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'quiz_page_break_id' : quiz_page_break_id,
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    }).disableSelection();
  });
</script>
{{-- sortable --}}

{{-- edit pagebreak  --}}
<script type="text/javascript">
	$(function(){
		$(document).on('click','#editPageBreak',function(e){
			e.preventDefault();
			var data_id = $(this).attr('data-id');
			var data_title = $(this).attr('data-title');
			var data_description = $(this).attr('data-description');
			$("#pageBreakId").val(data_id);
			$("#pageBreakTitle").val(data_title);
			$("#pageBreakDescription").val(data_description);
			$("#modalEditPageBreak").modal('show');
		});
	});
</script>
{{-- edit pagebreak  --}}

{{-- preview trigger --}}
@if (Session::has('error'))
	<script>
		$('#button-nav-pengaturan').trigger('click');
	</script>
@endif
@endpush
