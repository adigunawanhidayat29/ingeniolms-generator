@extends('layouts.app')

@section('title')
	{{ 'Import Quiz' }}
@endsection


@section('content')
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Import Question & Answer</h3>
      </div>
      <div class="panel-body">
        <a href="/quiz_format/default_format.xlsx" class="btn btn-raised btn-success">Download Format Quiz</a>
        <br>
        <form action="/course/quiz/question/import_store/{{Request::segment(5)}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">Pilih file yang akan di import</label>
            <div class="col-md-12">
              <input type="text" readonly="" class="form-control" placeholder="Browse...">
              <input type="file" name="file">
            </div>
          </div>
          <input type="submit" value="Import" class="btn btn-primary btn-raised">
        </form>
      </div>
    </div>
  </div>
@endsection
