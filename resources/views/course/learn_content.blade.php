@extends('layouts.app-static')

@section('title', $course->title . " - " . $content->title)

@push('style')

  @if($content->type_content == 'discussion')
    <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  @endif

  <link rel="stylesheet" href="/css/preload.min.css">
  <link rel="stylesheet" href="/css/plugins.min.css">
  <link rel="stylesheet" href="/css/content-dq-style.css">
  <link rel="stylesheet" href="/css/content-dq-layout.css">
  <link rel="stylesheet" href="/css/content-sidebar.css">
  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">
  <link rel="shotcut icon" href="{!! isset(Setting('icon')->value) ? Setting('icon')->value : '/img/ingenio-logo.png' !!}">
  <link rel="stylesheet" type="text/css" href="{{asset('uploadify-master/sample/uploadifive.css')}}">


  <style media="screen">
    body {
      overflow-x: hidden;
      overflow-y: auto;
    }
    <?= $content->type_content == 'video' ? 'body{overflow: hidden;}' : '' ?>
    <?= $content->type_content == 'url-video' ? 'body{overflow: hidden;}' : '' ?>
    <?= $content->type_content == 'url-file' ? 'body{overflow: hidden;}' : '' ?>
    /* Jika quiz atau assignment maka body{overflow: hidden;} */
    .vjs-play-progress{
      background: red !important;
      height: 8px;
    }
    .vjs-big-play-button {
      font-size: 3em;
      line-height: 2em !important;
      height: 2em !important;
      width: 3em !important;
      display: block;
      position: relative;
      top: 50% !important;
      left: 50% !important;
      padding: 0;
      cursor: pointer;
      opacity: 1;
      border: 0.06666em solid #fff;
      background-color: red !important;
      -webkit-border-radius: 0.3em;
      -moz-border-radius: 0.3em;
      border-radius: 0.3em;
      -webkit-transition: all 0.4s;
      -moz-transition: all 0.4s;
      -ms-transition: all 0.4s;
      -o-transition: all 0.4s;
      transition: all 0.4s;
    }
    .video-js .vjs-time-control{
      flex: none;
      font-size: 1em;
      line-height: 5.2em;
      min-width: 2em;
      width: auto;
      padding-left: 1em;
      padding-right: 1em;
    }
    .vjs-volume-bar .vjs-slider-horizontal{
      width: 5em;
      height: 1.3em;
    }
    .video-js .vjs-volume-bar{
      margin: 2.35em 0.45em;
      margin-top: 2.35em;
      margin-right: 0.45em;
      margin-bottom: 2.35em;
      margin-left: 0.45em;
    }
    .vjs-playback-rate .vjs-playback-rate-value{
      pointer-events: none;
      font-size: 1.5em;
      line-height: 3.3em;
      text-align: center;
    }
    .vjs-menu-button-popup .vjs-menu .vjs-menu-content{
      background-color: rgba(43, 51, 63, 0.7);
      position: absolute;
      width: 100%;
      bottom: 3.5em;
      max-height: 15em;
    }
    .video-js .vjs-control-bar{
      width: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 5.0em;
      background-color: rgba(43, 51, 63, 0.7);
    }
    .vjs-button > .vjs-icon-placeholder::before {
      font-size: 1.8em;
      line-height: 2.8em;
    }
  </style>

  <style media="screen">

    .player-content--text:before {
      content: '';
      position: fixed;
      display: block;
      background: linear-gradient(180deg, #000 -5%, transparent 10%);
      width: 100%;
      height: 100%;
      opacity: 0;
      top: 0;
      z-index: 0;
      transition: 0.3s ease-in-out;
    }

    <?php if($content->type_content == 'discussion'){ ?>

      .player-content--text:before {
        background: transparent; /* default linear-gradient(180deg, #333, transparent) */
      }

    <?php } ?>
    .embed-responsive:before {
      content: '';
      position: relative;
      display: block;
      background: linear-gradient(180deg, #000 -5%, transparent 10%);
      height: 100%;
      opacity: 0;
      z-index: 1; /* Jika quiz atau assignment z-index: 0; */
      transition: 0.3s ease-in-out;
    }

    .player-content--text:hover:before,
    .embed-responsive:hover:before,
    .embed-responsive:hover .player-navbar {
      opacity: 1;
      /* -moz-animation: cssAnimation 0.5s ease-out 3s;
      -webkit-animation: cssAnimation 0.5s ease-out 3s;
      -o-animation: cssAnimation 0.5s ease-out 3s;
      animation: cssAnimation 0.5s ease-out 3s;
      -webkit-animation-fill-mode: forwards;
      animation-fill-mode: forwards; */
    }

    @keyframes cssAnimation {
      to {
        opacity: 0;
      }
    }
    @-webkit-keyframes cssAnimation {
      to {
        opacity: 0;
      }
    }

    .player-content--text .player-navbar .content-toggle,
    .embed-responsive .player-navbar .content-toggle {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #222;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-title,
    .embed-responsive .player-navbar .content-title {
      color: #fff;
      line-height: 50px;
      padding: 0 1rem;
      margin: 0;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-close,
    .embed-responsive .player-navbar .content-close {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #e41f1f;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0
    }

    .player-content--text .player-navbar {
      position: sticky;
      margin-bottom: 1rem;
    }

    .player-content--text .player-navigate {
      position: sticky;
      top: calc(50% - 50px);
      z-index: 0;
    }

    .player-content--text .row.player-content-group {
      margin-top: -50px;
      margin-left: 0;
      margin-right: 0;
    }

    .player-content--text .player-attach {
      background: #eee;
      padding: 2rem 0;
      margin: 0;
    }

    .player-content--text .player-attach .attach-link {
      color: #007bff;
    }

    .player-content--text .player-attach .attach-link:hover {
      color: #004b9c;
    }

    .player-content--text:hover .player-navbar .content-toggle,
    .player-content--text:hover .player-navbar .content-title,
    .player-content--text:hover .player-navbar .content-close,
    .embed-responsive:hover .player-navbar .content-toggle,
    .embed-responsive:hover .player-navbar .content-title,
    .embed-responsive:hover .player-navbar .content-close {
      opacity: 1;
    }

    .embed-responsive:hover .vjs-big-play-centered .vjs-big-play-button,
    .embed-responsive:hover .video-js .vjs-control-bar {
      z-index: 2;
    }

    .player-content--text:hover .player-navbar .content-toggle:hover,
    .embed-responsive:hover .player-navbar .content-toggle:hover {
      background: #111;
      color: #fff;
    }

    .player-content--text:hover .player-navbar .content-close:hover,
    .embed-responsive:hover .player-navbar .content-close:hover {
      background: #cc3333;
      color: #fff;
    }

    .player-navbar {
      position: absolute;
      width: 100%;
      opacity: 1;
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      top: 0;
      z-index: 999;
      transition: 0.5s ease-in-out;
    }

    .player-navigate {
      position: absolute;
      display: flex;
      width: 100%;
      height: 100%;
      align-items: center;
      justify-content: space-between;
      top: 0;
    }

    .player-navigate .navigate {
      display: flex;
      height: auto;
      background: rgba(0, 0, 0, 0.25);
      align-items: center;
      justify-content: center;
      font-size: 26px;
      color: #fff;
      padding: 0.75rem;
      cursor: pointer;
      z-index: 3;
      transition: 0.3s ease-in-out;
    }

    .player-navigate .navigate:hover {
      background: rgba(0, 0, 0, 0.75);
    }

    #sidebar .player-content-headbox {
      position: sticky;
      display: flex;
      background: #f3f3f3;
      align-items: center;
      font-weight: 600;
      color: #333;
      top: 0;
      z-index: 10;
    }

    #sidebar .player-content-headbox .headbox-button {
      display: inline-flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      font-size: 20px;
      border-right: 2px solid #e3e3e3;
      cursor: pointer;
      padding: 1rem;
    }

    #sidebar .player-content-headbox .headbox-button.button-right {
      display: none;
      border-left: 2px solid #e3e3e3;
      border-right: none;
    }

    #sidebar .player-content-headbox .headbox-group {
      display: flex;
      align-items: center;
      width: calc(100% - 50px);
      height: 50px;
      padding: 1rem;
    }

    #sidebar .player-content-list {
      display: block;
      overflow-y: auto;
      height: calc(100% - 50px);
    }

    #sidebar .player-content-list .head-of-list {
      background: #ececec;
      display: block;
      padding: 0.75rem;
    }

    #sidebar .player-content-list .content-of-list {
      background: #f3f3f3;
      display: block;
      color: #999;
      /* color: #333; */
      padding: 0.75rem;
      font-weight: 600;
      transition: all 0.1s;
    }

    #sidebar .player-content-list .content-of-list:hover {
      color: #333;
    }

    #sidebar .player-content-list li.active .content-of-list {
      background: #dae5eb;
      color: #333;
      border-left: 5px solid #333;
    }

    #sidebar .player-content-list .content-of-list .list-row {
      display: flex;
      align-items: baseline;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-icon,
    #sidebar .player-content-list .content-of-list .list-row .list-text,
    #sidebar .player-content-list .content-of-list .list-row .list-check {
      padding-left: 5px;
      padding-right: 5px;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-check {
      margin-left: auto;
    }

    #content {
      background: {{$content->type_content == 'video' || $content->type_content == 'url-video' || $content->type_content == 'scorm' ? 'black' : 'white'}};
    }

    #content .player-content-headbox,
    #content .player-content-list {
      display: none;
    }

    @media (max-width: 768px) {
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }
      .embed-responsive-16by9::before{
        padding-top: 56.25%;
      }
      .player-navbar {
        display: none;
      }
    }

    @media (max-width: 420px) {
      body {
        overflow-x: hidden;
        overflow-y: auto;
      }
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }

      .player-content--text .player-navbar {
        display: flex;
      }

      .player-content--text .player-navigate {
        z-index: 1;
      }

      .player-content--text .row .content--text * {
        font-size: 14px;
      }

      .player-content--text .player-navbar .content-toggle,
      .player-content--text .player-navbar .content-close,
      .embed-responsive .player-navbar .content-toggle,
      .embed-responsive .player-navbar .content-close {
        width: 50px;
        height: 50px;
        opacity: 1
      }

      .player-content--text .player-navbar .content-title,
      .embed-responsive .player-navbar .content-title,
      .player-content--text .player-navbar .content-close {
        display: none;
      }

      .player-navigate {
        position: absolute;
        display: flex;
        width: 100%;
        height: 100%;
        align-items: center;
        justify-content: space-between;
        top: 0;
      }

      .player-navigate .navigate {
        font-size: 26px;
        padding: 0.5rem;
      }

      #sidebar .player-content-headbox .headbox-group {
        width: calc(100% - 100px);
      }

      #sidebar .player-content-headbox .headbox-button.button-right {
        display: inline-flex;
      }

      #sidebar .player-content-list {
        height: calc(100% + 20px);
      }

      #content {
        background: {{$content->type_content == 'video' || $content->type_content == 'url-video' || $content->type_content == 'scorm'  ? '#f9f9f9' : 'white'}}
      }

      #content .container-full.controlable {
        position: sticky;
        top: 0;
        z-index: 1;
      }

      #content .player-content-headbox {
        position: fixed; /* addon */
        width: 100%; /* addon */
        background: #f3f3f3;
        display: flex;
        align-items: center;
        z-index: 1; /* addon */
      }

      #content .player-content-headbox .headbox-group {
        width: calc(100% - 50px);
        padding: 0.75rem;
      }

      #content .player-content-headbox .headbox-button {
        width: 50px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        border-right: 2px solid #e3e3e3;
        padding: 0.75rem;
      }

      #content .player-content-list {
        background: #f9f9f9;
        display: block;
        padding-bottom: 0.5rem; /* addon */
        margin-top: 55px; /* addon */
      }

      #content .player-content-list .head-of-list {
        background: #ececec;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list .content-of-list {
        background: #f3f3f3;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list li.active .content-of-list {
        background: #dae5eb;
      }

      #content .player-content-list .content-of-list .list-row {
        display: flex;
        align-items: baseline;
      }

      #content .player-content-list .content-of-list .list-row .list-icon,
      #content .player-content-list .content-of-list .list-row .list-text,
      #content .player-content-list .content-of-list .list-row .list-check {
        padding-left: 5px;
        padding-right: 5px;
      }
    }
  </style>

  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .upload-page{
      background: #f8f8f8;
      border: 1px solid #d1d1d1;
      border-top: none;
      padding: 10px 15px;
      padding-bottom: 5px;
    }

    .upload-page input{
      /* z-index: 999; */
      /* display: none; */
    }

    .hidden{
      display: none;
    }

    .komentar-first{
      border: 1px solid #d3cfcf;
      height: 70px;
      margin-top: 10px;
      cursor: pointer;
      padding: 10px 20px;
      color: #c7c7c7;
    }
  </style>

  @if($content->type_content == 'discussion')
    <!-- Bootstrap styles -->
    <!-- <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous"
    /> -->

      <!-- Generic page styles -->
      <style>
        #navigation {
          margin: 10px 0;
        }
        @media (max-width: 767px) {
          #title,
          #description {
            display: none;
          }
        },
        #sidebar .player-content-list .content-of-list {
          font-size: 14px !important;
        }
        .uploadifive-button:hover {
            background-color: none;
        }
      </style>
      <!-- blueimp Gallery styles -->
      <link
        rel="stylesheet"
        href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"
      />
      <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
      <link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload.css" />
      <link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui.css" />
      <!-- CSS adjustments for browsers with JavaScript disabled -->
      <noscript
        ><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-noscript.css"
      /></noscript>
      <noscript
        ><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui-noscript.css"
      /></noscript>
  @endif

  @if($content->type_content == 'scorm')
    <style type="text/css">
      iframe {
        width: 100%;
        z-index: 2;
      }
      #btnExitAll {
        display: none;
      }
    </style>
    <script src="/js/scorm/sscompat.js" type="text/javascript"></script>
    <script src="/js/scorm/sscorlib.js" type="text/javascript"></script>
    <script src="/js/scorm/ssfx.Core.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/scorm/API_BASE.js"></script>
    <script type="text/javascript" src="/js/scorm/API.js"></script>
    <script type="text/javascript" src="/js/scorm/Controls.js"></script>
    <script type="text/javascript" src="/js/scorm/Player.js"></script>

    <script type="text/javascript">
      function InitPlayer() {
        PlayerRun();
      }
      function PlayerRun() {
        var contentDiv = document.getElementById('placeholder_contentIFrame');
        contentDiv.innerHTML = "";
        Run.ManifestByURL("{{asset_url($content->path_file)}}", true);
      }
      InitPlayer()
    </script>
  @endif
@endpush

@section('content')

  @if($content->type_content == 'scorm') <body onload="InitPlayer()"> @endif

    <div id="wrapper" class="wrapper">
      <nav id="sidebar">
        <div class="player-content-headbox">
          {{-- <a class="headbox-button" href="/course/learn/{{ $course->slug }}" title="Back to course">
            <i class="zmdi zmdi-arrow-left"></i>
          </a> --}}
          <a class="headbox-button" href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" title="@lang('front.page_learn_content.back_to_courses')">
            <i class="zmdi zmdi-arrow-left"></i>
          </a>
          <div class="headbox-group">
            <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
          </div>
          <a id="sidebarCollapseOff" class="headbox-button button-right" href="#" title="Hide menu">
            <i class="zmdi zmdi-close"></i>
          </a>
        </div>
        <div class="player-content-list">
          <ul class="list-unstyled components">
            <?php $i=1; ?>
            @foreach($sections as $section)
              <li class="padding-10">
                <a class="head-of-list" href="#page{{$i}}" data-toggle="collapse" aria-expanded="false">
                  <p class="fs-12 nopadding nomargin">Section {{$i}}:</p>
                  <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
                </a>
                <ul class="collapse list-unstyled {{$content->id_section == $section['id'] ? 'show in' : ''}}" id="page{{$i}}">

                  @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                    @if($all->section_type == 'content')
                      @php $content_section = $all; @endphp
                      @php
                        $href = '/course/learn/content/'.$course->slug.'/'.$content_section->id;
                      @endphp
                      @if($content_section->type_content != 'label')
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="{{$href}}">
                            <div class="list-row">
                              <i class="list-icon {{content_type_icon($content_section->type_content)}}"></i>
                              <div class="list-text">{{$content_section->title ? $content_section->title : strip_tags($content_section->description)}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endif
                    @endif

                    @if($all->section_type == 'quizz')
                      @php $quiz = $all; @endphp
                      <li class="">
                        <a class="content-of-list" href="{{'/course/learn/quiz/'.$course->slug.'/'. $quiz->id}}">
                          <div class="list-row">
                            <i class="list-icon fa fa-star"></i>
                            <div class="list-text">{{$quiz->name}}</div>
                            <div class="list-check checkbox">
                            </div>
                          </div>
                        </a>
                      </li>
                    @endif

                    @if($all->section_type == 'assignment')
                      @php $assignment = $all; @endphp
                      @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                        <li class="">
                          <a class="content-of-list" href="{{'/course/learn/assignment/'.$course->slug.'/'. $assignment->id}}">
                            <div class="list-row">
                              <i class="list-icon fa fa-tasks"></i>
                              <div class="list-text">{{$assignment->title}}</div>
                              <div class="list-check checkbox">
                              </div>
                            </div>
                          </a>
                        </li>
                      @endif
                    @endif
                  @endforeach
                </ul>
              </li>
              <?php $i++; ?>
            @endforeach
          </ul>
        </div>
      </nav>

      <div id="content">
        <div id="contentBody">
          @if($content->type_content == 'video')
            @if($content->full_path_original)
              @php
                $fixedUrl = '';
                if(strpos($content->full_path_original, "https://") != false){
                  $fixedUrl = server_assets() . $content->full_path_original;
                } else {
                  $fixedUrl = $content->full_path_original;
                }
              @endphp
              <div class="container-full controlable">
                <div class="embed-responsive embed-responsive-16by9" style="position: relative; height: 100%;">
                  <nav class="player-navbar">
                    <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                      <i class="zmdi zmdi-menu"></i>
                    </a>
                    <p class="content-title text-truncate">{{$content->title}}</p>
                    <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                      <i class="zmdi zmdi-close"></i>
                    </a>
                  </nav>
                  <div class="player-navigate">
                    <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                    <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
                  </div>
                  <video
                    id="my-video"
                    class="video-js embed-responsive-item vjs-big-play-centered"
                    controls="true"
                    preload="auto"
                    autoplay="true"
                    width="100%"
                    height="100%">
                    <source src="{{$fixedUrl}}" type='video/mp4'>
                  </video>
                </div>
              </div>
            @else
              <div style="margin-top:50px; !important" class="text-center">
                <span class="alert alert-warning">@lang('front.page_learn_content.video_have_problem')</span>
              </div>
            @endif
            <div class="player-content-headbox">
              <a class="headbox-button" href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}"><i class="zmdi zmdi-arrow-left"></i></a>
              <div class="headbox-group">
                <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
              </div>
            </div>
            <div class="player-content-list">
              <ul class="list-unstyled components">
                <?php $i=1; ?>

                @foreach($sections as $section)
                  <li class="padding-10">
                    <a class="head-of-list" href="#section{{$i}}" data-toggle="collapse" aria-expanded="false">
                      <p class="fs-12 nopadding nomargin">Section {{$i}}:</p>
                      <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
                    </a>
                    <ul class="collapse list-unstyled {{$content->id_section == $section['id'] ? 'show in' : ''}}" id="section{{$i}}">

                      @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                        @if($all->section_type == 'content')
                        @php $content = $all; @endphp
                          <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                            <a class="content-of-list" href="/course/learn/content/{{$course->slug}}/{{$content_section->id}}">
                              <div class="list-row">
                                <i class="list-icon {{content_type_icon($content_section->type_content)}}"></i>
                                <div class="list-text">{{$content_section->title}}</div>
                                <div class="list-check checkbox">
                                </div>
                              </div>
                            </a>
                          </li>
                        @endif

                        @if($all->section_type == 'quizz')
                          @php $quiz = $all; @endphp
                          <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                            <a class="content-of-list" href="{{'?quiz=/quiz/'. $quiz->id}}">
                              <div class="list-row">
                                <i class="list-icon fa fa-star"></i>
                                <div class="list-text">{{$quiz->name}}</div>
                                <div class="list-check checkbox">
                                </div>
                              </div>
                            </a>
                          </li>
                        @endif

                        @if($all->section_type == 'assignment')
                          @php $assignment = $all; @endphp
                          @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                            <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                              <a class="content-of-list" href="{{'?assignment=/assignment/'. $assignment->id}}">
                                <div class="list-row">
                                  <i class="list-icon fa fa-tasks"></i>
                                  <div class="list-text">{{$assignment->title}}</div>
                                  <div class="list-check checkbox">
                                  </div>
                                </div>
                              </a>
                            </li>
                          @endif
                        @endif
                      @endforeach

                    </ul>
                  </li>
                <?php $i++; ?>
                @endforeach
              </ul>
            </div>
          @endif

          @if($content->type_content == 'text' || $content->type_content == 'file' || $content->type_content == 'label')
            <div class="container-full">
              <div class="player-content--text">
                <nav class="player-navbar">
                  <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                    <i class="zmdi zmdi-menu"></i>
                  </a>
                  <p class="content-title text-truncate">{{$content->title}}</p>
                  <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                    <i class="zmdi zmdi-close"></i>
                  </a>
                </nav>
                <div class="player-navigate">
                  <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                  <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
                </div>
                <div class="row player-content-group">
                  <div class="col-md-8 mx-auto">
                    <h2 class="headline headline-md"><span>{{ $content->title }}</span></h2>
                    <div class="divider divider-dark"></div>
                    <div class="content--text">{!! $content->description !!}</div>
                  </div>
                </div>
                @if($content->path_file != NUll)
                  <div class="row player-attach">
                    <div class="col-md-8 mx-auto">
                      <h3 class="headline headline-sm"><span>@lang('front.page_learn_content.attachment')</span></h3>
                      <a class="attach-link" href="{{$content->full_path_file}}" target="_blank"><i class="fa fa-save"></i> Download {{$content->name_file}}</a>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          @endif

          @if($content->type_content == 'discussion')
          <div class="container-full">
            <div class="player-content--text">
              <nav class="player-navbar">
                <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                  <i class="zmdi zmdi-menu"></i>
                </a>
                <p class="content-title text-truncate">{{$content->title}}</p>
                <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                  <i class="zmdi zmdi-close"></i>
                </a>
              </nav>
              <div class="player-navigate">
                <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
              </div>

              <div class="container">
                  <div class="row player-content-group">
                      <div class="col-md-12">

                          <center>
                              @if(Session::has('success'))
                              <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  {!!Session::get('success')!!}
                              </div>
                              @elseif(Session::has('error'))
                              <div class="alert alert-error alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  {!!Session::get('error')!!}
                              </div>
                              @endif
                          </center>

                          <h2 class="headline headline-md" style="margin-bottom: 1.5rem;"><span>{{$content->title}}</span></h2>

                          <div class="card-assignment">
                              {{-- <div class="card-header">
                                  <h3 class="card-title">{{$content->title}}</h3>
                              </div> --}}
                              <div class="card-block">
                                  {!!$content -> description !!}
                                  <hr>
                              </div>
                          </div>
                          <div class="card-assignment">
                              <div class="card-header">
                                  <h3 class="card-title">@lang('front.page_learn_content.comment')</h3>
                              </div>
                              <div class="card-block">

                                  @if($errors->any())
                                      {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                                  @endif
                                  <div class="komentar-first">@lang('front.page_learn_content.give_your_comment')</div>
                                  <div class="komentar-second hidden">
                                    {{ Form::open(array('url' => '/course/content/discussion/submit/'.$content->id, 'method' => 'post', 'enctype' => 'multipart/form-data')) }}

                                    <textarea name="body" id="answer" class="form-control" required="required"></textarea>

                                    <div class="upload-file-new" style="background: #f8f8f8;padding: 0px 10px;border: 1px solid #d1d1d1;">
                                      <input id="file_upload" name="file_upload" type="file" multiple="true">
                                      <div id="queue"></div>
                                    </div>

                                    <div class="form-group">
                                        {{  Form::submit("Submit Komentar" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
                                        <input type="button" class="btn btn-default btn-raised" id="cancel-komen" name="submit" value="Batal">

                                    </div>
                                    {{ Form::close() }}
                                  </div>
                              </div>

                          </div>
                          @if($content->discussion)
                            @foreach($content->discussion->where('level_1', null) as $answer)
                            <div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
                              @if($answer->user_id == Auth::user()->id)
                              <div class="groups-dropdown">
                                <div class="groups-dropdown-group">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <ul class="groups-dropdown-menu">
                                    <!-- <li>
                                      <a class="groups-dropdown-item" href="#">
                                        <i class="fa fa-pencil"></i> Ubah
                                      </a>
                                    </li> -->
                                    <li>
                                      <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$answer->id}}').submit()">
                                        <i class="fa fa-trash"></i> Hapus
                                      </a>
                                      <form action="{{url('/course/content/discussion/delete/'.$answer->id)}}" id="form-koment-delete-{{$answer->id}}" method="post" style="display:none;">
                                        {{csrf_field()}}
                                      </form>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              @endif
                              <div class="card-block">
                                <img src="{{avatar($answer->user->photo)}}" style="width: 25px; height: 25px; object-fit: cover; border-radius: 100%;    margin-right: 10px;" alt="{{Auth::user()->name}}">
                                <a class="groups-title" href="#">{{$answer->user->name}}</a>
                                <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($answer->created_at))}}&nbsp;&middot;&nbsp;</p>
                                <p class="groups-description">{!!$answer->body!!}</p>
                                @if($answer->file)
                                <?php
                                  $files = json_decode($answer->file);
                                  usort($files, function ($a, $b) use ($files) {

                                      // preg_match("/(\d+(?:-\d+)*)/", $a[0], $matches);
                                      // $firstimage = $matches[1];
                                      // preg_match("/(\d+(?:-\d+)*)/", $b[0], $matches);
                                      // $lastimage = $matches[1];
                                      $ext = explode('.',$a[0]);
                                      $ext = strtolower($ext[count($ext)-1]);
                                      $firstimage = $ext;
                                      $ext = explode('.',$b[0]);
                                      $ext = strtolower($ext[count($ext)-1]);
                                      $lastimage = $ext;


                                      return ($firstimage != 'png') ? -1 : 1;
                                  });
                                ?>
                                <p class="fs-14" style="color: #999;">Attachment</p>
                                  <div class="row">
                                    @foreach($files as $file)
                                      <?php
                                          $ext = explode('.',$file[0]);
                                          $ext = strtolower($ext[count($ext)-1]);
                                          $filename = $file[1];
                                          $img = asset_url('uploads/fileUpload/discussion/'.$file[0]);
                                          if($ext == 'pdf'){
                                            $img = asset('img/pdf.png');
                                          }elseif ($ext == 'xlsx') {
                                            $img = asset('img/excel.png');
                                          }elseif ($ext == 'docx') {
                                            $img = asset('img/word.png');
                                          }
                                       ?>
                                      @if($ext == 'png' || $ext == 'jpeg' || $ext == 'jpg')
                                        <div class="col-lg-3">
                                          <div class="img-attachment">
                                            <img src="{{$img}}" style="width:100%;height: 90px;border-radius: 11px;margin-bottom: 10px;" alt="{{$filename}}" title="{{$filename}}">
                                            <a class="groups-title" href="{{asset_url('uploads/fileUpload/discussion/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                                          </div>
                                        </div>
                                      @else
                                      <div class="col-lg-12">
                                        <div class="img-attachment">
                                          <img src="{{$img}}" style="width:50px;margin-right:20px;" alt="{{$filename}}" title="{{$filename}}">
                                          <a class="groups-title" href="{{asset_url('uploads/fileUpload/discussion/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                                        </div>
                                      </div>
                                      @endif
                                    @endforeach
                                  </div>
                                @endif
                                <div class="reply-comment">
                                  <hr style="border-color:#bbb">
                                  <button class="reply-btn btn btn-info btn-raised">Balas</button>
                                  <div class="text-reply">
                                    <form method="POST" action="{{url('/course/content/discussion/submit/'.$content->id)}}" accept-charset="UTF-8" enctype="multipart/form-data">
                                      {{csrf_field()}}
                                      <input name="parents" type="hidden" value="{{$answer->id}}">
                                      {{ Form::open(array('url' => '/course/content/discussion/submit/'.$content->id, 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                                      <textarea name="body" id="discussion-reply-{{$answer->id}}" class="form-control discussion-reply" required="required"></textarea>
                                      <input class="btn btn-primary btn-raised" name="button" type="submit" value="Submit Komentar">
                                    </form>
                                  </div>
                                  <div class="reply_1-page">
                                    @foreach($answer->reply_1 as $reply_1)
                                    <div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
                                      @if($reply_1->user_id == Auth::user()->id)
                                      <div class="groups-dropdown">
                                        <div class="groups-dropdown-group">
                                          <i class="fa fa-ellipsis-v"></i>
                                          <ul class="groups-dropdown-menu">
                                            <!-- <li>
                                              <a class="groups-dropdown-item" href="#">
                                                <i class="fa fa-pencil"></i> Ubah
                                              </a>
                                            </li> -->
                                            <li>
                                              <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$reply_1->id}}').submit()">
                                                <i class="fa fa-trash"></i> Hapus
                                              </a>
                                              <form action="{{url('/course/content/discussion/delete/'.$reply_1->id)}}" id="form-koment-delete-{{$reply_1->id}}" method="post" style="display:none;">
                                                {{csrf_field()}}
                                              </form>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      @endif
                                      <div class="card-block">
                                        <a class="groups-title" href="#">{{$reply_1->user->name}}</a>
                                        <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($reply_1->created_at))}}&nbsp;&middot;&nbsp;</p>
                                        <p class="groups-description">{!!$reply_1->body!!}</p>
                                      </div>
                                    </div>
                                    @endforeach
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endforeach
                          @endif

                      </div>
                  </div>
              </div>
            </div>
          </div>
          @endif

          @if($content->type_content == 'folder')
          <div class="container-full">
            <div class="player-content--text">
              <nav class="player-navbar">
                <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                  <i class="zmdi zmdi-menu"></i>
                </a>
                <p class="content-title text-truncate">{{$content->title}}</p>
                <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                  <i class="zmdi zmdi-close"></i>
                </a>
              </nav>
              <div class="player-navigate">
                <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
              </div>

              <div class="container">
                  <div class="row player-content-group">
                      <div class="col-md-12">

                          <center>
                              @if(Session::has('success'))
                              <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  {!!Session::get('success')!!}
                              </div>
                              @elseif(Session::has('error'))
                              <div class="alert alert-error alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  {!!Session::get('error')!!}
                              </div>
                              @endif
                          </center>

                          <h2 class="headline headline-md" style="margin-bottom: 1.5rem;"><span>{{$content->title}}</span></h2>

                          <div class="card-assignment">
                              {{-- <div class="card-header">
                                  <h3 class="card-title">{{$content->title}}</h3>
                              </div> --}}
                              <div class="card-block">
                                  {!!$content -> description !!}
                                  <hr>
                              </div>
                          </div>
                          <div class="card-assignment">
                              <div class="card-header">
                                  <h3 class="card-title">Daftar File</h3>
                              </div>
                              <div class="card-block">
                                <table id="example" class="table table-striped d-table">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th style="min-width: 25vh;">File</th>
                                      <th>Tipe</th>
                                      <th class="text-center">Size</th>
                                      <th class="text-center">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($content->files as $index => $item)
                                    <?php
                                      $file_name = explode('-',$item->file_name);
                                      unset($file_name[0]);
                                      $file_name = implode('-',$file_name);
                                    ?>
                                      <tr>
                                        <td>{{$index+1}}</td>
                                        <td style="min-width: 25vh;">
                                          @if($item->file_type == 'pdf')
                                              <a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/pdf.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
                                          @elseif($item->file_type == 'docx')
                                              <a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/word.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
                                          @else
                                              <a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{$item->file_path}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
                                          @endif
                                        </td>
                                        <td>{{$item->file_type}}</td>
                                        <td class="text-center">{{formatSizeUnits($item->file_size)}}</td>
                                        <td class="text-center">Action</td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>

                          </div>


                      </div>
                  </div>
              </div>
            </div>
          </div>
          @endif

          @if($content->type_content == 'url-video')
            <div class="container-full controlable">
              <div class="embed-responsive embed-responsive-16by9" style="position: relative; height: 100%;">
                <nav class="player-navbar">
                  <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                    <i class="zmdi zmdi-menu"></i>
                  </a>
                  <p class="content-title text-truncate">{{$content->title}}</p>
                  <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                    <i class="zmdi zmdi-close"></i>
                  </a>
                </nav>
                <div class="player-navigate">
                  <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                  <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
                </div>
                <video
                  id="my-video"
                  class="video-js embed-responsive-item vjs-big-play-centered"
                  controls="true"
                  preload="auto"
                  autoplay="true"
                  width="100%"
                  height="100%"
                  data-setup='{ "techOrder": ["youtube", "html5"], "sources": [{ "type": "video/youtube", "src": "{{$content->full_path_file}}"}] }'
                ></video>
              </div>
            </div>
            <div class="player-content-headbox">
              <a class="headbox-button" href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}"><i class="zmdi zmdi-arrow-left"></i></a>
              <div class="headbox-group">
                <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
              </div>
            </div>
            <div class="player-content-list">
              <ul class="list-unstyled components">
                <?php $i=1; ?>
                @foreach($sections as $section)
                  <li class="padding-10">
                    <a class="head-of-list" href="#section{{$i}}" data-toggle="collapse" aria-expanded="false">
                      <p class="fs-12 nopadding nomargin">@lang('front.page_learn_content.section') {{$i}}:</p>
                      <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
                    </a>
                    <ul class="collapse list-unstyled {{$content->id_section == $section['id'] ? 'show in' : ''}}" id="section{{$i}}">
                      @foreach($section['section_contents'] as $content_section)
                        @php
                          // $content_progress = DB::table('progresses')->where(['content_id'=>$content_section->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                        @endphp
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="/course/learn/content/{{$course->slug}}/{{$content_section->id}}">
                            <div class="list-row">
                              <i class="list-icon {{content_type_icon($content_section->type_content)}}"></i>
                              <div class="list-text">{{$content_section->title}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_quizzes'] as $quiz)
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="{{'?quiz=/quiz/'. $quiz->id}}">
                            <div class="list-row">
                              <i class="list-icon fa fa-star"></i>
                              <div class="list-text">{{$quiz->name}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_assignments'] as $assignment)
                        @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                          <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                            <a class="content-of-list" href="{{'?assignment=/assignment/'. $assignment->id}}">
                              <div class="list-row">
                                <i class="list-icon fa fa-tasks"></i>
                                <div class="list-text">{{$assignment->title}}</div>
                                <div class="list-check checkbox">
                                  {{-- <label class="nomargin">
                                    <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                  </label> --}}
                                </div>
                              </div>
                            </a>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </li>
                <?php $i++; ?>
                @endforeach
              </ul>
            </div>
          @endif

          @if($content->type_content == 'scorm')
            <div class="container-full controlable">
              <div class="embed-responsive embed-responsive-16by9" style="position: relative; height: 100%;">
                <nav class="player-navbar">
                  <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                    <i class="zmdi zmdi-menu"></i>
                  </a>
                  <p class="content-title text-truncate">{{$content->title}}</p>
                  <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                    <i class="zmdi zmdi-close"></i>
                  </a>
                </nav>
                <div class="player-navigate">
                  <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                  <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
                </div>
                {{-- <iframe src="{{url($content->full_path_file)}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 100%; z-index: 2">
                  Your browser doesnot support iframes <a href="{{$content->full_path_file}}"> click here to view the page directly. </a>
                </iframe> --}}
                <div id="placeholder_contentIFrame" style="z-index: 2 !important;width: 100%; height: 100%;overflow:auto;-webkit-overflow-scrolling:touch;"></div>
              </div>
            </div>
            <div class="player-content-headbox">
              <a class="headbox-button" href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}"><i class="zmdi zmdi-arrow-left"></i></a>
              <div class="headbox-group">
                <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
              </div>
            </div>
            <div class="player-content-list">
              <ul class="list-unstyled components">
                <?php $i=1; ?>
                @foreach($sections as $section)
                  <li class="padding-10">
                    <a class="head-of-list" href="#section{{$i}}" data-toggle="collapse" aria-expanded="false">
                      <p class="fs-12 nopadding nomargin">@lang('front.page_learn_content.section') {{$i}}:</p>
                      <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
                    </a>
                    <ul class="collapse list-unstyled {{$content->id_section == $section['id'] ? 'show in' : ''}}" id="section{{$i}}">
                      @foreach($section['section_contents'] as $content_section)
                        @php
                          // $content_progress = DB::table('progresses')->where(['content_id'=>$content_section->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                        @endphp
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="/course/learn/content/{{$course->slug}}/{{$content_section->id}}">
                            <div class="list-row">
                              <i class="list-icon {{content_type_icon($content_section->type_content)}}"></i>
                              <div class="list-text">{{$content_section->title}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_quizzes'] as $quiz)
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="{{'?quiz=/quiz/'. $quiz->id}}">
                            <div class="list-row">
                              <i class="list-icon fa fa-star"></i>
                              <div class="list-text">{{$quiz->name}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_assignments'] as $assignment)
                        @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                          <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                            <a class="content-of-list" href="{{'?assignment=/assignment/'. $assignment->id}}">
                              <div class="list-row">
                                <i class="list-icon fa fa-tasks"></i>
                                <div class="list-text">{{$assignment->title}}</div>
                                <div class="list-check checkbox">
                                  {{-- <label class="nomargin">
                                    <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                  </label> --}}
                                </div>
                              </div>
                            </a>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </li>
                <?php $i++; ?>
                @endforeach
              </ul>
            </div>
          @endif

          @if($content->type_content == 'url-file')
            <div class="container-full controlable">
              <div class="embed-responsive embed-responsive-16by9" style="position: relative; height: 100%;">
                <nav class="player-navbar">
                  <a id="sidebarCollapse" class="content-toggle" title="Show menu">
                    <i class="zmdi zmdi-menu"></i>
                  </a>
                  <p class="content-title text-truncate">{{$content->title}}</p>
                  <a href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}" class="content-close" title="Back to course">
                    <i class="zmdi zmdi-close"></i>
                  </a>
                </nav>
                <div class="player-navigate">
                  <a href="#" id="backContent" class="navigate" title="Previous"><i class="fa fa-angle-left"></i></a>
                  <a href="#" id="nextContent" class="navigate" title="Next"><i class="fa fa-angle-right"></i></a>
                </div>
                <iframe src="{{$content->full_path_file}}" width="100%" style="height: 1000px !important"></iframe>
              </div>
            </div>
            <div class="player-content-headbox">
              <a class="headbox-button headbox-button-danger" href="{{Request::get('preview') ? '/course/preview/'.$course->id : '/course/learn/'.$course->slug}}"><i class="zmdi zmdi-arrow-left"></i></a>
              <div class="headbox-group">
                <p class="text-truncate weight-600 nopadding nomargin">{{ $course->title }}</p>
              </div>
            </div>
            <div class="player-content-list">
              <ul class="list-unstyled components">
                <?php $i=1; ?>
                @foreach($sections as $section)
                  <li class="padding-10">
                    <a class="head-of-list" href="#section{{$i}}" data-toggle="collapse" aria-expanded="false">
                      <p class="fs-12 nopadding nomargin">@lang('front.page_learn_content.section') {{$i}}:</p>
                      <p class="weight-600 nopadding nomargin">{{ $section['title'] }}</p>
                    </a>
                    <ul class="collapse list-unstyled {{$content->id_section == $section['id'] ? 'show in' : ''}}" id="section{{$i}}">
                      @foreach($section['section_contents'] as $content_section)
                        @php
                          // $content_progress = DB::table('progresses')->where(['content_id'=>$content_section->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                        @endphp
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="/course/learn/content/{{$course->slug}}/{{$content_section->id}}">
                            <div class="list-row">
                              <i class="list-icon {{content_type_icon($content_section->type_content)}}"></i>
                              <div class="list-text">{{$content_section->title}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_quizzes'] as $quiz)
                        <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                          <a class="content-of-list" href="{{'?quiz=/quiz/'. $quiz->id}}">
                            <div class="list-row">
                              <i class="list-icon fa fa-star"></i>
                              <div class="list-text">{{$quiz->name}}</div>
                              <div class="list-check checkbox">
                                {{-- <label class="nomargin">
                                  <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                </label> --}}
                              </div>
                            </div>
                          </a>
                        </li>
                      @endforeach

                      @foreach($section['section_assignments'] as $assignment)
                        @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                          <li class="{{$content->id == $content_section->id ? 'active' : ''}}">
                            <a class="content-of-list" href="{{'?assignment=/assignment/'. $assignment->id}}">
                              <div class="list-row">
                                <i class="list-icon fa fa-tasks"></i>
                                <div class="list-text">{{$assignment->title}}</div>
                                <div class="list-check checkbox">
                                  {{-- <label class="nomargin">
                                    <input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent_" data-id="{{$content_section->id}}">
                                  </label> --}}
                                </div>
                              </div>
                            </a>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </li>
                  <?php $i++; ?>
                @endforeach
              </ul>
            </div>
          @endif
        </div>
      </div>
    </div>

    @foreach($content_video_quizes as  $data)
      <div class="modal" id="content_video_quizes{{$data->id}}" role="dialog" aria-labelledby="" style="z-index:9999999999999999999 !important; position:absolute !important;">
        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">{{$data->question}}</h3>
                </div>
                <div class="panel-body">
                  <div id="alertCheckAnswer{{$data->id}}"></div>
                  @foreach($data->content_video_quiz_answers as $content_video_quiz_answer)
                    <div class="">
                      <label>
                        <input type="radio" name="content_video_quiz_answer_{{$data->id}}" id="optionsRadios1" value="{{$content_video_quiz_answer->id}}"> {{$content_video_quiz_answer->answer}}
                      </label>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" id="btnCheckAnswer" data-id="{{$data->id}}" class="btn btn-primary">@lang('front.page_learn_content.check_answer')</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">@lang('front.page_learn_content.check_answer_next')</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    <!-- Modal -->
    <div id="level_up" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">LEVELING UP</h4>
          </div>
          <div class="modal-body">
            <p>Congratulations</p>
            <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course->levelup_img)}}" alt="Card image">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

  @if($content->type_content == 'scorm') </body> @endif

@endsection

@push('script')
  <script src="/js/jquery.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/videojs/ie8/videojs-ie8.min.js"></script>
  <script src="/videojs/video.js"></script>
  <script src="/videojs/Youtube.min.js"></script>
  <script src="/videojs/videojs-markers.min.js"></script>
  <script src="/videojs/silvermine-videojs-quality-selector.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
  <script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="{{asset('uploadify-master/sample/jquery.uploadifive.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
    <?php $timestamp = time();?>
    $(function() {
      var token = '{{ csrf_token() }}';
      $('#file_upload').uploadifive({
        'auto'             : true,
        'formData'         : {
                     'timestamp' : '<?php echo $timestamp;?>',
                     'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
                     '_token': token
                             },
       'method'            : 'POST',
        'queueID'          : 'queue',
        'uploadScript'     : '{{route("uploadify", "discussion")}}',
        'onUploadComplete' : function(file, data) { console.log(data); },
        'onCancel' : function(file) {
          var token = '{{ csrf_token() }}';
          $.ajax({
            url : "{{route('uploadify.cancel', 'discussion')}}",
            type : 'POST',
            data : {
              'data' : file.originName,
              '_token' : '{{csrf_token()}}'
            },
            success : function(result){
              console.log(result);
            }
          });
         },
        'buttonText' : `<img src="{{asset('img/folder.svg')}}" class="img-file"> <span class="add-file">Add File</span>`
      });
    });

  </script>
  <style media="screen">
    .img-file{
      width: 20px;
    }

    .add-file{
      margin-left: 2px;
    }

    .uploadifive-button {
        float: left;
        margin-right: 10px;
        background: none;
        color: black;
        border: none;
        right: 12px;
        cursor: all-scroll;
    }

    .uploadifive-button {
      float: left;
      margin-right: 10px;
    }

    #queue {
        overflow: auto;
        padding: 0 3px 3px;
        width: 100%;
    }

    .uploadifive-button:hover {
        background: none !important;
    }
  </style>
  <script>

    // Get the element with id="defaultOpen" and click on it
    // document.getElementById("defaultOpen").click();

    // trigger modal add konten / quiz

    $(document).ready(function() {
      $('#example').DataTable({
        language: { search: '', searchPlaceholder: "Search ..." },
      });
    });

    $(".komentar-first").click(function(){
      $(".komentar-second").removeClass('hidden');
      $(".komentar-first").addClass('hidden');
    });

    $("#cancel-komen").click(function(){
      $(".komentar-first").removeClass('hidden');
      $(".komentar-second").addClass('hidden');
    });
  </script>


  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>

  {{-- CKEDITOR --}}

  <script type="text/javascript">
    // =========== VIDEO PROPERTY =======
    videojs('my-video', {
      // "autoplay": true,
      playbackRates: [0.5, 1, 1.5, 2],
      controlBar: {
        fullscreenToggle: true,
      },
    });
    // =========== VIDEO PROPERTY =======

    var player = videojs('my-video');
    player.controlBar.addChild('QualitySelector');

    // markers
    player.markers({
      markerStyle: {
         'width':'8px',
         'background-color': 'yellow'
      },
      markers: [
        @foreach($content_video_quizes as  $data)
         {time: '{{$data->seconds}}.1', text: "{{$data->question}}"},
        @endforeach
      ]
    });
    // markers

    // Prevent for right click on video
    player.on('contextmenu', function(e) {
        e.preventDefault();
    });

    // Prevent for right click on video

    // event ended playing video
    player.on('ended', function() {
      $.ajax({
        url : '/course/learn-content/finish',
        type : 'POST',
        data : {
          'content_id' : {{$content->id}},
          'course_id' : '{{$course->id}}',
          '_token' : '{{csrf_token()}}'
        },
        success : function(result){
          var toNextContent = $("#nextContent").attr('href')
          window.location.assign('{{url("/")}}'+toNextContent)
        }
      });
    });
    // event ended playing video

    // quiz up
    @foreach($content_video_quizes as  $data)
      var timeCheck{{$data->id}} = function() {
        if (player.currentTime() > {{$data->seconds}}) {
          player.off('timeupdate', timeCheck{{$data->id}});
          player.trigger('played{{$data->seconds}}Second');
        }
      };
      player.on('played{{$data->seconds}}Second', function(){
        player.pause();
        $("#content_video_quizes{{$data->id}}").modal({backdrop: 'static', keyboard: false, show:'true'});
        // console.log(player.currentTime()); // get current time
      });
      player.on('timeupdate', timeCheck{{$data->id}});

      $('#content_video_quizes{{$data->id}}').on('hidden.bs.modal', function () {
        player.play();
      })
    @endforeach
    // quiz up

  </script>

  <script type="text/javascript">
    $(".text-reply").hide();
    $(".reply-btn").click(function(){
      $(".text-reply").hide();
      $(this).siblings(".text-reply").show();

    });
  </script>

  {{-- check answer --}}
  <script type="text/javascript">
    $(function(){
      $(document).on('click','#btnCheckAnswer',function(e){
        e.preventDefault();
          var data_id = $(this).attr('data-id');
          var content_video_quiz_answer_id = $("input[name*='content_video_quiz_answer_"+data_id+"']:checked").val()

          $.ajax({
            url: '/content-video-quiz/check-answer',
            type: 'post',
            data: {
              _token: '{{csrf_token()}}',
              content_video_quiz_answer_id: content_video_quiz_answer_id
            },
            success: function(result){
              if(result.is_correct == '1'){
                $("#alertCheckAnswer"+data_id).removeClass('alert alert-danger').addClass('alert alert-success');
                $("#alertCheckAnswer"+data_id).html(
                  '<strong>Betul</strong>'+
                  '<p style="color:white">'+result.answer_description+'</p>'
                );
              }else{
                $("#alertCheckAnswer"+data_id).removeClass('alert alert-success').addClass('alert alert-danger');
                $("#alertCheckAnswer"+data_id).html(
                  '<strong>Salah</strong>'+
                  '<p style="color:white">'+result.answer_description+'</p>'
                );
              }
            }
          })
      });
    });

  </script>
  {{-- check answer --}}

  <script type="text/javascript">
    $(function(){
      $(document).on('change','#CheckContent',function(e){
        e.preventDefault();
          var content_id = $(this).attr('data-id');
          if($(this).is(':checked') === true){
            $.ajax({
							url : '/course/learn-content/finish',
							type : 'POST',
							data : {
								'content_id' : content_id,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }else{
            $.ajax({
							url : '/course/learn-content/unfinish',
							type : 'POST',
							data : {
								'content_id' : content_id,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }
      });
    });
  </script>

  <script type="text/javascript">
    $("#PostComment").click(function(){
      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('content/comment')}}",
        type : "POST",
        data : {
          content_id : {{Request::segment(5)}},
          comment : $("#comment").val(),
          _token: token
        },
        success : function(result){
          loadComment();
          $("#comment").val('');
        },
      });
    })

    function loadComment(){
      $.ajax({
        url : "{{url('content/comment/get/'.Request::segment(5))}}",
        type : "get",
        success : function(result){
          $("#newComments").html(result);
        },
      });
    }
  </script>

  <script type="text/javascript">
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $('#sidebar').removeClass('active');
    } else {
      $('#sidebar').addClass('active');
    }
  	 $(document).ready(function () {
  			 $('#sidebarCollapse').on('click', function () {
  					 $('#sidebar').toggleClass('active');
  			 });
  	 });
  	 $(document).ready(function () {
  			 $('#sidebarCollapseOff').on('click', function () {
  					 $('#sidebar').toggleClass();
  			 });
  	 });
  </script>

  <script type="text/javascript">
    $("#buttonModalComment").click(function(){
      $("#modalComment").modal('show');
    })
  </script>

  @php
    $contents = [];
    foreach($sections as $section){
      foreach($section['section_contents'] as $contentNewData){
        array_push($contents, [
          'type' => 'content',
          'id' => $contentNewData->id,
        ]);
      }
      foreach($section['section_quizzes'] as $quiz){
        array_push($contents, [
          'type' => 'quiz',
          'id' => $quiz->id,
        ]);
      }
      foreach($section['section_assignments'] as $assignment){
        array_push($contents, [
          'type' => 'assignment',
          'id' => $assignment->id,
        ]);
      }
    }
    // dd($contents);
    $current_content = !Request::get('quiz') && !Request::get('assignment') ? Request::segment(5) : (Request::get('quiz') ? Request::get('quiz') : Request::get('assignment')) ;
    $current_index = array_search($current_content, array_column($contents, 'id'));
    $next = $current_index + 1;
    $prev = $current_index - 1;
  @endphp

  @if($prev >= 0)
    <script type="text/javascript">
      @if($contents[$prev]['type'] == 'content')
        $("#backContent").attr("href", "/course/learn/content/{{$course->slug}}/{{$contents[$prev]['id']}}")
      @endif
      @if($contents[$prev]['type'] == 'quiz')
        $("#backContent").attr("href", "/course/learn/{{$course->slug}}/quiz/{{$contents[$prev]['id']}}")
      @endif
      @if($contents[$prev]['type'] == 'assignment')
        $("#backContent").attr("href", "/course/learn/{{$course->slug}}/assignment/{{$contents[$prev]['id']}}")
      @endif
    </script>
  @endif

  @if($next < count($contents))
    <script type="text/javascript">
      @if($contents[$next]['type'] == 'content')
        $("#nextContent").attr("href", "/course/learn/content/{{$course->slug}}/{{$contents[$next]['id']}}")
      @endif
      @if($contents[$next]['type'] == 'quiz')
        $("#nextContent").attr("href", "/course/learn/{{$course->slug}}/quiz/{{$contents[$next]['id']}}")
      @endif
      @if($contents[$next]['type'] == 'assignment')
        $("#nextContent").attr("href", "/course/learn/{{$course->slug}}/assignment/{{$contents[$next]['id']}}")
      @endif
    </script>
  @endif

  @if($content->type_content == 'discussion')

    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
    <!--[if (gte IE 8)&(lt IE 10)]>
      <script src="/jquery-upload/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
    <style>
      .dataTables_length label {
          font-size: 18px;
          padding: 10px 0px;
      }

      .dataTables_filter label {
          font-size: 18px;
          padding: 10px 0px;
      }
    </style>
  @endif

  <script>
    if({{$level_up_status === true}})
    {
      $('#level_up').modal('show');  
    }   
  </script>
@endpush
