@extends('layouts.app-static')

@section('title', $course->title . " - " . $content->title)

@push('style')
  <style media="screen">
    :not(.bmd-drawer-out).bmd-drawer-in.bmd-drawer-f-l>.bmd-layout-header{
      width:calc(100% - 370px);
      margin-left: 370px;
    }
    :not(.bmd-drawer-out).bmd-drawer-in.bmd-drawer-f-l>.bmd-layout-content{
      margin-left: 370px;
    }
    :not(.bmd-drawer-out).bmd-drawer-in.bmd-drawer-f-l>.bmd-layout-drawer{
      transform: translateX(0);
    }
    .bmd-drawer-f-l>.bmd-layout-drawer{
      transform: translateX(-370px);
      width:370px;
    }
    .navbar{
      background-color: #202020 !important;
      color: white;
    }
    .embed-responsive.embed-responsive-16by9{
      padding-bottom:47%;
    }
    .embed-responsive.embed-responsive-16by9::before{
      padding-top:47%;
    }
    .frame-container {
      position: relative;
    }
    .iframe-button {
      display: none;
      position: absolute;
      top: 15px;
      left: 15px;
    }
    .frame-container:hover .iframe-button {
      display: initial;
    }
  </style>
@endpush

@section('content')
<div class="bmd-layout-container bmd-drawer-f-l">
  <header class="bmd-layout-header">
    <div class="navbar navbar-light bg-faded">
      <button style="color:#818181" class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s1">
        <span class="sr-only">Toggle drawer</span>
        <i class="material-icons">menu</i>
      </button>
      <ul class="nav navbar-nav" style="color:#818181">
        <li class="nav-item">{{$content->title}}</li>
        <li class="nav-item">
          <a href="/course/learn/{{$course->slug}}">Go to Dashboard</a>
        </li>
      </ul>
    </div>
  </header>

  <main class="bmd-layout-content" style="background-color: {{$content->type_content == 'video' ? 'black' : 'white'}}">
    <div class="container-fluid" id="contentBody">
      <div class="row">
        <div class="col-md-12">
          <br>
          @if($content->type_content == 'video')
            <div class="dq-video">
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="{{$content->full_path_file.'/preview'}}" allowfullscreen="true"></iframe>
                  {{-- <button type="button" class="btn btn-primary" name="button" style="top:1000px">Button</button> --}}
                <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
              </div>
            </div>
          @endif
          @if($content->type_content == 'text')
            <div class="panel panel-default">
              <div class="panel-body">
                {!! $content->description !!}
              </div>
              <div class="panel-footer">
                <h3>Attachment</h3>
                <li>
                  <a href="{{$content->full_path_file}}" target="_blank">{{$content->name_file}}</a>
                </li>
              </div>
            </div>
          @endif

          @php
            $contents = [];
            foreach($sections as $section){
              foreach($section['section_contents'] as $content){
                array_push($contents, [
                  'type' => 'content',
                  'id' => $content->id,
                ]);
              }
              foreach($section['section_quizzes'] as $quiz){
                array_push($contents, [
                  'type' => 'quiz',
                  'id' => '/quiz/'.$quiz->id,
                ]);
              }
            }
            // dd($contents);
            $lengthContents = count($contents);
            $current_content = !Request::get('quiz') ?  Request::segment(5) : Request::get('quiz') ;
            $indexOfContent = array_search($current_content, array_column($contents, 'id'));
            $linkContent = $current_content < $lengthContents -1 ? ++$indexOfContent : $lengthContents -1;
            $linkContent = isset($contents[$linkContent]) ? $contents[$linkContent] : '' ;
          @endphp
        </div>
      </div>
    </div>

    @if(++$indexOfContent <= $lengthContents)
      <div class="ytp-chrome-controls" style="margin-top:-2.5%; margin-right:10%; position:relative;">
        <div class="ytp-left-controls pull-right">
          <a class="btn btn-danger btn-raised btn-sm" href="/course/learn/content/{{$course->slug}}/{{$linkContent['type'] == 'content' ? $linkContent['id'] : Request::segment(5) .'?quiz='.$linkContent['id'] }}" style="opacity:0.7">Continue</a>
        </div>
      </div>
    @endif

  </main>


  <div id="dw-s1" class="bmd-layout-drawer bg-faded" style="background-color:#f7f7f7;">
    <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
      <?php $i=1; ?>
      <div class="panel-group" id="accordion fade in active" style="padding:15px;">
        @foreach($sections as $section)
         <div class="panel panel-default" style="margin-bottom:20px; background-color:white;">
           <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}" style="text-decoration:none">
             <div class="panel-heading" style="padding:25px">
               <h4 class="panel-title">
                  <span style=" font-size:14px">Section {{$i}}:</span>
                  <br/>
                  <span style="font-size:20px">{{ $section['title'] }}</span>
               </h4>
             </div>
           </a>
           <div id="content{{$i}}" class="panel-collapse collapse">
             <div class="panel-body" style="background:white; padding:0">
              <div class="list-group" style="padding:0;">
                @foreach($section['section_contents'] as $content)
                  @php
                    $content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                  @endphp
                  <a style="padding:18px 15px" href="/course/learn/content/{{$course->slug}}/{{$content->id}}" class="list-group-item list-group-item-action withripple">
                    <i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}
                    <div class="checkbox">
                      <label>
                        &nbsp;<input type="checkbox" {{$content_progress ? 'checked' : ''}} id="CheckContent" data-id="{{$content->id}}">
                      </label>
                    </div>
                  </a>
                @endforeach
                @foreach($section['section_quizzes'] as $quiz)
                  <a style="padding:18px 15px;cursor:pointer" href="{{'?quiz=/quiz/'. $quiz->id}}" class="list-group-item list-group-item-action withripple">
                    <i class="fa fa-star"></i>{{$quiz->name}}
                  </a>
                @endforeach
                @foreach($section['section_assignments'] as $assignment)
                  @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                    <a style="padding:18px 15px; cursor:pointer" class="list-group-item list-group-item-action withripple" id="contentAssignment" href="{{'?assignment=/assignment/'. $assignment->id}}">
                      <i class="fa fa-tasks"></i>{{$assignment->title}}
                    </a>
                  @else
                    {{$assignment->title}} (time has run out) <br>
                  @endif
                @endforeach
              </div>
             </div>
           </div>
         </div>
        <?php $i++; ?>
        @endforeach
      </div>
    </div>
  </div>

</div>

@endsection

@push('script')
  <script type="text/javascript">

  @if(Request::get('quiz'))
  $("#contentBody").html(
    '<div class="dq-video">'+
    '<div class="embed-responsive embed-responsive-16by9">'+
    '<iframe class="embed-responsive-item" id="contentBody" src="{{Request::get('quiz')}}"></iframe>'+
    '<div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>'+
    '</div>'+
    '</div>');
  @endif

  @if(Request::get('assignment'))
  $("#contentBody").html(
    '<div class="dq-video">'+
    '<div class="embed-responsive embed-responsive-16by9">'+
    '<iframe class="embed-responsive-item" id="contentBody" src="{{Request::get('assignment')}}"></iframe>'+
    '<div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>'+
    '</div>'+
    '</div>');
  @endif

  </script>

  <script type="text/javascript">
    $(function(){
      $(document).on('change','#CheckContent',function(e){
        e.preventDefault();
          var content_id = $(this).attr('data-id');
          if($(this).is(':checked') === true){
            $.ajax({
							url : '/course/learn-content/finish',
							type : 'POST',
							data : {
								'content_id' : content_id,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }else{
            $.ajax({
							url : '/course/learn-content/unfinish',
							type : 'POST',
							data : {
								'content_id' : content_id,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }
      });
    });
  </script>
@endpush
