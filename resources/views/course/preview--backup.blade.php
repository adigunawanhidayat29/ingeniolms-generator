@extends('layouts.app')

@section('title', $course->title)

@push('style')
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="/css/bootstrap-editable.css">
  <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <style media="screen">
    .editableform .form-group{
      margin: 0px !important;
      padding: 0px !important;
    }
    .editableform .form-group .form-control{
      height: 33px;
      padding: 0px;
      margin-top: -1.3rem;
      margin-bottom: 0px;
      line-height: auto;
    }
    .editable-input .form-control{
      color: white !important;
      width: auto;
      font-size: 2.4rem;
      font-weight: bold;
    }
    .editable-buttons{
      display: none;
    }
  </style>
  <style>
    /* Style the tab */
    .tab {
        float: left;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        width: 30%;
        height: auto;
    }

    /* Style the buttons inside the tab */
    .tab button {
        display: block;
        background-color: inherit;
        color: black;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current "tab button" class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        float: left;
        padding: 0px 12px;
        border: 1px solid #ccc;
        width: 70%;
        border-left: none;
        height: auto;
    }

    .placeholder{
        background-color:white;
        height:18px;
    }

    .move:hover{
      cursor: move;
    }

    .lh-3{
      line-height: 3;
    }
    .ContentSortable{
      border:0.2px solid #dfe6e9;
      min-height:60px;
      padding:10px;
    }
    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
      width: 120px;
      height: 120px;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Pengajar</a></li>
          <li><a href="#">Kelola Kelas</a></li>
          <li>{{$course->title}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div style="background:#393939; padding:25px;margin-top:0px;">
    <div class="container">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>
      <div class="row">
          <div class="col-md-5">
            <a href="/bigbluebutton/meeting/join/{{$course->id}}">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.gif" />' : ''!!}</span>
            </a>
            @if($course->introduction != NULL)
              <div class="card">
                <div class="embed-responsive embed-responsive-16by9" style="position:relative;">
                  <iframe class="embed-responsive-item" src="{{$course->introduction.'/preview'}}" allowfullscreen="true"></iframe>
                  <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
                </div>
              </div>
            @else
              <img class="img-fluid" src="{{asset_url(course_image_medium($course->image))}}" alt="{{$course->title}}">
              <a href="#" data-target="#modalSetImage" data-toggle="modal" class="pull-right">Edit Image <i style="color:white;" class="fa fa-pencil"></i></a>
            @endif
          </div>
          <div class="col-md-7">
            <h3 class="color-white" style="font-weight:bold;">
              <a class="mt-6 mb-6" href="#" id="CourseTitle" data-type="text">
                {{$course->title}} <i class="fa fa-pencil color-white"></i>
              </a>
            </h3>
            <h4 class="color-white" style="font-weight:bold;">
              <a class="mt-6 mb-6" href="#" id="CourseSubTitle" data-type="text">
                {{$course->subtitle}} <i class="fa fa-pencil color-white"></i>
              </a>
            </h4>
            <p>
              <span>
                <a href="#">
                  @for($i=1;$i<=$rate;$i++)
                    <i class="fa fa-star color-warning"></i>
                  @endfor
                  @for($i=$rate;$i<5;$i++)
                    <i class="fa fa-star color-white"></i>
                  @endfor
                  Beri penilaian
                </a>
              </span>
            </p>
            <p style="color:white">
              <strong>{{$num_progress}}</strong> dari <strong>{{$num_contents}}</strong> Modul selesai <a href="#">Reset Progres</a>
              <div class="progress">
                <div class="progress-bar" role="progressbar"
                  aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%">
                  {{ceil($percentage)}}%
                </div>
              </div>
            </p>
            <a href="/course/learn/{{$course->slug}}" class="btn btn-primary btn-raised">Lihat Halaman Pelajar</a>
          </div>
      </div>
    </div>
  </div>

  <div class="container">
<!--
    <br>
    <ul class="breadcrumb">
      <li><a href="#/">Pengajar</a></li>
      <li><a href="/course/dashboard">Kelola Kelas</a></li>
      <li>{{$course->title}}</li>
    </ul>
    <br><br> -->

    <section class="ms-component-section">
      <div class="card card-flat">
        <div class="container">
          <ul class="nav nav-tabs nav-tabs-transparent indicator-info nav-tabs-full nav-tabs-6" role="tablist">
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
                <i class="fa fa-eye"></i>
                <span class="d-none d-sm-inline">Ikhtisar</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple {{is_liveCourse($course->id) == false ? 'active' : ''}}" href="#course-content" aria-controls="course-content" role="tab" data-toggle="tab">
                <i class="fa fa-graduation-cap"></i>
                <span class="d-none d-sm-inline">Konten</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple {{is_liveCourse($course->id) == true ? 'active' : ''}}" href="#meeting" aria-controls="meeting" role="tab" data-toggle="tab">
                <i class="fa fa-users"></i>
                <span class="d-none d-sm-inline">{{is_liveCourse($course->id) == true ? 'Kelas Live' : 'Webinar'}}</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
                <i class="fa fa-question-circle-o"></i>
                <span class="d-none d-sm-inline">Diskusi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                <i class="fa fa-bullhorn"></i>
                <span class="d-none d-sm-inline">Pengumuman</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#progresses" aria-controls="announcements" role="tab" data-toggle="tab">
                <i class="fa fa-circle-o-notch"></i>
                <span class="d-none d-sm-inline">Progres Peserta</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="card-block no-pl no-pr">
              <div class="tab-content">

                <!-- TAB OVERVIEW -->
                <div role="tabpanel" class="tab-pane fade" id="overview">
                  <div class="row">

                    <!--RECENT ACTIVITY-->
                    <div class="col-md-12">
                      <h2 class="headline-sm headline-line">Aktifitas <span>Terbaru</span></h2>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group">
                          <li class="list-group-item">
                            <b>Pertanyaan Terbaru</b>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group">
                          <li class="list-group-item">
                            <b>Pengumuman Pengajar Terbaru</b>
                          </li>
                        </ul>
                      </div>
                    </div>

                    <!--ABOUT COURSE-->
                    <div class="col-md-12">
                      <h2 class="headline-sm headline-line">Tentang <span>Kelas Ini</span></h2>
                    </div>
                    <div class="col-md-12">
                      <div class="card card-info">
                        <div class="card-block">
                          <div class="row">
                            <div class="col-md-2">
                              <b>Dengan Nomor</b>
                            </div>
                            <ul class="col-md-3">
                              <li class="dq-list-item">Modul: {{count($sections)}}</li>
                              <li class="dq-list-item">Kategori:
                                <a href="#" id="CourseCategory" data-type="select">
                                  {{$course->category}} <i class="fa fa-pencil"></i>
                                </a>
                                </li>
                              <li class="dq-list-item">Skill level:
                                <a href="#" id="CourseLevel" data-type="select">
                                  {{$course->level}} <i class="fa fa-pencil"></i>
                                </a>
                              </li>
                              Harga:
                              <a href="#" id="CoursePrice" data-type="text">
                                {{$course->price}} <i class="fa fa-pencil"></i>
                              </a>
                            </ul>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-2">
                              <b>Penjelasan</b>
                            </div>
                            <ul class="col-md-8">
                              <div id="recentDescription">
                                {!! $course->description !!}
                              </div>
                              <a href="#" id="getEditorDescription" data-target="#modalSetEditorDescription" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                            </ul>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-2">
                              <b>Tujuan</b>
                            </div>
                            <ul class="col-md-8">
                                <div id="recentGoal">
                                  {!! $course->goal !!}
                                </div>
                                <a href="#" id="getEditorGoal" data-target="#modalSetEditorGoal" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                            </ul>
                          </div>
                          @if($course->model == 'private')
                            <hr>
                            <div class="row">
                              <div class="col-md-2">
                                <b>Password / Kode</b>
                              </div>
                              <ul class="col-md-8">
                                  <div id="recentGoal">
                                    {{ $course->password }}
                                  </div>
                              </ul>
                            </div>
                          @endif
                          @if($course->model == 'live')
                            @php
                              $course_live = DB::table('course_lives')->where('course_id', $course->id)->first();
                            @endphp
                            <hr>
                            <div class="row">
                              <div class="col-md-2">
                                <b>Tanggal Mulai</b>
                              </div>
                              <ul class="col-md-8">
                                  <div id="recentGoal">
                                    {{$course_live->start_date}}
                                  </div>
                              </ul>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-md-2">
                                <b>Tanggal Akhir</b>
                              </div>
                              <ul class="col-md-8">
                                  <div id="recentGoal">
                                    {{$course_live->end_date}}
                                  </div>
                              </ul>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-md-2">
                                <b>Total Live</b>
                              </div>
                              <ul class="col-md-8">
                                  <div id="recentGoal">
                                    {{$course_live->meeting_total}}
                                  </div>
                              </ul>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-md-2">
                                <b>Total Peserta</b>
                              </div>
                              <ul class="col-md-8">
                                  <div id="recentGoal">
                                    {{$course_live->participant_total}}
                                  </div>
                              </ul>
                            </div>
                          @endif
                          <hr>
                          <div class="row">
                            <div class="col-md-2">
                              <b>Pengajar</b>
                            </div>
                            <div class="col-md-10">
                              <div class="row">
                                <div class="col-md-2">
                                  <img src="{{avatar($course->photo)}}" class="img-circle">
                                </div>
                                <div class="col-md-6">
                                  <h3 class="color-dark">{{$course->author}}</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- TAB COURSE CONTENT -->
                <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == false ? 'active show' : ''}} " id="course-content">
                  <input type="hidden" id="content-id-value" value="" />
                  <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
                    <?php $i=1; ?>


                    <div class="panel-group SectionSortable" id="accordion fade in active">
                      @foreach($sections as $section)
                         <div class="panel panel-default" id="section{{$section['id']}}" data-id="{{$section['id']}}">
                           <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
                             <div class="panel-heading" style="background:#e9e9e9">
                               <h4 class="panel-title">
                                  {{-- <span style="color:#474545; font-size:13px">Topik {{$i}}:</span>
                                  <br/><br/> --}}
                                  <i class="move fa fa-arrows-alt"></i>
                                  <strong style="color:#2e2e2e">{{ $section['title'] }}</strong>
                                  <div class="pull-right">
                                    <a class="color-primary" href="#/" id="EditSection" data-id="{{ $section['id'] }}" data-title="{{ $section['title'] }}" data-description="{{ $section['description'] }}">Edit <i class="fa fa-pencil"></i></a>
                                    <a class="color-danger" href="/course/section/delete/{{ $section['id_course'] }}/{{ $section['id'] }}" onclick="return confirm('Topik ini memiliki beberapa konten, Anda yakin ingin menghapusnya?')">Hapus <i class="fa fa-trash"></i></a>
                                  </div>
                                </h4>
                             </div>
                           </a>
                           <div id="content{{$i}}" class="panel-collapse collapse show">
                             <div class="panel-body" style="background:white;">
                               {{-- <a href="#" class="btn btn-primary btn-raised" id="AddContent" data-section="{{ $section['id'] }}">Buat Konten Baru</a> --}}
                              <a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" da class="btn btn-primary btn-raised">Tambah Konten / Kuis</a>
                              {{-- <a onClick="document.getElementById('content-id-value').value='{{ $section['id'] }}'; alert(document.getElementById('content-id-value').value);" href="#" da class="btn btn-default btn-raised">Tambah Konten dari Library</a> --}}

                              <div class="modal" id="ModalMenuContent{{ $section['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                                    <div class="modal-content">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <h3>Tambah Konten / Kuis</h3>
                                          <div class="tab">
                                            <button class="tablinks" onclick="openCity(event, 'Video{{ $section['id'] }}')" id="defaultOpen"><i class="fa fa-video-camera"></i> Konten Video</button>
                                            <button class="tablinks" onclick="openCity(event, 'Teks{{ $section['id'] }}')"><i class="fa fa-text-height"></i> Konten Teks</button>
                                            <button class="tablinks" onclick="openCity(event, 'File{{ $section['id'] }}')"><i class="fa fa-file-o"></i> Konten File</button>
                                            <button class="tablinks" onclick="openCity(event, 'Kuis{{ $section['id'] }}')"><i class="fa fa-question"></i> Kuis</button>
                                            <button class="tablinks" onclick="openCity(event, 'Tugas{{ $section['id'] }}')"><i class="fa fa-tasks"></i> Tugas</button>
                                          </div>

                                          <div id="Video{{ $section['id'] }}" class="tabcontent">
                                            <h3>Tambah Konten Video</h3>
                                            <p>
                                              Tambahkan konten video agar proses belajar menjadi lebih interaktif
                                            </p>
                                            <a href="/course/content/library/{{ $section['id'] }}?content=video" class="btn btn-default btn-raised">Ambil dari Library</a>
                                            <a href="/course/content/create/{{ $section['id'] }}?content=video" class="btn btn-primary btn-raised">Upload Video</a>
                                            <a href="/course/content/create/{{ $section['id'] }}?content=url&urlType=video" class="btn btn-primary btn-raised">Tambah URL Youtube</a>

                                            <br>
                                          </div>

                                          <div id="Teks{{ $section['id'] }}" class="tabcontent">
                                            <h3>Tambah Konten Teks</h3>
                                            <p>
                                              Anda bisa menambahkan konten dengan hanya teks saja. bisa digunakan seperti membuat pendahuluan dan lain-lain.
                                            </p>
                                            <a href="/course/content/create/{{ $section['id'] }}?content=text" class="btn btn-primary btn-raised">Tambah Konten Teks</a>
                                            <a href="/course/content/library/{{ $section['id'] }}?content=text" class="btn btn-default btn-raised">Ambil dari Library</a>
                                            <br>
                                          </div>

                                          <div id="File{{ $section['id'] }}" class="tabcontent">
                                            <h3>Tambah Konten File</h3>
                                            <p>
                                              Anda bisa menambahkan konten file / berkas. bisa digunakan seperti membuat file pdf, power point dan lain-lain.
                                            </p>
                                            <a href="/course/content/create/{{ $section['id'] }}?content=file" class="btn btn-primary btn-raised">Tambah Konten File</a>
                                            <a href="/course/content/library/{{ $section['id'] }}?content=file" class="btn btn-default btn-raised">Ambil dari Library</a>
                                            <a href="/course/content/create/{{ $section['id'] }}?content=url&urlType=file" class="btn btn-primary btn-raised">Tambah Dari URL</a>

                                            <br>
                                          </div>

                                          <div id="Kuis{{ $section['id'] }}" class="tabcontent">
                                            <h3>Tambah Kuis</h3>
                                            <p>
                                              Tambahkan kuis untuk menguji peserta sejauhmana mereka belajar
                                            </p>
                                            <a href="/course/quiz/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">Tambah Kuis</a>
                                            <a href="/course/content/library/{{ $section['id'] }}?content=quiz" class="btn btn-default btn-raised">Ambil dari Library</a>
                                            <br>
                                          </div>

                                          <div id="Tugas{{ $section['id'] }}" class="tabcontent">
                                            <h3>Tambah Tugas</h3>
                                            <p>
                                              Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.
                                            </p>
                                            <a href="/course/assignment/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">Tambah Tugas</a>
                                            <br>
                                          </div>
                                          <br><br><br><br> &nbsp;
                                          {{-- <a href="#" class="btn btn-default btn-raised">Batal</a> --}}
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                              <br>
                              <div class="list-group">
                                <div class="ContentSortable" id="{{ $section['id'] }}">
                                  @foreach($section['section_contents'] as $content)
                                    <li style="list-style:none" id="{{$content->id}}" data-section="{{$content->id_section}}">
                                      @php
                                        $content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                      @endphp
                                      <a href="#" class="list-group-item list-group-item-action withripple">
                                        <i class="move fa fa-arrows-alt"></i>
                                        {{-- <span class="lh-3" onclick="location.href = '/course/learn/content/{{$course->slug}}/{{$content->id}}?preview=true'"><i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}} <i class="text-danger">{{$content->type_content == 'video' ? ($content->full_path_original == null ? 'Video mengalami masalah. Silakan hapus / upload ulang' : '') : ''}}</i> </span> --}}
                                        <span class="lh-3" onclick="location.href = '/course/learn/content/{{$course->slug}}/{{$content->id}}?preview=true'"><i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}</span>
                                        <span class="ml-auto" style="display:inline-flex;">
                                          @if($content->type_content == 'video')
                                            <span class="text-right lh-3">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</span> &nbsp;
                                          @endif
                                          @if($course->public == '1')
                                            <label style="margin-right:0" class="lh-3">
                                              {{-- &nbsp;<input type="checkbox" id="CheckContent" data-id="{{$content->id}}"> --}}
                                              <input type="checkbox" value="{{$content->id}}" {{$content->preview == '1' ? 'checked' : ''}} id="preview{{$content->id}}" class="preview" data-on-text="Preview" data-off-text="Non Preview" data-size="small">
                                            </label>
                                          @endif
                                          <span class="lh-3 text-{{$content->status == '0' ? 'danger' : 'success'}}">&nbsp;{{$content->status == '0' ? 'Draft&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : 'Published'}}</span>
                                          &nbsp; &nbsp;
                                          <div class="dropdown">
                                            <button class="btn dropdown-toggle bn-default btn-raised btn-sm" type="button" id="" data-toggle="dropdown">
                                              Pilihan
                                              <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                              <li><span onclick="location.href = '{{url('course/content/update/'.$content->id)}}'">&nbsp; <i class="fa fa-pencil"></i> Edit &nbsp;</span></li>
                                              <li><span class="text-danger" onclick="if(confirm('sure?')){ return location.href = '{{url('course/content/delete/'.$course->id.'/'.$content->id)}}' }">&nbsp; <i class="fa fa-trash-o"></i> Hapus &nbsp;</span></li>
                                              <li><span onclick="location.href = '{{url('course/content/store/library/'.$content->id)}}'">&nbsp; Simpan Library</span></li>
                                              @if($content->type_content == 'video')
                                                <li><span onclick="location.href = '{{url('course/content/video/manage/'.$content->id)}}'">&nbsp; Manage Video</span></li>
                                              @endif
                                            </ul>
                                          </div>
                                        </span>
                                    </a>
                                  </li>
                                  @endforeach
                                </div>
                              </div>
                              {{-- <a href="#" id="AddQuiz" data-section="{{ $section['id'] }}">Add New Quiz</a> --}}
                              {{-- <a class="btn btn-primary btn-raised" href="/course/quiz/create/{{ $section['id'] }}">Buat Kuis Baru</a> --}}
                              <br>
                              <div class="list-group">
                                @foreach($section['section_quizzes'] as $quiz)
                                  <a href="#" class="list-group-item list-group-item-action withripple">
                                    <span class="lh-3"><i class="fa fa-star"></i> {{$quiz->name}}</span>
                                    {{-- | <span onclick="location.href = '{{url('course/quiz/delete/'.$course->id.'/'.$quiz->id)}}'"> &nbsp; Hapus</span> --}}
                                    <span class="ml-auto" style="display:inline-flex;">
                                      <span class="lh-3 text-{{$quiz->status == '0' ? 'danger' : 'success'}}">&nbsp;{{$quiz->status == '0' ? 'Draft' : 'Published'}}</span>
                                      &nbsp; &nbsp;
                                      <div class="dropdown">
                                        <button class="btn dropdown-toggle bn-default btn-raised btn-sm" type="button" id="" data-toggle="dropdown">
                                          Pilihan
                                          <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                          <li><span onclick="location.href = '{{url('course/quiz/question/manage/'.$quiz->id)}}'"> &nbsp; Manage &nbsp;</span></li>
                                          <li><span onclick="location.href = '{{url('course/quiz/store/library/'.$quiz->id)}}'">&nbsp; Simpan Library</span></li>
                                          <li><span onclick="location.href = '{{url('course/quiz/update/'.$quiz->id)}}'"> &nbsp; <i class="fa fa-pencil"></i> Edit  &nbsp;</span></li>
                                          <li><span onclick="location.href = '{{url('course/quiz/publish/'.$quiz->id)}}'"> &nbsp; {{$quiz->status == '1' ? 'published' : 'publish'}}  &nbsp;</span></li>
                                          <li><span class="text-danger" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('course/quiz/delete/'.$course->id.'/'.$quiz->id)}}'"> &nbsp; <i class="fa fa-trash-o"></i> Hapus  &nbsp;</span></li>
                                        </ul>
                                      </div>
                                    </span>
                                  </a>
                                @endforeach
                              </div>

                              {{-- <a class="btn btn-primary btn-raised" href="/course/assignment/create/{{ $section['id'] }}">Buat Tugas</a> --}}
                              <br>
                              <div class="list-group">
                                @foreach($section['section_assignments'] as $assignment)
                                  <a href="#" class="list-group-item list-group-item-action withripple">
                                    <span class="lh-3"><i class="fa fa-tasks"></i>{{$assignment->title}}</span>
                                    <span class="ml-auto" style="display:inline-flex;">
                                      <span class="lh-3 text-{{$assignment->status == '0' ? 'danger' : 'success'}}">&nbsp;{{$assignment->status == '0' ? 'Draft' : 'Published'}}</span>
                                      &nbsp; &nbsp;
                                      <div class="dropdown">
                                        <button class="btn dropdown-toggle bn-default btn-raised btn-sm" type="button" id="" data-toggle="dropdown">
                                          Pilihan
                                          <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                          <li><span onclick="location.href = '{{url('course/assignment/update/'.$assignment->id)}}'"> &nbsp; Edit  &nbsp;</span></li>
                                          <li><span onclick="location.href = '{{url('course/assignment/answer/'.$assignment->id)}}'"> &nbsp; Lihat Jawaban  &nbsp;</span></li>
                                          <li><span onclick="location.href = '{{url('course/assignment/publish/'.$assignment->id)}}'"> &nbsp; {{$assignment->status == '1' ? 'published' : 'publish'}}  &nbsp;</span></li>
                                          <li><span class="text-danger" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('course/assignment/delete/'.$course->id.'/'.$assignment->id)}}'"> &nbsp; <i class="fa fa-trash-o"></i> Hapus  &nbsp;</span></li>
                                        </ul>
                                      </div>
                                    </span>
                                  </a>
                                @endforeach
                              </div>
                             </div>
                           </div>
                         </div>
                      <?php $i++; ?>
                      @endforeach
                    </div>

                    <div id="place_new_section"></div>
                    <input type="hidden" value="0" id="section_row">
                    <button type="button" class="btn btn-raised btn-block btn-lg" id="add_new_section">Tambah Topik</button>
                  </div>
                </div>

                <!-- TAB WEBINAR -->
                <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == true ? 'active show' : ''}}" id="meeting">
                  <a href="#" data-toggle="modal" data-target="#modalWebinarAddSchedule" class="btn btn-primary btn-raised">Buat Jadwal {{is_liveCourse($course->id) == true ? 'Kelas Live' : 'Webinar'}}</a>

                  <div class="modal fade" id="modalWebinarAddSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="">Buat Jadwal {{is_liveCourse($course->id) == true ? 'Kelas Live' : 'Webinar'}}</h4>
                        </div>
                        <form class="" action="/meeting/schedule/store" method="post">
                          {{csrf_field()}}
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="">Judul</label>
                                  <input type="text" name="name" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Deskripsi / Penjelasan</label>
                                  <textarea name="description" class="form-control" rows="8" cols="80"></textarea>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="">Tanggal</label>
                                  <input type="date" name="date" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Waktu</label>
                                  <input type="time" name="time" class="form-control" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Rekaman</label>
                                  <select class="form-control selectpicker" name="recording">
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                  </select>
                                </div>
                                <input type="hidden" name="course_id" value="{{$course->id}}">
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary btn-raised">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>

                  <div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($meeting_schedules as $index => $meeting_schedule)
                      <div class="mb-0 card card-default">
                        <div class="card-header" role="tab" id="headingWebinar{{$index}}">
                          <h4 class="card-title">
                            <a class="collapsed dq-none-rotation" role="button" data-toggle="collapse" data-parent="#accordion" href="#webinar{{$index}}" aria-expanded="false" aria-controls="collapse{{$index}}">
                              <span class="dq-section-auto pull-right">
                                <p class="color-info">
                                  <i class="fa fa-calendar"></i> {{$meeting_schedule->date}}
                                  &nbsp;|&nbsp;
                                  <i class="fa fa-clock-o"></i> {{$meeting_schedule->time}}
                                </p>
                              </span>
                              <h2 class="headline-sm">{{$meeting_schedule->name}}</h2>
                            </a>
                          </h4>
                        </div>
                        <div id="webinar{{$index}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingWebinar{{$index}}">
                          <div class="card-block">
                            <p class="pull-right">
                              <b class="color-dark">Rekaman:</b>
                              &nbsp;
                              {{$meeting_schedule->recording == '1' ? 'Ya' : 'Tidak'}}
                              {{-- {!! getRecording($meeting_schedule->id) ? '&nbsp;|&nbsp; <a target="_blank" href="'.getRecording($meeting_schedule->id)->playback->format->url.'">Lihat Rekaman</a>' : '' !!}</p> --}}
                            <h3 class="headline-sm fs-20 mt-0"><span>Deskripsi</span></h3>
                            <p>{{$meeting_schedule->description}}</p>
                            <hr>
                            @if($meeting_schedule->date == date("Y-m-d"))
                              <a title="Start Meeting" href="{{url('bigbluebutton/meeting/instructor/join/'.$course->id. '/' . $meeting_schedule->id )}}" class="btn btn-raised btn-success">Mulai</a>
                            @endif
                            <a id="webinarEditSchedule" data-id="{{$meeting_schedule->id}}" data-name="{{$meeting_schedule->name}}" data-description="{{$meeting_schedule->description}}" data-date="{{$meeting_schedule->date}}" data-time="{{$meeting_schedule->time}}" data-recording="{{$meeting_schedule->recording}}" href="#/" class="btn btn-raised btn-warning">Ubah</a>
                            <a onclick="return confirm('Yakin?')" class="btn btn-raised btn-danger" href="/meeting/schedule/delete/{{$meeting_schedule->id}}">Hapus</a>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>

                  <div class="modal fade" id="modalWebinarEditSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="">Edit Jadwal Webinar</h4>
                        </div>
                        <form class="" action="/meeting/schedule/update" method="post">
                          {{csrf_field()}}
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="">Judul</label>
                                  <input type="text" name="name" class="form-control" id="webinarName" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Deskripsi / Penjelasan</label>
                                  <textarea name="description" class="form-control" id="webinarDescription" rows="8" cols="80"></textarea>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="">Tanggal</label>
                                  <input type="date" name="date" id="webinarDate" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Waktu</label>
                                  <input type="time" name="time" class="form-control" id="webinarTime" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="">Rekaman</label>
                                  <select class="form-control selectpicker" id="webinarRecording" name="recording">
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                  </select>
                                </div>
                                <input type="hidden" name="course_id" value="{{$course->id}}">
                                <input type="hidden" name="id" id="webinarId" value="{{$course->id}}">
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary btn-raised">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>

                </div>

                <!-- TAB Q&A -->
                <div role="tabpanel" class="tab-pane fade" id="QandA">
                  {{-- <div class="card">
                    <table class="table dq-table">
                      <tr>
                        <td class="col-md-4 dq-no-margin">
                          <input type="text" class="form-control" name="" placeholder="Search questions">
                        </td>
                        <td>or</td>
                        <td></td>
                        <td><a href="#" data-toggle="modal" data-target="#modalAskQuestion" class="btn btn-raised btn-lg btn-primary">Ask a new question</a></td>
                      </tr>
                    </table>
                  </div> --}}
                  {{-- <div class="row">
                    <div class="col-lg-4 dq-no-margin">
                      <select class="form-control selectpicker" data-dropup-auto="false">
                        <option>Sort by recency</option>
                        <option>Sort by popular</option>
                      </select>
                    </div>
                    <div class="col-lg-8 dq-no-margin">
                      <div class="checkbox dq-padding right">
                        <label>
                          <input type="checkbox">See questions I'm following</label>
                        <label>
                          <input type="checkbox">See questions without responses</label>
                      </div>
                    </div>
                  </div> --}}
                  <div class="card">
                    <ul class="list-group">
                      @foreach($discussions as $discussion)
                        <a href="/discussion/detail/{{$discussion->id}}">
                          <li class="list-group-item card-block">
                            <span class="text-center dq-pl">
                              <img src="{{avatar($discussion->photo)}}" class="dq-frame-round-xs" alt="{{$discussion->name}}">
                            </span>
                            <span class="col-lg-10 dq-max">
                              <b>{{$discussion->name}}</b>
                              <p>{{$discussion->body}}</p>
                            </span>
                            <span class="text-center dq-pl dq-pr">
                              <b>{{(DB::table('discussions_answers')->where('discussion_id', $discussion->id))->count()}}</b>
                              <p>responses</p>
                            </span>
                          </li>
                        </a>
                      @endforeach
                    </ul>
                  </div>
                  <div class="text-center">
                    {{-- <a href="" class="btn btn-raised btn-info">
                      Load More
                    </a> --}}
                  </div>
                </div>

                <!-- TAB ANNOUNCEMENTS -->
                <div role="tabpanel" class="tab-pane fade" id="announcements">
                  <a title="Pengumuman" href="{{url('course/announcement/create/'.$course->id)}}" class="btn btn-primary btn-raised">
										Buat Pengumuman
									</a>
                  @foreach($announcements as $announcement)
                    <div class="card">
                      <div class="card-block-big">
                        <p>
                          <div class="row">
                            <img src="{{avatar($announcement['photo'])}}" class="dq-image-user" style="margin-left:15px">
                            <div class="col-md-8">
                              <div>
                                {{$announcement['name']}}
                              </div>
                              <div>
                                <span>{{$announcement['created_at']}}</span>
                              </div>
                            </div>
                          </div>
                        </p>
                        <h3>{{$announcement['title']}}</h3>
                        <p>
                          {!!$announcement['description']!!}
                        </p>
                        <form name="delete" action="/course/announcement/delete/{{$announcement['id']}}" method="post">
													{{csrf_field()}}
													<input type="hidden" name="_method" value="DELETE">
													<button type="submit" class="btn btn-danger btn-raised btn-sm" onclick="return confirm('Yakin akan menghapus pengumuman?')">Delete</button>
                          <a class="btn btn-warning btn-raised btn-sm" href="/course/announcement/edit/{{$announcement['id']}}">Edit</a>
												</form>
                        <!--FLOATING FORM-->
                        <p>
                          <form class="form-group label-floating"  method="post" action="#">
                            {{csrf_field()}}
                            <div class="input-group">
                              <span class="input-group-addon">
                                <img src="{{avatar(Auth::user()->photo)}}" class="dq-image-user dq-ml">
                              </span>
                              <label class="control-label" for="addon2">Enter your comment</label>
                              <input type="text" id="addon2" class="form-control" name="comment">
                              <input type="hidden" name="course_announcement_id" value="{{$announcement['id']}}">
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-raised btn-primary">
                                  Kirim
                                </button>
                              </span>
                            </div>
                          </form>
                        </p>

                        <p>
                          <div class="ms-collapse no-margin" id="accordion{{$announcement['id']}}" role="tablist" aria-multiselectable="true">
                            <div class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{$announcement['id']}}" href="#collapse{{$announcement['id']}}" aria-expanded="false" aria-controls="collapse{{$announcement['id']}}">
                                 Comments({{count($announcement['announcement_comments'])}})
                              </a>
                              <div id="collapse{{$announcement['id']}}" class="card-collapse collapse" role="tabpanel">
                                @foreach($announcement['announcement_comments'] as $announcement_comment)
                                <div class="dq-padding-comment">
                                  <span>
                                    <div class="row">
                                      <img src="{{avatar($announcement_comment->photo)}}" class="dq-image-user dq-ml">
                                      <div class="col-md-8">
                                        <div>
                                          <span>
                                            {{$announcement_comment->name}}
                                          </span>
                                          .
                                          <span>{{$announcement_comment->created_at}}</span>
                                          {{-- .
                                          <span>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Report Abuse">
                                              <i class="fa fa-flag"></i>
                                            </a>
                                          </span> --}}
                                        </div>
                                        <div>
                                          <span>
                                            {{$announcement_comment->comment}}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </span>
                                </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </p>
                      </div>
                    </div>
                  @endforeach
                </div>

                <div role="tabpanel" class="tab-pane fade" id="progresses">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-block text-center">
                          <h3>Progres Peserta</h3>

        									<a title="Penyelesaian Peserta" href="{{url('course/completion/'.$course->id)}}" class="btn btn-default btn-raised">
        										Penyelesaian Peserta
        									</a>
        									<a title="Nilai Peserta" href="{{url('course/grades/'.$course->id)}}" class="btn btn-primary btn-raised">
        										Nilai Peserta
        									</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="modal" id="modalSetEditorDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <textarea name="editorDescription" id="editorDescription"></textarea>
            <input type="button" id="saveEditorDescription" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalSetEditorGoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <textarea name="editorGoal" id="editorGoal"></textarea>
            <input type="button" id="saveEditorGoal" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalSetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <img class="img-fluid" src="{{asset_url(course_image_medium($course->image))}}" alt="{{$course->title}}">
            <input type="file" id="editorImage" value="">
            <input type="button" id="saveEditorImage" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalEditSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Edit Section</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label for="">Judul</label>
                <input type="text" class="form-control" id="setSectionTitle">
              </div>

              <div class="form-group">
                <label for="">Deskripsi / Penjelasan</label>
                <textarea id="setSectionDescription" class="form-control" rows="6"></textarea>
              </div>
              <input type="hidden" id="setSectionId">
              <input type="button" id="saveEditSection" value="Save" class="btn btn-primary btn-raised">
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="modal" id="modalAddContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title" id="contentForm"></h3>
            </div>
            <div class="panel-body">
              <form id="formAddContent" action="" method="post">
                {{csrf_field()}}

                <div class="form-group">
                  <label for="title">Judul</label>
                  <input placeholder="Judul" type="text" class="form-control" id="contentTitle" name="title" value="" required>
                </div>

                <div class="form-group">
                  <label for="title">Deskripsi / Penjelasan</label>
                  <textarea name="description" id="contentDescription"></textarea>
                </div>

                <div class="form-group">
                  <label for="title">Tipe Konten</label>
                  <select class="form-control selectpicker" id="contentTypeContent" name="type_content" required>
                    <option value="">Pilih Tipe Konten</option>
                    <option value="text">Text</option>
                    <option value="video">Video</option>
                    <option value="file">File</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="title">Lampiran (Dokumen / Video)</label>
                  <input type="file" id="file" class="form-control">
                  <input type="hidden" id="path_file" name="path_file">
                  <input type="hidden" id="name_file" name="name_file">
                  <input type="hidden" id="file_size" name="file_size">
                  <input type="hidden" id="video_duration" name="video_duration">
                </div>

                <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                <div id="container"></div>

                <div class="progress" style="display:none">
                  <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only">0%</span>
                  </div>
                </div>
                <div id="uploadedMessage"></div>

                <div class="form-group">
                  <label for="title">Urutan</label>
                  <input placeholder="Urutan" id="contentSequence" type="text" class="form-control" name="sequence" value="" required>
                </div>

                <input type="hidden" name="id" id="contentId" value="">
                <div class="form-group">
                  {{  Form::submit("Save Content" , array('class' => 'btn btn-primary', 'name' => 'button', 'id' => 'saveContent')) }}
                </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="modal" id="modalAddQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title" id="quizForm"></h3>
            </div>
            <div class="panel-body">
              <form id="formAddQuiz" action="" method="post">
                {{csrf_field()}}

                <div class="form-group">
                  {{-- <label for="name">Judul Kuis</label> --}}
                  <input placeholder="Judul Kuis" type="text" class="form-control" name="name" id="quizname" required>
                </div>

                <div class="form-group">
                  <label for="title">Deskripsi / Penjelasan</label>
                  <textarea name="description" id="Quizdescription"></textarea>
                </div>
                <div class="form-group">
                  <label for="title">Acak kuis?</label> &nbsp;
                  <input type="radio" name="shuffle" checked value="0">Tidak &nbsp;
                  <input type="radio" name="shuffle" value="1">Ya
                </div>
                <div class="form-group">
                  {{-- <label for="name">Waktu Mulai</label> --}}
                  <input placeholder="Waktu Mulai" type="text" class="form-control form_datetime" name="time_start" id="Quiztime_start" required>
                </div>
                <div class="form-group">
                  {{-- <label for="name">Waktu Akhir</label> --}}
                  <input placeholder="Waktu Akhir" type="text" class="form-control form_datetime" name="time_end" id="Quiztime_end" required>
                </div>
                <div class="form-group">
                  <label for="name">Durasi Menit </label>
                  <input placeholder="(Contoh: 60)" type="number" class="form-control" name="duration" id="Quizduration" required>
                </div>

                <input type="hidden" name="id" id="QuizId" value="">
                <div class="form-group">
                  {{  Form::submit("Simpan Kuis" , array('class' => 'btn btn-raised btn-primary', 'name' => 'button', 'id' => 'saveQuiz')) }}
                </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
    </div>
  </div>

@endsection

@push('script')
  <script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
  <script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  <script>
    $.fn.editable.defaults.mode = 'inline';

    $(document).ready(function() {

      $('#CourseTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'title',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseSubTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'subtitle',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        },
      });

      $('#CoursePrice').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'price',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseLevel').editable({
        value: {{$course->level_id}},
        source: [
          @php $course_levels = DB::table('course_levels')->get(); @endphp
          @foreach($course_levels as $course_level)
            {value: {{$course_level->id}}, text: '{{$course_level->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_level_course',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseCategory').editable({
        value: {{$course->id_category}},
        source: [
          @php $categories = DB::table('categories')->get(); @endphp
          @foreach($categories as $category)
            {value: {{$category->id}}, text: '{{$category->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_category',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

    });
</script>

{{-- CKEDITOR --}}
<script src="{{url('ckeditor/ckeditor.js')}}"></script>
<script>
  const textDescription = `{!! $course->description !!}`;
  const textGoal = `{!! $course->goal !!}`;
  CKEDITOR.replace('editorDescription').setData(textDescription);
  CKEDITOR.replace('editorGoal').setData(textGoal);
	CKEDITOR.replace('contentDescription');
	CKEDITOR.replace('Quizdescription');
</script>
{{-- CKEDITOR --}}

<script>
  $("#saveEditorDescription").click(function(){
    var new_description = CKEDITOR.instances.editorDescription.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'description',
        value : new_description,
      },
      success : function(data){
        $("#modalSetEditorDescription").modal('hide')
        $("#recentDescription").html(new_description);
      }
    })
  })

  $("#saveEditorGoal").click(function(){
    var new_goal = CKEDITOR.instances.editorGoal.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'goal',
        value : new_goal,
      },
      success : function(data){
        $("#modalSetEditorGoal").modal('hide')
        $("#recentGoal").html(new_goal);
      }
    })
  })

  var fileName = '';
  $("#editorImage").change(function(e){
    fileName = e.target.files[0];
  })

  $("#saveEditorImage").click(function(e){
    var formData = new FormData();
    formData.append('value', fileName);
    formData.append('name', 'image');

    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,
      headers: {
	       'X-CSRF-TOKEN': '{{csrf_token()}}'
	    },
      success : function(data){
        location.reload();
      }
    })
  })
</script>

{{-- add new section --}}
<script type="text/javascript">
  $("#add_new_section").click(function(){

    $("#section_row").val(parseInt($("#section_row").val())+1)
    var section_add_row = $("#section_row").val();

    $("#place_new_section").append(
      '<div class="panel panel-default" id="section_row_'+section_add_row+'">'+
        '<div class="panel-heading">'+
          '<h3 class="panel-title">Tambah Topik</h3>'+
        '</div>'+
        '<div class="panel-body">'+
          '<div class="form-group">'+
            '<input placeholder="Judul Topik" type="text" class="form-control" id="new_section_title_'+section_add_row+'" value="" required>'+
          '</div>'+
            '<button type="button" id="save_new_section" data-id="'+section_add_row+'" class="btn btn-primary btn-raised">Simpan</button> '+
            '<button type="button" id="remove_new_section" data-id="'+section_add_row+'" class="btn btn-danger btn-raised">Batal</button>'+
        '</div>'+
      '</div>'
    );
  });
</script>
{{-- add new section --}}

{{-- save new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#save_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var new_section_title = $("#new_section_title_"+data_id).val();

        var id_course = '{{$course->id}}';
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('course/save_section')}}",
          type : "POST",
          data : {
            title : new_section_title,
            id_course : id_course,
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save new section --}}

{{-- remove new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#remove_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        $("#section_row_"+data_id).remove();
    });
  });
</script>
{{-- remove new section --}}

{{-- set edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#EditSection',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var data_title = $(this).attr('data-title');
        var data_description = $(this).attr('data-description');

        $("#setSectionId").val(data_id)
        $("#setSectionTitle").val(data_title)
        $("#setSectionDescription").val(data_description)
        $("#modalEditSection").modal('show');
    });
  });
</script>
{{-- set edit section --}}

{{-- save edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#saveEditSection',function(e){
      e.preventDefault();
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('section/edit-section')}}",
          type : "POST",
          data : {
            title : $("#setSectionTitle").val(),
            description : $("#setSectionDescription").val(),
            id : $("#setSectionId").val(),
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save edit section --}}


<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>

{{-- set sectionid on add content --}}
<script type="text/javascript">
  var data_section = '';
  $(function(){
    $(document).on('click','#AddContent',function(e){
      e.preventDefault();
        $("#contentForm").html('Tambah Konten');
        data_section = $(this).attr('data-section');
        $("#formAddContent").attr('action', '/course/content/create_action/'+data_section)
        $("#modalAddContent").modal('show');

        // Custom example logic
        var path_file = $("#path_file").val();
        var name_file = $("#name_file").val();
        var file_size = $("#file_size").val();
        var video_duration = $("#video_duration").val();
        var section_id = data_section;
        // $("#saveContent").prop('disabled', true);

        var uploader = new plupload.Uploader({
          runtimes : 'html5,flash,silverlight,html4',
          browse_button : 'file', // you can pass an id...
          container: document.getElementById('container'), // ... or DOM Element itself
          url : '/course/content/upload/'+section_id,
          flash_swf_url : '/plupload/Moxie.swf',
          silverlight_xap_url : '/plupload/Moxie.xap',
          chunk_size: '1mb',
          dragdrop: true,
          headers: {
             'X-CSRF-TOKEN': '{{csrf_token()}}'
          },
          filters : {
            mime_types: [
              {title : "Document files", extensions : "docs,xlsx,pptx"},
              {title : "Video files", extensions : "mp4,webm"},
            ]
          },

          init: {
            PostInit: function() {
              document.getElementById('filelist').innerHTML = '';
            },

            FilesAdded: function(up, files) {
              plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                uploader.start();
                return false;
              });
            },

            UploadProgress: function(up, file) {
              // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
              $('.progress').show();
              $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
              $("#uploadedMessage").html('Upload dalam proses...');
            },

            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                // console.log('[FileUploaded] File:', file, "Info:", info);
                var response = JSON.parse(info.response);
                // console.log(response.result.original.path);
                path_file = $("#path_file").val(response.result.original.path);
                name_file = $("#name_file").val(response.result.original.name);
                file_size = $("#file_size").val(response.result.original.size);
                video_duration = $("#video_duration").val(response.result.original.video_duration);
            },

            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                // console.log('[ChunkUploaded] File:', file, "Info:", info);
            },

            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                // console.log('[UploadComplete]');
                // $("#saveContent").prop('disabled', false);
                $("#uploadedMessage").html('Upload selesai');
            },

            Error: function(up, err) {
              document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
          }
        });

        uploader.init();
    });
  });
</script>
{{-- set sectionid on add content --}}

<script>
// edit content
$(function(){
  $(document).on('click','#EditContent',function(e){
    e.preventDefault();
    $("#contentForm").html('Edit Content');
    $("#formAddContent").attr('action', '/course/content/update_action');
    var data_id = $(this).attr('data-id');
    var data_title = $(this).attr('data-title');
    var data_description = $(this).attr('data-description');
    var data_type_content = $(this).attr('data-type-content');
    var data_sequence = $(this).attr('data-sequence');
    var data_section = $(this).attr('data-section');
    $("#contentId").val(data_id);
    $("#contentTitle").val(data_title);
    CKEDITOR.instances['contentDescription'].setData(data_description)
    $("#contentTypeContent").val(data_type_content).change();
    $("#contentSequence").val(data_sequence);
    $("#modalAddContent").modal('show');

    // Custom example logic
    var path_file = $("#path_file").val();
    var name_file = $("#name_file").val();
    var file_size = $("#file_size").val();
    var video_duration = $("#video_duration").val();
    var section_id = data_section;
    // $("#saveContent").prop('disabled', true);

    var uploader = new plupload.Uploader({
      runtimes : 'html5,flash,silverlight,html4',
      browse_button : 'file', // you can pass an id...
      container: document.getElementById('container'), // ... or DOM Element itself
      url : '/course/content/upload/'+section_id,
      flash_swf_url : '/plupload/Moxie.swf',
      silverlight_xap_url : '/plupload/Moxie.xap',
      chunk_size: '1mb',
      dragdrop: true,
      headers: {
         'X-CSRF-TOKEN': '{{csrf_token()}}'
      },
      filters : {
        mime_types: [
          {title : "Document files", extensions : "docs,xlsx,pptx"},
          {title : "Video files", extensions : "mp4,webm"},
        ]
      },

      init: {
        PostInit: function() {
          document.getElementById('filelist').innerHTML = '';
        },

        FilesAdded: function(up, files) {
          plupload.each(files, function(file) {
            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            uploader.start();
            return false;
          });
        },

        UploadProgress: function(up, file) {
          // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
          $('.progress').show();
          $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
          $("#uploadedMessage").html('Upload dalam proses...');
        },

        FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            // console.log('[FileUploaded] File:', file, "Info:", info);
            var response = JSON.parse(info.response);
            // console.log(response.result.original.path);
            path_file = $("#path_file").val(response.result.original.path);
            name_file = $("#name_file").val(response.result.original.name);
            file_size = $("#file_size").val(response.result.original.size);
            video_duration = $("#video_duration").val(response.result.original.video_duration);
        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
            // $("#saveContent").prop('disabled', false);
            $("#uploadedMessage").html('Upload selesai');
        },

        Error: function(up, err) {
          document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
        }
      }
    });

    uploader.init();

  })
})
// edit content
</script>

{{-- datetime picker --}}
<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript">
  $('.form_datetime').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
  });
</script>
{{-- datetime picker --}}

<script>
// add quiz
  var data_section = '';
  $(function(){
    $(document).on('click','#AddQuiz',function(e){
      $("#quizForm").html('Tambah Kuis');
      data_section = $(this).attr('data-section');
      $("#formAddQuiz").attr('action', '/course/quiz/create_action/'+data_section)
      $("#modalAddQuiz").modal('show');
    })
  })
  // add quiz
</script>

<script type="text/javascript">
  $("[name=publish{{$course->id}}]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
    var token = "{{csrf_token()}}";
    if(state === true){
      $.ajax({
        url : "/meeting/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          publish : 1,
          _token: token
        },
        success : function(result){

        },
      });
    }else{
      $.ajax({
        url : "/meeting/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          publish : 0,
          _token: token
        },
        success : function(result){

        },
      });
    }
  });
</script>


<script>
// trigger modal add konten / quiz
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

  // trigger modal add konten / quiz
</script>

<script>
$(function(){
  $(document).on('click','#webinarEditSchedule',function(e){
    $("#webinarId").val($(this).attr('data-id'));
    $("#webinarName").val($(this).attr('data-name'));
    $("#webinarDescription").val($(this).attr('data-description'));
    $("#webinarDate").val($(this).attr('data-date'));
    $("#webinarTime").val($(this).attr('data-time'));
    $("#webinarRecording").val($(this).attr('data-recording')).change();
    $("#modalWebinarEditSchedule").modal('show');
  })
})
</script>

{{-- sortable --}}
<script src="/jquery-ui/jquery-ui.js"></script>

<script>
  $( function() {
    $( ".SectionSortable" ).sortable({
      axis: 'y',
      connectWith: '.SectionSortable',
      helper: 'clone',
      out: function(event, ui) {
        var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
        console.log(itemOrder)

        $.ajax({
          url : '/course/section/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'course_id' : '{{$course->id}}',
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    });
    $( ".SectionSortable" ).disableSelection();
  });
</script>

<script>
  $( function() {
    var section_id = ''
    $( ".ContentSortable" ).sortable({
      axis: 'y',
      connectWith: '.ContentSortable',
      helper: 'clone',
      placeholder: "placeholder",
      over:function(event,ui){
        section_id = $('.placeholder').parent().attr('id');
      },
      out: function(event, ui) {
        var sequence = ui.item.index();
        var content_id = ui.item.attr('id');
        var itemOrder = $(this).sortable('toArray', { attribute: 'id' });
        console.log(itemOrder);
        $.ajax({
          url : '/course/content/update-sequence',
          type : 'POST',
          data : {
            'datas' : itemOrder,
            'section_id' : section_id,
            '_token' : '{{csrf_token()}}'
          },
          success : function(){
          }
        })
      },
    }).disableSelection();
  });
</script>

{{-- sortable --}}

<script type="text/javascript">
  $("[class=preview]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
    var token = "{{csrf_token()}}";
    var content_id = $(this)[0].value;
    if(state === true){
      // alert(state);
      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 1,
          _token: token
        },
        success : function(result){
          // alert(result.message);
          if(result.alert == 'error'){
            $("[id=preview"+content_id+"]").bootstrapSwitch('state', false)
          }
          swal(result.title, result.message, result.alert);
        },
      });
    }else{
      // alert(state);
      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 0,
          _token: token
        },
        success : function(result){
          // swal(result.title, result.message, result.alert);
        },
      });
    }
  });
</script>

<script type="text/javascript">
  // $(function(){
  //   $(document).ready(function(){
  //     $('.get-from-library').on('click', function(){
  //       $('#content-id-value').val($('.get-from-library').attr('content-id'));
  //       alert($('#content-id-value').val());
  //     })
  //   })
  // })
</script>

@endpush
