@extends('layouts.app')

@section('title')
	{{ 'Buat Kuis ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					{{-- <div class="box-header with-border">
						<h3 class="box-title">{{$button}} Kuis - {{$section->title}}</h3>
					</div> --}}
					<div class="box-body">

						<center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">{{$button}} Kuis</h3>
						  </div>
						  <div class="panel-body">
								{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

									<div class="form-group">
										<label for="name">Judul Kuis</label>
										<input placeholder="Judul Kuis" type="text" class="form-control" name="name" value="{{ $name }}" required>
									</div>

									<div class="form-group">
										<label for="title">Deskripsi / Penjelasan</label>
										<textarea name="description" id="description">{{ $description }}</textarea>
									</div>
									<div class="form-group">
										<label for="title">Acak?</label><br>
										<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
										<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
									</div>
									<div class="form-group">
										<label for="name">Tanggal Mulai</label>
										<input placeholder="Tanggal Mulai" type="text" class="form-control form_datetime" name="time_start" value="{{ $time_start }}" required>
									</div>
									<div class="form-group">
										<label for="name">Tanggal Selesai</label>
										<input placeholder="Tanggal Selesai" type="text" class="form-control form_datetime" name="time_end" value="{{ $time_end }}" required>
									</div>
									<div class="form-group">
										<label for="name">Durasi (Menit)</label>
										<input placeholder="60" type="text" class="form-control" name="duration" value="{{ $duration }}" required>
									</div>
									<div class="form-group">
										<label for="name">Kesempatan / Percobaan</label>
										<input placeholder="3" type="text" class="form-control" name="attempt" value="{{ $attempt }}" required>
									</div>
									<div class="form-group">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
										<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a>
									</div>
								{{ Form::close() }}
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}
@endpush
