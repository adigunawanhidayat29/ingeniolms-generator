<form method="POST" action="/badge/store">
{{ csrf_field() }}
  <div class="form-group">
    <label for="badge_name">Badge Name</label>
    <input type="text" class="form-control" id="badge_name">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <input type="text" class="form-control" id="description">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="idcourse" value="">
  </div>
  <div class="form-group">
    <div class="custom-file">
        <input type="file" class="custom-file-input" id="badge_image">
        <label class="custom-file-label" for="badge_image">Choose file</label>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

@push('script')

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>

@endpush