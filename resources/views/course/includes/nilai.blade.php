<div class="row">
    <div class="col-md-3 nav-tabs-ver-container">
      <div class="card no-shadow">
        <ul class="nav nav-tabs-ver" role="tablist" style="background: #fff; margin-bottom: 0; padding-bottom: 0;">
          <li class="nav-item"><a class="nav-link active" href="#gradeDetail" aria-controls="gradeDetail" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> @lang('front.learn_course.grade_menu_detail')</a></li>
          <li class="nav-item"><a class="nav-link" href="#gradeCertificate" aria-controls="gradeCertificate" role="tab" data-toggle="tab"><i class="fa fa-certificate"></i> @lang('front.learn_course.grade_menu_ceritificate')</a></li>
          <li class="nav-item"><a class="nav-link" href="#absensi" aria-controls="gradeCertificate" role="tab" data-toggle="tab"><i class="fa fa-certificate"></i> @lang('front.learn_course.grade_menu_attendance')</a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-9">
      <div class="card card-light no-shadow">
        <div class="card-block">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="gradeDetail">
              <h3 class="headline headline-sm mt-0 mb-2">@lang('front.learn_course.grade_menu_detail')</h3>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>@lang('front.learn_course.grade_table_head_content')</th>
                      <th>@lang('front.learn_course.grade_table_head_grade')</th>
                    </tr>
                  </thead>
                  <tbody>

                    @php
                      $grade_total = 0 ;
                      $grade_percent_total = 0 ;
                      $grade_percent_total_assigment = 0 ;
                    @endphp

                    @foreach($sections as $section)
                      @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                        
                        @if($all->section_type == 'quizz')
                          @php $quiz = $all; @endphp

                          @php
                            $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                          @endphp
                          @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => \Auth()->user()->id, 'quiz_id' => $quiz->id])->first() @endphp

                          @php
                            $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                            if(!$quiz_grade_setting){
                              $quiz_grade_setting_percent = 100;
                            }else{
                              $quiz_grade_setting_percent = $quiz_grade_setting->percentage;
                            }
                          @endphp

                          @if($quiz_participant)
                            @php
                              $grade = $quiz_participant->grade
                            @endphp
                          @else
                            @php $grade = 0 @endphp
                          @endif

                          @php $grade_percent_total = ($grade * $quiz_grade_setting_percent) / 100  @endphp
                          @php $grade_total += $grade_percent_total @endphp

                          <tr>
                            <td>{{$quiz->name}} ({{isset($quiz_grade_setting) ? $quiz_grade_setting_percent : '0'}}%)</td>
                            <td>{{ $grade }}</td>
                          </tr>
                        @endif
                      

                        @if($all->section_type == 'assignment')
                          @php $assignment = $all; @endphp

                          @php
                            $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                          @endphp

                          @php
                            $assignments_answers = \DB::table('assignments_answers')
                              ->where(['assignments_answers.user_id' => \Auth::user()->id, 'assignments_answers.assignment_id' => $assignment->id])
                              ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                              ->first()
                          @endphp

                          @php
                            $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                            if(!$assignment_grade_setting){
                              $assignment_grade_setting_percent = 100;
                            }else{
                              $assignment_grade_setting_percent = $assignment_grade_setting->percentage ?? 0;
                            }
                          @endphp

                          @if($assignments_answers)
                            @php $grade_assignment = $assignments_answers->grade @endphp
                          @else
                            @php
                              $grade_assignment = 0 ;
                            @endphp
                          @endif

                          @php $grade_percent_total_assigment = ($grade_assignment * $assignment_grade_setting_percent) / 100  @endphp
                          @php $grade_total += $grade_percent_total_assigment @endphp

                          <tr>
                            <td>{{$assignment->title}} ({{isset($assignment_grade_setting) ? $assignment_grade_setting->percentage : '0'}}%)</td>
                            <td>{{ $grade_assignment }}</td>
                          </tr>
                        @endif

                      @endforeach
                    @endforeach

                    @php
                      // echo $grade_total;
                      // $countQuizAssignment = count($section['section_quizzes']) + count($section['section_assignments']);
                      // if ($countQuizAssignment > 0 ) {
                      //   $totalFinish = $grade_total;
                      // } else {
                      //   $totalFinish = 0;
                      // }
                      // echo $grade_total;
                    @endphp

                    <tr class="fw-600" style="border-top: 2px solid rgba(0,0,0,.2);">
                      <td>@lang('front.learn_course.grade_total')</td>
                      <td>{{ round($grade_total, 2)}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="gradeCertificate">
              <div class="d-flex align-items-center justify-content-between mb-2">
                <h3 class="headline headline-sm mt-0 mb-0">@lang('front.learn_course.grade_menu_ceritificate')</h3>
                <div>
                  @if($certificate)
                    <a href="#" onclick="printDiv('printableArea')" class="btn btn-sm btn-raised btn-primary mt-0 mb-0"><i class="zmdi zmdi-print"></i>@lang('front.general.text_print')</a>
                    <a href="#" onclick="downloadCert()" class="btn btn-sm btn-raised btn-success mt-0 mb-0"><i class="zmdi zmdi-download"></i>@lang('front.general.text_download')</a>
                  @endif
                </div>
              </div>
              @if(!$certificate)

                  <div class="alert alert-info">
                    @lang('front.learn_course.grade_ceritificate_not_yet')
                  </div>

              @else

                  @php
                    $validCeritificate = false;

                    // jika pengaturan sertifikat adalah persentase
                    if ($course->get_ceritificate == '1') {
                      $certificate_percentage = ceil($percentage >= 100 ? 100 : ($percentage <= 1 ? 0 : $percentage));
                      if ($certificate_percentage >= 100) {
                        $validCeritificate = true;
                      }
                    }

                    // jika pengaturan sertifikat adalah grade
                    if ($course->get_ceritificate == '2') {
                      $certificate_grade = $totalFinish;
                      if ($certificate_grade >= 80) {
                        $validCeritificate = true;
                      }
                    }

                    // jika pengaturan sertifikat adalah manual
                    if ($course->get_ceritificate == '3') {
                      $isCeritificateUser = \DB::table('ceritificate_course_users')->where(['user_id' => Auth::user()->id, 'course_id' => $course->id])->first();
                      if ($isCeritificateUser) {
                        $validCeritificate = true;
                      }
                    }

                  @endphp

                  @if($validCeritificate)
                    <div class="" id="printableArea">
                        <svg  width="600" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image href="{{ $certificate->image }}" height="600" width="600" />
                            @foreach ( $certificate->certificate_attributes as $data )
                              @php
                                $position_name = '';
                                if ($data->position == 'Nama Peserta') {
                                  $position_name = Auth::user()->name;
                                }
                                if ($data->position == 'Nama Kelas') {
                                  $position_name = $course->title;
                                }
                                if ($data->position == 'Tanda Tangan') {
                                  $position_name = $course->author;
                                }
                              @endphp
                              <text style="font-size: 16px; text-align: center;" x="{{ $data->x_coordinate }}" y="{{ $data->y_coordinate }}">{{ $position_name }}</text>
                            @endforeach
                        </svg>
                    </div>
                  @else
                    <div class="alert alert-info">
                      @lang('front.learn_course.grade_certificate_cannot_be_obtained_yet')
                    </div>
                  @endif

              @endif

            </div>

            <div role="tabpanel" class="tab-pane" id="absensi">
              <!--ABSENSI ACTIVITY-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.learn_course.grade_menu_attendance') <span>@lang('front.learn_course.grade_presence')</span></h2>
                </div>
                <div class="col-md-12">
                  <div class="card">

                    <div class="table-responsive">
                      <table class="table table-striped">
                        <tr>
                          <th>@lang('front.learn_course.grade_attendance_table_no')</th>
                          <th>@lang('front.learn_course.grade_attendance_table_name')</th>
                          <th>@lang('front.learn_course.grade_attendance_table_description')</th>
                          <th>@lang('front.learn_course.grade_attendance_table_date_start')</th>
                          <th>@lang('front.learn_course.grade_attendance_table_date_end')</th>
                          <th class="text-right">@lang('front.learn_course.grade_attendance_table_option')</th>
                        </tr>
                        @foreach($CourseAttendances as $index => $data)
                          <tr class="bg-white">
                            <td>{{$index + 1}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->description}}</td>
                            <td>{{$data->datetime}}</td>
                            <td>{{$data->end_datetime}}</td>
                            <td class="text-right">
                              @if ($data->manual_student == '1')
                                <a class="btn-circle btn-circle-primary btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#modalSessionDetail{{$data->id}}" title="Mulai"><i class="fa fa-{{$data->course_attendance_user_hasone ? 'refresh' : 'play'}}"></i></a>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </table>
                    </div>

                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach($CourseAttendances as $index => $data)
			<div class="modal" id="modalSessionDetail{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm m-0">Mulai Sesi {{$data->description}}</span></h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										<i class="zmdi zmdi-close"></i>
									</span>
								</button>
              </div>
              
              @if (!$data->course_attendance_user_hasone)

                <form action="/course/attendance/submit/user/{{$course->id}}/{{$data->id}}" method="post">
                  {{csrf_field()}}
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <tr>
                        <th class="{{$data->id}}check_p">Nama</th>
                        <th class="{{$data->id}}check_p">Deskripsi</th>
                        <th class="{{$data->id}}check_p">Hadir</th>
                        <th class="{{$data->id}}check_l">Telambat</th>
                        <th class="{{$data->id}}check_e">Berhalangan</th>
                        <th class="{{$data->id}}check_a">Absen</th>
                        <th>Remark</th>
                      </tr>
                          <tr class="bg-white">
                            <td>{{$data->name}}</td>
                            <td>{{$data->description}}</td>
                            <td>
                              <input type="radio" name="status" checked value="p">
                            </td>
                            <td>
                              <input type="radio" name="status" value="l">
                            </td>
                            <td>
                              <input type="radio" name="status" value="e">
                            </td>
                            <td>
                              <input type="radio" name="status" value="a">
                            </td>
                            <td>
                              <input type="text" class="form-control" name="remarks">
                            </td>
                          </tr>
                    </table>
                  </div>
    
                  <div class="form-group text-right">
                    <input type="submit" class="btn btn-raised btn-primary" value="Simpan Kehadiran">
                  </div>
                </form>
              @else
                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th class="check_p">Nama</th>
                      <th class="check_p">Deskripsi</th>
                      <th class="check_p">Hadir</th>
                      <th class="check_l">Telambat</th>
                      <th class="check_e">Berhalangan</th>
                      <th class="check_a">Absen</th>
                      <th>Remark</th>
                    </tr>
                        <tr class="bg-white">
                          <td>{{$data->name}}</td>
                          <td>{{$data->description}}</td>
                          <td>
                            <input type="checkbox" name="status" {{$data->course_attendance_user_hasone->status == 'p' ? 'checked' : ''}} value="p">
                          </td>
                          <td>
                            <input type="checkbox" name="status" {{$data->course_attendance_user_hasone->status == 'l' ? 'checked' : ''}} value="l">
                          </td>
                          <td>
                            <input type="checkbox" name="status" {{$data->course_attendance_user_hasone->status == 'e' ? 'checked' : ''}} value="e">
                          </td>
                          <td>
                            <input type="checkbox" name="status" {{$data->course_attendance_user_hasone->status == 'a' ? 'checked' : ''}} value="a">
                          </td>
                          <td>
                            <input type="text" class="form-control" name="remarks" value="{{$data->course_attendance_user_hasone->remarks}}">
                          </td>
                        </tr>
                  </table>
                </div>
              @endif

						</div>
					</div>
				</div>
			</div>
		@endforeach



@push('script')

<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.4/rgbcolor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stackblur-canvas/1.4.1/stackblur.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/canvg/dist/browser/canvg.min.js"></script>

  <script>
      function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          window.print();

          document.body.innerHTML = originalContents;
      }
  </script>

<script>

  // Initiate download of blob
  function download(
    filename, // string
    blob // Blob
  ) {
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveBlob(blob, filename);
    } else {
      const elem = window.document.createElement('a');
      elem.href = window.URL.createObjectURL(blob);
      elem.download = filename;
      document.body.appendChild(elem);
      elem.click();
      document.body.removeChild(elem);
    }
  }

  function downloadCert() {
    var svg = document.querySelector('svg');
    var data = (new XMLSerializer()).serializeToString(svg);

    var canvas = document.createElement('canvas');
    canvg(canvas, data, {
      renderCallback: function () {
        canvas.toBlob(function (blob) {
          download('Sertifikat-{{ $course->title . '-' . \Auth::user()->name }}.png', blob);
        });
      }
    });
  }


</script>

@endpush
