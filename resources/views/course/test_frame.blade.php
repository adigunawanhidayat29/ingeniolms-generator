{{-- @extends('layouts.app')

@section('title', 'Video')

@push('style')
  <link href="http://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
@endpush

@section('content')
  <video
    id="my-video"
    class="video-js"
    controls="true"
    preload="auto"
    width="640"
    height="264"
    data-setup='{"aspectRatio":"640:267", "fluid":"true"}' >
    <source src="{{asset_url('uploads/videos/laravel-51-basics-introductionmp4-3002.mp4')}}" type='video/mp4'>
  </video>
@endsection

@push('script')
  <!-- If you'd like to support IE8 -->
  <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
  <script src="http://vjs.zencdn.net/6.6.3/video.js"></script>
  <script type="text/javascript">
    // =========== VIDEO PROPERTY =======
    // videojs('my-video', {
    //   playbackRates: [0.5, 1, 1.5, 2, 10],
    //   controlBar: {
    //     fullscreenToggle: true
    //   }
    // });
    // =========== VIDEO PROPERTY =======


    // =========== EVENT ON SECOND =======
    var player = videojs('my-video');
    var timeCheck = function() {
      if (player.currentTime() > 5) {
        player.off('timeupdate', timeCheck);
        player.trigger('played5Second');
      }
    };
    player.on('played5Second', function(){
      player.pause();
      alert('muncul quis')
      console.log(player.currentTime()); // get current time
    });
    player.on('timeupdate', timeCheck);
    // =========== EVENT ON SECOND =======

  </script>
@endpush --}}




<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="http://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
<link href="/videojs/videojs.markers.min.css" rel="stylesheet">
<style media="screen">
  .vjs-play-progress{
    background: red !important;
    height: 5px;
  }
  .modal-dialog {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
</style>

<video
  id="my-video"
  class="video-js"
  controls="true"
  preload="auto"
  width="640"
  height="264"
  {{-- poster="{{asset_url('uploads/courses/laravel.png')}}" --}}
  data-setup='{"aspectRatio":"640:264", "fluid":"true"}' >
  <source src="{{asset_url('uploads/videos/laravel-51-basics-introductionmp4-3002.mp4')}}" type='video/mp4'>
</video>
<br>
<button class="btn btn-default" type="button" id="btnAdd">Buat Kuis</button>

<div class="modal fade" id="modalAddKuis" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Input Kuis Dalam detik ke <span id="video-secon">0</span></h4>
      </div>
      <form class="" action="/content-video-quiz/store" method="post">
        {{csrf_field()}}
        <div class="modal-body">
          <div class="form-group">
            <label for="">Pertanyaan</label>
            <input type="text" name="question" class="form-control" id="" placeholder="Pertanyaan" required>
            <input type="hidden" name="seconds" id="question_seconds">
            <input type="hidden" name="content_id" value="1">
          </div>
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <label for="">Jawaban</label>
                  <input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]" required>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="">Deskripsi Jawaban</label>
                    <input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">
                  </div>
                </div>
                <div class="form-group">
                  <select class="form-control" name="is_correct[]" style="width:15%">
                    <option value="1">Benar</option>
                    <option value="0">Salah</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <label for="">Jawaban</label>
                  <input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]">
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="">Deskripsi Jawaban</label>
                    <input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">
                  </div>
                </div>
                <div class="form-group">
                  <select class="form-control" name="is_correct[]" style="width:15%">
                    <option value="1">Benar</option>
                    <option value="0">Salah</option>
                  </select>
                </div>
              </div>
            </div>
            <div id="place_new_answer"></div>
          </div>
          <button type="button" id="add_new_answer" class="btn btn-default">Tambah Jawaban</button>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Simpan">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </form>
    </div>
  </div>
</div>

@foreach($content_video_quizes as  $data)
  <div class="modal fade" id="content_video_quizes{{$data->id}}" role="dialog" aria-labelledby="" style="z-index:9999999999999999999 !important; position:absolute !important;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">{{$data->question}}</h3>
            </div>
            <div class="panel-body">
              <div id="alertCheckAnswer{{$data->id}}"></div>
              @foreach($data->content_video_quiz_answers as $content_video_quiz_answer)
                <div class="radio">
                  <label><input type="radio" name="content_video_quiz_answer_{{$data->id}}" value="{{$content_video_quiz_answer->id}}">{{$content_video_quiz_answer->answer}}</label>
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="btnCheckAnswer" data-id="{{$data->id}}" class="btn btn-primary">Periksa Jawaban</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Lanjutkan</button>
        </div>
      </div>
    </div>
  </div>
@endforeach

<!-- If you'd like to support IE8 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/6.6.3/video.js"></script>
<script src="/videojs/videojs-markers.min.js"></script>
<script type="text/javascript">
  // =========== VIDEO PROPERTY =======
  videojs('my-video', {
    playbackRates: [0.5, 1, 1.5, 2],
    controlBar: {
      fullscreenToggle: true
    }
  });
  // =========== VIDEO PROPERTY =======


  // =========== EVENT ON SECOND =======
  var player = videojs('my-video');

  // markers
  player.markers({
    markerStyle: {
       'width':'3px',
       'background-color': 'yellow'
    },
    markers: [
      @foreach($content_video_quizes as  $data)
       {time: '{{$data->seconds}}.1', text: "{{$data->question}}"},
      @endforeach
    ]
  });
  // markers

    @foreach($content_video_quizes as  $data)
      var timeCheck{{$data->id}} = function() {
        if (player.currentTime() > {{$data->seconds}}) {
          player.off('timeupdate', timeCheck{{$data->id}});
          player.trigger('played{{$data->seconds}}Second');
        }
      };
      player.on('played{{$data->seconds}}Second', function(){
        player.pause();
        $("#content_video_quizes{{$data->id}}").modal({backdrop: 'static', keyboard: false, show:'true'});
        console.log(player.currentTime()); // get current time
      });
      player.on('timeupdate', timeCheck{{$data->id}});
    @endforeach

  // =========== EVENT ON SECOND =======

  $("#btnAdd").click(function(){
    // console.log(player.currentTime()); // get current time
    $("#video-secon").html(Math.floor(player.currentTime()))
    $("#question_seconds").val(Math.floor(player.currentTime()))
    player.pause();
    $("#modalAddKuis").modal('show');
  })
</script>

{{-- add new section --}}
<script type="text/javascript">
  $("#add_new_answer").click(function(){
    $("#place_new_answer").append(
      '<div class="panel panel-default">'+
        '<div class="panel-body">'+
          '<div class="form-group">'+
            '<label for="">Jawaban</label>'+
            '<input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]">'+
          '</div>'+
          '<div class="col-md-12">'+
            '<div class="form-group">'+
              '<label for="">Deskripsi Jawaban</label>'+
              '<input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">'+
            '</div>'+
          '</div>'+
          '<div class="form-group">'+
            '<select class="form-control" name="is_correct[]" style="width:15%">'+
              '<option value="1">Benar</option>'+
              '<option value="0">Salah</option>'+
            '</select>'+
          '</div>'+
        '</div>'+
      '</div>'
    );
  });
</script>
{{-- add new section --}}

{{-- check answer --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#btnCheckAnswer',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var content_video_quiz_answer_id = $("input[name*='content_video_quiz_answer_"+data_id+"']:checked").val()

        $.ajax({
          url: '/content-video-quiz/check-answer',
          type: 'post',
          data: {
            _token: '{{csrf_token()}}',
            content_video_quiz_answer_id: content_video_quiz_answer_id
          },
          success: function(result){
            if(result.is_correct == '1'){
              $("#alertCheckAnswer"+data_id).removeClass('alert alert-danger').addClass('alert alert-success');
              $("#alertCheckAnswer"+data_id).html(
                '<strong>Betul</strong>'+
                '<p>'+result.answer_description+'</p>'
              );
            }else{
              $("#alertCheckAnswer"+data_id).removeClass('alert alert-success').addClass('alert alert-danger');
              $("#alertCheckAnswer"+data_id).html(
                '<strong>Salah</strong>'+
                '<p>'+result.answer_description+'</p>'
              );
            }
          }
        })
    });
  });

</script>
{{-- check answer --}}
