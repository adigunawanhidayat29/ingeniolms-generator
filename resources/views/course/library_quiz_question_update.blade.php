@extends('layouts.app')

@section('title')
	{{ 'Question Edit'}}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{$button}} Question</h3>
					</div>
					<div class="box-body">

						<center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						<div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title">{{$button}} Quiz Question</h3>
						  </div>
						  <div class="panel-body">

								<h3>Update Question</h3>
								{{ Form::open(array('url' => $action, 'method' => $method)) }}

									<div class="form-group">
										<label for="name">Quiz Type</label>
										<select class="form-control" name="quiz_type_id" id="quiz_type_id" required>
											<option value="">Choose Type</option>
											@foreach($quiz_types->where('id', '<=', 7) as $quiz_type)
												<option {{ $quiz_type->id == $quiz_type_id ? 'selected' : '' }} value="{{$quiz_type->id}}">{{$quiz_type->type}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										<label for="title">Question</label>
										<textarea name="question" id="question">{{ $question }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Weight</label>
										<input type="number" name="weight" class="form-control" value="{{$weight}}">
									</div>

									<div class="form-group">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit($button . " Quiz Question" , array('class' => 'btn btn-primary', 'name' => 'button')) }}
										<a href="{{ url('instructor/library/quiz/question/manage/'.$quiz->id) }}" class="btn btn-default">Back to Question Manage</a>
									</div>
								{{ Form::close() }}

									<h3>Answers</h3>

									<div id="new_row_answer">
										@foreach($answers as $answer)
											<div class="panel panel-default">
											  <div class="panel-body">
													<div class="form-group">
														<label for="title">Answer</label>
														<input type="text" id="edit_text_answer{{$answer->id}}" value="{{$answer->answer}}" class="form-control">
													</div>

													<div class="form-group">
														<label for="title">is correct ?</label>
														<select class="form-control" id="edit_text_answer_correct{{$answer->id}}">
															<option {{$answer->answer_correct == '0' ? 'selected' : ''}} value="0">Incorrect</option>
															<option {{$answer->answer_correct == '1' ? 'selected' : ''}} value="1">Correct</option>
														</select>
													</div>

													<div class="form-group">
														<button id="save_edit_answer" data-id="{{$answer->id}}" type="button" class="btn btn-warning">Edit</button>
														<button onclick="return confirm('are you sure?')" id="delete_answer" data-id="{{$answer->id}}" class="btn btn-danger">Delete</button>
													</div>
											  </div>
											</div>
										@endforeach
										{{ Form::open(array('url' => 'library/quiz/question/answer/create', 'method' => 'post')) }}
											<div id="form_new_answer"></div>
											<div class="form-group">
												<input type="hidden" name="quiz_question_id" value="{{$id}}">
												<input id="save_new_answer" type="submit" name="submit" value="Save New Answer" class="btn btn-primary">
											</div>
										{{Form::close()}}
									</div>
									<button id="add_new_answer" type="button" class="btn">Add New Answer</button>
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('question');
	</script>
	{{-- CKEDITOR --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$("#save_new_answer").hide();
		$("#add_new_answer").click(function(){
			$("#save_new_answer").show();
			$("#form_new_answer").append(
				'<div class="panel panel-default">'+
					'<div class="panel-body">'+
						'<div class="form-group">'+
							'<label for="title">Answer</label>'+
							'<input type="text" name="answer[]" value="" class="form-control">'+
						'</div>'+

						'<div class="form-group">'+
							'<label for="title">is correct ?</label>'+
							'<select class="form-control" name="answer_correct[]">'+
								'<option value="0">Incorrect</option>'+
								'<option value="1">Correct</option>'+
							'</select>'+
						'</div>'+
					'</div>'+
				'</div>'
			);
		})
	</script>
	{{-- add new answer --}}

	{{-- delete answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#delete_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var url_delete_answer =  "{{url('library/quiz/question/answer/delete/'.$quiz->id.'/')}}";
				$.ajax({
					url : url_delete_answer + '/' + data_id,
					type : "GET",
					success : function(){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- delete answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#save_edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var token = '{{ csrf_token() }}';
				$.ajax({
					url : "{{url('library/quiz/question/answer/update_action')}}",
					type : "POST",
					data : {
						answer : $("#edit_text_answer"+data_id).val(),
						answer_correct : $("#edit_text_answer_correct"+data_id).val(),
						id : data_id,
						_token: token
					},
					success : function(result){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- edit answer --}}
@endpush
