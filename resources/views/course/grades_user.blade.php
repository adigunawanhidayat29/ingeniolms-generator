@extends('layouts.app')

@section('title', 'Course Grades')

@push('style')
  <style media="screen">
    .link-grade{
      text-decoration: underline;
    }
    .text-bold{
      font-weight: bold;
    }
  </style>
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Buku Nilai <span>{{$course_users->name}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Kelola Kelas</a></li>
          <li><a href="/course/preview/{{Request::segment(3)}}">"Nama Kelas"</a></li>
          <li><a href="/course/grades/{{Request::segment(3)}}">Daftar Nilai</a></li>
          <li>{{$course_users->name}}</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
      <div class="row">
        <div class="col-md-12">
					<a class="btn btn-raised btn-default no-shadow mt-0 mb-2" href="/course/grades/{{Request::segment(3)}}"><i class="zmdi zmdi-arrow-left"></i> Kembali</a>
          <div class="card card-groups">
            <div class="card-block">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr style="background-color: #e6e6e6;">
                      <th>Kuis / Tugas</th>
                      <th>Nilai</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $grade_total = 0 @endphp
                    @foreach($quizzes as $quiz)
                      <tr>
                      <td>{{$quiz->name}}</td>
                      @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_users->user_id, 'quiz_id' => $quiz->id])->first() @endphp
                      @if($quiz_participant)
                        @php $grade = $quiz_participant->grade @endphp
                        <td>{{$grade}}</td>
                      @else
                        @php $grade = 0 @endphp
                        <td>-</td>
                      @endif

                      @php $grade_total += $grade @endphp
                      </tr>
                    @endforeach

                    @foreach($assignments as $assignment)
                      <tr>
                      <td>{{$assignment->title}}</td>

                      @php
                        $assignments_answers = \DB::table('assignments_answers')
                          ->where(['assignments_answers.user_id' => $course_users->user_id, 'assignments_answers.assignment_id' => $assignment->id])
                          ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                          ->first()
                      @endphp
                      @if($assignments_answers)
                        @php $grade_assignment = $assignments_answers->grade @endphp
                        <td>{{$grade_assignment}}</td>
                      @else
                        @php $grade_assignment = 0 @endphp
                        <td>-</td>
                      @endif

                      @php $grade_total += $grade_assignment @endphp
                      </tr>
                    @endforeach

                    <tr>
                      <td>Completion</td>
                      @php
                        //count percentage
                        $num_progress = 0;
                        $num_progress = count(\DB::table('progresses')->where(['course_id'=>\Request::segment(3), 'user_id' => $course_users->user_id, 'status' => '1'])->get());
                        $percentage = 0;
                        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
                        $percentage = 100 / $percentage;
                        //count percentage
                      @endphp
                      <td>{{ceil($percentage)}}</td>
                    </tr>

                    <tr class="fw-600" style="background: #eee;">
                      <td>Total</td>
                      <td>{{ceil( ($grade_total + $percentage) / (count($quizzes) + count($assignments) + 1)) }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

			{{-- <div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Buku Nilai {{$course_users->name}}</h3>
			  </div>
			  <div class="panel-body" style="background:white">
					<table class="table table-bordered">
              <tr>
                <th>Kuis / Tugas</th>
                <th>Nilai</th>
              </tr>

              @php $grade_total = 0 @endphp
              @foreach($quizzes as $quiz)
                <tr>
  							<td>{{$quiz->name}}</td>
                @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_users->user_id, 'quiz_id' => $quiz->id])->first() @endphp
                @if($quiz_participant)
                  @php $grade = $quiz_participant->grade @endphp
                  <td>{{$grade}}</td>
                @else
                  @php $grade = 0 @endphp
                  <td>-</td>
                @endif

                @php $grade_total += $grade @endphp
                </tr>
              @endforeach

              @foreach($assignments as $assignment)
                <tr>
  							<td>{{$assignment->title}}</td>

                @php
                  $assignments_answers = \DB::table('assignments_answers')
                    ->where(['assignments_answers.user_id' => $course_users->user_id, 'assignments_answers.assignment_id' => $assignment->id])
                    ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                    ->first()
                @endphp
                @if($assignments_answers)
                  @php $grade_assignment = $assignments_answers->grade @endphp
                  <td>{{$grade_assignment}}</td>
                @else
                  @php $grade_assignment = 0 @endphp
                  <td>-</td>
                @endif

                @php $grade_total += $grade_assignment @endphp
                </tr>
              @endforeach

              <tr>
                <td>Completion</td>
                @php
                  //count percentage
                  $num_progress = 0;
                  $num_progress = count(\DB::table('progresses')->where(['course_id'=>\Request::segment(3), 'user_id' => $course_users->user_id, 'status' => '1'])->get());
                  $percentage = 0;
                  $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
                  $percentage = 100 / $percentage;
                  //count percentage
                @endphp
                <td>{{ceil($percentage)}}</td>
              </tr>

              <tr class="text-bold">
                <td>Total</td>
                <td>{{ceil( ($grade_total + $percentage) / (count($quizzes) + count($assignments) + 1)) }}</td>
              </tr>

					</table>
			  </div>
			</div> --}}
		</div>
	</div>
@endsection
