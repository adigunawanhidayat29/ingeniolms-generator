@extends('layouts.app')

@section('title', $course->title)

@push('meta')
<meta name="description" content="{{strip_tags($course->description)}}" />
<meta name="keywords" content="{{$course->title}}" />

<meta property="og:type" content="article"/>
<meta property="og:title" content="{{$course->title}}"/>
<meta property="og:description" content="{{strip_tags($course->description)}}"/>
<meta property="og:image" content="{{$course->image}}"/>
<meta property="og:url" content="{{url('course/' . $course->slug )}}"/>
<meta property="og:site_name" content="Ingenio"/>
<meta property="article:publisher" content="https://www.facebook.com/ingeniocoid/"/>
<meta property="og:image:width" content="640"/>
<meta property="og:image:height" content="424"/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{$course->title}}">
<meta name="twitter:description" content="{{strip_tags($course->description)}}">
<meta name="twitter:image" content="{{$course->image}}">
<meta name="twitter:site" content="@ingeniocoid">
@endpush

@push('style')
  <style media="screen">
    .card.card-info span{
      font-family: Roboto,sans-serif !important;
      font-size: 15px !important;
    }
    .fa-socmed{
      padding: 7px;
      font-size: 26px;
    }
    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
      width: 120px;
      height: 120px;
    }
    .share span{
      display: inline-flex;
    }
    .date-time span{
      display: inline-block;
      margin-bottom: 0;
      margin-right: 1rem;
    }

    @media (max-width: 768px){
      .share span{
        display: block;
      }
      .date-time span{
        display: block;
        margin-bottom: 0.5rem;
        margin-right: 0;
      }
    }
  </style>
  <style media="screen">
    .status{
      position: absolute;
      top: 0;
      right: 0;
      padding: 0.5rem 3rem;
    }
    .status span{
      font-size: 14px !important;
      font-weight: 500 !important;
    }
    .list-group{
      border: 0;
    }
    .list-group a.list-group-item{
      border: 1px solid #eee;
    }
  </style>

  <style>
    .list-group .list-group-item,
    .list-group a.list-group-item {
      display: flex;
      align-items: flex-start;
    }

    .list-group .list-group-item i,
    .list-group a.list-group-item i {
      align-self: inherit;
      line-height: inherit;
    }

    .checkbox input[type=checkbox][disabled]:checked+.checkbox-material .check{
      color: #d6d4d4;
      border-color: #d6d4d4;
      cursor: not-allowed;
    }

    .checkbox input[type=checkbox][disabled]:checked+.checkbox-material .check:before{
      color: #d6d4d4;
    }

    .checkbox .checkbox-material{
      position: inherit;
    }

    .checkbox input[type=checkbox][disabled]:not(:checked)~.checkbox-material .check{
      cursor: not-allowed;

    }

    .checkbox .checkbox-material:before{
      display: none;
    }

    .checkbox input[type=checkbox]:checked+.checkbox-material:after{
      display: none;
    }

    .list-group-item.disabled, .list-group-item.disabled:hover {
      background: #eee;
      color: #999;
      cursor: context-menu;
    }

    a.list-group-item.disabled:hover, a.list-group-item.disabled:focus{
      color: #999;
    }

    .list-group li {
      list-style: none;
      border: solid 1px #eee;
    }

    .list-group a {
      border: none !important;
      background: transparent;
    }

    .restrict ul li{
      list-style: disc;
      border: none;
    }

    .restrict{
      margin-left: 42px;
    }

    .restrict h5{
      margin-top:0px
    }

    .list-group li.disabled {
        background: #eee;
        color: #999;
        cursor: context-menu;
    }

    .list-group li.disabled a{
        cursor: not-allowed;
    }

  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.learn_course.breadcrumb_home')</a></li>
          <li><a href="/my-course">@lang('front.learn_course.breadcrumb_first')</a></li>
          <li>{{$course->title}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div style="background:#3b3b3b; padding:25px;margin-top:0px;">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <img class="img-fluid" src="{{$course->image}}" alt="{{$course->title}}">
          <div class="status">
            @if(is_liveCourse($course['id']) == true) <span class="badge badge-danger">@lang('front.learn_course.live_course')</span> @endif
            @if($course['public'] == '0') <span class="badge badge-warning">@lang('front.learn_course.private_course')</span> @endif
            @if($course['model'] == 'batch') <span class="badge badge-success">@lang('front.learn_course.schedule_course')</span> @endif
            @if($course['model'] == 'subscription') <span class="badge badge-primary">@lang('front.learn_course.lifetime_course')</span> @endif
          </div>
        </div>
        <div class="col-md-7">
          <h2 class="headline-md color-white mt-0"><span>{{$course->title}}</span></h2>
          <p class="fs-20 color-white mt-0">{{$course->subtitle}}</p>
          @if($course->setting->manual_self_completion == 0)
            @if($percentage < 100)
              @if($NextContent)
                <p style="color:white;">
                  <a href="/course/learn/content/{{$course->slug}}/{{$NextContent->id}}" class="btn btn-danger btn-raised">@lang('front.learn_course.continue_learning')</a>
                  <a class="btn-circle btn-circle-sm btn-facebook color-white" target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow"><i class="zmdi zmdi-facebook"></i></a>
                  <a class="btn-circle btn-circle-sm btn-twitter color-white" target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid"><i class="zmdi zmdi-twitter"></i></a>
                </p>
              @endif
            @else
              @if($course_user->progress == 0)
                <form action="{{url('/course/learn/progress/finish/'.$course->id)}}" method="post">
                  {{csrf_field()}}
                  <p style="color:white;">
                    <button type="submit" class="btn btn-success btn-raised">@lang('front.learn_course.finish_learning')</button>
                    <a class="btn-circle btn-circle-sm btn-facebook color-white" target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow"><i class="zmdi zmdi-facebook"></i></a>
                    <a class="btn-circle btn-circle-sm btn-twitter color-white" target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid"><i class="zmdi zmdi-twitter"></i></a>
                  </p>
                </form>
              @else
                <p style="color:white;">
                  <a href="#" class="btn btn-success btn-raised">@lang('front.learn_course.course_finished')</a>
                  <a class="btn-circle btn-circle-sm btn-facebook color-white" target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow"><i class="zmdi zmdi-facebook"></i></a>
                  <a class="btn-circle btn-circle-sm btn-twitter color-white" target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid"><i class="zmdi zmdi-twitter"></i></a>
                </p>
              @endif
            @endif
          @else
            <form action="{{url('/course/learn/progress/finish/'.$course->id)}}" method="post">
              <p style="color:white;">
                @if($percentage < 100)
                  @if($NextContent)
                  <a href="/course/learn/content/{{$course->slug}}/{{$NextContent->id}}" class="btn btn-danger btn-raised">@lang('front.learn_course.continue_learning')</a>
                  @endif
                @endif
                @if($course_user->progress == 0)
                  {{csrf_field()}}
                    <button type="submit" class="btn btn-success btn-raised">@lang('front.learn_course.finish_learning')</button>
                @else
                  <a href="#" class="btn btn-success btn-raised">@lang('front.learn_course.course_finished')</a>
                @endif
                <a class="btn-circle btn-circle-sm btn-facebook color-white" target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow"><i class="zmdi zmdi-facebook"></i></a>
                <a class="btn-circle btn-circle-sm btn-twitter color-white" target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid"><i class="zmdi zmdi-twitter"></i></a>
              </p>
            </form>
          @endif
          <p>
            <span>
              <a href="#" data-target="#modalRating" data-toggle="modal">
                @for($i=1;$i<=$rate;$i++)
                  <i class="fa fa-star color-warning"></i>
                @endfor
                @for($i=$rate;$i<5;$i++)
                  <i class="fa fa-star color-white"></i>
                @endfor
                @lang('front.learn_course.rating_button')
              </a>
            </span>
          </p>
          {{-- <p class="share pull-right">
            <a class="btn-circle btn-circle-sm btn-facebook color-white" target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow"><i class="zmdi zmdi-facebook"></i></a>
            <a class="btn-circle btn-circle-sm btn-twitter color-white" target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid"><i class="zmdi zmdi-twitter"></i></a>
            <a href="#" class="btn-circle btn-google"><i class="zmdi zmdi-google-plus"></i></a>
          </p>
          <p class="share">
            <span style="color:white">Bagikan:</span>
            <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow" class="btn btn-sm btn-raised btn-facebook"><i class="zmdi zmdi-facebook"></i> Facebook</a>
            <a target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid" class="btn btn-sm btn-raised btn-twitter"><i class="zmdi zmdi-twitter"></i> Twitter</a>
            <a href="#" class="btn-circle btn-google"><i class="zmdi zmdi-google-plus"></i></a>
          </p> --}}
          <p style="color:white">
            <strong>{{$num_progress}}</strong> @lang('front.learn_course.progress_from') <strong>{{$num_contents}}</strong> @lang('front.learn_course.progress_to') <a id="progress_reset" href="#">@lang('front.learn_course.progress_reset')</a>
            <div class="progress">
              <div class="progress-bar" role="progressbar"
                aria-valuenow="{{$percentage >= 100 ? 100 : $percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage >= 100 ? 100 : $percentage}}%">
                &nbsp;&nbsp;&nbsp;&nbsp;{{ceil($percentage >= 100 ? 100 : ($percentage <= 1 ? 0 : $percentage))}}%
              </div>
            </div>
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-flat">
          <div class="">
            <ul class="nav nav-tabs nav-tabs-full" role="tablist">
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
                  <i class="fa fa-eye"></i>
                  <span class="d-none d-sm-inline">@lang('front.learn_course.overview_tab')</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple {{is_liveCourse($course->id) == false ? 'active' : ''}}" href="#course-content" aria-controls="course-content" role="tab" data-toggle="tab">
                  <i class="fa fa-graduation-cap"></i>
                  <span class="d-none d-sm-inline">@lang('front.learn_course.content_tab')</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple {{is_liveCourse($course->id) == true ? 'active' : ''}}" href="#meeting" aria-controls="meeting" role="tab" data-toggle="tab">
                  <i class="fa fa-users"></i>
                  <span class="d-none d-sm-inline">{{is_liveCourse($course->id) == true ? Lang::get('front.learn_course.live_class_tab') : Lang::get('front.learn_course.webinar_tab')}}</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
                  <i class="fa fa-question-circle-o"></i>
                  <span class="d-none d-sm-inline">@lang('front.learn_course.discussion_tab')</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                  <i class="fa fa-bullhorn"></i>
                  <span class="d-none d-sm-inline">@lang('front.learn_course.information_tab')</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#usergrade" aria-controls="usergrade" role="tab" data-toggle="tab">
                  <i class="fa fa-info-circle"></i>
                  <span class="d-none d-sm-inline">@lang('front.learn_course.grade_tab')</span>
                </a>
              </li>
              @php
                $userid = Auth::user()->id;
              @endphp
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#badgeform" aria-controls="badgeform" role="tab" data-toggle="tab">
                  <i class="fa fa-info-circle"></i>
                  <span class="d-none d-sm-inline">@lang('front.badges.badge')</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#level" aria-controls="level" role="tab" data-toggle="tab">
                  <i class="fa fa-info-circle"></i>
                  <span class="d-none d-sm-inline">Level</span>
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link withoutripple" href="#broadcast" aria-controls="broadcast" role="tab" data-toggle="tab">
                  <i class="fa fa-signal"></i>
                  <span class="d-none d-sm-inline">Broadcast</span>
                </a>
              </li> --}}
              {{-- <li class="nav-item">
                <a class="nav-link withoutripple" href="#projects" aria-controls="projects" role="tab" data-toggle="tab">
                  <i class="fa fa-trophy"></i>
                  <span class="d-none d-sm-inline">Projects</span>
                </a>
              </li> --}}
            </ul>
          </div>
        </div>

        <div class="card-block no-pl no-pr">
          <div class="tab-content">

            <!-- TAB OVERVIEW -->
            <div role="tabpanel" class="tab-pane fade" id="overview">
              <div class="row">

                <!--RECENT ACTIVITY-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.learn_course.latest_activity')</h2>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <b>@lang('front.learn_course.latest_discussion')</b>
                      </li>
                      @foreach($discussions as $discussion)
                        <a href="/discussion/detail/{{$discussion->id}}">
                          <li class="list-group-item">
                            <span class="text-center">
                              <img src="{{avatar($discussion->photo)}}" class="dq-frame-round-xs">
                            </span>
                            <span class="col-md-11 dq-max dq-no-margin">
                              <p>
                                <b>{{$discussion->name}}</b>
                                <p>{{$discussion->body}}</p>
                              </p>
                            </span>
                          </li>
                        </a>
                      @endforeach
                      {{-- <li class="list-group-item">
                          <a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
                            Browse all questions
                          </a>
                      </li> --}}
                    </ul>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <b>@lang('front.learn_course.latest_information')</b>
                      </li>
                      @foreach($announcements as $announcement)
                        <a class="withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab" aria-selected="true">
                          <li class="list-group-item">
                            <span class="text-center">
                              <img src="{{avatar($announcement['photo'])}}" class="dq-frame-round-xs">
                            </span>
                            <span class="col-md-11 dq-max dq-no-margin">
                              <p>
                                {{$announcement['title']}}
                              </p>
                            </span>
                          </li>
                        </a>
                      @endforeach
                      {{-- <li class="list-group-item">
                          <a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                            Browse all announcements
                          </a>
                      </li> --}}
                    </ul>
                  </div>
                </div>

                <!--ABOUT COURSE-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.learn_course.about_course')</h2>
                </div>
                <div class="col-md-12">
                  <div class="card card-info">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.learn_course.course_information')</b>
                        </div>
                        <ul class="col-md-3">
                          {{-- <li class="dq-list-item">Lectures: {{count($sections)}}</li> --}}
                          <li class="dq-list-item">@lang('front.learn_course.course_category'): {{$course->category}}</li>
                          <li class="dq-list-item">@lang('front.learn_course.course_level'): {{$course->level}}</li>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.learn_course.course_description')</b>
                        </div>
                        <ul class="col-md-8">
                          {!! $course->description !!}
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.learn_course.course_goal')</b>
                        </div>
                        <ul class="col-md-8">
                          {!! $course->goal !!}
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.learn_course.course_contributor')</b>
                        </div>
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-2">
                              <a href="/user/{{$course->author_slug}}">
                                <img src="{{avatar($course->photo)}}" class="dq-frame-round-md">
                              </a>
                            </div>
                            <div class="col-md-6">
                              <a href="/user/{{$course->author_slug}}"><h3 class="color-dark">{{$course->author}}</h3></a>
                              <h5>{!! $course->author_short_description !!}</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

            <!-- TAB COURSE CONTENT -->
            <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == false ? 'active show' : ''}}" id="course-content">
              {{-- <div class="row">
                <span class="col-md-4">
                  <form class="dq-search-form">
                    <input id="inputDefault" type="text" class="dq-search-input" placeholder="Search..." name="q" />
                    <label for="inputDefault">
                      <i class="fa fa-search"></i>
                    </label>
                  </form>
                </span>
                <div class="col-md-8">
                  <div class="dq-padding right">
                    <a href="">Current Section</a>
                    <a href="">All Sections</a>
                    <a href="">All Resources</a>
                  </div>
                </div>
              </div> --}}
              <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
                <?php $i=1; ?>
                <div class="panel-group" id="accordion fade in active">
                  @foreach($sections as $section)
                    <div class="panel panel-default">
                      <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
                        <div class="panel-heading" style="background:#e9e9e9">
                          <div style="font-size: 12px; color: #333;">@lang('front.learn_course.content_topic') {{$i}}:</div>
                          <div style="font-size: 17px; font-weight: 500; color: #333;">{{ $section['title'] }}</div>
                          <div>{{ $section['description'] }}</div>
                        </div>
                      </a>
                      <div id="content{{$i}}" class="panel-collapse collapse {{$i == 1 ? 'show' : ''}}">
                        <div class="panel-body" style="background:white; padding: 0;">
                          <div class="list-group">
                            {{-- @foreach($section['section_contents'] as $content) --}}
                            @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                              @if($all->section_type == 'content')
                                @php $content = $all; @endphp

                                @php
                                  $content->restrict = $content->restrict->where('progress.status', 0);
                                  $content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                  $default_type = $content->type_content;
                                  if (strpos($default_type, '-') !== false) {
                                      $default_type = explode('-', $default_type);
                                      $default_type = $default_type[1];
                                  }
                                  $content_default_setting = $course->default_activity_completion->where('type_content', $default_type)->first();
                                  if($content->setting){
                                    $content_default_setting = $content->setting;
                                  }
                                @endphp
                                <li class="{{count($content->restrict) > 0 ? 'disabled' : ''}}">
                                  <a href="{{count($content->restrict) > 0 ? '#' : '/course/learn/content/'.$course->slug.'/'.$content->id}}" class="list-group-item list-group-item-action withripple">
                                    <i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title ? $content->title : strip_tags($content->description)}}
                                    @if($content->type_content != 'folder')
                                      <div class="checkbox ml-auto">
                                        @if($content->type_content == 'video')
                                          <span class="text-right">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</span>
                                        @endif
                                        @if($content->type_content != 'label')
                                          <label style="margin-right:0">
                                            &nbsp;<input type="checkbox" {{$content_progress ? 'checked' : ''}} class="CheckContent" type-content="content" data-id="{{$content->id}}" {{isset($content_default_setting->completion_tracking) && $content_default_setting->completion_tracking != 1 ? 'disabled' : ''}} {{count($content->restrict) > 0 ? 'disabled' : ''}}>
                                          </label>
                                        @endif
                                      </div>
                                    @endif
                                  </a>

                                  @if(count($content->restrict) > 0)
                                    <div class="restrict">
                                      <h5><b>@lang('front.learn_course.content_restrict')</b></h5>
                                      <ul>
                                        @foreach($content->restrict as $restrict)
                                          @php
                                            $restrict_item = null;
                                            if($restrict->to_content){
                                              $restrict_item = $restrict->to_content;
                                              $restrict_item->title = $restrict_item->title;
                                            }elseif($restrict->to_quizz){
                                              $restrict_item = $restrict->to_quizz;
                                              $restrict_item->title = $restrict_item->name;
                                            }elseif($restrict->to_assignment){
                                              $restrict_item = $restrict->to_assignment;
                                              $restrict_item->title = $restrict_item->title;
                                            }
                                          @endphp
                                          <li>{{$restrict_item->title}}</li>
                                        @endforeach
                                      </ul>
                                    </div>
                                  @endif
                                </li>
                              @endif

                              @if($all->section_type == 'quizz')
                                @if($all->status == 1)
                                  @php $quiz = $all; @endphp
                                  @php
                                  $quiz->restrict = $quiz->restrict->where('progress.status', 0);
                                  $content_progress = DB::table('progresses')->where(['quiz_id'=>$quiz->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                  $quiz_default_setting = $course->default_activity_completion->where('type_content', 'quizz')->first();
                                  if($quiz->setting){
                                    $quiz_default_setting = $quiz->setting;
                                  }

                                  $start = false;
                                  $end = false;
                                  $start_time = false;
                                  $end_time = false;
                                  $error = '';

                                  if($quiz->time_end_required == 1 && $quiz->time_start_required == 1){
                                    if(date("Y-m-d H:i:s") <= $quiz->time_start){
                                      $start_time = true;
                                      $error = 'not start yet';
                                    }

                                    if(date("Y-m-d H:i:s") >= $quiz->time_end){
                                      $end_time = true;
                                      $error = 'time run out';
                                    }


                                  }elseif($quiz->time_end_required == 1){
                                    if(date("Y-m-d H:i:s") >= $quiz->time_end){
                                      $end_time = true;
                                      $error = 'time run out';
                                    }

                                  }elseif($quiz->time_start_required == 1){
                                    if(date("Y-m-d H:i:s") <= $quiz->time_start){
                                      $start_time = true;
                                      $error = 'not start yet';
                                    }
                                  }

                                  @endphp
                                  <li class="{{count($quiz->restrict) > 0 ? 'disabled' : ''}}">
                                    <a class="list-group-item list-group-item-action withripple {{$end_time || $start_time ? 'disabled' : ''}}"
                                        @if(!$end_time && !$start_time)
                                        href="{{count($quiz->restrict) > 0 ? '#' : '/course/learn/quiz/'.$course->slug.'/'.$quiz->id}}"
                                        @else
                                        href="#"
                                        @endif
                                      >
                                      @if($quiz->quizz_type == 'quiz')
                                      <i class="fa fa-star"></i> {{$quiz->name}} {{$end_time || $start_time ? '('.$error.')' : ''}}
                                      @else
                                      <i class="fa fa-edit"></i> {{$quiz->name}} {{$end_time || $start_time ? '('.$error.')' : ''}}
                                      @endif
                                      @if(!$end_time && !$start_time)
                                      <div class="checkbox ml-auto">
                                        <label style="margin-right:0">
                                          &nbsp;<input type="checkbox" {{$content_progress ? 'checked' : ''}} class="CheckContent" type-content="quizz" data-id="{{$quiz->id}}" {{isset($quiz_default_setting->completion_tracking) && $quiz_default_setting->completion_tracking != 1 ? 'disabled' : ''}} {{count($quiz->restrict) > 0 ? 'disabled' : ''}}>
                                        </label>
                                      </div>
                                      @endif
                                    </a>

                                    @if(count($quiz->restrict) > 0)
                                      <div class="restrict">
                                        <h5><b>@lang('front.learn_course.content_restrict')</b></h5>
                                        <ul>
                                          @foreach($quiz->restrict as $restrict)
                                            @php
                                              $restrict_item = null;
                                              if($restrict->to_content){
                                                $restrict_item = $restrict->to_content;
                                                $restrict_item->title = $restrict_item->title;
                                              }elseif($restrict->to_quizz){
                                                $restrict_item = $restrict->to_quizz;
                                                $restrict_item->title = $restrict_item->name;
                                              }elseif($restrict->to_assignment){
                                                $restrict_item = $restrict->to_assignment;
                                                $restrict_item->title = $restrict_item->title;
                                              }
                                            @endphp
                                            <li>{{$restrict_item->title}}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                    @endif
                                  </li>
                                @endif
                              @endif

                              @if($all->section_type == 'assignment')
                                @php $assignment = $all; @endphp
                                @php
                                  $assignment->restrict = $assignment->restrict->where('progress.status', 0);
                                  $content_progress = DB::table('progresses')->where(['assignment_id'=>$assignment->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                  $assigment_default_setting = $course->default_activity_completion->where('type_content', 'assigment')->first();
                                  if($assignment->setting){
                                    $assigment_default_setting = $assignment->setting;
                                  }
                                @endphp
                                <li class="{{count($assignment->restrict) > 0 ? 'disabled' : ''}}">
                                  @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                                    <a class="list-group-item list-group-item-action withripple" href="/course/learn/assignment/{{$course->slug}}/{{$assignment->id}}">
                                      <i class="fa fa-tasks"></i>{{$assignment->title}}

                                      <div class="checkbox ml-auto">
                                        <label style="margin-right:0">
                                          &nbsp;<input type="checkbox" {{$content_progress ? 'checked' : ''}} class="CheckContent" type-content="assignment" data-id="{{$assignment->id}}" {{ isset($assigment_default_setting->completion_tracking) && $assigment_default_setting->completion_tracking != 1 ? 'disabled' : ''}} {{count($assignment->restrict) > 0 ? 'disabled' : ''}}>
                                        </label>
                                      </div>
                                    </a>
                                  @else
                                    <a class="list-group-item list-group-item-action withripple disabled" href="#">
                                      <i class="fa fa-tasks"></i>{{$assignment->title}} (time has run out)
                                    </a>
                                  @endif

                                  @if(count($assignment->restrict) > 0)
                                    <div class="restrict">
                                      <h5><b>@lang('front.learn_course.content_restrict')</b></h5>
                                      <ul>
                                        @foreach($assignment->restrict as $restrict)
                                          @php
                                            $restrict_item = null;
                                            if($restrict->to_content){
                                              $restrict_item = $restrict->to_content;
                                              $restrict_item->title = $restrict_item->title;
                                            }elseif($restrict->to_quizz){
                                              $restrict_item = $restrict->to_quizz;
                                              $restrict_item->title = $restrict_item->name;
                                            }elseif($restrict->to_assignment){
                                              $restrict_item = $restrict->to_assignment;
                                              $restrict_item->title = $restrict_item->title;
                                            }
                                          @endphp
                                          <li>{{$restrict_item->title}}</li>
                                        @endforeach
                                      </ul>
                                    </div>
                                  @endif
                              </li>
                              @endif
                            @endforeach

                          </div>
                        </div>
                      </div>
                    </div>
                    <?php $i++; ?>
                  @endforeach
                </div>
              </div>
            </div>

            <!-- TAB DISCUSSION -->
            <div role="tabpanel" class="tab-pane fade" id="QandA">
              <div class="card">
                <table class="table dq-table">
                  <tr>
                    <td class="col-md-4 dq-no-margin">
                      {{-- <input type="text" class="form-control" name="" placeholder="Search questions"> --}}
                    </td>
                    <td></td>
                    <td><a href="#" data-toggle="modal" data-target="#modalAskQuestion" class="btn btn-raised btn-lg btn-primary">@lang('front.learn_course.discussion_create_button')</a></td>
                  </tr>
                </table>
              </div>
              <div class="row">
              </div>
              <div class="card">
                <ul class="list-group">
                  @foreach($discussions as $discussion)
                    <a href="/discussion/detail/{{$discussion->id}}">
                      <li class="list-group-item card-block">
                        <span class="text-center dq-pl">
                          <img src="{{avatar($discussion->photo)}}" class="dq-frame-round-sm" alt="{{$discussion->name}}">
                        </span>
                        <span class="col-lg-10 dq-max">
                          <b>{{$discussion->name}}</b>
                          <p>{{$discussion->body}}</p>
                        </span>
                        <span class="text-center dq-pl dq-pr">
                          <b>{{(DB::table('discussions_answers')->where('discussion_id', $discussion->id))->count()}}</b>
                          <p>@lang('front.learn_course.discussion_answer')</p>
                        </span>
                      </li>
                    </a>
                  @endforeach
                </ul>
              </div>
              {{$discussions->links('pagination.default')}}
              <div class="text-center">
                {{-- <a href="" class="btn btn-raised btn-info">
                  Load More
                </a> --}}
              </div>
            </div>

            <!-- TAB ANNOUNCEMENTS -->
            <div role="tabpanel" class="tab-pane fade" id="announcements">
              @foreach($announcements as $announcement)
                <div class="card">
                  <div class="card-block-big">
                    <p>
                      <div class="row">
                        <img src="{{avatar($announcement['photo'])}}" class="dq-frame-round-sm" style="margin-left:15px">
                        <div class="col-md-8">
                          <div>
                            {{$announcement['name']}}
                          </div>
                          <div>
                            <span>{{$announcement['created_at']}}</span>
                          </div>
                        </div>
                      </div>
                    </p>
                    <h3>{{$announcement['title']}}</h3>
                    <p>
                      {!!$announcement['description']!!}
                    </p>
                    <!--FLOATING FORM-->
                    <p>
                      <form class="form-group label-floating"  method="post" action="/course/announcement/comment">
                        {{csrf_field()}}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <img src="{{avatar(Auth::user()->photo)}}" class="dq-frame-round-xs dq-ml">
                          </span>
                          <label class="control-label" for="addon2">@lang('front.learn_course.information_comment_label')</label>
                          <input type="text" id="addon2" class="form-control" name="comment">
                          <input type="hidden" name="course_announcement_id" value="{{$announcement['id']}}">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-raised btn-primary">
                              @lang('front.learn_course.information_submit_button')
                            </button>
                          </span>
                        </div>
                      </form>
                    </p>

                    <p>
                      <div class="ms-collapse no-margin" id="accordion{{$announcement['id']}}" role="tablist" aria-multiselectable="true">
                        <div class="mb-0">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{$announcement['id']}}" href="#collapse{{$announcement['id']}}" aria-expanded="false" aria-controls="collapse{{$announcement['id']}}">
                            @lang('front.learn_course.information_comment_count') ({{count($announcement['announcement_comments'])}})
                          </a>
                          <div id="collapse{{$announcement['id']}}" class="card-collapse collapse" role="tabpanel">
                            @foreach($announcement['announcement_comments'] as $announcement_comment)
                              <div class="dq-padding-comment">
                                <span>
                                  <div class="row">
                                    <img src="{{avatar($announcement_comment->photo)}}" class="dq-frame-round-xs dq-ml">
                                    <div class="col-md-8">
                                      <div>
                                        <span>
                                          {{$announcement_comment->name}}
                                        </span>
                                        .
                                        <span>{{$announcement_comment->created_at}}</span>
                                        {{-- .
                                        <span>
                                          <a href="" data-toggle="tooltip" data-placement="top" title="Report Abuse">
                                            <i class="fa fa-flag"></i>
                                          </a>
                                        </span> --}}
                                      </div>
                                      <div>
                                        <span>
                                          {{$announcement_comment->comment}}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </span>
                              </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </p>
                  </div>
                </div>
              @endforeach
            </div>

            <!-- TAB WEBINAR -->
            <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == true ? 'active show' : ''}}" id="meeting">
              <div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($meeting_schedules as $index => $meeting_schedule)
                  <div class="mb-0 card card-default">
                    <div class="card-header" role="tab" id="headingWebinar{{$index}}">
                      <h4 class="card-title">
                        <a class="collapsed dq-none-rotation" role="button" data-toggle="collapse" data-parent="#accordion" href="#webinar{{$index}}" aria-expanded="false" aria-controls="collapse{{$index}}">
                          <span class="dq-section-auto pull-right">
                            <p class="date-time color-info">
                              <span>
                                <i class="fa fa-calendar"></i> {{$meeting_schedule->date}}
                              </span>
                              <span>
                                <i class="fa fa-clock-o"></i> {{$meeting_schedule->time}}
                              </span>
                            </p>
                          </span>
                          <h2 class="headline-sm">{{$meeting_schedule->name}}</h2>
                        </a>
                      </h4>
                    </div>
                    <div id="webinar{{$index}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingWebinar{{$index}}">
                      <div class="card-block">
                        <h3 class="headline-sm fs-20 mt-0"><span>@lang('front.learn_course.webinar_description')</span></h3>
                        <p>{{$meeting_schedule->description}}</p>
                        @if($meeting_schedule->date == date("Y-m-d"))
                          <hr>
                          <a href="{{url('bigbluebutton/meeting/join/'. $course->id . '/' . $meeting_schedule->id)}}" class="btn btn-raised btn-success">@lang('front.learn_course.webinar_start_button')</a>
                        @endif
                      </div>
                    </div>
                    @if($meeting_schedule->date == date("Y-m-d"))
                      {{-- @if($meeting->publish === '1')
                        <a href="{{url('bigbluebutton/meeting/join/'. $course->id)}}" class="btn btn-primary btn-raised">Mulai Sekarang</a>
                      @else
                        <p>Webinar belum mulai</p>
                      @endif --}}
                      <a href="{{url('bigbluebutton/meeting/join/'. $course->id . '/' . $meeting_schedule->id)}}" class="btn btn-primary btn-raised">@lang('front.learn_course.webinar_start_button')</a>
                    @endif
                  </div>
                @endforeach
              </div>
            </div>

            <!-- TAB BROADCAST -->
            {{-- <div role="tabpanel" class="tab-pane fade" id="broadcast">
              <div class="row">
                <div class="col-md-12">
                  @if($Broadcast)
                    <a href="{{url('course/streaming/'. $Broadcast->id . '/' . str_slug($Broadcast->title))}}" target="_blank" class="btn btn-danger btn-raised">Streaming Now</a>
                  @else
                    <p>Broadcast not ready</p>
                  @endif
                </div>
              </div>
            </div> --}}

            {{-- <div role="tabpanel" class="tab-pane fade" id="projects">
              <div class="card">
                <div class="card-block-big">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Projects</h3>
                    </div>
                    <div class="panel-body">
                      <table class="table">
                        <tr>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Option</th>
                        </tr>
                        @foreach($projects as $project)
                          @php
                            $project_user = DB::table('course_project_users')->where('course_project_id', $project->id)->first();
                          @endphp
                          <tr>
                            <td>{{$project->title}}</td>
                            <td>{!!$project->description!!}</td>
                            <td>
                              @if(!$project_user)
                                <a href="/course/project-user/{{$project->id}}" class="btn btn-default btn-raised">Submit Project</a>
                              @else
                                <a href="/course/project-user/show/{{$project_user->slug}}" class="btn btn-default btn-raised">Show Project</a>
                                <a href="/course/project-user/edit/{{$project->id . "/" .$project_user->id}}" class="btn btn-warning btn-raised">Update Project</a>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- TAB USER GRADE / NILAI -->
            <div role="tabpanel" class="tab-pane fade" id="usergrade">
              @include('course.includes.nilai')
            </div>

            <!-- TAB BADGE FORM-->
            <div role="tabpanel" class="tab-pane fade" id="badgeform">

              @php
                $teacher = isTeacher(Auth::user()->id);
              @endphp

              @if($teacher == true)
                <a href="/badge/create" class="btn btn-primary">@lang('front.badges.create_badge')</a>
              @endif
              <a href="/{{$course->id}}/list_badges_user" class="btn btn-primary">@lang('front.badges.list_of_badges')</a>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="level">
              <div class="container">
                <div class="card" style="width:400px">
                  <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course->levelup_img)}}" alt="Card image">
                  <div class="card-body">
                    <h4 class="card-title text-center">{{$course_user->level_point}}xp</h4>

                    @php
                      $get_current_level = get_current_level($course->id, $course_user->level);
                      $get_next_level = get_next_level($course->id, $course_user->level);

                      if($get_current_level != null && $get_next_level != null)
                      {


                        $current_point_level = ($course_user->level_point - $get_current_level->points_required);

                        if($get_current_level->level == $get_next_level->level)
                        {
                            $points_togo = 0;

                            $points_difference = 0;

                            $exp_percentage = 100;
                        }

                        else

                        {
                            $points_difference = $get_next_level->points_required - $get_current_level->points_required;

                            $exp_percentage = number_format($current_point_level / $points_difference * 100, 2);

                            $points_togo = ($get_next_level->points_required - $course_user->level_point);
                        }
                      }

                      else
                      {
                        $points_togo = 0;
                        $exp_percentage = 0;
                      }

                    @endphp

                      <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="{{$exp_percentage}}"
                          aria-valuemin="0" aria-valuemax="100" style="width:{{$exp_percentage}}%">
                              {{$exp_percentage}}%
                          </div>
                      </div>
                      <p>{{$points_togo}}xp to go</p>
                    <a href="/course/{{$course->id}}/level_up_ladder" class="btn btn-primary">@lang('front.level_up.info')</a>
                    <a href="/course/{{$course->id}}/level_up_ladder" class="btn btn-primary">@lang('front.level_up.ladder')</a>
                  </div>
                </div>

                <div class="card" style="width:400px">
                  <table id="table-log" class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>@lang('front.level_up.event_time')</th>
                            <th>@lang('front.level_up.name')</th>
                            <th>@lang('front.level_up.reward')</th>
                            <th>@lang('front.level_up.event_name')</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users_leveling_logs as $log_data)
                        <tr>
                            <td>{{$log_data->log_created_at}}</td>
                            <td>{{$log_data->user_name}}</td>
                            <td>{{$log_data->log_reward}}</td>
                            
                            @php
                                $contents = get_contents_rule($log_data->leveling_rules_id);
                            @endphp

                            <td>
                                <ul>
                                    @foreach($contents as $content)
                                        <li>{{$content->content_name}}</li>

                                        @php
                                            $criterias = get_rules_criteria($content->rules_criteria_id);
                                        @endphp

                                        <p>-{{$criterias->name}}</p>

                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modalAskQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form action="{{url('discussion/ask')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="course_id" value="{{$course->id}}">
        <div class="modal-dialog animated zoomIn animated-3x modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-primary" id="myModalLabel">@lang('front.learn_course.discussion_create_header')</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label">@lang('front.learn_course.discussion_question_label')</label>
                <input name="body" placeholder="@lang('front.learn_course.discussion_question_placeholder')" class="form-control">
              </div>
              <div class="form-group mt-0">
                <label class="control-label">@lang('front.learn_course.discussion_description_label')</label>
                <textarea name="deskripsi" id="answer" cols="30" rows="1" class="form-control"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.learn_course.discussion_close_button')</button>
              <input type="submit" id="ask_discussion" class="btn btn-primary btn-raised" name="submit" value="@lang('front.learn_course.discussion_submit_button')">
            </div>
          </div>
        </div>
      </form>
    </div>

    <div class="modal" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog animated zoomIn animated-3x" role="document">
            <div class="modal-content">
              <form class="" action="/course/rating/{{$course->id}}" method="post">
                {{csrf_field()}}
                <div class="modal-header">
                    <h3 class="modal-title color-primary" id="myModalLabel">@lang('front.learn_course.rating_header')</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                </div>
                <div class="modal-body">
                  <div class="text-center">
                    <i value="1" class="fa fa-star color-default fa-4x rating"></i>
                    <i value="2" class="fa fa-star color-default fa-4x rating"></i>
                    <i value="3" class="fa fa-star color-default fa-4x rating"></i>
                    <i value="4" class="fa fa-star color-default fa-4x rating"></i>
                    <i value="5" class="fa fa-star color-default fa-4x rating"></i>
                    <br>
                    <span>@lang('front.learn_course.rating_text') <b id="ratingValueText">0</b> @lang('front.learn_course.rating_grade_text')</span>
                    <input type="hidden" name="rating" id="ratingValue">
                  </div>
                  <div class="form-group">
                    {{-- <label for="">Your Comment</label> --}}
                    <textarea placeholder="@lang('front.learn_course.rating_review_label')" name="comment" required class="form-control" rows="3"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.learn_course.rating_close_button')</button>
                    <input type="submit" class="btn btn-primary btn-raised" value="@lang('front.learn_course.rating_submit_button')">
                </div>
              </form>
            </div>
        </div>
    </div>

    @if(Session::has('error'))
      <div class="modal" id="modalRatingError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog animated zoomIn animated-3x" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title color-primary" id="myModalLabel"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                </div>
                <div class="modal-body">
                  <div class="alert alert-danger">
                    {{Session::get('error')}}
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.learn_course.course_close_button')</button>
                </div>
              </div>
          </div>
      </div>
    @endif
  </div>
@endsection

@push('script')

  </script>
  {{-- CKEDITOR --}}
  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>

  <script type="text/javascript">
    $("#progress_reset").click(function(){
      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('progress/reset')}}",
        type : "POST",
        data : {
          course_id : {{$course->id}},
          _token: token
        },
        success : function(result){
          location.reload();
        },
      });
    })
  </script>

  <script type="text/javascript">
    $(function(){
      $(document).on('change','.CheckContent',function(e){
        e.preventDefault();
          var content_id = $(this).attr('data-id');
          var content_type = $(this).attr('type-content');
          if (content_type == undefined){
            content_type = 'content';
          }
          if($(this).is(':checked') === true){
            $.ajax({
							url : '/course/learn-content/finish',
							type : 'POST',
							data : {
                'content_id' : content_id,
								'type' : content_type,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }else{
            $.ajax({
							url : '/course/learn-content/unfinish',
							type : 'POST',
							data : {
								'content_id' : content_id,
                'type' : content_type,
                'course_id' : '{{$course->id}}',
								'_token' : '{{csrf_token()}}'
							},
							success : function(result){
							}
						});
          }
      });
    });
  </script>

    <script type="text/javascript">

    $(".rating").hover(function() {
        $(this).addClass("color-warning");
        $("#ratingValueText").html($(this).attr('value'))
    }, function() {
        $(this).removeClass("color-warning");
    });
    $(".rating").click(function() {
        $(this).addClass("color-warning");
        $("#ratingValue").val($(this).attr('value'))
        $("#ratingValueText").html($(this).attr('value'))
    });

    @if(Session::has('error'))
      $("#modalRatingError").modal('show');
    @endif
  </script>

  <script type="text/javascript">
    // var url = window.location.href;
    // var activeTab = url.substring(url.indexOf("#") + 1);
    // $(".tab-pane").removeClass("active in");
    // $("#" + activeTab).addClass("active in");
  </script>
@endpush
