@extends('layouts.app')

@section('title', 'Katalog Kelas')

@push('style')
	<style>
		.card-list {
			position: relative;
			display: inline-flex;
			width: 100%;
			background: #f9f9f9;
			margin-bottom: 1rem;
			transition: all 0.2s;
		}

		.card-list:hover {
			box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.05);
		}

		.card-list img {
			width: 250px;
			height: 150px; /* Default 170px */
			object-fit: cover;
		}

		.card-list .card-block {
			width: 100%;
			position: relative;
			border: 1px solid #f5f5f5;
			padding: 1rem 2rem;
		}

		.card-list .card-title {
			font-size: 20px;
			font-weight: 600;
			color: #333;
			margin-top: 0rem;
			margin-bottom: 0.5rem;
		}

		.card-list .card-list-info.mobile {
			display: none;
		}

		.card-list .card-list-info.desktop {
			display: inline-flex;
		}

		.card-list .card-list-info {
			font-size: 14px;
			color: #424242;
		}

		.card-list .card-list-info p {
			margin-bottom: 0rem;
		}

		.card-list .card-list-info .rating i {
			color: #FFB800;
		}

		.card-list .card-list-info .category i {
			color: #079992;
		}

		.card-list .card-list-author {
			font-size: 14px;
			color: #424242;
			margin-bottom: 0rem;
		}

		.card-list .card-list-author i {
			color: #eb2f06;
		}

		.card-list .card-list-price {
			font-size: 14px;
			color: #424242;
		}

		.card-list .card-list-price span {
			font-size: 16px;
			font-weight: 600;
		}

		.card-list .card-list-enroll-button {
			font-size: 14px;
			font-weight: 600;
			border: 0px;
			border-radius: 5px;
			padding: 0.5rem 2rem;
			transition: all 0.2s;
			margin-left: 1rem;
		}

		.card-list .card-list-enroll-button.primary {
			background: #ff6347;
			color: #fff;
		}

		.card-list .card-list-enroll-button.secondary {
			background: #e6ca18;
			color: #333;
		}

		.card-list .card-list-enroll-button:hover {
			box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.05);
		}

		.category-filter {
			padding: 0; 
			margin: 0;
		}

		.category-filter li {
			list-style: none;
		}

		.category-filter li a {
			display: flex;
			align-items: center;
			width: 100%;
		}
		
		.category-filter li a i.zmdi.zmdi-chevron-down {
			margin-left: auto;
			margin-right: 0;
		}

		.category-filter li ul.category-first-level {
			display: none;
			padding: 0;
		}

		.category-filter li ul.category-first-level.active {
			display: block;
		}
		
		.category-filter li ul.category-first-level li {
			list-style: none;
		}
		
		.category-filter li ul.category-first-level li a {
			display: flex;
			align-items: center;
			width: 100%;
		}

		.category-filter li ul.category-first-level li ul.category-second-level {
			display: none;
			padding-left: 1.5rem;
		}

		.category-filter li ul.category-first-level li ul.category-second-level.active {
			display: block;
		}

		.category-filter li ul.category-first-level li ul.category-second-level li {
			list-style: none;
		}

		.category-filter li ul.category-first-level li ul.category-second-level li a {
			display: block;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Katalog <span>Kelas</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>Katalog Kelas</li>
        </ul>
      </div>
    </div>
	</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row" style="
				position: sticky;
				top: 0;
			">
        <div id="mobile" class="col-md-3" style="
					position: sticky;
					top: 0;
					margin-top: 0;
				">
          <div class="row">
            <div class="col-md-12">
              <h3 class="headline-sm headline-line mt-0">Filter <span>Kategori</span></h3>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
							{{-- <a href="#">Kategori Dropdown 1</a> --}}
							<ul class="category-filter">
								<li>
									<a href="#">Kategori 1</a>
								</li>
								<li>
									<a href="#">Kategori 2 <i class="zmdi zmdi-chevron-down"></i></a>
									<ul class="category-first-level">
										<li>
											<a href="#"><i class="zmdi zmdi-chevron-right" style="margin-right: 1rem;"></i> Sub Kategori 1</a>
										</li>
										<li>
											<a href="#"><i class="zmdi zmdi-chevron-right" style="margin-right: 1rem;"></i> Sub Kategori 2 <i class="zmdi zmdi-chevron-down"></i></a>
											<ul class="category-second-level">
												<li>
													<a href="#"><i class="zmdi zmdi-chevron-right" style="margin-right: 1rem;"></i> Sub Sub Kategori 1</a>
												</li>
												<li>
													<a href="#"><i class="zmdi zmdi-chevron-right" style="margin-right: 1rem;"></i> Sub Sub Kategori 2</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="#"><i class="zmdi zmdi-chevron-right" style="margin-right: 1rem;"></i> Sub Kategori 3</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">Kategori 3</a>
								</li>
							</ul>
						</div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-12">
							<div class="card-list">
								<a href="#">
									<div style="position: absolute; line-height: 1; top: 1rem; left: 1rem;">
										<span class="badge badge-success">E-Learning</span>															
										<span class="badge badge-danger ">Private</span>
									</div>
									<img src="/img/default.png" alt="DL-CPNS24 : Pelatihan Dasar CPNS Kemendikbud Angkatan 24 Tahun 2020">
								</a>
								<div class="card-block" style="width: 70%;">
									<a href="#">
										<h3 class="card-title text-truncate">DL-CPNS24 : Pelatihan Dasar CPNS Kemendikbud Angkatan 24 Tahun 2020</h3>
															
										<div class="card-list-info desktop" style="width: 100%;">
											<p class="rating">
												<i class="fa fa-star color-warning"></i> 5.0 (10 Penilaian)
											</p>
											&nbsp;&nbsp;|&nbsp;&nbsp;
											<p class="category text-truncate" style="width: 60.5%;">
												<i class="fa fa-tag"></i> Kategori: <b>Desain</b>
											</p>
										</div>
									
										<div class="card-list-info mobile" style="width: 100%;">
											<p class="rating">
												<i class="fa fa-star color-warning"></i> 5.0 (10 Penilaian)
											</p>
										</div>
									
										<p class="card-list-author text-truncate" style="width: 100%;">
											<i class="fa fa-user"></i> Administrator
										</p>
									</a>
									<div style="display: flex; align-items: center; justify-content: flex-end;">
										<a href="#">
											<div class="card-list-price"><span>Free</span></div>												
										</a>
										<a href="#" class="card-list-enroll-button secondary" data-toggle="modal" data-target="#infoLogin">Enroll</a>
									</div>
								</div>
							</div>
							<div class="card-list">
								<a href="#">
									<div style="position: absolute; line-height: 1; top: 1rem; left: 1rem;">
										<span class="badge badge-success">E-Learning</span>															
										<span class="badge badge-danger ">Private</span>
									</div>
									<img src="/img/default.png" alt="Course Mahal">
								</a>
								<div class="card-block" style="width: 70%;">
									<a href="#">
										<h3 class="card-title text-truncate">Course Mahal</h3>
															
										<div class="card-list-info desktop" style="width: 100%;">
											<p class="rating">
												<i class="fa fa-star color-warning"></i> 5.0 (10 Penilaian)
											</p>
											&nbsp;&nbsp;|&nbsp;&nbsp;
											<p class="category text-truncate" style="width: 60.5%;">
												<i class="fa fa-tag"></i> Kategori: <b>Desain</b>
											</p>
										</div>
									
										<div class="card-list-info mobile" style="width: 100%;">
											<p class="rating">
												<i class="fa fa-star color-warning"></i> 5.0 (10 Penilaian)
											</p>
										</div>
									
										<p class="card-list-author text-truncate" style="width: 100%;">
											<i class="fa fa-user"></i> Administrator
										</p>
									</a>
									<div style="display: flex; align-items: center; justify-content: flex-end;">
										<a href="#">
											<div class="card-list-price"><span>Rp 250.000</span></div>												
										</a>
										<a href="#" class="card-list-enroll-button secondary" data-toggle="modal" data-target="#infoLogin">Enroll</a>
									</div>
								</div>
							</div>
            </div>
					</div>
        </div>
      </div>

      <!--Pagination Wrap Start-->
      <div class="text-center">
        <nav aria-label="Page navigation"> <ul class="pagination pagination-plain justify-content-center"> <li class="page-item  disabled"> <a class="page-link" href="http://127.0.0.1:6060/courses?page=1" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>  <li class="page-item  active"><a class="page-link" href="http://127.0.0.1:6060/courses?page=1">1</a></li>  <li class="page-item "><a class="page-link" href="http://127.0.0.1:6060/courses?page=2">2</a></li>  <li class="page-item "><a class="page-link" href="http://127.0.0.1:6060/courses?page=3">3</a></li>  <li class="page-item "><a class="page-link" href="http://127.0.0.1:6060/courses?page=4">4</a></li>  <li class="page-item "><a class="page-link" href="http://127.0.0.1:6060/courses?page=5">5</a></li>  <li class="page-item "><a class="page-link" href="http://127.0.0.1:6060/courses?page=6">6</a></li>  <li class="page-item "> <a class="page-link " href="http://127.0.0.1:6060/courses?page=2" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li> </ul> </nav> 
      </div>
      <!--Pagination Wrap End-->
    </div>
  </div>
@endsection

@push('script')
	<script>
		$(document).ready(function(){
			$('.category-filter a').on("click", function(e){
				$(this).next('.category-first-level').toggleClass('active');
				e.stopPropagation();
				e.preventDefault();
			});
			$('.category-first-level a').on("click", function(e){
				$(this).next('.category-second-level').toggleClass('active');
				e.stopPropagation();
				e.preventDefault();
			});
		});
	</script>
@endpush