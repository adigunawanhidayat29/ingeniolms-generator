@extends('layouts.app')

@section('title')
	{{ 'Tambah Pertanyaan ' . $quiz->name }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-body">

						<center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						<div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title">Manage Pertanyaan - {{$quiz->title}}</h3>
						  </div>
						  <div class="panel-body">

								{{-- <h3>Add Question</h3> --}}
								{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

									<div class="form-group">
										<label for="name">Tipe Kuis</label>
										<select class="form-control selectpicker" name="quiz_type_id" id="quiz_type_id" required>
											<option value="">Pilih Tipe Kuis</option>
											@foreach($quiz_types->where('id', '<=', 7) as $quiz_type)
												<option {{ $quiz_type->id == $quiz_type_id ? 'selected' : '' }} value="{{$quiz_type->id}}">{{$quiz_type->type}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										<label for="title">Pertanyaan</label>
										<textarea name="question" id="question">{{ $question }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Bobot</label>
										<input type="number" placeholder="weiht" class="form-control" name="weight" value="{{$weight}}">
									</div>

									<h3>Tambah Jawaban</h3>

									<div id="new_row_answer">
										<div class="panel panel-default">
										  <div class="panel-body">
												<div class="form-group">
													<label for="title">Jawaban</label>
													<input type="text" placeholder="Jawaban" name="answer[]" value="" class="form-control">
												</div>

												<div class="form-group">
													<label for="title">Status Jawaban?</label>
													<select class="form-control selectpicker" name="answer_correct[]">
														<option value="0">Salah</option>
														<option value="1">Benar</option>
													</select>
												</div>
										  </div>
										</div>

									</div>
									<button id="add_new_answer" type="button" class="btn btn-raised">Tambah Jawaban Lain</button>
									<br><br>

									<div class="form-group">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('instructor/library') }}" class="btn btn-default btn-raised">Batal</a>
									</div>
								{{ Form::close() }}
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">Edit Answer</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Answer</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">is correct ?</label>
						<select class="form-control" id="edit_text_answer_correct">
							<option value="0">Incorrect</option>
							<option value="1">Correct</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="save_edit_answer">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('question');
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript">
		$("#quiz_type_id").select2();
	</script>
	{{-- SELECT2 --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$("#add_new_answer").click(function(){
			$("#new_row_answer").append(
				'<div class="panel panel-default">'+
					'<div class="panel-body">'+
						'<div class="form-group">'+
							'<label for="title">Jawaban</label>'+
							'<input type="text" placeholder="Jawaban" name="answer[]" value="" class="form-control">'+
						'</div>'+

						'<div class="form-group">'+
							'<label for="title">Status Jawaban ?</label>'+
							'<select class="form-control" name="answer_correct[]">'+
								'<option value="0">Salah</option>'+
								'<option value="1">Benar</option>'+
							'</select>'+
						'</div>'+
					'</div>'+
				'</div>'
			);
		})
	</script>
	{{-- add new answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}
@endpush
