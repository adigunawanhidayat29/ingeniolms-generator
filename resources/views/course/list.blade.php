@extends('layouts.app')

@section('title', Lang::get('front.page_my_courses.title'))

@push('style')
<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">

<style>
	.progress.progress-custom {
		height: 5px;
	}

	.progress-info {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
	}

	.progress-info .progress-percentage {
		font-size: 12px;
		color: #424242;
		line-height: 1;
	}

	.progress-info .progress-status {
		display: inline-block;
		text-align: center;
		color: #03a9f4;
		border: 2px solid #03a9f4;
		padding: 0.5rem 1rem;
	}

	.form-group {
		margin-top: 0;
		margin-left: 2rem;
	}

	.form-group .btn-group {
		margin: 0;
	}

	#role-user {
		width: 25%;
	}

	#class-card {
		height: 275px;
	}

	@media (max-width: 767px) {
		.form-group {
			margin-left: 0;
		}

		#role-user {
			width: 50%;
			margin-left: 1rem;
		}

		#class-card {
			height: auto;
		}
	}
</style>
@endpush

@section('content')
<div class="bg-page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="headline-md no-m">@lang('front.page_my_courses.header_dashboard')
					<span>@lang('front.page_my_courses.header_courses')</span></h2>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li><a href="/">@lang('front.page_my_courses.breadcumbs_home')</a></li>
				<li>@lang('front.page_my_courses.breadcumbs_my_courses')</li>
			</ul>
		</div>
	</div>
</div>

<div class="wrap pt-2 pb-2 mb-2 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="card-md radius-0">
					<div class="card-block">
						<div style="display: flex; align-items: center; width: 100%;">

							{{-- BUTTON TAB SEMUA KELAS --}}
							<div class="btn-group" id="allCourses">
								<button type="button" class="btn btn-sm btn-raised btn-primary dropdown-toggle" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									@lang('front.page_my_courses.action_create_courses') <i class="zmdi zmdi-chevron-down right"></i>
								</button>
								<ul class="dropdown-menu">
									@if (isCreator(Auth::user()->id))
									<li><a class="dropdown-item"
											href="{{url('course/create')}}">@lang('front.page_my_courses.action_create_new_courses')</a></li>
									@endif
									<li><a class="dropdown-item" href="/courses">@lang('front.page_my_courses.action_courses_catalog')</a>
									</li>
									<li><a class="dropdown-item" href="#" data-toggle="modal"
											data-target="#modalPrivateCourse">@lang('front.page_my_courses.action_use_courses_password')</a>
									</li>
								</ul>
							</div>

							@if (isCreator(Auth::user()->id))
							{{-- BUTTON TAB PENGAJAR --}}
							<a id="teacherCourses" class="btn btn-sm btn-primary btn-raised" href="{{url('course/create')}}">
								@lang('front.page_my_courses.action_create_new_courses')
							</a>
							@endif

							{{-- BUTTON TAB PELAJAR --}}
							<a class="btn btn-sm btn-primary btn-raised studentCourses" href="/courses">
								@lang('front.page_my_courses.action_courses_catalog')
							</a>
							<a class="btn btn-sm btn-primary btn-raised studentCourses" href="#" data-toggle="modal"
								data-target="#modalPrivateCourse">
								@lang('front.page_my_courses.action_use_courses_password')
							</a>

							{{-- BUTTON TAB PELAJAR --}}
							<a class="btn btn-sm btn-primary btn-raised studentPrograms" href="/programs">
								@lang('front.page_my_courses.action_program_catalog')
							</a>
							{{-- <a class="btn btn-sm btn-primary btn-raised studentPrograms" href="#" data-toggle="modal" data-target="#modalPrivateProgram">
									Gunakan Kode Program
								</a> --}}

							<div id="role-user" class="form-group">
								<select class="form-control selectpicker filterCourses" data-dropup-auto="false">
									<option value="all">@lang('front.page_my_courses.filter_all')</option>
									<option value="student">@lang('front.page_my_courses.filter_as_student')</option>
									<option value="teacher">@lang('front.page_my_courses.filter_as_instructor')</option>
								</select>
							</div>

							<form class="d-none d-sm-flex" action="" method="get" style="align-items: center; width: 100%;">
								<div class="form-group" style="width: 100%;">
									<input type="text" name="q" class="form-control m-0"
										placeholder="{{Lang::get('front.page_my_courses.search_courses_placeholder')}}">
									{!!Request::get('q') ? '<a
										href="/course/dashboard">'.Lang::get('front.page_my_courses.reset_search_courses_placeholder').'</a>'
									: ''!!}
								</div>
								<button type="submit" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary"
									style="width: 43px; margin-left: 1rem;"><i class="zmdi zmdi-search"></i></button>
							</form>
						</div>

						<form class="d-flex d-sm-none" action="" method="get" style="align-items: center; width: 100%;">
							<div class="form-group" style="width: 100%;">
								<input value="{{Request::get('q')}}" type="text" name="q" class="form-control m-0"
									placeholder="{{Lang::get('front.page_my_courses.search_courses_placeholder')}}">
								{!!Request::get('q') ? '<a
									href="/course/dashboard">'.Lang::get('front.page_my_courses.reset_search_courses_placeholder').'</a>'
								: ''!!}
							</div>
							<button type="submit" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary"
								style="width: 45px; margin-left: 1rem;"><i class="zmdi zmdi-search"></i></button>
						</form>
					</div>
				</div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs mb-2" role="tablist">
					<li class="nav-item">
						<a class="nav-link withoutripple active" href="#course" aria-controls="course" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-book"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_courses')</span>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link withoutripple" href="#program" aria-controls="program" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-collection-bookmark"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_program')</span>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link withoutripple" href="#archive" aria-controls="archive" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-archive"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_archive')</span>
						</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<!-- TAB SEMUA KELAS -->
					<div role="tabpanel" class="tab-pane active show " id="course">

						<div id="divAllCourses">
							<div class="row">
								@foreach($all_courses as $course)
								<div class="col-md-3 mb-2 {{$course->id_author == \Auth::user()->id ? 'card-teacher' : 'card-student'}}">
									<div id="class-card" class="card-sm">
	
										@if($course->id_author == \Auth::user()->id)
											<div class="btn-opsi">
												<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-ellipsis-v"></i>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li>
														<a class="dropdown-item" href="/course/preview/{{ $course->id }}">
															<i class="fa fa-cog"></i> @lang('front.page_my_courses.manage_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan menduplikasi kelas ini?')"
															href="/course/duplicate/{{ $course->id }}">
															<i class="fa fa-clone"></i> @lang('front.page_my_courses.duplicate_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan mengarsipkan kelas ini?')"
															href="/course/archive/{{ $course->id }}">
															<i class="fa fa-archive"></i> @lang('front.page_my_courses.archive_course_teacher_option')
														</a>
													</li>
												</ul>
											</div>
										@endif
	
										<a
											href="{{ $course->id_author == \Auth::user()->id ? url('/course/preview/'.$course->id) : url('course/learn/'. $course->slug)}}">
											<img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
										</a>
	
										@if($course->id_author == \Auth::user()->id)
	
										<div class="card-block text-left">
											<a href="/course/preview/{{ $course->id }}">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
											</a>
	
											@php $status_checked = $course->status == '1' ? 'checked' : ''; @endphp
											<div class="form-group m-0">
												<div class="togglebutton">
													<label class="control-label m-0" title="Terbitkan atau simpan sbg draf">
														<input type="checkbox" {{$status_checked}} name="status{{$course->id}}">
													</label>
												</div>
											</div>
	
											<div class="section-body-xs no-m">
												<p class="no-m fs-12">
													<span><i class="zmdi zmdi-info"></i> @lang('front.page_my_courses.courses_status')
														{{$status_checked == 'checked' ? Lang::get('front.page_my_courses.courses_status_active') : Lang::get('front.page_my_courses.courses_status_nonactive')}}</span>
													&nbsp;|&nbsp;
													<span><i class="zmdi zmdi-accounts"></i> {{course_users($course->id)}}
														@lang('front.page_my_courses.courses_student')</span>
												</p>
											</div>
	
										</div>
										@else
	
										@php
										$num_contents = count($course->contents);
										// echo $num_contents;
										//count percentage
										$num_progress = 0;
										$num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' =>
										Auth::user()->id, 'status' => '1'])->get());
										$percentage = 0;
										$percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
										$percentage = $percentage == 0 ? 1 : $percentage ;
										$percentage = 100 / $percentage;
										//count percentage
										@endphp
										<a href="{{ url('course/learn/'. $course->slug)}}">
											<div class="card-block text-left">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
												<p class="fs-12 text-truncate color-dark" style="line-height: 1;">{{$course->author}}</p>
												<div class="m-0">
													<div class="progress progress-custom mb-1">
														<div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}"
															role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100"
															style="width: {{$percentage}}%;"></div>
													</div>
													<div class="progress-info">
														<div class="progress-percentage">
															{{ceil($percentage >= 100 ? 100 : $percentage)}}%
															@lang('front.page_my_courses.courses_percentage_finish')
														</div>
													</div>
												</div>
											</div>
										</a>
										@endif
	
										@if($course->id_author == \Auth::user()->id)
										<div class="code-class">
											<div class="no-m fs-12">
												<i class="fa fa-lock"></i> @lang('front.page_my_courses.courses_password_text') : <p class="m-0">
													{{$course->password}}</p>
												<button class="btn btn-primary btn-raised fs-10 btn-copy"
													data-clipboard-text="{{$course->password}}"
													style="padding: 5px 10px">@lang('front.page_my_courses.courses_copy_code_text')</button>
												<button class="btn btn-primary btn-raised fs-10" type="button" style="padding: 5px 10px"
													data-toggle="modal"
													data-target="#shareCode">@lang('front.page_my_courses.courses_share_code_text')</button>
											</div>
										</div>
										@endif
	
									</div>
								</div>
								@endforeach
							</div>
	
							<div class="text-center">
								{{$all_courses->links('pagination.default')}}
							</div>
						</div>

						{{-- <div id="divTeacherCourses">
							<div class="row">
								@foreach($teacher_courses as $course)
								<div class="col-md-3 mb-2">
									<div id="class-card" class="card-sm">
	
										@if($course->id_author == \Auth::user()->id)
											<div class="btn-opsi">
												<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-ellipsis-v"></i>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li>
														<a class="dropdown-item" href="/course/preview/{{ $course->id }}">
															<i class="fa fa-cog"></i> @lang('front.page_my_courses.manage_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan menduplikasi kelas ini?')"
															href="/course/duplicate/{{ $course->id }}">
															<i class="fa fa-clone"></i> @lang('front.page_my_courses.duplicate_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan mengarsipkan kelas ini?')"
															href="/course/archive/{{ $course->id }}">
															<i class="fa fa-archive"></i> @lang('front.page_my_courses.archive_course_teacher_option')
														</a>
													</li>
												</ul>
											</div>
										@endif
	
										<a
											href="{{ $course->id_author == \Auth::user()->id ? url('/course/preview/'.$course->id) : url('course/learn/'. $course->slug)}}">
											<img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
										</a>
	
										@if($course->id_author == \Auth::user()->id)
	
										<div class="card-block text-left">
											<a href="/course/preview/{{ $course->id }}">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
											</a>
	
											@php $status_checked = $course->status == '1' ? 'checked' : ''; @endphp
											<div class="form-group m-0">
												<div class="togglebutton">
													<label class="control-label m-0" title="Terbitkan atau simpan sbg draf">
														<input type="checkbox" {{$status_checked}} name="status{{$course->id}}">
													</label>
												</div>
											</div>
	
											<div class="section-body-xs no-m">
												<p class="no-m fs-12">
													<span><i class="zmdi zmdi-info"></i> @lang('front.page_my_courses.courses_status')
														{{$status_checked == 'checked' ? Lang::get('front.page_my_courses.courses_status_active') : Lang::get('front.page_my_courses.courses_status_nonactive')}}</span>
													&nbsp;|&nbsp;
													<span><i class="zmdi zmdi-accounts"></i> {{course_users($course->id)}}
														@lang('front.page_my_courses.courses_student')</span>
												</p>
											</div>
	
										</div>
										@else
	
										@php
										$num_contents = count($course->contents);
										// echo $num_contents;
										//count percentage
										$num_progress = 0;
										$num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' =>
										Auth::user()->id, 'status' => '1'])->get());
										$percentage = 0;
										$percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
										$percentage = $percentage == 0 ? 1 : $percentage ;
										$percentage = 100 / $percentage;
										//count percentage
										@endphp
										<a href="{{ url('course/learn/'. $course->slug)}}">
											<div class="card-block text-left">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
												<p class="fs-12 text-truncate color-dark" style="line-height: 1;">{{$course->author}}</p>
												<div class="m-0">
													<div class="progress progress-custom mb-1">
														<div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}"
															role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100"
															style="width: {{$percentage}}%;"></div>
													</div>
													<div class="progress-info">
														<div class="progress-percentage">
															{{ceil($percentage >= 100 ? 100 : $percentage)}}%
															@lang('front.page_my_courses.courses_percentage_finish')
														</div>
													</div>
												</div>
											</div>
										</a>
										@endif
	
										@if($course->id_author == \Auth::user()->id)
										<div class="code-class">
											<div class="no-m fs-12">
												<i class="fa fa-lock"></i> @lang('front.page_my_courses.courses_password_text') : <p class="m-0">
													{{$course->password}}</p>
												<button class="btn btn-primary btn-raised fs-10 btn-copy"
													data-clipboard-text="{{$course->password}}"
													style="padding: 5px 10px">@lang('front.page_my_courses.courses_copy_code_text')</button>
												<button class="btn btn-primary btn-raised fs-10" type="button" style="padding: 5px 10px"
													data-toggle="modal"
													data-target="#shareCode">@lang('front.page_my_courses.courses_share_code_text')</button>
											</div>
										</div>
										@endif
	
									</div>
								</div>
								@endforeach
							</div>
	
							<div class="text-center">
								{{$teacher_courses->links('pagination.default')}}
							</div>
						</div> --}}

						{{-- <div id="divStudentCourses">
							<div class="row">
								@foreach($student_courses as $course)
								<div class="col-md-3 mb-2">
									<div id="class-card" class="card-sm">
	
										@if($course->id_author == \Auth::user()->id)
											<div class="btn-opsi">
												<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-ellipsis-v"></i>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li>
														<a class="dropdown-item" href="/course/preview/{{ $course->id }}">
															<i class="fa fa-cog"></i> @lang('front.page_my_courses.manage_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan menduplikasi kelas ini?')"
															href="/course/duplicate/{{ $course->id }}">
															<i class="fa fa-clone"></i> @lang('front.page_my_courses.duplicate_course_teacher_option')
														</a>
													</li>
													<li>
														<a class="dropdown-item" onclick="return confirm('Yakin akan mengarsipkan kelas ini?')"
															href="/course/archive/{{ $course->id }}">
															<i class="fa fa-archive"></i> @lang('front.page_my_courses.archive_course_teacher_option')
														</a>
													</li>
												</ul>
											</div>
										@endif
	
										<a
											href="{{ $course->id_author == \Auth::user()->id ? url('/course/preview/'.$course->id) : url('course/learn/'. $course->slug)}}">
											<img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
										</a>
	
										@if($course->id_author == \Auth::user()->id)
	
										<div class="card-block text-left">
											<a href="/course/preview/{{ $course->id }}">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
											</a>
	
											@php $status_checked = $course->status == '1' ? 'checked' : ''; @endphp
											<div class="form-group m-0">
												<div class="togglebutton">
													<label class="control-label m-0" title="Terbitkan atau simpan sbg draf">
														<input type="checkbox" {{$status_checked}} name="status{{$course->id}}">
													</label>
												</div>
											</div>
	
											<div class="section-body-xs no-m">
												<p class="no-m fs-12">
													<span><i class="zmdi zmdi-info"></i> @lang('front.page_my_courses.courses_status')
														{{$status_checked == 'checked' ? Lang::get('front.page_my_courses.courses_status_active') : Lang::get('front.page_my_courses.courses_status_nonactive')}}</span>
													&nbsp;|&nbsp;
													<span><i class="zmdi zmdi-accounts"></i> {{course_users($course->id)}}
														@lang('front.page_my_courses.courses_student')</span>
												</p>
											</div>
	
										</div>
										@else
	
										@php
										$num_contents = count($course->contents);
										// echo $num_contents;
										//count percentage
										$num_progress = 0;
										$num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' =>
										Auth::user()->id, 'status' => '1'])->get());
										$percentage = 0;
										$percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
										$percentage = $percentage == 0 ? 1 : $percentage ;
										$percentage = 100 / $percentage;
										//count percentage
										@endphp
										<a href="{{ url('course/learn/'. $course->slug)}}">
											<div class="card-block text-left">
												<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
												<p class="fs-12 text-truncate color-dark" style="line-height: 1;">{{$course->author}}</p>
												<div class="m-0">
													<div class="progress progress-custom mb-1">
														<div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}"
															role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100"
															style="width: {{$percentage}}%;"></div>
													</div>
													<div class="progress-info">
														<div class="progress-percentage">
															{{ceil($percentage >= 100 ? 100 : $percentage)}}%
															@lang('front.page_my_courses.courses_percentage_finish')
														</div>
													</div>
												</div>
											</div>
										</a>
										@endif
	
										@if($course->id_author == \Auth::user()->id)
										<div class="code-class">
											<div class="no-m fs-12">
												<i class="fa fa-lock"></i> @lang('front.page_my_courses.courses_password_text') : <p class="m-0">
													{{$course->password}}</p>
												<button class="btn btn-primary btn-raised fs-10 btn-copy"
													data-clipboard-text="{{$course->password}}"
													style="padding: 5px 10px">@lang('front.page_my_courses.courses_copy_code_text')</button>
												<button class="btn btn-primary btn-raised fs-10" type="button" style="padding: 5px 10px"
													data-toggle="modal"
													data-target="#shareCode">@lang('front.page_my_courses.courses_share_code_text')</button>
											</div>
										</div>
										@endif
	
									</div>
								</div>
								@endforeach
							</div>
	
							<div class="text-center">
								{{$student_courses->links('pagination.default')}}
							</div>
						</div> --}}

					</div>

					<!-- TAB PROGRAM -->
					<div role="tabpanel" class="tab-pane" id="program">
						<div class="row">
							@foreach ($programs as $data)
							<div class="col-md-3 mb-2">
								<div class="card-sm">
									<a href="/program/learn/{{$data->slug}}">
										<img src="{{$data->image}}" alt="" class="dq-image img-fluid">
										<div class="card-block text-left">
											<h4 class="headline-xs dq-max-title">{{$data->title}}</h4>
										</div>
									</a>
								</div>
							</div>
							@endforeach
						</div>
					</div>

					<!-- TAB ARSIP -->
					<div role="tabpanel" class="tab-pane" id="archive">
						<div class="row">
							@if(isInstructor(Auth::user()->id))
							@foreach($teacher_courses_archives as $course)
							<div class="col-md-3 mb-2">
								<div class="card-sm">
									@if($course->id_author == \Auth::user()->id)
									<div class="btn-opsi">
										<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-ellipsis-v"></i>
										</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a class="dropdown-item"
													onclick="return confirm('{{Lang::get('front.page_my_courses.courses_archive_restore_alert')}}')"
													href="/course/restore/{{$course->id}}">
													<i class="fa fa-refresh"></i> @lang('front.page_my_courses.courses_archive_restore')
												</a>
											</li>
											<li>
												<a class="dropdown-item"
													onclick="return confirm('{{Lang::get('front.page_my_courses.courses_archive_delete_alert')}}')"
													href="/course/delete/{{$course->id}}">
													<i class="fa fa-trash-o"></i> @lang('front.page_my_courses.courses_archive_delete')
												</a>
											</li>
										</ul>
									</div>
									@endif
									<a href="#">
										<img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
									</a>
									<div class="card-block text-left">
										<a href="#">
											<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
										</a>
										<div class="section-body-xs no-m">
											<p class="no-m fs-12">
												<span><i class="zmdi zmdi-info"></i> @lang('front.page_my_courses.courses_archived_text')</span>
												&nbsp;|&nbsp;
												<span><i class="zmdi zmdi-accounts"></i> {{course_users($course->id)}}
													@lang('front.page_my_courses.courses_student')</span>
											</p>
										</div>
									</div>
									@if($course->password != NULL)
									<div class="code-class">
										<div class="no-m fs-12">
											<i class="fa fa-lock"></i> @lang('front.page_my_courses.courses_password_text') : <p>
												{{$course->password}}</p>
										</div>
									</div>
									@endif
								</div>
							</div>
							@endforeach
							@else
							@foreach($student_courses_archives as $course)
							<div class="col-md-3 mb-2">
								<div class="card-sm">
									<div class="btn-opsi">
										<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-ellipsis-v"></i>
										</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a class="dropdown-item"
													onclick="return confirm('{{Lang::get('front.page_my_courses.courses_archive_restore_alert')}}')"
													href="/course/user/restore/{{$course->id}}">
													<i class="fa fa-refresh"></i> @lang('front.page_my_courses.courses_archive_restore')
												</a>
											</li>
										</ul>
									</div>
									<a href="{{url('course/learn/'. $course->slug)}}">
										<img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
									</a>
									<div class="card-block text-left">
										@if(is_liveCourse($course->id) == true) <span
											class="badge badge-danger">@lang('front.page_my_courses.courses_status_live')</span> @endif
										@if($course->public == '0') <span
											class="badge badge-warning">@lang('front.page_my_courses.courses_status_private')</span> @endif
										@if($course->model == 'batch') <span
											class="badge badge-success">@lang('front.page_my_courses.courses_status_scheduled')</span> @endif
										@if($course->model == 'subscription') <span
											class="badge badge-primary">@lang('front.page_my_courses.courses_status_lifetime')</span> @endif
										<a href="{{url('course/learn/'. $course->slug)}}">
											<h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
										</a>
										<div class="section-body-xs no-m">
											<p class="no-m fs-12"><i class="fa fa-users"></i> @lang('front.page_my_courses.total_student') :
												({{course_users($course->id)}})</p>
										</div>
									</div>
									@if($course->password != NULL)
									<div class="code-class">
										<div class="no-m fs-12">
											<i class="fa fa-lock"></i> @lang('front.page_my_courses.courses_password_text') : <p>
												{{$course->password}}</p>
										</div>
									</div>
									@endif
								</div>
							</div>
							@endforeach
							@endif
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal" id="shareCode" tabindex="-1" role="dialog" aria-labelledby="shareCode">
					<div class="modal-dialog animated zoomIn animated-3x" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<h2 class="headline text-center">@lang('front.page_my_courses.courses_share_code_via_text')</h2>
								<div class="text-center">
									<a href="#" class="btn btn-whatsapp"><i class="zmdi zmdi-whatsapp"></i> Whatsapp</a>
									<a href="#" class="btn btn-google"><i class="zmdi zmdi-google"></i> Google</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal" id="modalPrivateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog animated zoomIn animated-3x" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title color-primary" id="myModalLabel">@lang('front.page_my_courses.input_course_password')</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
							class="zmdi zmdi-close"></i></span></button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">@lang('front.page_my_courses.courses_password_text')</label>
					<input type="text" class="form-control" id="CoursePassword" placeholder="{{Lang::get('front.page_my_courses.input_course_password')}}">
				</div>
			</div>
			<div class="modal-footer">
				<button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">@lang('front.page_my_courses.course_with_password_join_button')</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalPrivateProgram" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog animated zoomIn animated-3x" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title color-primary" id="myModalLabel">@lang('front.page_my_courses.input_course_password')</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
							class="zmdi zmdi-close"></i></span></button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">@lang('front.page_my_courses.courses_password_text')</label>
					<input type="text" class="form-control" id="ProgramPassword" placeholder="{{Lang::get('front.page_my_courses.input_course_password')}}">
				</div>
			</div>
			<div class="modal-footer">
				<button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">@lang('front.page_my_courses.course_with_password_join_button')</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
@endsection

@push('script')
<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
<script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('clipboard/clipboard.min.js')}}"></script>

<script type="text/javascript">
	var clipboard = new ClipboardJS('.btn-copy');
	clipboard.on('success', function (e) {
		alert(e.text + ' Berhasil disalin')
		console.info('Action:', e.action);
		console.info('Text:', e.text);
		console.info('Trigger:', e.trigger);
		e.clearSelection();
	});
</script>

@foreach($teacher_courses as $course)
<script type="text/javascript">
	$("[name=status{{$course->id}}]").on("change", function () {
		var state = $("[name=status{{$course->id}}]").is(":checked")
		var token = "{{csrf_token()}}";
		if (state === true) {
			$.ajax({
				url: "/course/publish",
				type: "POST",
				data: {
					id: '{{$course->id}}',
					status: 1,
					live: "{{is_liveCourse($course->id) == true ? 'true' : 'false'}}",
					_token: token
				},
				success: function (result) {
					// console.log(result);
					if (result.status != 200) {
						swal({
							type: 'warning',
							title: 'Gagal...',
							html: result.message,
						})
					} else {
						swal("Berhasil!", "Kursus Anda telah aktif dan dapat diakses", "success");
					}
				},
			});
		} else {
			$.ajax({
				url: "/course/publish",
				type: "POST",
				data: {
					id: "{{$course->id}}",
					status: 0,
					_token: token,
					live: "{{is_liveCourse($course->id) == true ? ' true ' : 'false'}}",
				},
				success: function (result) {

				},
			});
		}
	});
</script>

<script type="text/javascript">
	$("[name=publish{{$course->id}}]").on("change", function () {
		var state = $("[name=publish{{$course->id}}]").is(":checked")
		var token = "{{csrf_token()}}";
		if (state === true) {
			$.ajax({
				url: "/meeting/publish",
				type: "POST",
				data: {
					id: "{{$course->id}}",
					publish: 1,
					_token: token
				},
				success: function (result) {

				},
			});
		} else {
			$.ajax({
				url: "/meeting/publish",
				type: "POST",
				data: {
					id: "{{$course->id}}",
					publish: 0,
					_token: token
				},
				success: function (result) {

				},
			});
		}
	});
</script>
@endforeach

@if(\Session::get('message'))
<script type="text/javascript">
	swal("Informasi", "{{\Session::get('message')}}", "info");
</script>
@endif

<script>
	@if(isInstructor(Auth::user() -> id))
	$("#allCourses").show();
	$("#teacherCourses").hide();
	$(".studentCourses").hide();
	$(".studentPrograms").hide();
	@else
	$("#allCourses").hide();
	$("#teacherCourses").hide();
	$(".studentCourses").show();
	$(".studentPrograms").hide();
	@endif

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		//show selected tab / active
		// alert ( $(e.target).attr('aria-controls') );

		if ($(e.target).attr('aria-controls') == 'course') {
			$("#allCourses").show();
			$("#teacherCourses").hide();
			$(".studentCourses").hide();
			$(".studentPrograms").hide();
		}
		if ($(e.target).attr('aria-controls') == 'open-class') {
			$("#allCourses").hide();
			$("#teacherCourses").show();
			$(".studentCourses").hide();
			$(".studentPrograms").hide();
		}
		if ($(e.target).attr('aria-controls') == 'close-class') {
			$("#allCourses").hide();
			$("#teacherCourses").hide();
			$(".studentCourses").show();
			$(".studentPrograms").hide();
		}
		if ($(e.target).attr('aria-controls') == 'program') {
			$("#allCourses").hide();
			$("#teacherCourses").hide();
			$(".studentCourses").hide();
			$(".studentPrograms").show();
		}
		if ($(e.target).attr('aria-controls') == 'archive') {
			$("#allCourses").hide();
			$("#teacherCourses").hide();
			$(".studentCourses").hide();
			$(".studentPrograms").hide();
		}

	});
</script>

<script type="text/javascript">
	$("#CheckPassword").click(function () {
		var password = $("#CoursePassword").val();
		$.ajax({
			url: '/course/join-private-course',
			type: 'post',
			data: {
				password: password,
				_token: '{{csrf_token()}}',
			},
			success: function (response) {
				console.log(response);
				if (response.code == 200) {
					$.ajax({
						url: '/course/enroll',
						type: 'get',
						data: {
							id: response.data.id,
						},
						success: function (data) {
							swal({
								type: 'success',
								title: 'Berhasil...',
								html: response.message,
							}).then(function () {
								window.location.assign('/course/learn/' + response.data.slug)
							});
						}
					})
				} else {
					swal({
						type: 'error',
						title: 'Gagal!',
						html: response.message,
					})
				}
			}
		})
	})
</script>

<script>
	$(".filterCourses").change(function() {
		// window.location.assign('?levels=' + $(this).val())
		if($(this).val() === 'teacher') {
			$(".card-teacher").show()
			$(".card-student").hide()
		}
		if($(this).val() === 'student') {
			$(".card-teacher").hide()
			$(".card-student").show()
		}
		if($(this).val() === 'all') {
			$(".card-teacher").show()
			$(".card-student").show()
		}
	})
</script>

@endpush