@extends('layouts.app')

@section('title', Lang::get('front.page_manage_preview_content.title'))

@push('style')

  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">

  <style>
    .modal .modal-dialog .modal-content .modal-header .modal-title {
      width: 100%;
      padding: 2rem 3rem;
    }

    .modal .modal-dialog .modal-content .modal-header .close {
      margin: 2rem 3rem;
    }

    .card-md {
      position: relative;
    }

    .card-md .md-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 2rem;
      top: 2rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card-md .md-dropdown .md-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card-md .md-dropdown .md-dropdown-group .md-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      width: max-content;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card-md .md-dropdown .md-dropdown-group:hover .md-dropdown-menu {
      display: block;
    }

    .card-md .md-dropdown .md-dropdown-group .md-dropdown-menu .md-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card-md .md-dropdown .md-dropdown-group .md-dropdown-menu .md-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card-md .card-footer {
      padding: 2rem;
    }

    .card-md .card-footer .footer-headline {
      font-weight: 700;
      margin-bottom: 0;
    }

    .form-group {
      margin-top: 1rem;
    }

    .btn-comment {
      background: #fff;
      color: #ccc;
      cursor: text;
      border: 1px solid #ddd;
      padding: 1rem;
      margin-top: 1rem;
      margin-bottom: 3rem;
    }

    .comment {
      position: relative;
      border-top: 1px solid #ccc;
      border-bottom: 1px solid #ccc;
      padding: 2rem 0;
    }

    .comment .comment-action {
      position: absolute;
      right: 0;
    }

    .comment .comment-action a {
      width: 30px;
      height: 30px;
      background: #fff;
      color: #333;
      cursor: pointer;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      border: 1px solid #4ca3d9;
    }

    .comment .comment-action a:hover {
      color: #333;
    }

    .comment .comment-info {
      margin-bottom: 0.5rem;
    }

    .comment .comment-info .comment-user {
      font-weight: 700;
      color: #333;
      margin-bottom: 0;
    }

    .comment .comment-info .comment-date {
      font-size: 12px;
      color: #999;
      line-height: 1.5;
      margin-bottom: 0;
    }

    .comment .comment-content {
      margin-bottom: 0;
    }
  </style>

  <style media="screen">
    .vjs-play-progress{
      background: red !important;
      height: 8px;
    }
    .vjs-big-play-button {
      font-size: 3em;
      line-height: 2em !important;
      height: 2em !important;
      width: 3em !important;
      display: block;
      position: relative;
      top: 50% !important;
      left: 50% !important;
      padding: 0;
      cursor: pointer;
      opacity: 1;
      border: 0.06666em solid #fff;
      background-color: red !important;
      -webkit-border-radius: 0.3em;
      -moz-border-radius: 0.3em;
      border-radius: 0.3em;
      -webkit-transition: all 0.4s;
      -moz-transition: all 0.4s;
      -ms-transition: all 0.4s;
      -o-transition: all 0.4s;
      transition: all 0.4s;
    }
    .video-js .vjs-time-control{
      flex: none;
      font-size: 1em;
      line-height: 5.2em;
      min-width: 2em;
      width: auto;
      padding-left: 1em;
      padding-right: 1em;
    }
    .vjs-volume-bar .vjs-slider-horizontal{
      width: 5em;
      height: 1.3em;
    }
    .video-js .vjs-volume-bar{
      margin: 2.35em 0.45em;
      margin-top: 2.35em;
      margin-right: 0.45em;
      margin-bottom: 2.35em;
      margin-left: 0.45em;
    }
    .vjs-playback-rate .vjs-playback-rate-value{
      pointer-events: none;
      font-size: 1.5em;
      line-height: 3.3em;
      text-align: center;
    }
    .vjs-menu-button-popup .vjs-menu .vjs-menu-content{
      background-color: rgba(43, 51, 63, 0.7);
      position: absolute;
      width: 100%;
      bottom: 3.5em;
      max-height: 15em;
    }
    .video-js .vjs-control-bar{
      width: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 5.0em;
      background-color: rgba(43, 51, 63, 0.7);
    }
    .vjs-button > .vjs-icon-placeholder::before {
      font-size: 1.8em;
      line-height: 2.8em;
    }
  </style>

  <style media="screen">

    .player-content--text:before {
      content: '';
      position: fixed;
      display: block;
      background: linear-gradient(180deg, #000 -5%, transparent 10%);
      width: 100%;
      height: 100%;
      opacity: 0;
      top: 0;
      z-index: 0;
      transition: 0.3s ease-in-out;
    }

    .embed-responsive:before {
      content: '';
      position: relative;
      display: block;
      background: linear-gradient(180deg, #000 -5%, transparent 10%);
      height: 100%;
      opacity: 0;
      z-index: 1; /* Jika quiz atau assignment z-index: 0; */
      transition: 0.3s ease-in-out;
    }

    .player-content--text:hover:before,
    .embed-responsive:hover:before,
    .embed-responsive:hover .player-navbar {
      opacity: 1;
      /* -moz-animation: cssAnimation 0.5s ease-out 3s;
      -webkit-animation: cssAnimation 0.5s ease-out 3s;
      -o-animation: cssAnimation 0.5s ease-out 3s;
      animation: cssAnimation 0.5s ease-out 3s;
      -webkit-animation-fill-mode: forwards;
      animation-fill-mode: forwards; */
    }

    @keyframes cssAnimation {
      to {
        opacity: 0;
      }
    }
    @-webkit-keyframes cssAnimation {
      to {
        opacity: 0;
      }
    }

    .player-content--text .player-navbar .content-toggle,
    .embed-responsive .player-navbar .content-toggle {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #222;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-title,
    .embed-responsive .player-navbar .content-title {
      color: #fff;
      line-height: 50px;
      padding: 0 1rem;
      margin: 0;
      opacity: 0;
    }

    .player-content--text .player-navbar .content-close,
    .embed-responsive .player-navbar .content-close {
      display: flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      background: #e41f1f;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      transition: 0.2s ease-in-out;
      opacity: 0
    }

    .player-content--text .player-navbar {
      position: sticky;
      margin-bottom: 1rem;
    }

    .player-content--text .player-navigate {
      position: sticky;
      top: calc(50% - 50px);
      z-index: 0;
    }

    .player-content--text .row.player-content-group {
      margin-top: -50px;
      margin-left: 0;
      margin-right: 0;
    }

    .player-content--text .player-attach {
      background: #eee;
      padding: 2rem 0;
      margin: 0;
    }

    .player-content--text .player-attach .attach-link {
      color: #007bff;
    }

    .player-content--text .player-attach .attach-link:hover {
      color: #004b9c;
    }

    .player-content--text:hover .player-navbar .content-toggle,
    .player-content--text:hover .player-navbar .content-title,
    .player-content--text:hover .player-navbar .content-close,
    .embed-responsive:hover .player-navbar .content-toggle,
    .embed-responsive:hover .player-navbar .content-title,
    .embed-responsive:hover .player-navbar .content-close {
      opacity: 1;
    }

    .embed-responsive:hover .vjs-big-play-centered .vjs-big-play-button,
    .embed-responsive:hover .video-js .vjs-control-bar {
      z-index: 2;
    }

    .player-content--text:hover .player-navbar .content-toggle:hover,
    .embed-responsive:hover .player-navbar .content-toggle:hover {
      background: #111;
      color: #fff;
    }

    .player-content--text:hover .player-navbar .content-close:hover,
    .embed-responsive:hover .player-navbar .content-close:hover {
      background: #cc3333;
      color: #fff;
    }

    .player-navbar {
      position: absolute;
      width: 100%;
      opacity: 1;
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      top: 0;
      z-index: 999;
      transition: 0.5s ease-in-out;
    }

    .player-navigate {
      position: absolute;
      display: flex;
      width: 100%;
      height: 100%;
      align-items: center;
      justify-content: space-between;
      top: 0;
    }

    .player-navigate .navigate {
      display: flex;
      height: auto;
      background: rgba(0, 0, 0, 0.25);
      align-items: center;
      justify-content: center;
      font-size: 26px;
      color: #fff;
      padding: 0.75rem;
      cursor: pointer;
      z-index: 3;
      transition: 0.3s ease-in-out;
    }

    .player-navigate .navigate:hover {
      background: rgba(0, 0, 0, 0.75);
    }

    #sidebar .player-content-headbox {
      position: sticky;
      display: flex;
      background: #f3f3f3;
      align-items: center;
      font-weight: 600;
      color: #333;
      top: 0;
      z-index: 10;
    }

    #sidebar .player-content-headbox .headbox-button {
      display: inline-flex;
      width: 50px;
      height: 50px;
      align-items: center;
      justify-content: center;
      font-size: 20px;
      border-right: 2px solid #e3e3e3;
      cursor: pointer;
      padding: 1rem;
    }

    #sidebar .player-content-headbox .headbox-button.button-right {
      display: none;
      border-left: 2px solid #e3e3e3;
      border-right: none;
    }

    #sidebar .player-content-headbox .headbox-group {
      display: flex;
      align-items: center;
      width: calc(100% - 50px);
      height: 50px;
      padding: 1rem;
    }

    #sidebar .player-content-list {
      display: block;
      overflow-y: auto;
      height: calc(100% - 50px);
    }

    #sidebar .player-content-list .head-of-list {
      background: #ececec;
      display: block;
      padding: 0.75rem;
    }

    #sidebar .player-content-list .content-of-list {
      background: #f3f3f3;
      display: block;
      color: #999;
      /* color: #333; */
      padding: 0.75rem;
      font-weight: 600;
      transition: all 0.1s;
    }

    #sidebar .player-content-list .content-of-list:hover {
      color: #333;
    }

    #sidebar .player-content-list li.active .content-of-list {
      background: #dae5eb;
      color: #333;
      border-left: 5px solid #333;
    }

    #sidebar .player-content-list .content-of-list .list-row {
      display: flex;
      align-items: baseline;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-icon,
    #sidebar .player-content-list .content-of-list .list-row .list-text,
    #sidebar .player-content-list .content-of-list .list-row .list-check {
      padding-left: 5px;
      padding-right: 5px;
    }

    #sidebar .player-content-list .content-of-list .list-row .list-check {
      margin-left: auto;
    }

    #content {
      background: {{$content->type_content == 'video' || $content->type_content == 'url-video'  ? 'black' : 'white'}};
    }

    #content .player-content-headbox,
    #content .player-content-list {
      display: none;
    }

    @media (max-width: 768px) {
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }
      .embed-responsive-16by9::before{
        padding-top: 56.25%;
      }
      .player-navbar {
        display: none;
      }
    }

    @media (max-width: 420px) {
      body {
        overflow-x: hidden;
        overflow-y: auto;
      }
      .player-content--text:before,
      .embed-responsive:before {
        background: transparent;
        z-index: 0;
      }

      .player-content--text .player-navbar {
        display: flex;
      }

      .player-content--text .player-navigate {
        z-index: 1;
      }

      .player-content--text .row .content--text * {
        font-size: 14px;
      }

      .player-content--text .player-navbar .content-toggle,
      .player-content--text .player-navbar .content-close,
      .embed-responsive .player-navbar .content-toggle,
      .embed-responsive .player-navbar .content-close {
        width: 50px;
        height: 50px;
        opacity: 1
      }

      .player-content--text .player-navbar .content-title,
      .embed-responsive .player-navbar .content-title,
      .player-content--text .player-navbar .content-close {
        display: none;
      }

      .player-navigate {
        position: absolute;
        display: flex;
        width: 100%;
        height: 100%;
        align-items: center;
        justify-content: space-between;
        top: 0;
      }

      .player-navigate .navigate {
        font-size: 26px;
        padding: 0.5rem;
      }

      #sidebar .player-content-headbox .headbox-group {
        width: calc(100% - 100px);
      }

      #sidebar .player-content-headbox .headbox-button.button-right {
        display: inline-flex;
      }

      #sidebar .player-content-list {
        height: calc(100% + 20px);
      }

      #content {
        background: {{$content->type_content == 'video' || $content->type_content == 'url-video'  ? '#f9f9f9' : 'white'}}
      }

      #content .container-full.controlable {
        position: sticky;
        top: 0;
        z-index: 1;
      }

      #content .player-content-headbox {
        position: fixed; /* addon */
        width: 100%; /* addon */
        background: #f3f3f3;
        display: flex;
        align-items: center;
        z-index: 1; /* addon */
      }

      #content .player-content-headbox .headbox-group {
        width: calc(100% - 50px);
        padding: 0.75rem;
      }

      #content .player-content-headbox .headbox-button {
        width: 50px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        border-right: 2px solid #e3e3e3;
        padding: 0.75rem;
      }

      #content .player-content-list {
        background: #f9f9f9;
        display: block;
        padding-bottom: 0.5rem; /* addon */
        margin-top: 55px; /* addon */
      }

      #content .player-content-list .head-of-list {
        background: #ececec;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list .content-of-list {
        background: #f3f3f3;
        display: block;
        padding: 0.75rem;
      }

      #content .player-content-list li.active .content-of-list {
        background: #dae5eb;
      }

      #content .player-content-list .content-of-list .list-row {
        display: flex;
        align-items: baseline;
      }

      #content .player-content-list .content-of-list .list-row .list-icon,
      #content .player-content-list .content-of-list .list-row .list-text,
      #content .player-content-list .content-of-list .list-row .list-check {
        padding-left: 5px;
        padding-right: 5px;
      }
    }
  </style>

@if($content->type_content == 'scorm')
  <style type="text/css">
    iframe {
      width: 100%; height: 387px;
    }
  </style>
  <script src="/js/scorm/sscompat.js" type="text/javascript"></script>
  <script src="/js/scorm/sscorlib.js" type="text/javascript"></script>
  <script src="/js/scorm/ssfx.Core.js" type="text/javascript"></script>
  <script type="text/javascript" src="/js/scorm/API_BASE.js"></script>
  <script type="text/javascript" src="/js/scorm/API.js"></script>
  <script type="text/javascript" src="/js/scorm/Controls.js"></script>
  <script type="text/javascript" src="/js/scorm/Player.js"></script>

  <script type="text/javascript">
    function InitPlayer() {
      PlayerRun();
    }
    function PlayerRun() {
      var contentDiv = document.getElementById('placeholder_contentIFrame');
      contentDiv.innerHTML = "";
      Run.ManifestByURL("{{asset_url($content->path_file)}}", true);
    }
    InitPlayer()
  </script>
@endif

@endpush

@section('content')

@if($content->type_content == 'scorm') <body onload="InitPlayer()"> @endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.page_manage_preview_content.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_preview_content.breadcumb_manage_courses')</a></li>
          <li><a href="/course/preview/{{ $course->id }}">{{ $course->title }}</a></li>
          <li>@lang('front.page_manage_preview_content.breadcumb_preview')</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <a href="/course/preview/{{ $course->id }}" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" title="@lang('front.general.text_back')" style="position: absolute; margin-left: 15px;"><i class="zmdi zmdi-arrow-left"></i></a>
        <div class="col-md-8 mx-auto">
          <div class="card-md">
            <div class="md-dropdown">
              <div class="md-dropdown-group">
                <i class="fa fa-ellipsis-v"></i>
                <ul class="md-dropdown-menu">
                  <li>
                    <a class="md-dropdown-item" href="{{url('course/publish_unpublish/'.$content->id)}}">
                      <i class="fa fa-circle"></i> @lang('front.page_manage_preview_content.option_set_as') {{$content->status == '0' ? 'publish' : 'draft'}}
                    </a>
                  </li>
                  <li>
                    <a class="md-dropdown-item preview_content" href="#" value="{{$content->id}}" data-preview="{{ $content->preview }}" id="previewEye{{$content->id}}">
                      <i class="fa fa-eye"></i> @lang('front.page_manage_preview_content.option_set_as') {{ $content->preview == '1' ? 'non-preview' : 'preview' }}
                    </a>
                  </li>
                  <li>
                    <a class="md-dropdown-item" href="{{url('course/content/add/library/'.$content->id.'/'.$course->id)}}">
                      <i class="fa fa-plus"></i> @lang('front.page_manage_preview_content.option_add_to_library')
                    </a>
                  </li>
                  <li>
                    <a class="md-dropdown-item" href="{{url('course/content/update/'.$content->id. '?content=' . $content->type_content)}}">
                      <i class="fa fa-pencil"></i> @lang('front.general.text_edit')
                    </a>
                  </li>
                  <li>
                    <a class="md-dropdown-item" href="{{url('course/content/delete/'.$course->id.'/'.$content->id)}}" onclick="return confirm('@lang('front.page_manage_preview_content.option_alert_delete')')">
                      <i class="fa fa-trash"></i> @lang('front.general.text_delete')
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            {{-- KONTEN TEKS --}}
            @if($content->type_content == 'text')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                {!! $content->description !!}
              </div>
            @endif
            {{-- KONTEN TEKS --}}

            {{-- KONTEN SCORM --}}
              @if($content->type_content == 'scorm')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{$content->title}}</span></h2>
                {{-- <iframe src="{{$content->full_path_file}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 387px;">
                  Your browser doesnot support iframes <a href="{{$content->full_path_file}}"> click here to view the page directly. </a>
                </iframe> --}}
                <div id="placeholder_contentIFrame" style="width: 100%; height: 387px;overflow:auto;-webkit-overflow-scrolling:touch;"></div>
              </div>
              <div class="card-footer">
                {{$content->description}}
              </div>
              @endif
            {{-- KONTEN SCORM --}}

            {{-- KONTEN YOUTUBE --}}
            @if($content->type_content == 'url-video')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                {{-- <iframe src="https://www.youtube.com/embed/kmXW0RAseP4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 387px;"></iframe> --}}
                <video
                  id="my-video"
                  class="video-js embed-responsive-item vjs-big-play-centered"
                  controls="true"
                  preload="auto"
                  autoplay="false"
                  width="100%"
                  height="387px"
                  style="width: 100%; height: 387px;"
                >
                  <source src="{{$content->full_path_file}}" type="video/youtube">
                </video>
              </div>
              <div class="card-footer">
                {!! $content->is_description_show == '1' ? $content->description : '' !!}
              </div>
            @endif
            {{-- KONTEN YOUTUBE --}}

            {{-- KONTEN VIDEO --}}
            @if($content->type_content == 'video')
              {{-- @php
                $fixedUrl = '';
                if(strpos($content->full_path_original, "https://") != false){
                  $fixedUrl = server_assets() . $content->full_path_original;
                } else {
                  $fixedUrl = $content->full_path_original;
                }
              @endphp --}}
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                <video
                  id="my-video"
                  class="video-js embed-responsive-item vjs-big-play-centered"
                  controls="true"
                  preload="auto"
                  autoplay="true"
                  width="100%"
                  style="width: 100% !important; height: 500px;"
                  height="100%">
                  <source src="{{asset_url($content->path_file)}}" type='video/mp4'>
                </video>
              </div>
            @endif
            {{-- KONTEN VIDEO --}}

            {{-- KONTEN FILE --}}
            @if($content->type_content == 'file')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                {!! $content->description !!}
              </div>
              <div class="card-footer">
                <p class="footer-headline">@lang('front.page_manage_preview_content.file_attachment')</p>
                <a href="{{$content->full_path_file}}"><i class="fa fa-save"></i> @lang('front.page_manage_preview_content.file_download') {{$content->name_file}}</a>
              </div>
            @endif
            {{-- KONTEN FILE --}}

            {{-- KONTEN FILE --}}
            @if($content->type_content == 'folder')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                {!! $content->description !!}
              </div>
              <div class="table-responsive table-cs">
								<table id="example" class="table table-striped d-table">
									<thead>
										<tr>
		                  <th>No</th>
											<th style="min-width: 25vh;">File</th>
											<th>Tipe</th>
											<th class="text-center">Size</th>
										</tr>
									</thead>
									<tbody>
										@foreach($content->files as $index => $item)
										<?php
											$file_name = explode('-',$item->file_name);
											unset($file_name[0]);
											$file_name = implode('-',$file_name);
										 ?>
											<tr>
												<td>{{$index+1}}</td>
												<td style="min-width: 25vh;">
													@if($item->file_type == 'pdf')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/pdf.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@elseif($item->file_type == 'docx')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/word.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@elseif($item->file_type == 'mp4')
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/mp4.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@else
															<a href="{{$item->file_path}}" title="{{$item->file_name}}" download="{{$file_name}}"><img src="{{asset('images/file.png')}}" style="width: 50px;"> <span>{{$file_name}}</span></a>
													@endif
												</td>
												<td>{{$item->file_type}}</td>
												<td class="text-center">{{formatSizeUnits($item->file_size)}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
            @endif
            {{-- KONTEN FILE --}}

            {{-- DISKUSI --}}
            @if($content->type_content == 'discussion')
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                <p>{!! $content->description !!}</p>
              </div>
              <div class="card-footer">
                <div class="btn-comment" id="commentBtn">Berikan komentar anda ...</div>
                <form id="comment" action="#" style="margin-bottom: 3rem;">
                  <textarea name="body" id="answer" class="form-control" required="required"></textarea>
                  <button class="btn btn-sm btn-raised btn-primary" type="submit">Kirim</button>
                </form>

                <div class="comment">
                  <div class="comment-action">
                    <a id="replyBtn" title="Balas"><i class="fa fa-reply"></i></a>
                    <a href="#" title="Hapus"><i class="fa fa-trash"></i></a>
                  </div>
                  <div class="comment-info">
                    <p class="comment-user">John Doe</p>
                    <p class="comment-date">25 Aug 2020</p>
                  </div>
                  <p class="comment-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <form id="reply" action="#">
                    <textarea name="body" id="commentReply" class="form-control" required="required"></textarea>
                    <button class="btn btn-sm btn-raised btn-primary" type="submit">Kirim</button>
                  </form>
                </div>
              </div>
            @endif
            {{-- DISKUSI --}}

            {{-- TUGAS --}}
            @if(isset($assignment))
              <div class="card-block">
                <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
                <p>{!! $content->description !!}</p>
                @if($content->file)
                  <a href="{{ asset_url($content->file) }}"><i class="fa fa-download"></i> Download</a>
                @endif
              </div>
              <div class="card-footer">
                <p class="footer-headline mb-2">Ringkasan Penilaian</p>
                <table class="table table-striped table-hover">
                  <tr>
                    <td>Jumlah Peserta</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>Peserta Mengirimkan</td>
                    <td>5</td>
                  </tr>
                  <tr>
                    <td>Perlu Penilaian</td>
                    <td>3</td>
                  </tr>
                  <tr>
                    <td>Batas Waktu</td>
                    <td>31 Agustus 2020</td>
                  </tr>
                  <tr>
                    <td>Waktu Tersisa</td>
                    <td>4 hari</td>
                  </tr>
                </table>
                <a href="#" class="btn btn-sm btn-raised btn-white" data-toggle="modal" data-target="#showSubmissions">Lihat Semua Kiriman</a>
                <a href="#" class="btn btn-sm btn-raised btn-primary" data-toggle="modal" data-target="#giveGrade">Nilai</a>
              </div>
            @endif
            {{-- TUGAS --}}

            {{-- KUIS --}}
            {{-- <div class="card-block">
              <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>Lorem Ipsum</span></h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a hendrerit sem, in varius tortor. Ut ornare elit nec nibh posuere, at venenatis turpis ultricies. Etiam id mi sed augue euismod lobortis. Pellentesque eleifend libero et euismod pretium. Donec vitae convallis mi. Vestibulum tincidunt dapibus nisi sit amet pretium. Praesent vel eros vulputate, feugiat nunc eget, malesuada dui. Sed eu auctor sapien. Nam fringilla pellentesque erat ornare lobortis. Fusce tellus ante, lobortis sit amet rutrum a, posuere sed urna.</p>
            </div>
            <div class="card-footer">
              <p style="font-size: 20px; font-weight: 400; text-align: center; margin-bottom: 0;">Pratinjau Kuis</p>
              <div class="d-flex">
                <a href="#" class="btn btn-sm btn-raised btn-primary mx-auto">Mencoba Kuis</a>
              </div>
              <table class="table table-striped table-hover">
                <tr>
                  <th>#</th>
                  <th>Status</th>
                  <th class="text-center">Jawaban Benar / 2.00</th>
                  <th class="text-center">Nilai / 10.00</th>
                  <th class="text-center">Ulasan</th>
                </tr>
                <tr>
                  <td>Pratinjau</td>
                  <td>Selesai</td>
                  <td class="text-center">1.00</td>
                  <td class="text-center">5.00</td>
                  <td class="text-center"><a href="#">Ulasan</a></td>
                </tr>
              </table>
              <div class="d-flex">
                <a href="#" class="btn btn-sm btn-raised btn-primary mx-auto">Mencoba Kembali Kuis</a>
              </div>
            </div> --}}
            {{-- KUIS --}}
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL SUBMISSIONS --}}
  <div class="modal" id="showSubmissions" tabindex="-1" role="dialog" aria-labelledby="labelShowSubmissions">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title color-primary" id="myModalLabel">Semua Kiriman</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-hover">
            <tr>
              <td>John Doe</td>
              <td>26 Agustus 2020</td>
              <td class="text-center"><a href="#">Lihat Jawaban</a></td>
            </tr>
            <tr>
              <td>Jane Doe</td>
              <td>26 Agustus 2020</td>
              <td class="text-center"><a href="#">Lihat Jawaban</a></td>
            </tr>
            <tr>
              <td>Robert Martin</td>
              <td>26 Agustus 2020</td>
              <td class="text-center"><a href="#">Lihat Jawaban</a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL NILAI --}}
  <div class="modal" id="giveGrade" tabindex="-1" role="dialog" aria-labelledby="labelgiveGrade">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title color-primary" id="myModalLabel">Penilaian</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-hover">
            <tr>
              <td>John Doe</td>
              <td>26 Agustus 2020</td>
              <td class="text-center">86</td>
            </tr>
            <tr>
              <td>Jane Doe</td>
              <td>26 Agustus 2020</td>
              <td class="text-center"><a href="#">Beri Nilai</a></td>
            </tr>
            <tr>
              <td>Robert Martin</td>
              <td>26 Agustus 2020</td>
              <td class="text-center"><a href="#">Beri Nilai</a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  @if($content->type_content == 'scorm') </body> @endif
@endsection

@push('script')
  <script src="/videojs/ie8/videojs-ie8.min.js"></script>
  <script src="/videojs/video.js"></script>
  <script src="/videojs/Youtube.min.js"></script>
  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('#commentReply').each(function(e){
      CKEDITOR.replace( this.id, options);
    });
  </script>
  <script>
    $('#comment').hide();
    $('#commentBtn').click(function() {
      $(this).slideToggle(300);
      $('#comment').slideToggle(300);
    });

    $('#reply').hide();
    $('#replyBtn').click(function() {
      $('#reply').slideToggle(300);
    });
  </script>

<script type="text/javascript">
  // =========== VIDEO PROPERTY =======
  videojs('my-video', {
    // "autoplay": true,
    playbackRates: [0.5, 1, 1.5, 2],
    controlBar: {
      fullscreenToggle: true,
    },
  });
  // =========== VIDEO PROPERTY =======

  var player = videojs('my-video');
  player.controlBar.addChild('QualitySelector');

</script>

<script type="text/javascript">
  $(".preview_content").click(function(){
    var data_id = $(this).attr('value');
    var token = "{{csrf_token()}}";
    var content_id = $(this).attr('value');
    var data_preview = $(this).attr('data-preview');
    // alert(data_preview)
    if(data_preview == '0'){

      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 1,
          _token: token
        },
        success : function(result){
          // alert(result.message);
          swal({
            title: result.title,
            text: result.message,
            type: 'success'
          }).then(function() {
              window.location = "";
          });
        },
      });

    }else{

      $.ajax({
        url : "/course/content/update-preview",
        type : "POST",
        data : {
          id : content_id,
          course_id : '{{$course->id}}',
          preview : 0,
          _token: token
        },
        success : function(result){
          swal({
            title: result.title,
            text: result.message,
            type: 'success'
          }).then(function() {
              window.location = "";
          });
        },
      });

    }
  })
</script>
@endpush
