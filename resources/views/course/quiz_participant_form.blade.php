@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ 'Category' }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Category Create</h3>
					</div>
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => $action, 'method' => 'post')) }}
							<div class="form-group">
								<label for="">Choose User</label>
								<select class="form-control" name="user_id[]" id="user_id" multiple="multiple">
									<option value="0">User</option>
									@foreach($users as $user)
										<option value="{{$user->id}}">{{$user->name}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								{{  Form::submit('Create' , array('class' => 'btn btn-primary', 'name' => 'button')) }}
								<a href="{{ url('course/quiz/question/manage/'.$quiz_id) }}" class="btn btn-default">Back To Quiz Manage</a>
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#user_id").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
