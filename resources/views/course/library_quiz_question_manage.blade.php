@extends('layouts.app')

@section('title')
	{{ 'Kelola Kuis ' . $quiz->name }}
@endsection

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<style media="screen">
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			background: #ccc;
			color: #333!important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:active {
			background: none;
			color: black!important;
		}
		select.form-control:not([size]):not([multiple]){
			height: auto !important;
		}
	</style>
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Library Question Quiz - {{$quiz->title}}</h3>
					</div>
					<div class="box-body">

						{{-- <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center> --}}

						<div class="panel panel-default">
						  <div class="panel-heading">
						    <h3 class="panel-title">Kelola Kuis</h3>
						  </div>
						  <div class="panel-body">

								<a href="{{url('instructor/library/quiz/question/create/'.$quiz->id)}}" class="btn btn-primary btn-raised"><i class="fa fa-plus"></i> Tambah Pertanyaan</a>
								{{-- <a href="{{url('course/quiz/question/import/'.$quiz->id)}}" class="btn btn-success btn-raised"><i class="fa fa-plus"></i> Import Pertanyaan</a> --}}
								<div class="btn-group">
									<button type="button" class="btn btn-primary btn-raised dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Import Pertanyaan
									<i class="zmdi zmdi-chevron-down right only"></i>
									</button>
									<form id="submit-file" method="post" action="/library/quiz/question/import/{{$quiz->id}}" enctype="multipart/form-data">
										{{csrf_field()}}
										<input id="file" type="file" name="file" style="display:none;">
										<ul class="dropdown-menu dropdown-menu-primary">
											<li><a href="/quiz_format/default_format.xlsx" class="dropdown-item">Download Format Quiz</a></li>
											<li><a href="#" id="upload-file" class="dropdown-item">Upload File</a></li>
										</ul>
									</form>
								</div>
								<a href="{{url('library/quiz/get/'.$quiz->id)}}" class="btn btn-raised"><i class="fa fa-plus"></i> Ambil Pertanyaan dari Kuis</a>
								<br><br>

								<div class="panel-group" id="accordion">
									@php $no = 1 @endphp
									@foreach($quiz_questions_answers as $quiz_question_answer)
										<div class="panel panel-default">
											<a data-toggle="collapse" data-parent="#accordion{{$no}}" href="#collapse{{$no}}">
									      <div class="panel-heading">
													<span>Pertanyaan No. {{$no}}</span>
									        <h4 class="panel-title">
									          {!!$quiz_question_answer['question']!!}
									        </h4>
									      </div>
											</a>
								      <div id="collapse{{$no}}" class="panel-collapse collapse">
								        <div class="panel-body">
													<table class="table">
														<tr>
															<td><span class="badge">{{$no}}</span></td>
															<td></td>
														</tr>
														<tr>
															<td>Tipe Kuis</td>
															<td>{{$quiz_question_answer['type']}}</td>
														</tr>
														<tr>
															<td>Pertanyaan</td>
															<td>{!!$quiz_question_answer['question']!!}</td>
														</tr>
														<tr>
															<td>Bobot</td>
															<td>{!!$quiz_question_answer['weight']!!}</td>
														</tr>
														<tr>
															<td>
																<a class="btn btn-warning btn-sm btn-raised" href="{{url('instructor/library/quiz/question/update/'.$quiz->id.'/'.$quiz_question_answer['id'])}}">Edit Pertanyaan</a>
																<a onclick="return confirm('Are You sure?')" class="btn btn-danger btn-sm btn-raised" href="{{url('library/quiz/question/delete/'.$quiz->id.'/'.$quiz_question_answer['id'])}}">Delete Pertanyaan</a>
															</td>
															<td></td>
														</tr>
														@foreach($quiz_question_answer['question_answers'] as $question_answer)
														<tr>
															<td>Jawaban</td>
															<td class="{{$question_answer->answer_correct == '1' ? 'success' : 'danger'}}">
																{{$question_answer->answer}} - {{$question_answer->answer_correct == '1' ? '(Benar)' : '(Salah)'}}
																<button type="button" class="btn btn-warning btn-sm btn-raised" id="edit_answer" data-id="{{$question_answer->id}}" data-answer="{{$question_answer->answer}}" data-answer-correct="{{$question_answer->answer_correct}}"><i class="fa fa-pencil"></i> Edit</button>
																&nbsp;
																<a class="btn btn-danger btn-sm btn-raised" onclick="return confirm('Are You sure?')" href="{{url('library/quiz/question/answer/delete/'.$quiz->id.'/'.$question_answer->id)}}"><i class="fa fa-trash"></i> Hapus</a>
															</td>
														</tr>
														@endforeach
													</table>
												</div>
								      </div>
								    </div>
									@php $no++ @endphp
									@endforeach
								</div>
								<a href="{{url('instructor/library')}}" class="btn btn-default btn-raised ">Kembali</a>
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">Edit Jawaban</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Jawaban</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">Status Jawaban ?</label>
						<select class="form-control" id="edit_text_answer_correct">
							<option value="0">Salah</option>
							<option value="1">Benar</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	        <button type="button" class="btn btn-primary" id="save_edit_answer">Simpan</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}

@endsection

@push('script')

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('library/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}

	{{-- upload file --}}

	<script type="text/javascript">
		$(document).ready(function(){
			console.log( "Ready, Document loaded!" );
			$("#file").on("change", function(e){
				document.forms["submit-file"].submit();
			})
		})
		
	</script>

	<script type="text/javascript">
		$("#upload-file").click(function(){
			document.getElementById('file').click();
			//document.forms["submit-file"].submit();
		})
	</script>

	{{-- upload file --}}

	{{-- participant list --}}
	<script src="{{url('js/jquery.dataTables.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script> <!-- Datatable -->
	<script type="text/javascript">
	  $(function(){
			var asset_url = '{{asset_url()}}';
	    $($("#data").DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: '{{ url("course/quiz/participant/serverside/".$quiz->id) }}',
	      columns: [
					{ data: 'name', name: 'name' },
	        { data: 'action', 'searchable': false, 'orderable':false }
	      ],
				"oLanguage": {
					"sLengthMenu": "Tampilkan _MENU_",
					"sSearch": "Cari: ",
		      "oPaginate": {
						"sPrevious": "<",
						"sNext": ">",
					}
		    },
	    }).table().container()).removeClass( 'form-inline' );
	  });
  </script>

	{{-- participant list --}}
@endpush
