@extends('layouts.app')

@section('title')
	{{ \Lang::get('front.content_create.title') . ' - ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.content_create.create') <span>@lang('front.content_create.content_title')</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
			<li><a href="/">@lang('front.content_create.home')</a></li>
			<li><a href="/course/dashboard">@lang('front.content_create.manage_course')</a></li>
			<li><a href="/course/preview/{{$section->id_course}}">@lang('front.content_create.preview')</a></li>
			<li>@lang('front.content_create.create') {{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title') : \Lang::get('front.content_create.content_title')}}</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="headline-sm headline-line" style="text-transform: capitalize;">@lang('front.content_create.create') @lang('front.content_create.content_title') @lang('front.content_create.scorm_title') - <span>{{$section->title}}</span></h3>
				</div>
				<div class="col-md-12">
					<!-- Default box -->
					<div class="box">
						<div class="box-body">
							<center>
								@if(Session::has('success'))
									<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
									</div>
								@elseif(Session::has('error'))
									<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
									</div>
								@endif
							</center>
							<div class="card-md">
							  <div class="card-block">
									<form action="/course/content/create/scorm/{{$section->id}}" method="POST" enctype="multipart/form-data">
										@csrf

										<div class="form-group">
											<label for="title">{{\Lang::get('front.content_create.content_title_name')}}</label>
											<input placeholder="{{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title_name') : \Lang::get('front.content_create.content_title_name')}}" type="text" class="form-control" name="title" value="" required>
											@if ($errors->has('title'))
												<span class="text-danger">{{ $errors->first('title') }}</span>
											@endif
										</div>
										
										<div class="form-group">
											<label for="">@lang('front.content_create.content_file_name')</label>
											<input type="file" name="files" accept=".zip,.rar,.7zip">
										</div>

										{{-- <div class="form-group" id="divDescription">
											<label for="title">@lang('front.content_create.description')</label>
											<textarea name="description" id="description"></textarea>
										</div> --}}

										<label for="is_description_show" style="color: black !important;">
											<input id="is_description_show" name="is_description_show" type="checkbox" value="1"> @lang('front.content_create.description_show')
										</label>

										<div class="form-group">
											<button type="submit" id="contentSave" class="btn btn-success btn-raised">@lang('front.content_create.save_and_publish')</button>
											<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">@lang('front.content_create.cancel')</a>
										</div>

									</form>
							  </div>
							</div>
						</div>
						<!-- /.box-body-->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')

@endpush