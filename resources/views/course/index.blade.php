@extends('layouts.app')

@section('title', Lang::get('front.all_course.title'))

@push('style')
	<style>
		.card-list {
			position: relative;
			display: inline-flex;
			width: 100%;
			background: #f9f9f9;
			margin-bottom: 1rem;
			transition: all 0.2s;
		}

		.card-list:hover {
			box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.05);
		}

		.card-list img {
			width: 250px;
			height: 150px; /* Default 170px */
			object-fit: cover;
		}

		.card-list .card-block {
			width: 100%;
			position: relative;
			/* border: 1px solid #f5f5f5; */
			padding: 1rem 2rem;
		}

		.card-list .card-title {
			font-size: 20px;
			font-weight: 600;
			color: #333;
			margin-top: 0rem;
			margin-bottom: 0.5rem;
		}

		.card-list .card-list-info.mobile {
			display: none;
		}

		.card-list .card-list-info.desktop {
			display: inline-flex;
		}

		.card-list .card-list-info {
			font-size: 14px;
			color: #424242;
		}

		.card-list .card-list-info p {
			margin-bottom: 0rem;
		}

		.card-list .card-list-info .rating i {
			color: #FFB800;
		}

		.card-list .card-list-info .category i {
			color: #079992;
		}

		.card-list .card-list-author {
			font-size: 14px;
			color: #424242;
			margin-bottom: 0rem;
		}

		.card-list .card-list-author i {
			color: #eb2f06;
		}

		.card-list .card-list-price {
			font-size: 14px;
			color: #424242;
		}

		.card-list .card-list-price span {
			font-size: 16px;
			font-weight: 600;
		}

		.card-list .card-list-enroll-button {
			font-size: 14px;
			font-weight: 600;
			border: 0px;
			border-radius: 5px;
			padding: 0.5rem 2rem;
			transition: all 0.2s;
			margin-left: 1rem;
		}

		.card-list .card-list-enroll-button.primary {
			background: #ff6347;
			color: #fff;
		}

		.card-list .card-list-enroll-button.secondary {
			background: #e6ca18;
			color: #333;
		}

		.card-list .card-list-enroll-button:hover {
			box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.05);
		}

		.category-filter {
			padding: 0; 
			margin: 0;
		}

		.category-filter li {
			list-style: none;
		}

		.category-filter li a {
			display: flex;
			align-items: center;
			width: 100%;
			cursor: pointer;
			margin-left: 1rem;
		}

		.category-filter li a:not([href]):not([tabindex]) {
			color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
			transition: 0.1s all ease-in-out;
		}

		.category-filter li a:not([href]):not([tabindex]):hover {
			color: #424242;
		}
		
		.category-filter li a i.zmdi.zmdi-chevron-down {
			margin-left: auto;
			margin-right: 0;
		}

		.category-filter li ul.category-first-level {
			display: none;
			padding: 0;
		}

		.category-filter li ul.category-first-level.active {
			display: block;
		}
		
		.category-filter li ul.category-first-level li {
			list-style: none;
		}
		
		.category-filter li ul.category-first-level li a {
			display: flex;
			align-items: center;
			width: 100%;
			margin-left: 0;
		}

		.category-filter li ul.category-first-level li ul.category-second-level {
			display: none;
			padding-left: 1.5rem;
		}

		.category-filter li ul.category-first-level li ul.category-second-level.active {
			display: block;
		}

		.category-filter li ul.category-first-level li ul.category-second-level li {
			list-style: none;
		}

		.category-filter li ul.category-first-level li ul.category-second-level li a {
			display: block;
		}

		@media (max-width: 767px){
			.card-list {
				padding: 0;
			}
			
      .card-list img{
        width: 150px;
        height: 100px;
        object-fit: cover;
      }

			.card-list .card-block{
				padding: 1rem;
			}

			.card-list .card-title{
				font-size: 18px;
				overflow: hidden;
				min-height: 20px;
				max-height: 50px;
				display: -webkit-box !important;
				text-overflow: ellipsis;
				-webkit-box-orient: vertical;
				-webkit-line-clamp: 1;
				white-space: normal;
				margin: 0;
			}

			.card-list .card-list-info.desktop{
				display: none;
			}

			.card-list .card-list-info.mobile{
				display: block;
				font-size: 12px;
			}

			.card-list .card-list-date{
				display: none;
			}

			.card-list .card-list-author{
				display: none;
				font-size: 12px;
			}

			.card-list .card-list-author i{
				color: #eb2f06;
			}

			.card-list .card-list-price{
				font-size: 12px;
				position: relative;
			}

			.card-list .card-list-enroll-button{
				font-size: 12px;
				padding: 0.15rem 1rem;
			}
		
			.card-list .card-list-enroll-button:hover{
				box-shadow: none;
			}
    }
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.all_course.header')</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.all_course.breadcrumb_home')</a></li>
          <li>@lang('front.all_course.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
	</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div id="mobile" class="col-md-3">
          <div class="row">
            <div class="col-md-12">
              <h3 class="headline-sm headline-line mt-0">@lang('front.all_course.filter_by_category')</h3>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
				{{-- <a href="#">Kategori Dropdown 1</a> --}}
				<ul class="category-filter">
					@php $Categories = DB::table('categories')->where('parent_category', '0')->get(); @endphp
					@foreach($Categories as $index => $Category)
						<li>
							@php $ChildCategories = DB::table('categories')->where('parent_category', $Category->id)->get(); @endphp
							@php $dataParentRequests = []; @endphp
							@php
								if (Request::get('parent')) {
									$dataParentRequests = explode('-', Request::get('parent'));
								}
							@endphp
							@if(count($ChildCategories) > 0)
								<div class="d-flex align-items-center parentCategory">
									<input type="checkbox" {{in_array($Category->id, $dataParentRequests) ? 'checked' : ''}} class="parentCategoryCheck" id="parentCategoryCheck{{$Category->id}}" value="{{$Category->id}}">													
									<a>{{$Category->title}} <i class="zmdi zmdi-chevron-down"></i></a>
								</div>
							@else
								<div class="d-flex align-items-center">
									<input type="checkbox" {{in_array($Category->id, $dataParentRequests) ? 'checked' : ''}} class="parentCategoryCheck" id="parentCategoryCheck{{$Category->id}}" value="{{$Category->id}}">                        
									<a>{{$Category->title}}</a>
								</div>
							@endif
							<ul class="category-first-level">
								@foreach($ChildCategories as $data)
									<li>
										<div class="d-flex align-items-center childrenCategory" style="margin-left: 8px !important;">
											<input {{ !Request::get('child') ? (in_array($data->parent_category, $dataParentRequests) ? 'checked' : '') : (in_array($data->id, explode('-', Request::get('child'))) ? 'checked' : '') }} type="checkbox" class="childCategoryCheck" id="childCategoryCheck{{$data->id}}" value="{{$data->id}}">	                            
											<a style="cursor: pointer;"><i style="margin-right: 1rem;"></i> {{$data->title}}</a>
										</div>
									</li>
								@endforeach
							</ul>
						</li>
					@endforeach
				</ul>
			</div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-12">
              @foreach($courses as $course)
                <div class="card-list">
                  <a href="{{url('course/'. $course->slug)}}">
                    {{-- <div style="position: absolute; line-height: 1; top: 1rem; left: 1rem;"> --}}
                      {{-- <span class="badge badge-success">E-Learning</span> --}}
                      {{-- <span class="badge badge-danger ">Private</span> --}}
                    {{-- </div> --}}
                    <img src="{{$course->image}}" alt="{{ $course->title }}">
                  </a>
                  <div class="card-block" style="width: 70%;">
                    <a href="{{url('course/'. $course->slug)}}">
                      <h3 class="card-title text-truncate">{{ $course->title }}</h3>
                                
                      <div class="card-list-info desktop" style="width: 100%;">
                        <p class="rating">
                          <i class="fa fa-star color-warning"></i> 5.0 (10 @lang('front.all_course.rating_text'))
                        </p>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <p class="category text-truncate" style="width: 60.5%;">
                          <i class="fa fa-tag"></i> @lang('front.all_course.course_category'): <b>{{$course->category}}</b>
                        </p>
                      </div>
                    
                      <div class="card-list-info mobile" style="width: 100%;">
                        <p class="rating">
                          <i class="fa fa-star color-warning"></i> 5.0 (10 @lang('front.all_course.rating_text'))
                        </p>
                      </div>
                    
                      <p class="card-list-author text-truncate" style="width: 100%;">
                        <i class="fa fa-user"></i> {{ $course->name }}
                      </p>
                    </a>
                    <div style="display: flex; align-items: center; justify-content: flex-end;">
                      <a href="{{url('course/'. $course->slug)}}">
                        <div class="card-list-price"><span>{{$course->price == 0 ? '' : "" . number_format($course->price, 0, '.', '.')}}</span></div>												
                      </a>
                      <a href="{{url('course/'. $course->slug)}}" class="card-list-enroll-button secondary">@lang('front.all_course.enroll_button')</a>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
		  </div>
        </div>
      </div>
    </div>

    <!--Pagination Wrap Start-->
    <div class="text-center mt-5">
      {{$courses->appends(request()->input())->links('pagination.default')}}
    </div>
    <!--Pagination Wrap End-->
  </div>
@endsection

@push('script')
	<script>
		$(document).ready(function(){
			$('.category-filter .parentCategory').on("click", function(e){
				$(this).next('.category-first-level').toggleClass('active');
				e.stopPropagation();
			});
			$('.category-first-level .childrenCategory').on("click", function(e){
				$(this).next('.category-second-level').toggleClass('active');
				e.stopPropagation();
			});
		});
	</script>

	<script type="text/javascript">
		$(".parentCategoryCheck").on("click", function() {
			var value = $(this).attr('value');
			var state = $("#parentCategoryCheck" + value).is(":checked")

			var reqParent = "{{Request::get('parent')}}";
			if (reqParent != '') {
				var newReqParent = reqParent.split('-');
				if(jQuery.inArray(value, newReqParent) !== -1) {
					newReqParent.splice($.inArray(value, newReqParent), 1);
				} else {
					newReqParent.push(value);
				}
				newReqParent = newReqParent.join('-');
				window.location.href = "/courses?parent=" + newReqParent + "{!! Request::get('child') ? '&child=' . Request::get('child') : '' !!}";
			} else {
				window.location.href = "/courses?parent=" + value + "{!! Request::get('child') ? '&child=' . Request::get('child') : '' !!}";
			}
		});

		$(".childCategoryCheck").on("click", function() {
			var value = $(this).attr('value');
			var state = $("#childCategoryCheck" + value).is(":checked")

			var reqChild = "{{Request::get('child')}}";
			if (reqChild != '') {
				var newReqChild = reqChild.split('-');
				if(jQuery.inArray(value, newReqChild) !== -1) {
					newReqChild.splice($.inArray(value, newReqChild), 1);
				} else {
					newReqChild.push(value);
				}
				newReqChild = newReqChild.join('-');
				window.location.href = "/courses?child=" + newReqChild + "{!! Request::get('parent') ? '&parent=' . Request::get('parent') : '' !!}";
			} else {
				window.location.href = "/courses?child=" + value + "{!! Request::get('parent') ? '&parent=' . Request::get('parent') : '' !!}";
			}

		});
	</script>
@endpush