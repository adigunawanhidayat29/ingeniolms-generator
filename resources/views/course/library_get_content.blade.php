@extends('layouts.app')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	
@endpush

@section('content')
<div class="Container">
	<h2 style="margin-left:10px">Pilih Konten</h2>
	<hr />
	<noscript>You need to enable JavaScript to run this app.</noscript>
    <input type="hidden" id="section_id_val" value="{{$section_id}}" />
	<input type="hidden" id="type_content" value="{{Request::get('content')}}" />
	<div id="root"></div>
	<!-- <script src="js/app.js"></script> -->
	<script type="text/javascript" src="{{url('js/helper.js')}}"></script>
	{{-- <script type="text/javascript" src="{{url('js/library.pick.js')}}"></script> --}}
	<script type="text/javascript" src="{{url('js/global.134340.min.js')}}"></script> 
</div>
@endsection

@push('script')
    <script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript">
		// Custom example logic
		// var path_file = $("#path_file").val();
		// var name_file = $("#name_file").val();
		// var file_size = $("#file_size").val();
		// var video_duration = $("#video_duration").val();
		// var full_path_original = $("#full_path_original").val();
		// var resize_path = $("#resize_path").val();
        // var type_content = $("#type_content").val();
		// $("#saveContent").prop('disabled', true);
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'file', // you can pass an id...
			// container: document.getElementById('container'), // ... or DOM Element itself
			url : '/upload/file/{{Auth::user()->id}}',
			flash_swf_url : '/plupload/Moxie.swf',
			silverlight_xap_url : '/plupload/Moxie.xap',
			chunk_size: '1mb',
			dragdrop: true,
			headers: {
				'X-CSRF-TOKEN': '{{csrf_token()}}'
			},
			filters : {
				mime_types: [
					{title : "Files", extensions : "docx,doc,xlsx,pptx,pdf,xls,txt,mp4,webm"},
				]
			},

			init: {
				PostInit: function() {
					document.getElementById('filelist').innerHTML = '';
				},

				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						// document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						// $("#divTitle").show()
						// $("#divDescription").show()
						// $("#contentTitle").val(file.name)
						// $("#contentSave").show()
						//$('#progressDiv').css('width', 240 + 'px');
						$("#btnstart").click();
						uploader.start();
						// $("#contentSave").prop('disabled', true);
						return false;
					});
				},

				UploadProgress: function(up, file) {
					// document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
					// $('.progress').show();
					if (file.percent < 99) {
						$('#progressDiv').css('width', ((file.percent / 100)*240) + 'px');
					}
					//$("#uploadedMessage").text('Upload masih dalam proses...');
					//$("#btnrefresh").click();
					// $("#btnrefprogress").text(file.percent+'');
					

					// $("#uploadedMessage").addClass('alert alert-info');
				},

				FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
                        console.log('[FileUploaded] File:', file, "Info:", info);
						var response = JSON.parse(info.response);
						console.log('Original Response: '+JSON.stringify(response.result.original));
						// path_file = $("#path_file").val(response.result.original.path);
						// name_file = $("#name_file").val(response.result.original.name);
						// file_size = $("#file_size").val(response.result.original.size);
						// video_duration = $("#video_duration").val(response.result.original.video_duration);
						// full_path_original = $("#full_path_original").val(response.result.original.full_path_original);
						// resize_path = $("#resize_path").val(response.result.original.resize_path);
                        // type_content = $("#type_content").val(response.result.original.type_content);

						$.ajax({
							url: '/api/upload/file/store-data',
							type: 'POST',
							data: {
								//_token : '{{csrf_token()}}',
								file: true,
								path_file: response.result.original.path,
								name_file: response.result.original.name,
								file_size: response.result.original.size,
								video_duration: response.result.original.video_duration,
								full_path_original: response.result.original.full_path_original,
								resize_path: response.result.original.resize_path,
								type_content: response.result.original.type_content,
                                user_id: document.head.querySelector('meta[name="host_id"]').content,
                                original_file_name: response.result.original.original_file_name,
                                parent: document.head.querySelector('meta[name="app-parents"]').content
							},
							success: function(responses){
								// alert('saved draft');
                                //alert('saved draft');
                                //console.log('Ini response Kenapa gk muncul?: '+JSON.stringify(responses));
								//$("#contentId").val(response.id)
                                //window.location.reload()
                                //document.location.replace("{{url('instructor/library'.empty(Request::get('parents')) ? '' : '?parents='.Request::get('parents'))}}");
							}
						})

        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
						// $("#saveContent").prop('disabled', false);
						// console.log(up);
						// $('#progressBar').attr('aria-valuenow', 100).css('width', 100 + '%').text(100 + '%');
						$('#progressDiv').css('width', 0 + 'px');
						$("#uploadedMessage").text('Upload selesai');
						// $("#uploadedMessage").addClass('alert alert-success');
						// $("#contentSave").prop('disabled', false);
						$("#btnrefresh").click();
        },

				Error: function(up, err) {
					// console.log(up);
					console.log(err);
					// document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
					if(err.status == 500 || err.code == "-200"){
						$("#uploadedMessage").text('Upload Gagal');
						// $("#uploadedMessageError").html('Upload Gagal');
						// $("#uploadedMessageError").addClass('alert alert-danger');
						console.log('Upload Gagal');
					}
				},

				Destroy : function(){
					console.log('error');
				}
			}
		});

		uploader.init();
	</script>
@endpush