@extends('layouts.app_nation')

@section('title', 'Choose Question from Quiz')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	
@endpush

@section('content')
	<noscript>You need to enable JavaScript to run this app.</noscript>
	<div id="root"></div>
	<!-- <script src="js/app.js"></script> -->
    <input type="hidden" id="quiz_id" value="{{$id}}" />
    <input type="hidden" id="target" value="{{$target}}" />
	<script type="text/javascript" src="{{url('js/helper.js')}}"></script>
	{{-- <script type="text/javascript" src="{{url('js/library.sub_question.js')}}"></script> --}}
	<script type="text/javascript" src="{{url('js/global.134340.min.js')}}"></script> 
@endsection