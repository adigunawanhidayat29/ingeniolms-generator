
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="CoffeeCup HTML Editor (www.coffeecup.com)">
    <meta name="dcterms.created" content="Wed, 12 Jun 2019 03:20:44 GMT">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Grade Book</title>
	<!-- Font Awesome 4-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <style>
body{
  font-family: Arial, 'Sans-serif';
  background: #f2f2f2;
 }	
	
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th, .export-btn {
  background-color: #81BD5D;
  color: #fff;
}
.export-btn{
  padding: 10px 16px;
  border: 0;
  outline: 0;
  font-size: 18px;
  border-radius: 4px;
  margin: 10px auto;
  cursor: pointer;
  
 }
.export-btn:hover{
  opacity: 0.9;

 }
 main{
  background: #fff;
  }
 
 h1{
  text-align:center;
  color: #313131;
  font-size: 24px;
 
  }

@media only screen and (min-width: 720px){
   main{
     min-width: 600px;
	 margin: 10px auto;
	 padding: 16px;
	 box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.4);
    }

 }
</style>

  </head>
  <body>
  
  <main>
  <h1>Grade Book "{{$Course->title}}"</h1>

  <table id="tableCompany">
  <tr>
    <th>No</th>
    <th>Name</th>
    @foreach($quizzes as $quiz)
      @php
        $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
      @endphp
      <th>
        {{$quiz->name}} (bobot {{isset($quiz_grade_setting) ? $quiz_grade_setting->percentage : '0'}}%)
      </th>
    @endforeach
    @foreach($assignments as $assignment)
      @php
        $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();                    
      @endphp
      <th>
        {{$assignment->title}} (bobot {{isset($assignment_grade_setting) ? $assignment_grade_setting->percentage : '0'}}%)
      </th>
    @endforeach
    {{-- <th>Completion</th> --}}
    <th>Total</th>
  </tr>

  @foreach($course_users as $index => $course_user)

    <tr>
      <td>{{$index +1}}</td>
      <td>
        {{$course_user->name}}
      </td>

      @php
        $grade_total = 0 ;
        $grade_percent_total = 0 ;
      @endphp

      @foreach($quizzes as $quiz)

        @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_user->user_id, 'quiz_id' => $quiz->id])->first() @endphp

        @php
          $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
          if(!$quiz_grade_setting){
            $quiz_grade_setting_percent = 100;
          }else{
            $quiz_grade_setting_percent = $quiz_grade_setting->percentage;
          }
        @endphp

        @if($quiz_participant)
          @php
            $grade = $quiz_participant->grade
          @endphp
          <td>{{$grade}}</td>
        @else
          @php $grade = 0 @endphp
          <td>-</td>
        @endif

        @php $grade_percent_total += $grade * $quiz_grade_setting_percent / 100  @endphp
        @php $grade_total += $grade @endphp
      @endforeach

      @foreach($assignments as $assignment)

        @php
          $assignments_answers = \DB::table('assignments_answers')
            ->where(['assignments_answers.user_id' => $course_user->user_id, 'assignments_answers.assignment_id' => $assignment->id])
            ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
            ->first()
        @endphp

        @php
          $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
          if(!$assignment_grade_setting){
            $assignment_grade_setting_percent = 100;
          }else{
            $assignment_grade_setting_percent = $quiz_grade_setting->percentage ?? 0;
          }
        @endphp

        @if($assignments_answers)
          @php $grade_assignment = $assignments_answers->grade @endphp
          <td>{{$grade_assignment}}</td>
        @else
          @php
            $grade_assignment = 0 ;
          @endphp
          <td>-</td>
        @endif

        @php $grade_percent_total += $grade_assignment * $assignment_grade_setting_percent / 100  @endphp
        @php $grade_total += $grade_assignment @endphp

      @endforeach
      
      @php 
        $totalFinish = ($grade_percent_total) / (count($quizzes) + count($assignments));
      @endphp
      <td>{{ round($totalFinish, 2)}}</td>
    </tr>
  @endforeach

</table>

 <button class="export-btn"><i class="fa fa-file-excel-o"></i> Export to Excel</button>

 </main>
 
  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="/js/tableHTMLExport.js"></script>
  <script>
  $(document).ready(function(){
  
   $(".export-btn").click(function(){
    
     $("#tableCompany").tableHTMLExport({
	    type:'csv',
		filename:'{{$Course->title . "-". date("Y-m-d")}}.csv',
		});
 
   });
  
  });
  
  </script>

  </body>
</html>