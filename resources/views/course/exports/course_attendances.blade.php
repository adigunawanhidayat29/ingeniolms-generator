<table>
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Hadir (p)</th>
      <th>Terlambat (l)</th>
      <th>Berhalangan (e)</th>
      <th>Absen (a)</th>
      <th>Poin</th>
      <th>Persentase</th>
      @foreach($CourseAttendancesSummary as $index => $data)
      <th>{{$data->description}}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    @php $summary = [] @endphp
    @foreach ($reports as $index => $item)
      <tr>
        <td>{{$index + 1}}</td>
        <td>{{$item->user->name}}</td>
        <td>{{$item->sum_status_p}}</td>
        <td>{{$item->sum_status_l}}</td>
        <td>{{$item->sum_status_e}}</td>
        <td>{{$item->sum_status_a}}</td>
        <td>{{($item->sum_status_p + $item->sum_status_l)}} / {{$item->count_attendances}}</td>
        <td>{{ round( (($item->sum_status_p + $item->sum_status_l) / $item->count_attendances) * 100) }} %</td>
        @foreach($CourseAttendancesSummary as $data)
          @php 
            $get_status = DB::table('course_attendance_users')->where(['user_id' => $item->user->id, 'course_attendances_id' => $data->course_attendances_id])->first();
          @endphp
          <td>{{$get_status ? $get_status->status : '?'}} {{$get_status && $get_status->remarks != null ? '(' . $get_status->remarks . ')' : ''}}</td>
        @endforeach
      </tr>	
    @endforeach
    <tr>
      <td colspan="8">Ringkasan</td>
      @foreach($CourseAttendancesSummary as $index => $data)
        <td>
          <span>P: {{$data->sum_status_p}}</span> <br>
          <span>L: {{$data->sum_status_l}}</span> <br>
          <span>E: {{$data->sum_status_e}}</span> <br>
          <span>A: {{$data->sum_status_a}}</span>
        </td>
      @endforeach
    </tr>
  </tbody>
</table>