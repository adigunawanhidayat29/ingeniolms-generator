@extends('layouts.app')

@section('title', $course->title)

@push('meta')
<meta name="description" content="{{$course->subtitle}}. {{strip_tags($course->description)}}" />
<meta name="keywords" content="elearning {{$course->title}}, ecourse {{$course->title}}, training {{$course->title}}, kelas {{$course->title}}" />

<meta property="og:type" content="article"/>
<meta property="og:title" content="{{$course->title}}"/>
<meta property="og:description" content="{{strip_tags($course->description)}}"/>
<meta property="og:image" content="{{asset_url(course_image_medium($course->image))}}"/>
<meta property="og:url" content="{{url('course/' . $course->slug )}}"/>
<meta property="og:site_name" content="Ingenio"/>
<meta property="article:publisher" content="https://www.facebook.com/ingeniocoid/"/>
<meta property="og:image:width" content="640"/>
<meta property="og:image:height" content="424"/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{$course->title}}">
<meta name="twitter:description" content="{{strip_tags($course->description)}}">
<meta name="twitter:image" content="{{asset_url(course_image_medium($course->image))}}">
<meta name="twitter:site" content="@ingeniocoid">
@endpush

@push('style')
  <style media="screen">
    .ms-navbar{
      margin-bottom: 0;
    }
    .top-title-bg{
      /* background: #192a56; */
      background: #2b51a2;
      min-height: 150px;
      max-height: 350px;
      padding: 3% 3% 6% 3%;
    }
      .top-title-bg .mobile{
        display: none;
      }
      .top-title-bg .desktop{
        display: block;
      }
    .dq-description-max{
      overflow: hidden;
      min-height: 20px;
      max-height: 300px;
      display: -webkit-box!important;
      -webkit-box-orient: vertical;
      white-space: normal;
      margin:0;
    }
    .mt-20{
      margin-top: 20rem;
    }
    @media (max-width:768px){
      .top-title-bg{
        background: #2b51a2;
        max-height: none;
        padding: 3rem 0;
      }
        .top-title-bg .mobile h2{
          display: none;
        }
        .top-title-bg .mobile{
          display: inline-flex;
          position: fixed;
          background: #2b51a2;
          text-align: left !important;
          bottom: 0;
          z-index: 3;
        }
        .top-title-bg .desktop{
          display: none;
        }

      .mobileHide{
        display: none !important;
      }

      .center{
        text-align: center !important;
      }
      .mt-20{
        margin-top: auto !important;
      }
    }
  </style>

  <style media="screen">
    .play-preview{
      color: #dcdde1;
      font-size:95px;
      padding: 10px;
    }
    .play-preview:hover{
      color: #f5f6fa;
      font-size:100px;
    }

    .btn-preview{
      position: absolute;
      width: 80%;
      height: auto;
      text-align: center;
      z-index: 30;
    }
    /*
      .btn-preview .frame{
        width: 120px;
        height: 120px;
        background: #00000073;
        padding: auto;
        opacity: 1;
        border: 0;
        border-radius: 100%;
        line-height: 11;
        transition: 0.2s ease;
        z-index: 29
      }
      .btn-preview .frame:hover{
        width: 150px;
        height: 150px;
      }
      */
      .btn-preview i{
        background: #0000009c;
        width: 100px;
        height: 100px;
        padding: 25px;
        border-radius: 100%;
        opacity: 1;
        margin-top: 10%;
        font-size: 50px;
        transition: 0.1s;
        z-index: 28;
      }
      .btn-preview i:hover{
        font-size: 60px;
        width: 110px;
        height: 110px;
      }

      #desktop{
        display: block;
      }
      #mobile{
        display: none;
      }
      @media (max-width:767px){
        #desktop{
          display: none;
        }
        #mobile{
          display: block;
        }
        .detailSide{
          display: none;
        }

        .col-md-6 p{
          margin: 0;
        }
      }
  </style>
  <style media="screen">
    .course-detail span, .course-detail p{
      font-family: Roboto,sans-serif !important;
      font-size: 15px !important;
      color: #424242 !important;
    }
  </style>

  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">

  <link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>
  <style media="screen">
    .responsips div{
      padding: 0.25rem;
    }
    .responsips img{
      width: 100%;
    }
    .responsips button{
      background: #fff;
      opacity: 0.5;
      transition: all 0.3s;
    }
    .responsips button:hover{
      background: #fff;
      opacity: 0.8;
    }
      .responsips .slick-prev:before, .responsips .slick-next:before{
        color: #3f3f3f;
        font-size: 75px;
        transition: all 0.3s;
      }
      .responsips .slick-prev,.responsips .slick-next{
        width: 50px;
        height: 100%;
      }
      .responsips .slick-prev:hover:before,.responsips .slick-next:hover:before{
        color: #000 !important;
        /* color: #fff !important; */
        /* background: #333; */
      }
      .responsips .slick-prev{
        content: "\f053" !important;
        left: 0px;
        z-index: 1;
      }
      .responsips .slick-next{
        right: 0px;
        z-index: 1;
      }

    .item_wall{
      width: 100%;
      padding-left: 15px;
      padding-right: 15px;
      transform: translateY(-50%);
    }
    .item{
      display: inline-flex;
      width: 100%;
    }
      .item .item_text{
        display: block;
        width: 25%;
        padding: 1rem 2rem;
      }
      .item .item_text h2{
        text-transform: uppercase;
        font-size: 14px;
        font-weight: 300;
        margin: 0;
        transform: translateX(20%);
      }
      .item .item_text p{
        font-size: 18px;
        font-weight: 500;
        margin: 0;
        transform: translateX(20%);
      }
      .item .item_separator{
        display: block;
        width: 1px;
        /* width: 1rem; */
        height: 30px;
        background: #ccc;
        align-self: center;
      }
    @media only screen and (max-width: 414px){
      .item_wall{
        transform: translateY(0);
        padding: 0;
      }
      .item{
        width: 100%;
        display: block;
      }
        .item .item_text{
          width: 100%;
        }
        .item .item_text h2, .item .item_text p{
          transform: translateX(0);
          display: inline-block;
        }
          .item .item_text h2:after{
            content: ' :';
          }
        .item .item_separator{
          display: none;
        }
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Semua Kategori</a></li>
          <li>{{$course->category}}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="top-title-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h1 class="headline-lg color-white mt-0"><span>{{$course->title}}</span></h1>
          <p class="color-white fs-18">{{$course->subtitle}}</p>
          <span class="">
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <span style="color:white;padding: 0 7px;">5.0 (1 penilaian)</span>
          </span>
          <span style="color:white; padding: 0 7px;">{{$course->category}}</span>
          <span style="color:white; padding: 0 7px;">{{$course->level}}</span>
          <br>
          <span style="color:white;">Pengajar <a style="color:white;" href="/user/{{$course->author_slug}}"><strong>{{$course->author}}</strong></a></span>
          {{-- <li><strong>Pengajar:</strong> {{$course->author}}</li> --}}
          {{-- <li><strong>Kategori:</strong> {{$course->category}}</li>
          <li><strong>Level:</strong> {{$course->level}}</li> --}}
        </div>
        <div class="col-md-4 text-right desktop">
          <h2 class="headline-md color-white no-m">Smarter ID</h2>
          <button class="btn btn-lg btn-raised btn-primary" type="button" name="button">Join Program</button>
        </div>
        <div class="col-md-4 text-right mobile" id="joinBtn">
          <h2 class="headline-md color-white no-m">Smarter ID</h2>
          <button class="btn btn-lg btn-raised btn-primary" type="button" name="button">Join Program</button>
        </div>
        {{--
        <div id="mobile" class="col-md-4">
          <div class="card-md">
            <div class="card-block">
              @if($content_previews)
                <a id="previewContent" data-id="{{$content_previews[0]['id']}}" href="#/">
                  <div class="btn-preview">
                    <i class="fa fa-play color-white"></i>
                  </div>
                  <img src="{{asset_url(course_image_small($course->image))}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
                </a>
              @else
                <img src="{{asset_url(course_image_small($course->image))}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
              @endif
              @if($course->archive != '1')
                <h3 class="text-center color-dark" style="font-family:sans-serif;font-weight:600;">{{$course->price == 0 ? 'Gratis' : "Harga: " . rupiah($course->price)}}</h3>
                @php
                  if(Request::get('aff')) {
                    $affiliate = '?aff='.Request::get('aff');
                  }else{
                    $affiliate = '';
                  }
                @endphp
                @if($course->price === 0)
                  {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                    <input type="hidden" name="id" value="{{$course->id}}">
                    <input type="hidden" name="slug" value="{{$course->slug}}">
                    <button type="submit" class="btn btn-success btn-raised btn-block">Enroll Sekarang</button>
                  {{Form::close()}}
                @else
                  @if($CourseOnCart === false)
                    {{Form::open(['url' => 'cart/buy/'.$course->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-success btn-raised btn-block">Beli</button>
                    {{Form::close()}}
                  @endif
                  @if($CourseOnCart === false)
                    {{Form::open(['url' => 'cart/add/'.$course->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-primary btn-raised btn-block ">Tambahkan keranjang</button>
                    {{Form::close()}}
                  @else
                    <a href="{{url('cart')}}" class="btn btn-primary btn-raised btn-block">Lihat Keranjang</a>
                  @endif
                  @if($course->password)
                    <a href="#" data-target="{{Auth::check() ? '#CourseCheckPassword' : '#ms-account-modal'}}" data-toggle="modal" class="btn btn-info btn-raised btn-block">Enroll Dengan Password</a>
                  @endif
                @endif
              @endif
              <div class="color-dark">
                <b>Termasuk:</b>
                <ul>
                  <li>Total Durasi video <span id="video_durations"></span> </li>
                  <li>Akses seumur hidup</li>
                  <li>Akses di seluler</li>
                </ul>
              </div>
              <hr>
              <div class="text-center">
                <p>Bagikan:</p>
                <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
                <a target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
                @if(Auth::check())
                  @if(isAffiliate(Auth::user()->id) != false)
                    <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy" data-clipboard-text="{{url('course/' . $course->slug ) . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
                  @endif
                @endif
              </div>
            </div>
          </div>
        </div>
        --}}
      </div>
    </div>
  </div>

  <div class="wrap pt-0 pb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="item_wall">
          <div class="card-md radius-0 border-0" style="background: linear-gradient(180deg, #f3f3f3 0%, #ffffff 100%);">
            <div class="card-block">
              <div class="item">
                <div class="item_text">
                  <h2>Kelas</h2>
                  <p>5</p>
                </div>
                <div class="item_separator"></div>
                <div class="item_text">
                  <h2>Durasi</h2>
                  <p>29 minggu</p>
                </div>
                <div class="item_separator"></div>
                <div class="item_text">
                  <h2>Investasi</h2>
                  <p>Gratis</p>
                </div>
                <div class="item_separator"></div>
                <div class="item_text">
                  <h2>Penyelenggara</h2>
                  <p>Smarter ID</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- CONTENT -->
  <div class="wrap pt-2 pb-2 bg-white mb-2 color-dark" id="descriptionContent" style="min-height: 700px;">
    <div class="container w-800">
      <div class="row">
        <div class="col-md-12">
          <!-- DESKRIPSI -->
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>Deskripsi</span></h2>
            </div>
          </div>
          <div class="mb-2">
            <div id="description" class="course-detail">
              <p>{!! $course->description !!}</p>
            </div>
            <a class="btn-more" id="more-deskripsi">+ Selengkapnya</a>
          </div>

          <!-- TUJUAN -->
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>Tujuan</span></h2>
            </div>
          </div>
          <div class="mb-2">
            <div id="target" class="course-detail">
              <p>{!! $course->goal !!}</p>
            </div>
            <a class="btn-more" id="more-tujuan">+ Selengkapnya</a>
          </div>

          <!-- SKILL -->
          <div class="card-md mb-2 radius-0">
            <div class="card-block">
              <h2 class="headline-sm mt-0"><span class="fw-400">What skills will I learn?</span></h2>
              <div class="">
                <p>Designed for business professionals with 3+ years experience, who are seeking to step up to senior management roles and considering MBA-level study to advance their careers, this program will enable you to</p>
                <ul>
                  <li>recognise opportunities to reduce costs and create value through better supply chain management;</li>
                  <li>use market segmentation to improve your organisation’s performance;</li>
                  <li>improve sales from your interactions with customers, existing and new;</li>
                  <li>evaluate funding opportunities for business growth and development;</li>
                  <li>improve your critical, analytical, planning and prioritising skills;</li>
                  <li>and develop your ethical awareness as a manager.</li>
                </ul>
              </div>
            </div>
          </div>

          <!-- KELAS -->
          <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
            <div class="row">
              <div class="col-md-12">
                <h2 class="headline-md headline-line">Kelas untuk <span>program ini</span></h2>
              </div>
            </div>
            <?php $i=1; $video_durations = 0 ?>

          </div>
          <div class="responsips">
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 1</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="http://assets.ingenio.co.id//uploads/courses/small_default.jpg" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 2</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="http://assets.ingenio.co.id//uploads/courses/small_default.jpg" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 3</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 4</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 5</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 6</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 7</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
            <div class="card-sm">
              <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
              <img class="dq-frame-square" src="/img/imgdefault.png" data-lazy="" alt="">

              <div class="card-block text-left">
                <h4 class="headline-xs dq-max-title">kelas 8</h4>
                <p class="section-footer mb-1">
                  <span><i class="fa fa-hourglass-half"></i> 4 weeks</span>
                  <span><i class="fa fa-clock-o"></i> 3 hours</span>
                </p>
                <a class="btn btn-sm btn-block btn-raised btn-primary no-m" href="#">Ikuti kelas</a>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal" id="CourseCheckPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id="CoursePassword" placeholder="Masukan Password">
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button id="CheckPassword" type="button" class="btn  btn-primary">Enroll</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->

  <!-- preview konten -->
  <div class="modal" id="modalPreviewContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content" style="background:#2f3640">
              <div class="modal-header">
                  <h3 class="modal-title" id="previewTitle" style="color: white;"></h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div id="previewKonten" style="min-height:300px; color:white !important"></div>
                <div class="">
                  <br>
                  <p style="color: white; font-size:16px">Gratis sampel video:</p>
                  <ul class="list-group" style="list-style:none">
                    @foreach ($content_previews as $key => $content_preview)
                      <a id="previewContent" data-id="{{$content_preview['id']}}" href="#/">
                        <li class="list-group-item" style="list-style:none">
                          <p class="color-dark">
                            <i class="{{content_type_icon($content_preview['type_content'])}}"></i> {{$content_preview['title']}}
                          </p>
                        </li>
                      </a>
                    @endforeach
                  </ul>
                </div>
              </div>
          </div>
      </div>
  </div>
  <!-- preview konten -->
@endsection

@push('script')
  <script src="/videojs/ie8/videojs-ie8.min.js"></script>
  <script src="/videojs/video.js"></script>
  <script src="/videojs/videojs-markers.min.js"></script>
  <script src="/videojs/silvermine-videojs-quality-selector.min.js"></script>
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('clipboard/clipboard.min.js')}}"></script>

  <script type="text/javascript" src="/slick/slick.min.js"></script>
  <script type="text/javascript">
    $('.responsips').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 2.9,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2.25,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            // slidesToShow: 2.2,
            slidesToShow: 1.85,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  </script>

	<script type="text/javascript">
		var clipboard = new ClipboardJS('.btn-copy');
		clipboard.on('success', function(e) {
	    swal("Berhasil!", "Silakan bagikan link ini untuk referal Anda: " + e.text, "success");
	    e.clearSelection();
		});
	</script>

  <script type="text/javascript">
    $("#video_durations").html('{{$video_durations >= 3600 ? gmdate("H", $video_durations) . ' jam' : gmdate("i", $video_durations) . ' menit'}}')
  </script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/course/check-password/{{$course->id}}',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          if(response === 'true'){
            $.ajax({
              url : '/course/enroll',
              type : 'get',
              data : {
                id : {{$course->id}},
                _token : '{{csrf_token()}}',
              },
              success : function(data){
                window.location.assign('/course/learn/{{$course->slug}}')
              }
            })
          }else{
            alert('Password salah');
          }
        }
      })
    })
  </script>
  <script type="text/javascript">
    if($('#description').height() > 300){
      $('#description').addClass('dq-description-max');
      $('#more-deskripsi').addClass('show');
    }
    if($('#target').height() > 300){
      $('#target').addClass('dq-description-max');
      $('#more-tujuan').addClass('show');
    }

    $('#more-deskripsi').click(function(){
      $('#description').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-deskripsi').show();
    });
    $('#more-tujuan').click(function(){
      $('#target').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-tujuan').show();
    });
  </script>

  <script type="text/javascript">
    $(function(){
      $(document).on('click', '#previewContent', function(){
        var content_id = $(this).attr('data-id');
        // alert(content_id);

        $.ajax({
          url : '/course/content',
          type : 'post',
          data : {
            'id' : content_id,
            '_token' : '{{csrf_token()}}'
          },
          beforeSend: function(){
            $("#modalPreviewContent").modal('show');
            $("#previewKonten").html('Mohon tunggu...');
          },
          success : function(result){
            // console.log(result);
            $("#previewTitle").html(result.title);

            if(result.type_content == 'video'){
              $("#previewKonten").html('<video id="my-video" class="video-js vjs-default-skin vjs-16-9 vjs-big-play-centered" controls="true" autoplay="true" preload="auto" width="100%" height="500"><source src="'+result.content+'" type="video/mp4" selected="true" label="360p"></video>');
              videojs('my-video', {playbackRates: [0.5, 1, 1.5, 2],controlBar: {fullscreenToggle: true},});
              var player = videojs('my-video');
              player.on('contextmenu', function(e) {
                  e.preventDefault();
              });

            }else if(result.type_content == 'file'){
              $("#previewKonten").html('<iframe width="100%" height="500px" src="'+result.content+'/preview"></iframe><div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>');
            }else{
              $("#previewKonten").html(result.content)
            }
          },
          error: function(){
            $("#previewKonten").html('Mohon maaf. Terjadi kesalahan');
          }
        });

        if(videojs('my-video')){
          videojs('my-video').dispose();
        }

      })
    })
  </script>

  <script type="text/javascript">
    $('#modalPreviewContent').on('hidden.bs.modal', function () {
      $("#previewKonten").html('');
      videojs('my-video').dispose();
    })
  </script>

  <script type="text/javascript">
    $(window).scroll(function(){
      var primaryContent = document.getElementById('descriptionContent');
      var primary = primaryContent.offsetTop * 1.8;
      if ($(window).scrollTop() > primary) {
          $('#joinBtn').addClass('mobileHide').fadeIn(600);
      }
      else {
          $('#joinBtn').removeClass('mobileHide');
      }
    });
  </script>

@endpush
