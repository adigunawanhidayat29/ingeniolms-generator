@extends('layouts.app')

@section('title', 'Course Grades')

@push('style')
  <style media="screen">
    .link-grade{
      text-decoration: underline;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Nilai <span>Peserta</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Pengajar</a></li>
          <li><a href="/course/preview/{{Request::segment(3)}}">Progress Peserta</a></li>
          <li>Buku Nilai</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
      <a class="btn btn-primary btn-raised" href="/course/grades/setting/{{Request::segment(3)}}">Pengaturan Nilai</a>
      
      <a target="_blank" class="btn btn-success btn-raised pull-right" href="/course/grades/{{Request::segment(3)}}/export">Download CSV</a>
      
      <br><br>
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Buku Nilai</h3>
			  </div>
			  <div class="panel-body" style="background:white">
          @if(count($quizzes) < 1 && count($assignments) < 1)
            <p class="alert alert-info">Kuis belum tersedia</p>
          @else
  					<table class="table table-bordered">
  						<tr>
  							<th>No</th>
  							<th>Name</th>
                @foreach($quizzes as $quiz)
                  @php
                    $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                  @endphp
    							<th><a class="link-grade" href="/course/grades/{{Request::segment(3)}}/quiz/{{$quiz->id}}">{{$quiz->name}} (bobot {{isset($quiz_grade_setting) ? $quiz_grade_setting->percentage : '0'}}%)</a></th>
                @endforeach
                @foreach($assignments as $assignment)
                  @php
                    $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();                    
                  @endphp
    							<th><a class="link-grade" href="/course/grades/{{Request::segment(3)}}/assignment/{{$assignment->id}}">{{$assignment->title}} (bobot {{isset($assignment_grade_setting) ? $assignment_grade_setting->percentage : '0'}}%)</a></th>
                @endforeach
                {{-- <th>Completion</th> --}}
                <th>Total</th>
  						</tr>

              @foreach($course_users as $index => $course_user)

                <tr>
                  <td>{{$index +1}}</td>
                  <td><a class="link-grade" href="/course/grades/{{Request::segment(3)}}/user/{{$course_user->user_id}}">{{$course_user->name}}</a></td>

                  @php
                    $grade_total = 0 ;
                    $grade_percent_total = 0 ;
                  @endphp

                  @foreach($quizzes as $quiz)

                    @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_user->user_id, 'quiz_id' => $quiz->id])->first() @endphp

                    @php
                      $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                      if(!$quiz_grade_setting){
                        $quiz_grade_setting_percent = 100;
                      }else{
                        $quiz_grade_setting_percent = $quiz_grade_setting->percentage;
                      }
                    @endphp

                    @if($quiz_participant)
                      @php
                        $grade = $quiz_participant->grade
                      @endphp
                      <td>{{$grade}}</td>
                    @else
                      @php $grade = 0 @endphp
                      <td>-</td>
                    @endif

                    @php $grade_percent_total += $grade * $quiz_grade_setting_percent / 100  @endphp
                    @php $grade_total += $grade @endphp
                  @endforeach

                  @foreach($assignments as $assignment)

                    @php
                      $assignments_answers = \DB::table('assignments_answers')
                        ->where(['assignments_answers.user_id' => $course_user->user_id, 'assignments_answers.assignment_id' => $assignment->id])
                        ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                        ->first()
                    @endphp

                    @php
                      $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                      if(!$assignment_grade_setting){
                        $assignment_grade_setting_percent = 100;
                      }else{
                        $assignment_grade_setting_percent = $quiz_grade_setting->percentage ?? 0;
                      }
                    @endphp

                    @if($assignments_answers)
                      @php $grade_assignment = $assignments_answers->grade @endphp
                      <td>{{$grade_assignment}}</td>
                    @else
                      @php
                        $grade_assignment = 0 ;
                      @endphp
                      <td>0</td>
                    @endif

                    @php $grade_percent_total += $grade_assignment * $assignment_grade_setting_percent / 100  @endphp
                    @php $grade_total += $grade_assignment @endphp

                  @endforeach
                  
                  @php 
                    $totalFinish = ($grade_total) / (count($quizzes) + count($assignments));
                  @endphp
                  <td>{{ round($totalFinish, 2)}}</td>
                </tr>
              @endforeach

  					</table>
          @endif
			  </div>
			</div>
		</div>
	</div>

@endsection
