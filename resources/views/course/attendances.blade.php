@extends('layouts.app')

@section('title', Lang::get('front.page_manage_attendances.title') . ' - ' . $Course->title)

@push('style')
<link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
	<style>
		.nav-tabs-ver-container .nav-tabs-ver {
			padding: 0;
			margin: 0;
		}

		.nav-tabs-ver-container .nav-tabs-ver:after {
			display: none;
		}
	</style>
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.page_manage_attendances.title') <span>{{ $Course->title }}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.page_manage_atendee.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_atendee.breadcumb_manage_courses')</a></li>
          <li><a href="/course/preview/{{$Course->id}}">{{ $Course->title }}</a></li>
          <li>@lang('front.page_manage_attendances.title')</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-3 nav-tabs-ver-container">
					<img src="{{$Course->image}}" alt="{{ $Course->title }}" class="img-fluid mb-2">
					<div class="card no-shadow">
						<ul class="nav nav-tabs-ver" role="tablist">
							<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$Course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/atendee/{{$Course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
							<li class="nav-item"><a class="nav-link active" href="/course/attendance/{{$Course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/grades/{{$Course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>
							<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$Course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>
							<li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$Course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
							@if(isTeacher(Auth::user()->id) === true)
                                <li class="nav-item"><a class="nav-link" href="/{{$Course->id}}/list_badges"><i class="fa fa-shield"></i> Badges</a></li>
                                @if($Course->level_up == 1)
                                    <li class="nav-item"><a class="nav-link" href="/{{$Course->id}}/level_settings"><i class="fa fa-trophy"></i> Levels</a></li>
                                @endif
                            @endif
						</ul>
					</div>
				</div>
				<div class="col-md-9">

					<div class="card card-flat">
						<ul class="nav nav-tabs nav-tabs-full nav-tabs-3" role="tablist">
							<li class="nav-item">
								<a class="nav-link withoutripple active" href="#sessions" aria-controls="sessions" role="tab" data-toggle="tab">
									<i class="fa fa-eye"></i>
									<span class="d-none d-sm-inline">@lang('front.page_manage_attendances.horizontal_tab_session')</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link withoutripple" href="#add_sessions" aria-controls="add_sessions" role="tab" data-toggle="tab">
									<i class="fa fa-eye"></i>
									<span class="d-none d-sm-inline">@lang('front.page_manage_attendances.horizontal_tab_add_session')</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link withoutripple" href="#report" aria-controls="report" role="tab" data-toggle="tab">
									<i class="fa fa-list"></i>
									<span class="d-none d-sm-inline">@lang('front.page_manage_attendances.horizontal_tab_report')</span>
								</a>
							</li>
						</ul>
					</div>

					<div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="sessions">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm mt-0 mb-0">@lang('front.page_manage_attendances.list_session')</h3>
							</div>

							<div class="table-responsive">
								<table class="table table-striped">
									<tr>
										<th>@lang('front.page_manage_attendances.table_session_no')</th>
										<th>@lang('front.page_manage_attendances.table_session_name')</th>
										<th>@lang('front.page_manage_attendances.table_session_description')</th>
										<th>@lang('front.page_manage_attendances.table_session_datetime_start')</th>
										<th>@lang('front.page_manage_attendances.table_session_datetime_end')</th>
										<th>@lang('front.page_manage_attendances.table_session_manual_absent')</th>
										<th class="text-right">@lang('front.page_manage_attendances.table_session_option')</th>
									</tr>
									@foreach($CourseAttendances as $index => $data)
										<tr class="bg-white">
											<td>{{$index + 1}}</td>
											<td>{{$data->name}}</td>
											<td>{{$data->description}}</td>
											<td>{{$data->datetime}}</td>
											<td>{{$data->end_datetime}}</td>
											<td>{{$data->manual_student == '1' ? Lang::get('front.page_manage_attendances.table_session_manual_student_yes') : Lang::get('front.page_manage_attendances.table_session_manual_student_no')}}</td>
											<td class="text-right">
												<a class="btn-circle btn-circle-primary btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#modalSessionDetail{{$data->id}}" title="@lang('front.page_manage_attendances.table_session_option_play')"><i class="fa fa-play"></i></a>
												<a class="btn-circle btn-circle-warning btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#modalSessionUpdate{{$data->id}}" title="@lang('front.page_manage_attendances.table_session_option_edit')"><i class="fa fa-pencil"></i></a>
												<a class="btn-circle btn-circle-danger btn-circle-raised btn-circle-sm" href="/course/attendance/delete/{{$data->id}}" onclick="return confirm('@lang('front.page_manage_attendances.table_session_option_delete_alert')')" title="@lang('front.page_manage_attendances.table_session_option_delete')"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="add_sessions">
							<h3>@lang('front.page_manage_attendances.horizontal_tab_add_session')</h3>
							{{-- <a href="#" data-target="#modalSessionNew" data-toggle="modal" class="btn btn-sm btn-raised btn-primary">Tambah Sesi</a> --}}

							<form action="/course/attendance/store/{{$Course->id}}" method="post">
								{{ csrf_field() }}
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.add_session_description')</label>
									<input placeholder="@lang('front.page_manage_attendances.add_session_description')" type="text" class="form-control" name="description" value="" required>
								</div>
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.add_session_datetime_start')</label>
									<input placeholder="@lang('front.page_manage_attendances.add_session_datetime_start')" type="text" class="form-control datetimepicker" autocomplete="off" name="datetime" value="" required>
								</div>
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.add_session_datetime_end')</label>
									<input placeholder="@lang('front.page_manage_attendances.add_session_datetime_end')" type="text" class="form-control datetimepicker" autocomplete="off" name="end_datetime" value="" required>
								</div>

								<hr>

								<h3>@lang('front.page_manage_attendances.add_session_multiple')</h3>
								<div class="form-group">
									<label for="">@lang('front.page_manage_attendances.add_session_repeat_on_day')</label> <br>
									<input type="checkbox" id="monday" name="repeat_on[]" value="monday"> <label for="monday">@lang('front.page_manage_attendances.add_session_repeat_on_day_monday')</label>
									<input type="checkbox" id="tuesday" name="repeat_on[]" value="tuesday"> <label for="tuesday">@lang('front.page_manage_attendances.add_session_repeat_on_day_tuesday')</label>
									<input type="checkbox" id="wednesday" name="repeat_on[]" value="wednesday"> <label for="wednesday">@lang('front.page_manage_attendances.add_session_repeat_on_day_wednesday')</label>
									<input type="checkbox" id="thursday" name="repeat_on[]" value="thursday"> <label for="thursday">@lang('front.page_manage_attendances.add_session_repeat_on_day_thursday')</label>
									<input type="checkbox" id="friday" name="repeat_on[]" value="friday"> <label for="friday">@lang('front.page_manage_attendances.add_session_repeat_on_day_friday')</label>
									<input type="checkbox" id="saturday" name="repeat_on[]" value="saturday"> <label for="saturday">@lang('front.page_manage_attendances.add_session_repeat_on_day_saturday')</label>
									<input type="checkbox" id="sunday" name="repeat_on[]" value="sunday"> <label for="sunday">@lang('front.page_manage_attendances.add_session_repeat_on_day_sunday')</label>
								</div>

								<div class="form-group">
									<label for="">@lang('front.page_manage_attendances.add_session_repeat_every_week')</label> <br>
									<select name="" id="" class="form-control selectpicker" style="width: 30px !important">
										<option value="1">1</option>
										{{-- <option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option> --}}
									</select>
								</div>

								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.add_session_repeat_every_week_until')</label>
									<input type="text" class="form-control datepicker" autocomplete="off" name="end_date" value="">
								</div>

								<hr>

								<h3>@lang('front.page_manage_attendances.add_session_manual_absen')</h3>
								<input type="checkbox" name="manual_student" value="1"> @lang('front.page_manage_attendances.add_session_manual_absen_yes')

								<div class="form-group">
									<input type="submit" class="btn btn-raised btn-primary" value="@lang('front.page_manage_attendances.add_session_save')">
								</div>
							</form>

						</div>
						<div role="tabpanel" class="tab-pane" id="report">
							<h3>@lang('front.page_manage_attendances.horizontal_tab_report')</h3>
							<a target="_blank" href="/course/attendance/download/excel/{{$Course->id}}" class="btn btn-raised btn-sm btn-success">@lang('front.page_manage_attendances.report_export')</a>
							<div class="table-responsive">

								<table class="table">
									<thead>
										<th>@lang('front.page_manage_attendances.report_table_no')</th>
										<th>@lang('front.page_manage_attendances.report_table_name')</th>
										<th>@lang('front.page_manage_attendances.report_table_present')</th>
										<th>@lang('front.page_manage_attendances.report_table_late')</th>
										<th>@lang('front.page_manage_attendances.report_table_excused')</th>
										<th>@lang('front.page_manage_attendances.report_table_absent')</th>
										<th>@lang('front.page_manage_attendances.report_table_point')</th>
										<th>@lang('front.page_manage_attendances.report_table_percentage')</th>
										@foreach($CourseAttendancesSummary as $index => $data)
										<th>{{$data->description}}</th>
										@endforeach
									</thead>
									<tbody>
										@php $summary = [] @endphp
										@foreach ($reports as $index => $item)
											<tr>
												<td>{{$index + 1}}</td>
												<td>{{$item->user->name}}</td>
												<td>{{$item->sum_status_p}}</td>
												<td>{{$item->sum_status_l}}</td>
												<td>{{$item->sum_status_e}}</td>
												<td>{{$item->sum_status_a}}</td>
												<td>{{($item->sum_status_p + $item->sum_status_l)}} / {{$item->count_attendances}}</td>
												<td>{{ round( (($item->sum_status_p + $item->sum_status_l) / $item->count_attendances) * 100) }} %</td>
												@foreach($CourseAttendancesSummary as $data)
													@php
														$get_status = DB::table('course_attendance_users')->where(['user_id' => $item->user->id, 'course_attendances_id' => $data->course_attendances_id])->first();
													@endphp
													<td>{{$get_status ? $get_status->status : '?'}} {{$get_status && $get_status->remarks != null ? '(' . $get_status->remarks . ')' : ''}}</td>
												@endforeach
											</tr>
										@endforeach
										<tr>
											<td colspan="8">@lang('front.page_manage_attendances.report_table_summary')</td>
											@foreach($CourseAttendancesSummary as $index => $data)
												<td>
													<span>P: {{$data->sum_status_p}}</span> <br>
													<span>L: {{$data->sum_status_l}}</span> <br>
													<span>E: {{$data->sum_status_e}}</span> <br>
													<span>A: {{$data->sum_status_a}}</span>
												</td>
											@endforeach
										</tr>
									</tbody>
								</table>

							</div>
						</div>
						{{-- <div role="tabpanel" class="tab-pane" id="export">
							<h3>Export</h3>
						</div> --}}
					</div>
				</div>

			</div>
		</div>
	</div>

		{{-- <div class="modal" id="modalSessionNew" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">Tambah Sesi</span></h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
						<form action="/course/attendance/store/{{$Course->id}}" method="post">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="title">Nama Sesi</label>
								<input placeholder="Nama Group" type="text" class="form-control" name="description" value="" required>
							</div>
							<div class="form-group">
								<label for="title">Tanggal</label>
								<input placeholder="Tangal" type="text" class="form-control datetimepicker" autocomplete="off" name="datetime" value="" required>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-raised btn-primary" value="Simpan">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div> --}}

		@foreach($CourseAttendances as $index => $data)
			<div class="modal" id="modalSessionDetail{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm m-0">@lang('front.page_manage_attendances.table_session_option_play') {{$data->description}}</span></h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										<i class="zmdi zmdi-close"></i>
									</span>
								</button>
							</div>

							<form action="/course/attendance/submit/{{$Course->id}}/{{$data->id}}" method="post">
								{{csrf_field()}}
								<div class="table-responsive">
									<table class="table table-striped">
										<tr>
											<th>@lang('front.page_manage_attendances.report_table_no')</th>
											<th>@lang('front.page_manage_attendances.report_table_name')</th>
											<th class="{{$data->id}}check_p">@lang('front.page_manage_attendances.report_table_present')</th>
											<th class="{{$data->id}}check_l">@lang('front.page_manage_attendances.report_table_late')</th>
											<th class="{{$data->id}}check_e">@lang('front.page_manage_attendances.report_table_excused')</th>
											<th class="{{$data->id}}check_a">@lang('front.page_manage_attendances.report_table_absent')</th>
											<th>@lang('front.page_manage_attendances.report_table_remark')</th>
										</tr>
										@if (count($data->course_attendance_user) == 0)
											@foreach($courses_users as $index => $courses_user)
												<tr class="bg-white">
													<td>{{$index + 1}}</td>
													<td>
														{{$courses_user->name}}
														<input type="hidden" name="user_id[]" value="{{$courses_user->user_id}}">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_p" name="{{$courses_user->user_id}}_status" checked value="p">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_l" name="{{$courses_user->user_id}}_status" value="l">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_e" name="{{$courses_user->user_id}}_status" value="e">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_a" name="{{$courses_user->user_id}}_status" value="a">
													</td>
													<td>
														<input type="text" class="form-control" name="{{$courses_user->user_id}}_remarks">
													</td>
												</tr>
											@endforeach
										@else
											@foreach($data->course_attendance_user as $index => $course_attendance_user)
												<tr class="bg-white">
													<td>{{$index + 1}}</td>
													<td>
														{{$course_attendance_user->user->name}}
														<input type="hidden" name="user_id[]" value="{{$course_attendance_user->user_id}}">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_p" name="{{$course_attendance_user->user_id}}_status" {{$course_attendance_user->status == 'p' ? 'checked' : ''}} value="p">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_l" name="{{$course_attendance_user->user_id}}_status" {{$course_attendance_user->status == 'l' ? 'checked' : ''}} value="l">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_e" name="{{$course_attendance_user->user_id}}_status" {{$course_attendance_user->status == 'e' ? 'checked' : ''}} value="e">
													</td>
													<td>
														<input type="radio" class="{{$data->id}}status_a" name="{{$course_attendance_user->user_id}}_status" {{$course_attendance_user->status == 'a' ? 'checked' : ''}} value="a">
													</td>
													<td>
													<input type="text" class="form-control" name="{{$course_attendance_user->user_id}}_remarks" value="{{$course_attendance_user->remarks}}">
													</td>
												</tr>
											@endforeach
										@endif
									</table>
								</div>

								<div class="form-group text-right">
									<input type="submit" class="btn btn-raised btn-primary" value="@lang('front.page_manage_attendances.add_session_save')">
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

			<div class="modal" id="modalSessionUpdate{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm m-0">@lang('front.page_manage_attendances.horizontal_tab_update_session')</span></h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										<i class="zmdi zmdi-close"></i>
									</span>
								</button>
							</div>
							<form action="/course/attendance/update/{{$data->id}}" method="post">
								{{ csrf_field() }}
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.table_session_name')</label>
									<input placeholder="@lang('front.page_manage_attendances.table_session_name')" type="text" class="form-control" name="description" value="{{$data->description}}" required>
								</div>
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.table_session_datetime_start')</label>
									<input placeholder="@lang('front.page_manage_attendances.table_session_datetime_start')" type="text" class="form-control datetimepicker" autocomplete="off" name="datetime" value="{{$data->datetime}}" required>
								</div>
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.table_session_datetime_end')</label>
									<input placeholder="@lang('front.page_manage_attendances.table_session_datetime_end')" type="text" class="form-control datetimepicker" autocomplete="off" name="end_datetime" value="{{$data->end_datetime}}">
								</div>
								<div class="form-group">
									<label for="title">@lang('front.page_manage_attendances.table_session_manual_absent')</label>
									<input {{$data->manual_student == '1' ? 'checked' : ''}} type="checkbox" name="manual_student"> @lang('front.page_manage_attendances.table_session_manual_student_yes')
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-raised btn-primary" value="@lang('front.page_manage_attendances.add_session_save')">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		@endforeach

@endsection

@push('script')
	@foreach($CourseAttendances as $index => $data)
		<script>
			$(".{{$data->id}}check_p").click(function() {
				$(".{{$data->id}}status_p").prop("checked", true);
			})
			$(".{{$data->id}}check_l").click(function() {
				$(".{{$data->id}}status_l").prop("checked", true);
			})
			$(".{{$data->id}}check_e").click(function() {
				$(".{{$data->id}}status_e").prop("checked", true);
			})
			$(".{{$data->id}}check_a").click(function() {
				$(".{{$data->id}}status_a").prop("checked", true);
			})
		</script>
	@endforeach

@endpush
