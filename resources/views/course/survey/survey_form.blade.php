@extends('layouts.app')

@section('title')
	{{ 'Buat Survei ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
	<style>
		.custom-select{
			height: 40px !important;
		}

		.custom-select option{
			padding: 10px;
		}

		.hidden{
			display: none;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Membuat <span>Survei</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li><a href="/course/dashboard">Kelola Kelas</a></li>
				  <li><a href="/course/preview/{{ $course->id }}">{{ $course->title }}</a></li>
					<li>Membuat Survei</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						{{-- <div class="box-header with-border">
							<h3 class="box-title">{{$button}} Survei - {{$section->title}}</h3>
						</div> --}}
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										<div class="form-group">
											<label for="name">Judul Survei</label>
											<input placeholder="Judul Survei" type="text" class="form-control" name="name" value="{{ $name }}" required>
										</div>

										<div class="form-group">
											<label for="title">Deskripsi / Penjelasan</label>
											<textarea name="description" id="description">{{ $description }}</textarea>
										</div>
										{{-- <div class="form-group">
											<label for="title">Acak?</label><br>
											<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
											<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
										</div> --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Tanggal Mulai</label>
													<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : date("Y-m-d") }}" required>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Jam Mulai</label>
													<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ isset($time_start) ? date("H:i:s", strtotime($time_start)) : '08:00' }}" required>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<input type="checkbox" value="1" name="time_end_required" class="time_end_required"> Aktifkan Tanggal Selesai
												</div>
											</div>
										</div>

										<div class="row end_form hidden">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Tanggal Selesai</label>
													<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{  isset($time_end) ? date("Y-m-d", strtotime($time_end)) : '' }}">
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Jam Selesai</label>
													<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ isset($time_end) ? date("H:i:s", strtotime($time_end)) : '08:00' }}" required>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Submission Grade</label>
													<select class="form-control custom-select" name="grade" id="id_grade">
								            <option value="0" selected="">No grade</option>
								            <option value="100">100</option>
								            <option value="99">99</option>
								            <option value="98">98</option>
								            <option value="97">97</option>
								            <option value="96">96</option>
								            <option value="95">95</option>
								            <option value="94">94</option>
								            <option value="93">93</option>
								            <option value="92">92</option>
								            <option value="91">91</option>
								            <option value="90">90</option>
								            <option value="89">89</option>
								            <option value="88">88</option>
								            <option value="87">87</option>
								            <option value="86">86</option>
								            <option value="85">85</option>
								            <option value="84">84</option>
								            <option value="83">83</option>
								            <option value="82">82</option>
								            <option value="81">81</option>
								            <option value="80">80</option>
								            <option value="79">79</option>
								            <option value="78">78</option>
								            <option value="77">77</option>
								            <option value="76">76</option>
								            <option value="75">75</option>
								            <option value="74">74</option>
								            <option value="73">73</option>
								            <option value="72">72</option>
								            <option value="71">71</option>
								            <option value="70">70</option>
								            <option value="69">69</option>
								            <option value="68">68</option>
								            <option value="67">67</option>
								            <option value="66">66</option>
								            <option value="65">65</option>
								            <option value="64">64</option>
								            <option value="63">63</option>
								            <option value="62">62</option>
								            <option value="61">61</option>
								            <option value="60">60</option>
								            <option value="59">59</option>
								            <option value="58">58</option>
								            <option value="57">57</option>
								            <option value="56">56</option>
								            <option value="55">55</option>
								            <option value="54">54</option>
								            <option value="53">53</option>
								            <option value="52">52</option>
								            <option value="51">51</option>
								            <option value="50">50</option>
								            <option value="49">49</option>
								            <option value="48">48</option>
								            <option value="47">47</option>
								            <option value="46">46</option>
								            <option value="45">45</option>
								            <option value="44">44</option>
								            <option value="43">43</option>
								            <option value="42">42</option>
								            <option value="41">41</option>
								            <option value="40">40</option>
								            <option value="39">39</option>
								            <option value="38">38</option>
								            <option value="37">37</option>
								            <option value="36">36</option>
								            <option value="35">35</option>
								            <option value="34">34</option>
								            <option value="33">33</option>
								            <option value="32">32</option>
								            <option value="31">31</option>
								            <option value="30">30</option>
								            <option value="29">29</option>
								            <option value="28">28</option>
								            <option value="27">27</option>
								            <option value="26">26</option>
								            <option value="25">25</option>
								            <option value="24">24</option>
								            <option value="23">23</option>
								            <option value="22">22</option>
								            <option value="21">21</option>
								            <option value="20">20</option>
								            <option value="19">19</option>
								            <option value="18">18</option>
								            <option value="17">17</option>
								            <option value="16">16</option>
								            <option value="15">15</option>
								            <option value="14">14</option>
								            <option value="13">13</option>
								            <option value="12">12</option>
								            <option value="11">11</option>
								            <option value="10">10</option>
								            <option value="9">9</option>
								            <option value="8">8</option>
								            <option value="7">7</option>
								            <option value="6">6</option>
								            <option value="5">5</option>
								            <option value="4">4</option>
								            <option value="3">3</option>
								            <option value="2">2</option>
								            <option value="1">1</option>
								        	</select>
												</div>
											</div>
										</div>


										<div class="form-group">
											<label for="name">Durasi (Menit)</label>
											<input placeholder="60" type="number" class="form-control" name="duration" value="{{ $duration }}" required>
										</div>
										{{-- <div class="form-group">
											<label for="name">Kesempatan / Percobaan</label>
											<input placeholder="3" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
										</div> --}}
										<input placeholder="3" type="hidden" class="form-control" name="attempt" value="{{ $attempt }}">
										<div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Survei" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})

		$(".time_end_required").change(function(){
			var cheked = $(this).prop('checked');
			if(cheked){
				$(".end_form").removeClass('hidden');
				$(".end_form .datePicker").prop('required', true);
			}else {
				$(".end_form").addClass('hidden');
				$(".end_form .datePicker").prop('required', false);
			}

		});
	</script>
@endpush
