@extends('layouts.app')

@section('title')
	{{ 'Pengumuman' }}
@endsection


@section('content')
	<div class="container mt-3">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Pengumuman</h3>
		  </div>
		  <div class="panel-body" style="background:white;">
				<a class="btn btn-primary btn-raised" href="/course/announcement/create/{{Request::segment(3)}}">Buat Pengumuman</a>
				<a class="btn btn-default btn-raised" href="/course/preview/{{Request::segment(3)}}">Kelola Kelas</a>
				<table class="table table-bordered table-hover">
					<tr>
						<th>Judul</th>
						<th>Deskripsi</th>
						<th>Tanggal</th>
						<th>Pilihan</th>
					</tr>
					@foreach($course_announcements as $course_announcement)
						<tr>
							<td>{{$course_announcement->title}}</td>
							<td>{!!$course_announcement->description!!}</td>
							<td>{{$course_announcement->created_at}}</td>
							<td>
								<div class="btn-group">
								    <button type="button" class="btn btn btn-default dropdown-toggle btn-raised" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								        Pilihan <i class="zmdi zmdi-chevron-down right"></i>
								    </button>
								    <ul class="dropdown-menu">
												<form name="delete" action="/course/announcement/delete/{{$course_announcement->id}}" method="post">
													{{csrf_field()}}
													<input type="hidden" name="_method" value="DELETE">
													<li><button type="submit" class="dropdown-item" onclick="return confirm('are you sure?')">Hapus</button></li>
												</form>
												<li><a class="dropdown-item" href="/course/announcement/edit/{{$course_announcement->id}}">Edit</a></li>
								    </ul>
								</div>
							</td>
						</tr>
					@endforeach
				</table>
				{{$course_announcements->render()}}
		  </div>
		</div>
	</div>
@endsection
