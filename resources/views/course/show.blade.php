@extends('layouts.app')

@section('title')
	{{ 'Course' }}
@endsection


@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">

					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
							<div class="panel panel-primary">
						    <div class="panel-heading">Course : {{$courses->title}}</div>
						    <div class="panel-body">
									<table class="table table-hover">
		                <tr>
		                  <td>Title</td>
											<td>{{$courses->title}}</td>
		                </tr>
										<tr>
		                  <td>Image</td>
											<td><img src="{{asset_url($courses->image)}}" alt="{{$courses->title}}" class="img-thumbnail" /></td>
		                </tr>
										<tr>
		                  <td>Goal</td>
											<td>{!!$courses->goal!!}</td>
		                </tr>
										<tr>
		                  <td>Description</td>
											<td>{!!$courses->description!!}</td>
		                </tr>
										<tr>
		                  <td>Category</td>
											<td>{{$courses->category}}</td>
		                </tr>
										<tr>
		                  <td>Level</td>
											<td>{{$courses->level}}</td>
		                </tr>
										<tr>
		                  <td>Author</td>
											<td>{{$courses->author}}</td>
		                </tr>
										<tr>
		                  <td>Participant</td>
											<td>{{$course_user}} participant <a href="{{url('course/participant/'. $courses->id)}}">View Participant</a></td>
		                </tr>
		              </table>
								</div>
						  </div>
            </div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
