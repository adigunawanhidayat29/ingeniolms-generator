@extends('layouts.app')

@section('title', Lang::get('front.page_manage_atendee.title') . ' ' . $Course->title)

@push('style')
<link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
	<style>
		.nav-tabs-ver-container .nav-tabs-ver {
			padding: 0;
			margin: 0;
		}

		.nav-tabs-ver-container .nav-tabs-ver:after {
			display: none;
		}
	</style>
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.page_manage_atendee.title') <span>{{ $Course->title }}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.page_manage_atendee.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_atendee.breadcumb_manage_courses')</a></li>
          <li><a href="/course/preview/{{$Course->id}}">{{ $Course->title }}</a></li>
          <li>@lang('front.page_manage_atendee.breadcumb_atendee')</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-3 nav-tabs-ver-container">
					<img src="{{$Course->image}}" alt="{{ $Course->title }}" class="img-fluid mb-2">
					<div class="card no-shadow">
						<ul class="nav nav-tabs-ver" role="tablist">
							<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$Course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
							<li class="nav-item"><a class="nav-link active" href="/course/atendee/{{$Course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/attendance/{{$Course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/grades/{{$Course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$Course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$Course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
							@if(isTeacher(Auth::user()->id) === true)
                                <li class="nav-item"><a class="nav-link" href="/{{$Course->id}}/list_badges"><i class="fa fa-shield"></i> Badges</a></li>
                                @if($Course->level_up == 1)
                                    <li class="nav-item"><a class="nav-link" href="/{{$Course->id}}/level_settings"><i class="fa fa-trophy"></i> Levels</a></li>
                                @endif
                            @endif
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="tab-content">
            {{-- Discussions --}}
            <div role="tabpanel" class="tab-pane active" id="attendee">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm mt-0 mb-0">@lang('front.page_manage_atendee.title')</h3>
								<a href="/course/completion/{{$Course->id}}/export" target="_blank" class="btn btn-sm btn-raised btn-success mt-0 mb-0"><i class="zmdi zmdi-arrow-right"></i>@lang('front.page_manage_atendee.export_atendee')</a>
								<a href="#" data-toggle="modal" data-target="#modalImportAtendee" class="btn btn-sm btn-raised btn-info mt-0 mb-0"><i class="zmdi zmdi-arrow-right"></i>@lang('front.page_manage_atendee.import_atendee')</a>
							</div>
							<a href="#" data-target="#modalGroupLists" data-toggle="modal" class="btn btn-sm btn-raised btn-primary">@lang('front.page_manage_atendee.group_atendee')</a>

							<div class="table-responsive">
								<table class="table table-striped">
									<tr>
										<th>@lang('front.page_manage_atendee.table_no')</th>
										<th>@lang('front.page_manage_atendee.table_name')</th>
										<th>@lang('front.page_manage_atendee.table_percentage')</th>
										<th class="text-right">@lang('front.page_manage_atendee.table_option')</th>
									</tr>
									@foreach($completions as $index => $completion)
										<tr class="bg-white">
											<td>{{$index + 1}}</td>
											<td>{{$completion['name']}}</td>
											<td>{{round($completion['percentage'], 2)}}%</td>
											<td class="text-right">
												<a class="btn-circle btn-circle-primary btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#userProgressDetail{{$completion['user_id']}}" title="Detail pembelajaran"><i class="fa fa-info-circle"></i></a>
												{{-- <a onclick="return confirm('Yakin akan menghapus peserta ini?')" class="btn-circle btn-circle-danger btn-circle-raised btn-circle-sm" href="/course/completion/delete/{{Request::segment(3)}}/{{$completion['user_id']}}" title="Hapus peserta"><i class="fa fa-trash"></i></a> --}}
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>

				{{-- <div class="col-md-12">
					<div class="card card-groups mt-2">
						<div class="card-block">
							<div class="table-responsive">
								<table class="table">
									<tr style="background-color: #e6e6e6;">
										<th>No</th>
										<th>Nama</th>
										<th>Pilihan</th>
									</tr>
									@foreach($completions as $index => $completion)
										<tr>
											<td>{{$index + 1}}</td>
											<td>{{$completion['name']}}</td>
											<td>
												<a class="btn btn-primary btn-raised btn-xs m-0" href="/course/completion/detail/{{Request::segment(3)}}/{{$completion['user_id']}}">Lihat Progres</a>
												<a onclick="return confirm('Yakin akan menghapus peserta ini?')" class="btn btn-danger btn-raised btn-xs m-0" href="/course/completion/delete/{{Request::segment(3)}}/{{$completion['user_id']}}">Hapus</a>
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div> --}}
			</div>
		</div>
	</div>

	<!-- Modal -->
	@foreach($completions as $index => $completion)
		<div class="modal" id="userProgressDetail{{$completion['user_id']}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">@lang('front.page_manage_atendee.detail_learning_title') <span>{{$completion['name']}}</span></h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr style="background-color: #e6e6e6;">
										<th>@lang('front.page_manage_atendee.modal_table_section')</th>
										<th>@lang('front.page_manage_atendee.modal_table_content')</th>
										<th>@lang('front.page_manage_atendee.modal_table_status')</th>
									</tr>
								</thead>
								<tbody>
									{{-- <tr style="{{$content_progress ? 'background-color: #eee; color: #999;' : ''}}"> --}}
									
									@foreach($completion['section_data'] as $section)
										@foreach($section['section_contents'] as $content)
											@php
												$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => $completion['user_id'], 'status' => '1'])->first();
											@endphp
											<tr style="{{$content_progress ? 'background-color: #eee; color: #999;' : ''}}">
												<td>{{$section['title']}}</td>
												<td>{{$content->title}}</td>
												<td>
													<span class="badge {{ $content_progress ? 'badge-success' : 'badge-danger' }}">{{ $content_progress ? \Lang::get('front.page_manage_atendee.detail_learning_title_finished') : \Lang::get('front.page_manage_atendee.detail_learning_title_nofinished') }}</span>
												</td>
											</tr>
										@endforeach
									@endforeach
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach

		<div class="modal" id="modalGroupLists" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">List Group</span></h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
						<div class="table-responsive">
							<a href="#" data-toggle="modal" class="btn btn-primary btn-raised" data-target="#modalGroupNew">@lang('front.page_manage_atendee.modal_create_group_title')</a>
							<table class="table">
								<thead>
									<tr style="background-color: #e6e6e6;">
										<th>No</th>
										<th>@lang('front.page_manage_atendee.modal_create_group_table_group_name')</th>
										<th>@lang('front.page_manage_atendee.modal_create_group_table_atendee')</th>
										<th>@lang('front.page_manage_atendee.modal_create_group_table_option')</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($course_student_groups as $index => $item)
											<tr>
												<td>{{$index + 1}}</td>
												<td>{{$item->name}}</td>
												<td>
													@foreach ($item->course_student_group_user as $data)
															<ul>
																<li>{{$data->user->name}}</li>
															</ul>
													@endforeach
												</td>
												<td><a href="#" data-target="#modalGroupNewStudent{{$item->id}}" data-toggle="modal">@lang('front.page_manage_atendee.modal_create_group_table_option_add_participant')</a></td>
											</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="modalGroupNew" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">@lang('front.page_manage_atendee.modal_create_group_title')</span></h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
						<form action="/courses/student/group/store/{{$Course->id}}" method="post">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="title">@lang('front.page_manage_atendee.modal_create_group_table_group_name')</label>
								<input type="text" class="form-control" name="name" value="" required>
							</div>
							<div class="form-group">
								<label for="title">@lang('front.page_manage_atendee.modal_create_group_table_group_description')</label>
								<textarea class="form-control"></textarea>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-raised btn-primary" value="@lang('front.general.text_save')">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		@foreach ($course_student_groups as $index => $item)
			<div class="modal" id="modalGroupNewStudent{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="d-flex align-items-center justify-content-between mb-2">
								<h3 class="headline headline-sm m-0">Tambah Peserta {{$item->name}}</span></h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										<i class="zmdi zmdi-close"></i>
									</span>
								</button>
							</div>
							<form action="/courses/student/group/add-user/{{ $item->id }}" method="post">
									{{ csrf_field() }}
									<div class="modal-body">
											<select name="user_id[]" id="duallist{{ $item->id }}" multiple>
													@foreach($course_student_group_users_by_course as $index => $user)
															<option @foreach($item->course_student_group_user as $data) @if($user->user_id == $data->user_id) selected @endif @endforeach value="{{ $user->user_id }}">{{ isset($user->student->name) ? $user->student->name : '' }}</option>
													@endforeach
													@foreach($item->course_student_group_user as $data)
															<option selected value="{{ $data->user_id }}">{{ $data->user->name }}</option>
													@endforeach
											</select>
									</div>
									<div class="modal-footer">
											<button type="submit" class="btn btn-link waves-effect">@lang('front.general.text_save')</button>
											<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('front.general.text_close')</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		<div class="modal" id="modalImportAtendee" tabindex="-1" role="dialog" aria-labelledby="userProgressDetailLabel">
			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h3 class="headline headline-sm m-0">Import</span></h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="zmdi zmdi-close"></i>
								</span>
							</button>
						</div>
						<form action="/courses/atendee/import/{{$Course->id}}" method="post" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="modal-body">
									<div class="">
										<input type="file" name="file">
									</div>
								</div>
								<div class="modal-footer">
										<button type="submit" class="btn btn-link waves-effect">@lang('front.general.text_save')</button>
										<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('front.general.text_close')</button>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection

@push('script')
    <script src="/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
		
		@foreach ($course_student_groups as $index => $item)
			<script>
					var demo1 = $('#duallist{{ $item->id }}').bootstrapDualListbox({
							nonSelectedListLabel: 'Non-selected',
							selectedListLabel: 'Selected',
							preserveSelectionOnMove: 'moved',
							moveOnSelect: true
					});

					$(function () {
							demo1.trigger('bootstrapDualListbox.refresh' , true);
					});
			</script>
		@endforeach

@endpush