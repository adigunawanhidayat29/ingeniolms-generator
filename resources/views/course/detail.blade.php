@extends('layouts.app')

@section('title', $course->title)

@push('meta')
  <meta name="description" content="{{$course->subtitle}}. {{strip_tags($course->description)}}" />
  <meta name="keywords" content="elearning {{$course->title}}, ecourse {{$course->title}}, training {{$course->title}}, kelas {{$course->title}}" />

  <meta property="og:type" content="article"/>
  <meta property="og:title" content="{{$course->title}}"/>
  <meta property="og:description" content="{{strip_tags($course->description)}}"/>
  <meta property="og:image" content="{{$course->image}}"/>
  <meta property="og:url" content="{{url('course/' . $course->slug )}}"/>
  <meta property="og:site_name" content="Ingenio"/>
  <meta property="article:publisher" content="https://www.facebook.com/ingeniocoid/"/>
  <meta property="og:image:width" content="640"/>
  <meta property="og:image:height" content="424"/>

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="{{$course->title}}">
  <meta name="twitter:description" content="{{strip_tags($course->description)}}">
  <meta name="twitter:image" content="{{$course->image}}">
  <meta name="twitter:site" content="@ingeniocoid">
@endpush

@push('style')
  <style media="screen">
    .ms-navbar{
      margin-bottom: 0;
    }

    .no-radius{
      border-radius: 0;
    }

    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
    }

    .dq-description-max{
      overflow: hidden;
      min-height: 20px;
      max-height: 300px;
      display: -webkit-box!important;
      -webkit-box-orient: vertical;
      white-space: normal;
      margin:0;
    }

    .mt-20{
      margin-top: 20rem;
    }

    .detailSide{
      position: absolute;
      width: 380px;
      left: 38%;
      top: 10%;
      transform: translate(100%, 0%);
    }

    @media (max-width:1366px){
      .detailSide{
        position: absolute;
        width: 380px;
        left: 36%;
        top: 15%;
        transform: translate(100%, 0%);
      }
    }

    @media (max-width:768px){
      .center{
        text-align: center !important;
      }

      .mt-20{
        margin-top: auto !important;
      }
    }
  </style>

  <style media="screen">
    .play-preview{
      color: #dcdde1;
      font-size:95px;
      padding: 10px;
    }

    .play-preview:hover{
      color: #f5f6fa;
      font-size:100px;
    }

    a#previewContent {
      display: inline-block;
      position: relative;
    }

    a#previewContent .btn-preview {
      position: absolute;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 100%;
      height: 100%;
      transition: all 0.2s;
      z-index: 30;
    }

    a#previewContent:hover .btn-preview {
      background: rgba(0, 0, 0, 0.2);
    }

    a#previewContent .btn-preview i {
      background: rgba(0, 0, 0, 0.75);
      display: flex;
      align-items: center;
      justify-content: center;
      width: 100px;
      height: 100px;
      padding: 25px;
      border-radius: 100%;
      opacity: 1;
      font-size: 50px;
      transition: all 0.2s;
      z-index: 28;
    }

    a#previewContent:hover .btn-preview i {
      width: 120px;
      height: 120px;
      font-size: 70px;
    }

    #desktop{
      display: block;
    }

    #mobile{
      display: none;
    }

    @media (max-width:767px){
      #desktop{
        display: none;
      }

      #mobile{
        display: block;
      }

      .detailSide{
        display: none;
      }

      .col-md-6 p{
        margin: 0;
      }
    }

    .card-md.set-desktop {
      display: block;
    }

    .card-md.set-mobile {
      display: none;
    }

    @media (max-width: 767px) {
      .card-md.set-desktop {
        display: none;
      }

      .card-md.set-mobile {
        display: block;
        width: 100%;
        background: #fff;
        box-shadow: 0px -5px 20px rgba(0, 0, 0, 0.1);
        position: fixed;
        left: 0;
        bottom: 0;
        z-index: 999;
      }
    }
  </style>
  <style media="screen">
    .course-detail span{
      font-family: Roboto,sans-serif !important;
      font-size: 15px !important;
    }
    .list-group{
      border: 0;
    }
    .list-group li.list-group-item{
      border: 1px solid #eee;
    }
  </style>

  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">
@endpush

@section('content')
  @php
    $enrollmentSelected = [];
    if (isset($course->enrollment_type) && $course->enrollment_type != null) {
      $enrollmentSelected = explode(',', $course->enrollment_type);
    } else {
      $enrollmentSelected = explode(',', Setting('enrollment')->value);
    }
  @endphp

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="{{ url('/') }}">@lang('front.detail_course.breadcrumb_home')</a></li>
          <li><a href="{{ url('courses') }}">@lang('front.detail_course.breadcrumb_first')</a></li>
          <li>{{$course->title}}</li>
          {{-- <li>{{$course->category}}</li> --}}
        </ul>
      </div>
    </div>
  </div>

  <div class="top-title-bg">
    <div class="container" style="position: relative;">
      <div class="row">
        <div class="col-md-8">
          <h2 class="headline-md mt-0"><span>{{$course->title}}</span></h2>
          <p style="font-size: 18px; margin-bottom: 0;">{{$course->subtitle}}</p>
          <span>
            <i class="fa fa-star color-warning"></i>
            <i class="fa fa-star color-warning"></i>
            <i class="fa fa-star color-warning"></i>
            <i class="fa fa-star color-warning"></i>
            <i class="fa fa-star color-warning"></i>
            <span style="padding: 0 7px;">5.0 (1 @lang('front.detail_course.rating_text'))</span>
          </span>
          <span style="padding: 0 7px;">{{$course->category}}</span>
          <span style="padding: 0 7px;">{{$course->level}}</span>
          <br>
          <span>@lang('front.detail_course.contributor') <a href="/user/{{$course->author_slug}}"><strong>{{$course->author}}</strong></a></span>
          {{-- <li><strong>Pengajar:</strong> {{$course->author}}</li> --}}
          {{-- <li><strong>Kategori:</strong> {{$course->category}}</li>
          <li><strong>Level:</strong> {{$course->level}}</li> --}}
        </div>
      </div>
    </div>
  </div>

  <!-- CONTENT -->
  <div class="wrap pt-2 pb-2 bg-white mb-2 color-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <!-- DESKRIPSI -->
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>@lang('front.detail_course.description')</span></h2>
            </div>
          </div>
          <div>
            <div id="description" class="course-detail">
              <p>{!! $course->description !!}</p>
            </div>
            <a class="btn-more" id="more-deskripsi">+ @lang('front.detail_course.description_button')</a>
          </div>

          <!-- TUJUAN -->
          {{-- <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>Tujuan</span></h2>
            </div>
          </div>
          <div>
            <div id="target" class="course-detail">
              <p>{!! $course->goal !!}</p>
            </div>
            <a class="btn-more" id="more-tujuan">+ Selengkapnya</a>
          </div> --}}

          <!-- MATERI -->
          <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
            <div class="row">
              <div class="col-md-12">
                <h2 class="headline-md headline-line">@lang('front.detail_course.material_for_course')</h2>
              </div>
            </div>
            <?php $i=1; $video_durations = 0 ?>
            <div class="panel-group" id="accordion fade in active">
              @foreach($sections as $section)
               <div class="panel card-panel">
                 <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
                   <div class="panel-heading" style="background:#f9f9f9">
                     <h4 class="panel-title">
                        <span style="color:#474545; font-size:13px">@lang('front.detail_course.topic_text') {{$i}}:</span>
                        <br/><br/>
                        <strong style="color:#2e2e2e">{{ $section['title'] }}</strong>
                     </h4>
                   </div>
                 </a>
                 <div id="content{{$i}}" class="panel-collapse collapse {{$i == 1 ? 'show' : ''}}">
                   <div class="panel-body" style="background:white;">
                    <ul class="list-group">
                      @foreach($section['section_contents'] as $content)
                        @if($content->preview == '1') <a id="previewContent" data-id="{{$content->id}}" href="#/"> @endif
                          @if($content->type_content == 'label')
                            <div class="label-dq-content mb-1 mt-1">
                              {!!$content->description!!}
                            </div>
                          @else
                            <li class="list-group-item">
                              <p class="color-dark">
                                <i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}
                                <div class="ml-auto">
                                  @if($content->type_content == 'video')
                                    <span class="text-right">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</span> &nbsp;
                                    @php $video_durations += $content->video_duration @endphp
                                  @endif
                                  @if($content->type_content == 'url-video')
                                    <span class="text-right">{{ getYoutubeDuration($content->full_path_file) }}</span> &nbsp;
                                    @php $video_durations += $content->video_duration @endphp
                                  @endif
                                  @if($content->preview == '0')
                                    <i style="color:#bbb" class="fa fa-lock"></i>
                                  @else
                                    <i style="color:#bbb" class="fa fa-unlock"></i>
                                  @endif
                                </div>
                              </p>
                            </li>
                          @endif
                        @if($content->preview == '1') </a> @endif
                      @endforeach
                      @foreach($section['section_quizzes'] as $quiz)
                        <li class="list-group-item">
                          <p class="color-dark">
                            <i class="fa fa-star"></i> {{$quiz->name}}
                            <div class="ml-auto">
                              <i style="color:#bbb" class="fa fa-lock"></i>
                            </div>
                          </p>
                        </li>
                      @endforeach
                    </ul>
                   </div>
                 </div>
               </div>
              <?php $i++; ?>
              @endforeach
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div style="position: sticky; top: 100px; margin-top: -250px;">
            <div class="card-md set-desktop">
              @if($content_previews)
                <a id="previewContent" data-id="{{$content_previews[0]['id']}}" href="#/">
                  <div class="btn-preview">
                    <i class="fa fa-play color-white"></i>
                  </div>
                  <img src="{{$course->image}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
                </a>
              @else
                <img src="{{$course->image}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
              @endif
              <div class="card-block">
                @if($course->archive != '1')
                  <h3 class="text-center color-dark mt-0 mb-1" style="font-family: sans-serif; font-weight: 600;">{{$course->price == 0 ? Lang::get('front.detail_course.free_price') : rupiah($course->price)}}</h3>
                  @php
                    if(Request::get('aff')) {
                      $affiliate = '?aff='.Request::get('aff');
                    }else{
                      $affiliate = '';
                    }
                  @endphp
                  @if($course->price === 0)
                    @if(in_array("self", $enrollmentSelected))
                      {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                        <input type="hidden" name="id" value="{{$course->id}}">
                        <input type="hidden" name="slug" value="{{$course->slug}}">
                        <button type="submit" class="btn btn-success btn-raised btn-block no-shadow no-radius">@lang('front.detail_course.join_button')</button>
                      {{Form::close()}}
                    @endif
                  @else
                    @if($CourseOnCart === false)
                      @if(in_array("self", $enrollmentSelected))
                        {{Form::open(['url' => 'cart/buy/'.$course->id . $affiliate, 'method' => 'post'])}}
                          <button type="submit" name="addCart" class="btn btn-success btn-raised btn-block no-shadow no-radius">@lang('front.detail_course.buy_button')</button>
                        {{Form::close()}}
                      @endif
                    @endif
                    @if(in_array("self", $enrollmentSelected))
                      @if($CourseOnCart === false)
                        {{Form::open(['url' => 'cart/add/'.$course->id . $affiliate, 'method' => 'post'])}}
                          <button type="submit" name="addCart" class="btn btn-primary btn-raised btn-block no-shadow no-radius ">@lang('front.detail_course.add_cart_button')</button>
                        {{Form::close()}}
                        @else
                        <a href="{{url('cart')}}" class="btn btn-primary btn-raised btn-block no-shadow no-radius">@lang('front.detail_course.open_cart_button')</a>
                      @endif
                    @endif
                    @if(in_array("self", $enrollmentSelected))
                      @if($course->password)
                        <a href="#" data-target="{{Auth::check() ? '#CourseCheckPassword' : '#ms-account-modal'}}" data-toggle="modal" class="btn btn-info btn-raised btn-block no-shadow no-radius">@lang('front.detail_course.join_by_code')</a>
                      @endif
                    @endif
                  @endif
                @endif
                <div class="color-dark">
                  <b>@lang('front.detail_course.include_text'):</b>
                  <ul>
                    {{-- <li>Total Durasi video <span id="video_durations"></span> </li> --}}
                    <li>@lang('front.detail_course.lifetime_access_text')</li>
                    <li>@lang('front.detail_course.mobile_access_text')</li>
                  </ul>
                </div>
                <hr>
                <div class="text-center">
                  <p>@lang('front.detail_course.share_text'):</p>
                  <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
                  <a target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
                  @if(Auth::check())
                    @if(isAffiliate(Auth::user()->id) != false)
                      <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy" data-clipboard-text="{{url('course/' . $course->slug ) . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
                    @endif
                  @endif
                </div>
              </div>
            </div>

            <div id="desktop" class="mt-3">
              <h2 class="headline-md text-center mb-1">@lang('front.detail_course.about_contributor')</h2>
              <div class="card-md">
                <div class="card-block text-center">
                  <a href="/user/{{$course->author_slug}}">
                    <img src="{{avatar($course->photo)}}" class="dq-frame-round-md">
                  </a>
                  <a href="/user/{{$course->author_slug}}">
                    <h3 style="font-weight:900;">{{$course->author}}</h3>
                  </a>
                  <div class="row text-left">
                    <div class="col-md-6 center">
                      <p><i class="fa fa-star-o"></i> {{$rate}} @lang('front.detail_course.rating_text')</p>
                      <p><i class="fa fa-comments-o"></i> {{$count}} @lang('front.detail_course.review_text')</p>
                    </div>
                    <div class="col-md-6 center">
                      <p><i class="fa fa-users"></i> {{$total_students}} @lang('front.detail_course.student_text')</p>
                      <p><i class="fa fa-play-o"></i> {{$total_courses}} @lang('front.detail_course.course_text')</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="card-md set-mobile">
            <div class="card-body d-flex align-items-center">
              <p class="text-center color-dark" style="font-family:sans-serif; font-size: 24px; font-weight: 600; margin: 0;">{{$course->price == 0 ? Lang::get('front.detail_course.free_price') : rupiah($course->price)}}</p>
              <div class="d-flex ml-auto">
                <a class="btn-circle btn-circle-raised btn-circle-info mx-1" href="#" title="Detail Kelas" data-toggle="modal" data-target="#infoModal"><i class="fa fa-info"></i></a>
                {{-- <a class="btn-circle btn-circle-raised btn-circle-success" href="#" title="Enroll Sekarang"><i class="fa fa-arrow-right"></i></a> --}}
                {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                  <input type="hidden" name="id" value="{{$course->id}}">
                  <input type="hidden" name="slug" value="{{$course->slug}}">
                  <button type="submit" class="btn-circle btn-circle-raised btn-circle-success mx-1" title="Enroll Sekarang"><i class="fa fa-arrow-right"></i></button>
                {{Form::close()}}
              </div>
            </div>
          </div>
        </div>

        <div id="mobile" class="col-md-4 color-dark text-center mt-20">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line">@lang('front.detail_course.about_contributor')</h2>
            </div>
          </div>
          <div class="card-md">
            <div class="card-block">
              <a href="/user/{{$course->author_slug}}">
                <img width="120" height="120" src="{{avatar($course->photo)}}" class="dq-frame-round-md">
              </a>

              <a href="/user/{{$course->author_slug}}">
                <h3 style="font-weight:900;">{{$course->author}}</h3>
              </a>
              <div class="row text-left">
                <div class="col-md-6 center">
                  <p><i class="fa fa-star-o"></i> {{$rate}} @lang('front.detail_course.rating_text')</p>
                  <p><i class="fa fa-comments-o"></i> {{$count}} @lang('front.detail_course.review_text')</p>
                </div>
                <div class="col-md-6 center">
                  <p><i class="fa fa-users"></i> {{$total_students}} @lang('front.detail_course.student_text')</p>
                  <p><i class="fa fa-play-o"></i> {{$total_courses}} @lang('front.detail_course.course_text')</p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Info -->
  <div class="modal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="infoModalLabel">@lang('front.detail_course.header_modal')</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                  @if($content_previews)
                    <a id="previewContent" data-id="{{$content_previews[0]['id']}}" href="#/">
                      <div class="btn-preview">
                        <i class="fa fa-play color-white"></i>
                      </div>
                      <img src="{{$course->image}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
                    </a>
                  @else
                    <img src="{{$course->image}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
                  @endif
                  @if($course->archive != '1')
                    <h3 class="text-center color-dark" style="font-family:sans-serif;font-weight:600;">{{$course->price == 0 ? Lang::get('front.detail_course.free_price') : rupiah($course->price)}}</h3>
                    @php
                      if(Request::get('aff')) {
                        $affiliate = '?aff='.Request::get('aff');
                      }else{
                        $affiliate = '';
                      }
                    @endphp
                    @if($course->price === 0)
                      @if(in_array("self", $enrollmentSelected))
                        {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                          <input type="hidden" name="id" value="{{$course->id}}">
                          <input type="hidden" name="slug" value="{{$course->slug}}">
                          <button type="submit" class="btn btn-success btn-raised btn-block">@lang('front.detail_course.join_button')</button>
                        {{Form::close()}}
                      @endif
                    @else
                      @if($CourseOnCart === false)
                        @if(in_array("self", $enrollmentSelected))
                          {{Form::open(['url' => 'cart/buy/'.$course->id . $affiliate, 'method' => 'post'])}}
                            <button type="submit" name="addCart" class="btn btn-success btn-raised btn-block">@lang('front.detail_course.buy_button')</button>
                          {{Form::close()}}
                        @endif
                      @endif
                      @if($CourseOnCart === false)
                        @if(in_array("self", $enrollmentSelected))
                          {{Form::open(['url' => 'cart/add/'.$course->id . $affiliate, 'method' => 'post'])}}
                            <button type="submit" name="addCart" class="btn btn-primary btn-raised btn-block ">@lang('front.detail_course.add_cart_button')</button>
                          {{Form::close()}}
                        @endif
                      @else
                        @if(in_array("self", $enrollmentSelected))
                          <a href="{{url('cart')}}" class="btn btn-primary btn-raised btn-block">@lang('front.detail_course.open_cart_button')</a>
                        @endif
                      @endif
                      @if($course->password)
                        @if(in_array("self", $enrollmentSelected))
                          <a href="#" data-target="{{Auth::check() ? '#CourseCheckPassword' : '#ms-account-modal'}}" data-toggle="modal" class="btn btn-info btn-raised btn-block">@lang('front.detail_course.join_by_code')</a>
                        @endif
                      @endif
                    @endif
                  @endif
                  <div class="color-dark">
                    <b>@lang('front.detail_course.include_text'):</b>
                    <ul>
                      {{-- <li>Total Durasi video <span id="video_durations"></span> </li> --}}
                      <li>@lang('front.detail_course.lifetime_access_text')</li>
                      <li>@lang('front.detail_course.mobile_access_text')</li>
                    </ul>
                  </div>
                  <hr>
                  <div class="text-center">
                    <p>@lang('front.detail_course.share_text'):</p>
                    <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
                    <a target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
                    @if(Auth::check())
                      @if(isAffiliate(Auth::user()->id) != false)
                        <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy" data-clipboard-text="{{url('course/' . $course->slug ) . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
                      @endif
                    @endif
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal Info -->

  <!-- Modal -->
  <div class="modal" id="CourseCheckPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">@lang('front.detail_course.header_password_modal')</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>@lang('front.detail_course.password_label')</label>
                  <input type="password" class="form-control" id="CoursePassword" placeholder="@lang('front.detail_course.password_label')">
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('front.detail_course.close_button')</button>
                  <button id="CheckPassword" type="button" class="btn btn-primary">@lang('front.detail_course.join_button')</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->

  <!-- preview konten -->
  <div class="modal" id="modalPreviewContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content" style="background:#2f3640">
              <div class="modal-header">
                  <h3 class="modal-title" id="previewTitle" style="color: white;"></h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div id="previewKonten" style="min-height:300px; color:white !important"></div>
                <div class="">
                  <br>
                  <p style="color: white; font-size:16px">@lang('front.detail_course.free_sample_video'):</p>
                  <ul class="list-group" style="list-style:none">
                    @foreach ($content_previews as $key => $content_preview)
                      <a id="previewContent" data-id="{{$content_preview['id']}}" href="#/">
                        <li class="list-group-item" style="list-style:none">
                          <p class="color-dark">
                            <i class="{{content_type_icon($content_preview['type_content'])}}"></i> {{$content_preview['title']}}
                          </p>
                        </li>
                      </a>
                    @endforeach
                  </ul>
                </div>
              </div>
          </div>
      </div>
  </div>
  <!-- preview konten -->
@endsection

@push('script')
  <script src="/videojs/ie8/videojs-ie8.min.js"></script>
  <script src="/videojs/video.js"></script>
  <script src="/videojs/Youtube.min.js"></script>
  <script src="/videojs/videojs-markers.min.js"></script>
  <script src="/videojs/silvermine-videojs-quality-selector.min.js"></script>
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('clipboard/clipboard.min.js')}}"></script>

	<script type="text/javascript">
		var clipboard = new ClipboardJS('.btn-copy');
		clipboard.on('success', function(e) {
	    swal("Berhasil!", "Silakan bagikan link ini untuk referal Anda: " + e.text, "success");
	    e.clearSelection();
		});
	</script>

  <script type="text/javascript">
    $("#video_durations").html('{{$video_durations >= 3600 ? gmdate("H", $video_durations) . ' jam' : gmdate("i", $video_durations) . ' menit'}}')
    // $("#video_durations").html('link');
  </script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/course/check-password/{{$course->id}}',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          if(response === 'true'){
            $.ajax({
              url : '/course/enroll',
              type : 'get',
              data : {
                id : {{$course->id}},
                _token : '{{csrf_token()}}',
              },
              success : function(data){
                window.location.assign('/course/learn/{{$course->slug}}')
              }
            })
          }else{
            alert('Password salah');
          }
        }
      })
    })
  </script>

  <script type="text/javascript">
    if($('#description').height() > 300){
      $('#description').addClass('dq-description-max');
      $('#more-deskripsi').addClass('show');
    }
    if($('#target').height() > 300){
      $('#target').addClass('dq-description-max');
      $('#more-tujuan').addClass('show');
    }

    $('#more-deskripsi').click(function(){
      $('#description').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-deskripsi').show();
    });
    $('#more-tujuan').click(function(){
      $('#target').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-tujuan').show();
    });
  </script>

  <script type="text/javascript">
    $(function(){
      $(document).on('click', '#previewContent', function(){
        var content_id = $(this).attr('data-id');
        // alert(content_id);

        $.ajax({
          url : '/course/content',
          type : 'post',
          data : {
            'id' : content_id,
            '_token' : '{{csrf_token()}}'
          },
          beforeSend: function(){
            $("#modalPreviewContent").modal('show');
            $("#previewKonten").html('Mohon tunggu...');
          },
          success : function(result){
            // console.log(result);
            $("#previewTitle").html(result.title);

            if(result.type_content == 'video'){
              $("#previewKonten").html('<video id="my-video" class="video-js vjs-default-skin vjs-16-9 vjs-big-play-centered" controls="true" autoplay="true" preload="auto" width="100%" height="500"><source src="'+result.content+'" type="video/mp4" selected="true" label="360p"></video>');
              videojs('my-video', {playbackRates: [0.5, 1, 1.5, 2],controlBar: {fullscreenToggle: true},});
              var player = videojs('my-video');
              player.on('contextmenu', function(e) {
                  e.preventDefault();
              });

            } else if(result.type_content == 'url-video'){
              $("#previewKonten").html('<video id="my-video" class="video-js vjs-default-skin vjs-16-9 vjs-big-play-centered" controls="true" autoplay="true" preload="auto" width="100%" height="500"><source src="'+result.content+'" type="video/youtube"></video>');
              videojs('my-video', {playbackRates: [0.5, 1, 1.5, 2],controlBar: {fullscreenToggle: true},});
              var player = videojs('my-video');
              player.on('contextmenu', function(e) {
                  e.preventDefault();
              });

            }else if(result.type_content == 'file'){
              $("#previewKonten").html('<iframe width="100%" height="500px" src="'+result.content+'/preview"></iframe><div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>');
            }else{
              $("#previewKonten").html(result.content)
            }
          },
          error: function(){
            $("#previewKonten").html('Mohon maaf. Terjadi kesalahan');
          }
        });

        if(videojs('my-video')){
          videojs('my-video').dispose();
        }

      })
    })
  </script>

  <script type="text/javascript">
    $('#modalPreviewContent').on('hidden.bs.modal', function () {
      $("#previewKonten").html('');
      videojs('my-video').dispose();
    })
  </script>
@endpush
