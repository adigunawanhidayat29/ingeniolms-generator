@extends('layouts.app')

@section('title')
	{{ 'Buat Kuis ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
	<style>
		.hidden{
			display: none;
		}

		#accordionExample p{
			margin: 10px;
		}
		#accordionExample .form-group{
			margin-top: 0
		}

		input:disabled,
		input[disabled]{
		  border: 1px solid #999999;
		  background-color: #cccccc;
		  color: #666666;
		}

		.form-control[disabled], .form-group .form-control[disabled] {
			border: 1px solid #999999;
		  background-color: #cccccc;
		  color: #666666;
		}
	</style>
@endpush

@section('content')
<div class="bg-page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="headline-md no-m">@lang('front.content_create.create') <span>@lang('front.content_create.quiz')</span></h2>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li><a href="#">@lang('front.content_create.teacher')</a></li>
				<li><a href="/course/dashboard">@lang('front.content_create.manage_course')</a></li>
				<li><a href="/course/preview/{{$section->id_course}}">@lang('front.content_create.preview')</a></li>
				<li>@lang('front.content_create.create') @lang('front.content_create.quiz') - {{$section->title}}</li>
			</ul>
		</div>
	</div>
</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						{{-- <div class="box-header with-border">
							<h3 class="box-title">{{$button}} Kuis - {{$section->title}}</h3>
						</div> --}}
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										<div class="form-group">
											<label for="name">@lang('front.content_create.content_title_name') @lang('front.content_create.quiz')</label>
											<input placeholder="@lang('front.content_create.content_title_name')" type="text" class="form-control" name="name" value="{{ $name }}" required>
										</div>

										<div class="form-group">
											<label for="title">@lang('front.content_create.description')</label>
											<textarea name="description" id="description">{{ $description }}</textarea>
										</div>
										{{-- <div class="form-group">
											<label for="title">Acak?</label><br>
											<input type="radio" name="shuffle" {{$shuffle == '0' ? 'checked' : '' }} checked value="0">Tidak <br>
											<input type="radio" name="shuffle" {{$shuffle == '1' ? 'checked' : '' }} value="1">Ya
										</div> --}}
										<div class="accordion" id="accordionExample">
										  <div class="">
										    <div class="" id="headingOne">
										      <h2 class="mb-0">
										        <button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										          <i class="fa fa-th-list" aria-hidden="true"></i> Timing
															<hr style="margin-top:10px;margin-bottom:0">
										        </button>
										      </h2>
										    </div>

										    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
										      <div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<p for="name">@lang('front.page_manage_courses.overview_course_start_date')</p>
															</div>
															<div class="col-md-3 input_disabled">
																<div class="form-group">
																	<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ isset($time_start) ? date("Y-m-d", strtotime($time_start)) : date("Y-m-d") }}" required disabled>
																</div>
															</div>
															<div class="col-md-3 input_disabled">
																<div class="form-group">
																	<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ isset($time_start) ? date("H:i:s", strtotime($time_start)) : '08:00' }}" required disabled>
																</div>
															</div>
															<div class="col-sm-2">
																<p>
																	<input type="checkbox" value="1" name="time_start_required" class="activated_input"> @lang('front.content_create.activate')
																</p>
															</div>
														</div>
														<div class="row">
															<div class="col-md-2">
																<p for="name">@lang('front.page_manage_courses.overview_course_end_date')</p>
															</div>
															<div class="col-md-3 input_disabled">
																<div class="form-group">
																	<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{ date("Y-m-d")}}" disabled>
																</div>
															</div>
															<div class="col-md-3 input_disabled">
																<div class="form-group">
																	<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ isset($time_end) ? date("H:i:s", strtotime($time_end)) : '08:00' }}" required disabled>
																</div>
															</div>
															<div class="col-sm-2">
																<p>
																	<input type="checkbox" value="1" name="time_end_required" class="activated_input"> @lang('front.content_create.content_title_name')
																</p>
															</div>
														</div>
														<div class="row">
															<div class="col-md-2">
																<p for="name">@lang('front.content_create.duration') (@lang('front.content_create.minute'))</p>
															</div>
															<div class="col-md-2 input_disabled">
																<div class="form-group">
																	<input placeholder="60" type="number" class="form-control" name="duration" value="{{ $duration }}" required disabled>
																</div>
															</div>
															<div class="col-sm-2">
																<p>
																	<input type="checkbox" value="1" name="duration_required" class="activated_input"> @lang('front.content_create.content_title_name')
																</p>
															</div>
														</div>
										      </div>
										    </div>
										  </div>
											<div class="">
													<div class="" id="grading">
														<h2 class="mb-0 mt-0">
															<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapsegrading" aria-expanded="true" aria-controls="collapseOne">
																<i class="fa fa-th-list" aria-hidden="true"></i> Grade
																<hr style="margin-top:10px;margin-bottom:0">
															</button>
														</h2>
													</div>

													<div id="collapsegrading" class="collapse" aria-labelledby="grading" data-parent="#accordionExample">
														<div class="card-body">
															<div class="row">
																<div class="col-md-2">
																	<p for="name">@lang('front.content_create.grade')</p>
																</div>
																<div class="col-md-2 input_disabled">
																	<div class="form-group">
																		<input placeholder="60" type="number" class="form-control" name="grade" value="100" required disabled>
																	</div>
																</div>
																<div class="col-sm-2">
																	<p>
																		<input type="checkbox" value="1" name="grade_required" class="activated_input"> @lang('front.content_create.content_title_name')
																	</p>
																</div>
															</div>
														</div>
													</div>
											</div>

											<div class="">
													<div class="" id="access">
														<h2 class="mb-0 mt-0">
															<button class="btn btn-link" style="padding-left:10px;width:100%;text-align:left" type="button" data-toggle="collapse" data-target="#collapseaccess" aria-expanded="true" aria-controls="collapseOne">
																<i class="fa fa-th-list" aria-hidden="true"></i> Access
																<hr style="margin-top:10px;margin-bottom:0">
															</button>
														</h2>
													</div>

													<div id="collapseaccess" class="collapse" aria-labelledby="access" data-parent="#accordionExample">
														<div class="card-body">
															<div class="row">
																<div class="col-md-2">
																	<p for="name">@lang('front.content_create.content_for')</p>
																</div>
																<div class="col-md-4">
																	<div class="form-group">
																		<select name="group_student_id" class="form-control selectpicker" id="group_student_id">
																			<option value="all">@lang('front.content_create.content_for_all')</option>
																			<option value="group" {{count($group_has_content) > 0 ? 'selected' : ''}}>@lang('front.content_create.content_for_group')</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-2">
																	<p for="name">@lang('front.content_create.publish')</p>
																</div>
																<div class="col-md-2">
																	<div class="form-group">
																		<p><input type="radio" value="1" name="publish" class="time_end_required" checked> @lang('front.content_create.yes')</p>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="form-group">
																		<p><input type="radio" value="2" name="publish" class="time_end_required"> @lang('front.content_create.no')</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										{{-- <div class="form-group">
											<label for="name">Kesempatan / Percobaan</label>
											<input placeholder="3" type="number" class="form-control" name="attempt" value="{{ $attempt }}" required>
										</div> --}}
										<input placeholder="3" type="hidden" class="form-control" name="attempt" value="{{ $attempt }}">

										<div id="section_show_group">
											@foreach ($course_student_groups as $item)
												<label for="course_student_group_{{$item->id}}" style="color: black !important;">
													<input id="course_student_group_{{$item->id}}" name="course_student_group[]" class="course_student_group" type="checkbox" value="{{$item->id}}" @foreach($group_has_content as $data) {{$data->course_student_group_id == $item->id ? 'checked' : ''}} @endforeach> {{$item->name}}
												</label> <br>
											@endforeach
										</div>

										<div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Quiz" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">@lang('front.content_create.cancel')</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>

	<script>
		@if ($button == "Update")
			@if ($group_has_content->count() > 0)
				$("#section_show_group").show();
			@else
				$("#section_show_group").hide();
			@endif
		@else
			$("#section_show_group").hide();
		@endif

		$("#group_student_id").change(function() {
			if ($(this).val() == 'group') {
				$("#section_show_group").show();
				$("input[name^=course_student_group]").prop('checked', true)
			} else {
				$("#section_show_group").hide();
				$("input[name^=course_student_group]").prop('checked', false)
			}
		})

		$(".activated_input").change(function(){
			var cheked = $(this).prop('checked');
			if(cheked){
				$.each($(this).parents('.row').children('.input_disabled'), function(){
					$(this).find('input').prop('disabled', false);
				})
			}else {
				$.each($(this).parents('.row').children('.input_disabled'), function(){
					$(this).find('input').prop('disabled', true);
				})
			}

		});
	</script>
@endpush
