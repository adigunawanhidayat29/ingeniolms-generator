@extends('layouts.app')

@section('title')
	{{ 'Edit Pertanyaan'}}
@endsection

@push('style')
	<style media="screen">
		.feedback-form textarea{
			background: white!important;
			padding: 10px;
			border: 1px solid #e3e3e3 !important;
			margin-top: 20px;
		}

		.feedback-form{
			display: none;
		}

		.activated{
			display: block;
		}
	</style>

	<style>
		#new_row_answer {
			margin-top: 28px;
		}

		.quiz-form-wrap {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			-ms-flex-align: center!important;
			align-items: center!important;
			margin-right: -15px;
			margin-left: -15px;
		}

		.quiz-form-wrap.no-check,
		.quiz-form-wrap.no-check.no-editor {
			justify-content: flex-end;
		}

		.quiz-form-wrap .quiz-check {
			width: 5%; 
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap .quiz-check .addon-check {
			position: relative;
			display: inline-block;
			width: 20px;
			height: 20px;
			border: 2px solid rgba(0,0,0,.54);
			border-radius: 2px;
			cursor: pointer;
			overflow: hidden;
			margin-top: 10px;
			z-index: 1;
		}

		.quiz-form-wrap .quiz-form {
			width: 86%; 
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap.no-check .quiz-form {
			width: 91%;
		}

		.quiz-form-wrap.no-check.no-editor .quiz-form {
			width: 94%;
		}

		.quiz-form-wrap .quiz-form .form-group label.control-label {
			margin-top: 0;
		}

		.quiz-form-wrap .quiz-form .form-group .form-control {
			margin-bottom: 0;
		}

		.quiz-form-wrap .quiz-action {
			width: 9%; 
			display: flex;
			align-items: center;
			justify-content: space-between;
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap.no-check.no-editor .quiz-action {
			width: 6%;
			justify-content: flex-end;
		}

		@media (max-width: 1199px) {
			.quiz-form-wrap .quiz-check {
				width: 6%;
			}

			.quiz-form-wrap .quiz-form {
				width: 83%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 89%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 93%;
			}

			.quiz-form-wrap .quiz-action {
				width: 11%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 7%;
			}
		}

		@media (max-width: 991px) {
			.quiz-form-wrap .quiz-check {
				width: 8%;
			}

			.quiz-form-wrap .quiz-form {
				width: 77%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 85%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 90%;
			}

			.quiz-form-wrap .quiz-action {
				width: 15%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 10%;
			}
		}

		@media (max-width: 767px) {
			.quiz-form-wrap .quiz-check {
				width: 11%;
			}

			.quiz-form-wrap .quiz-form {
				width: 68%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 79%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 87%;
			}

			.quiz-form-wrap .quiz-action {
				width: 21%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 13%;
			}
		}

		@media (max-width: 480px) {
			.quiz-form-wrap .quiz-check {
				width: 12%;
			}

			.quiz-form-wrap .quiz-form {
				width: 60%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 72%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 81%;
			}

			.quiz-form-wrap .quiz-action {
				width: 28%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 19%;
			}
		}

		@media (max-width: 380px) {
			.quiz-form-wrap .quiz-check {
				width: 12%;
			}

			.quiz-form-wrap .quiz-form {
				width: 56%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 68%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 78%;
			}

			.quiz-form-wrap .quiz-action {
				width: 32%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 22%;
			}
		}
	</style>
@endpush


@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Edit <span>Pertanyaan</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
					<li><a href="/course/dashboard">Kelola Kelas</a></li>
					<li><a href="/course/preview/{{$section->course->id}}">{{$section->course->title}}</a></li>
					<li><a href="/course/quiz/question/manage/{{$quiz->id}}">{{$quiz->name}}</a></li>
					<li>Edit Pertanyaan</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">

									{{ Form::open(array('url' => $action, 'method' => $method)) }}

										{{-- <div class="form-group">
											<label for="name">Tipe Kuis</label>
											<select class="form-control selectpicker" name="quiz_type_id" id="quiz_type_id" required>
												<option value="">Pilih Tipe</option>
												@foreach($quiz_types->where('id', '<=', 7) as $quiz_type)
													<option {{ $quiz_type->id == $quiz_type_id ? 'selected' : '' }} value="{{$quiz_type->id}}">{{$quiz_type->type}}</option>
												@endforeach()
											</select>
										</div> --}}
										<input type="hidden" name="quiz_type_id" value="{{$quiz_type_id}}">

										<div class="form-group">
											@if($quiz_type_id != '4')
												<label for="title">Pertanyaan</label>
											@endif
											<textarea name="question" id="question">{!! $question !!}</textarea>
											<div class="feedback-form {{$quiz_question->feedback_required == 1 ? 'activated' : ''}}">
												<textarea name="feedback" class="form-control" placeholder="Feedback" name="feedback_question">{{$quiz_question->feedback}}</textarea>
											</div>
										</div>

										@if($quiz_type_id != '4')
											<div class="form-group">
												<label for="title">Poin</label>
												<input type="number" name="weight" class="form-control" value="{{$weight}}">
											</div>
										@endif

										{{-- <div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('course/quiz/question/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div> --}}

										@if($quiz_type_id != '4')
											<div id="new_row_answer">
												@php $index = 0 @endphp
												@foreach($answers as $index => $answer)
													<div class="card-sm mb-2" id="row_answer{{$index}}">
														<div class="card-block bg-white">
															@if($quiz_type_id == '1' || $quiz_type_id == '2')
																<div class="quiz-form-wrap">
															@else
																<div class="quiz-form-wrap no-check">
															@endif
																@if($quiz_type_id == '1' || $quiz_type_id == '2')
																	<div class="quiz-check">
																		<div class="checkbox">
																			<label class="m-0" title="Tentukan jawaban benar">
																				<input type="hidden" value="{{$answer->answer_correct}}" id="correctAnswertext{{$index}}" name="old_answer_correct[]">
																				<input type="checkbox" name="" id="correctAnswerCheck" data-increment="{{$index}}" value="1" {{$answer->answer_correct == '1' ? 'checked' : ''}}>
																			</label>
																		</div>
																	</div>
																@endif
																<div class="quiz-form">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukkan pilihan</label>
																		<input autocomplete="off" type="text" name="old_answer[]" id="text_answer{{$index}}" value="{{$answer->answer}}" class="form-control">
																		<input type="hidden" name="old_answer_id[]" value="{{$answer->id}}">
																		@if($quiz_type_id == '3')
																			<input type="hidden" name="old_answer_ordering[]" value="{{$index}}">
																		@endif
																		<div id="text_answer_editor_place{{$index}}"></div>
																	</div>
																</div>
																<div class="quiz-action">
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="{{$index}}" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" answer-id="{{$answer->id}}" data-id="{{$index}}" title="Hapus"><i class="zmdi zmdi-close"></i></a>
																</div>
															</div>

															{{-- <div class="row">
																<div class="col-md-10">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukkan pilihan</label>
																		<input autocomplete="off" type="text" name="old_answer[]" id="text_answer{{$index}}" value="{{$answer->answer}}" class="form-control">
																		<input type="hidden" name="old_answer_id[]" value="{{$answer->id}}">
																		@if($quiz_type_id == '3')
																			<input type="hidden" name="old_answer_ordering[]" value="{{$index}}">
																		@endif
																		<div id="text_answer_editor_place{{$index}}"></div>
																		<a href="#/" id="remove_row_answer" answer-id="{{$answer->id}}" data-id="{{$index}}">hapus</a>
																		<a href="#/" class="answer_editor" data-id="{{$index}}">gunakan editor</a>
																	</div>
																</div>
																<div class="col-md-2">
																	<br>
																	@if($quiz_type_id == '1' || $quiz_type_id == '2')
																		<div class="text-center">
									                    <label>
																				<input type="hidden" value="{{$answer->answer_correct}}" id="correctAnswertext{{$index}}" name="old_answer_correct[]">
								                        <input type="checkbox" name="" id="correctAnswerCheck" data-increment="{{$index}}" value="1" {{$answer->answer_correct == '1' ? 'checked' : ''}}>
									                    </label>
										                </div>
																	@endif
																</div>
															</div> --}}
															<div class="feedback-form">
																<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]">{{$answer->feedback}}</textarea>
															</div>
														</div>
													</div>
												@endforeach
												<input type="hidden" id="choice_increments" value="{{$index}}">
											</div>

											@if(Request::get('quiz_type') != '5')
												<button id="{{$quiz_type_id == '3' || $quiz_type_id == '6' || $quiz_type_id == '7' ? 'add_new_answer_ordering' : 'add_new_answer'}}" type="button" class="btn btn-raised">Tambah Pilihan</button>
											@endif											
										@endif

										<hr>

										{{-- <input type="checkbox" class="feedback-check" name="feedback_required" value="1" {{$quiz_question->feedback_required == 1 ? 'checked' : ''}}> Feedback --}}
										<div class="checkbox">
											<label class="m-0" style="display: inline-flex; align-items: center;">
												<input type="checkbox" class="feedback-check" name="feedback_required" value="1" {{$quiz_question->feedback_required == 1 ? 'checked' : ''}}> Feedback
											</label>
										</div>

										<div class="form-group">
											<input type="hidden" name="remove_answers" id="remove_answers" value="">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('course/quiz/question/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">Edit Answer</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Answer</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">is correct ?</label>
						<select class="form-control" id="edit_text_answer_correct">
							<option value="0">Incorrect</option>
							<option value="1">Correct</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="save_edit_answer">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}

	{{-- open editor --}}
	<div class="modal fade" id="modal_answer_editor" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-body">
					<textarea id="text_answer_editor"></textarea>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Batal</button>
	        <button type="button" class="btn btn-primary btn-raised" id="get_answer_editor">Simpan</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- open editor --}}
@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('question');
		CKEDITOR.replace('text_answer_editor');
	</script>

	<script type="text/javascript">
		var js = document.createElement("script");
		js.type = "text/javascript";
		js.src = "WIRISplugins.js?viewer=image";
		document.head.appendChild(js);
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	{{-- CKEDITOR --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$(".feedback-check").change(function(){
			var cheked = $(this).prop('checked');
			if(cheked){
				$(".feedback-form").addClass('activated');
			}else {
				$(".feedback-form").removeClass('activated');
			}
		});
		
		$("#add_new_answer").click(function(){
			var choice_increments = parseInt($("#choice_increments").val()) + 1
			var cheked = $(".feedback-check").prop('checked');
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="quiz-form-wrap">'+
							'<div class="quiz-check">'+
								'<div class="text-center">'+
									'<label class="m-0" title="Tentukan jawaban benar">'+
										'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
										'<input type="checkbox" class="addon-check" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
									'</label>'+
								'</div>'+
							'</div>'+
							'<div class="quiz-form">'+
								'<div class="form-group no-m">'+
									'<label class="control-label" for="inputDefault">Masukkan pilihan</label>'+
									'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
									'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
								'</div>'+
							'</div>'+
							'<div class="quiz-action">'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="'+choice_increments+'" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="'+choice_increments+'" title="Hapus"><i class="zmdi zmdi-close"></i></a>'+
							'</div>'+
						'</div>'+
						'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
							'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
						'</div>'+
					'</div>'+
				'</div>'

				// '<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
				// '<div class="card-block bg-white">'+
				// '<div class="row">'+
				// 	'<div class="col-md-10">'+
				// 		'<div class="form-group no-m">'+
				// 			'<label class="control-label" for="inputDefault">Masukkan pilihan</label>'+
				// 			'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
				// 			'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
				// 			'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
				// 			'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
				// 		'</div>'+
				// 	'</div>'+

				// 	'<div class="col-md-2">'+
				// 		'<br>'+
				// 		'<div class="text-center">'+
				// 				'<label>'+
				// 					'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
				// 					'<input type="checkbox" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
				// 				'</label>'+
				// 		'</div>'+
				// 	'</div>'+
				// '</div>'+
				// '<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
				// 	'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
				// '</div>'+
				// '</div>'+
				// '</div>'+
				// '<br>'
			);
		})
	</script>

	<script type="text/javascript">
		$(function(){
			$(document).on('change','#correctAnswerCheck',function(e){
				e.preventDefault();
				var data_increment = $(this).attr('data-increment');
				$("#correctAnswertext" + data_increment).val($(this).is(":checked") == true ? '1' : '0')
			});
		})
	</script>
	{{-- add new answer --}}

	{{-- add new answer ordering --}}
	<script type="text/javascript">
		$("#add_new_answer_ordering").click(function(){
			var choice_increments = parseInt($("#choice_increments").val()) + 1
			var cheked = $(".feedback-check").prop('checked');
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="quiz-form-wrap no-check">'+
							'<div class="quiz-form">'+
								'<div class="form-group no-m">'+
									'<label class="control-label" for="focusedInput1">Masukkan pilihan</label>'+
									'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
									'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
									'<input type="hidden" name="answer_ordering[]" value="'+choice_increments+'">'+
								'</div>'+
							'</div>'+
							'<div class="quiz-action">'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="'+choice_increments+'" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="'+choice_increments+'" title="Hapus"><i class="zmdi zmdi-close"></i></a>'+
							'</div>'+
						'</div>'+
						'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
							'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
						'</div>'+
					'</div>'+
				'</div>'

				// '<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
				// 	'<div class="card-block bg-white">'+
				// 		'<div class="form-group">'+
				// 			'<label class="control-label" for="focusedInput1">Masukkan pilihan</label>'+
				// 			'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
				// 			'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
				// 			'<input type="hidden" name="answer_ordering[]" value="'+choice_increments+'">'+
				// 			'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
				// 			'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
				// 			'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
				// 				'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
				// 			'</div>'+
				// 		'</div>'+
				// 	'</div>'+
				// '</div>'+
				// '<br>'
			);
		})
	</script>
	{{-- add new answer ordering --}}

	{{-- remove new answer --}}
	<script type="text/javascript">
		$(function(){
			var removed_answer = [];
			$(document).on('click','#remove_row_answer',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					var answer_id = $(this).attr('answer-id');
					removed_answer.push(answer_id)
					if(answer_id){
						$("#remove_answers").val(removed_answer)
					}
					$("#row_answer"+data_id).remove();
			});
		});
	</script>
	{{-- remove new answer --}}

	{{-- delete answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#delete_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var url_delete_answer =  "{{url('course/quiz/question/answer/delete/'.$quiz->id.'/')}}";
				$.ajax({
					url : url_delete_answer + '/' + data_id,
					type : "GET",
					success : function(){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- delete answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#save_edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var token = '{{ csrf_token() }}';
				$.ajax({
					url : "{{url('course/quiz/question/answer/update_action')}}",
					type : "POST",
					data : {
						answer : $("#edit_text_answer"+data_id).val(),
						answer_correct : $("#edit_text_answer_correct"+data_id).val(),
						id : data_id,
						_token: token
					},
					success : function(result){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- edit answer --}}

	<script>
		$(function(){
			$(document).on('click','.answer_editor',function(e){
				e.preventDefault();
					data_id = $(this).attr('data-id');
					CKEDITOR.instances['text_answer_editor'].setData($("#text_answer" + data_id).val())
					$("#modal_answer_editor").modal('show');
			});
		});

		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});
	</script>
@endpush
