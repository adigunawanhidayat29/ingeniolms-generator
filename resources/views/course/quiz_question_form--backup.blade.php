@extends('layouts.app')

@section('title')
	{{ 'Tambah Pertanyaan ' . $quiz->name }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">

	<style>
		ul.breadcrumb {
				padding: 10px 16px;
				list-style: none;
				background-color: #eee;
		}
		ul.breadcrumb li {
				display: inline;
				font-size: 18px;
		}
		ul.breadcrumb li+li:before {
				padding: 8px;
				color: black;
				content: "/\00a0";
		}
		ul.breadcrumb li a {
				color: #0275d8;
				text-decoration: none;
		}
		ul.breadcrumb li a:hover {
				color: #01447e;
				text-decoration: underline;
		}
	</style>
@endpush

@section('content')
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">

			<br>
			<ul class="breadcrumb">
				<li><a href="/course/dashboard">Pengajar</a></li>
				<li><a href="/course/dashboard">Kelola Kelas</a></li>
				<li><a href="/course/preview/{{$section->course->id}}">{{$section->course->title}}</a></li>
				<li><a href="/course/quiz/question/manage/{{$quiz->id}}">{{$quiz->name}}</a></li>
				<li>Pertanyaan Baru</li>
			</ul>
			<br><br>

			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="panel panel-default">
							  <div class="panel-heading">
							    <h3 class="panel-title">Manage Pertanyaan - {{$quiz->name}}</h3>
							  </div>
							  <div class="panel-body">

									{{-- <h3>Add Question</h3> --}}
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										{{-- <div class="form-group">
											<label for="name">Tipe Kuis</label>
											<select class="form-control selectpicker" name="quiz_type_id" id="quiz_type_id" required>
												<option value="">Pilih Tipe Kuis</option>
												@foreach($quiz_types as $quiz_type)
													<option {{ $quiz_type->id == $quiz_type_id ? 'selected' : ( $quiz_type->id == Request::get('quiz_type') ? 'selected' : '') }} value="{{$quiz_type->id}}">{{$quiz_type->type}}</option>
												@endforeach()
											</select>
										</div> --}}
										<input type="hidden" name="quiz_type_id" value="{{Request::get('quiz_type')}}">

										<div class="form-group">
											<label for="title">Pertanyaan</label>
											<textarea name="question" id="question">{{ $question }}</textarea>
										</div>

										<div class="form-group">
											<label for="title">Poin</label>
											<input type="number" placeholder="poin" class="form-control" name="weight" value="{{$weight}}">
										</div>

										@if(Request::get('quiz_type') == '1' || Request::get('quiz_type') == '2')
											<div class="row">
												<div class="col-md-10">
													<h3>Pilihan Jawaban</h3>
												</div>
												<div class="col-md-2">
													Pilihan Benar ?
												</div>
											</div>

											<div id="new_row_answer">
												@for($i=1; $i<=4; $i++)
													<div class="row" id="row_answer{{$i}}" style="border:1px solid #bbb">
														<div class="col-md-10">
															<div class="form-group label-floating">
																<label class="control-label" for="focusedInput1">Masukan pilihan</label>
																<input type="text" name="answer[]" value="" class="form-control">
																<a href="#/" id="remove_row_answer" data-id="{{$i}}">hapus</a>
															</div>
														</div>
														<div class="col-md-2">
															<br>
															<div class="">
							                    <label>
																		<input type="hidden" value="0" id="correctAnswertext{{$i}}" name="answer_correct[]">
						                        <input type="checkbox" name="" id="correctAnswerCheck" data-increment="{{$i}}" value="1">
							                    </label>
							                </div>
														</div>
													</div>
													<br>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}">
											</div>
											<button id="add_new_answer" type="button" class="btn btn-raised">Tambah Pilihan</button>
											<br><br>
										@endif

										@if(Request::get('quiz_type') == '3')
											<div id="new_row_answer">
												@for($i=1; $i<=4; $i++)
													<div class="row" id="row_answer{{$i}}" style="border:1px solid #bbb">
														<div class="col-md-12">
															<div class="form-group label-floating">
																<label class="control-label" for="focusedInput1">Masukan pilihan</label>
																<input type="text" name="answer[]" value="" class="form-control">
																<input type="hidde" name="answer_ordering[]" value="{{$i}}">
																<a href="#/" id="remove_row_answer" data-id="{{$i}}">hapus</a>
															</div>
														</div>
													</div>
													<br>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}">
											</div>
											<button id="add_new_answer_ordering" type="button" class="btn btn-raised">Tambah Pilihan</button>
											<br><br>
										@endif

										<div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
												<a href="{{ url('course/quiz/question/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>
	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">Edit Answer</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Answer</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">is correct ?</label>
						<select class="form-control" id="edit_text_answer_correct">
							<option value="0">Incorrect</option>
							<option value="1">Correct</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="save_edit_answer">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('question', {
			// removePlugins: 'link,save,preview,print,templates,form,checkbox,font,iframe,pagebreak,smiley,flash,language,creatediv,hiddenfield,showblocks,forms,creatediv'
		});
	</script>

	<script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript">
		$("#quiz_type_id").select2();
	</script>
	{{-- SELECT2 --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$("#add_new_answer").click(function(){
			var choice_increments = $("#choice_increments").val()
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="row" id="row_answer'+choice_increments+'" style="border:1px solid #bbb">'+
					'<div class="col-md-10">'+
						'<div class="form-group label-floating">'+
							'<label class="control-label" for="focusedInput1">Masukan pilihan</label>'+
							'<input type="text" name="answer[]" class="form-control">'+
							'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
						'</div>'+
					'</div>'+

					'<div class="col-md-2">'+
						'<div class="">'+
								'<label>'+
									'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
									'<input type="checkbox" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
								'</label>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<br>'
			);
		})
	</script>

	<script type="text/javascript">
		$(function(){
			$(document).on('change','#correctAnswerCheck',function(e){
				e.preventDefault();
				var data_increment = $(this).attr('data-increment');
				$("#correctAnswertext" + data_increment).val($(this).is(":checked") == true ? '1' : '0')
			});
		})
	</script>
	{{-- add new answer --}}


	{{-- add new answer ordering --}}
	<script type="text/javascript">
		$("#add_new_answer_ordering").click(function(){
			var choice_increments = $("#choice_increments").val()
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="row" id="row_answer'+choice_increments+'" style="border:1px solid #bbb">'+
					'<div class="col-md-10">'+
						'<div class="form-group label-floating">'+
							'<label class="control-label" for="focusedInput1">Masukan pilihan</label>'+
							'<input type="text" name="answer[]" class="form-control">'+
							'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<br>'
			);
		})
	</script>
	{{-- add new answer ordering --}}

	{{-- remove new answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#remove_row_answer',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					$("#row_answer"+data_id).remove();
			});
		});
	</script>
	{{-- remove new answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}
@endpush
