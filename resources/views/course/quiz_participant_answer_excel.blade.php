<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

  <div class="table-responsive">
    <h4>
      Nama Peserta: <b>{{$user->name}}</b>
    </h4>
    <h4>
      {{-- Nilai Peserta: <b class="labelNilaiAkhirPeserta">{{ $quiz->grade }}</b> --}}
    </h4>
    <table class="table table-hover">
      <tr class="bg-primary">
        <th>No</th>
        <th>Tipe Pertanyaan</th>
        <th>Pertanyaan</th>
        <th>Jawaban</th>
        @if($quiz->quizz_type != 'survey')
        <th>Bobot</th>
        <th>Nilai</th>
        @endif
      </tr>

      @php
        $nilaiTotal = 0;
        $nilaiPGTotal = 0;
        $nilaiEssayTotal = 0;
        $nilaiOrderingTotal = 0;
        $i = 1;
      @endphp


      @foreach($quiz_participants as $index => $quiz_participant)

        {{-- TIPE KUIS PILIHAN GANDA / BENAR SALAH --}}
        @if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '7' )
          <tr class="">
            <td>{{$i++}}</td>
            <td>Pilihan Ganda</td>
            <td>{!!$quiz_participant->question!!}</td>
            <td>{!!$quiz_participant->answer!!}</td>
            @if($quiz->quizz_type != 'survey')
            <td>
              {{$quiz_participant->weight}}
              <input type="number" class="grade" hidden value="{{$quiz_participant->answer_correct}}">
            </td>
            <td>
              @php
                  if($quiz_participant->answer_correct == "1"){
                    echo "Benar";
                  }else{
                    echo "Salah";
                  }
              @endphp
            </td>
            @endif
          </tr>

          @if($quiz->quizz_type != 'survey')
          @php
            $nilaiPGTotal += $quiz_participant->answer_correct == "1" ? $quiz_participant->weight : 0;
          @endphp
          @endif

        {{-- TIPE KUIS MENGURUTKAN /ORDERING --}}
        @elseif($quiz_participant->quiz_type_id == '3')
          @php
            $question_answers = DB::table('quiz_question_answers')
              ->whereIn('id', explode(',', $quiz_participant->quiz_question_answer_id))
              ->orderByRaw(DB::raw("FIELD(id, $quiz_participant->quiz_question_answer_id)"))
              ->get();
          @endphp
          <tr class="">
            <td>{{$i++}}</td>
            <td>Mengurutkan</td>
            <td>{!!$quiz_participant->question!!}</td>
            <td>
              @foreach($question_answers as $index => $question_answer)
                {!!$question_answer->answer!!} <br> <br>
              @endforeach
            </td>
            @if($quiz->quizz_type != 'survey')
            <td>
              {{$quiz_participant->weight}}
              @php
                  $sortGrade = intval($quiz_participant->answer_ordering_grade);
                  $sortGrade = $sortGrade * intval($quiz_participant->weight) / 100;
              @endphp
              <input type="number" class="grade" hidden value="{{$sortGrade}}">
            </td>
            <td>
                @if($quiz_participant->answer_ordering_grade == null)
                    <a href="#" id="orderingGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Berikan Nilai Mengurutkan</a>
                @else
                    {{$quiz_participant->answer_ordering_grade}} <a href="#" id="orderingGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Edit Nilai</a>
                @endif
            </td>
            @endif
          </tr>

          @if($quiz->quizz_type != 'survey')
          @php
            $nilaiOrderingTotal += $sortGrade ;
          @endphp
          @endif

        {{-- TIPE KUIS ESSAY --}}
        @elseif($quiz_participant->quiz_type_id == '5')
          <tr class="">
            <td>{{$i++}}</td>
            <td>Essay</td>
            <td>{!!$quiz_participant->question!!}</td>
            <td>
              {!!$quiz_participant->answer_essay!!}
            </td>
            @if($quiz->quizz_type != 'survey')
            <td>
              {{$quiz_participant->weight}}
              @php
                  $essayGrade = intval($quiz_participant->answer_essay_grade);
                  $essayGrade = $essayGrade * intval($quiz_participant->weight) / 100;
              @endphp
              <input type="number" class="grade" hidden value="{{$essayGrade}}">
            </td>
            <td>
              @if($quiz_participant->answer_essay_grade == null)
                <a href="#" class="essayGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Berikan Nilai Essay</a>
              @else
                {{$quiz_participant->answer_essay_grade}} <a href="#" class="essayGrade" data-id="{{$quiz_participant->quiz_participant_answer_id}}">Edit Nilai</a>
              @endif
            </td>
            @endif
          </tr>

          @if($quiz->quizz_type != 'survey')
          @php
            $nilaiEssayTotal += $essayGrade ;
          @endphp
          @endif

        {{-- TIPE KUIS JAWABAN SINGKAT/SHORT ANSWER --}}
        @elseif($quiz_participant->quiz_type_id == '6')
          <tr class="">
            <td>{{$i++}}</td>
            <td>Short Answer</td>
            <td>{!!$quiz_participant->question!!}</td>
            <td>
              {!!$quiz_participant->answer_short_answer!!}
            </td>
            @if($quiz->quizz_type != 'survey')
            <td>
              {{$quiz_participant->weight}}
            </td>
            <td>

            </td>
            @endif
          </tr>

        @endif

      @endforeach

      @php
        $nilaiTotal += ($nilaiPGTotal + $nilaiEssayTotal + $nilaiOrderingTotal);
      @endphp

    </table>

    <div class="text-right">
      <strong class="btn btn-sm btn-success btn-raised">Nilai Total: {{ $nilaiTotal }}</strong>
    </div>

    @php
      // update nilai dengan nilai total
      DB::table('quiz_participants')->where(['user_id' => $user->id, 'quiz_id' => $quiz->id])->update(['grade' => $nilaiTotal]);
    @endphp

  </div>
</body>
</html>
