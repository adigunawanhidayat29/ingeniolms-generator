@extends('layouts.app')

@section('title', 'Course Grades')

@section('content')
	<div class="container">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Course Completion</h3>
		  </div>
		  <div class="panel-body" style="background:white">
				<table class="table table-bordered">
					<tr>
						<th>Name</th>
						<th>Title</th>
					</tr>
					@foreach($progresses as $progress)
						<tr>
							<td>{{$progress->name}}</td>
							<td>{{$progress->title}}</td>
						</tr>
					@endforeach
				</table>
		  </div>
		</div>
	</div>

@endsection
