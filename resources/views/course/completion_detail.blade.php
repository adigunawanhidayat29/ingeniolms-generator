@extends('layouts.app')

@section('title', 'Course Completions Progress')

@push('style')
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
@endpush

@section('content')
	<div class="bg-page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@php
						$get_user = DB::table('users')->where('id', $user_id)->first();
					@endphp
					<h2 class="headline-md no-m">Detail Pembelajaran <span>{{$get_user->name}}</span></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
          <li><a href="/course/dashboard">Kelola Kelas</a></li>
					<li><a href="/course/preview/{{$Course->id}}">{{$Course->title}}</a></li>
					<li><a href="/course/atendee/{{$Course->id}}">Daftar Peserta</a></li>
					<li>Detail Pembelajaran</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-raised btn-default no-shadow mt-0 mb-2" href="/course/atendee/{{$Course->id}}"><i class="zmdi zmdi-arrow-left"></i> Kembali</a>
					<div class="card card-groups">
						<div class="card-block">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr style="background-color: #e6e6e6;">
											<th>Section</th>
											<th>Content</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										@foreach($sections as $section)
											@foreach($section['section_contents'] as $content)
												@php
													$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => $user_id, 'status' => '1'])->first();
												@endphp
												<tr style="{{$content_progress ? 'background-color: #eee; color: #999;' : ''}}">
													<td>{{$section['title']}}</td>
													<td>{{$content->title}}</td>
													<td>
														<span class="badge {{ $content_progress ? 'badge-success' : 'badge-danger' }}">{{ $content_progress ? 'Selesai' : 'Belum selesai' }}</span>
													</td>
												</tr>
											@endforeach
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			{{-- <div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Course Completion Progress - {{$Course->title}} - </h3>
			  </div>
			  <div class="panel-body" style="background:white">
					<a class="btn btn-default btn-raised btn-sm" href="/course/completion/{{$Course->id}}"><i class="fa fa-chevron-left"></i> Back to {{$Course->title}}</a>
					<table class="table table-bordered">
						<tr>
							<th>Section</th>
							<th>Content</th>
							<th></th>
						</tr>
						@foreach($sections as $section)
							@foreach($section['section_contents'] as $content)
								@php
									$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => $user_id, 'status' => '1'])->first();
								@endphp
							<tr>
								<td>{{$section['title']}}</td>
								<td>{{$content->title}}</td>
								<td>
									<button type="button" class="btn btn-sm btn-raised {{$content_progress ? 'btn-success' : 'btn-danger'}}">{{$content_progress ? 'Sudah' : 'Belum'}}</button>
								</td>
							</tr>
							@endforeach
						@endforeach
					</table>
			  </div>
			</div> --}}
			
		</div>
	</div>
@endsection
