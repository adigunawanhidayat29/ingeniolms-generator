@extends('layouts.app')

@section('title')
	{{ 'Import Quiz' }}
@endsection


@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Import <span>Pertanyaan</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
					<li><a href="/course/dashboard">Kelola Kelas</a></li>
				  <li><a href="#">"Nama Kelas"</a></li>
				  <li><a href="#">"Nama Kuis"</a></li>
					<li>Import Pertanyaan</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
	  <div class="container">

			<a href="/quiz_format/default_format.xlsx" class="btn btn-raised btn-success">Download Format Quiz</a>

	    <div class="card-md">
	      <!-- <div class="panel-heading">
	        <h3 class="panel-title">Import Question & Answer</h3>
	      </div> -->

	      <div class="card-block">
	        <form action="/course/quiz/question/import_store/{{Request::segment(5)}}" method="post" enctype="multipart/form-data">
	          {{csrf_field()}}
	          <div class="form-group no-m">
	            <label for="">Pilih file yang akan di import</label>
	            <div class="col-md-12">
	              <input type="text" readonly="" class="form-control" placeholder="Browse...">
	              <input type="file" name="file">
	            </div>
	          </div>
	          <input type="submit" value="Import" class="btn btn-primary btn-raised">
	        </form>
	      </div>
	    </div>
	  </div>
	</div>

@endsection
