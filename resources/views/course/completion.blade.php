@extends('layouts.app')

@section('title', 'Course Completions ' . $Course->title)

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Penyelesaian <span>Peserta</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Pengajar</a></li>
          <li><a href="/course/preview/{{Request::segment(3)}}">Progress Peserta</a></li>
          <li>Progress Peserta</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<!-- <div class="card-md"> -->
			  <!-- <div class="panel-heading">
			    <h3 class="panel-title">Course Completion - {{$Course->title}}</h3>
			  </div> -->
			  <!-- <div class="card-block"> -->
          <h2 class="headline-sm no-m">Course Completion - <span>{{$Course->title}}</span></h2>
          <a href="/course/completion/{{$Course->id}}/export" target="_black" class="btn btn-success btn-raised">Download Excel</a>
					<table class="table table-bordered mt-2">
						<tr class="bg-primary">
							<th>Nama</th>
							<th>Pesentase Progres</th>
							<th>Pilihan</th>
						</tr>
						@foreach($completions as $completion)
							<tr class="bg-white">
								<td>{{$completion['name']}}</td>
								<td>{{round($completion['percentage'], 2)}}%</td>
								<td>
                  <a class="btn btn-primary btn-raised btn-sm" href="/course/completion/detail/{{Request::segment(3)}}/{{$completion['user_id']}}">Detail</a>
                  {{-- <a onclick="return confirm('Yakin akan menghapus peserta ini?')" class="btn btn-danger btn-raised btn-sm" href="/course/completion/delete/{{Request::segment(3)}}/{{$completion['user_id']}}">Hapus</a> --}}
                </td>
							</tr>
						@endforeach
					</table>
			  <!-- </div> -->
			<!-- </div> -->
		</div>
	</div>
@endsection
