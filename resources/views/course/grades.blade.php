@extends('layouts.app')

@section('title', Lang::get('front.page_manage_gradebook.title') . " - " . $course->title)

@push('style')
  <style media="screen">
    .link-grade{
      text-decoration: underline;
    }
  </style>
  <style>
		.nav-tabs-ver-container .nav-tabs-ver {
			padding: 0;
			margin: 0;
		}

		.nav-tabs-ver-container .nav-tabs-ver:after {
			display: none;
		}
	</style>
	<style>
		.card.card-groups {
			background: #f9f9f9;
			transition: all 0.3s;
			border: 1px solid #f5f5f5;
			border-radius: 5px;
		}

		.card.card-groups .groups-dropdown {
			position: absolute;
			background-color: #fff;
			color: #333;
			right: 1rem;
			top: 1rem;
			cursor: pointer;
			border: 1px solid #4ca3d9;
			z-index: 99;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group {
			padding: 0.5rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
			position: absolute;
			top: 1rem;
			right: 2rem;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 15rem;
			padding: 0;
			margin: .125rem 0 0;
			font-size: 1rem;
			color: #212529;
			text-align: left;
			list-style: none;
			background-color: #fff;
			border: 1px solid rgba(0,0,0,.15);
			border-radius: .25rem;
			box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
			display: block;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
			display: block;
			background: #fff;
			width: 100%;
			font-size: 14px;
			color: #212529;
			padding: 0.75rem 1.5rem;
		}

		.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
			background: #4ca3d9;
			color: #fff;
		}

		.card.card-groups .card-block {
			width: 100%;
		}

		.card.card-groups.dropdown .card-block {
			width: calc(100% - 50px);
		}

		.card.card-groups .card-block .groups-title {
			font-size: 16px;
			font-weight: 500;
			color: #4ca3d9;
			margin-bottom: 0;
		}

		.card.card-groups .card-block .groups-title:hover {
			text-decoration: underline;
		}

		.card.card-groups .card-block .groups-description {
			font-weight: 400;
			color: #666;
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.page_manage_gradebook.title') <span>{{ $course->title }}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.page_manage_atendee.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_atendee.breadcumb_manage_courses')</a></li>
          <li><a href="/course/preview/{{Request::segment(3)}}">{{ $course->title }}</a></li>
          <li>@lang('front.page_manage_gradebook.title')</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
      <div class="row">
        <div class="col-md-3 nav-tabs-ver-container">
          <img src="{{$course->image}}" alt="" class="img-fluid mb-2">
					<div class="card no-shadow">
						<ul class="nav nav-tabs-ver" role="tablist">
							<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/atendee/{{$course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/attendance/{{$course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
							<li class="nav-item"><a class="nav-link active" href="/course/grades/{{$course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>             
							<li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
              @if(isTeacher(Auth::user()->id) === true)
                <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/list_badges"><i class="fa fa-shield"></i> Badges</a></li>
                @if($course->level_up == 1)
                  <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/level_settings"><i class="fa fa-trophy"></i> Levels</a></li>
                @endif
              @endif
						</ul>
					</div>
        </div>
        <div class="col-md-9">
					<div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="attendee">
							<div class="d-flex align-items-center justify-content-between mb-2">
                <h3 class="headline headline-sm mt-0 mb-0">@lang('front.page_manage_gradebook.title')</h3>
                @if(count($quizzes) > 0 || count($assignments) > 0)
                <div>
                  <a class="btn btn-sm btn-raised btn-primary" href="#" data-toggle="modal" data-target="#gradesSetting"><i class="zmdi zmdi-settings"></i>@lang('front.page_manage_gradebook.settings_grade')</a>
                  <a class="btn btn-sm btn-raised btn-success mt-0 mb-0" href="/course/grades/{{Request::segment(3)}}/export" target="_blank"><i class="zmdi zmdi-arrow-right"></i>@lang('front.page_manage_gradebook.export')</a>
                </div>
                @endif
              </div>
              @if(count($quizzes) < 1 && count($assignments) < 1)
                <p class="alert alert-warning">@lang('front.page_manage_gradebook.message_quiz_or_assignment_not_available')</p>
              @else
                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>@lang('front.page_manage_gradebook.table_no')</th>
                      <th>@lang('front.page_manage_gradebook.table_name')</th>
                      <th>@lang('front.page_manage_gradebook.table_grade_total')</th>
                      <th class="text-right">@lang('front.page_manage_gradebook.table_option')</th>
                    </tr>
                    @foreach($course_users as $index => $course_user)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$course_user->name}}</td>

                        @php
                          $grade_total = 0 ;
                          $grade_percent_total = 0 ;
                        @endphp

                        @foreach($quizzes as $quiz)

                          @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_user->user_id, 'quiz_id' => $quiz->id])->first() @endphp

                          @php
                            $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                            if(!$quiz_grade_setting){
                              $quiz_grade_setting_percent = 100;
                            }else{
                              $quiz_grade_setting_percent = $quiz_grade_setting->percentage;
                            }
                          @endphp

                          @if($quiz_participant)
                            @php
                              $grade = $quiz_participant->grade
                            @endphp
                          @else
                            @php $grade = 0 @endphp
                          @endif

                          @php $grade_percent_total += $grade * $quiz_grade_setting_percent / 100  @endphp
                          @php $grade_total += $grade @endphp
                        @endforeach

                        @foreach($assignments as $assignment)

                          @php
                            $assignments_answers = \DB::table('assignments_answers')
                              ->where(['assignments_answers.user_id' => $course_user->user_id, 'assignments_answers.assignment_id' => $assignment->id])
                              ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                              ->first()
                          @endphp

                          @php
                            $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                            if(!$assignment_grade_setting){
                              $assignment_grade_setting_percent = 100;
                            }else{
                              $assignment_grade_setting_percent = $quiz_grade_setting->percentage ?? 0;
                            }
                          @endphp

                          @if($assignments_answers)
                            @php $grade_assignment = $assignments_answers->grade @endphp
                          @else
                            @php
                              $grade_assignment = 0 ;
                            @endphp
                          @endif

                          @php $grade_percent_total += $grade_assignment * $assignment_grade_setting_percent / 100  @endphp
                          @php $grade_total += $grade_assignment @endphp

                        @endforeach

                        @php
                          $totalFinish = ($grade_total) / (count($quizzes) + count($assignments));
                        @endphp
                        <td>{{ round($totalFinish, 2)}}</td>
                        <td class="text-right">
                          <a class="btn-circle btn-circle-primary btn-circle-raised btn-circle-sm" href="#" data-toggle="modal" data-target="#gradesDetail{{ $index }}" title="Detail"><i class="fa fa-info-circle"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </table>
                </div>
              @endif
						</div>
					</div>
        </div>
      </div>
		</div>
	</div>

  <!-- Modal -->
  <div class="modal" id="gradesSetting" tabindex="-1" role="dialog" aria-labelledby="gradesSettingLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="d-flex align-items-center justify-content-between mb-2">
            <h3 class="headline headline-sm m-0">@lang('front.page_manage_gradebook.settings_grade')</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                <i class="zmdi zmdi-close"></i>
              </span>
            </button>
          </div>
          <form action="/course/grades/setting/save/{{$course->id}}" method="post">
            {{csrf_field()}}
            @foreach($quizzes as $quiz)
              @php
                $grade_setting_quiz = \DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
              @endphp
              <div class="form-group">
                <label class="control-label">{{$quiz->name}}</label>
                <input type="text" class="form-control" id="" name="percentage[]" placeholder="persentase {{$quiz->name}}" value="{{isset($grade_setting_quiz) ? $grade_setting_quiz->percentage : '0'}}">
                <input type="hidden" name="table_id[]" value="{{$quiz->id}}">
                <input type="hidden" name="table_name[]" value="quizzes">
              </div>
            @endforeach

            @foreach($assignments as $assignment)
              @php
                $grade_setting_assignment = \DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
              @endphp
              <div class="form-group">
                <label class="control-label">{{$assignment->title}}</label>
                <input type="text" class="form-control" id="" name="percentage[]" placeholder="persentase {{$assignment->title}}" value="{{isset($grade_setting_assignment) ? $grade_setting_assignment->percentage : '0'}}">
                <input type="hidden" name="table_id[]" value="{{$assignment->id}}">
                <input type="hidden" name="table_name[]" value="assignments">
              </div>
            @endforeach

            <input type="submit" name="" class="btn btn-primary btn-raised" value="@lang('front.general.text_save')">
          </form>
        </div>
      </div>
    </div>
  </div>

  @if(count($quizzes) > 0 || count($assignments) > 0)
    @foreach($course_users as $index => $course_user)
      <div class="modal" id="gradesDetail{{ $index }}" tabindex="-1" role="dialog" aria-labelledby="gradesDetailLabel">
        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <div class="d-flex align-items-center justify-content-between mb-2">
                <h3 class="headline headline-sm m-0">@lang('front.page_manage_gradebook.title') <span>{{$course_user->name}}</span></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">
                    <i class="zmdi zmdi-close"></i>
                  </span>
                </button>
              </div>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>@lang('front.page_manage_gradebook.table_quiz_assignment_title')</th>
                      <th>@lang('front.page_manage_gradebook.table_grade')</th>
                    </tr>
                  </thead>
                  <tbody>

                    @php
                      $grade_total = 0 ;
                      $grade_percent_total = 0 ;
                    @endphp

                    @foreach($quizzes as $quiz)
                      @php
                        $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                      @endphp
                      @php $quiz_participant = \DB::table('quiz_participants')->where(['user_id' => $course_user->user_id, 'quiz_id' => $quiz->id])->first() @endphp

                      @php
                        $quiz_grade_setting = DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
                        if(!$quiz_grade_setting){
                          $quiz_grade_setting_percent = 100;
                        }else{
                          $quiz_grade_setting_percent = $quiz_grade_setting->percentage;
                        }
                      @endphp

                      @if($quiz_participant)
                        @php
                          $grade = $quiz_participant->grade
                        @endphp
                      @else
                        @php $grade = 0 @endphp
                      @endif

                      @php $grade_percent_total += $grade * $quiz_grade_setting_percent / 100  @endphp
                      @php $grade_total += $grade @endphp

                      <tr>
                        <td>{{$quiz->name}} ({{isset($quiz_grade_setting) ? $quiz_grade_setting->percentage : '0'}}%)</td>
                        <td>{{ $grade }}</td>
                      </tr>
                    @endforeach

                    @foreach($assignments as $assignment)
                      @php
                        $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                      @endphp

                      @php
                      $assignments_answers = \DB::table('assignments_answers')
                        ->where(['assignments_answers.user_id' => $course_user->user_id, 'assignments_answers.assignment_id' => $assignment->id])
                        ->join('assignments_grades', 'assignments_grades.assignment_answer_id', '=', 'assignments_answers.id')
                        ->first()
                      @endphp

                      @php
                      $assignment_grade_setting = DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
                      if(!$assignment_grade_setting){
                        $assignment_grade_setting_percent = 100;
                      }else{
                        $assignment_grade_setting_percent = $quiz_grade_setting->percentage ?? 0;
                      }
                      @endphp

                      @if($assignments_answers)
                      @php $grade_assignment = $assignments_answers->grade @endphp
                      @else
                      @php
                        $grade_assignment = 0 ;
                      @endphp
                      @endif

                      @php $grade_percent_total += $grade_assignment * $assignment_grade_setting_percent / 100  @endphp
                      @php $grade_total += $grade_assignment @endphp

                      <tr>
                        <td>{{$assignment->title}} ({{isset($assignment_grade_setting) ? $assignment_grade_setting->percentage : '0'}}%)</td>
                        <td>{{ $grade_assignment }}</td>
                      </tr>
                    @endforeach

                    @php
                      $totalFinish = ($grade_total) / (count($quizzes) + count($assignments));
                    @endphp

                    <tr class="fw-600" style="border-top: 2px solid rgba(0,0,0,.2);">
                      <td>@lang('front.page_manage_gradebook.table_grade_total')</td>
                      <td>{{ round($totalFinish, 2)}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  @endif

@endsection
