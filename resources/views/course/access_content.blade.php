@extends('layouts.app')

@section('title', Lang::get('front.page_manage_content_access.title') . " - " . $course->title)

@push('style')
<link rel="stylesheet" href="{{asset('duallistbox/bootstrap-duallistbox.min.css')}}">
<style>
	.nav-tabs-ver-container .nav-tabs-ver {
		padding: 0;
		margin: 0;
	}

	.nav-tabs-ver-container .nav-tabs-ver:after {
		display: none;
	}
</style>
<style>
	.card.card-groups {
		background: #f9f9f9;
		transition: all 0.3s;
		border: 1px solid #f5f5f5;
		border-radius: 5px;
	}

	.card.card-groups .groups-dropdown {
		position: absolute;
		background-color: #fff;
		color: #333;
		right: 1rem;
		top: 1rem;
		cursor: pointer;
		border: 1px solid #4ca3d9;
		z-index: 99;
	}

	.card.card-groups .groups-dropdown .groups-dropdown-group {
		padding: 0.5rem 1.5rem;
	}

	.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
		position: absolute;
		top: 1rem;
		right: 2rem;
		z-index: 1000;
		display: none;
		float: left;
		min-width: 15rem;
		padding: 0;
		margin: .125rem 0 0;
		font-size: 1rem;
		color: #212529;
		text-align: left;
		list-style: none;
		background-color: #fff;
		border: 1px solid rgba(0, 0, 0, .15);
		border-radius: .25rem;
		box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
	}

	.card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
		display: block;
	}

	.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
		display: block;
		background: #fff;
		width: 100%;
		font-size: 14px;
		color: #212529;
		padding: 0.75rem 1.5rem;
	}

	.card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
		background: #4ca3d9;
		color: #fff;
	}

	.card.card-groups .card-block {
		width: 100%;
	}

	.card.card-groups.dropdown .card-block {
		width: calc(100% - 50px);
	}

	.card.card-groups .card-block .groups-title {
		font-size: 16px;
		font-weight: 500;
		color: #4ca3d9;
		margin-bottom: 0;
	}

	.card.card-groups .card-block .groups-title:hover {
		text-decoration: underline;
	}

	.card.card-groups .card-block .groups-description {
		font-weight: 400;
		color: #666;
	}
</style>
@endpush

@section('content')
<div class="bg-page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="headline-md no-m">@lang('front.page_manage_content_access.title') <span>{{ $course->title }}</span>
				</h2>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li><a href="/">@lang('front.page_manage_atendee.breadcumb_home')</a></li>
				<li><a href="/course/dashboard">@lang('front.page_manage_atendee.breadcumb_manage_courses')</a></li>
				<li><a href="/course/preview/{{$course->id}}">{{ $course->title }}</a></li>
				<li>@lang('front.page_manage_content_access.title')</li>
			</ul>
		</div>
	</div>
</div>
<div class="wrap pt-2 pb-2 mb-2 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-3 nav-tabs-ver-container">
				<img src="{{$course->image}}" alt="{{ $course->title }}" class="img-fluid mb-2">
				<div class="card no-shadow">
					<ul class="nav nav-tabs-ver" role="tablist">
						<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
						<li class="nav-item"><a class="nav-link" href="/course/atendee/{{$course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
						<li class="nav-item"><a class="nav-link" href="/course/attendance/{{$course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
						<li class="nav-item"><a class="nav-link" href="/course/grades/{{$course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>
						<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>
						<li class="nav-item"><a class="nav-link active" href="/courses/access-content/{{$course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
						@if(isTeacher(Auth::user()->id) === true)
							<li class="nav-item"><a class="nav-link" href="/{{$course->id}}/list_badges"><i class="fa fa-shield"></i> @lang('front.badges.badge')</a></li>
							@if($course->level_up == 1)
								<li class="nav-item"><a class="nav-link" href="/{{$course->id}}/level_settings"><i class="fa fa-trophy"></i> @lang('front.level_up.level')</a></li>
							@endif
						@endif
					</ul>
				</div>
			</div>
			<div class="col-md-9">
				<div class="tab-content">
					<?php $i=1; ?>
					<div class="panel-group" id="accordion fade in active">
						@foreach($sections as $section)
						<div class="panel panel-default" id="section{{$section['id']}}" data-id="{{$section['id']}}">
							<div class="panel-heading"
								style="background: #e9e9e9; display: flex; align-items: flex-start; justify-content: space-between;">
								<a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}" style="width: 100%;">
									<div style="display: flex; align-items: baseline;">
										<div>
											<h4 class="panel-title fw-500">{{ $section['title'] }}</h4>
											<p class="mb-0" style="color: #aaa;">{{ $section['description'] }}</p>
										</div>
									</div>
								</a>
							</div>

							<div id="content{{$i}}" class="panel-collapse collapse show">
								<div class="panel-body" style="background:white;">

									<div class="list-group">
										<div class="" id="{{ $section['id'] }}">
											@foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
											@if($all->section_type == 'content')
											@php
											$content = $all;
											@endphp
											@php
											$content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' =>
											Auth::user()->id, 'status' => '1'])->first();
											@endphp

											<div
												class="list-group-item list-group-item-action withripple {{$content->status == '0' ? 'disabled' : ''}}">
												@if($content->type_content == 'label')
												<div class="item-content">
													<div>
														{{ $content->title ? $content->title : strip_tags($content->description) }}
													</div>
												</div>
												@else
												<a class="item-content" href="#" data-toggle="modal"
													data-target="#modalContentDetail{{$content->id}}">
													<div>
														<i class="{{content_type_icon($content->type_content)}}"></i>
														{{ $content->title ? $content->title : strip_tags($content->description) }}
														<br />
													</div>
												</a>
												@endif
											</div>

											<div class="modal" id="modalContentDetail{{$content->id}}" tabindex="-1" role="dialog"
												aria-labelledby="userProgressDetailLabel">
												<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
													<div class="modal-content">
														<div class="modal-body">
															<div class="d-flex align-items-center justify-content-between mb-2">
																<h3 class="headline headline-sm m-0">
																	@lang('front.page_manage_content_access.title')</span></h3>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="zmdi zmdi-close"></i>
																	</span>
																</button>
															</div>
															<div>
																<table class="table">
																	<thead>
																		<th>@lang('front.page_manage_content_access.table_no')</th>
																		<th>@lang('front.page_manage_content_access.table_name')</th>
																		<th>@lang('front.page_manage_content_access.table_time')</th>
																	</thead>
																	<tbody>
																		@foreach ($content->progress_users as $index => $progress_user)
																		<tr>
																			<td>{{$index + 1}}</td>
																			<td>{{$progress_user->user->name}}</td>
																			<td>{{$progress_user->created_at}}</td>
																		</tr>
																		@endforeach
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endif

											@if($all->section_type == 'quizz')
											@php $quiz = $all; @endphp
											<div
												class="list-group-item list-group-item-action withripple {{$quiz->status == '0' ? 'disabled' : ''}}">
												<a class="item-content" href="#" data-target="#modalQuizDetail{{$quiz->id}}"
													data-toggle="modal">
													@if($quiz->quizz_type == 'quiz')
													<i class="fa fa-star"></i> {{$quiz->name}}
													<i
														style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('quizzes', $quiz->id)['name'] == null ? '' : 'Dapat diakses Oleh: ' . group_has_content('quizzes', $quiz->id)['name'] }}</i>
													@else
													<i class="fa fa-edit"></i> {{$quiz->name}}
													<i
														style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('quizzes', $quiz->id)['name'] == null ? '' : 'Dapat diakses Oleh: ' . group_has_content('quizzes', $quiz->id)['name'] }}</i>
													@endif
												</a>
											</div>

											<div class="modal" id="modalQuizDetail{{$quiz->id}}" tabindex="-1" role="dialog"
												aria-labelledby="userProgressDetailLabel">
												<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
													<div class="modal-content">
														<div class="modal-body">
															<div class="d-flex align-items-center justify-content-between mb-2">
																<h3 class="headline headline-sm m-0">
																	@lang('front.page_manage_content_access.title')</span></h3>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="zmdi zmdi-close"></i>
																	</span>
																</button>
															</div>
															<div>
																<table class="table">
																	<thead>
																		<th>@lang('front.page_manage_content_access.table_no')</th>
																		<th>@lang('front.page_manage_content_access.table_name')</th>
																		<th>@lang('front.page_manage_content_access.table_time')</th>
																	</thead>
																	<tbody>
																		@foreach ($quiz->progress_users as $index => $progress_user)
																		<tr>
																			<td>{{$index + 1}}</td>
																			<td>{{$progress_user->user->name}}</td>
																			<td>{{$progress_user->created_at}}</td>
																		</tr>
																		@endforeach
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endif

											@if($all->section_type == 'assignment')
											@php $assignment = $all; @endphp
											<div
												class="list-group-item list-group-item-action withripple {{$assignment->status == '0' ? 'disabled' : ''}}">
												<a class="item-content" href="#" data-target="#modalAssignmentDetail{{$assignment->id}}"
													data-toggle="modal">
													<i class="fa fa-tasks"></i> {{$assignment->title}}
													<i
														style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('assignments', $assignment->id)['name'] == null ? '' : 'Dapat diakses Oleh: ' . group_has_content('assignments', $assignment->id)['name'] }}</i>
												</a>
											</div>

											<div class="modal" id="modalAssignmentDetail{{$assignment->id}}" tabindex="-1" role="dialog"
												aria-labelledby="userProgressDetailLabel">
												<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
													<div class="modal-content">
														<div class="modal-body">
															<div class="d-flex align-items-center justify-content-between mb-2">
																<h3 class="headline headline-sm m-0">
																	@lang('front.page_manage_content_access.title')</span></h3>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="zmdi zmdi-close"></i>
																	</span>
																</button>
															</div>
															<div>
																<table class="table">
																	<thead>
																		<th>@lang('front.page_manage_content_access.table_no')</th>
																		<th>@lang('front.page_manage_content_access.table_name')</th>
																		<th>@lang('front.page_manage_content_access.table_time')</th>
																	</thead>
																	<tbody>
																		@foreach ($assignment->progress_users as $index => $progress_user)
																		<tr>
																			<td>{{$index + 1}}</td>
																			<td>{{$progress_user->user->name}}</td>
																			<td>{{$progress_user->created_at}}</td>
																		</tr>
																		@endforeach
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endif
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++; ?>

						@endforeach
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


@endsection

@push('script')

@endpush
