@extends('layouts.app')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
<div class="modal modal-default" id="create-folder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header d-block shadow-2dp no-pb">
                <h4 style="padding:10px">Buat Folder Baru</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="/library/create/folder">
                    {{ csrf_field() }}
                    <fieldset>
                        <div class="form-group label-floating">
                            <div class="input-group">
                                <label class="control-label" for="ms-form-text">Nama Folder</label>
                                <input type="hidden" name="parent" value="{{empty(Request::get('parents')) ? '.' : Request::get('parents')}}">
                                <input type="text" id="ms-form-text" name="title" class="form-control" value="Unamed Folder">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-raised btn-primary">Buat</button>
                            </div>
                            <div class="col-sm-3">
                                <button data-toggle="modal" data-target="#create-folder-modal" type="button" class="btn btn-raised">Batal</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-default" id="upload-file-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header d-block shadow-2dp no-pb">
                <h4 style="padding:10px">Upload File</h4>
            </div>
            <div class="modal-body">
            {{ Form::open(array('url' => '/upload/file', 'method' => 'POST')) }}
            <div class="form-group" id="FileFile">
                <label for="title">Pilih File </label>
                <input type="text" readonly="" class="form-control" placeholder="Pilih...">
                <input type="file" id="file" class="form-control">
                <input type="hidden" id="path_file" name="path_file">
                <input type="hidden" id="name_file" name="name_file">
                <input type="hidden" id="file_size" name="file_size">
                <input type="hidden" id="video_duration" name="video_duration">
                <input type="hidden" id="full_path_original" name="full_path_original">
                <input type="hidden" id="resize_path" name="resize_path">
                <input type="hidden" id="type_content" name="type_content">

                <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                <div id="container"></div>

                <div class="progress" style="display:none">
                    <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span class="sr-only">0%</span>
                    </div>
                </div>

                <div id="uploadedMessageError">
                    <div id="uploadedMessage"></div>
                </div>
            </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h2>My Library</h2>
    <!-- <li style="list-style: none;" class="dropdown">
        <a class="btn btn-primary btn-raised" role="button" data-toggle="dropdown" href="#">Tambah</a>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#create-folder-modal" aria-haspopup="true" aria-expanded="false">Buat Folder Baru</a></li>
            <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#upload-file-modal" aria-haspopup="true" aria-expanded="false">Upload File</a></li>
        </ul>
    </li> -->
    <div class="row" style="padding-left:10px">
        <a href="{{url('instructor/library')}}">
            <h3 style="padding:10px">Direktori Saya</h3>
        </a>
        @foreach(array_reverse($parents) as $parent)
            @if($parent['parent']!='Directory Saya')
                <h3 style="padding:10px">></h3>
                <a href="{{url('instructor/library?parents='.$parent['link_parent'])}}">
                    <h3 style="padding:10px">{{$parent['parent']}}</h3>
                </a>
            @endif
        @endforeach
    </div>
    <hr>
    <div class="row" style="padding:20px">
        @foreach($resources as $resource)
        <div class="col-sm-2" style="margin:10px">
            <div class="row" style="padding:10px;align-items:center;justify-content:center;background-color:#fff;border-radius:15px;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2)">
            <!-- <span data-toggle="dropdown" style="position:absolute;z-index:999;top:9px;right:5px" class="glyphicon glyphicon-option-vertical"></span>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#" aria-expanded="false">Salin</a></li>
                <li><a class="text-danger dropdown-item" href="#" onclick="if(confirm('{{empty(DB::table('userlibraries')->where(['parent' => $resource['name_file']])->first()) ? 'Anda yakin?' : 'Didalam directori ini ada beberapa file, anda yakin ingin menghapusnya?'}}')){ return location.href = '{{url('file/delete/'.$resource['id'])}}' }">Hapus</a></li>
            </ul> -->
                <a href="{{($resource['type_content']=='file' || $resource['type_content']=='video') ? url('add/to/section/'.$resource['id']) : url('instructor/library?parents='.$resource['name_file'])}}" style="padding-left:10px;padding-right:10px">
                    @if($resource['type_content']=='file')
                        <img src="http://icons.iconarchive.com/icons/zhoolego/material/512/Filetype-Docs-icon.png" alt="Resource Title" class="dq-image" style="width:45px;height:45px">
                    @elseif($resource['type_content']=='video')
                        <img src="https://png.icons8.com/color/260/circled-play.png" alt="Resource Title" class="dq-image" style="width:45px;height:45px">
                    @else
                        <img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/folder-icon.png" alt="Resource Title" class="dq-image" style="width:45px;height:45px">
                    @endif
                </a>
                <h4 style="padding-left:10px;padding-right:10px">{{$resource['title']}}</h4>
            </div>
        </div>
        @endforeach
        
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>
	<script type="text/javascript">
		// Custom example logic
		var path_file = $("#path_file").val();
		var name_file = $("#name_file").val();
		var file_size = $("#file_size").val();
		var video_duration = $("#video_duration").val();
		var full_path_original = $("#full_path_original").val();
		var resize_path = $("#resize_path").val();
        var type_content = $("#type_content").val();
		// $("#saveContent").prop('disabled', true);
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'file', // you can pass an id...
			container: document.getElementById('container'), // ... or DOM Element itself
			url : '/upload/file/{{$user_id}}',
			flash_swf_url : '/plupload/Moxie.swf',
			silverlight_xap_url : '/plupload/Moxie.xap',
			chunk_size: '1mb',
			dragdrop: true,
			headers: {
				'X-CSRF-TOKEN': '{{csrf_token()}}'
			},
			filters : {
				mime_types: [
					{title : "Files", extensions : "docx,doc,xlsx,pptx,pdf,xls,txt,mp4,webm"},
				]
			},

			init: {
				PostInit: function() {
					document.getElementById('filelist').innerHTML = '';
				},

				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						$("#divTitle").show()
						$("#divDescription").show()
						$("#contentTitle").val(file.name)
						$("#contentSave").show()

						uploader.start();
						$("#contentSave").prop('disabled', true);
						return false;
					});
				},

				UploadProgress: function(up, file) {
					// document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
					$('.progress').show();
					if(file.percent < 97){
						$('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
					}
					$("#uploadedMessage").html('Upload masih dalam proses...');
					$("#uploadedMessage").addClass('alert alert-info');
				},

				FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
                        console.log('[FileUploaded] File:', file, "Info:", info);
						var response = JSON.parse(info.response);
						console.log('Original Response: '+JSON.stringify(response.result.original));
						path_file = $("#path_file").val(response.result.original.path);
						name_file = $("#name_file").val(response.result.original.name);
						file_size = $("#file_size").val(response.result.original.size);
						video_duration = $("#video_duration").val(response.result.original.video_duration);
						full_path_original = $("#full_path_original").val(response.result.original.full_path_original);
						resize_path = $("#resize_path").val(response.result.original.resize_path);
                        type_content = $("#type_content").val(response.result.original.type_content);

						$.ajax({
							url: '/api/upload/file/store-data',
							type: 'POST',
							data: {
								//_token : '{{csrf_token()}}',
								file: true,
								path_file: response.result.original.path,
								name_file: response.result.original.name,
								file_size: response.result.original.size,
								video_duration: response.result.original.video_duration,
								full_path_original: response.result.original.full_path_original,
								resize_path: response.result.original.resize_path,
								type_content: response.result.original.type_content,
                                user_id: '{{$user_id}}',
                                original_file_name: response.result.original.original_file_name,
                                parent: "{{empty(Request::get('parents')) ? '.' : Request::get('parents')}}"
							},
							success: function(responses){
								// alert('saved draft');
                                //alert('saved draft');
                                //console.log('Ini response Kenapa gk muncul?: '+JSON.stringify(responses));
								//$("#contentId").val(response.id)
                                window.location.reload()
                                //document.location.replace("{{url('instructor/library'.empty(Request::get('parents')) ? '' : '?parents='.Request::get('parents'))}}");
							}
						})

        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
						// $("#saveContent").prop('disabled', false);
						// console.log(up);
						$('#progressBar').attr('aria-valuenow', 100).css('width', 100 + '%').text(100 + '%');
						$("#uploadedMessage").html('Upload selesai');
						$("#uploadedMessage").addClass('alert alert-success');
						$("#contentSave").prop('disabled', false);
        },

				Error: function(up, err) {
					// console.log(up);
					console.log(err);
					// document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
					if(err.status == 500 || err.code == "-200"){
						$("#uploadedMessage").html('');
						$("#uploadedMessageError").html('Upload Gagal');
						$("#uploadedMessageError").addClass('alert alert-danger');
						console.log('Upload Gagal');
					}
				},

				Destroy : function(){
					console.log('error');
				}
			}
		});

		uploader.init();
	</script>
@endpush