@extends('layouts.app')

@section('title', 'Buat Kelas Baru')

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp

						@if(Session::get('message'))
							<div class="alert alert-warning"><i class="fa fa-info-circle"></i> Kelas privat terbatas. <a style="text-decoration:underline" href="#"><strong>Upgrade akun Anda</strong></a> untuk mendapatkan kelas tanpa batas</div>
						@endif

						<div class="panel panel-primary">
						  <div class="panel-heading">
						    <h3 class="panel-title">Buat Kelas {{Request::get('live') ? 'Live' : ''}}</h3>
						  </div>
						  <div class="panel-body">
								{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

									<div class="form-group">
										<label for="title">Judul</label>
										<input placeholder="Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
										@if ($errors->has('title'))
											<span class="text-danger">{{ $errors->first('title') }}</span>
										@endif
									</div>

									<div class="form-group">
										<label for="title">Sub Judul</label>
										<input placeholder="Sub Judul" type="text" class="form-control" name="subtitle" value="{{ $subtitle }}">
										@if ($errors->has('subtitle'))
											<span class="text-danger">{{ $errors->first('subtitle') }}</span>
										@endif
									</div>

									<div class="form-group">
				            <label class="">Terjadwal</label>
										<div class="radio radio-primary">
											<label>
												<input type="radio" name="is_batch" value="1"> Ya
											</label>
										</div>
										<div class="radio radio-primary">
											<label>
												<input type="radio" name="is_batch" value="0" checked=""> Tidak
											</label>
										</div>
					        </div>

									<div id="div_is_batch">
										<div class="form-group">
											<label for="name">Jumlah Pertemuan</label>
											<select class="form-control selectpicker" name="meeting_total">
												<option value="">Pilih Pertemuan</option>
												@for ($i=1; $i <= 10; $i++)
													<option value="{{$i}}">{{$i}}</option>
												@endfor
											</select>
										</div>
										<div class="form-group">
											<label for="name">Kuota Peserta</label>
											<select class="form-control selectpicker" name="participant_total">
												<option value="">Pilih</option>
												<option value="5">5</option>
												<option value="10">10</option>
												<option value="25">25</option>
												<option value="50">50</option>
												<option value="100">100</option>
											</select>
										</div>
										<div class="form-group">
											<label for="title">Tanggal Mulai</label>
											<input placeholder="Tanggal Mulai" type="text" id="datePicker1" class="form-control" name="start_date">
										</div>
										<div class="form-group">
											<label for="title">Tanggal Akhir</label>
											<input placeholder="Tanggal Akhir" type="text" id="datePicker2" class="form-control" name="end_date">
										</div>
										<div class="form-group">
					            <label class="">Live</label>
											<div class="radio radio-primary">
												<label>
													<input type="radio" name="is_live" value="1"> Ya
												</label>
											</div>
											<div class="radio radio-primary">
												<label>
													<input type="radio" name="is_live" value="0" checked=""> Tidak
												</label>
											</div>
						        </div>
									</div>

									<div class="form-group">
										<label for="title">Tujuan Pembelajaran</label>
										<textarea name="goal" id="goal">{{ $goal }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Deskripsi</label>
										<textarea name="description" id="description">{{ $description }}</textarea>
									</div>

									<div class="form-group">
										<label for="title">Gambar</label>
										<input type="text" readonly="" class="form-control" placeholder="Pilih...">
										<input type="file" class="form-control" name="image" accept="image/*">
										<input type="hidden" name="old_image" value="{{ $image }}">
									</div>

									<div class="form-group">
										<label for="price">Harga</label>
										<input placeholder="Price" type="number" class="form-control" name="price" value="{{ $price }}" required>
									</div>

									<div class="form-group">
										<label for="name">Kategori</label>
										<select class="form-control" name="id_category" id="id_category" required>
											<option value="">Pilih Kategori</option>
											@foreach($categories as $category)
												<option {{ $category->id == $id_category ? 'selected' : '' }} value="{{$category->id}}">{{$category->title}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										<label for="name">Tingkatan</label>
										<select class="form-control" name="id_level_course" id="id_level_course"  required>
											<option value="">Pilih Tingkatan</option>
											@foreach($course_levels as $course_level)
												<option {{ $course_level->id == $id_level_course ? 'selected' : '' }} value="{{$course_level->id}}">{{$course_level->title}}</option>
											@endforeach()
										</select>
									</div>

									{{-- <div class="form-group">
										<label for="name">Tambah Pengajar</label>
										<select class="form-control" name="id_author[]" id="id_author" multiple>
											@foreach($authors as $author)
												<option {{ $author->id == $id_author ? 'selected' : '' }} value="{{$author->id}}">{{$author->name}} - {{$author->email}}</option>
											@endforeach()
										</select>
									</div>

									<div class="form-group">
										<label for="name">Share dengan Group</label>
										<select class="form-control" name="id_instructor_group[]" id="id_instructor_group" multiple>
											@foreach($instructor_groups as $instructor_group)
												<option {{ $instructor_group->id == $id_instructor_group ? 'selected' : '' }} value="{{$instructor_group->id}}">{{$instructor_group->title}}</option>
											@endforeach()
										</select>
									</div> --}}

									{{-- @if(Request::get('model') == 'private')
										<div class="form-group">
											<label for="title">Password / Kode Akses</label>
											<input placeholder="Masukan Password / Kode Akses" type="text" class="form-control" name="password" value="{{ generateRandomString(8) }}" required>
											@if ($errors->has('password'))
												<span class="text-danger">{{ $errors->first('password') }}</span>
											@endif
										</div>
									@endif --}}

									<div class="form-group">
										<input type="hidden" id="model" name="model" value="subscription">
										<input type="hidden" name="public" value="{{ Request::get('public') == 'true' ? '1' : '0' }}">
										{{  Form::hidden('id', $id) }}
										{{  Form::submit($button . " Kelas" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
										<a href="{{ url('course/dashboard') }}" class="btn btn-default btn-raised">Batal</a>
									</div>
								{{ Form::close() }}
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

@endsection

@push('script')
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>

	{{-- CKEDITOR --}}
	<script type="text/javascript">
	  CKEDITOR.replace('goal');
		CKEDITOR.replace('description');
		CKEDITOR.replace('section_description');
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#id_category").select2();
		$("#id_level_course").select2();
		$("#id_author").select2({
		   placeholder: "Pilih pengajar"
		});
		$("#id_instructor_group").select2({
		   placeholder: "Pilih group"
		});
	</script>
	{{-- SELECT2 --}}

	<script type="text/javascript">
		$("#div_is_batch").hide()
		$('input[type=radio][name=is_batch]').change(function() {
			if (this.value == '1') {
				$("#div_is_batch").show()
				$("#model").val('batch')
			}else{
				$("#div_is_batch").hide()
				$("#model").val('subscription')
			}
		});
	</script>

	<script type="text/javascript">
		$('input[type=radio][name=is_live]').change(function() {
			if (this.value == '1') {
				$("#model").val('live')
			}else{
				$("#model").val('batch')
			}
		});
	</script>

	<script type="text/javascript">
		$("#datePicker1").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
		$("#datePicker2").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>
@endpush
