@extends('layouts.app')

@section('title', $course->title.' - Setting')

@push('style')
  <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{asset('/fileuploads/css/fileupload.css')}}" rel="stylesheet" type="text/css"/>
  <style>
    @media (max-width: 420px) {
      .headline.headline-md {
        font-size: 24px;
        margin-top: 0;
      }
    }

    .hidden{
      display: none;
    }

    .date-course{
      display: flex;
    }

    .date-course input[type="date"]{
      width:20%;
      margin-right: 20px;
    }

    .date-course input[type="time"]{
      width:15%;
    }

    .date-course input{
      padding-left: 10px;
    }

    .enable{
      padding: 10px;
      margin-left: 20px;
    }

    .date-course input[disabled]{
      background: #ece9e9 !important;
    }

    .onoff input{
    	margin: 6px;
      margin-right: 5px;
    }

    .onoff input:last-child{
    	margin: 6px;
    	margin-right: 5px;
      margin-left: 20px;
    }

    .img-check-upload{
      position: absolute;
      margin: 25% auto;
      left: 35%;
      background: #4cd137;
      padding: 13px;
      border-radius: 10px;
      color: white;
    }

    .pilih-gambar::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: white !important;
    }

    .form-check input[type=checkbox]{
      height: 14px;
      opacity: 1;
      width: 12px;
      position: inherit;
      z-index: 1;
    }

    .fcontainer .form-group{
      margin: 0;
    }

    .modules .iconlarge {
        width: 50px;
        margin-right: 10px;
    }

    .modules.activity-completion-etail .iconlarge {
        width: 35px;
        margin-right: 10px;
    }

    input[type="checkbox"][readonly] {
      display:inline-block
    }

    .restrict-item label{
      width: 100%;
    }

    .restrict-item select{
      height: 35px;
      width: 100%;
    }

    .btn-activity-remove{
      margin: 0;
      margin-bottom: .5rem;
      padding: 3px 10px;
      text-align: center;
      padding-right: 2px;
    }

    .btn-activity-remove-keep{
      margin: 0;
      margin-bottom: .5rem;
      padding: 3px 10px;
      text-align: center;
      padding-right: 2px;
    }
  </style>

	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.menu.home')</a></li>
          <li>@lang('front.menu.courses')</li>
          <li><a href="{{url('/course/preview/'.$course->id)}}">{{$course->title}}</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container">
    @if(Session::get('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
    @elseif(Session::get('message'))
      <div class="alert alert-danger">{{Session::get('message')}}</div>
    @endif
    <div class="card nav-tabs-ver-container">
      <div class="row">
        <div class="col-lg-3">
          <ul class="nav nav-tabs-ver nav-tabs-ver-primary d-none d-sm-flex" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#tab-standard" aria-controls="tab-standard" role="tab" data-toggle="tab"><i class="fa fa-gear"></i><span class="d-none d-sm-inline"> @lang('front.advanced_setting.standard.menu')</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab-description" aria-controls="tab-description" role="tab" data-toggle="tab"><i class="fa fa-gear"></i><span class="d-none d-sm-inline"> @lang('front.advanced_setting.description.menu')</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab-completion" aria-controls="tab-completion" role="tab" data-toggle="tab"><i class="fa fa-gear"></i><span class="d-none d-sm-inline"> @lang('front.advanced_setting.completion.menu')</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab-restrict-access" aria-controls="tab-restrict-access" role="tab" data-toggle="tab"><i class="fa fa-gear"></i><span class="d-none d-sm-inline"> @lang('front.advanced_setting.restrict.menu')</a></li>
          </ul>
        </div>
        <div class="col-lg-9 nav-tabs-ver-container-content">
          <div class="card-block">
            {{Form::open(['url' => '/course/update_action','method' => 'put', 'files' => true])}}
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="tab-standard">
                <h3 class="headline headline-md"><span>@lang('front.advanced_setting.standard.header')</span></h3>
                <div class="divider divider-dark"></div>
                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.title')</label>
                  <input placeholder="Judul" type="text" class="form-control" name="title" value="{{ $course->title }}" required>
                  @if ($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                  @endif
                </div>

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.subtitle')</label>
                  <input placeholder="Sub Judul" type="text" class="form-control" name="subtitle" value="{{ $course->subtitle }}">
                  @if ($errors->has('subtitle'))
                    <span class="text-danger">{{ $errors->first('subtitle') }}</span>
                  @endif
                </div>

                <div class="form-group">
                  <label for="name">@lang('front.advanced_setting.standard.category')</label>
                  <select class="form-control" name="id_category" id="id_category" required style="height:40px">
                    <option value="">Pilih Kategori</option>
                    @foreach($categories as $category)
                      @php
                        $ParentCategory = \DB::table('categories')->where('id', $category->parent_category)->first();
                      @endphp
                      <option {{ $category->id == $category->id ? 'selected' : '' }} value="{{$category->id}}">{{isset($ParentCategory) ? $ParentCategory->title . ' > ' : ''}} {{$category->title}}</option>
                    @endforeach()
                  </select>
                </div>

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.class_type')</label>
                  <br>
                  <div class="onoff" style="display:flex">
                    <input type="radio" name="public" value="1" {{$course->public == '1' ? 'checked' : ''}}> @lang('front.advanced_setting.standard.open')
                    <input type="radio" name="public" value="0" {{$course->public == '0' ? 'checked' : ''}}> @lang('front.advanced_setting.standard.close')
                  </div>
                </div>

                @php
                  $training_provider = is_inTrainingProvider(Auth::user()->id);
                @endphp

                @if($training_provider > 0 && $course->status == '0')
                  <div class="form-group">
                    <label for="title">@lang('front.advanced_setting.standard.publish')</label>
                    <br>
                    <p class="text-danger">@lang('front.advanced_setting.standard.waiting_approval')</p>
                  </div>
                @else
                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.publish')</label>
                  <br>
                  <input type="checkbox" name="status" {{$course->status == '0' ? '' : 'checked'}}>
                </div>
                @endif

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.start')</label>
                  <label for="title">@lang('front.level_up.level')</label>
                  <br>
                  <input type="checkbox" name="level_up" {{$course->level_up == '0' ? '' : 'checked'}}>
                </div>

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.start')</label>
                  <div class="date-course">
                    <input type="date" class="form-control" name="start_date" value="{{ $course->start_date ? date('Y-m-d', strtotime($course->start_date)) : date('Y-m-d') }}">
                    <input type="time" class="form-control" name="start_time" value="{{ $course->start_date ? date('H:i', strtotime($course->start_date)) :'00:00' }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.standard.end')</label>
                  <div class="date-course">
                    <input type="date" class="form-control" name="end_date" id="end-date" value="{{ $course->end_date ? date('Y-m-d', strtotime($course->end_date)) : date('Y-m-d') }}" {{$course->end_date ? '' : 'disabled'}}>
                    <input type="time" class="form-control" name="end_time" id="end-time" value="{{ $course->end_date ? date('H:i', strtotime($course->end_date)) :'00:00' }}" {{$course->end_date ? '' : 'disabled'}}>
                    <div class="enable">
                      <input type="checkbox" class="" style="margin-right:10px" name="end_date_enable" id="enable-date" {{$course->end_date ? 'checked' : ''}}> @lang('front.advanced_setting.standard.enable')
                    </div>
                  </div>
                </div>

                <input type="submit" name="submit" value="@lang('front.advanced_setting.standard.save')" class="btn btn-primary btn-raised">
              </div>

              <div role="tabpanel" class="tab-pane" id="tab-description">
                <h3 class="headline headline-md"><span>@lang('front.advanced_setting.description.header')</span></h3>
                <div class="divider divider-dark"></div>
                <div class="form-group">
                    <label for="title">@lang('front.advanced_setting.description.image')</label>
                    <input type="file" name="image" class="dropify" data-default-file="{{$course->image ? asset($course->image): asset('img/default.png')}}" data-height="450" />
                </div>

                <div class="form-group">
                  <label for="title">@lang('front.advanced_setting.description.description')</label>
                  <textarea name="description" id="description">{!!$course->description!!}</textarea>
                </div>

                <input type="hidden" value="{{$course->id}}" name="id">
                <input type="submit" name="submit" value="@lang('front.advanced_setting.standard.save')" class="btn btn-primary btn-raised">
              </div>

              <div role="tabpanel" class="tab-pane" id="tab-completion">
                <h3 class="headline headline-md"><span>@lang('front.advanced_setting.completion.header')</span></h3>
                <div class="divider divider-dark"></div>
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs  shadow-2dp" role="tablist">
                        <li class="nav-item"><a class="nav-link withoutripple active" href="#course-completion" aria-controls="course-completion" role="tab" data-toggle="tab"><i class="zmdi zmdi-home"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.completion.course_completion.menu')</span></a></li>
                        <li class="nav-item"><a class="nav-link withoutripple" href="#default-activity-completion" aria-controls="default-activity-completion" role="tab" data-toggle="tab"><i class="zmdi zmdi-account"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.completion.default_activity_completion.menu')</span></a></li>
                        <li class="nav-item"><a class="nav-link withoutripple" href="#activitye-completion" aria-controls="activitye-completion" role="tab" data-toggle="tab"><i class="zmdi zmdi-email"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.completion.activity_completion.menu')</span></a></li>
                    </ul>

                    <div class="card-block">
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active show" id="course-completion">
                            <h3 class="headline headline-md"><span>@lang('front.advanced_setting.completion.course_completion.header')</span></h3>
                            <br>
                            <div class="ms-collapse" id="course-completion-parents-1" role="tablist" aria-multiselectable="true">
                              <div class="mb-0 card card-primary">
                                  <div class="card-header" role="tab" id="course-completion-collapse-heading-1">
                                      <h4 class="card-title">
                                          <a class="withripple" role="button" data-toggle="collapse" data-parent="#course-completion-parents-1" href="#course-completion-collapse-tab-1" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-star"></i> @lang('front.advanced_setting.completion.course_completion.completion_requirements')
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="course-completion-collapse-tab-1" class="card-collapse collapse show" role="tabpanel" aria-labelledby="course-completion-collapse-tab-1">
                                      <div class="card-block">
                                          @lang('front.advanced_setting.completion.course_completion.completion_requirements') :
                                          <select name="completion_requirements">
                                            <option value="0" {{$course->setting->course_completion == 0 ? 'checked' : ''}}>Course is complete when ALL conditions are met</option>
                                            <option value="1" {{$course->setting->course_completion == 1 ? 'checked' : ''}}>Course is complete when ANY of the conditions are met</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="mb-0 card card-primary">
                                  <div class="card-header" role="tab" id="course-completion-collapse-heading-2">
                                      <h4 class="card-title">
                                          <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#course-completion-parents-1" href="#course-completion-collapse-tab-2" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-star"></i> @lang('front.advanced_setting.completion.course_completion.condition_activity_completion')
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="course-completion-collapse-tab-2" class="card-collapse collapse" role="tabpanel" aria-labelledby="course-completion-collapse-tab-2">
                                      <?php $condition_activity = explode(',', $course->setting->activity_completion); ?>
                                      <div class="card-block">
                                        <div class="fcontainer clearfix">
                                          <div class="form-group row  fitem  checkboxgroup1">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="condition_activity_diskusi" class="form-check-input checkboxgroup1" value="{{$condition_activity[0]}}" {{$condition_activity[0] == 1 ? 'checked' : ''}}>
                                                      @lang('front.advanced_setting.completion.course_completion.discussion_forum')
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem  checkboxgroup1">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="condition_activity_assigment" class="form-check-input checkboxgroup1" value="{{$condition_activity[1]}}" {{$condition_activity[1] == 1 ? 'checked' : ''}}>
                                                      @lang('front.advanced_setting.completion.course_completion.assignment')
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem femptylabel  ">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 form-inline felement" data-fieldtype="static">
                                                <div class="form-control-static">
                                                @lang('front.advanced_setting.completion.course_completion.condition_activity_completion_note')
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem   ">
                                            <div class="col-md-3">
                                              <label class="col-form-label d-inline " for="id_activity_aggregation">
                                                  @lang('front.advanced_setting.completion.condition_requires')
                                              </label>
                                            </div>
                                            <div class="col-md-9 form-inline felement" data-fieldtype="select">
                                                <select class="" style="padding:0px 20px;" name="condition_activity">
                                                    <option value="0" {{$condition_activity[2] == 0 ? 'selected' : '' }}>@lang('front.advanced_setting.completion.course_completion.all_activity_complete')</option>
                                                    <option value="1" {{$condition_activity[2] == 1 ? 'selected' : '' }}>@lang('front.advanced_setting.completion.course_completion.any_activity_complete')</option>
                                                </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="mb-0 card card-primary">
                                  <div class="card-header" role="tab" id="course-completion-collapse-heading-2-1">
                                      <h4 class="card-title">
                                          <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#course-completion-parents-1" href="#course-completion-collapse-tab-2-1" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-star"></i> @lang('front.advanced_setting.completion.course_completion.condition_course_grade')
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="course-completion-collapse-tab-2-1" class="card-collapse collapse" role="tabpanel" aria-labelledby="course-completion-collapse-tab-2-1">
                                      <div class="card-block">
                                        <div class="fcontainer clearfix">
                                          <div class="form-group row  fitem  ">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-8 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="condition_grade_enable" class="form-check-input" value="{{$course->setting->course_grade_enable}}" id="condition_grade_enable"  {{$course->setting->course_grade_enable == 0 ? '' : 'checked'}}>
                                                      @lang('front.advanced_setting.standard.enable')
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem">
                                            <div class="col-md-4">
                                              <p class="col-form-label d-inline" aria-hidden="true" style="cursor: default;">
                                                  @lang('front.advanced_setting.completion.required_course_grade')
                                              </p>
                                            </div>
                                            <div class="col-md-8 form-inline felement" data-fieldtype="date_selector">
                                              <div class="date-course">
                                                <input type="text" class="form-control " name="condition_grade_value" id="condition_grade_value" value="{{$course->setting->course_grade}}" {{$course->setting->course_grade_enable == 0 ? 'disabled' : ''}}>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <div class="mb-0 card card-primary">
                                  <div class="card-header" role="tab" id="course-completion-collapse-heading-3">
                                      <h4 class="card-title">
                                          <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#course-completion-parents-1" href="#course-completion-collapse-tab-3" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-star"></i> @lang('front.advanced_setting.completion.course_completion.condition_self_completion')
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="course-completion-collapse-tab-3" class="card-collapse collapse" role="tabpanel" aria-labelledby="course-completion-collapse-tab-3">
                                      <div class="card-block">
                                        <div class="fcontainer clearfix">
                                          <div class="form-group row  fitem  ">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="manual_self_completion" class="form-check-input " value="{{$course->setting->manual_self_completion}}"  {{$course->setting->manual_self_completion == 0 ? '' : 'checked'}}>
                                                      @lang('front.advanced_setting.standard.enable')
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem">
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-9 form-inline felement" data-fieldtype="date_selector">
                                              <div class="form-control-static">
                                                @lang('front.advanced_setting.completion.course_completion.condition_self_completion_note')
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="mb-0 card card-primary">
                                  <div class="card-header" role="tab" id="course-completion-collapse-heading-4">
                                      <h4 class="card-title">
                                          <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#course-completion-parents-1" href="#course-completion-collapse-tab-4" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-star"></i> @lang('front.advanced_setting.completion.course_completion.condition_manual_completion_by_other')
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="course-completion-collapse-tab-4" class="card-collapse collapse" role="tabpanel" aria-labelledby="course-completion-collapse-tab-4">
                                      <?php $condition_manual_other_completion = explode(',', $course->setting->manual_other_completion); ?>
                                      <div class="card-block">
                                        <div class="fcontainer clearfix">
                                          <div class="form-group row  fitem  checkboxgroup1">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="condition_manual_other_completion_admin" class="form-check-input checkboxgroup1" value="{{$condition_manual_other_completion[0]}}" {{$condition_manual_other_completion[0] == 0 ? '' : 'checked'}}>
                                                      Admin
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem  checkboxgroup1">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 checkbox">
                                                <div class="form-check">
                                                    <label>
                                                      <input type="checkbox" name="condition_manual_other_completion_teacher" class="form-check-input checkboxgroup1" value="{{$condition_manual_other_completion[1]}}" {{$condition_manual_other_completion[1] == 0 ? '' : 'checked'}}>
                                                      @lang('front.advanced_setting.standard.teacher')
                                                    </label>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem femptylabel  ">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-9 form-inline felement" data-fieldtype="static">
                                                <div class="form-control-static">
                                                @lang('front.advanced_setting.completion.course_completion.condition_manual_completion_by_other_note')
                                                </div>
                                            </div>
                                          </div>
                                          <div class="form-group row  fitem   ">
                                            <div class="col-md-3">
                                              <label class="col-form-label d-inline ">
                                                  @lang('front.advanced_setting.completion.condition_requires')
                                              </label>
                                            </div>
                                            <div class="col-md-9 form-inline felement" data-fieldtype="select">
                                              <select class="" style="padding:0px 20px;" name="condition_manual_other_completion">
                                                  <option value="0" {{$condition_manual_other_completion[2] == "0" ? 'selected' : '' }}>@lang('front.advanced_setting.completion.course_completion.all_activity_complete')</option>
                                                  <option value="1" {{$condition_manual_other_completion[2] == "1" ? 'selected' : '' }}>@lang('front.advanced_setting.completion.course_completion.any_activity_complete')</option>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="default-activity-completion">
                            <h3 class="headline headline-md"><span>@lang('front.advanced_setting.completion.default_activity_completion.header')</span></h3>
                            <br>
                            <div class="modules">
                              @foreach($course->default_activity_completion as $indexDefaultActivity => $default_activity)
                              <input type="hidden" name="default_activity_completion[{{$default_activity->type_content}}][id]" value="{{$default_activity->id}}">
                              <div class="mb-1">
                                 <div class="row mb-1 row-fluid">
                                  <div class="col-md-8">
                                      @if($default_activity->type_content == 'assigment')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/assign/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_assignments')</span>
                                      @elseif($default_activity->type_content == 'quizz')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/quiz/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_quizzes')</span>
                                      @elseif($default_activity->type_content == 'text')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/wiki/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_text')</span>
                                      @elseif($default_activity->type_content == 'file')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/resource/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_files')</span>
                                      @elseif($default_activity->type_content == 'video')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/zoom/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_video')</span>
                                      @elseif($default_activity->type_content == 'discussion')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/forum/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_discussion')</span>
                                      @else
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/label/1598246781/icon" alt=" " role="presentation">
                                      <span>@lang('front.advanced_setting.activity_content.content_label')</span>
                                      @endif
                                  </div>
                                  <div class="col-md-2">
                                      <img class="icon " alt="Students can manually mark the activity as completed" title="Students can manually mark the activity as completed" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/core/1598246781/i/completion-manual-y">
                                      <span class="text-muted muted" style="padding: 5px;line-height: 3;">@lang('front.advanced_setting.standard.manual')</span>
                                  </div>
                                  <div class="col-md-2">
                                      <a class="collapsed withripple" role="button" data-toggle="collapse" href="#course-default-activity-completion-{{$default_activity->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                        <i class="fa fa-gears"></i>
                                        <span class="text-muted muted" style="padding-left: 5px;line-height: 3;">@lang('front.advanced_setting.standard.edit')</span>
                                      </a>
                                  </div>
                                  <div class="col-sm-12">
                                    <div id="course-default-activity-completion-{{$default_activity->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                      <div  class="form-group row  fitem   ">
                                          <div class="col-md-4">
                                            <span class="float-sm-right text-nowrap">
                                              <a class="btn btn-link p-0" style="margin: auto;" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                  data-content="
                                                    <div class=&quot;no-overflow&quot;>
                                                      <p>If enabled, activity completion is tracked, either manually or automatically, based on certain conditions. Multiple conditions may be set if desired. If so, the activity will only be considered complete when ALL conditions are met.</p>
                                                      <p>A tick next to the activity name on the course page indicates when the activity is complete.</p>
                                                    </div>"
                                                  data-html="true" tabindex="0" data-trigger="focus">
                                                  <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Completion tracking" aria-label="Help with Completion tracking"></i>
                                                </a>
                                            </span>
                                            <label class="col-form-label d-inline " for="id_completion">
                                                @lang('front.advanced_setting.completion.default_activity_completion.completion_tracking')
                                            </label>
                                          </div>
                                          <div class="col-md-8 form-inline felement" data-fieldtype="select">
                                              <select class="custom-select" name="default_activity_completion[{{$default_activity->type_content}}][completion_tracking]" style="height: 35px;">
                                                  <option value="0" {{$default_activity->completion_tracking == 0 ? 'selected' : ''}}>Do not indicate activity completion</option>
                                                  <option value="1" {{$default_activity->completion_tracking == 1 ? 'selected' : ''}}>Students can manually mark the activity as completed</option>
                                                  <option value="2" {{$default_activity->completion_tracking == 2 ? 'selected' : ''}}>Show activity as complete when conditions are met</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="form-group row  fitem">
                                          <div class="col-md-4">
                                            <label for="id_completionview">
                                                @lang('front.advanced_setting.completion.default_activity_completion.required_view')
                                            </label>
                                          </div>
                                          <div class="col-md-8 checkbox">
                                              <div class="form-check">
                                                  <label>
                                                  <input type="hidden" name="default_activity_completion[{{$default_activity->type_content}}][required_view]" value="{{$default_activity->required_view}}">
                                                  <input type="checkbox" class="form-check-input" value="{{$default_activity->required_view}}" {{$default_activity->completion_tracking != 2 ? 'disabled' : ''}} {{$default_activity->required_view == 1 ? 'checked' : ''}}>
                                                          @lang('front.advanced_setting.completion.default_activity_completion.student_view')
                                                  </label>
                                              </div>
                                          </div>
                                      </div>
                                      @if($default_activity->type_content == 'assigment' || $default_activity->type_content == 'quizz')
                                      <div class="form-group row  fitem  " id="yui_3_17_2_1_1598813789329_212">
                                        <div class="col-md-4">
                                          <label for="id_completionusegrade">
                                              @lang('front.advanced_setting.completion.default_activity_completion.required_grade')
                                          </label>
                                        </div>
                                        <div class="col-md-8 checkbox" id="yui_3_17_2_1_1598813789329_211">
                                          <div class="form-check" id="yui_3_17_2_1_1598813789329_210">
                                            <label id="yui_3_17_2_1_1598813789329_209">
                                              <input type="hidden" name="default_activity_completion[{{$default_activity->type_content}}][required_grade]" value="{{$default_activity->required_grade}}">
                                              <input type="checkbox" class="form-check-input" value="{{$default_activity->required_grade}}" {{$default_activity->completion_tracking != 2 ? 'disabled' : ''}} {{$default_activity->required_grade == 1 ? 'checked' : ''}}>
                                              @lang('front.advanced_setting.completion.default_activity_completion.student_grade')
                                            </label>
                                            <span class="text-nowrap">
                                                <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                    data-content="
                                                      <div class=&quot;no-overflow&quot;>
                                                        <p>If enabled, the activity is considered complete when a student receives a grade. Pass and fail icons may be displayed if a pass grade for the activity has been set.</p>
                                                      </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                  <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Require grade" aria-label="Help with Require grade"></i>
                                                </a>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group row  fitem  " id="yui_3_17_2_1_1598433662294_203">
                                          <div class="col-md-4">
                                            <label for="id_completionview">
                                            </label>
                                          </div>
                                          <div class="col-md-8 checkbox" id="yui_3_17_2_1_1598433662294_202">
                                              <div class="form-check" id="yui_3_17_2_1_1598433662294_201">
                                                  <label id="yui_3_17_2_1_1598433662294_200">
                                                    <input type="hidden" name="default_activity_completion[{{$default_activity->type_content}}][required_submit]" value="{{$default_activity->required_submit}}">
                                                  <input type="checkbox" class="form-check-input" value="{{$default_activity->required_submit}}" {{$default_activity->completion_tracking != 2 ? 'disabled' : ''}} {{$default_activity->required_submit == 1 ? 'checked' : ''}}>
                                                    @lang('front.advanced_setting.completion.default_activity_completion.student_submit')
                                                  </label>
                                              </div>
                                              <div class="form-control-feedback invalid-feedback" id="id_error_completionview">

                                              </div>
                                          </div>
                                      </div>
                                      @endif
                                      <div id="fitem_id_qwerty" class="form-group row  fitem femptylabel  ">
                                          <div class="col-md-4">
                                              <span class="float-sm-right text-nowrap">
                                              </span>
                                          </div>
                                          <div class="col-md-8 form-inline felement" data-fieldtype="static">
                                              <div class="form-control-static">
                                                @lang('front.advanced_setting.completion.default_activity_completion.note', ["activity" => '<b>'.ucfirst($default_activity->type_content).'</b>'])
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                              <hr class="row">
                              @endforeach
                            </div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="activitye-completion">
                            <h3 class="headline headline-md"><span>@lang('front.advanced_setting.completion.activity_completion.header')</span></h3>
                            <br>
                            <div class="modules activity-completion-etail">
                            @if(count($course->contents) > 0)
                              <p>-- @lang('front.advanced_setting.standard.content')</p>
                              <br>
                              @foreach($course->contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder') as $content)
                                @php
                                  $default_type = $content->type_content;
                                  if (strpos($default_type, '-') !== false) {
                                      $default_type = explode('-', $default_type);
                                      $default_type = $default_type[1];
                                  }
                                @endphp
                                  <div class="mb-1" style="margin-left: 20px;">
                                     <div class="row mb-1 row-fluid">
                                        <div class="col-md-8">
                                            @if($default_type == 'text')
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/wiki/1598246781/icon" alt=" " role="presentation">
                                            <span>{{strip_tags($content->description)}}</span>
                                            @elseif($default_type == 'file')
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/resource/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$content->title}}</span>
                                            @elseif($default_type == 'video')
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/zoom/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$content->title}}</span>
                                            @elseif($default_type == 'discussion')
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/forum/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$content->title}}</span>
                                            @else
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/label/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$content->title}}</span>
                                            @endif
                                        </div>
                                        @if($content->setting)
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-danger btn-raised bt-activity-completion-delete" style="padding: 8px;" role="button" content-id="{{$content->setting->id}}">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 0px;color:white !important">@lang('front.advanced_setting.standard.delete')</span>
                                            </button>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="collapsed withripple btn btn-success btn-raised" style="padding: 8px;" role="button" data-toggle="collapse" href="#course-activity-content-completion-{{$content->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 0px;color:white !important">@lang('front.advanced_setting.standard.edit')</span>
                                            </a>
                                        </div>

                                        <div class="col-sm-12">
                                          <div id="course-activity-content-completion-{{$content->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                            <input type="hidden" name="activity_completion[{{$content->setting->id}}]" value="{{$content->setting->id}}">
                                            <div class="form-group row fitem">
                                                <div class="col-md-4">
                                                  <span class="float-sm-right text-nowrap">
                                                    <a class="btn btn-link p-0" style="margin: auto;" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                        data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>If enabled, activity completion is tracked, either manually or automatically, based on certain conditions. Multiple conditions may be set if desired. If so, the activity will only be considered complete when ALL conditions are met.</p>
                                                            <p>A tick next to the activity name on the course page indicates when the activity is complete.</p>
                                                          </div>"
                                                        data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Completion tracking" aria-label="Help with Completion tracking"></i>
                                                      </a>
                                                  </span>
                                                  <label class="col-form-label d-inline " for="id_completion">
                                                    @lang('front.advanced_setting.completion.default_activity_completion.completion_tracking')
                                                  </label>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="select">
                                                    <select class="custom-select" name="activity_completion[{{$content->setting->id}}][completion_tracking]" style="height: 35px;">
                                                        <option value="0" {{$content->setting->completion_tracking == 0 ? 'selected' : ''}}>Do not indicate activity completion</option>
                                                        <option value="1" {{$content->setting->completion_tracking == 1 ? 'selected' : ''}}>Students can manually mark the activity as completed</option>
                                                        <option value="2" {{$content->setting->completion_tracking == 2 ? 'selected' : ''}}>Show activity as complete when conditions are met</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                    @lang('front.advanced_setting.completion.default_activity_completion.required_view')
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                        <input type="hidden" name="activity_completion[{{$content->setting->id}}][required_view]" value="{{$content->setting->required_view}}">
                                                        <input type="checkbox" class="form-check-input" value="{{$content->setting->required_view}}" {{$content->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$content->setting->required_view == 1 ? 'checked' : ''}}>
                                                                  @lang('front.advanced_setting.completion.default_activity_completion.student_view')
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row  fitem   " data-groupname="completionexpected">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>This setting specifies the date when the activity is expected to be completed.</p>
                                                          </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                          <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Expect completed on" aria-label="Help with Expect completed on"></i>
                                                       </a>
                                                    </span>
                                                    <p class="col-form-label d-inline" aria-hidden="true" style="cursor: default;">
                                                      Expect completed on
                                                    </p>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="date_time_selector">
                                                    <fieldset class="m-0 p-0 border-0">
                                                      <legend class="sr-only">Expect completed on</legend>
                                                      <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1598433662294_138">

                                                        <div class="  fitem  " id="yui_3_17_2_1_1598433662294_206">
                                                          <div class="form-group" style="margin: 0;">
                                                            <div class="date-course">
                                                              <input type="date" style="width: 150px;" class="form-control datetime-default-activity" name="activity_completion[{{$content->setting->id}}][end_date_default_activity]" value="{{$content->setting->expected_date ? date('Y-m-d', strtotime($content->setting->expected_date)) : date('Y-m-d')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <input type="time" style="width: 85px;" class="form-control datetime-default-activity" name="activity_completion[{{$content->setting->id}}][end_time_default_activity]" value="{{$content->setting->expected_date ? date('H:i', strtotime($content->setting->expected_date)) : date('H:i')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <div class="enable">
                                                                <input type="checkbox" class="" style="margin-right:10px" name="end_date_default_activity_enable" {{$content->setting->expected_date ? 'checked' : ''}}> Enable
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </fieldset>
                                                </div>
                                            </div> -->
                                            <div class="form-group row  fitem femptylabel  ">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                    </span>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="static">
                                                    <div class="form-control-static">
                                                      Some settings specific to <b>{{ucfirst($content->setting->type_content)}}</b> have been hidden. To view unselect other activities
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-md-2">
                                          <button type="button" class="btn btn-success btn-raised bt-activity-completion" role="button" content-id="{{$content->id}}" content-type="{{$default_type}}">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.make')</span>
                                          </button>
                                        </div>
                                        @endif
                                     </div>
                                  </div>
                                  <hr class="row">
                              @endforeach
                            @endif
                            @if(count($course->assigment) > 0)
                              <p>-- Assignment</p>
                              <br>
                              @foreach($course->assigment as $assignment)
                              @php
                                $default_type = 'assigment';
                              @endphp
                                  <div class="mb-1" style="margin-left: 20px;">
                                     <div class="row mb-1 row-fluid">
                                        <div class="col-md-8">
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/assign/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$assignment->title}}</span>
                                        </div>
                                        @if($assignment->setting)
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-danger btn-raised bt-activity-completion-delete" style="padding: 8px;" role="button" content-id="{{$assignment->setting->id}}">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 0px;color:white !important">@lang('front.advanced_setting.standard.delete')</span>
                                            </button>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="collapsed withripple btn btn-success btn-raised" role="button" style="padding: 8px;" data-toggle="collapse" href="#course-activity-assigment-completion-{{$assignment->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 0px;color:white !important">@lang('front.advanced_setting.standard.edit')</span>
                                            </a>
                                        </div>

                                        <div class="col-sm-12">
                                          <div id="course-activity-assigment-completion-{{$assignment->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                            <input type="hidden" name="activity_completion[{{$assignment->setting->id}}]" value="{{$assignment->setting->id}}">
                                            <div  class="form-group row  fitem   ">
                                                <div class="col-md-4">
                                                  <span class="float-sm-right text-nowrap">
                                                    <a class="btn btn-link p-0" style="margin: auto;" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                        data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>If enabled, activity completion is tracked, either manually or automatically, based on certain conditions. Multiple conditions may be set if desired. If so, the activity will only be considered complete when ALL conditions are met.</p>
                                                            <p>A tick next to the activity name on the course page indicates when the activity is complete.</p>
                                                          </div>"
                                                        data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Completion tracking" aria-label="Help with Completion tracking"></i>
                                                      </a>
                                                  </span>
                                                  <label class="col-form-label d-inline " for="id_completion">
                                                      Completion tracking
                                                  </label>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="select">
                                                    <select class="custom-select" name="activity_completion[{{$assignment->setting->id}}][completion_tracking]" style="height: 35px;">
                                                        <option value="0" {{$assignment->setting->completion_tracking == 0 ? 'selected' : ''}}>Do not indicate activity completion</option>
                                                        <option value="1" {{$assignment->setting->completion_tracking == 1 ? 'selected' : ''}}>Students can manually mark the activity as completed</option>
                                                        <option value="2" {{$assignment->setting->completion_tracking == 2 ? 'selected' : ''}}>Show activity as complete when conditions are met</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                      Require view
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                          <input type="hidden" name="activity_completion[{{$assignment->setting->id}}][required_view]" value="{{$assignment->setting->required_view}}">
                                                          <input type="checkbox" class="form-check-input" value="{{$assignment->setting->required_view}}" {{$assignment->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$assignment->setting->required_view == 1 ? 'checked' : ''}}>
                                                                Student must view this activity to complete it
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem  " id="yui_3_17_2_1_1598813789329_212">
                                              <div class="col-md-4">
                                                <label for="id_completionusegrade">
                                                    Require grade
                                                </label>
                                              </div>
                                              <div class="col-md-8 checkbox" id="yui_3_17_2_1_1598813789329_211">
                                                <div class="form-check" id="yui_3_17_2_1_1598813789329_210">
                                                  <label id="yui_3_17_2_1_1598813789329_209">
                                                    <input type="hidden" name="activity_completion[{{$assignment->setting->id}}][required_grade]" value="{{$assignment->setting->required_grade}}">
                                                    <input type="checkbox" class="form-check-input" value="{{$assignment->setting->required_grade}}" {{$assignment->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$assignment->setting->required_grade == 1 ? 'checked' : ''}}>
                                                    Student must receive a grade to complete this activity
                                                  </label>
                                                  <span class="text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                            <div class=&quot;no-overflow&quot;>
                                                              <p>If enabled, the activity is considered complete when a student receives a grade. Pass and fail icons may be displayed if a pass grade for the activity has been set.</p>
                                                            </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Require grade" aria-label="Help with Require grade"></i>
                                                      </a>
                                                  </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                          <input type="hidden" name="activity_completion[{{$assignment->setting->id}}][required_submit]" value="{{$assignment->setting->required_submit}}">
                                                          <input type="checkbox" class="form-check-input" value="{{$assignment->setting->required_submit}}" {{$assignment->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$assignment->setting->required_submit == 1 ? 'checked' : ''}}>
                                                                Student must submit to this activity to complete it
                                                        </label>
                                                    </div>
                                                    <div class="form-control-feedback invalid-feedback">

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row  fitem   " data-groupname="completionexpected">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>This setting specifies the date when the activity is expected to be completed.</p>
                                                          </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                          <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Expect completed on" aria-label="Help with Expect completed on"></i>
                                                       </a>
                                                    </span>
                                                    <p class="col-form-label d-inline" aria-hidden="true" style="cursor: default;">
                                                      Expect completed on
                                                    </p>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="date_time_selector">
                                                    <fieldset class="m-0 p-0 border-0">
                                                      <legend class="sr-only">Expect completed on</legend>
                                                      <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1598433662294_138">

                                                        <div class="  fitem  " id="yui_3_17_2_1_1598433662294_206">
                                                          <div class="form-group" style="margin: 0;">
                                                            <div class="date-course">
                                                              <input type="date" style="width: 150px;" class="form-control datetime-default-activity" name="activity_completion[{{$assignment->setting->id}}][end_date_default_activity]" value="{{$assignment->setting->expected_date ? date('Y-m-d', strtotime($assignment->setting->expected_date)) : date('Y-m-d')}}" {{$assignment->expected_date ? '' : 'disabled'}} required>
                                                              <input type="time" style="width: 85px;" class="form-control datetime-default-activity" name="activity_completion[{{$assignment->setting->id}}][end_time_default_activity]" value="{{$assignment->setting->expected_date ? date('H:i', strtotime($assignment->setting->expected_date)) : date('H:i')}}" {{$assignment->expected_date ? '' : 'disabled'}} required>
                                                              <div class="enable">
                                                                <input type="checkbox" class="" style="margin-right:10px" name="end_date_default_activity_enable" {{$default_activity->expected_date ? 'checked' : ''}}> Enable
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </fieldset>
                                                </div>
                                            </div> -->
                                            <div class="form-group row  fitem femptylabel  ">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                    </span>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="static">
                                                    <div class="form-control-static">
                                                      Some settings specific to <b>{{ucfirst($default_activity->type_content)}}</b> have been hidden. To view unselect other activities
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-md-2">
                                          <button type="button" class="btn btn-success btn-raised bt-activity-completion" role="button" content-id="{{$assignment->id}}" content-type="{{$default_type}}">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.make')</span>
                                          </button>
                                        </div>
                                        @endif
                                     </div>
                                  </div>
                                  <hr class="row">
                              @endforeach
                            @endif
                            @if(count($course->quizz) > 0)
                              <p>-- Quizz</p>
                              <br>
                              @foreach($course->quizz as $quizz)
                                @php
                                  $default_type = 'quizz';
                                @endphp
                                @if($quizz->quizz_type == 'quiz')
                                  <div class="mb-1" style="margin-left: 20px;">
                                     <div class="row mb-1 row-fluid">
                                        <div class="col-md-8">
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/quiz/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$quizz->name}}</span>
                                        </div>
                                        @if($quizz->setting)
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-danger btn-raised bt-activity-completion-delete" style="padding: 8px;" role="button" content-id="{{$quizz->setting->id}}">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.delete')</span>
                                            </button>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="collapsed withripple btn btn-success btn-raised" role="button" style="padding: 8px;" data-toggle="collapse" href="#course-activity-quizz-completion-{{$quizz->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 3px;color:white !important">@lang('front.advanced_setting.standard.edit')</span>
                                            </a>
                                        </div>

                                        <div class="col-sm-12">
                                          <div id="course-activity-quizz-completion-{{$quizz->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                            <div  class="form-group row  fitem   ">
                                                <div class="col-md-4">
                                                  <span class="float-sm-right text-nowrap">
                                                    <a class="btn btn-link p-0" style="margin: auto;" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                        data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>If enabled, activity completion is tracked, either manually or automatically, based on certain conditions. Multiple conditions may be set if desired. If so, the activity will only be considered complete when ALL conditions are met.</p>
                                                            <p>A tick next to the activity name on the course page indicates when the activity is complete.</p>
                                                          </div>"
                                                        data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Completion tracking" aria-label="Help with Completion tracking"></i>
                                                      </a>
                                                  </span>
                                                  <label class="col-form-label d-inline " for="id_completion">
                                                      Completion tracking
                                                  </label>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="select">
                                                    <select class="custom-select" name="activity_completion[{{$quizz->setting->id}}][completion_tracking]" style="height: 35px;">
                                                        <option value="0" {{$quizz->setting->completion_tracking == 0 ? 'selected' : ''}}>Do not indicate activity completion</option>
                                                        <option value="1" {{$quizz->setting->completion_tracking == 1 ? 'selected' : ''}}>Students can manually mark the activity as completed</option>
                                                        <option value="2" {{$quizz->setting->completion_tracking == 2 ? 'selected' : ''}}>Show activity as complete when conditions are met</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                      Require view
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                        <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_view]" value="{{$quizz->setting->required_view}}">
                                                        <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_view}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_view == 1 ? 'checked' : ''}}>
                                                                Student must view this activity to complete it
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem  " id="yui_3_17_2_1_1598813789329_212">
                                              <div class="col-md-4">
                                                <label for="id_completionusegrade">
                                                    Require grade
                                                </label>
                                              </div>
                                              <div class="col-md-8 checkbox" id="yui_3_17_2_1_1598813789329_211">
                                                <div class="form-check" id="yui_3_17_2_1_1598813789329_210">
                                                  <label id="yui_3_17_2_1_1598813789329_209">
                                                    <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_grade]" value="{{$quizz->setting->required_grade}}">
                                                    <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_grade}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_grade == 1 ? 'checked' : ''}}>
                                                    Student must receive a grade to complete this activity
                                                  </label>
                                                  <span class="text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                            <div class=&quot;no-overflow&quot;>
                                                              <p>If enabled, the activity is considered complete when a student receives a grade. Pass and fail icons may be displayed if a pass grade for the activity has been set.</p>
                                                            </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Require grade" aria-label="Help with Require grade"></i>
                                                      </a>
                                                  </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                          <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_submit]" value="{{$quizz->setting->required_submit}}">
                                                        <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_submit}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_submit == 1 ? 'checked' : ''}}>
                                                                Student must submit to this activity to complete it
                                                        </label>
                                                    </div>
                                                    <div class="form-control-feedback invalid-feedback">

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row  fitem   " data-groupname="completionexpected">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>This setting specifies the date when the activity is expected to be completed.</p>
                                                          </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                          <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Expect completed on" aria-label="Help with Expect completed on"></i>
                                                       </a>
                                                    </span>
                                                    <p class="col-form-label d-inline" aria-hidden="true" style="cursor: default;">
                                                      Expect completed on
                                                    </p>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="date_time_selector">
                                                    <fieldset class="m-0 p-0 border-0">
                                                      <legend class="sr-only">Expect completed on</legend>
                                                      <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1598433662294_138">

                                                        <div class="  fitem  " id="yui_3_17_2_1_1598433662294_206">
                                                          <div class="form-group" style="margin: 0;">
                                                            <div class="date-course">
                                                              <input type="date" style="width: 150px;" class="form-control datetime-default-activity" name="default_activity_completion[{{$default_activity->type_content}}][end_date_default_activity]" value="{{$default_activity->expected_date ? date('Y-m-d', strtotime($default_activity->expected_date)) : date('Y-m-d')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <input type="time" style="width: 85px;" class="form-control datetime-default-activity" name="default_activity_completion[{{$default_activity->type_content}}][end_time_default_activity]" value="{{$default_activity->expected_date ? date('H:i', strtotime($default_activity->expected_date)) : date('H:i')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <div class="enable">
                                                                <input type="checkbox" class="" style="margin-right:10px" name="end_date_default_activity_enable" {{$default_activity->expected_date ? 'checked' : ''}}> Enable
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </fieldset>
                                                </div>
                                            </div> -->
                                            <div class="form-group row  fitem femptylabel  ">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                    </span>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="static">
                                                    <div class="form-control-static">
                                                      Some settings specific to <b>{{ucfirst($quizz->setting->type_content)}}</b> have been hidden. To view unselect other activities
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-md-2">
                                          <button type="button" class="btn btn-success btn-raised bt-activity-completion" role="button" content-id="{{$quizz->id}}" content-type="{{$default_type}}">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.make')</span>
                                          </button>
                                        </div>
                                        @endif
                                     </div>
                                  </div>
                                  <hr class="row">
                                @endif
                              @endforeach
                              <p>-- Survey</p>
                              <br>
                              @foreach($course->quizz as $index => $quizz)
                                  @php
                                    $default_type = 'quizz';
                                  @endphp
                                  @if($quizz->quizz_type == 'survey')
                                  <div class="mb-1" style="margin-left: 20px;">
                                     <div class="row mb-1 row-fluid">
                                        <div class="col-md-8">
                                            <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/quiz/1598246781/icon" alt=" " role="presentation">
                                            <span>{{$quizz->name}}</span>
                                        </div>
                                        @if($quizz->setting)
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-danger btn-raised bt-activity-completion-delete" style="padding: 8px;" role="button" content-id="{{$quizz->setting->id}}">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.delete')</span>
                                            </button>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="collapsed withripple btn btn-success btn-raised" role="button" style="padding: 8px;" data-toggle="collapse" href="#course-activity-quizz-completion-{{$quizz->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                              <i class="fa fa-gears"></i>
                                              <span class="text-muted muted" style="padding-left: 3px;color:white !important">@lang('front.advanced_setting.standard.edit')</span>
                                            </a>
                                        </div>

                                        <div class="col-sm-12">
                                          <div id="course-activity-quizz-completion-{{$quizz->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                            <div  class="form-group row  fitem   ">
                                                <div class="col-md-4">
                                                  <span class="float-sm-right text-nowrap">
                                                    <a class="btn btn-link p-0" style="margin: auto;" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                        data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>If enabled, activity completion is tracked, either manually or automatically, based on certain conditions. Multiple conditions may be set if desired. If so, the activity will only be considered complete when ALL conditions are met.</p>
                                                            <p>A tick next to the activity name on the course page indicates when the activity is complete.</p>
                                                          </div>"
                                                        data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Completion tracking" aria-label="Help with Completion tracking"></i>
                                                      </a>
                                                  </span>
                                                  <label class="col-form-label d-inline " for="id_completion">
                                                      Completion tracking
                                                  </label>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="select">
                                                    <select class="custom-select" name="activity_completion[{{$quizz->setting->id}}][completion_tracking]" style="height: 35px;">
                                                        <option value="0" {{$quizz->setting->completion_tracking == 0 ? 'selected' : ''}}>Do not indicate activity completion</option>
                                                        <option value="1" {{$quizz->setting->completion_tracking == 1 ? 'selected' : ''}}>Students can manually mark the activity as completed</option>
                                                        <option value="2" {{$quizz->setting->completion_tracking == 2 ? 'selected' : ''}}>Show activity as complete when conditions are met</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                      Require view
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                        <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_view]" value="{{$quizz->setting->required_view}}">
                                                        <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_view}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_view == 1 ? 'checked' : ''}}>
                                                                Student must view this activity to complete it
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row  fitem  " id="yui_3_17_2_1_1598813789329_212">
                                              <div class="col-md-4">
                                                <label for="id_completionusegrade">
                                                    Require grade
                                                </label>
                                              </div>
                                              <div class="col-md-8 checkbox" id="yui_3_17_2_1_1598813789329_211">
                                                <div class="form-check" id="yui_3_17_2_1_1598813789329_210">
                                                  <label id="yui_3_17_2_1_1598813789329_209">
                                                    <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_grade]" value="{{$quizz->setting->required_grade}}">
                                                    <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_grade}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_grade == 1 ? 'checked' : ''}}>
                                                    Student must receive a grade to complete this activity
                                                  </label>
                                                  <span class="text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                            <div class=&quot;no-overflow&quot;>
                                                              <p>If enabled, the activity is considered complete when a student receives a grade. Pass and fail icons may be displayed if a pass grade for the activity has been set.</p>
                                                            </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                        <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Require grade" aria-label="Help with Require grade"></i>
                                                      </a>
                                                  </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row  fitem">
                                                <div class="col-md-4">
                                                  <label for="id_completionview">
                                                  </label>
                                                </div>
                                                <div class="col-md-8 checkbox">
                                                    <div class="form-check">
                                                        <label>
                                                          <input type="hidden" name="activity_completion[{{$quizz->setting->id}}][required_submit]" value="{{$quizz->setting->required_submit}}">
                                                        <input type="checkbox" class="form-check-input" value="{{$quizz->setting->required_submit}}" {{$quizz->setting->completion_tracking != 2 ? 'disabled' : ''}} {{$quizz->setting->required_submit == 1 ? 'checked' : ''}}>
                                                                Student must submit to this activity to complete it
                                                        </label>
                                                    </div>
                                                    <div class="form-control-feedback invalid-feedback">

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row  fitem   " data-groupname="completionexpected">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                      <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right"
                                                          data-content="
                                                          <div class=&quot;no-overflow&quot;>
                                                            <p>This setting specifies the date when the activity is expected to be completed.</p>
                                                          </div> " data-html="true" tabindex="0" data-trigger="focus">
                                                          <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Expect completed on" aria-label="Help with Expect completed on"></i>
                                                       </a>
                                                    </span>
                                                    <p class="col-form-label d-inline" aria-hidden="true" style="cursor: default;">
                                                      Expect completed on
                                                    </p>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="date_time_selector">
                                                    <fieldset class="m-0 p-0 border-0">
                                                      <legend class="sr-only">Expect completed on</legend>
                                                      <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1598433662294_138">

                                                        <div class="  fitem  " id="yui_3_17_2_1_1598433662294_206">
                                                          <div class="form-group" style="margin: 0;">
                                                            <div class="date-course">
                                                              <input type="date" style="width: 150px;" class="form-control datetime-default-activity" name="default_activity_completion[{{$default_activity->type_content}}][end_date_default_activity]" value="{{$default_activity->expected_date ? date('Y-m-d', strtotime($default_activity->expected_date)) : date('Y-m-d')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <input type="time" style="width: 85px;" class="form-control datetime-default-activity" name="default_activity_completion[{{$default_activity->type_content}}][end_time_default_activity]" value="{{$default_activity->expected_date ? date('H:i', strtotime($default_activity->expected_date)) : date('H:i')}}" {{$default_activity->expected_date ? '' : 'disabled'}} required>
                                                              <div class="enable">
                                                                <input type="checkbox" class="" style="margin-right:10px" name="end_date_default_activity_enable" {{$default_activity->expected_date ? 'checked' : ''}}> Enable
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </fieldset>
                                                </div>
                                            </div> -->
                                            <div class="form-group row  fitem femptylabel  ">
                                                <div class="col-md-4">
                                                    <span class="float-sm-right text-nowrap">
                                                    </span>
                                                </div>
                                                <div class="col-md-8 form-inline felement" data-fieldtype="static">
                                                    <div class="form-control-static">
                                                      Some settings specific to <b>{{ucfirst($quizz->setting->type_content)}}</b> have been hidden. To view unselect other activities
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-md-2">
                                          <button type="button" class="btn btn-success btn-raised bt-activity-completion" role="button" content-id="{{$quizz->id}}" content-type="{{$default_type}}">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 5px;color:white !important">@lang('front.advanced_setting.standard.make')</span>
                                          </button>
                                        </div>
                                        @endif
                                     </div>
                                  </div>
                                  <hr class="row">
                                @endif
                              @endforeach
                            @endif
                          </div>
                        </div>
                    </div>
                </div> <!-- card -->
                </div>
                <input type="hidden" value="{{$course->id}}" name="id">
                <input type="hidden" value="{{$course->id_level_course}}" name="id_level_course">
                <input type="submit" name="submit" value="@lang('front.advanced_setting.standard.save')" class="btn btn-primary btn-raised">
              </div>

              @php
                $activity_order = true;
                $section_all = [];
                foreach ($course->contents->where('type_content', '!=', 'label')->where('type_content', '!=', 'folder') as $content) {
                  $content->section_type = 'content';
                  $content->activity_order = $content->activity_order;
                  $section_all[] = $content;

                  if(!$content->activity_order){
                    $activity_order = false;
                  }
                }
                foreach ($course->quizz as $quizze) {
                  $quizze->section_type = 'quizz';
                  $quizze->activity_order = $quizze->activity_order;
                  $section_all[] = $quizze;

                  if(!$quizze->activity_order){
                    $activity_order = false;
                  }
                }

                foreach ($course->assigment as $assignment) {
                  $assignment->section_type = 'assignment';
                  $assignment->activity_order = $assignment->activity_order;
                  $section_all[] = $assignment;

                  if(!$assignment->activity_order){
                    $activity_order = false;
                  }
                }
              @endphp

              <div role="tabpanel" class="tab-pane" id="tab-restrict-access">
                <button type="button" class="btn btn-success btn-raised btn-restrict-order" style="float:right">@lang('front.advanced_setting.restrict.generate')</button>
                <h3 class="headline headline-md"><span>@lang('front.advanced_setting.restrict.header')</span></h3>
                <div class="divider divider-dark"></div>
                <div class="card">
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs  shadow-2dp" role="tablist">
                      <li class="nav-item"><a class="nav-link withoutripple active" href="#restrict-access-setting" aria-controls="restrict-access-setting" role="tab" data-toggle="tab"><i class="zmdi zmdi-home"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.restrict.restrict_access')</span></a></li>
                      <li class="nav-item"><a class="nav-link withoutripple" href="#acticity-order-setting" aria-controls="acticity-order-setting" role="tab" data-toggle="tab"><i class="zmdi zmdi-account"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.restrict.activity_order')</span></a></li>
                  </ul>

                  <div class="card-block">
                        <!-- Tab panes -->
                    <div class="tab-content">

                      <div role="tabpanel" class="tab-pane fade  active show" id="restrict-access-setting">
                        <h3 class="headline headline-md"><span>@lang('front.advanced_setting.restrict.default_activity_completion.header')</span></h3>
                        <br>
                        <div class="modules activity-completion-etail">
                          @foreach(collect($section_all)->sortBy('activity_order.order') as $all)
                            @php
                            if($all->section_type == 'content'){
                              $content = $all;
                              $restrict_id = $content->id;
                              $title = $content->title;
                              $default_type = $content->type_content;
                              if (strpos($default_type, '-') !== false) {
                                  $default_type = explode('-', $default_type);
                                  $default_type = $default_type[1];
                              }
                            }elseif($all->section_type == 'quizz'){
                              $quizz = $all;
                              $restrict_id = $quizz->id;
                              $title = $quizz->name;
                            }elseif($all->section_type == 'assignment'){
                              $assignment = $all;
                              $restrict_id = $assignment->id;
                              $title = $assignment->title;
                            }
                            @endphp
                          <div class="mb-1" style="margin-left: 20px;">
                             <div class="row mb-1 row-fluid">
                                <div class="col-md-8">
                                    @if($all->section_type == 'content')
                                      @if($default_type == 'text')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/wiki/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$content->title}}</span>
                                      @elseif($default_type == 'file')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/resource/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$content->title}}</span>
                                      @elseif($default_type == 'video')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/zoom/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$content->title}}</span>
                                      @elseif($default_type == 'discussion')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/forum/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$content->title}}</span>
                                      @else
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/label/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$content->title}}</span>
                                      @endif
                                    @elseif($all->section_type == 'quizz')
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/quiz/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$quizz->name}}</span>
                                    @else
                                      <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/assign/1598246781/icon" alt=" " role="presentation">
                                      <span>{{$assignment->title}}</span>
                                    @endif
                                </div>
                                <div class="col-md-4" style="text-align:right">
                                    <a class="collapsed withripple btn btn-success btn-raised" style="padding: 8px;" role="button" data-toggle="collapse" href="#restrict-access-activity-content-{{$all->section_type}}-{{$all->id}}" aria-expanded="false" aria-controls="collapseOne2">
                                      <i class="fa fa-gears"></i>
                                      <span class="text-muted muted" style="padding-left: 0px;color:white !important">@lang('front.advanced_setting.standard.edit')</span>
                                    </a>
                                </div>

                                <div class="col-sm-12">
                                  <div id="restrict-access-activity-content-{{$all->section_type}}-{{$all->id}}" class="card-collapse collapse setting-default" role="tabpanel" aria-labelledby="course-default-activity-completion-1">
                                    <ul class="nav nav-tabs  shadow-2dp" role="tablist">
                                        <li class="nav-item"><a class="nav-link withoutripple active" href="#restrict-activity-completion-{{$all->section_type}}-{{$all->id}}" aria-controls="course-completion" role="tab" data-toggle="tab"><i class="zmdi zmdi-home"></i> <span class="d-none d-sm-inline">@lang('front.advanced_setting.restrict.activity_completion')</span></a></li>
                                        <li class="nav-item"><a class="nav-link withoutripple" href="#restrict-grade-{{$all->section_type}}-{{$all->id}}" aria-controls="activitye-completion" role="tab" data-toggle="tab"><i class="zmdi zmdi-email"></i> <span class="d-none d-sm-inline">Grade</span></a></li>
                                    </ul>

                                    <div class="">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane fade active show" id="restrict-activity-completion-{{$all->section_type}}-{{$all->id}}">
                                            <button class="btn btn-success btn-raised btn-activity-add" from-id={{$all->id}} type="button" style="float: right;padding: 5px 21px;"><i class="fa fa-plus"></i> Tambah Activity Completion</button>
                                            <h5 class="headline headline-md" style="font-size: 23px;"><span>@lang('front.advanced_setting.restrict.activity_completion')</span></h5>
                                            <br>
                                            <div class="availability-item">
                                              <input type="hidden" name="restrict_item[{{$all->id}}][id]" value="{{$all->id}}">
                                              <input type="hidden" name="restrict_item[{{$all->id}}][type]" value="{{$all->section_type}}">
                                              @foreach($all->restrict as $restrict_content)
                                              <div class="restrict-item">
                                                <div class="row">
                                                  <div class="col-sm-5">
                                                    <label>
                                                      <span class="accesshide">Activity </span>
                                                      <br>
                                                      <select class="custom-select-restrict-activity" name="restrict_item[{{$all->id}}][restrict][]" title="Activity or resource" required>
                                                        <option value="">Choose...</option>
                                                        @foreach(collect($section_all)->sortBy('activity_order.order') as $all_content)
                                                            @php
                                                            if($all_content->section_type == 'content'){
                                                              $content = $all_content;
                                                              $restrict_id = $content->id;
                                                              $title = $content->title;
                                                            }elseif($all_content->section_type == 'quizz'){
                                                              $quizz = $all_content;
                                                              $restrict_id = $quizz->id;
                                                              $title = $quizz->name;
                                                            }elseif($all_content->section_type == 'assignment'){
                                                              $assignment = $all_content;
                                                              $restrict_id = $assignment->id;
                                                              $title = $assignment->title;
                                                            }
                                                            @endphp
                                                            @if($all->id != $all_content->id)
                                                            <option value="{{$restrict_id}}-{{$all_content->section_type}}" {{$restrict_id == $restrict_content->to->id ? 'selected' : ''}} type-attr="{{$all_content->section_type}}">{{$title}}</option>`+
                                                            @endif
                                                        @endforeach
                                                      </select>
                                                    </label>
                                                  </div>
                                                  <div class="col-sm-5">
                                                    <label>
                                                      <span class="accesshide">Required completion status </span>
                                                      <br>
                                                      <select class="custom-select" name="e" title="Required completion status" required>
                                                        <option value="1">must be marked complete</option>
                                                      </select>
                                                    </label>
                                                  </div>
                                                  <div class="col-sm-2 text-center">
                                                    <br>
                                                    <button type="button" class="btn btn-danger btn-raised btn-activity-remove-keep"><i class="fa fa-trash"></i></button>
                                                  </div>
                                                </div>
                                              </div>
                                              @endforeach
                                            </div>
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade" id="restrict-grade-{{$all->section_type}}-{{$all->id}}">
                                            <button class="btn btn-success btn-raised btn-activity-grade-add" type="button" style="float: right;padding: 5px 21px;"><i class="fa fa-plus"></i> Tambah Grade Completion</button>
                                            <h5 class="headline headline-md" style="font-size: 23px;"><span>Grade</span></h5>
                                            <br>
                                            <div class="availability-item">
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                             </div>
                          </div>
                          <hr class="row">
                          @endforeach
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="acticity-order-setting">
                        <h3 class="headline headline-md"><span>Activity completion</span></h3>
                        <br>
                        <div class="modules">
                          @if($activity_order == false)
                          <button class="btn btn-success btn-raised btn-activity-order-generate" type="button"><i class="fa fa-plus"></i> Buat Order</button>
                          @else
                          <div class="modules ContentSortable">
                            @foreach(collect($section_all)->sortBy('activity_order.order') as $all)
                              @if($all->section_type == 'content')
                                @php
                                  $content = $all;
                                  $default_type = $content->type_content;
                                  if (strpos($default_type, '-') !== false) {
                                      $default_type = explode('-', $default_type);
                                      $default_type = $default_type[1];
                                  }
                                @endphp
                                <div class="mb-1" style="margin-left: 20px;" order-id="{{$content->activity_order->order}}" id="{{$content->activity_order->id}}">
                                   <div class="row mb-1 row-fluid">
                                      <div class="col-md-8">
                                          @if($default_type == 'text')
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/wiki/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$content->title}}</span>
                                          @elseif($default_type == 'file')
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/resource/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$content->title}}</span>
                                          @elseif($default_type == 'video')
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/zoom/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$content->title}}</span>
                                          @elseif($default_type == 'discussion')
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/forum/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$content->title}}</span>
                                          @else
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/label/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$content->title}}</span>
                                          @endif
                                      </div>
                                      <div class="col-md-4" style="text-align:right">
                                          <a class="btn btn-success btn-raised" style="padding: 8px;" role="button">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 0px;color:white !important">Drag To Change</span>
                                          </a>
                                      </div>
                                   </div>
                                </div>
                              @endif

                              @if($all->section_type == 'quizz')
                                @php
                                  $quizz = $all;
                                  $default_type = 'quizz';
                                @endphp
                                <div class="mb-1" style="margin-left: 20px;" order-id="{{$quizz->activity_order->order}}" id="{{$quizz->activity_order->id}}">
                                   <div class="row mb-1 row-fluid">
                                      <div class="col-md-8">
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/quiz/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$quizz->name}}</span>
                                      </div>
                                      <div class="col-md-4" style="text-align:right">
                                          <a class="btn btn-success btn-raised" role="button" style="padding: 8px;">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 0px;color:white !important">Drag To Change</span>
                                          </a>
                                      </div>
                                   </div>
                                </div>
                              @endif

                              @if($all->section_type == 'assignment')
                                @php
                                  $assignment = $all;
                                  $default_type = 'assigment';
                                @endphp
                                <div class="mb-1" style="margin-left: 20px;" order-id="{{$assignment->activity_order->order}}" id="{{$assignment->activity_order->id}}">
                                   <div class="row mb-1 row-fluid">
                                      <div class="col-md-8">
                                          <img class="iconlarge activityicon" src="https://testmoodle.moodlenesia.com/theme/image.php/moove/assign/1598246781/icon" alt=" " role="presentation">
                                          <span>{{$assignment->title}}</span>
                                      </div>
                                      <div class="col-md-4" style="text-align:right">
                                          <a class="btn btn-success btn-raised" role="button" style="padding: 8px;">
                                            <i class="fa fa-gears"></i>
                                            <span class="text-muted muted" style="padding-left: 0px;color:white !important">Drag To Change</span>
                                          </a>
                                      </div>
                                   </div>
                                </div>
                              @endif

                            @endforeach
                          </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div> <!-- card -->
                </div>

                <input type="hidden" value="{{$course->id}}" name="id">
                <input type="hidden" value="{{$course->id_level_course}}" name="id_level_course">
                <input type="submit" name="submit" value="@lang('front.advanced_setting.standard.save')" class="btn btn-primary btn-raised">
              </div>
            </div>
            {{Form::close()}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <script src="/jquery-ui/jquery-ui.js"></script>
  <!-- INTERNAL  FILE UPLOADES JS -->
  <script src="{{asset('/fileuploads/js/fileupload.js')}}"></script>
  <script src="{{asset('/fileuploads/js/file-upload.js')}}"></script>

  <script>
    var restrict_item = `<div class="restrict-item">
      <div class="row">
        <div class="col-sm-5">
          <label>
            <span class="accesshide">Activity </span>
            <br>
            <select class="custom-select-restrict-activity" title="Activity or resource" required>
            <option value="">Choose...</option>`+
            @foreach(collect($section_all)->sortBy('activity_order.order') as $all_content)
              @if($all_content->section_type == 'content')
                @php
                  $content = $all_content;
                @endphp
                `<option value="{{$content->id}}-content" type-attr="{{$content->section_type}}">{{$content->title}}</option>`+
              @endif
              @if($all_content->section_type == 'quizz')
                @php
                  $quizz = $all_content;
                @endphp
                `<option value="{{$quizz->id}}-quizz" type-attr="{{$quizz->section_type}}">{{$quizz->name}}</option>`+
              @endif
              @if($all_content->section_type == 'assignment')
                @php
                  $assignment = $all_content;
                @endphp
                `<option value="{{$assignment->id}}-assignment" type-attr="{{$assignment->section_type}}">{{$assignment->title}}</option>`+
              @endif
            @endforeach
            `</select>
          </label>
        </div>
        <div class="col-sm-5">
          <label>
            <span class="accesshide">Required completion status </span>
            <br>
            <select class="custom-select" name="e" title="Required completion status" required>
              <option value="1">must be marked complete</option>
            </select>
          </label>
        </div>
        <div class="col-sm-2 text-center">
          <br>
          <button type="button" class="btn btn-warning btn-raised btn-activity-remove"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>`;

    $("[name=status]").bootstrapSwitch()
    $("[name=level_up]").bootstrapSwitch()
    $('#enable-date').click(function(){
        if($(this).is(":checked")){
            $("#end-date").prop('disabled', false);
            $("#end-time").prop('disabled', false);
        }
        else if($(this).is(":not(:checked)")){
          $("#end-date").prop('disabled', true);
          $("#end-time").prop('disabled', true);
        }
    });

    $(".availability-item").on('click', '.btn-activity-remove', function(){
      $(this).parents(".restrict-item").remove();
    });

    $(".availability-item").on('click', '.btn-activity-remove-keep', function(){
      $(this).parents(".restrict-item").remove();
    });

    $(".availability-item").on('change', '.custom-select-restrict-activity', function(){
      var item = $(this).parents('.availability-item');
      var activity = $(this).val();
      var type = $(this).find('option:selected').attr('type-attr');

      var same = false;
      if(item.find('.restrict-item .custom-select-restrict-activity option:selected[value='+activity+']option:selected[type-attr='+type+']').length == 2){
        $(this).val(null);
        alert('Activity Sudah Dipilih.');
      }
    });

    $(".btn-activity-add").click(function(){
      var from = $(this).attr('from-id');
      var data = $.parseHTML(restrict_item);
      $(data).find('option[value='+from+']').remove();
      $(data).find('.custom-select-restrict-activity').attr('name', 'restrict_item['+from+'][restrict][]');

      $(this).siblings('.availability-item').append(data);
    });

    $('input[name=end_date_default_activity_enable]').click(function(){
        if($(this).is(":checked")){
            $(this).parents('.date-course').find('.datetime-default-activity').prop('disabled', false);
        }
        else if($(this).is(":not(:checked)")){
          $(this).parents('.date-course').find('.datetime-default-activity').prop('disabled', true);
        }
    });

    $('#condition_grade_enable').click(function(){
        if($(this).is(":checked")){
          $('#condition_grade_value').prop('disabled', false);
        }
        else if($(this).is(":not(:checked)")){
          $('#condition_grade_value').prop('disabled', true);
        }
    });

    $('.form-check-input').click(function(){
        var check_input = $(this).parents('.setting-default').find('.form-check-input:checked');
        console.log();
        var one_check = true;
        if($(this).is(":not(:checked)")){
          if(check_input.length == 0){
            alert('Salah satu kondisi harus aktif.');
            $(this).prop('checked', true);
            $(this).siblings('input').val(1);
          }
          $(this).siblings('input').val(0);
        }else {
          $(this).siblings('input').val(1);
        }
    });

    // $.validator.addMethod("roles", function(value, elem, param) {
    //    return $(".form-check-input:checkbox:checked").length > 0;
    // },"You must select at least one!");

    $(".custom-select").change(function(){
      var check = $(this).parents('.setting-default').find('.form-check-input');
      var check_array = [];
      $.each(check, function(key, value){
        check_array.push($(this).val());
      });
      if($(this).val() == 2){
        $(this).parents('.setting-default').find('.form-check-input').prop('disabled', false);
      }else {
        $(this).parents('.setting-default').find('.form-check-input').prop('disabled', true);
        $.each(check, function(key, value){
          if(check_array[key] == 1){
            $(this).siblings('input').val(1);
            $(this).prop('checked', true);
          }else {
            $(this).siblings('input').val(0);
            $(this).prop('checked', false);
          }
        });
      }
    });

    $(".bt-activity-completion").click(function(){
      var token = '{{ csrf_token() }}';
      var content_id = $(this).attr('content-id');
      var content_type = $(this).attr('content-type');

      $.ajax({
        url : "{{url('course/advanced/setting/activity/completion/create')}}/"+content_id+"/"+content_type,
        type : "POST",
        data : {
          _token: token
        },
        success : function(result){
          location.reload();
        },
      });
    })

    $(".btn-restrict-order").click(function(){
      $.ajax({
        url : "{{url('course/advanced/setting/restrict/order/create')}}/"+{{$course->id}},
        type : "POST",
        data : {
          _token: '{{ csrf_token() }}'
        },
        success : function(result){
          location.reload();
        },
      });
    });

    $(".btn-activity-order-generate").click(function(){
      var token = '{{ csrf_token() }}';
      var course_id = {{$course->id}};

      $.ajax({
        url : "{{url('course/advanced/setting/activity/order/generate')}}/"+course_id,
        type : "POST",
        data : {
          _token: token
        },
        success : function(result){
          // console.log(result)
          location.reload();
        },
      });
    })

    $(".bt-activity-completion-delete").click(function(){
      var token = '{{ csrf_token() }}';
      var content_id = $(this).attr('content-id');

      $.ajax({
        url : "{{url('course/advanced/setting/activity/completion/destroy')}}/"+content_id,
        type : "POST",
        data : {
          _token: token
        },
        success : function(result){
          location.reload();
        },
      });
    })
  </script>

	{{-- CKEDITOR --}}
	<script type="text/javascript">
	  // CKEDITOR.replace('goal', {
		// 	filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
    //   filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		// });
		CKEDITOR.replace('description', {
			filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
      		filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		});

		// CKEDITOR.replace('section_description', {
		// 	filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
      	// filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		// });
	</script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#id_category").select2();
		$("#id_level_course").select2();
		$("#id_author").select2({
		   placeholder: "Pilih pengajar"
		});
		$("#id_instructor_group").select2({
		   placeholder: "Pilih group"
		});

    $("#check-img").change(function(){
      if (this.checked) {
            $(".img-check-upload").removeClass('hidden');
        } else {
            $(".img-check-upload").addClass('hidden');
        }
    });
	</script>
	{{-- SELECT2 --}}

  <script>
    $( function() {
      var section_id = ''
      $( ".ContentSortable" ).sortable({
        axis: 'y',
        connectWith: '.ContentSortable',
        helper: 'clone',
        placeholder: "placeholder",
        over:function(event,ui){

        },
        out: function(event, ui) {
          var sequence = ui.item.index();
          var order_id = ui.item.attr('order-id');
          var itemOrder = $(this).sortable('toArray', { attribute: 'id' });
          $.ajax({
            url : '/course/content/order/update',
            type : 'POST',
            data : {
              'datas' : itemOrder,
              '_token' : '{{csrf_token()}}'
            },
            success : function(result){
              console.log(result);
            }
          })
        },
      }).disableSelection();
    });

  </script>

  @endpush
