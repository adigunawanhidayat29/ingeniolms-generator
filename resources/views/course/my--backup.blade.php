@extends('layouts.app')

@section('title', 'Kelas Saya')

@push('style')
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">
  <style media="screen">
    .swal2-container {
      z-index: 10000;
    }
		.filter button{
      width: 250px !important;
      display: block;
		}
    @media (min-width:768px) and (max-width:1023px){
      .filter button{
        width: 129px !important;
        display: block;
        /* color: #ffffff !important; */
      }
    }
    @media (max-width:767px){
      .filter button{
        width: 100% !important;
        display: block;
        /* color: #ffffff !important; */
      }
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Kelas <span>Saya</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li>Kelas Saya</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-right">
            <a href="#/" class="btn btn-primary btn-primary btn-raised" data-toggle="modal" data-target="#modalPrivateCourse"><i class="fa fa-plus"></i> Ikuti Kelas Private</a>
          </div>
          <div class="card-md mb-2">
            <div class="card-block">
              <div class="row">
                <div class="col-md-5">
                  <form class="form-group mt-1" action="" method="get">
                    <input value="{{Request::get('q')}}" type="text" name="q" class="form-control" placeholder="Pencarian modul...">
                    {!!Request::get('q') ? '<a href="/my-course">Reset pencarian</a>' : ''!!}
                  </form>
                </div>
                <div class="col-md-7">
                  <form class="form-inline" action="" method="get">
                    <div class="form-group mt-1">
                      <select class="form-control selectpicker no-m filter" name="filter">
                        <option value="">Filter Berdasarkan</option>
                        <option {{Request::get('filter') == 'title' ? 'selected' : ''}} value="title">Judul</option>
                        <option {{Request::get('filter') == 'created_at' ? 'selected' : ''}} value="created_at">Tanggal</option>
                      </select>
                    </div>
                    <div class="form-group mt-1">
                      <select class="form-control selectpicker no-m filter" name="sort">
                        <option value="">Urutkan</option>
                        <option {{Request::get('sort') == 'asc' ? 'selected' : ''}} value="asc">A-Z (Ascending)</option>
                        <option {{Request::get('sort') == 'desc' ? 'selected' : ''}} value="desc">Z-A (Descending)</option>
                      </select>
                    </div>
                    <input type="submit" class="btn btn-sm btn-border" value="Filter">
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            @foreach($courses_datas as $course)
            @php
              $num_contents = $course['num_contents'];
              //count percentage
              $num_progress = 0;
              $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
              $percentage = 0;
              $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
              $percentage = 100 / $percentage;
              //count percentage
            @endphp

            <div class="col-md-3 mb-2">
              <div class="card-sm">
                <a href="/bigbluebutton/meeting/join/{{$course['id']}}">
                  <span class="course-live">{!!is_liveCourse($course['id']) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
                </a>
                <a href="{{url('course/learn/'. $course['slug'])}}">
                  <img src="{{asset_url(course_image_small($course['image']))}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
                </a>
                <a href="{{url('course/'. $course['slug'])}}">
                  <div class="card-block text-left">
                    <span class="badge badge-danger">Live</span>
                    <span class="badge badge-warning">Tertutup</span>
                    <span class="badge badge-success">Terjadwal</span>
                    <h4 class="headline-xs dq-max-title">{{$course['title']}}</h4>
                    <p class="section-body-xs">
                      <span class="dq-max-subtitle">{{ $course['name'] }}</span>
                    </p>
                    <div class="m-0">
                      <div class="progress">
                        <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
                      </div>
                      <span class="color-primary dq-span-border">
                        {{$percentage > 1 ? 'LANJUTKAN' : 'MULAI'}}
                      </span>
                      <br><br>
                      {{-- <span class="mr-2 pull-right">
                        <i class="zmdi zmdi-star color-warning"></i>
                        <i class="zmdi zmdi-star color-warning"></i>
                        <i class="zmdi zmdi-star color-warning"></i>
                        <i class="zmdi zmdi-star color-warning"></i>
                        <i class="zmdi zmdi-star color-dark"></i>
                      </span> --}}
                    </div>
                  </div>
                </a>
              </div>
            </div>
            @endforeach
          </div>
          {{$courses->links('pagination.default')}}
        </div>
      </div>
    </div>
  </div>


  <!-- ===================================================================== -->
  <!-- <div class="container mt-6">
    <div class="text-right">
      <a href="#/" class="btn btn-primary btn-primary btn-raised" data-toggle="modal" data-target="#modalPrivateCourse"><i class="fa fa-plus"></i> Ikuti Kelas Private</a>
    </div>

    <div class="well well-md mx-auto">
      <div class="row">
        <div class="col-md-12">
          <form class="form-inline" action="" method="get">
            <input value="{{Request::get('q')}}" type="text" name="q" class="form-control" placeholder="Pencarian modul...">
            {!!Request::get('q') ? '<a href="/my-course">Reset pencarian</a>' : ''!!}
            <div class="form-group mt-1">
              <select class="form-control selectpicker no-m filter" name="filter">
                <option value="">Filter Berdasarkan</option>
                <option {{Request::get('filter') == 'title' ? 'selected' : ''}} value="title">Judul</option>
                <option {{Request::get('filter') == 'created_at' ? 'selected' : ''}} value="created_at">Tanggal</option>
              </select>
            </div>

            <div class="form-group mt-1">
              <select class="form-control selectpicker no-m filter" name="sort">
                <option value="">Urutkan</option>
                <option {{Request::get('sort') == 'asc' ? 'selected' : ''}} value="asc">A-Z (Ascending)</option>
                <option {{Request::get('sort') == 'desc' ? 'selected' : ''}} value="desc">Z-A (Descending)</option>
              </select>
            </div>

            <input type="submit" class="btn btn-default btn-raised" value="Filter">

          </form>
        </div>
      </div>
    </div>

    @if(count($course_lives) == 0 && count($courses_datas) == 0)
      <h3>Kelas Anda masih kosong</h3>
      <p>Silakan ikuti kelas yang tersedia di ingenio dengan nyaman</p>
      <a href="/courses" class="btn btn-lg btn-primary btn-raised">Lihat Kelas</a>
    @endif

    @if(count($course_lives) >= 1)
      <h1>Kelas Live</h1>
    @endif
    <div class="row">
    @foreach($course_lives as $course)
        <div class="col-md-3">
          <div class="card">
            <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
            <a href="{{url('course/'. $course->slug)}}" class="zoom-img withripple">
              <img src="{{asset_url(course_image_small($course->image))}}" alt="{{ $course->title }}" class="dq-image-big img-fluid">
            </a>
            <div class="card-block text-left">
              <a href="{{url('course/'. $course->slug)}}">
                <h4 class="color-primary dq-max-title">{{$course->title}}</h4>
              </a>
              <p>
                <i class="color-dark"><a href="/user/{{ $course->user_slug }}">{{ $course->name }}</a></i>
              </p>
              <div class="mt-2 text-right">
                <a href="{{url('course/'. $course->slug)}}" class="btn btn-raised btn-primary">
                  <i class="fa fa-money"></i>{{$course->price == 0 ? 'Free' : rupiah($course->price)}}
                </a>
              </div>
            </div>
          </div>
        </div>
    @endforeach
    </div>

    @if(count($courses_datas) >= 1)
      <h1>Kelas Video</h1>
    @endif
    <div class="row">
      @foreach($courses_datas as $course)
      @php
        $num_contents = $course['num_contents'];
        //count percentage
        $num_progress = 0;
        $num_progress = count(DB::table('progresses')->where(['course_id'=>$course['id'], 'user_id' => Auth::user()->id, 'status' => '1'])->get());
        $percentage = 0;
        $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
        $percentage = 100 / $percentage;
        //count percentage
      @endphp

        <div class="col-md-3">
          <div class="card">
            <a href="/bigbluebutton/meeting/join/{{$course['id']}}">
              <span class="course-live">{!!is_liveCourse($course['id']) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
            </a>
            <a href="{{url('course/learn/'. $course['slug'])}}" class="zoom-img withripple">
              <img src="{{asset_url(course_image_small($course['image']))}}" alt="{{ $course['title'] }}" class="dq-image img-fluid">
            </a>
            <a href="{{url('course/'. $course['slug'])}}">
              <div class="card-block text-left">
                <h4 class="color-primary dq-max-title">{{$course['title']}}</h4>
                <p>
                  <i class="color-dark">{{ $course['name'] }}</i>
                </p>
                <div class="mt-2">
                <div class="progress">
                  <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">{{ceil($percentage)}}%</div>
                </div>
                <span class="color-primary dq-span-border">
                  {{$percentage > 1 ? 'LANJUTKAN' : 'MULAI'}}
                </span>
                <br><br>
                {{-- <span class="mr-2 pull-right">
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-warning"></i>
                  <i class="zmdi zmdi-star color-dark"></i>
                </span> --}}
                </div>
              </div>
            </a>
          </div>
        </div>
        @endforeach
      </div>
      {{$courses->links('pagination.default')}}

      @if(count($course_privates) >= 1)
        <h1>Kelas Private</h1>
      @endif
      <div class="row">
      @foreach($course_privates as $course)
          <div class="col-md-3">
            <div class="card">
              <a href="{{url('course/learn/'. $course->slug)}}" class="zoom-img withripple">
                <img src="{{asset_url(course_image_small($course->image))}}" alt="{{ $course->title }}" class="dq-image-big img-fluid">
              </a>
              <div class="card-block text-left">
                <a href="{{url('course/learn/'. $course->slug)}}">
                  <h4 class="color-primary dq-max-title">{{$course->title}}</h4>
                </a>
                <p>
                  <i class="color-dark"><a href="/user/{{ $course->user_slug }}">{{ $course->name }}</a></i>
                </p>
                <div class="mt-2 text-right">
                  <a href="{{url('course/'. $course->slug)}}" class="btn btn-raised btn-primary">
                    <i class="fa fa-money"></i>{{$course->price == 0 ? 'Free' : rupiah($course->price)}}
                  </a>
                </div>
              </div>
            </div>
          </div>
      @endforeach
      </div>

  </div> -->
  <!-- ===================================================================== -->


  <!-- Modal -->
  <div class="modal" id="modalPrivateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password / Kode</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password / Kode</label>
                  <input type="text" class="form-control" id="CoursePassword" placeholder="Masukan Password / Kode">
                </div>
              </div>
              <div class="modal-footer">
                <button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">Gabung</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->
@endsection

@push('script')
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/course/join-private-course',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          console.log(response);
          if(response.code == 200){
            $.ajax({
              url : '/course/enroll',
              type : 'get',
              data : {
                id : response.data.id,
              },
              success : function(data){
                swal({
                  type: 'success',
                  title: 'Berhasil...',
                  html: response.message,
                }).then(function() {
                    window.location.assign('/course/learn/' + response.data.slug)
                });
              }
            })
          }else{
            swal({
              type: 'error',
              title: 'Gagal!',
              html: response.message,
            })
          }
        }
      })
    })
  </script>
@endpush
