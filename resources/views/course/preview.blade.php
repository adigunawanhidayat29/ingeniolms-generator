@extends('layouts.app')

@section('title', Lang::get('front.page_manage_courses.title') . " - " . $course->title)

@push('style')
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="/css/bootstrap-editable.css">
  <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{asset('uploadify-master/sample/uploadifive.css')}}">
  <style media="screen">
    .editableform .form-group{
      margin: 0px !important;
      padding: 0px !important;
    }
    .editableform .form-group .form-control{
      height: 33px;
      padding: 0px;
      margin-top: -1.3rem;
      margin-bottom: 0px;
      line-height: auto;
    }
    .editable-input .form-control{
      color: white !important;
      width: auto;
      font-size: 2.4rem;
      font-weight: bold;
    }
    .editable-buttons{
      display: none;
    }
  </style>
  <style>
    .modal .modal-dialog .modal-content .modal-body.preview-modal-tab {
      display: flex;
      padding: 0;
    }

    .preview-modal-tab .tab {
      background-color: #f1f1f1;
      width: 30%;
      height: auto;
    }

    .preview-modal-tab .tab .spacer {
      background: #fcfcfc;
      height: 5px;
    }

    .preview-modal-tab .tab button {
      display: block;
      background-color: inherit;
      color: #424242;
      padding: 1.5rem 2.5rem;
      width: 100%;
      border: none;
      outline: none;
      text-align: left;
      cursor: pointer;
      transition: 0.3s;
      font-size: 14px;
    }

    .preview-modal-tab .tab button:hover {
      background-color: #ddd;
    }

    .preview-modal-tab .tab button.active {
      background-color: #ccc;
    }

    .preview-modal-tab .tabcontent {
      background: #fcfcfc;
      width: 70%;
      height: auto;
      padding: 2rem;
    }

    .preview-modal-tab .tabcontent h3 {
      margin-top: 0;
      margin-bottom: 1rem;
    }

    .placeholder{
      background-color:white;
      height:18px;
    }

    .move:hover{
      cursor: move;
    }

    .lh-3{
      line-height: 3;
    }
    /* .ContentSortable{
      border:0.2px solid #dfe6e9;
      min-height:60px;
    } */
    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
      width: 120px;
      height: 120px;
    }

    .togglebutton label .toggle{
      margin-left: 15px;
      margin-right: 15px;
    }

    .preview-group{
      display: inline-flex;
    }
      .preview-group .preview{
        text-align: center;
      }
      .preview-group .status{
        margin-right: 1rem;
      }

    .date-time span{
      display: inline-block;
      margin-bottom: 0;
      margin-right: 1rem;
    }

    @media (max-width: 768px){
      .preview-group{
        display: block;
      }
        .preview-group .duration{
          text-align: right;
        }
        .preview-group .preview{
          display: inline-flex;
          margin-right: 1rem;
        }
        .preview-group .status{
          text-align: right;
          margin-right: 0;
        }
        .togglebutton label .toggle{
          margin-left: 10px;
          margin-right: 0;
        }
      .date-time span{
        display: block;
        margin-bottom: 0.5rem;
        margin-right: 0;
      }
    }
  </style>
  <style media="screen">
    .eyeCheckbox > input{
        display: none;
      }
      .eyeCheckbox input[type=checkbox]{
        opacity:0;
        position: absolute;
      }
      .eyeCheckbox i{
        cursor: pointer;
      }
      .list-group{
      border: 0;
    }

    .btn-opsi-preview button {
      line-height: 1;
    }

    .list-group li {
      list-style: none;
    }

    .list-group li .list-group-item{
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      border: 1px solid #eee;
      margin-bottom: -1px;
    }

    .list-group li .list-group-item.disabled,
    .list-group li .list-group-item.disabled .item-content {
      background: #eee;
      color: #999;
    }

    .item-content {
      width: 100%;
      display: flex;
      align-items: baseline;
    }

    .item-content i {
      margin-right: 1rem;
    }

    .importLibraryModal .nav-tabs-ver-container .nav-tabs-ver {
      padding: 0;
      margin: 0;
    }

    .course-library .form-group{
      padding-bottom: 7px;
      margin: 0;
      padding: 10px;
      padding-bottom: 0;
    }

    .library-list{
      margin: 10px !important;
    }

    .table-responsive{
      max-height: 500px;
    }

    .nav.indicator-info{
      display: table;
    }

    .nav.indicator-info.nav-tabs.nav-tabs-full.nav-tabs-6 li{
      display: table-cell;
      width:auto;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .edit-image-button {
      position: absolute;
      top: 1rem;
      right: 3rem;
      background-color: #f9f9f9;
      padding: .25rem 1rem;
      border-radius: 5px;
      transition: all 0.3s;
    }

    .edit-image-button:hover {
      background-color: #ddd;
    }

    .edit-image-button i {
      color: #333;
    }
  </style>
  <style>
    .dropdown-item:focus,
    .dropdown-item:hover,
    .dropdown-menu li a.dropdown-item:hover {
      background-color: #f3f3f3;
      color: #16181b;
    }

    .modal .modal-dialog .modal-content .modal-header {
      border-bottom: 1px solid #eee;
    }

    .modal .modal-dialog .modal-content .modal-header .modal-title {
      width: 100%;
      padding: 2rem 3rem;
    }

    .modal .modal-dialog .modal-content .modal-header .close {
      line-height: 1.65;
      margin: 2rem 3rem;
    }

    .ContentSortable li{
      border: 1px solid #eee;
    }

    .ContentSortable .list-group-item{
      border: none !important;
    }

    .restrict ul li{
      list-style: disc;
      border: none;
    }

    .restrict{
      margin-left: 40px;
    }

    .restrict h5{
      margin-top:0px
    }
    .uploadifive-button:hover {
        background-color: none;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.page_manage_courses.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.page_manage_courses.breadcumb_manage_course')</a></li>
          <li>{{$course->title}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div style="background:#393939; padding:25px;margin-top:0px;">
    <div class="container">
      <center>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('success') !!}
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-error alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('error') !!}
          </div>
        @endif
      </center>
      <div class="row">
        <div class="col-md-5">
          <a href="/bigbluebutton/meeting/join/{{$course->id}}">
            <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.gif" />' : ''!!}</span>
          </a>
          @if($course->introduction != NULL)
            <div class="card">
              <div class="embed-responsive embed-responsive-16by9" style="position:relative;">
                <iframe class="embed-responsive-item" src="{{$course->introduction.'/preview'}}" allowfullscreen="true"></iframe>
                <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
              </div>
            </div>
          @else
            <img class="img-fluid" src="{{$course->image}}" alt="{{$course->title}}">
            <a href="#" data-target="#modalSetImage" data-toggle="modal" class="edit-image-button"><i class="fa fa-pencil"></i></a>
          @endif
        </div>
        <div class="col-md-7">
          <h3 class="no-m" style="font-weight:bold;">
            <a class="color-white fs-30" href="#" id="CourseTitle" data-type="text">
              {{$course->title}} <i class="fa fa-pencil color-white"></i>
            </a>
          </h3>
          <h4 class="mt-1" style="font-weight:bold;">
            <a class="color-white fs-20" href="#" id="CourseSubTitle" data-type="text">
              {{$course->subtitle}} <i class="fa fa-pencil color-white"></i>
            </a>
          </h4>
          <p>
            <span>
              <a href="#">
                @for($i=1;$i<=$rate;$i++)
                  <i class="fa fa-star color-warning"></i>
                @endfor
                @for($i=$rate;$i<5;$i++)
                  <i class="fa fa-star color-white"></i>
                @endfor
                @lang('front.page_manage_courses.give_rating')
              </a>
            </span>
          </p>
          <p style="color:white">
            <strong>{{$num_progress}}</strong> @lang('front.page_manage_courses.content_from') <strong>{{$num_contents}}</strong> @lang('front.page_manage_courses.module_finished') <a href="#">@lang('front.page_manage_courses.progress_reset')</a>
            <div class="progress">
              <div class="progress-bar" role="progressbar"
                aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%">
                {{ceil($percentage)}}%
              </div>
            </div>
          </p>
          <div class="d-flex align-items-center justify-content-between">
            <a href="/course/learn/{{$course->slug}}" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_open_preview') <i class="zmdi zmdi-arrow-right ml-1 mr-0"></i></a>
            {{-- <a href="/course/advanced/setting/{{$course->id}}" style="float:right" class="btn btn-primary btn-raised">Advanced Setting <i class="zmdi zmdi-arrow-right ml-1 mr-0"></i></a> --}}
            <a href="/course/advanced/setting/{{$course->id}}" class="btn-circle btn-circle-sm btn-circle-primary btn-circle-raised" title="Pengaturan Lanjutan"><i class="zmdi zmdi-settings"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-flat">
          <ul class="nav nav-tabs nav-tabs-full" role="tablist">
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
                <i class="fa fa-eye"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_overview')</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#update" aria-controls="overview" role="tab" data-toggle="tab">
                <i class="fa fa-list"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_update')</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple {{is_liveCourse($course->id) == false ? 'active' : ''}}" href="#course-content" aria-controls="course-content" role="tab" data-toggle="tab">
                <i class="fa fa-graduation-cap"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_content')</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple {{is_liveCourse($course->id) == true ? 'active' : ''}}" href="#meeting" aria-controls="meeting" role="tab" data-toggle="tab">
                <i class="fa fa-users"></i>
                <span class="d-none d-sm-inline">{{is_liveCourse($course->id) == true ? Lang::get('front.page_manage_courses.horizontal_tab_live') : Lang::get('front.page_manage_courses.horizontal_tab_webinar')}}</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
                <i class="fa fa-question-circle-o"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_dicuss')</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                <i class="fa fa-bullhorn"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_announcement')</span>
              </a>
            </li>
            {{-- <li class="nav-item">
              <a class="nav-link withoutripple" href="#progresses" aria-controls="progresses" role="tab" data-toggle="tab">
                <i class="fa fa-circle-o-notch"></i>
                <span class="d-none d-sm-inline">Administrasi</span>
              </a>
            </li> --}}
            <li class="nav-item">
              <a class="nav-link withoutripple" href="{{url('course/atendee/'.$course->id)}}">
                <i class="fa fa-circle-o-notch"></i>
                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_administration')</span>
              </a>
            </li>

            @if(isManager(Auth::user()->id) === true)
              <li class="nav-item">
                <a class="nav-link withoutripple" href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                  <i class="fa fa-cog"></i>
                  <span class="d-none d-sm-inline">@lang('front.page_manage_courses.horizontal_tab_settings')</span>
                </a>
              </li>
            @endif
          </ul>
        </div>

        <div class="card-block no-pl no-pr">
          <div class="tab-content">
            <!-- TAB OVERVIEW -->
            <div role="tabpanel" class="tab-pane fade" id="overview">
              <div class="row">

                <!--RECENT ACTIVITY-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.page_manage_courses.overview_newest_activity')</h2>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <b>@lang('front.page_manage_courses.overview_newest_questions')</b>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <b>@lang('front.page_manage_courses.overview_newest_announcements')</b>
                      </li>
                    </ul>
                  </div>
                </div>

                <!--ABOUT COURSE-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.page_manage_courses.overview_about_courses')</h2>
                </div>
                <div class="col-md-12">
                  <div class="card card-info">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_with_number')</b>
                        </div>
                        <ul class="col-md-3">
                          <li class="dq-list-item">@lang('front.page_manage_courses.overview_total_course_modules') {{count($sections)}}</li>
                          <li class="dq-list-item">@lang('front.page_manage_courses.overview_course_category')
                            <a href="#" id="CourseCategory" data-type="select">
                              {{$course->category}} <i class="fa fa-pencil"></i>
                            </a>
                            </li>
                          <li class="dq-list-item">@lang('front.page_manage_courses.overview_course_skill_level')
                            <a href="#" id="CourseLevel" data-type="select">
                              {{$course->level}} <i class="fa fa-pencil"></i>
                            </a>
                          </li>
                          @lang('front.page_manage_courses.overview_course_price')
                          <a href="#" id="CoursePrice" data-type="text">
                            {{$course->price}} <i class="fa fa-pencil"></i>
                          </a>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_course_description')</b>
                        </div>
                        <ul class="col-md-8">
                          <div id="recentDescription">
                            {!! $course->description !!}
                          </div>
                          <a href="#" id="getEditorDescription" data-target="#modalSetEditorDescription" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_course_goal')</b>
                        </div>
                        <ul class="col-md-8">
                            <div id="recentGoal">
                              {!! $course->goal !!}
                            </div>
                            <a href="#" id="getEditorGoal" data-target="#modalSetEditorGoal" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                        </ul>
                      </div>
                      @if($course->model == 'private')
                        <hr>
                        <div class="row">
                          <div class="col-md-2">
                            <b>@lang('front.page_manage_courses.overview_course_password')</b>
                          </div>
                          <ul class="col-md-8">
                              <div id="recentGoal">
                                {{ $course->password }}
                              </div>
                          </ul>
                        </div>
                      @endif
                      @if($course->model == 'live')
                        @php
                          $course_live = DB::table('course_lives')->where('course_id', $course->id)->first();
                        @endphp
                        <hr>
                        <div class="row">
                          <div class="col-md-2">
                            <b>@lang('front.page_manage_courses.overview_course_start_date')</b>
                          </div>
                          <ul class="col-md-8">
                              <div id="recentGoal">
                                {{$course_live->start_date}}
                              </div>
                          </ul>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col-md-2">
                            <b>@lang('front.page_manage_courses.overview_course_end_date')</b>
                          </div>
                          <ul class="col-md-8">
                              <div id="recentGoal">
                                {{$course_live->end_date}}
                              </div>
                          </ul>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col-md-2">
                            <b>@lang('front.page_manage_courses.overview_course_total_live')</b>
                          </div>
                          <ul class="col-md-8">
                              <div id="recentGoal">
                                {{$course_live->meeting_total}}
                              </div>
                          </ul>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col-md-2">
                            <b>@lang('front.page_manage_courses.overview_course_total_student')</b>
                          </div>
                          <ul class="col-md-8">
                              <div id="recentGoal">
                                {{$course_live->participant_total}}
                              </div>
                          </ul>
                        </div>
                      @endif
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_course_teacher')</b>
                        </div>
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-2">
                              <img src="{{avatar($course->photo)}}" class="img-circle">
                            </div>
                            <div class="col-md-6">
                              <h3 class="color-dark">{{$course->author}}</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_course_get_ceritificate')</b>
                        </div>
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-12">
                              {{ $course->get_ceritificate == '0' ? Lang::get('front.page_manage_courses.overview_course_no_ceritificate') : ($course->get_ceritificate == '1' ? Lang::get('front.page_manage_courses.overview_course_get_ceritificate_percentage') : ($course->get_ceritificate == '2' ? Lang::get('front.page_manage_courses.overview_course_get_ceritificate_gradebook') : Lang::get('front.page_manage_courses.overview_course_get_ceritificate_manual'))) }}
                              <p><a href="#" data-target="#modalEditGetCeritificate" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <b>@lang('front.page_manage_courses.overview_course_publish')</b>
                        </div>
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-12">
                              @php $status_checked = $course->status == '1' ? 'checked' : ''; @endphp
                              <div class="form-group m-0">
                                <div class="togglebutton">
                                  <label class="control-label m-0" title="Terbitkan atau simpan sbg draf">
                                    <input type="checkbox" {{$status_checked}} name="status{{$course->id}}">
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- TAB Update -->
            <div role="tabpanel" class="tab-pane fade" id="update">
              <div class="row">

                <!--RECENT ACTIVITY-->
                <div class="col-md-12">
                  <h2 class="headline-sm headline-line">@lang('front.page_manage_courses.update_newest_title')</h2>
                  <div class="form-group no-m">
                    <form action="{{url('course/update/submit')}}" method="post">
                      {{csrf_field()}}
                      <input type="hidden" name="course_id" value="{{$course->id}}">
                      <textarea name="body" id="answer-update" placeholder="Berikan Komentar Anda untuk membantu penanya menemukan jawaban terbaik." class="form-control" rows="8" cols="80"></textarea>
                      <div class="upload-file-new" style="background: #f8f8f8;padding: 0px 10px;border: 1px solid #d1d1d1;">
                        <input id="file_upload" name="file_upload" type="file" multiple="true">
                        <div id="queue"></div>
                      </div>

                      <div class="form-group no-m">
                        <input type="submit" class="btn btn-primary btn-raised" name="submit" value="@lang('front.page_manage_courses.submit')">
                      </div>
                    </form>
                  </div>
                  @foreach($update->where('level_1', null) as $answer)
                    <div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
                      @if($answer->user_id == Auth::user()->id)
                      <div class="groups-dropdown">
                        <div class="groups-dropdown-group">
                          <i class="fa fa-ellipsis-v"></i>
                          <ul class="groups-dropdown-menu">
                            <!-- <li>
                              <a class="groups-dropdown-item" href="#">
                                <i class="fa fa-pencil"></i> Ubah
                              </a>
                            </li> -->
                            <li>
                              <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$answer->id}}').submit()">
                                <i class="fa fa-trash"></i> @lang('front.page_manage_courses.update_answer_delete')
                              </a>
                              <form action="{{url('/course/content/discussion/delete/'.$answer->id)}}" id="form-koment-delete-{{$answer->id}}" method="post" style="display:none;">
                                {{csrf_field()}}
                              </form>
                            </li>
                          </ul>
                        </div>
                      </div>
                      @endif
                      <div class="card-block">
                        <a class="groups-title" href="#">{{$answer->user->name}}</a>
                        <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($answer->created_at))}}&nbsp;&middot;&nbsp;</p>
                        <p class="groups-description">{!!$answer->description!!}</p>
                        @if($answer->file)
                        <?php
                          $files = json_decode($answer->file);
                          usort($files, function ($a, $b) use ($files) {

                              // preg_match("/(\d+(?:-\d+)*)/", $a[0], $matches);
                              // $firstimage = $matches[1];
                              // preg_match("/(\d+(?:-\d+)*)/", $b[0], $matches);
                              // $lastimage = $matches[1];
                              $ext = explode('.',$a[0]);
                              $ext = strtolower($ext[count($ext)-1]);
                              $firstimage = $ext;
                              $ext = explode('.',$b[0]);
                              $ext = strtolower($ext[count($ext)-1]);
                              $lastimage = $ext;


                              return ($firstimage != 'png') ? -1 : 1;
                          });
                        ?>
                        <p class="fs-14" style="color: #999;">Attachment</p>
                          <div class="row">
                            @foreach($files as $file)
                              <?php
                                  $ext = explode('.',$file[0]);
                                  $ext = strtolower($ext[count($ext)-1]);
                                  $filename = $file[1];
                                  $img = asset_url('uploads/fileupload/courseupdate/'.$file[0]);
                                  if($ext == 'pdf'){
                                    $img = asset('img/pdf.png');
                                  }elseif ($ext == 'xlsx' || $ext == 'csv') {
                                    $img = asset('img/excel.png');
                                  }elseif ($ext == 'docx') {
                                    $img = asset('img/word.png');
                                  }
                               ?>
                              @if($ext == 'png' || $ext == 'jpeg' || $ext == 'jpg')
                                <div class="col-lg-3">
                                  <div class="img-attachment">
                                    <img src="{{$img}}" style="width:100%;height: 90px;border-radius: 11px;margin-bottom: 10px;" alt="{{$filename}}" title="{{$filename}}">
                                    <a class="groups-title" href="{{asset_url('uploads/fileUpload/courseupdate/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                                  </div>
                                </div>
                              @else
                              <div class="col-lg-12">
                                <div class="img-attachment">
                                  <img src="{{$img}}" style="width:50px;margin-right:20px;" alt="{{$filename}}" title="{{$filename}}">
                                  <a class="groups-title" href="{{asset_url('uploads/fileUpload/courseupdate/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                                </div>
                              </div>
                              @endif
                            @endforeach
                          </div>
                        @endif
                        <div class="reply-comment">
                          <hr style="border-color:#bbb">
                          <p class="reply-btn" style="cursor:pointer">@lang('front.page_manage_courses.update_answer_reply')</p>
                          <div class="text-reply">
                            <form method="POST" action="{{url('course/update/submit')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                              {{csrf_field()}}
                              <input type="hidden" name="course_id" value="{{$course->id}}">
                              <input name="parents" type="hidden" value="{{$answer->id}}">
                              <textarea name="body" id="discussion-reply-{{$answer->id}}" class="form-control discussion-reply" required="required"></textarea>
                              <input class="btn btn-primary btn-raised" name="button" type="submit" value="@lang('front.page_manage_courses.update_submit_comment')">
                            </form>
                          </div>
                          <div class="reply_1-page">
                            @if($answer->reply_1)
                              @foreach($answer->reply_1 as $reply_1)
                              <div class="card card-groups dropdown" style="box-shadow: none;margin-top:20px;">
                                @if($reply_1->user_id == Auth::user()->id)
                                <div class="groups-dropdown">
                                  <div class="groups-dropdown-group">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <ul class="groups-dropdown-menu">
                                      <!-- <li>
                                        <a class="groups-dropdown-item" href="#">
                                          <i class="fa fa-pencil"></i> Ubah
                                        </a>
                                      </li> -->
                                      <li>
                                        <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$reply_1->id}}').submit()">
                                          <i class="fa fa-trash"></i> @lang('front.page_manage_courses.update_answer_delete')
                                        </a>
                                        <form action="{{url('/course/content/discussion/delete/'.$reply_1->id)}}" id="form-koment-delete-{{$reply_1->id}}" method="post" style="display:none;">
                                          {{csrf_field()}}
                                        </form>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                @endif
                                <div class="card-block">
                                  <a class="groups-title" href="#">{{$reply_1->user->name}}</a>
                                  <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($reply_1->created_at))}}&nbsp;&middot;&nbsp;</p>
                                  <p class="groups-description">{!!$reply_1->description!!}</p>
                                </div>
                              </div>
                              @endforeach
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>

            <!-- TAB COURSE CONTENT -->
            <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == false ? 'active show' : ''}} " id="course-content">
              <input type="hidden" id="content-id-value" value="" />
              <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
                <?php $i=1; ?>

                <div class="panel-group SectionSortable" id="accordion fade in active">
                  @foreach($sections as $section)
                    <div class="panel panel-default" id="section{{$section['id']}}" data-id="{{$section['id']}}">
                      <div class="panel-heading" style="background: #e9e9e9; display: flex; align-items: flex-start; justify-content: space-between;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}" style="width: 100%;">
                          <div style="display: flex; align-items: baseline;">
                            <i class="move fa fa-arrows-alt mr-1"></i>
                            <div>
                              <h4 class="panel-title fw-500">{{ $section['title'] }}</h4>
                              <p class="mb-0" style="color: #aaa;">{{ $section['description'] }}</p>
                            </div>
                          </div>
                        </a>
                        <div style="width: 60px; display: flex; align-items: center; justify-content: space-between;">
                          <i class="fa fa-circle fs-20 {!! $section['status'] == '1' ? 'color-success' : 'color-dark' !!}"></i>
                          <div class="btn-group m-0">
                            <button type="button" class="btn btn-raised btn-white no-shadow dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: flex; align-items: center; justify-content: center; border: 1px solid #4ca3d9;">
                              <i class="zmdi zmdi-settings m-0"></i>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a class="dropdown-item" id="EditSection" href="#/" data-id="{{ $section['id'] }}" data-title="{{ $section['title'] }}" data-description="{{ $section['description'] }}" data-status="{{ $section['status'] }}"><i class="fa fa-pencil"></i> @lang('front.page_manage_courses.content_section_action_edit')</a></li>
                              <li><a class="dropdown-item" href="/course/section/delete/{{ $section['id_course'] }}/{{ $section['id'] }}" onclick="return confirm('@lang('front.page_manage_courses.content_section_action_delete_alert')')"><i class="fa fa-trash"></i> @lang('front.page_manage_courses.content_section_action_delete')</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div id="content{{$i}}" class="panel-collapse collapse show">
                        <div class="panel-body" style="background:white;">
                          <div class="mb-2" style="display: flex; align-items: center; justify-content: space-between;">
                            <a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" class="btn btn-primary btn-raised m-0 d-none d-sm-inline">@lang('front.page_manage_courses.content_section_add_contents')</a> <!-- For desktop view -->
                            <a href="#" data-toggle="modal" data-target="#ModalMenuContent{{ $section['id'] }}" class="btn-circle btn-circle-sm btn-circle-primary btn-circle-raised m-0 d-inline d-sm-none"><i class="zmdi zmdi-plus"></i></a> <!-- For mobile view -->
                            <div class="togglebutton">
                              <label>
                                <span class="d-none d-sm-inline">@lang('front.page_manage_courses.content_section_nonactive') </span><input class="sectionActivated" id-section="{{ $section['id'] }}" type="checkbox" {{ $section['status'] == '1' ? 'checked' : '' }}><span class="d-none d-sm-inline"> @lang('front.page_manage_courses.content_section_active') </span>
                              </label>
                            </div>
                          </div>

                          <div class="list-group">
                            <div class="ContentSortable" id="{{ $section['id'] }}">
                              @foreach(collect($section['section_all'])->sortBy('activity_order.order') as $all)
                                @if($all->section_type == 'content')
                                @php
                                  $content = $all;
                                @endphp
                                  <li id="{{$content->id}}" content-type="content" data-section="{{$content->id_section}}">
                                    @php
                                      $content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                    @endphp

                                    <div class="list-group-item list-group-item-action withripple {{$content->status == '0' ? 'disabled' : ''}}">
                                      @if($content->type_content == 'label')
                                        <div class="item-content">
                                          <i class="move fa fa-arrows-alt mr-1"></i>
                                          <div>
                                            {{ $content->title ? $content->title : strip_tags($content->description) }}
                                          </div>
                                        </div>
                                      @else
                                        <a class="item-content" href="/course/preview/content/{{$course->slug}}/{{$content->id}}">
                                          <i class="move fa fa-arrows-alt mr-1"></i>
                                          <div>
                                            <i class="{{content_type_icon($content->type_content)}}"></i> {{ $content->title ? $content->title : strip_tags($content->description) }}
                                            {{ group_has_content('contents', $content->id)['name'] == null ? '' : '<br/><i style="font-size: 12px !important; color: gray">' . Lang::get('front.page_manage_courses.content_section_access_by') . group_has_content('contents', $content->id)['name'] . '</i>' }}
                                          </div>
                                        </a>
                                      @endif

                                      <div style="display: flex; align-items: center;">
                                        @if($content->type_content == 'video')
                                          <div class="duration fs-12 fw-600 color-dark d-none d-sm-inline">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</div>
                                        @endif
                                        @if($course->public == '1')
                                          @if($content->preview == '1')
                                            <i title="Preview" class="fa fa-eye fs-14 ml-1"></i>
                                          @endif
                                        @endif
                                        <i title="{{$content->status == '0' ? Lang::get('front.page_manage_courses.content_section_content_draft') : Lang::get('front.page_manage_courses.content_section_content_publish')}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$content->status == '0' ? '' : 'color-success'}}"></i>
                                        <div class="btn-opsi-preview ml-1">
                                          <button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-v"></i>
                                          </button>
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                            @if($content->type_content == 'discussion')
                                              <li><a class="dropdown-item" href="{{url('course/content/discussion/manage/'.$content->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
                                            @elseif($content->type_content == 'folder')
                                              <li><a class="dropdown-item" href="{{url('course/content/folder/manage/'.$content->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
                                            @endif
                                            <li><a class="dropdown-item status" href="{{url('course/publish_unpublish/'.$content->id)}}"><i class="fa fa-circle {{$content->status == '0' ? '' : 'color-success'}}"></i> @lang('front.page_manage_courses.content_section_content_set_as') {{$content->status == '0' ? 'publish' : 'draft'}}</a></li>
                                            <li class="eyeCheckbox">
                                              <a class="dropdown-item preview_content" value="{{$content->id}}" data-preview="{{ $content->preview }}" id="previewEye{{$content->id}}" href="#"><i class="fa fa-eye"></i> @lang('front.page_manage_courses.content_section_content_set_as') {{ $content->preview == '1' ? 'non-preview' : 'preview' }}</a>
                                              <input type="checkbox" id="preview{{$content->id}}" class="preview_content" value="{{$content->id}}" {{$content->preview == '1' ? 'checked' : ''}}>
                                            </li>
                                            <li>
                                              <a class="dropdown-item" href="{{url('course/content/add/library/'.$content->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> @lang('front.page_manage_courses.content_section_content_add_to_library')</a>
                                            </li>
                                            <li><a class="dropdown-item" href="{{url('course/content/update/'.$content->id. '?content=' . $content->type_content)}}"><i class="fa fa-pencil"></i> @lang('front.page_manage_courses.content_section_content_option_edit')</a></li>
                                            <li><a class="dropdown-item" href="#" onclick="if(confirm('sure?')){ return location.href = '{{url('course/content/delete/'.$course->id.'/'.$content->id)}}' }"><i class="fa fa-trash-o"></i> @lang('front.page_manage_courses.content_section_content_option_delete')</a></li>
                                            @if($content->type_content == 'video' || $content->type_content == 'url-video')
                                              <li><a class="dropdown-item" href="{{url('course/content/video/manage/'.$content->id)}}"><i class="fa fa-cog"></i> @lang('front.page_manage_courses.content_section_content_option_manage_video')</a></li>
                                            @endif
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                    @if(count($content->restrict) > 0)
                                    <div class="restrict">
                                      <h5><b>Restrict</b></h5>
                                      <ul>
                                        @foreach($content->restrict as $restrict)
                                          @php
                                            $restrict_item = null;
                                            if($restrict->to_content){
                                              $restrict_item = $restrict->to_content;
                                              $restrict_item->title = $restrict_item->title;
                                            }elseif($restrict->quizz){
                                              $restrict_item = $restrict->to_quizz;
                                              $restrict_item->title = $restrict_item->name;
                                            }elseif($restrict->assignment){
                                              $restrict_item = $restrict->to_assignment;
                                              $restrict_item->title = $restrict_item->title;
                                            }
                                          @endphp
                                          <li>{{$restrict_item->title}}</li>
                                        @endforeach
                                      </ul>
                                    </div>
                                    @endif
                                  </li>
                                @endif
                                @if($all->section_type == 'quizz')
                                @php $quiz = $all; @endphp
                                <li id="{{$quiz->id}}" content-type="quizz" data-section="{{$quiz->id_section}}">
                                  <div class="list-group-item list-group-item-action withripple {{$quiz->status == '0' ? 'disabled' : ''}}">
                                    <a class="item-content" href="/course/quiz/question/preview/{{$quiz->id}}">
                                      <i class="move fa fa-arrows-alt mr-1"></i>
                                      @if($quiz->quizz_type == 'quiz')
                                      <i class="fa fa-star"></i> {{$quiz->name}}
                                      <i style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('quizzes', $quiz->id)['name'] == null ? '' : Lang::get('front.page_manage_courses.content_section_access_by') . group_has_content('quizzes', $quiz->id)['name'] }}</i>
                                      @else
                                      <i class="fa fa-edit"></i> {{$quiz->name}}
                                      <i style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('quizzes', $quiz->id)['name'] == null ? '' : Lang::get('front.page_manage_courses.content_section_access_by') . group_has_content('quizzes', $quiz->id)['name'] }}</i>
                                      @endif
                                    </a>
                                    <div style="display: flex; align-items: center;">
                                      <i title="{{$quiz->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$quiz->status == '0' ? '' : 'color-success'}}"></i>

                                      <div class="btn-opsi-preview ml-1">
                                        <button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
                                          <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                          <li><a class="dropdown-item" href="{{url('course/'.$quiz->quizz_type.'/question/manage/'.$quiz->id)}}"><i class="fa fa-cog"></i> Manage</a></li>
                                          <li><a class="dropdown-item" href="{{$quiz->status == '1' ? url('course/quiz/unpublish/'.$quiz->id.'/'.$course->id) : url('course/quiz/publish/'.$quiz->id.'/'.$course->id)}}"><i class="fa fa-circle {{$quiz->status == '1' ? 'color-success' : ''}}"></i> @lang('front.page_manage_courses.content_section_content_set_as') {{$quiz->status == '1' ? 'draft' : 'publish'}}</a></li>
                                          <li><a class="dropdown-item" id="previewEye{{$quiz->id}}" href="{{url('course/quiz/add/library/'.$quiz->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> @lang('front.page_manage_courses.content_section_content_add_to_library')</a></li>
                                          <li><a class="dropdown-item" href="{{url('course/quiz/update/'.$quiz->id)}}"><i class="fa fa-pencil"></i> @lang('front.page_manage_courses.content_section_content_option_edit')</a></li>
                                          <li><a class="dropdown-item" href="#" onclick="if(confirm('@lang('front.page_manage_courses.content_section_action_quiz_delete_alert')')) location.href = '{{url('course/quiz/delete/'.$course->id.'/'.$quiz->id)}}'"><i class="fa fa-trash-o"></i> @lang('front.page_manage_courses.content_section_content_option_delete')</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                  @if(count($quiz->restrict) > 0)
                                  <div class="restrict">
                                    <h5><b>Restrict</b></h5>
                                    <ul>
                                      @foreach($quiz->restrict as $restrict)
                                        @php
                                          $restrict_item = null;
                                          if($restrict->to_content){
                                            $restrict_item = $restrict->to_content;
                                            $restrict_item->title = $restrict_item->title;
                                          }elseif($restrict->to_quizz){
                                            $restrict_item = $restrict->to_quizz;
                                            $restrict_item->title = $restrict_item->name;
                                          }elseif($restrict->to_assignment){
                                            $restrict_item = $restrict->to_assignment;
                                            $restrict_item->title = $restrict_item->title;
                                          }
                                        @endphp
                                        <li>{{$restrict_item->title}}</li>
                                      @endforeach
                                    </ul>
                                  </div>
                                  @endif
                                </li>
                                @endif

                                @if($all->section_type == 'assignment')
                                @php $assignment = $all; @endphp
                                <li id="{{$assignment->id}}" content-type="assignment" data-section="{{$assignment->id_section}}">
                                  <div class="list-group-item list-group-item-action withripple {{$assignment->status == '0' ? 'disabled' : ''}}">
                                    <a class="item-content" href="/course/preview/content/{{$course->slug}}/assignment/{{$assignment->id}}">
                                      <i class="move fa fa-arrows-alt mr-1"></i>
                                      <i class="fa fa-tasks"></i> {{$assignment->title}}
                                      <i style="font-size: 12px !important; color: gray; margin-left: 12px !important;">{{ group_has_content('assignments', $assignment->id)['name'] == null ? '' : 'Dapat diakses Oleh: ' . group_has_content('assignments', $assignment->id)['name'] }}</i>
                                    </a>

                                    <div style="display: flex; align-items: center;">
                                      <i title="{{$assignment->status == '0' ? 'Draft' : 'Published'}}" class="status d-none d-sm-inline fa fa-circle fs-20 ml-1 {{$assignment->status == '0' ? '' : 'color-success'}}"></i>

                                      <div class="btn-opsi-preview ml-1">
                                        <button class="dropdown-toggle" type="button" id="" data-toggle="dropdown">
                                          <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="">
                                          <li><a class="dropdown-item" href="{{url('course/assignment/answer/'.$assignment->id)}}"><i class="fa fa-paste"></i> @lang('front.page_manage_courses.content_section_answer_detail_answer')</a></li>
                                          <li><a class="dropdown-item" href="{{$assignment->status == '1' ? url('course/assignment/unpublish/'.$assignment->id) : url('course/assignment/publish/'.$assignment->id)}}"><i class="fa fa-circle {{$assignment->status == '1' ? 'color-success' : ''}}"></i> @lang('front.page_manage_courses.content_section_content_set_as') {{$assignment->status == '1' ? 'draft' : 'publish'}}</a></li>
                                          <li><a class="dropdown-item" id="previewEye{{$assignment->id}}" href="{{url('course/assignment/add/library/'.$assignment->id.'/'.$course->id)}}"><i class="fa fa-plus"></i> @lang('front.page_manage_courses.content_section_content_add_to_library')</a></li>
                                          <li><a class="dropdown-item" href="{{url('course/assignment/update/'.$assignment->id)}}"><i class="fa fa-pencil"></i> @lang('front.page_manage_courses.content_section_content_option_edit')</a></li>
                                          <li><a class="dropdown-item" href="#" onclick="if(confirm('Yakin akan menghapus kuis?')) location.href = '{{url('course/assignment/delete/'.$course->id.'/'.$assignment->id)}}'"><i class="fa fa-trash-o"></i> @lang('front.page_manage_courses.content_section_content_option_delete')</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                  @if(count($assignment->restrict) > 0)
                                  <div class="restrict">
                                    <h5><b>Restrict</b></h5>
                                    <ul>
                                      @foreach($assignment->restrict as $restrict)
                                        @php
                                          $restrict_item = null;
                                          if($restrict->to_content){
                                            $restrict_item = $restrict->to_content;
                                            $restrict_item->title = $restrict_item->title;
                                          }elseif($restrict->to_quizz){
                                            $restrict_item = $restrict->to_quizz;
                                            $restrict_item->title = $restrict_item->name;
                                          }elseif($restrict->to_assignment){
                                            $restrict_item = $restrict->to_assignment;
                                            $restrict_item->title = $restrict_item->title;
                                          }
                                        @endphp
                                        <li>{{$restrict_item->title}}</li>
                                      @endforeach
                                    </ul>
                                  </div>
                                  @endif
                                </li>
                                @endif
                              @endforeach
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php $i++; ?>
                  @endforeach
                </div>

                <div id="place_new_section"></div>
                <input type="hidden" value="0" id="section_row">
                <button type="button" class="btn btn-raised btn-block btn-lg" id="add_new_section">@lang('front.page_manage_courses.content_section_add_new_section')</button>
              </div>
            </div>

            <!-- TAB WEBINAR -->
            <div role="tabpanel" class="tab-pane fade {{is_liveCourse($course->id) == true ? 'active show' : ''}}" id="meeting">
              <a href="#" data-toggle="modal" data-target="#modalWebinarAddSchedule" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.schedule_title') {{is_liveCourse($course->id) == true ? Lang::get('front.page_manage_courses.horizontal_tab_live') : Lang::get('front.page_manage_courses.horizontal_tab_webinar')}}</a>

              <div class="modal fade" id="modalWebinarAddSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="">@lang('front.page_manage_courses.schedule_title') {{is_liveCourse($course->id) == true ? Lang::get('front.page_manage_courses.horizontal_tab_live') : Lang::get('front.page_manage_courses.horizontal_tab_webinar')}}</h4>
                    </div>
                    <form class="" action="/meeting/schedule/store" method="post">
                      {{csrf_field()}}
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_title')</label>
                              <input type="text" name="name" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_description')</label>
                              <textarea name="description" class="form-control" rows="8" cols="80"></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_date')</label>
                              <input type="date" name="date" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_time')</label>
                              <input type="time" name="time" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_recording')</label>
                              <select class="form-control selectpicker" name="recording">
                                <option value="1">@lang('front.page_manage_courses.schedule_new_recording_yes')</option>
                                <option value="0">@lang('front.page_manage_courses.schedule_new_recording_no')</option>
                              </select>
                            </div>
                            <input type="hidden" name="course_id" value="{{$course->id}}">
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.page_manage_courses.button_close')</button>
                        <button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_save')</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($meeting_schedules as $index => $meeting_schedule)
                  <div class="mb-0 card card-default">
                    <div class="card-header" role="tab" id="headingWebinar{{$index}}">
                      <h4 class="card-title">
                        <a class="collapsed dq-none-rotation" role="button" data-toggle="collapse" data-parent="#accordion" href="#webinar{{$index}}" aria-expanded="false" aria-controls="collapse{{$index}}">
                          <span class="dq-section-auto pull-right">
                            <p class="date-time color-info">
                              <span>
                                <i class="fa fa-calendar"></i> {{$meeting_schedule->date}}
                              </span>
                              <span>
                                <i class="fa fa-clock-o"></i> {{$meeting_schedule->time}}
                              </span>
                            </p>
                          </span>
                          <h2 class="headline-sm">{{$meeting_schedule->name}}</h2>
                        </a>
                      </h4>
                    </div>
                    <div id="webinar{{$index}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="headingWebinar{{$index}}">
                      <div class="card-block">
                        <p class="pull-right">
                          <b class="color-dark">@lang('front.page_manage_courses.schedule_new_recording'):</b>
                          &nbsp;
                          {{$meeting_schedule->recording == '1' ? Lang::get('front.page_manage_courses.text_yes') : Lang::get('front.page_manage_courses.text_no')}}
                        <h3 class="headline-sm fs-20 mt-0"><span>@lang('front.page_manage_courses.schedule_new_description')</span></h3>
                        <p>{{$meeting_schedule->description}}</p>
                        <hr>
                        @if($meeting_schedule->date == date("Y-m-d"))
                          <a title="Start Meeting" href="{{url('bigbluebutton/meeting/instructor/join/'.$course->id. '/' . $meeting_schedule->id )}}" class="btn btn-raised btn-success">@lang('front.page_manage_courses.schedule_start')</a>
                        @endif
                        <a id="webinarEditSchedule" data-id="{{$meeting_schedule->id}}" data-name="{{$meeting_schedule->name}}" data-description="{{$meeting_schedule->description}}" data-date="{{$meeting_schedule->date}}" data-time="{{$meeting_schedule->time}}" data-recording="{{$meeting_schedule->recording}}" href="#/" class="btn btn-raised btn-warning">@lang('front.page_manage_courses.text_edit')</a>
                        <a onclick="return confirm('Yakin?')" class="btn btn-raised btn-danger" href="/meeting/schedule/delete/{{$meeting_schedule->id}}">@lang('front.page_manage_courses.text_delete')</a>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>

              <div class="modal fade" id="modalWebinarEditSchedule" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="">@lang('front.page_manage_courses.schedule_webinar_title_edit')</h4>
                    </div>
                    <form class="" action="/meeting/schedule/update" method="post">
                      {{csrf_field()}}
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_title')</label>
                              <input type="text" name="name" class="form-control" id="webinarName" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_description')</label>
                              <textarea name="description" class="form-control" id="webinarDescription" rows="8" cols="80"></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_date')</label>
                              <input type="date" name="date" id="webinarDate" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_time')</label>
                              <input type="time" name="time" class="form-control" id="webinarTime" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">@lang('front.page_manage_courses.schedule_new_recording')</label>
                              <select class="form-control selectpicker" id="webinarRecording" name="recording">
                                <option value="1">@lang('front.page_manage_courses.schedule_new_recording_yes')</option>
                                <option value="0">@lang('front.page_manage_courses.schedule_new_recording_no')</option>
                              </select>
                            </div>
                            <input type="hidden" name="course_id" value="{{$course->id}}">
                            <input type="hidden" name="id" id="webinarId" value="{{$course->id}}">
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">@lang('front.page_manage_courses.button_close')</button>
                        <button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_save')</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>

            <!-- TAB Q&A -->
            <div role="tabpanel" class="tab-pane fade" id="QandA">
              <div class="card">
                <table class="table dq-table">
                  <tr>
                    <td class="col-md-4 dq-no-margin">
                      {{-- <input type="text" class="form-control" name="" placeholder="Search questions"> --}}
                    </td>
                    <td></td>
                    <td><a href="#" data-toggle="modal" data-target="#modalAskQuestion" class="btn btn-raised btn-lg btn-primary">@lang('front.page_manage_courses.qna_add_question_title')</a></td>
                  </tr>
                </table>
              </div>
              <div class="row">
              </div>
              <div class="card">
                <ul class="list-group">
                  @foreach($discussions as $discussion)
                    <a href="/discussion/detail/{{$discussion->id}}">
                      <li class="list-group-item card-block">
                        <span class="text-center dq-pl">
                          <img src="{{avatar($discussion->photo)}}" class="dq-frame-round-xs" alt="{{$discussion->name}}">
                        </span>
                        <span class="col-lg-10 dq-max">
                          <b>{{$discussion->name}}</b>
                          <p>{{$discussion->body}}</p>
                        </span>
                        <span class="text-center dq-pl dq-pr">
                          <b>{{(DB::table('discussions_answers')->where('discussion_id', $discussion->id))->count()}}</b>
                          <p>responses</p>
                        </span>
                      </li>
                    </a>
                  @endforeach
                </ul>
              </div>
              <div class="text-center">
                {{-- <a href="" class="btn btn-raised btn-info">
                  Load More
                </a> --}}
              </div>
            </div>

            <!-- TAB ANNOUNCEMENTS -->
            <div role="tabpanel" class="tab-pane fade" id="announcements">
              <a title="Pengumuman" href="{{url('course/announcement/create/'.$course->id)}}" class="btn btn-primary btn-raised">
                @lang('front.page_manage_courses.announcement_add_title')
              </a>
              @foreach($announcements as $announcement)
                <div class="card">
                  <div class="card-block-big">
                    <p>
                      <div class="row">
                        <img src="{{avatar($announcement['photo'])}}" class="dq-image-user" style="margin-left:15px">
                        <div class="col-md-8">
                          <div>
                            {{$announcement['name']}}
                          </div>
                          <div>
                            <span>{{$announcement['created_at']}}</span>
                          </div>
                        </div>
                      </div>
                    </p>
                    <h3>{{$announcement['title']}}</h3>
                    <p>
                      {!!$announcement['description']!!}
                    </p>
                    <form name="delete" action="/course/announcement/delete/{{$announcement['id']}}" method="post">
                      {{csrf_field()}}
                      <input type="hidden" name="_method" value="DELETE">
                      <button type="submit" class="btn btn-danger btn-raised btn-sm" onclick="return confirm('@lang('front.page_manage_courses.announcement_delete_alert')')">@lang('front.page_manage_courses.text_delete')</button>
                      <a class="btn btn-warning btn-raised btn-sm" href="/course/announcement/edit/{{$announcement['id']}}">Edit</a>
                    </form>
                    <!--FLOATING FORM-->
                    <p>
                      <form class="form-group label-floating"  method="post" action="/course/announcement/comment">
                        {{csrf_field()}}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <img src="{{avatar(Auth::user()->photo)}}" class="dq-image-user dq-ml">
                          </span>
                          <label class="control-label" for="addon2">@lang('front.page_manage_courses.announcement_enter_your_comment')</label>
                          <input type="text" id="addon2" class="form-control" name="comment">
                          <input type="hidden" name="course_announcement_id" value="{{$announcement['id']}}">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-raised btn-primary">
                              @lang('front.page_manage_courses.announcement_submit')
                            </button>
                          </span>
                        </div>
                      </form>
                    </p>

                    <p>
                      <div class="ms-collapse no-margin" id="accordion{{$announcement['id']}}" role="tablist" aria-multiselectable="true">
                        <div class="mb-0">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{$announcement['id']}}" href="#collapse{{$announcement['id']}}" aria-expanded="false" aria-controls="collapse{{$announcement['id']}}">
                            @lang('front.page_manage_courses.announcement_comments')({{count($announcement['announcement_comments'])}})
                          </a>
                          <div id="collapse{{$announcement['id']}}" class="card-collapse collapse" role="tabpanel">
                            @foreach($announcement['announcement_comments'] as $announcement_comment)
                            <div class="dq-padding-comment">
                              <span>
                                <div class="row">
                                  <img src="{{avatar($announcement_comment->photo)}}" class="dq-image-user dq-ml">
                                  <div class="col-md-8">
                                    <div>
                                      <span>
                                        {{$announcement_comment->name}}
                                      </span>
                                      .
                                      <span>{{$announcement_comment->created_at}}</span>
                                      {{-- .
                                      <span>
                                        <a href="" data-toggle="tooltip" data-placement="top" title="Report Abuse">
                                          <i class="fa fa-flag"></i>
                                        </a>
                                      </span> --}}
                                    </div>
                                    <div>
                                      <span>
                                        {{$announcement_comment->comment}}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </span>
                            </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </p>
                  </div>
                </div>
              @endforeach
            </div>

            @if(isManager(Auth::user()->id) === true)
              <div role="tabpanel" class="tab-pane fade" id="settings">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-block">
                        <div class="row">
                          <div class="col-md-6">
                            <h3>Pengajar</h3>

                            <div class="row">
                              <div class="col-md-8">
                                <div class="form-group">
                                  <label for="name">Tambah Pengajar</label>
                                  <select class="form-control" name="id_author[]" id="id_author" multiple style="width:100%">
                                    @foreach($authors as $author)
                                      <option value="{{$author->id}}">{{$author->name}} - {{$author->email}}</option>
                                    @endforeach()
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <button style="margin-top: 60px;" id="saveAuthor" type="button" class="btn btn-primary btn-raised" name="button">Simpan</button>
                              </div>
                            </div>

                            <table class="table table-bordered">
                              <tr>
                                <th>No</th>
                                <th>Nama Pengajar</th>
                              </tr>
                              @php
                                $course_authors = DB::table('users')
                                  ->select('name')
                                  ->whereIn('id', explode(',', $course->id_author))
                                  ->get();
                              @endphp
                              @foreach($course_authors as $index =>  $course_author)
                                <tr>
                                  <td>{{$index+1}}</td>
                                  <td>{{$course_author->name}}</td>
                                </tr>
                              @endforeach
                            </table>
                          </div>

                          <div class="col-md-6">
                            <h3>Group</h3>

                            <div class="row">
                              <div class="col-md-8">
                                <div class="form-group">
                                  <label for="name">Share dengan Group</label>
                                  <select class="form-control" name="id_instructor_group[]" id="id_instructor_group" multiple style="width:100%">
                                    @foreach($instructor_groups as $instructor_group)
                                      <option value="{{$instructor_group->id}}">{{$instructor_group->title}}</option>
                                    @endforeach()
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <button style="margin-top: 60px;" id="saveInstructorGroup" type="button" class="btn btn-primary btn-raised" name="button">Simpan</button>
                              </div>
                            </div>

                            <table class="table table-bordered">
                              <tr>
                                <th>No</th>
                                <th>Nama Pengajar</th>
                              </tr>
                              @php
                                $course_instructor_groups = DB::table('instructor_groups')
                                  ->select('title')
                                  ->whereIn("id", explode(',', $course->id_instructor_group))
                                  ->get();
                              @endphp
                              @foreach($course_instructor_groups as $index =>  $course_instructor_group)
                                <tr>
                                  <td>{{$index+1}}</td>
                                  <td>{{$course_instructor_group->title}}</td>
                                </tr>
                              @endforeach
                            </table>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modalAskQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="{{url('discussion/ask')}}" method="post">
      {{csrf_field()}}
      <input type="hidden" name="course_id" value="{{$course->id}}">
      <div class="modal-dialog animated zoomIn animated-3x modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">@lang('front.learn_course.discussion_create_header')</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label class="control-label">@lang('front.learn_course.discussion_question_label')</label>
                    <input name="body" placeholder="@lang('front.learn_course.discussion_enter_question_label')" class="form-control">
                  </div>
                  <div class="form-group mt-0">
                    <label class="control-label">@lang('front.learn_course.discussion_description_label')</label>
                    <textarea name="deskripsi" id="answer" cols="30" rows="1" class="form-control"></textarea>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('front.general.text_cancel')</button>
                  <input type="submit" id="ask_discussion" class="btn btn-primary btn-raised" name="submit" value="@lang('front.general.text_submit')">
              </div>
          </div>
      </div>
    </form>
  </div>

  @foreach($sections as $section)
    <div class="modal" id="ModalMenuContent{{ $section['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="myModalLabel">@lang('front.page_manage_courses.add_new_content_title')</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
          </div>
          <div class="modal-body preview-modal-tab">
            <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'Teks{{ $section['id'] }}')"><i class="fa fa-text-height"></i> @lang('front.page_manage_courses.add_new_content_type_text')</button>
              <button class="tablinks" onclick="openCity(event, 'Video{{ $section['id'] }}')" id="defaultOpen"><i class="fa fa-video-camera"></i> @lang('front.page_manage_courses.add_new_content_type_video')</button>
              <button class="tablinks" onclick="openCity(event, 'Scorm{{ $section['id'] }}')"><i class="fa fa-file"></i> @lang('front.page_manage_courses.add_new_content_type_scorm')</button>
              <button class="tablinks" onclick="openCity(event, 'File{{ $section['id'] }}')"><i class="fa fa-file"></i> @lang('front.page_manage_courses.add_new_content_type_file')</button>
              <button class="tablinks" onclick="openCity(event, 'Folder{{ $section['id'] }}')"><i class="fa fa-folder"></i> @lang('front.page_manage_courses.add_new_content_type_folder')</button>
              <div class="spacer"></div>
              <button class="tablinks" onclick="openCity(event, 'Diskusi{{ $section['id'] }}')"><i class="fa fa-wechat"></i> @lang('front.page_manage_courses.add_new_content_type_discuss')</button>
              <button class="tablinks" onclick="openCity(event, 'Tugas{{ $section['id'] }}')"><i class="fa fa-tasks"></i> @lang('front.page_manage_courses.add_new_content_type_assignment')</button>
              <button class="tablinks" onclick="openCity(event, 'Survey{{ $section['id'] }}')"><i class="fa fa-edit"></i> @lang('front.page_manage_courses.add_new_content_type_surver')</button>
              <button class="tablinks" onclick="openCity(event, 'Kuis{{ $section['id'] }}')"><i class="fa fa-question"></i> @lang('front.page_manage_courses.add_new_content_type_quiz')</button>
              <div class="spacer"></div>
              <button class="tablinks" onclick="openCity(event, 'Label{{ $section['id'] }}')"><i class="fa fa-tags"></i> @lang('front.page_manage_courses.add_new_content_type_label')</button>
              <button class="tablinks" onclick="openCity(event, 'Library{{ $section['id'] }}')"><i class="fa fa-archive"></i> @lang('front.page_manage_courses.add_new_content_type_library')</button>
            </div>

            <div id="Teks{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_text_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_text_description')</p>
              <a href="/course/content/create/{{ $section['id'] }}?content=text" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_text_title')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=text" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <br>
            </div>

            <div id="Video{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_video_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_video_description')</p>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=video" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <a href="/course/content/create/{{ $section['id'] }}?content=video" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_video_upload')</a>
              <a href="/course/content/create/{{ $section['id'] }}?content=url&urlType=video" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_video_url')</a>
              <br>
            </div>

            <div id="Scorm{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_scorm_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_scorm_description')</p>
              <a href="/course/content/create/scorm/{{ $section['id'] }}" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_scorm_upload')</a>
              <br>
            </div>

            <div id="File{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_file_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_file_description')</p>
              <a href="/course/content/create/{{ $section['id'] }}?content=file" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_file_upload')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=file" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <a href="/course/content/create/{{ $section['id'] }}?content=url&urlType=file" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_file_url')</a>
              <br>
            </div>

            <div id="Diskusi{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_discuss_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_discuss_description')</p>
              <a href="/course/content/create/{{ $section['id'] }}?content=discussion" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_discuss_title')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=text" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <br>
            </div>

            <div id="Tugas{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_assignment_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_assignment_description')</p>
              <a href="/course/assignment/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_assignment_title')</a>
              <br>
            </div>

            <div id="Kuis{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_quiz_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_quiz_description')</p>
              <a href="/course/quiz/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_quiz_title')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=quiz" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <br>
            </div>

            <div id="Survey{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_survey_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_survey_description')</p>
              <a href="/course/survey/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_survey_title')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=quiz" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <br>
            </div>

            <div id="Label{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_label_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_label_description')</p>
              <a href="/course/content/create/{{ $section['id'] }}?content=label" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_label_title')</a>
              <br>
            </div>

            <div id="Library{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_library_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_library_description')</p>
              <a href="#" data-toggle="modal" data-target="#importLibrary{{ $section['id'] }}" data-dismiss="modal" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_library_import')</a>
            </div>

            <div id="Folder{{ $section['id'] }}" class="tabcontent">
              <h3>@lang('front.page_manage_courses.add_new_content_folder_title')</h3>
              <p>@lang('front.page_manage_courses.add_new_content_folder_description')</p>
              <a href="/course/content/create/{{ $section['id'] }}?content=folder" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.add_new_content_folder_title')</a>
              {{-- <a href="/course/content/library/{{ $section['id'] }}?content=file" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
              <br>
            </div>
          </div>

          {{-- <div id="Tugas{{ $section['id'] }}" class="tabcontent">
            <h3>Tambah Tugas</h3>
            <p>
              Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.
            </p>
            <a href="/course/assignment/create/{{ $section['id'] }}" class="btn btn-primary btn-raised">Tambah Tugas</a>
          </div>

          <div id="Library{{ $section['id'] }}" class="tabcontent">
            <h3>Tambah Content Library</h3>
            <p>
              Anda dapat menambhakan kontent yang sudah tersedia di library.
            </p>
            <a href="{{url('course/content/library/add/'.$course->id.'/'.$section['id'])}}" class="btn btn-primary btn-raised">Tambah Konten dari Library</a>
          </div> --}}
        </div>
      </div>
    </div>

    <div class="modal importLibraryModal" id="importLibrary{{ $section['id'] }}" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="nav-tabs-ver-container">
              <div class="row">
                <div class="col-lg-2" style="    flex: 0 0 12.666667%;max-width: 12.666667%;">
                  <ul class="nav nav-tabs-ver nav-tabs-ver-primary" role="tablist" style="height:100%">
                    <li class="nav-item">
                      <a class="nav-link active" style="text-align:center" href="#library-tab" aria-controls="home3" role="tab" data-toggle="tab">
                        <i class="fa fa-building" style="margin:0"></i><br>Library
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#group-tab" style="text-align:center"  aria-controls="profile3" role="tab" data-toggle="tab">
                        <i class="fa fa-users" style="margin:0"></i><br>Group
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#course-tab" style="text-align:center"  aria-controls="profile3" role="tab" data-toggle="tab">
                        <i class="fa fa-archive" style="margin:0"></i><br>Course
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-10 nav-tabs-ver-container-content" style="flex: 0 0 87.333333%;max-width: 87.333333%;padding-right: 0px;">
                  <div class="card-block">
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="library-tab">
                        <div class="nav-tabs-ver-container">
                          <div class="row">
                            <div class="col-lg-3" style="flex: 0 0 30%;max-width: 30%;padding-left:0">
                              <ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#my-library-tab" aria-controls="my-library-tab" role="tab" data-toggle="tab">
                                    <i class="fa fa-building"></i> My Library
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#shared-library-tab" aria-controls="shared-library-tab" role="tab" data-toggle="tab">
                                    <i class="fa fa-share-alt"></i> Shared Library
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#public-library-tab" aria-controls="profile3" role="tab" data-toggle="tab">
                                    <i class="fa fa-users"></i> Public Library
                                  </a>
                                </li>
                              </ul>
                            </div>
                            <div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%;max-width: 70%;">
                              <div class="card-block">
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="my-library-tab">
                                    <div class="content-full-search">
                                      <input type="text" class="form-control search-content" placeholder="Search Content" style="margin: 11px 0 0;">
                                      <form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
                                        {{csrf_field()}}
                                        <div class="table-responsive table-cs">
                                          <table class="display" style="width:100%">
                                            <thead>
                                              <tr>
                                                <th></th>
                                                <th>Nama</th>
                                                <th class="text-center">Tipe</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @foreach($folder->where('user_id', Auth::user()->id) as $item)
                                              <tr>
                                                <td class="text-center"></td>
                                                <td>
                                                  <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                                    <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                                  </a>
                                                  <div class="collapse" id="collapseExample-{{$item->id}}">
                                                    <div class="card card-body">
                                                      <table>
                                                        <thead>
                                                          <tr>
                                                            <th></th>
                                                            <th>Nama</th>
                                                            <th class="text-center">Tipe</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          @foreach($item->contents as $item_folder)
                                                            <?php
                                                              $data = dataContent($item_folder->library);
                                                              $icon = content_type_icon($data->type_content);
                                                              $type = 'Quiz';
                                                              if($data->type_content){
                                                                $type = $data->type_content;
                                                              }else if($data->type == "0"){
                                                                $type = 'Tugas';
                                                                $icon = "fa fa-tasks";
                                                              }else {
                                                                if($data->quizz_type){
                                                                  if($data->quizz_type == 'survey'){
                                                                    $icon = 'fa fa-edit';
                                                                  }
                                                                }
                                                              }
                                                            ?>
                                                            <tr>
                                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                              <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                              <td class="text-center">{{$type}}</td>
                                                            </tr>
                                                          @endforeach
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </td>
                                                <td class="text-center">Folder</td>
                                              </tr>
                                              @endforeach
                                              @foreach($myLibrary[0]->library as $item)
                                                <?php
                                                  $data = dataContent($item);
                                                  $icon = content_type_icon($data->type_content);
                                                  $type = 'Quiz';
                                                  if($data->type_content){
                                                    $type = $data->type_content;
                                                  }else if($data->type == "0"){
                                                    $type = 'Tugas';
                                                    $icon = "fa fa-tasks";
                                                  }else {
                                                    if($data->quizz_type){
                                                      if($data->quizz_type == 'survey'){
                                                        $icon = 'fa fa-edit';
                                                      }
                                                    }
                                                  }
                                                ?>
                                                <tr>
                                                  <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                  <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                  <td class="text-center">{{$type}}</td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                                        </div>

                                        <div class="modal-footer" style="padding-right: 0;">
                                          <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                          <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                  <div role="tabpanel" class="tab-pane" id="shared-library-tab">
                                    <div class="content-full-search">
                                      <input type="text" class="form-control search-content" placeholder="Search Content" style="margin: 11px 0 0;">
                                      <form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
                                        {{csrf_field()}}
                                        <div class="table-responsive table-cs">
                                          <table class="display" style="width:100%">
                                            <thead>
                                              <tr>
                                                <th></th>
                                                <th>Nama</th>
                                                <th class="text-center">Tipe</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @foreach($folder->where('user_id', '!=', Auth::user()->id) as $item)
                                              <tr>
                                                <td class="text-center"></td>
                                                <td>
                                                  <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExampleShared-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                                    <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                                  </a>
                                                  <div class="collapse" id="collapseExampleShared-{{$item->id}}">
                                                    <div class="card card-body">
                                                      <table>
                                                        <thead>
                                                          <tr>
                                                            <th></th>
                                                            <th>Nama</th>
                                                            <th class="text-center">Tipe</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          @foreach($item->contents as $item_folder)
                                                            <?php
                                                              $data = dataContent($item_folder->library);
                                                              $icon = content_type_icon($data->type_content);
                                                              $type = 'Quiz';
                                                              if($data->type_content){
                                                                $type = $data->type_content;
                                                              }else if($data->type == "0"){
                                                                $type = 'Tugas';
                                                                $icon = "fa fa-tasks";
                                                              }else {
                                                                if($data->quizz_type){
                                                                  if($data->quizz_type == 'survey'){
                                                                    $icon = 'fa fa-edit';
                                                                  }
                                                                }
                                                              }
                                                            ?>
                                                            <tr>
                                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                              <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                              <td class="text-center">{{$type}}</td>
                                                            </tr>
                                                          @endforeach
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </td>
                                                <td class="text-center">Folder</td>
                                              </tr>
                                              @endforeach
                                              @foreach($myLibrary[2]->library as $item)
                                                <?php
                                                  $data = dataContent($item);
                                                  $icon = content_type_icon($data->type_content);
                                                  $type = 'Quiz';
                                                  if($data->type_content){
                                                    $type = $data->type_content;
                                                  }else if($data->type == "0"){
                                                    $type = 'Tugas';
                                                    $icon = "fa fa-tasks";
                                                  }else {
                                                    if($data->quizz_type){
                                                      if($data->quizz_type == 'survey'){
                                                        $icon = 'fa fa-edit';
                                                      }
                                                    }
                                                  }
                                                ?>
                                                <tr>
                                                  <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                  <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                  <td class="text-center">{{$type}}</td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                                        </div>

                                        <div class="modal-footer" style="padding-right: 0;">
                                          <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                          <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                  <div role="tabpanel" class="tab-pane" id="public-library-tab">
                                    <div class="content-full-search">
                                      <input type="text" class="form-control search-content" placeholder="Search Content" style="margin: 11px 0 0;">
                                      <form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
                                        {{csrf_field()}}
                                        <div class="table-responsive table-cs">
                                          <table class="display" style="width:100%">
                                            <thead>
                                              <tr>
                                                <th></th>
                                                <th>Nama</th>
                                                <th class="text-center">Tipe</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @foreach($folder->where('status', '1') as $item)
                                                <tr>
                                                  <td class="text-center"></td>
                                                  <td>
                                                    <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExamplePublic-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                                      <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                                    </a>
                                                    <div class="collapse" id="collapseExamplePublic-{{$item->id}}">
                                                      <div class="card card-body">
                                                        <table>
                                                          <thead>
                                                            <tr>
                                                              <th></th>
                                                              <th>Nama</th>
                                                              <th class="text-center">Tipe</th>
                                                            </tr>
                                                          </thead>
                                                          <tbody>
                                                            @foreach($item->contents as $item_folder)
                                                            <?php
                                                              $data = dataContent($item_folder->library);
                                                              $icon = content_type_icon($data->type_content);
                                                              $type = 'Quiz';
                                                              if($data->type_content){
                                                                $type = $data->type_content;
                                                              }else if($data->type == "0"){
                                                                $type = 'Tugas';
                                                                $icon = "fa fa-tasks";
                                                              }else {
                                                                if($data->quizz_type){
                                                                  if($data->quizz_type == 'survey'){
                                                                    $icon = 'fa fa-edit';
                                                                  }
                                                                }
                                                              }
                                                            ?>
                                                            <tr>
                                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                              <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                              <td class="text-center">{{$type}}</td>
                                                            </tr>
                                                            @endforeach
                                                          </tbody>
                                                        </table>
                                                      </div>
                                                    </div>
                                                  </td>
                                                  <td class="text-center">Folder</td>
                                                </tr>
                                              @endforeach

                                              @foreach($publicLibrary->library as $item)
                                                <?php
                                                  $data = dataContent($item);
                                                  $icon = content_type_icon($data->type_content);
                                                  $type = 'Quiz';
                                                  if($data->type_content){
                                                    $type = $data->type_content;
                                                  }else if($data->type == "0"){
                                                    $type = 'Tugas';
                                                    $icon = "fa fa-tasks";
                                                  }else {
                                                    if($data->quizz_type){
                                                      if($data->quizz_type == 'survey'){
                                                        $icon = 'fa fa-edit';
                                                      }
                                                    }
                                                  }
                                                ?>
                                                <tr>
                                                  <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                  <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                  <td class="text-center">{{$type}}</td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                                        </div>

                                        <div class="modal-footer" style="padding-right: 0;">
                                          <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                          <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="group-tab">
                        <div class="nav-tabs-ver-container">
                          <div class="row">
                            <div class="col-lg-3" style="flex: 0 0 30%;max-width: 30%;padding-left:0">
                              <ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%">
                                @foreach($group as $index => $item)
                                  <li class="nav-item">
                                    <a class="nav-link {{$index == 0 ? 'active' : ''}}" href="#my-library-tab-{{$item->group_code}}" aria-controls="my-library-tab-{{$item->group_code}}" role="tab" data-toggle="tab">
                                      <i class="fa fa-building"></i> {{$item->title}}
                                    </a>
                                  </li>
                                @endforeach
                              </ul>
                            </div>
                            <div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%;max-width: 70%;">
                              <div class="card-block">
                                <div class="tab-content">
                                  @foreach($group as $index => $item)
                                    <div role="tabpanel" class="tab-pane {{$index == 0 ? 'active' : ''}}" id="my-library-tab-{{$item->group_code}}">
                                      <div class="content-full-search">
                                        <input type="text" class="form-control search-content" placeholder="Search Content" style="margin: 11px 0 0;">
                                        <form action="{{url('course/content/library/add/'.$course->id.'/'.$sections[0]['id'])}}" method="post">
                                          {{csrf_field()}}
                                          <div class="table-responsive table-cs">
                                            <table class="display" style="width:100%">
                                              <thead>
                                                <tr>
                                                  <th></th>
                                                  <th>Nama</th>
                                                  <th class="text-center">Tipe</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($item->library as $content)
                                                  @if($content->folder)
                                                    @if($content->folder->question_bank == '0')
                                                    <tr>
                                                      <td class="text-center"></td>
                                                      <td>
                                                        <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-Group-{{$content->folder->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-Group-{{$content->folder->id}}">
                                                          <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$content->folder->group_name}}
                                                        </a>
                                                        <div class="collapse" id="collapseExample-Group-{{$content->folder->id}}">
                                                          <div class="card card-body">
                                                            <table>
                                                              <thead>
                                                                <tr>
                                                                  <th></th>
                                                                  <th>Nama</th>
                                                                  <th class="text-center">Tipe</th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>
                                                                @foreach($content->folder->contents as $item_folder)
                                                                  <?php
                                                                    $data = dataContent($item_folder->library);
                                                                    $icon = content_type_icon($data->type_content);
                                                                    $type = 'Quiz';
                                                                    if($data->type_content){
                                                                      $type = $data->type_content;
                                                                    }else if($data->type == "0"){
                                                                      $type = 'Tugas';
                                                                      $icon = "fa fa-tasks";
                                                                    }else {
                                                                      if($data->quizz_type){
                                                                        if($data->quizz_type == 'survey'){
                                                                          $icon = 'fa fa-edit';
                                                                        }
                                                                      }
                                                                    }
                                                                  ?>
                                                                  <tr>
                                                                    <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                                    <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                                    <td class="text-center">{{$type}}</td>
                                                                  </tr>
                                                                @endforeach
                                                              </tbody>
                                                            </table>
                                                          </div>
                                                        </div>
                                                      </td>
                                                      <td class="text-center">Folder</td>
                                                    </tr>
                                                    @endif
                                                  @endif
                                                @endforeach

                                                @foreach($item->library as $content)
                                                  @if($content->content)
                                                    <?php
                                                      $data = dataContent($content->content);
                                                      $icon = content_type_icon($data->type_content);
                                                      $type = 'Quiz';
                                                      if($data->type_content){
                                                        $type = $data->type_content;
                                                      }else if($data->type == "0"){
                                                        $type = 'Tugas';
                                                        $icon = "fa fa-tasks";
                                                      }else {
                                                        if($data->quizz_type){
                                                          if($data->quizz_type == 'survey'){
                                                            $icon = 'fa fa-edit';
                                                          }
                                                        }
                                                      }
                                                    ?>
                                                    <tr>
                                                      <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/{{$type}}"></td>
                                                      <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                      <td class="text-center">{{$type}}</td>
                                                    </tr>
                                                  @endif
                                                @endforeach
                                              </tbody>
                                            </table>
                                          </div>

                                          <div class="modal-footer" style="padding-right: 0;">
                                            <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                            <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="course-tab">
                        <div class="nav-tabs-ver-container">
                          <div class="row">
                            <div class="col-lg-3" style="flex: 0 0 30%;max-width: 30%;padding-left:0">
                              <ul class="nav nav-tabs-ver nav-tabs-ver course-library" role="tablist" style="height:100%">
                                <li class="nav-item">
                                  <input type="text" class="form-control search-course" placeholder="search course">
                                </li>
                                @foreach($course_all as $index => $item)
                                  <li class="nav-item">
                                    <a class="nav-link {{$index == 0 ? 'active' : ''}}" href="#my-library-course-tab-{{$item->id}}" aria-controls="my-library-course-tab-{{$item->id}}" role="tab" data-toggle="tab">
                                      <i class="fa fa-building"></i> {{$item->title}}
                                    </a>
                                  </li>
                                @endforeach
                              </ul>
                            </div>
                            <div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%;max-width: 70%;">
                              <div class="card-block">
                                <div class="tab-content">
                                  @foreach($course_all as $index => $item)
                                    <div role="tabpanel" class="tab-pane {{$index == 0 ? 'active' : ''}}" id="my-library-course-tab-{{$item->id}}">
                                      <div class="content-full-search">
                                        <input type="text" class="form-control search-content" placeholder="Search Content" style="margin: 11px 0 0;">
                                        <form action="{{url('course/content/library/course/add/'.$course->id.'/'.$sections[0]['id'])}}" id="form-library-course-tab-{{$item->id}}" method="post">
                                          {{csrf_field()}}
                                          <div class="table-responsive table-cs">
                                            <table class="display" style="width:100%">
                                              <thead>
                                                <tr>
                                                  <th></th>
                                                  <th>Nama</th>
                                                  <th class="text-center">Tipe</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($item->sections as $section_course)
                                                  @php
                                                    $content_section = $section_course->contents;
                                                    $quiz_section = $section_course->quizzes;
                                                    $assignment_section = $section_course->assignments;
                                                  @endphp
                                                  @foreach($content_section as $content_sections)
                                                    @php
                                                      $data = $content_sections;
                                                      $icon = content_type_icon($data->type_content);
                                                    @endphp
                                                    <tr>
                                                      <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/content"></td>
                                                      <td><p class="library-list"><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                      <td class="text-center">{{$data->type_content}}</td>
                                                    </tr>
                                                  @endforeach
                                                  @foreach($quiz_section as $content_sections)
                                                    @php
                                                      $data = $content_sections;
                                                      $icon = content_type_icon($data->type_content);
                                                    @endphp
                                                    <tr>
                                                      <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/quiz"></td>
                                                      <td><p class="library-list"><i class="fa fa-{{$data->quizz_type == 'quiz' ? 'star' : 'edit'}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                      <td class="text-center">{{$data->quizz_type == 'quiz' ? 'Quiz' : 'Survey'}}</td>
                                                    </tr>
                                                  @endforeach
                                                  @foreach($assignment_section as $content_sections)
                                                    @php
                                                      $data = $content_sections;
                                                      $icon = content_type_icon($data->type_content);
                                                    @endphp
                                                    <tr>
                                                      <td class="text-center"><input type="checkbox" name="content[]" value="{{$data->id}}/assignment"></td>
                                                      <td><p class="library-list"><i class="fa fa-tasks mr-l"></i> {{$data->title ? $data->title : $data->name}}</p></td>
                                                      <td class="text-center">Tugas</td>
                                                    </tr>
                                                  @endforeach
                                                @endforeach
                                              </tbody>
                                            </table>
                                          </div>

                                          <div class="modal-footer" style="padding-right: 0;">
                                            <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                            <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

  <div class="modal" id="modalEditGetCeritificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-header">
          <h4 class="modal-title">@lang('front.page_manage_courses.ceritificate_settings')</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-content" style="padding: 20px;">
          <form action="/course/set-ceritifcated" method="POST">
            {{ csrf_field() }}
              <label for="">@lang('front.page_manage_courses.ceritificate_get_with')</label>
              <div class="demo-radio-button">
                  <input name="get_ceritificate" type="radio" id="radio_1" value="0" {{ $course->get_ceritificate == '0' ? 'checked' : '' }} />
                  <label for="radio_1">@lang('front.page_manage_courses.overview_course_no_ceritificate')</label>
                  <input name="get_ceritificate" type="radio" id="radio_4" value="3" {{ $course->get_ceritificate == '3' ? 'checked' : '' }} />
                  <label for="radio_4">@lang('front.page_manage_courses.overview_course_get_ceritificate_manual')</label>
                  <input name="get_ceritificate" type="radio" id="radio_2" value="1" {{ $course->get_ceritificate == '1' ? 'checked' : '' }} />
                  <label for="radio_2">@lang('front.page_manage_courses.overview_course_get_ceritificate_percentage')</label>
                  <input name="get_ceritificate" type="radio" id="radio_3" value="2" {{ $course->get_ceritificate == '2' ? 'checked' : '' }} />
                  <label for="radio_3">@lang('front.page_manage_courses.overview_course_get_ceritificate_gradebook')</label>
              </div>
              <input type="hidden" name="id" value="{{ $course->id }}">
              <button type="submit" class="btn btn-primary btn-raised">@lang('front.page_manage_courses.button_save')</button>
          </form>
        </div>
    </div>
  </div>

  <div class="modal" id="modalSetEditorDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <textarea name="editorDescription" id="editorDescription"></textarea>
        <input type="button" id="saveEditorDescription" value="@lang('front.page_manage_courses.button_save')" class="btn btn-primary btn-raised">
      </div>
    </div>
  </div>

  <div class="modal" id="modalSetEditorGoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <textarea name="editorGoal" id="editorGoal"></textarea>
        <input type="button" id="saveEditorGoal" value="@lang('front.page_manage_courses.button_save')" class="btn btn-primary btn-raised">
      </div>
    </div>
  </div>

  <div class="modal" id="modalSetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <img class="img-fluid" src="{{$course->image}}" alt="{{$course->title}}" style="margin-bottom: 2rem;">
        <input type="file" id="editorImage" value="">
        <input type="button" id="saveEditorImage" value="@lang('front.page_manage_courses.button_save')" class="btn btn-primary btn-raised">
      </div>
    </div>
  </div>

  <div class="modal" id="modalEditSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">@lang('front.page_manage_courses.section_edit_title')</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label for="">@lang('front.page_manage_courses.section_title')</label>
              <input type="text" class="form-control" id="setSectionTitle">
            </div>

            <div class="form-group">
              <label for="">@lang('front.page_manage_courses.section_description')</label>
              <textarea id="setSectionDescription" class="form-control" rows="6"></textarea>
            </div>

            <div class="form-group">
              <label for="">@lang('front.page_manage_courses.section_status')</label>
              <select class="form-control selectpicker" id="setSectionStatus">
                <option value="1">@lang('front.page_manage_courses.section_status_active')</option>
                <option value="0">@lang('front.page_manage_courses.section_nonstatus')</option>
              </select>
            </div>

            <input type="hidden" id="setSectionId">
            <input type="button" id="saveEditSection" value="@lang('front.page_manage_courses.button_save')" class="btn btn-primary btn-raised">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modalAddContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title" id="contentForm"></h3>
          </div>
          <div class="panel-body">
            <form id="formAddContent" action="" method="post">
              {{csrf_field()}}

              <div class="form-group">
                <label for="title">Judul</label>
                <input placeholder="Judul" type="text" class="form-control" id="contentTitle" name="title" value="" required>
              </div>

              <div class="form-group">
                <label for="title">Deskripsi / Penjelasan</label>
                <textarea name="description" id="contentDescription"></textarea>
              </div>

              <div class="form-group">
                <label for="title">Tipe Konten</label>
                <select class="form-control selectpicker" id="contentTypeContent" name="type_content" required>
                  <option value="">Pilih Tipe Konten</option>
                  <option value="text">Text</option>
                  <option value="video">Video</option>
                  <option value="file">File</option>
                </select>
              </div>

              <div class="form-group">
                <label for="title">Lampiran (Dokumen / Video)</label>
                <input type="file" id="file" class="form-control">
                <input type="hidden" id="path_file" name="path_file">
                <input type="hidden" id="name_file" name="name_file">
                <input type="hidden" id="file_size" name="file_size">
                <input type="hidden" id="video_duration" name="video_duration">
              </div>

              <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
              <div id="container"></div>

              <div class="progress" style="display:none">
                <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  <span class="sr-only">0%</span>
                </div>
              </div>
              <div id="uploadedMessage"></div>

              <div class="form-group">
                <label for="title">Urutan</label>
                <input placeholder="Urutan" id="contentSequence" type="text" class="form-control" name="sequence" value="" required>
              </div>

              <input type="hidden" name="id" id="contentId" value="">
              <div class="form-group">
                {{  Form::submit("Save Content" , array('class' => 'btn btn-primary', 'name' => 'button', 'id' => 'saveContent')) }}
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modalAddQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title" id="quizForm"></h3>
          </div>
          <div class="panel-body">
            <form id="formAddQuiz" action="" method="post">
              {{csrf_field()}}

              <div class="form-group">
                {{-- <label for="name">Judul Kuis</label> --}}
                <input placeholder="Judul Kuis" type="text" class="form-control" name="name" id="quizname" required>
              </div>

              <div class="form-group">
                <label for="title">Deskripsi / Penjelasan</label>
                <textarea name="description" id="Quizdescription"></textarea>
              </div>
              <div class="form-group">
                <label for="title">Acak kuis?</label> &nbsp;
                <input type="radio" name="shuffle" checked value="0">Tidak &nbsp;
                <input type="radio" name="shuffle" value="1">Ya
              </div>
              <div class="form-group">
                {{-- <label for="name">Waktu Mulai</label> --}}
                <input placeholder="Waktu Mulai" type="text" class="form-control form_datetime" name="time_start" id="Quiztime_start" required>
              </div>
              <div class="form-group">
                {{-- <label for="name">Waktu Akhir</label> --}}
                <input placeholder="Waktu Akhir" type="text" class="form-control form_datetime" name="time_end" id="Quiztime_end" required>
              </div>
              <div class="form-group">
                <label for="name">Durasi Menit </label>
                <input placeholder="(Contoh: 60)" type="number" class="form-control" name="duration" id="Quizduration" required>
              </div>

              <input type="hidden" name="id" id="QuizId" value="">
              <div class="form-group">
                {{  Form::submit("Simpan Kuis" , array('class' => 'btn btn-raised btn-primary', 'name' => 'button', 'id' => 'saveQuiz')) }}
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('script')
  <script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
  <script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
  <script src="{{asset('js/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script src="{{asset('uploadify-master/sample/jquery.uploadifive.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
  <?php $timestamp = time();?>
  $(function() {
    var token = '{{ csrf_token() }}';
    $('#file_upload').uploadifive({
      'auto'             : true,
      'formData'         : {
                   'timestamp' : '<?php echo $timestamp;?>',
                   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
                   '_token': token
                           },
     'method'            : 'POST',
      'queueID'          : 'queue',
      'uploadScript'     : '{{route("uploadify", "courseupdate")}}',
      'onUploadComplete' : function(file, data) { console.log(data); },
      'onCancel' : function(file) {
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{route('uploadify.cancel', 'courseupdate')}}",
          type : 'POST',
          data : {
            'data' : file.originName,
            '_token' : '{{csrf_token()}}'
          },
          success : function(result){
            console.log(result);
          }
        });
       },
     'buttonText' : `<img src="{{asset('img/folder.svg')}}" class="img-file"> <span class="add-file">{{Lang::get('front.instructor_group.action_add_file')}}</span>`
    });
  });

  </script>
  <style media="screen">
    .img-file{
      width: 20px;
    }

    .add-file{
      margin-left: 2px;
    }

    .uploadifive-button {
        float: left;
        margin-right: 10px;
        background: none;
        color: black;
        border: none;
        right: 12px;
        cursor: all-scroll;
    }

    .uploadifive-button {
      float: left;
      margin-right: 10px;
    }

    #queue {
        overflow: auto;
        padding: 0 3px 3px;
        width: 100%;
    }

    .uploadifive-button:hover {
        background: none !important;
    }
  </style>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer-update', options);
    CKEDITOR.replace('answer', options);
    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>
  <script>
    $.fn.editable.defaults.mode = 'inline';

    $(document).ready(function() {

      $(".search-content").keyup(function(){
        console.log($(this).val());
        filter_content($(this).val(), $(this));
      });

      $('#CourseTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'title',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseSubTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'subtitle',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        },
      });

      $('#CoursePrice').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'price',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseLevel').editable({
        value: {{$course->level_id}},
        source: [
          @php $course_levels = DB::table('course_levels')->get(); @endphp
          @foreach($course_levels as $course_level)
            {value: {{$course_level->id}}, text: '{{$course_level->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_level_course',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseCategory').editable({
        value: {{$course->id_category}},
        source: [
          @php $categories = DB::table('categories')->get(); @endphp
          @foreach($categories as $category)
            {value: {{$category->id}}, text: '{{$category->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_category',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

    });
  </script>

  {{-- CKEDITOR --}}
  <script src="{{url('ckeditor/ckeditor.js')}}"></script>
  <script>
    const textDescription = `{!! $course->description !!}`;
    const textGoal = `{!! $course->goal !!}`;
    CKEDITOR.replace('editorDescription').setData(textDescription);
    CKEDITOR.replace('editorGoal').setData(textGoal);
    CKEDITOR.replace('contentDescription');
    CKEDITOR.replace('Quizdescription');
  </script>
  {{-- CKEDITOR --}}

  <script>
    $("#saveEditorDescription").click(function(){
      var new_description = CKEDITOR.instances.editorDescription.getData();
      $.ajax({
        url : '/course/editable/{{$course->id}}',
        type : 'post',
        data : {
          _token : '{{csrf_token()}}',
          name : 'description',
          value : new_description,
        },
        success : function(data){
          $("#modalSetEditorDescription").modal('hide')
          $("#recentDescription").html(new_description);
        }
      })
    })

    $("#saveEditorGoal").click(function(){
      var new_goal = CKEDITOR.instances.editorGoal.getData();
      $.ajax({
        url : '/course/editable/{{$course->id}}',
        type : 'post',
        data : {
          _token : '{{csrf_token()}}',
          name : 'goal',
          value : new_goal,
        },
        success : function(data){
          $("#modalSetEditorGoal").modal('hide')
          $("#recentGoal").html(new_goal);
        }
      })
    })

    var fileName = '';
    $("#editorImage").change(function(e){
      fileName = e.target.files[0];
    })

    $("#saveEditorImage").click(function(e){
      var formData = new FormData();
      formData.append('value', fileName);
      formData.append('name', 'image');

      $.ajax({
        url : '/course/editable/{{$course->id}}',
        type : 'post',
        data : formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,
        headers: {
          'X-CSRF-TOKEN': '{{csrf_token()}}'
        },
        success : function(data){
          location.reload();
        }
      })
    })
  </script>

  {{-- add new section --}}
  <script type="text/javascript">
    $("#add_new_section").click(function(){

      $("#section_row").val(parseInt($("#section_row").val())+1)
      var section_add_row = $("#section_row").val();

      $("#place_new_section").append(
        '<div class="panel panel-default" id="section_row_'+section_add_row+'">'+
          '<div class="panel-heading">'+
            '<h3 class="panel-title">@lang("front.page_manage_courses.content_section_add_new_section")</h3>'+
          '</div>'+
          '<div class="panel-body">'+
            '<div class="form-group">'+
              '<input type="text" class="form-control" id="new_section_title_'+section_add_row+'" value="" required>'+
            '</div>'+
              '<button type="button" id="save_new_section" data-id="'+section_add_row+'" class="btn btn-primary btn-raised">@lang("front.general.text_save")</button> '+
              '<button type="button" id="remove_new_section" data-id="'+section_add_row+'" class="btn btn-danger btn-raised">@lang("front.general.text_cancel")</button>'+
          '</div>'+
        '</div>'
      );
    });
  </script>
  {{-- add new section --}}

  {{-- save new section --}}
  <script type="text/javascript">
    $(function(){
      $(document).on('click','#save_new_section',function(e){
        // e.preventDefault();
          var data_id = $(this).attr('data-id');
          var new_section_title = $("#new_section_title_"+data_id).val();

          var id_course = '{{$course->id}}';
          var token = '{{ csrf_token() }}';
          $.ajax({
            url : "{{url('course/save_section')}}",
            type : "POST",
            data : {
              title : new_section_title,
              id_course : id_course,
              _token: token
            },
            success : function(result){
              location.reload();
            },
          });
      });
    });
  </script>
  {{-- save new section --}}

  {{-- remove new section --}}
  <script type="text/javascript">
    $(function(){
      $(document).on('click','#remove_new_section',function(e){
        // e.preventDefault();
          var data_id = $(this).attr('data-id');
          $("#section_row_"+data_id).remove();
      });
    });
  </script>
  {{-- remove new section --}}

  {{-- set edit section --}}
  <script type="text/javascript">
    $(function(){
      $(document).on('click','#EditSection',function(e){
        // e.preventDefault();
          var data_id = $(this).attr('data-id');
          var data_title = $(this).attr('data-title');
          var data_description = $(this).attr('data-description');
          var data_status = $(this).attr('data-status');

          $("#setSectionId").val(data_id)
          $("#setSectionTitle").val(data_title)
          $("#setSectionDescription").val(data_description)
          $("#setSectionStatus").val(data_status).change()
          $("#modalEditSection").modal('show');
      });
    });
  </script>
  {{-- set edit section --}}

  {{-- save edit section --}}
  <script type="text/javascript">
    $(function(){
      $(document).on('click','#saveEditSection',function(e){
        // e.preventDefault();
          var token = '{{ csrf_token() }}';
          $.ajax({
            url : "{{url('section/edit-section')}}",
            type : "POST",
            data : {
              title : $("#setSectionTitle").val(),
              description : $("#setSectionDescription").val(),
              status : $("#setSectionStatus").val(),
              id : $("#setSectionId").val(),
              _token: token
            },
            success : function(result){
              location.reload();
            },
          });
      });
    });
  </script>
  {{-- save edit section --}}


  <script type="text/javascript" src="/plupload/plupload.full.min.js"></script>

  {{-- set sectionid on add content --}}
  <script type="text/javascript">
    var data_section = '';
    $(function(){
      $(document).on('click','#AddContent',function(e){
        // e.preventDefault();
          $("#contentForm").html('Tambah Konten');
          data_section = $(this).attr('data-section');
          $("#formAddContent").attr('action', '/course/content/create_action/'+data_section)
          $("#modalAddContent").modal('show');

          // Custom example logic
          var path_file = $("#path_file").val();
          var name_file = $("#name_file").val();
          var file_size = $("#file_size").val();
          var video_duration = $("#video_duration").val();
          var section_id = data_section;
          // $("#saveContent").prop('disabled', true);

          var uploader = new plupload.Uploader({
            runtimes : 'html5,flash,silverlight,html4',
            browse_button : 'file', // you can pass an id...
            container: document.getElementById('container'), // ... or DOM Element itself
            url : '/course/content/upload/'+section_id,
            flash_swf_url : '/plupload/Moxie.swf',
            silverlight_xap_url : '/plupload/Moxie.xap',
            chunk_size: '1mb',
            dragdrop: true,
            headers: {
              'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            filters : {
              mime_types: [
                {title : "Document files", extensions : "docs,xlsx,pptx"},
                {title : "Video files", extensions : "mp4,webm"},
              ]
            },

            init: {
              PostInit: function() {
                document.getElementById('filelist').innerHTML = '';
              },

              FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                  document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                  uploader.start();
                  return false;
                });
              },

              UploadProgress: function(up, file) {
                // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                $('.progress').show();
                $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
                $("#uploadedMessage").html('Upload dalam proses...');
              },

              FileUploaded: function(up, file, info) {
                  // Called when file has finished uploading
                  // console.log('[FileUploaded] File:', file, "Info:", info);
                  var response = JSON.parse(info.response);
                  // console.log(response.result.original.path);
                  path_file = $("#path_file").val(response.result.original.path);
                  name_file = $("#name_file").val(response.result.original.name);
                  file_size = $("#file_size").val(response.result.original.size);
                  video_duration = $("#video_duration").val(response.result.original.video_duration);
              },

              ChunkUploaded: function(up, file, info) {
                  // Called when file chunk has finished uploading
                  // console.log('[ChunkUploaded] File:', file, "Info:", info);
              },

              UploadComplete: function(up, files) {
                  // Called when all files are either uploaded or failed
                  // console.log('[UploadComplete]');
                  // $("#saveContent").prop('disabled', false);
                  $("#uploadedMessage").html('Upload selesai');
              },

              Error: function(up, err) {
                document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
              }
            }
          });

          uploader.init();
      });
    });
  </script>
  {{-- set sectionid on add content --}}

  <script>
    // edit content
    $(function(){
      $(document).on('click','#EditContent',function(e){
        // e.preventDefault();
        $("#contentForm").html('Edit Content');
        $("#formAddContent").attr('action', '/course/content/update_action');
        var data_id = $(this).attr('data-id');
        var data_title = $(this).attr('data-title');
        var data_description = $(this).attr('data-description');
        var data_type_content = $(this).attr('data-type-content');
        var data_sequence = $(this).attr('data-sequence');
        var data_section = $(this).attr('data-section');
        $("#contentId").val(data_id);
        $("#contentTitle").val(data_title);
        CKEDITOR.instances['contentDescription'].setData(data_description)
        $("#contentTypeContent").val(data_type_content).change();
        $("#contentSequence").val(data_sequence);
        $("#modalAddContent").modal('show');

        // Custom example logic
        var path_file = $("#path_file").val();
        var name_file = $("#name_file").val();
        var file_size = $("#file_size").val();
        var video_duration = $("#video_duration").val();
        var section_id = data_section;
        // $("#saveContent").prop('disabled', true);

        var uploader = new plupload.Uploader({
          runtimes : 'html5,flash,silverlight,html4',
          browse_button : 'file', // you can pass an id...
          container: document.getElementById('container'), // ... or DOM Element itself
          url : '/course/content/upload/'+section_id,
          flash_swf_url : '/plupload/Moxie.swf',
          silverlight_xap_url : '/plupload/Moxie.xap',
          chunk_size: '1mb',
          dragdrop: true,
          headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
          },
          filters : {
            mime_types: [
              {title : "Document files", extensions : "docs,xlsx,pptx"},
              {title : "Video files", extensions : "mp4,webm"},
            ]
          },

          init: {
            PostInit: function() {
              document.getElementById('filelist').innerHTML = '';
            },

            FilesAdded: function(up, files) {
              plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                uploader.start();
                return false;
              });
            },

            UploadProgress: function(up, file) {
              // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
              $('.progress').show();
              $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
              $("#uploadedMessage").html('Upload dalam proses...');
            },

            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                // console.log('[FileUploaded] File:', file, "Info:", info);
                var response = JSON.parse(info.response);
                // console.log(response.result.original.path);
                path_file = $("#path_file").val(response.result.original.path);
                name_file = $("#name_file").val(response.result.original.name);
                file_size = $("#file_size").val(response.result.original.size);
                video_duration = $("#video_duration").val(response.result.original.video_duration);
            },

            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                // console.log('[ChunkUploaded] File:', file, "Info:", info);
            },

            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                // console.log('[UploadComplete]');
                // $("#saveContent").prop('disabled', false);
                $("#uploadedMessage").html('Upload selesai');
            },

            Error: function(up, err) {
              document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
          }
        });

        uploader.init();

      })
    })
    // edit content
  </script>

  {{-- datetime picker --}}
  <script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
  <script type="text/javascript">
    $('.form_datetime').datetimepicker({
      language:  'id',
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      showMeridian: 1
    });
  </script>
  {{-- datetime picker --}}

  <script>
    // add quiz
    var data_section = '';
    $(function(){
      $(document).on('click','#AddQuiz',function(e){
        $("#quizForm").html('Tambah Kuis');
        data_section = $(this).attr('data-section');
        $("#formAddQuiz").attr('action', '/course/quiz/create_action/'+data_section)
        $("#modalAddQuiz").modal('show');
      })
    })
    // add quiz
  </script>

  <script type="text/javascript">
    $("[name=publish{{$course->id}}]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
      var token = "{{csrf_token()}}";
      if(state === true){
        $.ajax({
          url : "/meeting/publish",
          type : "POST",
          data : {
            id : {{$course->id}},
            publish : 1,
            _token: token
          },
          success : function(result){

          },
        });
      }else{
        $.ajax({
          url : "/meeting/publish",
          type : "POST",
          data : {
            id : {{$course->id}},
            publish : 0,
            _token: token
          },
          success : function(result){

          },
        });
      }
    });
  </script>

  <script>
    // trigger modal add konten / quiz
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    // trigger modal add konten / quiz
  </script>

  <script>
    $(function(){
      $(document).on('click','#webinarEditSchedule',function(e){
        $("#webinarId").val($(this).attr('data-id'));
        $("#webinarName").val($(this).attr('data-name'));
        $("#webinarDescription").val($(this).attr('data-description'));
        $("#webinarDate").val($(this).attr('data-date'));
        $("#webinarTime").val($(this).attr('data-time'));
        $("#webinarRecording").val($(this).attr('data-recording')).change();
        $("#modalWebinarEditSchedule").modal('show');
      })
    })
  </script>

  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>

  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        connectWith: '.SectionSortable',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          console.log(itemOrder)

          $.ajax({
            url : '/course/section/update-sequence',
            type : 'POST',
            data : {
              'datas' : itemOrder,
              'course_id' : '{{$course->id}}',
              '_token' : '{{csrf_token()}}'
            },
            success : function(){
            }
          })
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>

  <script>
    $( function() {
      var section_id = ''
      $( ".ContentSortable" ).sortable({
        axis: 'y',
        connectWith: '.ContentSortable',
        helper: 'clone',
        placeholder: "placeholder",
        over:function(event,ui){
          section_id = $('.placeholder').parent().attr('id');
        },
        out: function(event, ui) {
          var sequence = ui.item.index();
          var content_id = ui.item.attr('id');
          var content_type = ui.item.attr('content-type');
          var itemOrder = $(this).sortable('toArray', {attribute: 'id'} );
          var itemContentOrder = $(this).sortable('toArray', {attribute: 'content-type'} );
          console.log(itemOrder);
          console.log(itemContentOrder);
          $.ajax({
            url : '/course/content/update-sequence',
            type : 'POST',
            data : {
              'datas' : itemOrder,
              'content_datas' : itemContentOrder,
              'section_id' : section_id,
              '_token' : '{{csrf_token()}}'
            },
            success : function(){
            }
          })
        },
      }).disableSelection();
    });
  </script>

  {{-- sortable --}}

  <script type="text/javascript">
    $("[class=preview]").bootstrapSwitch().on("switchChange.bootstrapSwitch", function(event, state) {
      var token = "{{csrf_token()}}";
      var content_id = $(this)[0].value;
      if(state === true){
        // alert(state);
        $.ajax({
          url : "/course/content/update-preview",
          type : "POST",
          data : {
            id : content_id,
            course_id : '{{$course->id}}',
            preview : 1,
            _token: token
          },
          success : function(result){
            // alert(result.message);
            if(result.alert == 'error'){
              $("[id=preview"+content_id+"]").bootstrapSwitch('state', false)
            }
            swal(result.title, result.message, result.alert);
          },
        });
      }else{
        // alert(state);
        $.ajax({
          url : "/course/content/update-preview",
          type : "POST",
          data : {
            id : content_id,
            course_id : '{{$course->id}}',
            preview : 0,
            _token: token
          },
          success : function(result){
            // swal(result.title, result.message, result.alert);
          },
        });
      }
    });
  </script>

  <script type="text/javascript">
    // $(function(){
    //   $(document).ready(function(){
    //     $('.get-from-library').on('click', function(){
    //       $('#content-id-value').val($('.get-from-library').attr('content-id'));
    //       alert($('#content-id-value').val());
    //     })
    //   })
    // })
  </script>

  <script type="text/javascript">
    $(".preview_content").click(function(){
      var data_id = $(this).attr('value');
      var token = "{{csrf_token()}}";
      var content_id = $(this).attr('value');
      var data_preview = $(this).attr('data-preview');
      // alert(data_preview)
      if(data_preview == '0'){

        $.ajax({
          url : "/course/content/update-preview",
          type : "POST",
          data : {
            id : content_id,
            course_id : '{{$course->id}}',
            preview : 1,
            _token: token
          },
          success : function(result){
            // alert(result.message);
            swal({
              title: result.title,
              text: result.message,
              type: 'success'
            }).then(function() {
                window.location = "";
            });
          },
        });

      }else{

        $.ajax({
          url : "/course/content/update-preview",
          type : "POST",
          data : {
            id : content_id,
            course_id : '{{$course->id}}',
            preview : 0,
            _token: token
          },
          success : function(result){
            swal({
              title: result.title,
              text: result.message,
              type: 'success'
            }).then(function() {
                window.location = "";
            });
          },
        });

      }
    })
  </script>

  {{-- instructor n group --}}
  <script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
  <script type="text/javascript">
    $("#id_author").select2({
      placeholder: "Pilih pengajar"
    });
    $("#id_instructor_group").select2({
      placeholder: "Pilih group"
    });
  </script>
  {{-- instructor n group --}}

  <script type="text/javascript">
    $("#saveAuthor").click(function(){
      $.ajax({
        url : "/course/preview/update-authors",
        type : "POST",
        data : {
          course_id : '{{$course->id}}',
          authors : $("#id_author").val(),
          _token: '{{csrf_token()}}'
        },
        success : function(result){
          console.log(result);
          window.location.reload();
        },
      });
    })

    $("#saveInstructorGroup").click(function(){
      $.ajax({
        url : "/course/preview/update-instructor-groups",
        type : "POST",
        data : {
          course_id : '{{$course->id}}',
          id_instructor_group : $("#id_instructor_group").val(),
          _token: '{{csrf_token()}}'
        },
        success : function(result){
          // console.log(result);
          window.location.reload();
        },
      });
    })
  </script>

  <script>
    $(".sectionActivated").change(function(){
      var sectionStatus = $(this).prop('checked')
      // alert($(this).attr('id-section'))
      $.ajax({
        url: "/course/section/change-status",
        type: "post",
        data: {
          section_id: $(this).attr('id-section'),
          status: sectionStatus,
          _token: "{{csrf_token()}}"
        },
        success: function(){
          // alert()
        }
      })
    })
  </script>
  <!-- course filter -->
  <script>
  $(".search-course").keyup(function(){
    var parents = $(this).parents('.course-library');
    // console.log(parents);
    filter($(this).val(), parents);
  });

  function filter(course, parents) {
      var i = 0;
      parents.find('li').each(function() {
        if(i != 0){
          var course_search = $(this).find('a').text().toLowerCase().includes(course.toLowerCase());
          if(course_search){
              $(this).show();
          }
          else {
              $(this).hide();
          }
        }
        i++;
      });
  }

  function filter_content(content, parentstable) {
      var i = 0;
      parentstable.parents('.content-full-search').find('tbody').find('tr').each(function() {
          var course_search = $(this).find('.library-list').text().toLowerCase().includes(content.toLowerCase());
          if(course_search){
              $(this).show();
          }
          else {
              $(this).hide();
          }
        i++;
      });
  }
  </script>

  <script>
    $('#example-1').DataTable();
    $('#example-2').DataTable();
    $('#example-3').DataTable();
    @foreach($group as $index => $item)
    $('#example-group-{{$item->group_code}}').DataTable();
    @endforeach

    // <input type="checkbox" name="content[]" value="">
    // <input type="checkbox" name="content[]" value="">
    // <input type="checkbox" name="content[]" value="">

    @foreach($course_all as $index => $item)
    $('#example-course-{{$item->id}}').DataTable({
      'columnDefs': [{
                       'targets': 0,
                       'searchable': false,
                       'orderable': false,
                       'className': 'dt-body-center',
                       'render': function (data, type, full, meta){
                           return '<input type="checkbox" name="content[]" value="' + $('<div/>').text(data).html() + '">';
                       }
                    }],
      'order': [[1, 'asc']]
    });

    $('#form-library-course-tab-{{$item->id}}').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      $('#example-course-{{$item->id}}').$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });
    @endforeach

    $("#example-1_wrapper .dataTables_length").html("My Library");
    $("#example-2_wrapper .dataTables_length").html("Shared Library");
    $("#example-3_wrapper .dataTables_length").html("Public Library");
    @foreach($group as $index => $item)
    $("#example-group-{{$item->group_code}}_wrapper .dataTables_length").html("{{$item->title}}");
    @endforeach

    @foreach($course_all as $index => $item)
    $("#example-course-{{$item->id}}_wrapper .dataTables_length").html("{{$item->title}}");
    @endforeach
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
  </script>
  <!-- The template to display files available for upload -->
  <script>
    $(".komentar-first").click(function(){
      $(".komentar-second").removeClass('hidden');
      $(".komentar-first").addClass('hidden');
    });

    $("#cancel-komen").click(function(){
      $(".komentar-first").removeClass('hidden');
      $(".komentar-second").addClass('hidden');
    });

    $("#add-file-jquery-upload").change(function(){
      $(".jQuery-upload-btn").removeClass('hidden');
    });

    $("#cancel-jquery-upload").click(function(){
      $(".jQuery-upload-btn").addClass('hidden');
    });

    $(".text-reply").hide();
    $(".reply-btn").click(function(){
      $(".text-reply").hide();
      $(this).siblings(".text-reply").show();

    });

    function versandkosten() {
        var lengthTable = $('.template-upload').length;
        if(lengthTable == 0){
          $(".jQuery-upload-btn").addClass('hidden');
        }
    }

    var basketAmount = $('.table-files-upload');
    basketAmount.bind("DOMSubtreeModified", versandkosten);
  </script>
  <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
                  <button class="btn btn-success edit" data-index="{%=i%}" disabled>
                      <i class="glyphicon glyphicon-edit"></i>
                      <span>Edit</span>
                  </button>
                {% } %}
                {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel cancel-template-td">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
  </script>
  <!-- The template to display files available for download -->
  <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
      {% if (file.name.substring(0,{{strlen(Auth::user()->id)}}) == {{Auth::user()->id}}) { %}
        <tr class="template-download">
            <td>
                <span class="preview">
                    {% if (file.name.split(".")[file.name.split(".").length-1] == 'pdf') { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}"><img src="{{asset('images/pdf.png')}}" style="width: 50px;"></a>
                    {% }else if (file.name.split(".")[file.name.split(".").length-1] == 'docx') { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}"><img src="{{asset('images/word.png')}}" style="width: 50px;"></a>
                    {% } %}

                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}"><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">{%=file.name.substring({{strlen(Auth::user()->id)}}+1)%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl+'&page=question&id={{Auth::user()->id}}'%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Delete</span>
                    </button>
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
      {% } %}
    {% } %}
  </script>
  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
  <script src="/jquery-upload/js/vendor/jquery.ui.widget.js"></script>
  <!-- The Templates plugin is included to render the upload/download listings -->
  <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
  <!-- blueimp Gallery script -->
  <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="/jquery-upload/js/jquery.iframe-transport.js"></script>
  <!-- The basic File Upload plugin -->
  <script src="/jquery-upload/js/jquery.fileupload.js"></script>
  <!-- The File Upload processing plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-process.js"></script>
  <!-- The File Upload image preview & resize plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-image.js"></script>
  <!-- The File Upload audio preview plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-audio.js"></script>
  <!-- The File Upload video preview plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-video.js"></script>
  <!-- The File Upload validation plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-validate.js"></script>
  <!-- The File Upload user interface plugin -->
  <script src="/jquery-upload/js/jquery.fileupload-ui.js"></script>
  <!-- The main application script -->
  <script src="/jquery-upload/js/demo.js"></script>
  <script>
  // Initialize the jQuery File Upload widget:
  $('#fileupload-discussion').fileupload({
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    url: '/jquery-upload/server/php?page=course_update&id='+{{Auth::user()->id}},
    autoUpload:true
  });

  // Load existing files:
  $('#fileupload-discussion').addClass('fileupload-processing');
  $.ajax({
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    url: $('#fileupload-discussion').fileupload('option', 'url'),
    dataType: 'json',
    context: $('#fileupload-discussion')[0]
  })
    .always(function () {
      $(this).removeClass('fileupload-processing');
    })
    .done(function (result) {
      $(this)
        .fileupload('option', 'done')
        // eslint-disable-next-line new-cap
        .call(this, $.Event('done'), { result: result });
    });
  </script>

  <style>
    .table-cs .dataTables_length{
    background: #4ca3d9;
    color: white !important;
    padding: 0px 10px;
    border-radius: 10px;
    }

    .table-cs input{
    box-shadow: 0px 5px 18px -10px;
    border-radius: 10px;
    border: 1px solid #00000014;
    padding-left: 15px;
    }

    .table-cs thead>tr>th{
    border-bottom: none;
    padding: 8px 10px;
    }

    .table-cs tbody>tr>td{
    background: white !important;
    }

    .table-cs tbody>tr>td{
    background: white !important;
    }

    .table-cs .dataTables_paginate .paginate_button{
    }

    .table-cs .dataTables_paginate span a.paginate_button
    {
    padding: 0;
    /* background: #4cd137; */
    background: none;
    border-radius: 50px;
    border:none;
    }

    .table-cs .dataTables_paginate span a.paginate_button.current{
    background: #4cd137;
    color:white !important;
    }

    .table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after{
      display: none !important
    }

    .table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child{
      width: 0.1px !important;
      padding: 0;
    }

    .table-cs .dataTables_info{
    display: none;
    }

    .dataTables_filter{
    margin-bottom: 15px;
    }
  </style>

<script type="text/javascript">
  $("[name=status{{$course->id}}]").on("change", function() {
    var state = $("[name=status{{$course->id}}]").is(":checked")
    var token = "{{csrf_token()}}";
    if(state === true){
      $.ajax({
        url : "/course/publish",
        type : "POST",
        data : {
          id : '{{$course->id}}',
          status : 1,
          live : {{is_liveCourse($course->id) == true ? 'true' : 'false'}},
          _token: token
        },
        success : function(result){
          // console.log(result);
          if(result.status != 200){
            swal({
              type: 'warning',
              title: 'Gagal...',
              html: result.message,
            })
          }else{
            swal("Berhasil!", "Kursus Anda telah aktif dan dapat diakses", "success");
          }
        },
      });
    }else{
      $.ajax({
        url : "/course/publish",
        type : "POST",
        data : {
          id : {{$course->id}},
          status : 0,
          _token: token,
          live : '{{is_liveCourse($course->id) == true ? 'true' : 'false'}}',
        },
        success : function(result){

        },
      });
    }
  });
</script>
@endpush
