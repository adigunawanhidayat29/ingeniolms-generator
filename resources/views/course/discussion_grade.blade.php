@extends('layouts.app')

@section('title', 'Judul Grup')

@push('style')
  <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <style>
    .nav-tabs-ver-container .nav-tabs-ver {
      padding: 0;
      margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
      display: none;
    }
  </style>

  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .previous-tab, .next-tab{
    	display: inline-block;
    	border: 1px solid #444348;
    	border-radius: 3px;
    	margin: 5px;
    	color: #444348;
    	font-size: 14px;
    	padding: 10px 15px;
    	cursor: pointer;
    }

    .tab {
      display: none;
    }

    .grade-page input{
      width: 60px;
      margin-right: 20px;
      height: 30px;
      margin-top: 13px;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/my-course">Kelas Saya</a></li>
          <li>{{$discussion->title}}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <!-- <div class="col-md-3 nav-tabs-ver-container">
          <a class="btn btn-block btn-raised btn-default no-shadow mt-0 mb-2" href="/instructor/groups/detail"><i class="zmdi zmdi-arrow-left"></i> Kembali</a></li>
          <div class="card card-success-inverse no-shadow">
            <div class="card-block">
              <p class="fs-14 fw-600 mb-0">Access Code</p>
              <p class="fs-20 fw-300 text-uppercase" style="letter-spacing: 2px;">qwert-yuiop</p>
            </div>
          </div>
        </div> -->
        <div class="col-md-12">
          <!-- <div class="grade" style="text-align:right">
            <a class="btn btn-raised btn-primary no-shadow" style="width:170px;text-align:right" href="{{url('course/content/discussion/manage/grade', $discussion->id)}}"><i class="fa fa-users"></i> Grade Siswa</a>
          </div> -->
          <div class="mb-2">
            <h3 class="headline headline-sm mt-0">{{$discussion->title}}</h3>
            <p class="fs-14" style="color: #999;">{{date('d M Y',strtotime($discussion->created_at))}}</p>
            <p class="fs-16 mb-0">{!!$discussion->description!!}</p>
          </div>
          <div class="btn-page">
            <div class="row clearfix">
              <div class="col-sm-8">
              </div>
              <div class="col-sm-4">
                <div class="nexts" style="text-align:right">
                  <?php $i=0; ?>
                  <select name"siswa" class="form-control list-siswa" style="float:left;width: 40%;    height: 40px;padding:10px">
                    <option value="">-Pilih Siswa-</option>
                    @foreach($discussion_user as $index => $siswa)
                    @if(!in_array($siswa->user_id, explode(',', $course->id_author)))
                    <option value="{{$i}}">{{$siswa->user->name}}</option>
                    <?php $i++; ?>
                    @endif
                    @endforeach
                  </select>
                  <button type="button" id="prevBtn" onclick="nextPrev(-1)">Kembali</button>
                  <button type="button" id="nextBtn" onclick="nextPrev(1)">Selanjutnya</button>
                </div>
              </div>
            </div>
          </div>
          <br>
          <?php $i=0; ?>
          @foreach($discussion_user as $index => $siswa)
          @if(!in_array($siswa->user_id, explode(',', $course->id_author)))
          <?php
            $grade = $siswa->grade($discussion->id);
          ?>
          <div class="tab" siswa="{{$siswa->id}}" tabNumber="{{$i}}">
            <div class="row clearfix">
              <div class="col-sm-8">
                @foreach($discussion_list->where('user_id', $siswa->user->id) as $item)
                <div class="card card-groups dropdown">
                  @if($item->user_id == Auth::user()->id)
                  <div class="groups-dropdown">
                    <div class="groups-dropdown-group">
                      <i class="fa fa-ellipsis-v"></i>
                      <ul class="groups-dropdown-menu">
                        <li>
                          <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$item->id}}').submit()">
                            <i class="fa fa-trash"></i> Hapus
                          </a>
                          <form action="{{url('/course/content/discussion/delete/'.$item->id)}}" id="form-koment-delete-{{$item->id}}" method="post" style="display:none;">
                            {{csrf_field()}}
                          </form>
                        </li>
                      </ul>
                    </div>
                  </div>
                  @endif
                  <div class="card-block">
                    <a class="groups-title" href="#">{{$item->user->name}}</a>
                    <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($item->created_at))}}&nbsp;&middot;&nbsp;</p>
                    <p class="groups-description">{!!$item->body!!}</p>
                  </div>
                </div>
                @endforeach
              </div>
              <div class="col-sm-4">
                <div class="card card-groups dropdown">
                  <div class="card-block">
                    <p class="fs-14" style="color: #999;">SISWA</p>
                    <input type="hidden" class="id-siswa" value="{{$siswa->user_id}}">
                    <a class="groups-title siswa-name" href="#">{{$siswa->user->name}}</a>
                    <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($siswa->created_at))}}&nbsp;&middot;&nbsp;</p>
                    <p class="fs-14" style="color: #999;">Nilai</p>
                    <div class="grade-page" style="display:flex">
                      <p class="grade-nilai" style="margin: auto 37px;">{{$grade ? $grade->grade : 0}}</p>
                      <input type="number" name="nilai" class="grade-nilai-input" value="{{$grade ? $grade->grade : 0}}">
                      <button class="btn btn-raised btn-primary btn-nilai">Beri Nilai</button>
                      <div class="edit-nilai-page">
                        <button class="btn btn-raised btn-info btn-nilai-save"><i class="fa fa-check"></i></button>
                        <button class="btn btn-raised btn-danger btn-nilai-cancel"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <br>
                    <p class="fs-14" style="color: #999;">Catatan</p>
                    <div class="grade-catatan-page">
                      <P class="form-control">{{$grade ? $grade->comment : 'Tidak Ada Catatan'}}</p>
                      <textarea name="comment" class="grade-catatan-input" style="width:100%">{{$grade ? $grade->comment : null}}</textarea>
                      <button class="btn btn-raised btn-primary btn-catatan">Beri Catatan</button>
                      <div class="edit-catatan-page">
                        <button class="btn btn-raised btn-info btn-catatan-save"><i class="fa fa-check"></i></button>
                        <button class="btn btn-raised btn-danger btn-catatan-cancel"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php $i++; ?>
          @endif
          @endforeach

      </div>
    </div>
  </div>
@endsection

@push('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script>
      var getUrlParameter = function getUrlParameter(sParam) {
      var sPageURL = window.location.search.substring(1),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
          }
      }
    };

    var currentTab =1; // Current tab is set to be the first tab (0)
    var siswa = getUrlParameter('siswa');
    if(siswa){
      var tabSiswa = $(".tab[siswa="+siswa+"]");
      if(tabSiswa[0]){
        currentTab = parseInt(tabSiswa.attr('tabNumber'));
      }
    }

    var token = '{{ csrf_token() }}';
    $(".grade-nilai-input").hide();
    $(".grade-catatan-input").hide();
    showTab(currentTab); // Display the current tab

    function showTab(n) {
      // This function will display the specified tab of the form...
      var x = document.getElementsByClassName("tab");
      x[n].style.display = "block";
      //... and fix the Previous/Next buttons:
      if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
      } else {
        document.getElementById("prevBtn").style.display = "inline";
      }

      if (n == (x.length - 1)) {
        document.getElementById("nextBtn").style.display = "none";
      } else {
        document.getElementById("nextBtn").style.display = "inline";
      }
      //... and run a function that will display the correct step indicator:
      fixStepIndicator(n)
    }

    function nextPrev(n) {
      // This function will figure out which tab to display
      var x = document.getElementsByClassName("tab");
      console.log(x);
      // Exit the function if any field in the current tab is invalid:
      x[currentTab].style.display = "none";
      // Increase or decrease the current tab by 1:
      currentTab = currentTab + n;
      // Otherwise, display the correct tab:
      showTab(currentTab);
    }

    function fixStepIndicator(n) {
      // This function removes the "active" class of all steps...
      var i, x = document.getElementsByClassName("step");
      for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
      }
      //... and adds the "active" class on the current step:
      // x[n].className += " active";
    }

    $(".edit-nilai-page").hide();
    $(".edit-catatan-page").hide();
    $('#example-1').DataTable({
      language: { search: '', searchPlaceholder: "Search ..." },
    });

    $(".btn-nilai").click(function(){
      $(this).hide();
      $(this).siblings().show();
      $(this).siblings('p').hide();
      $(this).siblings('input').show();
      // $(this).siblings('p').replaceWith($('<input name="nilai" class="form-control" value="'+inpt+'"></input>'));
    });

    $(".btn-catatan").click(function(){
      $(this).hide();
      $(this).siblings().show();
      $(this).siblings('p').hide();
      $(this).siblings('textarea').show();
      // $(this).siblings('p').replaceWith($('<input name="nilai" class="form-control" value="'+inpt+'"></input>'));
    });

    $(".btn-catatan-cancel").click(function(){
      var inpt = $(this).parents('.edit-catatan-page').siblings('p').text();
      if(inpt == 'Tidak Ada Catatan'){
        inpt = null;
      }
      $(this).parents('.edit-catatan-page').hide();
      $(this).parents('.edit-catatan-page').siblings().show();
      $(this).parents('.edit-catatan-page').siblings('textarea').val(inpt);
      $(this).parents('.edit-catatan-page').siblings('textarea').hide();
      $(this).parents('.edit-catatan-page').siblings('p').show();
      // console.log($(this).parents('.edit-nilai-page'));
    });

    $(".btn-catatan-save").click(function(){
      var inpt = $(this).parents('.edit-catatan-page').siblings('textarea').val();
      $(this).parents('.edit-catatan-page').hide();
      $(this).parents('.edit-catatan-page').siblings().show();
      $(this).parents('.edit-catatan-page').siblings('textarea').hide();
      $(this).parents('.edit-catatan-page').siblings('p').text(inpt);
      $(this).parents('.edit-catatan-page').siblings('p').show();
      var name = $(this).parents('.grade-catatan-page').siblings('.siswa-name').text();
      var id_siswa = $(this).parents('.grade-catatan-page').siblings('.id-siswa').val();
      $.ajax({
        url : "{{url('course/content/discussion/manage/grade/nilai/')}}/"+id_siswa+"/"+"{{$discussion->id}}",
        type : "POST",
        data : {
          comment : inpt,
          _token: token
        },
        success : function(result){
          alert('Catatan untuk siswa '+name+' sudah diubah.');
        },
      });
    });

    $(".btn-nilai-cancel").click(function(){
      var inpt = parseInt($(this).parents('.edit-nilai-page').siblings('p').text());
      $(this).parents('.edit-nilai-page').hide();
      $(this).parents('.edit-nilai-page').siblings().show();
      $(this).parents('.edit-nilai-page').siblings('input').val(inpt);
      $(this).parents('.edit-nilai-page').siblings('input').hide();
      $(this).parents('.edit-nilai-page').siblings('p').show();
      // console.log($(this).parents('.edit-nilai-page'));
    });

    $(".btn-nilai-save").click(function(){
      var inpt = parseInt($(this).parents('.edit-nilai-page').siblings('input').val());
      $(this).parents('.edit-nilai-page').hide();
      $(this).parents('.edit-nilai-page').siblings().show();
      $(this).parents('.edit-nilai-page').siblings('input').hide();
      $(this).parents('.edit-nilai-page').siblings('p').text(inpt);
      $(this).parents('.edit-nilai-page').siblings('p').show();
      var name = $(this).parents('.grade-page').siblings('.siswa-name').text();
      var id_siswa = $(this).parents('.grade-page').siblings('.id-siswa').val();
      $.ajax({
        url : "{{url('course/content/discussion/manage/grade/nilai/')}}/"+id_siswa+"/"+"{{$discussion->id}}",
        type : "POST",
        data : {
          nilai : inpt,
          _token: token
        },
        success : function(result){
          console.log(result);
          alert('Berhasil merubah nilai siswa '+name);
        },
      });
      // console.log($(this).parents('.edit-nilai-page'));
    });

    $(".list-siswa").change(function(){
      var value = $(this).val();
      if($(this).val() == ""){
        currentTab = 0;
        showTab(currentTab);
      }else {
        $(".tab").css("display", "none");
        currentTab = parseInt($(this).val());
        showTab(currentTab);
      }

      $(this).val("");
    });
  </script>
@endpush
