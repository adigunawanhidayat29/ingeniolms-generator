@extends('layouts.app')

@section('title', $course->title)

@push('style')
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="/css/bootstrap-editable.css">
  <style media="screen">
    .editable-input input{
      /*color: white !important;*/
    }
  </style>
@endpush

@section('content')
  <div style="background:#393939; padding:25px;margin-top:-40px;">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          @if($course->introduction != NULL)
            <div class="card">
              <div class="embed-responsive embed-responsive-16by9" style="position:relative;">
                <iframe class="embed-responsive-item" src="{{$course->introduction.'/preview'}}" allowfullscreen="true"></iframe>
                <div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
              </div>
            </div>
          @else
            <img class="img-fluid" src="{{asset_url(course_image_medium($course->image))}}" alt="{{$course->title}}">
            <a href="#" data-target="#modalSetImage" data-toggle="modal" class="pull-right">Edit Image <i style="color:white;" class="fa fa-pencil"></i></a>
          @endif
        </div>
        <div class="col-md-7">
          <h3 style="color:white; font-weight:bold;">
            <a href="#" id="CourseTitle" data-type="text">
              {{$course->title}} <i style="color:white;" class="fa fa-pencil"></i>
            </a>
          </h3>
          <p>
            <span>
              <a href="#">
                @for($i=1;$i<=$rate;$i++)
                  <i class="fa fa-star color-warning"></i>
                @endfor
                @for($i=$rate;$i<5;$i++)
                  <i class="fa fa-star color-white"></i>
                @endfor
                Rate this course
              </a>
            </span>
          </p>
          <p style="color:white">
            <strong>{{$num_progress}}</strong> of <strong>{{$num_contents}}</strong> Items complete <a href="#">Reset Progress</a>
            <div class="progress">
              <div class="progress-bar" role="progressbar"
                aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%">
                {{ceil($percentage)}}%
              </div>
            </div>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <section class="ms-component-section">
      <div class="card card-flat">
        <div class="container">
          <ul class="nav nav-tabs nav-tabs-transparent indicator-info nav-tabs-full nav-tabs-4" role="tablist">
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
                <i class="fa fa-eye"></i>
                <span class="d-none d-sm-inline">Overview</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple active" href="#course-content" aria-controls="course-content" role="tab" data-toggle="tab">
                <i class="fa fa-graduation-cap"></i>
                <span class="d-none d-sm-inline">Course Content</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#QandA" aria-controls="QandA" role="tab" data-toggle="tab">
                <i class="fa fa-question-circle-o"></i>
                <span class="d-none d-sm-inline">Q & A</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link withoutripple" href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                <i class="fa fa-bullhorn"></i>
                <span class="d-none d-sm-inline">Announcements</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="card-block no-pl no-pr">
              <div class="tab-content">

                <!-- TAB OVERVIEW -->
                <div role="tabpanel" class="tab-pane fade" id="overview">
                  <div class="row">

                    <!--RECENT ACTIVITY-->
                    <div class="col-md-12">
                      <h3 class="color-primary"><b>Recent Activity</b></h3>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group">
                          <li class="list-group-item">
                            <b>Recent Questions</b>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group">
                          <li class="list-group-item">
                            <b>Recent Instructor Announcements</b>
                          </li>
                        </ul>
                      </div>
                    </div>

                    <!--ABOUT COURSE-->
                    <div class="col-md-12">
                      <hr>
                      <h3 class="color-primary"><b>About this course</b></h3>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <span>By the numbers</span>
                        </div>
                        <ul class="col-md-3">
                          <li class="dq-list-item">Lectures: {{count($sections)}}</li>
                          <li class="dq-list-item">Category:
                            <a href="#" id="CourseCategory" data-type="select">
                              {{$course->category}} <i class="fa fa-pencil"></i>
                            </a>
                            </li>
                          <li class="dq-list-item">Skill level:
                            <a href="#" id="CourseLevel" data-type="select">
                              {{$course->level}} <i class="fa fa-pencil"></i>
                            </a>
                          </li>
                          <i class="fa fa-money fa-2x"></i>
                          <a href="#" id="CoursePrice" data-type="text">
                            {{$course->price}} <i class="fa fa-pencil"></i>
                          </a>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <span>Description</span>
                        </div>
                        <ul class="col-md-8">
                          <div id="recentDescription">
                            {!! $course->description !!}
                          </div>
                          <a href="#" id="getEditorDescription" data-target="#modalSetEditorDescription" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <span>Goal</span>
                        </div>
                        <ul class="col-md-8">
                            <div id="recentGoal">
                              {!! $course->goal !!}
                            </div>
                            <a href="#" id="getEditorGoal" data-target="#modalSetEditorGoal" data-toggle="modal">Edit <i class="fa fa-pencil"></i></a>
                        </ul>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <span>Instructor</span>
                        </div>
                        <div class="col-md-10">
                          <div class="row">
                            <div class="col-md-2">
                              <img src="{{avatar($course->photo)}}" class="dq-image-user-big">
                            </div>
                            <div class="col-md-6">
                              <h3>{{$course->author}}</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- TAB COURSE CONTENT -->
                <div role="tabpanel" class="tab-pane fade active show" id="course-content">

                  <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
                    <?php $i=1; ?>
                    <div class="panel-group" id="accordion fade in active">
                      @foreach($sections as $section)
                       <div class="panel panel-default">
                         <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
                           <div class="panel-heading" style="background:#e9e9e9">
                             <h4 class="panel-title">
                                <span style="color:#474545; font-size:13px">Section {{$i}}:</span>
                                <br/><br/>
                                <strong style="color:#2e2e2e">{{ $section['title'] }}</strong>
                                <a href="#" id="EditSection" data-id="{{ $section['id'] }}" data-title="{{ $section['title'] }}" data-description="{{ $section['description'] }}">Edit <i class="fa fa-pencil"></i></a>
                             </h4>
                           </div>
                         </a>
                         <div id="content{{$i}}" class="panel-collapse collapse {{$i == 1 ? 'show' : ''}}">
                           <div class="panel-body" style="background:white;">
                            <a href="#" id="AddContent" data-section="{{ $section['id'] }}">Add New Content</a>
                            <div class="list-group">
                              @foreach($section['section_contents'] as $content)
                                @php
                                  $content_progress = DB::table('progresses')->where(['content_id'=>$content->id, 'user_id' => Auth::user()->id, 'status' => '1'])->first();
                                @endphp
                                <a target="_blank" href="/course/learn/content/{{$course->slug}}/{{$content->id}}" class="list-group-item list-group-item-action withripple">
                                  <i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}
                                </a>
                                <a href="#" id="EditContent" data-id="{{$content->id}}" data-title="{{$content->title}}" data-description="{{$content->description}}" data-type-content="{{$content->type_content}}" data-sequence="{{$content->sequence}}" data-section="{{$content->id_section}}">Edit</a>
                              @endforeach
                            </div>
                            <hr>
                            <a href="#" id="AddQuiz" data-section="{{ $section['id'] }}">Add New Quiz</a>
                            <div class="list-group">
                              @foreach($section['section_quizzes'] as $quiz)
                                <a href="/course/learn/content/{{$course->slug}}/{{$content->id}}?quiz=/quiz/{{$quiz->id}}" class="list-group-item list-group-item-action withripple">
                                  <i class="fa fa-star"></i>{{$quiz->name}}
                                  <a href="/course/quiz/question/manage/{{$quiz->id}}" target="_blank">Manage Quiz</a>
                                </a>
                              @endforeach
                            </div>
                            <hr>
                            <a target="_blank" href="/course/assignment/create/1">Add New Assignment</a>
                            <div class="list-group">
                              @foreach($section['section_assignments'] as $assignment)
                                @if(date("Y-m-d H:i:s") <= $assignment->time_end)
                                  <a class="list-group-item list-group-item-action withripple" href="/course/learn/content/{{$course->slug}}/{{$content->id}}?assignment=/assignment/{{$assignment->id}}">
                                    <i class="fa fa-tasks"></i>{{$assignment->title}}
                                  </a>
                                @else
                                  {{$assignment->title}} (time has run out) <br>
                                @endif
                              @endforeach
                            </div>
                           </div>
                         </div>
                       </div>
                      <?php $i++; ?>
                      @endforeach
                    </div>
                    <div id="place_new_section"></div>
                    <input type="hidden" value="0" id="section_row">
                    <button type="button" class="btn btn-raised btn-block btn-lg" id="add_new_section">Add New Section</button>
                  </div>
                </div>

                <!-- TAB Q&A -->
                <div role="tabpanel" class="tab-pane fade" id="QandA">
                  <div class="card">
                    <table class="table dq-table">
                      <tr>
                        <td class="col-md-4 dq-no-margin">
                          <input type="text" class="form-control" name="" placeholder="Search questions">
                        </td>
                        <td>or</td>
                        <td><a href="#" data-toggle="modal" data-target="#modalAskQuestion" class="btn btn-raised btn-lg btn-primary">Ask a new question</a></td>
                      </tr>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col-lg-4 dq-no-margin">
                      <select class="form-control selectpicker" data-dropup-auto="false">
                        <option>Sort by recency</option>
                        <option>Sort by popular</option>
                      </select>
                    </div>
                    <div class="col-lg-8 dq-no-margin">
                      <div class="checkbox dq-padding right">
                        <label>
                          <input type="checkbox">See questions I'm following</label>
                        <label>
                          <input type="checkbox">See questions without responses</label>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <ul class="list-group">
                      @foreach($discussions as $discussion)
                        <a href="/discussion/detail/{{$discussion->id}}">
                          <li class="list-group-item card-block">
                            <span class="text-center dq-pl">
                              <img src="{{avatar($discussion->photo)}}" class="dq-image-user" alt="{{$discussion->name}}">
                            </span>
                            <span class="col-lg-10 dq-max">
                              <b>{{$discussion->name}}</b>
                              <p>{{$discussion->body}}</p>
                            </span>
                            <span class="text-center dq-pl dq-pr">
                              <b>{{(DB::table('discussions_answers')->where('discussion_id', $discussion->id))->count()}}</b>
                              <p>responses</p>
                            </span>
                          </li>
                        </a>
                      @endforeach
                    </ul>
                  </div>
                  <div class="text-center">
                    {{-- <a href="" class="btn btn-raised btn-info">
                      Load More
                    </a> --}}
                  </div>
                </div>

                <!-- TAB ANNOUNCEMENTS -->
                <div role="tabpanel" class="tab-pane fade" id="announcements">
                  @foreach($announcements as $announcement)
                    <div class="card">
                      <div class="card-block-big">
                        <p>
                          <div class="row">
                            <img src="{{avatar($announcement['photo'])}}" class="dq-image-user" style="margin-left:15px">
                            <div class="col-md-8">
                              <div>
                                {{$announcement['name']}}
                              </div>
                              <div>
                                <span>{{$announcement['created_at']}}</span>
                              </div>
                            </div>
                          </div>
                        </p>
                        <h3>{{$announcement['title']}}</h3>
                        <p>
                          {!!$announcement['description']!!}
                        </p>
                        <!--FLOATING FORM-->
                        <p>
                          <form class="form-group label-floating"  method="post" action="#">
                            {{csrf_field()}}
                            <div class="input-group">
                              <span class="input-group-addon">
                                <img src="{{avatar(Auth::user()->photo)}}" class="dq-image-user dq-ml">
                              </span>
                              <label class="control-label" for="addon2">Enter your comment</label>
                              <input type="text" id="addon2" class="form-control" name="comment">
                              <input type="hidden" name="course_announcement_id" value="{{$announcement['id']}}">
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-raised btn-primary">
                                  Kirim
                                </button>
                              </span>
                            </div>
                          </form>
                        </p>

                        <p>
                          <div class="ms-collapse no-margin" id="accordion{{$announcement['id']}}" role="tablist" aria-multiselectable="true">
                            <div class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{$announcement['id']}}" href="#collapse{{$announcement['id']}}" aria-expanded="false" aria-controls="collapse{{$announcement['id']}}">
                                 Comments({{count($announcement['announcement_comments'])}})
                              </a>
                              <div id="collapse{{$announcement['id']}}" class="card-collapse collapse" role="tabpanel">
                                @foreach($announcement['announcement_comments'] as $announcement_comment)
                                <div class="dq-padding-comment">
                                  <span>
                                    <div class="row">
                                      <img src="{{avatar($announcement_comment->photo)}}" class="dq-image-user dq-ml">
                                      <div class="col-md-8">
                                        <div>
                                          <span>
                                            {{$announcement_comment->name}}
                                          </span>
                                          .
                                          <span>{{$announcement_comment->created_at}}</span>
                                          {{-- .
                                          <span>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Report Abuse">
                                              <i class="fa fa-flag"></i>
                                            </a>
                                          </span> --}}
                                        </div>
                                        <div>
                                          <span>
                                            {{$announcement_comment->comment}}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </span>
                                </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </p>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="modal" id="modalSetEditorDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <textarea name="editorDescription" id="editorDescription"></textarea>
            <input type="button" id="saveEditorDescription" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalSetEditorGoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <textarea name="editorGoal" id="editorGoal"></textarea>
            <input type="button" id="saveEditorGoal" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalSetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <img class="img-fluid" src="{{asset_url(course_image_medium($course->image))}}" alt="{{$course->title}}">
            <input type="file" id="editorImage" value="">
            <input type="button" id="saveEditorImage" value="Save" class="btn btn-primary btn-raised">
          </div>
      </div>
  </div>

  <div class="modal" id="modalEditSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Edit Section</h3>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control" id="setSectionTitle">
              </div>

              <div class="form-group">
                <label for="">Description</label>
                <textarea id="setSectionDescription" class="form-control" rows="6"></textarea>
              </div>
              <input type="hidden" id="setSectionId">
              <input type="button" id="saveEditSection" value="Save" class="btn btn-primary btn-raised">
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="modal" id="modalAddContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title" id="contentForm"></h3>
            </div>
            <div class="panel-body">
              <form id="formAddContent" action="" method="post">
                {{csrf_field()}}

                <div class="form-group">
                  <label for="title">Title</label>
                  <input placeholder="Title" type="text" class="form-control" id="contentTitle" name="title" value="" required>
                </div>

                <div class="form-group">
                  <label for="title">Description</label>
                  <textarea name="description" id="contentDescription"></textarea>
                </div>

                <div class="form-group">
                  <label for="title">Type Content</label>
                  <select class="form-control selectpicker" id="contentTypeContent" name="type_content" required>
                    <option value="">Choose Type Content</option>
                    <option value="text">Text</option>
                    <option value="video">Video</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="title">File (Document / Video)</label>
                  <input type="file" id="file" class="form-control">
                  <input type="hidden" id="path_file" name="path_file">
                  <input type="hidden" id="name_file" name="name_file">
                </div>

                <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                <div id="container"></div>

                <div class="progress" style="display:none">
                  <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only">0%</span>
                  </div>
                </div>
                <div id="uploadedMessage"></div>

                <div class="form-group">
                  <label for="title">Sequence</label>
                  <input placeholder="Sequence" id="contentSequence" type="text" class="form-control" name="sequence" value="" required>
                </div>

                <input type="hidden" name="id" id="contentId" value="">
                <div class="form-group">
                  {{  Form::submit("Save Content" , array('class' => 'btn btn-primary', 'name' => 'button', 'id' => 'saveContent')) }}
                </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="modal" id="modalAddQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title" id="quizForm"></h3>
            </div>
            <div class="panel-body">
              <form id="formAddQuiz" action="" method="post">
                {{csrf_field()}}

                <div class="form-group">
                  <label for="name">Name</label>
                  <input placeholder="Name" type="text" class="form-control" name="name" id="quizname" required>
                </div>

                <div class="form-group">
                  <label for="title">Description</label>
                  <textarea name="description" id="Quizdescription"></textarea>
                </div>
                <div class="form-group">
                  <label for="title">Shuffle</label><br>
                  <input type="radio" name="shuffle" checked value="0">No <br>
                  <input type="radio" name="shuffle" value="1">Yes
                </div>
                <div class="form-group">
                  <label for="name">Time Start</label>
                  <input placeholder="Time Start" type="text" class="form-control form_datetime" name="time_start" id="Quiztime_start" required>
                </div>
                <div class="form-group">
                  <label for="name">Time End</label>
                  <input placeholder="Time End" type="text" class="form-control form_datetime" name="time_end" id="Quiztime_end" required>
                </div>

                <input type="hidden" name="id" id="QuizId" value="">
                <div class="form-group">
                  {{  Form::submit("Save Quiz" , array('class' => 'btn btn-primary', 'name' => 'button', 'id' => 'saveQuiz')) }}
                </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
    </div>
  </div>

@endsection

@push('script')
  <script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>

  <script>
    $.fn.editable.defaults.mode = 'inline';

    $(document).ready(function() {

      $('#CourseTitle').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'title',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CoursePrice').editable({
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'price',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseLevel').editable({
        value: {{$course->level_id}},
        source: [
          @php $course_levels = DB::table('course_levels')->get(); @endphp
          @foreach($course_levels as $course_level)
            {value: {{$course_level->id}}, text: '{{$course_level->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_level_course',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

      $('#CourseCategory').editable({
        value: {{$course->id_category}},
        source: [
          @php $categories = DB::table('categories')->get(); @endphp
          @foreach($categories as $category)
            {value: {{$category->id}}, text: '{{$category->title}}'},
          @endforeach
        ],
        success: function(response, newValue) {
          $.ajax({
            url : '/course/editable/{{$course->id}}',
            type : 'post',
            data : {
              _token : '{{csrf_token()}}',
              name : 'id_category',
              value : newValue,
            },
            success : function(data){
              // console.log(data);
            }
          })
        }
      });

    });
</script>

{{-- CKEDITOR --}}
<script src="{{url('ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
  CKEDITOR.replace('editorDescription').setData('{!! $course->description !!}')
  CKEDITOR.replace('editorGoal').setData('{!! $course->goal !!}')
	CKEDITOR.replace('contentDescription');
	CKEDITOR.replace('Quizdescription');
</script>
{{-- CKEDITOR --}}

<script type="text/javascript">
  $("#saveEditorDescription").click(function(){
    var new_description = CKEDITOR.instances.editorDescription.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'description',
        value : new_description,
      },
      success : function(data){
        $("#modalSetEditorDescription").modal('hide')
        $("#recentDescription").html(new_description);
      }
    })
  })

  $("#saveEditorGoal").click(function(){
    var new_goal = CKEDITOR.instances.editorGoal.getData();
    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : {
        _token : '{{csrf_token()}}',
        name : 'goal',
        value : new_goal,
      },
      success : function(data){
        $("#modalSetEditorGoal").modal('hide')
        $("#recentGoal").html(new_goal);
      }
    })
  })

  var fileName = '';
  $("#editorImage").change(function(e){
    fileName = e.target.files[0];
  })

  $("#saveEditorImage").click(function(e){
    var formData = new FormData();
    formData.append('value', fileName);
    formData.append('name', 'image');

    $.ajax({
      url : '/course/editable/{{$course->id}}',
      type : 'post',
      data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,
      headers: {
	       'X-CSRF-TOKEN': '{{csrf_token()}}'
	    },
      success : function(data){
        location.reload();
      }
    })
  })
</script>

{{-- add new section --}}
<script type="text/javascript">
  $("#add_new_section").click(function(){

    $("#section_row").val(parseInt($("#section_row").val())+1)
    var section_add_row = $("#section_row").val();

    $("#place_new_section").append(
      '<div class="panel panel-default" id="section_row_'+section_add_row+'">'+
        '<div class="panel-heading">'+
          '<h3 class="panel-title">New Section</h3>'+
        '</div>'+
        '<div class="panel-body">'+
          '<div class="form-group">'+
            '<input placeholder="Section Title" type="text" class="form-control" id="new_section_title_'+section_add_row+'" value="" required>'+
          '</div>'+
            '<button type="button" id="save_new_section" data-id="'+section_add_row+'" class="btn btn-primary">Save Section</button> '+
            '<button type="button" id="remove_new_section" data-id="'+section_add_row+'" class="btn btn-danger">Remove Section</button>'+
        '</div>'+
      '</div>'
    );
  });
</script>
{{-- add new section --}}

{{-- save new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#save_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var new_section_title = $("#new_section_title_"+data_id).val();

        var id_course = '{{$course->id}}';
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('course/save_section')}}",
          type : "POST",
          data : {
            title : new_section_title,
            id_course : id_course,
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save new section --}}

{{-- remove new section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#remove_new_section',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        $("#section_row_"+data_id).remove();
    });
  });
</script>
{{-- remove new section --}}

{{-- set edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#EditSection',function(e){
      e.preventDefault();
        var data_id = $(this).attr('data-id');
        var data_title = $(this).attr('data-title');
        var data_description = $(this).attr('data-description');

        $("#setSectionId").val(data_id)
        $("#setSectionTitle").val(data_title)
        $("#setSectionDescription").val(data_description)
        $("#modalEditSection").modal('show');
    });
  });
</script>
{{-- set edit section --}}

{{-- save edit section --}}
<script type="text/javascript">
  $(function(){
    $(document).on('click','#saveEditSection',function(e){
      e.preventDefault();
        var token = '{{ csrf_token() }}';
        $.ajax({
          url : "{{url('section/edit-section')}}",
          type : "POST",
          data : {
            title : $("#setSectionTitle").val(),
            description : $("#setSectionDescription").val(),
            id : $("#setSectionId").val(),
            _token: token
          },
          success : function(result){
            location.reload();
          },
        });
    });
  });
</script>
{{-- save edit section --}}


<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>

{{-- set sectionid on add content --}}
<script type="text/javascript">
  var data_section = '';
  $(function(){
    $(document).on('click','#AddContent',function(e){
      e.preventDefault();
        $("#contentForm").html('Add Content');
        data_section = $(this).attr('data-section');
        $("#formAddContent").attr('action', '/course/content/create_action/'+data_section)
        $("#modalAddContent").modal('show');

        // Custom example logic
        var path_file = $("#path_file").val();
        var name_file = $("#name_file").val();
        var section_id = data_section;
        // $("#saveContent").prop('disabled', true);

        var uploader = new plupload.Uploader({
          runtimes : 'html5,flash,silverlight,html4',
          browse_button : 'file', // you can pass an id...
          container: document.getElementById('container'), // ... or DOM Element itself
          url : '/course/content/upload/'+section_id,
          flash_swf_url : '/plupload/Moxie.swf',
          silverlight_xap_url : '/plupload/Moxie.xap',
          chunk_size: '1mb',
          dragdrop: true,
          headers: {
             'X-CSRF-TOKEN': '{{csrf_token()}}'
          },
          filters : {
            mime_types: [
              {title : "Document files", extensions : "docs,xlsx,pptx"},
              {title : "Video files", extensions : "mp4,webm"},
            ]
          },

          init: {
            PostInit: function() {
              document.getElementById('filelist').innerHTML = '';
            },

            FilesAdded: function(up, files) {
              plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                uploader.start();
                return false;
              });
            },

            UploadProgress: function(up, file) {
              // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
              $('.progress').show();
              $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
              $("#uploadedMessage").html('Upload dalam proses...');
            },

            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                // console.log('[FileUploaded] File:', file, "Info:", info);
                var response = JSON.parse(info.response);
                // console.log(response.result.original.path);
                path_file = $("#path_file").val(response.result.original.path);
                name_file = $("#name_file").val(response.result.original.name);
            },

            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                // console.log('[ChunkUploaded] File:', file, "Info:", info);
            },

            UploadComplete: function(up, files) {
                // Called when all files are either uploaded or failed
                // console.log('[UploadComplete]');
                // $("#saveContent").prop('disabled', false);
                $("#uploadedMessage").html('Upload selesai');
            },

            Error: function(up, err) {
              document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
            }
          }
        });

        uploader.init();
    });
  });
</script>
{{-- set sectionid on add content --}}

<script>
// edit content
$(function(){
  $(document).on('click','#EditContent',function(e){
    e.preventDefault();
    $("#contentForm").html('Edit Content');
    $("#formAddContent").attr('action', '/course/content/update_action');
    var data_id = $(this).attr('data-id');
    var data_title = $(this).attr('data-title');
    var data_description = $(this).attr('data-description');
    var data_type_content = $(this).attr('data-type-content');
    var data_sequence = $(this).attr('data-sequence');
    var data_section = $(this).attr('data-section');
    $("#contentId").val(data_id);
    $("#contentTitle").val(data_title);
    CKEDITOR.instances['contentDescription'].setData(data_description)
    $("#contentTypeContent").val(data_type_content).change();
    $("#contentSequence").val(data_sequence);
    $("#modalAddContent").modal('show');

    // Custom example logic
    var path_file = $("#path_file").val();
    var name_file = $("#name_file").val();
    var section_id = data_section;
    // $("#saveContent").prop('disabled', true);

    var uploader = new plupload.Uploader({
      runtimes : 'html5,flash,silverlight,html4',
      browse_button : 'file', // you can pass an id...
      container: document.getElementById('container'), // ... or DOM Element itself
      url : '/course/content/upload/'+section_id,
      flash_swf_url : '/plupload/Moxie.swf',
      silverlight_xap_url : '/plupload/Moxie.xap',
      chunk_size: '1mb',
      dragdrop: true,
      headers: {
         'X-CSRF-TOKEN': '{{csrf_token()}}'
      },
      filters : {
        mime_types: [
          {title : "Document files", extensions : "docs,xlsx,pptx"},
          {title : "Video files", extensions : "mp4,webm"},
        ]
      },

      init: {
        PostInit: function() {
          document.getElementById('filelist').innerHTML = '';
        },

        FilesAdded: function(up, files) {
          plupload.each(files, function(file) {
            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            uploader.start();
            return false;
          });
        },

        UploadProgress: function(up, file) {
          // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
          $('.progress').show();
          $('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
          $("#uploadedMessage").html('Upload dalam proses...');
        },

        FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            // console.log('[FileUploaded] File:', file, "Info:", info);
            var response = JSON.parse(info.response);
            // console.log(response.result.original.path);
            path_file = $("#path_file").val(response.result.original.path);
            name_file = $("#name_file").val(response.result.original.name);
        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
            // $("#saveContent").prop('disabled', false);
            $("#uploadedMessage").html('Upload selesai');
        },

        Error: function(up, err) {
          document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
        }
      }
    });

    uploader.init();

  })
})
// edit content
</script>

{{-- datetime picker --}}
<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript">
  $('.form_datetime').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
  });
</script>
{{-- datetime picker --}}

<script>
// add quiz
  var data_section = '';
  $(function(){
    $(document).on('click','#AddQuiz',function(e){
      $("#quizForm").html('Add Quiz');
      data_section = $(this).attr('data-section');
      $("#formAddQuiz").attr('action', '/course/quiz/create_action/'+data_section)
      $("#modalAddQuiz").modal('show');
    })
  })
  // add quiz
</script>

@endpush
