@extends('layouts.app')

@section('level_ladder', 'Level Ladder')

@section('content')

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#info">@lang('front.level_up.info')</a></li>
    <li class="active"><a data-toggle="tab" href="#ladder">@lang('front.level_up.ladder')</a></li>
</ul>

<div class="tab-content">
    <br>
    <div id="info" class="tab-pane fade in">
        <div class="container">
            <div class="row">
                @foreach($course_leveling as $data_level)
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course_levelup_img)}}" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title text-center">{{$data_level->points_required}}</h4>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="ladder" class="tab-pane fade in">
        <div class="container">
            <table id="table-ladder" class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>@lang('front.level_up.ladder')</th>
                        <th>Level</th>
                        <th>@lang('front.level_up.participant')</th>
                        <th>Total</th>
                        <th>@lang('front.level_up.progress')</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($course_user as $no => $data_user)
                    <tr>
                        <td>{{$no+1}}</td>
                        <td>{{$data_user->level}}</td>
                        <td>{{$data_user->user_name}}</td>
                        <td>{{$data_user->level_point}}</td>
                        
                        @php
                            $get_current_level = get_current_level($data_user->course_id, $data_user->level);
                            $get_next_level = get_next_level($data_user->course_id, $data_user->level);

                            $current_point_level = ($data_user->level_point - $get_current_level->points_required);

                            if($get_current_level->level == $get_next_level->level)
                            {
                                $points_togo = 0;

                                $points_difference = 0;

                                $exp_percentage = 100;
                            }

                            else

                            {
                                $points_difference = $get_next_level->points_required - $get_current_level->points_required;

                                $exp_percentage = number_format($current_point_level / $points_difference * 100, 2);

                                $points_togo = ($get_next_level->points_required - $data_user->level_point);
                            }
                        @endphp

                        <td>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$exp_percentage}}"
                                aria-valuemin="0" aria-valuemax="100" style="width:{{$exp_percentage}}%">
                                    {{$exp_percentage}}%
                                </div>
                            </div>
                            <p>{{$points_togo}}xp to go</p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection

@push('script')

<script>
    $(function () {
        $('#table-ladder').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        });
    });
</script>

@endpush