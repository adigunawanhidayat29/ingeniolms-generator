@extends('layouts.app')

@section('title', 'Judul Grup')

@push('style')
  <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <style>
    .nav-tabs-ver-container .nav-tabs-ver {
      padding: 0;
      margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
      display: none;
    }
  </style>

  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/my-course">Kelas Saya</a></li>
          <li>{{$discussion->title}}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <!-- <div class="col-md-3 nav-tabs-ver-container">
          <a class="btn btn-block btn-raised btn-default no-shadow mt-0 mb-2" href="/instructor/groups/detail"><i class="zmdi zmdi-arrow-left"></i> Kembali</a></li>
          <div class="card card-success-inverse no-shadow">
            <div class="card-block">
              <p class="fs-14 fw-600 mb-0">Access Code</p>
              <p class="fs-20 fw-300 text-uppercase" style="letter-spacing: 2px;">qwert-yuiop</p>
            </div>
          </div>
        </div> -->
        <div class="col-md-12">
          <div class="grade" style="text-align:right">
            <a class="btn btn-raised btn-primary no-shadow" style="width:auto;text-align:right" href="#" data-toggle="modal" data-target="#createDiscussion"><i class="fa fa-users"></i> All Grade Siswa</a>
            <a class="btn btn-raised btn-primary no-shadow" style="width:170px;text-align:right" href="{{url('course/content/discussion/manage/grade', $discussion->id)}}"><i class="fa fa-users"></i> Grade Siswa</a>
          </div>
          <div class="mb-2">
            <h3 class="headline headline-sm mt-0">{{$discussion->title}}</h3>
            <p class="fs-14" style="color: #999;">{{date('d M Y',strtotime($discussion->created_at))}}</p>
            <p class="fs-16 mb-0">{!!$discussion->description!!}</p>
          </div>

          <div class="card card-groups">
            <div class="card-block">
              <form action="{{url('course/content/discussion/submit/'.$discussion->id)}}" method="post">
                {{csrf_field()}}
                <div class="form-group mt-0">
                  <label class="control-label">Tulis Komentar</label>
                  <textarea name="body" id="answer" cols="30" rows="1" class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-raised btn-primary">Kirim</button>
              </form>
            </div>
          </div>

          @foreach($discussion_list->where('level_1', null) as $item)
          <div class="card card-groups dropdown">
            @if($item->user_id == Auth::user()->id)
            <div class="groups-dropdown">
              <div class="groups-dropdown-group">
                <i class="fa fa-ellipsis-v"></i>
                <ul class="groups-dropdown-menu">
                  <li>
                    <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$item->id}}').submit()">
                      <i class="fa fa-trash"></i> Hapus
                    </a>
                    <form action="{{url('/course/content/discussion/delete/'.$item->id)}}" id="form-koment-delete-{{$item->id}}" method="post" style="display:none;">
                      {{csrf_field()}}
                    </form>
                  </li>
                </ul>
              </div>
            </div>
            @endif
            <div class="card-block">
              <a class="groups-title" href="#">{{$item->user->name}}</a>
              <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($item->created_at))}}&nbsp;&middot;&nbsp;</p>
              <p class="groups-description">{!!$item->body!!}</p>
              @if($item->file)
              <?php $files = json_decode($item->file); ?>
              <p class="fs-14" style="color: #999;">Attachment</p>
                @foreach($files as $file)
                <?php
                    $ext = explode('.',$file);
                    $ext = $ext[count($ext)-1];
                    $img = asset('files/learn/discussion/'.$file);
                    if($ext == 'pdf'){
                      $img = asset('images/pdf.png');
                    }elseif ($ext == 'docx') {
                      $img = asset('images/word.png');
                    }

                 ?>
                <div class="img-attachment">
                  <img src="{{$img}}" style="width:50px;margin-right:20px;">
                  <a class="groups-title" href="{{asset('files/learn/discussion/'.$file)}}" download="{{substr($file,strlen($item->user->id)+12,(strlen($file)-(strlen($item->user->id)+12)))}}">{{substr($file,strlen($item->user->id)+12,(strlen($file)-(strlen($item->user->id)+12)))}}</a>
                </div>
                <br>
                @endforeach
              @endif
              <div class="reply-comment">
                <hr style="border-color:#bbb">
                <p class="reply-btn" style="cursor:pointer">Reply</p>
                <div class="text-reply">
                  <form method="POST" action="{{url('/course/content/discussion/submit/'.$discussion->id)}}" accept-charset="UTF-8" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input name="parents" type="hidden" value="{{$item->id}}">
                    {{ Form::open(array('url' => '/course/content/discussion/submit/'.$discussion->id, 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                    <textarea name="body" id="discussion-reply-{{$item->id}}" class="form-control discussion-reply" required="required"></textarea>
                    <input class="btn btn-primary btn-raised" name="button" type="submit" value="Submit Komentar">
                  </form>
                </div>
                <div class="reply_1-page">
                  @foreach($item->reply_1 as $reply_1)
                  <div class="card card-groups dropdown" style="box-shadow: none;margin:0px;">
                    @if($reply_1->user_id == Auth::user()->id)
                    <div class="groups-dropdown">
                      <div class="groups-dropdown-group">
                        <i class="fa fa-ellipsis-v"></i>
                        <ul class="groups-dropdown-menu">
                          <!-- <li>
                            <a class="groups-dropdown-item" href="#">
                              <i class="fa fa-pencil"></i> Ubah
                            </a>
                          </li> -->
                          <li>
                            <a class="groups-dropdown-item" href="#" onclick="$('#form-koment-delete-{{$reply_1->id}}').submit()">
                              <i class="fa fa-trash"></i> Hapus
                            </a>
                            <form action="{{url('/course/content/discussion/delete/'.$reply_1->id)}}" id="form-koment-delete-{{$reply_1->id}}" method="post" style="display:none;">
                              {{csrf_field()}}
                            </form>
                          </li>
                        </ul>
                      </div>
                    </div>
                    @endif
                    <div class="card-block" style="padding:5px 2rem">
                      <a class="groups-title" href="#">{{$reply_1->user->name}}</a>
                      <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($reply_1->created_at))}}&nbsp;&middot;&nbsp;</p>
                      <p class="groups-description">{!!$reply_1->body!!}</p>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="createDiscussion" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
    <div class="modal-dialog animated zoomIn animated-3x modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Nilai Siswa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="table-responsive table-cs">
            <table id="example-1" class="display" style="width:100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Siswa</th>
                  <th class="text-center">Nilai</th>
                  <th>Catatan</th>
                  <th class="text-center"></th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; ?>
                @foreach($discussion_user as $index => $siswa)
                @if(!in_array($siswa->user_id, explode(',', $course->id_author)))
                <?php
                  $grade = $siswa->grade($discussion->id);
                  $i++;
                ?>
                <tr>
                  <td>{{$i}}</td>
                  <td class="user-name"><a href="{{url('course/content/discussion/manage/grade/'.$discussion->id.'?siswa='.$siswa->id)}}">{{$siswa->user->name}}</a></td>
                  <td class="text-center nilai">
                    <p>{{$grade ? $grade->grade : 0}}</p>
                    <input type="number" class="grade-nilai" style="width: 50px;" name="nilai" value="{{$grade ? $grade->grade : 0}}">
                  </td>
                  <td class="catatan">
                    <p>{{$grade ? $grade->comment : 'tidak ada'}}</p>
                    <textarea name="catatan" class="grade-catatan">{{$grade ? $grade->comment : 'tidak ada'}}</textarea>
                  </td>
                  <td class="page-grade">
                    <button class="btn btn-raised btn-primary btn-nilai">Beri Nilai</button>
                    <div class="edit-nilai-page">
                      <input type="hidden" class="user-id" value="{{$siswa->user_id}}">
                      <button class="btn btn-raised btn-info btn-nilai-save"><i class="fa fa-check"></i></button>
                      <button class="btn btn-raised btn-danger btn-nilai-cancel"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>
  <script type="text/javascript">
    $(".text-reply").hide();
    $(".reply-btn").click(function(){
      $(".text-reply").hide();
      $(this).siblings(".text-reply").show();

    });
  </script>
  <script>
    var token = '{{ csrf_token() }}';

    $(".grade-nilai").hide();
    $(".grade-catatan").hide();
    $(".edit-nilai-page").hide();
    $('#example-1').DataTable({
      language: { search: '', searchPlaceholder: "Search ..." },
    });

    $(".btn-nilai-cancel").click(function(){
      var value = parseInt($(this).parents('.page-grade').siblings('.nilai').children('p').text());
      var comment = $(this).parents('.page-grade').siblings('.catatan').children('p').text();
      if(comment == 'tidak ada'){
        comment = null;
      }
      $(this).parents('.page-grade').siblings('.nilai').children('input').val(value);
      $(this).parents('.page-grade').siblings('.catatan').children('textarea').val(comment);


      $(this).parents('.edit-nilai-page').hide();
      $(this).parents('.edit-nilai-page').siblings().show();
      $(this).parents('.page-grade').siblings('.nilai').children('p').show();
      $(this).parents('.page-grade').siblings('.nilai').children('input').hide();
      $(this).parents('.page-grade').siblings('.catatan').children('p').show();
      $(this).parents('.page-grade').siblings('.catatan').children('textarea').hide();
      // $(this).siblings('p').replaceWith($('<input name="nilai" class="form-control" value="'+inpt+'"></input>'));
    });

    $(".btn-nilai-save").click(function(){
      var data_btn = $(this);
      var value = $(this).parents('.page-grade').siblings('.nilai').children('input').val();
      var comment = $(this).parents('.page-grade').siblings('.catatan').children('textarea').val();

      var name = $(this).parents('.page-grade').siblings('.user-name').text();
      var id_siswa = $(this).siblings('.user-id').val();
      $.ajax({
        url : "{{url('course/content/discussion/manage/grade/nilai/')}}/"+id_siswa+"/"+"{{$discussion->id}}",
        type : "POST",
        data : {
          comment : comment,
          nilai : value,
          _token: token
        },
        success : function(result){
          data_btn.parents('.page-grade').siblings('.nilai').children('p').text(value);
          data_btn.parents('.page-grade').siblings('.catatan').children('p').text(comment);
          alert('Data nilai untuk siswa '+name+' sudah diubah.');
        },
      });

      $(this).parents('.edit-nilai-page').hide();
      $(this).parents('.edit-nilai-page').siblings().show();
      $(this).parents('.page-grade').siblings('.nilai').children('p').show();
      $(this).parents('.page-grade').siblings('.nilai').children('input').hide();
      $(this).parents('.page-grade').siblings('.catatan').children('p').show();
      $(this).parents('.page-grade').siblings('.catatan').children('textarea').hide();
      // $(this).siblings('p').replaceWith($('<input name="nilai" class="form-control" value="'+inpt+'"></input>'));
    });

    $(".btn-nilai").click(function(){
      $(this).hide();
      $(this).siblings().show();
      $(this).parents('.page-grade').siblings('.nilai').children('p').hide();
      $(this).parents('.page-grade').siblings('.nilai').children('input').show();
      $(this).parents('.page-grade').siblings('.catatan').children('p').hide();
      $(this).parents('.page-grade').siblings('.catatan').children('textarea').show();
      // $(this).siblings('p').replaceWith($('<input name="nilai" class="form-control" value="'+inpt+'"></input>'));
    });
  </script>
@endpush
