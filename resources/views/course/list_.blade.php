@extends('layouts.app')

@section('title', 'Course Dashboard')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3>Manage Your Courses</h3>
            <a href="{{url('course/create')}}" class="btn btn-primary btn-raised">Create Course</a><br><br>
					</div>
					<div class="box-body">
            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="data" style="width:1000px">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Image</th>
										<th>Publish</th>
										<th>Publish Meeting</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>

	</div>

@endsection

@push('script')
	<script src="{{url('js/jquery.dataTables.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script> <!-- Datatable -->
	<script src="{{url('js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->

	<script type="text/javascript">
	$(function(){
		var asset_url = '{{asset_url()}}';
		$("#data").DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ url("course/serverside") }}',
			columns: [
				{ data: 'title', name: 'title' },
				{
					"data": "image",
					"render": function(data, type, row) {
						return '<img src="'+asset_url+data+'" width=\"100\"//>';
					},
				},
				{ data: 'status', name: 'status' },
				{ data: 'publish_meeting', name: 'publish_meeting' },
				{ data: 'action', 'searchable': false, 'orderable':false }
			]
		});
	});
	</script>

	{{-- switchjs --}}

	{{-- switchjs --}}
@endpush
