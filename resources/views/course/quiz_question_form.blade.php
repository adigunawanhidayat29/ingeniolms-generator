@extends('layouts.app')

@section('title')
	{{ 'Tambah Pertanyaan ' . $quiz->name }}
@endsection

@push('style')
	<style media="screen">
		.feedback-form textarea{
			background: white!important;
			padding: 10px;
			border: 1px solid #e3e3e3 !important;
			margin-top: 20px;
		}

		.feedback-form{
			display: none;
		}

		.activated{
			display: block;
		}
	</style>

	<style>
		#new_row_answer {
			margin-top: 28px;
		}

		.quiz-form-wrap {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			-ms-flex-align: center!important;
			align-items: center!important;
			margin-right: -15px;
			margin-left: -15px;
		}

		.quiz-form-wrap.no-check,
		.quiz-form-wrap.no-check.no-editor {
			justify-content: flex-end;
		}

		.quiz-form-wrap .quiz-check {
			width: 5%; 
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap .quiz-check .addon-check {
			position: relative;
			display: inline-block;
			width: 20px;
			height: 20px;
			border: 2px solid rgba(0,0,0,.54);
			border-radius: 2px;
			cursor: pointer;
			overflow: hidden;
			margin-top: 10px;
			z-index: 1;
		}

		.quiz-form-wrap .quiz-form {
			width: 86%; 
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap.no-check .quiz-form {
			width: 91%;
		}

		.quiz-form-wrap.no-check.no-editor .quiz-form {
			width: 94%;
		}

		.quiz-form-wrap .quiz-form .form-group label.control-label {
			margin-top: 0;
		}

		.quiz-form-wrap .quiz-form .form-group .form-control {
			margin-bottom: 0;
		}

		.quiz-form-wrap .quiz-action {
			width: 9%; 
			display: flex;
			align-items: center;
			justify-content: space-between;
			padding-left: 15px; 
			padding-right: 15px;
		}

		.quiz-form-wrap.no-check.no-editor .quiz-action {
			width: 6%;
			justify-content: flex-end;
		}

		@media (max-width: 1199px) {
			.quiz-form-wrap .quiz-check {
				width: 6%;
			}

			.quiz-form-wrap .quiz-form {
				width: 83%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 89%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 93%;
			}

			.quiz-form-wrap .quiz-action {
				width: 11%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 7%;
			}
		}

		@media (max-width: 991px) {
			.quiz-form-wrap .quiz-check {
				width: 8%;
			}

			.quiz-form-wrap .quiz-form {
				width: 77%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 85%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 90%;
			}

			.quiz-form-wrap .quiz-action {
				width: 15%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 10%;
			}
		}

		@media (max-width: 767px) {
			.quiz-form-wrap .quiz-check {
				width: 11%;
			}

			.quiz-form-wrap .quiz-form {
				width: 68%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 79%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 87%;
			}

			.quiz-form-wrap .quiz-action {
				width: 21%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 13%;
			}
		}

		@media (max-width: 480px) {
			.quiz-form-wrap .quiz-check {
				width: 12%;
			}

			.quiz-form-wrap .quiz-form {
				width: 60%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 72%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 81%;
			}

			.quiz-form-wrap .quiz-action {
				width: 28%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 19%;
			}
		}

		@media (max-width: 380px) {
			.quiz-form-wrap .quiz-check {
				width: 12%;
			}

			.quiz-form-wrap .quiz-form {
				width: 56%;
			}

			.quiz-form-wrap.no-check .quiz-form {
				width: 68%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-form {
				width: 78%;
			}

			.quiz-form-wrap .quiz-action {
				width: 32%;
			}

			.quiz-form-wrap.no-check.no-editor .quiz-action {
				width: 22%;
			}
		}
	</style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Pertanyaan <span>Baru</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
					<li><a href="/course/dashboard">Kelola Kelas</a></li>
					<li><a href="/course/preview/{{$section->course->id}}">{{$section->course->title}}</a></li>
					<li><a href="/course/quiz/question/manage/{{$quiz->id}}">{{$quiz->name}}</a></li>
					<li>Pertanyaan Baru</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">

			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">

									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										<input type="hidden" name="quiz_type_id" value="{{Request::get('quiz_type')}}">

										<div class="form-group">
											@if(Request::get('quiz_type') != '4')
												<label for="title">Pertanyaan</label>
											@endif
											<textarea name="question" id="question">{{ $question }}</textarea>
											<div class="feedback-form">
												<textarea name="feedback" class="form-control" placeholder="Feedback" name="feedback_question"></textarea>
											</div>
										</div>

										@if(Request::get('quiz_type') != '4')
											<div class="form-group">
												<label for="title">Poin</label>
												<input type="number" placeholder="poin" class="form-control" name="weight" value="{{$weight}}">
											</div>
										@endif

										@if(Request::get('quiz_type') == '1' || Request::get('quiz_type') == '2' || Request::get('quiz_type') == '7')
											<div id="new_row_answer">
												@php
													if(Request::get('quiz_type') == '1'){ // multiple choice
														$iType = 4;
													}elseif(Request::get('quiz_type') == '7'){ // skala likert
														$iType = 3;
													}else{
														$iType = 2; // true or false
													}
												@endphp
												@for($i=1; $i<=$iType; $i++)
													<div class="card-sm mb-2" id="row_answer{{$i}}">
														<div class="card-block bg-white">
															@if(Request::get('quiz_type') == '7')
																<div class="quiz-form-wrap no-check">
															@else
																<div class="quiz-form-wrap">
															@endif
																@if(Request::get('quiz_type') != '7')
																	<div class="quiz-check">
																		<div class="checkbox">
																			<label class="m-0" title="Tentukan jawaban benar">
																				<input type="hidden" value="0" id="correctAnswertext{{$i}}" name="answer_correct[]">
																				<input type="checkbox" name="" id="correctAnswerCheck" data-increment="{{$i}}" value="1">
																			</label>
																		</div>
																	</div>
																@endif
																<div class="quiz-form">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukkan pilihan</label>
																		<input autocomplete="off" type="text" name="answer[]" id="text_answer{{$i}}" value="{{Request::get('quiz_type') == '7' ? ( $i == 1 ? 'Setuju' : ( $i == 2 ? 'Netral' : 'Tidak Setuju')  ) : ''}}" class="form-control">
																		<div id="text_answer_editor_place{{$i}}"></div>
																	</div>
																</div>
																<div class="quiz-action">
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="{{$i}}" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="{{$i}}" title="Hapus"><i class="zmdi zmdi-close"></i></a>
																</div>
															</div>

															<div class="feedback-form">
																<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>
															</div>
														</div>
													</div>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}">
											</div>
											@if(Request::get('quiz_type') == '1' || Request::get('quiz_type') == '7')
												<button id="add_new_answer" type="button" class="btn btn-raised">Tambah Pilihan</button>
											@endif
										@endif

										@if(Request::get('quiz_type') == '3')
											<div id="new_row_answer">
												@for($i=0; $i<=4; $i++)
													<div class="card-sm mb-2" id="row_answer{{$i}}">
														<div class="card-block bg-white">
															<div class="quiz-form-wrap no-check">
																<div class="quiz-form">
																	<div class="form-group no-m">
																		<label class="control-label" for="focusedInput1">Masukkan pilihan</label>
																		<input autocomplete="off" type="text" name="answer[]" id="text_answer{{$i}}" value="" class="form-control">
																		<input type="hidden" name="answer_ordering[]" value="{{$i}}">
																		<div id="text_answer_editor_place{{$i}}"></div>
																	</div>
																</div>
																<div class="quiz-action">
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="{{$i}}" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="{{$i}}" title="Hapus"><i class="zmdi zmdi-close"></i></a>
																</div>
															</div>
															<div class="feedback-form">
																<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>
															</div>
														</div>
													</div>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}">
												
												{{-- @for($i=0; $i<=4; $i++)
													<div class="row" id="row_answer{{$i}}">
														<div class="col-md-12">
															<div class="form-group">
																<label class="control-label" for="focusedInput1">Masukkan pilihan</label>
																<input autocomplete="off" type="text" name="answer[]" id="text_answer{{$i}}" value="" class="form-control">
																<input type="hidden" name="answer_ordering[]" value="{{$i}}">
																<div id="text_answer_editor_place{{$i}}"></div>
																<a href="#/" id="remove_row_answer" data-id="{{$i}}">hapus</a>
																<a href="#/" class="answer_editor" data-id="{{$i}}">gunakan editor</a>
																<div class="feedback-form">
																	<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>
																</div>
															</div>
														</div>
													</div>
													<br>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}"> --}}
											</div>
											<button id="add_new_answer_ordering" type="button" class="btn btn-raised">Tambah Pilihan</button>
										@endif

										@if(Request::get('quiz_type') == '6')
											<div id="new_row_answer">
												@for($i=1; $i<=1; $i++)
													<div class="card-sm mb-2" id="row_answer{{$i}}">
														<div class="card-block bg-white">
															<div class="quiz-form-wrap no-check">
																<div class="quiz-form">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukkan jawaban</label>
																		<input autocomplete="off" type="text" name="answer[]" id="text_answer{{$i}}" value="" class="form-control">
																		<input type="hidden" value="1" name="answer_correct[]">
																	</div>
																</div>
																<div class="quiz-action">
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="{{$i}}" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>
																	<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="{{$i}}" title="Hapus"><i class="zmdi zmdi-close"></i></a>
																</div>
															</div>
															<div class="feedback-form">
																<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>
															</div>

															{{-- <div class="row">
																<div class="col-md-12">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukkan jawaban</label>
																		<input autocomplete="off" type="text" name="answer[]" id="text_answer{{$i}}" value="" class="form-control">
																		<a href="#/" id="remove_row_answer" data-id="{{$i}}">hapus</a>
																		<input type="hidden" value="1" name="answer_correct[]">
																		<div class="feedback-form">
																			<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>
																		</div>
																	</div>
																</div>
															</div> --}}
														</div>
													</div>
												@endfor
												<input type="hidden" id="choice_increments" value="{{$i}}">
											</div>
											<button id="add_new_answer_short_answer" type="button" class="btn btn-raised">Tambah Pilihan</button>
										@endif

										<hr>

										{{-- <input type="checkbox" class="feedback-check" name="feedback_required" value="1"> Feedback --}}
										<div class="checkbox">
											<label class="m-0" style="display: inline-flex; align-items: center;">
												<input type="checkbox" class="feedback-check" name="feedback_required" value="1"> Feedback
											</label>
										</div>

										<div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
												<a href="{{ url('course/quiz/question/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

	{{-- edit answer --}}
	<div class="modal fade" id="form_edit_answer" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="">Edit Answer</h4>
	      </div>
	      <div class="modal-body">
					<div class="form-group">
						<label for="title">Answer</label>
						<input type="text" id="edit_text_answer" value="" class="form-control">
					</div>

					<div class="form-group">
						<label for="title">is correct ?</label>
						<select class="form-control" id="edit_text_answer_correct">
							<option value="0">Incorrect</option>
							<option value="1">Correct</option>
						</select>
					</div>
					<input type="hidden" id="edit_text_answer_id" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="save_edit_answer">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- edit answer --}}

	{{-- open editor --}}
	<div class="modal fade" id="modal_answer_editor" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-body">
					<textarea id="text_answer_editor"></textarea>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Batal</button>
	        <button type="button" class="btn btn-primary btn-raised" id="get_answer_editor">Simpan</button>
	      </div>
	    </div>
	  </div>
	</div>
	{{-- open editor --}}

@endsection

@push('script')
	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="application/javascript">
		CKEDITOR.replace('question', {
			filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
			filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		});
		CKEDITOR.replace('text_answer_editor', {
			filebrowserBrowseUrl: '/ckeditor/browser?type=Images',
			filebrowserUploadUrl: '/ckeditor/uploader?command=QuickUpload&type=Images',
		});
	</script>

	<script type="application/javascript">
		var js = document.createElement("script");
		js.type = "text/javascript";
		js.src = "WIRISplugins.js?viewer=image";
		document.head.appendChild(js);
	</script>
	<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	{{-- CKEDITOR --}}

	{{-- SELECT2 --}}
	<script type="text/javascript">
		$("#quiz_type_id").select2();
	</script>
	{{-- SELECT2 --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$(".feedback-check").change(function(){
			var cheked = $(this).prop('checked');
			if(cheked){
				$(".feedback-form").addClass('activated');
			}else {
				$(".feedback-form").removeClass('activated');
			}

		});
		$("#add_new_answer").click(function(){
			var choice_increments = $("#choice_increments").val()
			var cheked = $(".feedback-check").prop('checked');
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="quiz-form-wrap">'+
							'<div class="quiz-check">'+
								'<div class="text-center">'+
									'<label class="m-0" title="Tentukan jawaban benar">'+
										'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
										'<input type="checkbox" class="addon-check" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
									'</label>'+
								'</div>'+
							'</div>'+
							'<div class="quiz-form">'+
								'<div class="form-group no-m">'+
									'<label class="control-label" for="inputDefault">Masukkan pilihan</label>'+
									'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
									'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
								'</div>'+
							'</div>'+
							'<div class="quiz-action">'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="'+choice_increments+'" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="'+choice_increments+'" title="Hapus"><i class="zmdi zmdi-close"></i></a>'+
							'</div>'+
						'</div>'+
						'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
							'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
						'</div>'+
					'</div>'+
				'</div>'

				// '<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
				// '<div class="card-block bg-white">'+
				// '<div class="row">'+
				// 	'<div class="col-md-10">'+
				// 		'<div class="form-group no-m">'+
				// 			'<label class="control-label" for="inputDefault">Masukkan pilihan</label>'+
				// 			'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
				// 			'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
				// 			'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
				// 			'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
				// 		'</div>'+
				// 	'</div>'+

				// 	'<div class="col-md-2">'+
				// 		'<br>'+
				// 		'<div class="text-center">'+
				// 				'<label>'+
				// 					'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
				// 					'<input type="checkbox" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
				// 				'</label>'+
				// 		'</div>'+
				// 	'</div>'+
				// '</div>'+
				// '<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
				// 	'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
				// '</div>'+
				// '</div>'+
				// '</div>'+
				// '<br>'
			);
		})
	</script>

	<script type="text/javascript">
		$(function(){
			$(document).on('change','#correctAnswerCheck',function(e){
				e.preventDefault();
				var data_increment = $(this).attr('data-increment');
				$("#correctAnswertext" + data_increment).val($(this).is(":checked") == true ? '1' : '0')
			});
		})
	</script>
	{{-- add new answer --}}

	{{-- add new answer short answer --}}
	<script type="text/javascript">
		$("#add_new_answer_short_answer").click(function(){
			var choice_increments = $("#choice_increments").val()
			var cheked = $(".feedback-check").prop('checked');
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="quiz-form-wrap no-check">'+
							'<div class="quiz-form">'+
								'<div class="form-group no-m">'+
									'<label class="control-label" for="inputDefault">Masukkan jawaban</label>'+
									'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
									'<input type="hidden" value="1" name="answer_correct[]">'+
								'</div>'+
							'</div>'+
							'<div class="quiz-action">'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="'+choice_increments+'" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="'+choice_increments+'" title="Hapus"><i class="zmdi zmdi-close"></i></a>'+
							'</div>'+
						'</div>'+
						'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
							'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
						'</div>'+
					'</div>'+
				'</div>'

				// '<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
				// '<div class="card-block bg-white">'+
				// '<div class="row">'+
				// '<div class="col-md-12">'+
				// '<div class="form-group no-m">'+
				// '<label class="control-label" for="inputDefault">Masukkan jawaban</label>'+
				// '<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
				// '<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
				// '<input type="hidden" value="1" name="answer_correct[]">'+
				// '<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
				// 	'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
				// '</div>'+
				// '</div>'+
				// '</div>'+

				// '</div>'+
				// '</div>'+
				// '</div>'+
				// '<br>'
			);
		})
	</script>

	{{-- add new answer ordering --}}
	<script type="text/javascript">
		$("#add_new_answer_ordering").click(function(){
			var choice_increments = $("#choice_increments").val()
			var cheked = $(".feedback-check").prop('checked');
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="quiz-form-wrap no-check">'+
							'<div class="quiz-form">'+
								'<div class="form-group no-m">'+
									'<label class="control-label" for="focusedInput1">Masukkan pilihan</label>'+
									'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
									'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
									'<input type="hidden" name="answer_ordering[]" value="'+choice_increments+'">'+
								'</div>'+
							'</div>'+
							'<div class="quiz-action">'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-warning answer_editor" data-id="'+choice_increments+'" title="Gunakan editor"><i class="zmdi zmdi-edit"></i></a>'+
								'<a href="#/" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-danger" id="remove_row_answer" data-id="'+choice_increments+'" title="Hapus"><i class="zmdi zmdi-close"></i></a>'+
							'</div>'+
						'</div>'+
						'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
							'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
						'</div>'+
					'</div>'+
				'</div>'

				// '<div class="row" id="row_answer'+choice_increments+'">'+
				// 	'<div class="col-md-10">'+
				// 		'<div class="form-group label-floating">'+
				// 			'<label class="control-label" for="focusedInput1">Masukkan pilihan</label>'+
				// 			'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
				// 			'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
				// 			'<input type="hidden" name="answer_ordering[]" value="'+choice_increments+'">'+
				// 			'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
				// 			'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
				// 			'<div class="feedback-form '+(cheked ? 'activated' : '')+'">'+
				// 				'<textarea class="form-control" placeholder="Feedback" name="feedback_answer[]"></textarea>'+
				// 			'</div>'+
				// 		'</div>'+
				// 	'</div>'+
				// '</div>'+
				// '<br>'
			);
		})
	</script>
	{{-- add new answer ordering --}}

	{{-- remove new answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#remove_row_answer',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					$("#row_answer"+data_id).remove();
			});
		});
	</script>
	{{-- remove new answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var data_text_answer = $(this).attr('data-answer');
				var data_text_answer_correct = $(this).attr('data-answer-correct');
				$("#edit_text_answer").val(data_text_answer);
				$("#edit_text_answer_correct").val(data_text_answer_correct);
				$("#edit_text_answer_id").val(data_id);
				$("#form_edit_answer").modal('show');
			});
		});

		$("#save_edit_answer").click(function(){
			var token = '{{ csrf_token() }}';
			$.ajax({
				url : "{{url('course/quiz/question/answer/update_action')}}",
				type : "POST",
				data : {
					answer : $("#edit_text_answer").val(),
					answer_correct : $("#edit_text_answer_correct").val(),
					id : $("#edit_text_answer_id").val(),
					_token: token
				},
				success : function(result){
					location.reload();
				},
			});
		})
	</script>
	{{-- edit answer --}}

	<script type="text/javascript">
		$(function(){
			$(document).on('click','.answer_editor',function(e){
				e.preventDefault();
					data_id = $(this).attr('data-id');
					CKEDITOR.instances['text_answer_editor'].setData($("#text_answer" + data_id).val())
					$("#modal_answer_editor").modal('show');
			});
		});

		$("#get_answer_editor").click(function(){
			var value = CKEDITOR.instances['text_answer_editor'].getData()
			console.log(data_id);
			$("#text_answer" + data_id).val(value)
			$("#text_answer" + data_id).attr('type', 'hidden')
			$("#modal_answer_editor").modal('hide');
			$("#text_answer_editor_place" + data_id).html(value)
		})
	</script>

@endpush
