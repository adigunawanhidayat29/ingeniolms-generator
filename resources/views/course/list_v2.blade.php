@extends('layouts.app')

@section('title', Lang::get('front.page_my_courses.title'))

@push('style')
<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">

<style>
	.progress.progress-custom {
		height: 5px;
	}

	.progress-info {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
	}

	.progress-info .progress-percentage {
		font-size: 12px;
		color: #424242;
		line-height: 1;
	}

	.progress-info .progress-status {
		display: inline-block;
		text-align: center;
		color: #03a9f4;
		border: 2px solid #03a9f4;
		padding: 0.5rem 1rem;
	}

	.form-group {
		margin-top: 0;
		margin-left: 2rem;
	}

	.form-group .btn-group {
		margin: 0;
	}

	#role-user {
		width: 25%;
	}

	#class-card {
		height: 275px;
	}

	@media (max-width: 767px) {
		.form-group {
			margin-left: 0;
		}

		#role-user {
			width: 50%;
			margin-left: 1rem;
		}

		#class-card {
			height: auto;
		}
	}
</style>
@endpush

@section('content')
<div class="bg-page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="headline-md no-m">@lang('front.page_my_courses.header_dashboard')
					<span>@lang('front.page_my_courses.header_courses')</span></h2>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li><a href="/">@lang('front.page_my_courses.breadcumbs_home')</a></li>
				<li>@lang('front.page_my_courses.breadcumbs_my_courses')</li>
			</ul>
		</div>
	</div>
</div>

<div class="wrap pt-2 pb-2 mb-2 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="card-md radius-0">
					<div class="card-block">
						<div style="display: flex; align-items: center; width: 100%;">

							{{-- BUTTON TAB SEMUA KELAS --}}
							<div class="btn-group" id="allCourses">
								<button type="button" class="btn btn-sm btn-raised btn-primary dropdown-toggle" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									@lang('front.page_my_courses.action_create_courses') <i class="zmdi zmdi-chevron-down right"></i>
								</button>
								<ul class="dropdown-menu">
									@if (isCreator(Auth::user()->id))
									<li><a class="dropdown-item"
											href="{{url('course/create')}}">@lang('front.page_my_courses.action_create_new_courses')</a></li>
									@endif
									<li><a target="_blank" class="dropdown-item" href="/courses">@lang('front.page_my_courses.action_courses_catalog')</a>
									</li>
									<li><a class="dropdown-item" href="#" data-toggle="modal"
											data-target="#modalPrivateCourse">@lang('front.page_my_courses.action_use_courses_password')</a>
									</li>
								</ul>
							</div>

							<div id="role-user" class="form-group">
								<select class="form-control selectpicker filterCourses" data-dropup-auto="false">
									{{-- <option value="all">@lang('front.page_my_courses.filter_all')</option> --}}
									<option value="student">@lang('front.page_my_courses.filter_as_student')</option>
									<option value="teacher">@lang('front.page_my_courses.filter_as_instructor')</option>
								</select>
							</div>

							<form class="d-none d-sm-flex" action="" method="get" style="align-items: center; width: 100%;">
								<div class="form-group" style="width: 100%;">
									<input type="text" name="q" class="form-control m-0"
										placeholder="{{Lang::get('front.page_my_courses.search_courses_placeholder')}}">
									{!!Request::get('q') ? '<a
										href="/course/dashboard">'.Lang::get('front.page_my_courses.reset_search_courses_placeholder').'</a>'
									: ''!!}
								</div>
								<button type="submit" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary"
									style="width: 43px; margin-left: 1rem;"><i class="zmdi zmdi-search"></i></button>
							</form>
						</div>

						<form class="d-flex d-sm-none" action="" method="get" style="align-items: center; width: 100%;">
							<div class="form-group" style="width: 100%;">
								<input type="text" name="q" class="form-control m-0"
									placeholder="{{Lang::get('front.page_my_courses.search_courses_placeholder')}}">
								{!!Request::get('q') ? '<a
									href="/course/dashboard">'.Lang::get('front.page_my_courses.reset_search_courses_placeholder').'</a>'
								: ''!!}
							</div>
							<button type="submit" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary"
								style="width: 45px; margin-left: 1rem;"><i class="zmdi zmdi-search"></i></button>
						</form>
					</div>
				</div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs mb-2" role="tablist">
					<li class="nav-item">
						<a class="nav-link withoutripple active" href="#course" aria-controls="course" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-book"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_courses')</span>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link withoutripple" href="#program" aria-controls="program" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-collection-bookmark"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_program')</span>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link withoutripple" href="#archive" aria-controls="archive" role="tab" data-toggle="tab">
							<i class="zmdi zmdi-archive"></i>
							<span class="d-none d-sm-inline">@lang('front.page_my_courses.tab_horizontal_archive')</span>
						</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active show " id="course">
            <div class="row">
              @foreach($courses as $course)
              <div class="col-md-3 mb-2">
                <div id="class-card" class="card-sm">
                  <a
                    href="{{ url('course/learn/'. $course->slug)}}">
                    <img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
                  </a>
                  @php
                  $num_contents = count($course->contents);
                  //count percentage
                  $num_progress = 0;
                  $num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' =>
                  Auth::user()->id, 'status' => '1'])->get());
                  $percentage = 0;
                  $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
                  $percentage = $percentage == 0 ? 1 : $percentage ;
                  $percentage = 100 / $percentage;
                  //count percentage
                  @endphp
                  <a href="{{ url('course/learn/'. $course->slug)}}">
                    <div class="card-block text-left">
                      <h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
                      <p class="fs-12 text-truncate color-dark" style="line-height: 1;">{{$course->author}}</p>
                      <div class="m-0">
                        <div class="progress progress-custom mb-1">
                          <div class="progress-bar progress-bar-{{$percentage <= 50 ? 'danger' : 'success'}}"
                            role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100"
                            style="width: {{$percentage}}%;"></div>
                        </div>
                        <div class="progress-info">
                          <div class="progress-percentage">
                            {{ceil($percentage >= 100 ? 100 : ($percentage === 1 ? 0 : $percentage))}}%
                            @lang('front.page_my_courses.courses_percentage_finish')
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              @endforeach
            </div>

            <div class="text-center">
              {{$courses->links('pagination.default')}}
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="program">
            program
          </div>
          <div role="tabpanel" class="tab-pane" id="archive">
            archive
          </div>
				</div>

				<!-- Modal -->
				<div class="modal" id="shareCode" tabindex="-1" role="dialog" aria-labelledby="shareCode">
					<div class="modal-dialog animated zoomIn animated-3x" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<h2 class="headline text-center">@lang('front.page_my_courses.courses_share_code_via_text')</h2>
								<div class="text-center">
									<a href="#" class="btn btn-whatsapp"><i class="zmdi zmdi-whatsapp"></i> Whatsapp</a>
									<a href="#" class="btn btn-google"><i class="zmdi zmdi-google"></i> Google</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal" id="modalPrivateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog animated zoomIn animated-3x" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title color-primary" id="myModalLabel">@lang('front.page_my_courses.input_course_password')</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
							class="zmdi zmdi-close"></i></span></button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">@lang('front.page_my_courses.courses_password_text')</label>
					<input type="text" class="form-control" id="CoursePassword" placeholder="{{Lang::get('front.page_my_courses.input_course_password')}}">
				</div>
			</div>
			<div class="modal-footer">
				<button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">@lang('front.page_my_courses.course_with_password_join_button')</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalPrivateProgram" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog animated zoomIn animated-3x" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title color-primary" id="myModalLabel">@lang('front.page_my_courses.input_course_password')</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
							class="zmdi zmdi-close"></i></span></button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">@lang('front.page_my_courses.courses_password_text')</label>
					<input type="text" class="form-control" id="ProgramPassword" placeholder="{{Lang::get('front.page_my_courses.input_course_password')}}">
				</div>
			</div>
			<div class="modal-footer">
				<button id="CheckPassword" type="button" class="btn  btn-primary btn-raised">@lang('front.page_my_courses.course_with_password_join_button')</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
@endsection

@push('script')
<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
<script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('clipboard/clipboard.min.js')}}"></script>

<script type="text/javascript">
	var clipboard = new ClipboardJS('.btn-copy');
	clipboard.on('success', function (e) {
		alert(e.text + ' Berhasil disalin')
		console.info('Action:', e.action);
		console.info('Text:', e.text);
		console.info('Trigger:', e.trigger);
		e.clearSelection();
	});
</script>

@if(\Session::get('message'))
<script type="text/javascript">
	swal("Informasi", "{{\Session::get('message')}}", "info");
</script>
@endif

<script type="text/javascript">
	$("#CheckPassword").click(function () {
		var password = $("#CoursePassword").val();
		$.ajax({
			url: '/course/join-private-course',
			type: 'post',
			data: {
				password: password,
				_token: '{{csrf_token()}}',
			},
			success: function (response) {
				console.log(response);
				if (response.code == 200) {
					$.ajax({
						url: '/course/enroll',
						type: 'get',
						data: {
							id: response.data.id,
						},
						success: function (data) {
							swal({
								type: 'success',
								title: 'Berhasil...',
								html: response.message,
							}).then(function () {
								window.location.assign('/course/learn/' + response.data.slug)
							});
						}
					})
				} else {
					swal({
						type: 'error',
						title: 'Gagal!',
						html: response.message,
					})
				}
			}
		})
	})
</script>

<script>
	$(".filterCourses").change(function() {
		window.location.assign('?filter=' + $(this).val())
	})
</script>
@endpush