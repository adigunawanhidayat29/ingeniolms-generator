@extends('layouts.app')

@section('title')
	@lang('front.announcement.create.title')
@endsection

@push('style')
	<link href="{{asset('/summernote/summernote.min.css')}}" rel="stylesheet">
@endpush


@section('content')

  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>@lang('front.announcement.create.title')</span></h2>
        </div>
      </div>
    </div>
  </div>

	<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.announcement.create.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.announcement.create.breadcumb_courses')</a></li>
          <li><a href="/course/preview/{{ $course->id }}">{{ $course->title }}</a></li>
          <li>@lang('front.announcement.create.title')</li>
        </ul>
      </div>
    </div>
  </div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="card-md">
						<div class="card-block">
							{{Form::open(['url' => $url, 'method' => $method])}}
							{{csrf_field()}}

								<div class="form-group">
									<label for="">@lang('front.announcement.create.enter_title')</label>
									<input type="text" class="form-control" placeholder="@lang('front.announcement.create.enter_title')" name="title" value="{{$title}}">
								</div>

								<div class="form-group">
									<label for="">@lang('front.announcement.create.enter_description')</label>
									<textarea class="summernote" id="description" name="description">{!! $description !!}</textarea>
								</div>

								<input type="submit" value="@lang('front.general.text_save')" class="btn btn-primary btn-raised">
								<a class="btn btn-default btn-raised" href="/course/preview/{{ $course->id }}">@lang('front.general.text_cancel')</a>
							{{Form::close()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script src="{{asset('/summernote/summernote.min.js')}}"></script>
	<script>
		$(document).ready(function() {
		$('.summernote').summernote({
        height: 300
      });
	});
	</script>

	<script type="text/javascript">
		function back(){
			window.history.back();
		}
	</script>
@endpush
