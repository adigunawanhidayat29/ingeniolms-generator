@extends('layouts.app_nation')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>
	<style>
		/* Style the tab */
		.tab {
				float: left;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
				width: 30%;
				height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
				display: block;
				background-color: inherit;
				color: black;
				padding: 22px 16px;
				width: 100%;
				border: none;
				outline: none;
				text-align: left;
				cursor: pointer;
				transition: 0.3s;
				font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
				background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
				background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
				float: left;
				padding: 0px 12px;
				border: 1px solid #ccc;
				width: 70%;
				border-left: none;
				height: auto;
		}

		.placeholder{
				background-color:white;
				height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}
	</style>
@endpush

@section('content')
<div class="container">
	<h2>Library</h2>
	<a href="#" data-toggle="modal" data-target="#ModalMenuContent" da class="btn btn-primary btn-raised">Tambah Konten / Kuis</a>
	<br>
	<br>
	<div class="table-responsive card" style="padding:20px;">
		<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th style="width:20px;"></th>
                <th>Nama Kontent</th>
                <th>Tanggal Dibuat</th>
								<th>Ukuran</th>
								<th>Tipe</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
					@foreach($content as $index => $item)
            <tr>
                <td class="text-center"><i class="{{content_type_icon($item->type_content)}}"></i></td>
                <td>{{$item->title}}</td>
                <td>{{$item->created_at}}</td>
                <td class="text-center">-</td>
								<th>{{$item->type_content}}</th>
								<td>
									<form action="" method="post">
										<button class="btn btn-info" style="padding: 10px;cursor: pointer;text-align: center;margin-right: 5px;"><i style="margin:0" class="fa fa-edit"></i></button>
										<button class="btn btn-danger" style="padding: 10px;cursor: pointer;text-align: center;margin-right: 5px;"><i style="margin:0" class="fa fa-trash"></i></button>
									</form>
								</td>
            </tr>
					@endforeach
        </tbody>
    </table>
	</div>
	<div class="modal" id="ModalMenuContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
				<div class="modal-content">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3>Tambah Konten / Kuis</h3>
							<div class="tab">
								<button class="tablinks" onclick="openCity(event, 'Label')"><i class="fa fa-tags"></i> Label</button>
								<button class="tablinks" onclick="openCity(event, 'Video')" id="defaultOpen"><i class="fa fa-video-camera"></i> Konten Video</button>
								<button class="tablinks" onclick="openCity(event, 'Teks')"><i class="fa fa-text-height"></i> Konten Teks</button>
								<button class="tablinks" onclick="openCity(event, 'File')"><i class="fa fa-file-o"></i> Konten File</button>
								<button class="tablinks" onclick="openCity(event, 'Kuis')"><i class="fa fa-question"></i> Kuis</button>
								<button class="tablinks" onclick="openCity(event, 'Tugas')"><i class="fa fa-tasks"></i> Tugas</button>
							</div>

							<div id="Label" class="tabcontent">
								<h3>Tambah Label</h3>
								<p>
									Tambahkan label untuk informasi konten lebih jelas
								</p>

								<a href="/library/course/content/create/?content=label" class="btn btn-primary btn-raised">Tambah Label</a>

								<br>
							</div>

							<div id="Video" class="tabcontent">
								<h3>Tambah Konten Video</h3>
								<p>
									Tambahkan konten video agar proses belajar menjadi lebih interaktif
								</p>
								<a href="/library/course/content/create/?content=video" class="btn btn-primary btn-raised">Upload Video</a>
								<a href="/library/course/content/create/?content=url&urlType=video" class="btn btn-primary btn-raised">Tambah URL Youtube</a>

								<br>
							</div>

							<div id="Teks" class="tabcontent">
								<h3>Tambah Konten Teks</h3>
								<p>
									Anda bisa menambahkan konten dengan hanya teks saja. bisa digunakan seperti membuat pendahuluan dan lain-lain.
								</p>
								<a href="/course/content/library/create/?content=text" class="btn btn-primary btn-raised">Tambah Konten Teks</a>
								{{-- <a href="/course/content/library/?content=text" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<br>
							</div>

							<div id="File" class="tabcontent">
								<h3>Tambah Konten File</h3>
								<p>
									Anda bisa menambahkan konten file / berkas. bisa digunakan seperti membuat file pdf, power point dan lain-lain.
								</p>
								<a href="/course/content/library/create/?content=file" class="btn btn-primary btn-raised">Tambah Konten File</a>
								{{-- <a href="/course/content/library/?content=file" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<a href="/course/content/library/create/?content=url&urlType=file" class="btn btn-primary btn-raised">Tambah Dari URL</a>

								<br>
							</div>

							<div id="Kuis" class="tabcontent">
								<h3>Tambah Kuis</h3>
								<p>
									Tambahkan kuis untuk menguji peserta sejauhmana mereka belajar
								</p>
								<a href="/course/quiz/library/create" class="btn btn-primary btn-raised">Tambah Kuis</a>
								{{-- <a href="/course/content/library/?content=quiz" class="btn btn-default btn-raised">Ambil dari Library</a> --}}
								<br>
							</div>

							<div id="Tugas" class="tabcontent">
								<h3>Tambah Tugas</h3>
								<p>
									Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.
								</p>
								<a href="/course/assignment/create/" class="btn btn-primary btn-raised">Tambah Tugas</a>
								<br>
							</div>
							<br><br><br><br> &nbsp;
							{{-- <a href="#" class="btn btn-default btn-raised">Batal</a> --}}
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

@endsection

@push('script')
		<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
		<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
		<script src="{{asset('js/sweetalert.min.js')}}"></script>
		<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script>
		// trigger modal add konten / quiz
		  function openCity(evt, cityName) {
		    var i, tabcontent, tablinks;
		    tabcontent = document.getElementsByClassName("tabcontent");
		    for (i = 0; i < tabcontent.length; i++) {
		        tabcontent[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablinks");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(cityName).style.display = "block";
		    evt.currentTarget.className += " active";
		  }

		  // Get the element with id="defaultOpen" and click on it
		  document.getElementById("defaultOpen").click();

		  // trigger modal add konten / quiz

			$(document).ready(function() {
			    $('#example').DataTable();
			});
		</script>
@endpush
