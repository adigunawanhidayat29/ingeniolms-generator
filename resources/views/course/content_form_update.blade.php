@extends('layouts.app')

@section('title')
{{ \Lang::get('front.content_create.title') . ' - ' . $section->title }}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.content_create.update') <span>{{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title') : \Lang::get('front.content_create.content_title')}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.content_create.home')</a></li>
          {{-- <li><a href="/course/dashboard">@lang('front.content_create.teacher')</a></li> --}}
					<li><a href="/course/dashboard">@lang('front.content_create.manage_course')</a></li>
					<li><a href="/course/preview/{{$section->id_course}}">@lang('front.content_create.preview')</a></li>
					<li>@lang('front.content_create.create') {{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title') : \Lang::get('front.content_create.content_title')}}</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="headline-sm headline-line" style="text-transform: capitalize;">@lang('front.content_create.update') {{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title') : \Lang::get('front.content_create.content_title')}} {{Request::get('content') == 'folder' ? '' : Request::get('content')}} - <span>{{$section->title}}</span></h3>
				</div>
				<div class="col-md-12">
					<!-- Default box -->
					<div class="box">
						<div class="box-body">
							<center>
								@if(Session::has('success'))
									<div class="alert alert-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('success') !!}
									</div>
								@elseif(Session::has('error'))
									<div class="alert alert-error alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									{!! Session::get('error') !!}
									</div>
								@endif
							</center>
							<div class="card-md">
							  <!-- <div class="panel-heading">
							    <h3 class="panel-title">{{$button}} Konten</h3>
							  </div> -->
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method)) }}
										@if(Request::get('content') == 'video')
											<div class="form-group" id="FileVideo">
												<label for="title">@lang('front.content_create.file_upload_label')</label>
												<input type="text" readonly="" class="form-control" placeholder="@lang("front.general.text_choose")...">
												<input type="file" id="file" class="form-control">
												<input type="hidden" id="path_file" name="path_file">
												<input type="hidden" id="name_file" name="name_file">
												<input type="hidden" id="file_size" name="file_size">
												<input type="hidden" id="video_duration" name="video_duration">
												<input type="hidden" id="full_path_original" name="full_path_original">
												<input type="hidden" id="resize_path" name="resize_path">
												@if($button == "Update")
													<i class="{{content_type_icon($type_content)}}"></i> <a href="#">{{$title}}</a>
												@endif

												<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
												<div id="container"></div>

												<div class="progress" style="display:none">
													<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
														<span class="sr-only">0%</span>
													</div>
												</div>

												<div id="uploadedMessageError">
													<div id="uploadedMessage"></div>
												</div>
											</div>
										@endif

										@if(Request::get('content') == 'file')
											<div class="form-group" id="FileFile">
												<label for="title">File (Docx, PDF, xlsx, etc.)</label>
												<input type="text" readonly="" class="form-control" placeholder="Pilih...">
												<input type="file" id="file" class="form-control">
												<input type="hidden" id="path_file" name="path_file">
												<input type="hidden" id="name_file" name="name_file">

												<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
												<div id="container"></div>

												<div class="progress" style="display:none">
													<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
														<span class="sr-only">0%</span>
													</div>
												</div>

												<div id="uploadedMessageError">
													<div id="uploadedMessage"></div>
												</div>
											</div>
										@endif
										
										@if($button == 'Create')
											@if(Request::get('content') == 'url')
												@if(Request::get('urlType')=='video')
													<div class="form-group" id="YoutubeVids">
														<label for="title">@lang('front.content_create.search_youtube')</label>
														<input type="text" id="search" placeholder="@lang('front.content_create.search_video_keyword')" autocomplete="off" class="form-control" />
														<button class="btn btn-primary btn-raised" type="button" class="form-control btn btn-primary w100" onclick="searchVideo()">@lang('front.content_create.search_video_btn')</button>
														<div id="YouLib" style="display: none;">
															<div class="card card-body" style="background: #F5F5F5;">
																<table class="table table-no-border">
																</table>
															</div>
														</div>
													</div>
												@endif

												<div class="form-group" id="FileURL">
													<label for="title">URL</label>
													<input id="contentFullPathFile" placeholder="URL" type="text" class="form-control" name="full_path_file" value="" required>
												</div>
											@endif
										@endif
										
                    @if($button == 'Update')
                      @if(Request::get('content')=='url-video')
                        <div class="form-group" id="YoutubeVids">
                          <label for="title">@lang('front.content_create.search_youtube')</label>
                          <input type="text" id="search" placeholder="@lang('front.content_create.search_video_keyword')" autocomplete="off" class="form-control" />
                          <button class="btn btn-primary btn-raised" type="button" class="form-control btn btn-primary w100" onclick="searchVideo()">Cari Video</button>
                          <div id="YouLib" style="display: none;">
                            <div class="card card-body" style="background: #F5F5F5;">
                              <table class="table table-no-border">
                              </table>
                            </div>
                          </div>
                        </div>
												<div class="form-group" id="FileURL">
													<label for="title">URL</label>
													<input id="contentFullPathFile" placeholder="URL" type="text" class="form-control" name="full_path_file" value="{{$full_path_file}}" required>
												</div>
                      @endif
										@endif

										@if(Request::get('content') != 'label')
											<div class="form-group" id="divTitle">
												<label for="title">{{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title_name') : \Lang::get('front.content_create.content_title_name')}}</label>
												<input id="contentTitle" placeholder="{{Request::get('content') == 'folder' ? \Lang::get('front.content_create.folder_title_name') : \Lang::get('front.content_create.content_title_name')}}" type="text" class="form-control" name="title" value="{{ $title }}" required>
												@if ($errors->has('title'))
													<span class="text-danger">{{ $errors->first('title') }}</span>
												@endif
											</div>
										@endif

										@if(Request::get('urlType') != 'video')
											<div class="form-group" id="divDescription">
												<label for="title">@lang('front.content_create.description')</label>
												<textarea name="description" id="description">{{ $description }}</textarea>
											</div>
											<label for="is_description_show" style="color: black !important;">
												<input id="is_description_show" name="is_description_show" type="checkbox" value="1" {{$is_description_show == '1' ? 'checked' : ''}}> @lang('front.content_create.description_show')
											</label>
										@endif

										<div class="form-group" id="divDescription">
											<label for="title">@lang('front.content_create.content_for')</label>
											<select name="group_student_id" class="form-control selectpicker" id="group_student_id">
												<option value="all">@lang('front.content_create.content_for_all')</option>
												<option value="group">@lang('front.content_create.content_for_group')</option>
											</select>
										</div>

										<div id="section_show_group">
											@foreach ($course_student_groups as $item)
												<label for="course_student_group_{{$item->id}}" style="color: black !important;">
													<input id="course_student_group_{{$item->id}}" name="course_student_group[]" class="course_student_group" type="checkbox" value="{{$item->id}}" @foreach($group_has_content as $data) {{$data->course_student_group_id == $item->id ? 'checked' : ''}} @endforeach> {{$item->name}}
												</label> <br>
											@endforeach
										</div>

										<div class="form-group">
											<input name="id" type="hidden" id="contentId" value="{{$id}}">
											<input name="type_content" type="hidden" value="{{Request::get('content')}}">
											{{-- {{  Form::submit($button . " Content" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button', 'id' => 'saveContent')) }} --}}
											{{-- <button type="button" id="btnSaveDraft" class="btn btn-primary btn-raised">Simpan Sebagai Draft</button> --}}
											<button onclick="afterClick()" type="submit" id="contentSave" class="btn btn-success btn-raised">@lang('front.content_create.save_and_publish')</button>
											<a href="{{ url('course/preview/'.$section->id_course) }}" class="btn btn-default btn-raised">@lang('front.content_create.cancel')</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>
						</div>
						<!-- /.box-body-->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')

	<script>
		function afterClick(){
			$("#contentSave").removeClass('btn-success');
			$("#contentSave").removeClass('btn-raised');
			$("#contentSave").html('<i class="fa fa-circle-o-notch fa-spin"></i>@lang("front.general.text_waiting")');
		}
	</script>

	{{-- CKEDITOR --}}
	{{-- <script src="{{url('ckeditor/ckeditor.js')}}"></script> --}}
	<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	<script type="text/javascript">
		@if(Request::get('content') == 'video')
			$("#divTitle").hide()
			$("#divDescription").hide()
			$("#contentSave").hide()
			// $("#divSequence").hide()
		@endif
	</script>

	<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>
	<script type="text/javascript">
		// Custom example logic
		var path_file = $("#path_file").val();
		var name_file = $("#name_file").val();
		var file_size = $("#file_size").val();
		var video_duration = $("#video_duration").val();
		var full_path_original = $("#full_path_original").val();
		var resize_path = $("#resize_path").val();
		// $("#saveContent").prop('disabled', true);
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'file', // you can pass an id...
			container: document.getElementById('container'), // ... or DOM Element itself
			url : '/course/content/upload/{{$section->id}}',
			flash_swf_url : '/plupload/Moxie.swf',
			silverlight_xap_url : '/plupload/Moxie.xap',
			chunk_size: '1mb',
			dragdrop: true,
			headers: {
				'X-CSRF-TOKEN': '{{csrf_token()}}'
			},
			filters : {
				mime_types: [
					@if(Request::get('content') == 'video')
						{title : "Video files", extensions : "mp4,webm,avi,mkv"},
					@else
						{title : "Document files", extensions : "docx,doc,xlsx,pptx,pdf,xls,txt"},
					@endif
				]
			},

			init: {
				PostInit: function() {
					document.getElementById('filelist').innerHTML = '';
				},

				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						$("#divTitle").show()
						$("#divDescription").show()
						$("#contentTitle").val(file.name)
						$("#contentSave").show()

						$.ajax({
							url: '/course/content/store-draft',
							type: 'POST',
							data: {
								_token : '{{csrf_token()}}',
								field: 'title',
								value: file.name,
								section_id: '{{Request::segment(4)}}',
								type_content: '{{Request::get('content') ? Request::get('content') : $type_content}}',
								id: $("#contentId").val()
							},
							success: function(response){
								// alert('saved draft');
								$("#contentId").val(response.id)
							}
						})

						uploader.start();
						$("#contentSave").prop('disabled', true);
						return false;
					});
				},

				UploadProgress: function(up, file) {
					// document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
					$('.progress').show();
					if(file.percent < 97){
						$('#progressBar').attr('aria-valuenow', file.percent).css('width', file.percent + '%').text(file.percent + '%');
					}
					$("#uploadedMessage").html('Upload masih dalam proses...');
					$("#uploadedMessage").addClass('alert alert-info');
				},

				FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            // console.log('[FileUploaded] File:', file, "Info:", info);
						var response = JSON.parse(info.response);
						// console.log(response.result.original);
						path_file = $("#path_file").val(response.result.original.path);
						name_file = $("#name_file").val(response.result.original.name);
						file_size = $("#file_size").val(response.result.original.size);
						video_duration = $("#video_duration").val(response.result.original.video_duration);
						full_path_original = $("#full_path_original").val(response.result.original.full_path_original);
						resize_path = $("#resize_path").val(response.result.original.resize_path);

						$.ajax({
							url: '/course/content/store-draft',
							type: 'POST',
							data: {
								_token : '{{csrf_token()}}',
								file: true,
								path_file: response.result.original.path,
								name_file: response.result.original.name,
								file_size: response.result.original.size,
								video_duration: response.result.original.video_duration,
								full_path_original: response.result.original.full_path_original,
								resize_path: response.result.original.resize_path,
								section_id: '{{Request::segment(4)}}',
								type_content: '{{Request::get('content') ? Request::get('content') : $type_content}}',
								id: $("#contentId").val()
							},
							success: function(response){
								// alert('saved draft');
								$("#contentId").val(response.id)
							}
						})

        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            // console.log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            // Called when all files are either uploaded or failed
            // console.log('[UploadComplete]');
						// $("#saveContent").prop('disabled', false);
						// console.log(up);
						$('#progressBar').attr('aria-valuenow', 100).css('width', 100 + '%').text(100 + '%');
						$("#uploadedMessage").html('Upload selesai');
						$("#uploadedMessage").addClass('alert alert-success');
						$("#contentSave").prop('disabled', false);
        },

				Error: function(up, err) {
					// console.log(up);
					console.log(err);
					// document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
					if(err.status == 500 || err.code == "-200"){
						$("#uploadedMessage").html('');
						$("#uploadedMessageError").html('Upload Gagal');
						$("#uploadedMessageError").addClass('alert alert-danger');
						console.log('Upload Gagal');
					}
				},

				Destroy : function(){
					console.log('error');
				}
			}
		});

		uploader.init();
	</script>

	<script type="text/javascript">
		// kondisi type koenten
		var type_content = '{{ Request::get('content') ? Request::get('content') : $type_content }}'
		@if(Request::get('urlType'))
			type_content = 'url-' + '{{Request::get('urlType')}}'
		@endif
		// kondisi type koenten

		$("#contentFullPathFile").change(function(){
			$.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'full_path_file',
					value: $(this).val(),
					section_id: '{{Request::segment(4)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		})

		$("#contentTitle").change(function(){
			$.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'title',
					value: $(this).val(),
					section_id: '{{Request::segment(4)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					//console.log('Changed')
					$("#contentId").val(response.id)
				}
			})
		})

		CKEDITOR.instances.description.on('change', function() {
			$.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'description',
					value: CKEDITOR.instances['description'].getData(),
					section_id: '{{Request::segment(4)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		});

		$("#contentType").change(function(){
			$.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'type_content',
					value: $(this).val(),
					section_id: '{{Request::segment(4)}}',
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
				}
			})
		})

		// save draft
		$("#btnSaveDraft").click(function(){
			window.location.assign('/course/preview/{{$section->course_id}}')
		})

		// save n publish
		$("#contentSave").click(function(){
			$.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'status',
					value: '1',
					section_id: '{{Request::segment(4)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// window.location.assign('/course/preview/{{$section->id_course}}')
				}
			})
		})

  </script>

  <script>
		function searchVideo(){
			init();
			let keyword = $('#search').val();
			var request = gapi.client.youtube.search.list({
				part: "snippet",
				type: "video",
				q: encodeURIComponent(keyword).replace(/%20/g, "+"),
				maxResults: 10,
				order: "viewCount"
			});
			// execute the request
			request.execute(function(response) {
				var results = response.result;
				$("#results").html("");
				$('table').empty();
				$.each(results.items, function(index, item) {
					// console.log(index);
					console.log(item);
					$('table').append('<tr>' +
							'<td><img src="'+item.snippet.thumbnails.default.url+'"></td>' +
							'<td><h4>'+item.snippet.title+'</h4> <br/></td>' +
							'<td><button class="btn btn-danger btn-raised" type="button" onclick="window.open(\'https://www.youtube.com/watch?v='+item.id.videoId+'\',\'_blank\')" title="Preview video in new tab">Preview</button>' +
							// '<button class="btn btn-primary btn-raised" type="button" onclick="copyClipboard(this)" value="https://www.youtube.com/watch?v='+item.id.videoId+'" title="Copy link into clipboard">Copy Link</button></td>' +
							'<button class="btn btn-success btn-raised" type="button" id="setYoutubeVideo" data-title="'+item.snippet.title+'" data-description="'+item.snippet.description+'" data-url="https://www.youtube.com/watch?v='+item.id.videoId+'" value="https://www.youtube.com/watch?v='+item.id.videoId+'" title="Pilih vidio ini">Pilih</button></td>' +
							'</tr>');
				});
				// resetVideoHeight();
				$('#YouLib').css('display','block');
			});
			// $(window).on("resize", resetVideoHeight);
		}

		function copyClipboard(btnObj) {
			/* Get the text field */
			var copyText = btnObj.value;
			var dummy = $('<input>').val(copyText).appendTo('body').select();
			document.execCommand("copy");
		}

    $(document).on('click', "#setYoutubeVideo" , function() {
        var url = $(this).attr('data-url');
        var title = $(this).attr('data-title');
        var description = $(this).attr('data-description');
        // alert(description)

        $("#contentFullPathFile").val(url);
        $("#contentTitle").val(title);
        changeValues(title, url, description);
				$("#results").html("");
				$('table').empty();

        // CKEDITOR.instances['description'].setData(description);
    });

    function changeValues(title, url, description) {

      $.ajax({
				url: '/course/content/store-draft',
				type: 'POST',
				data: {
					_token : '{{csrf_token()}}',
					field: 'title',
					value: title,
					section_id: '{{Request::segment(4)}}',
					type_content: type_content,
					id: $("#contentId").val()
				},
				success: function(response){
					// alert('saved draft');
					$("#contentId").val(response.id)
          $.ajax({
            url: '/course/content/store-draft',
            type: 'POST',
            data: {
              _token : '{{csrf_token()}}',
              field: 'full_path_file',
              value: url,
              section_id: '{{Request::segment(4)}}',
              type_content: type_content,
              id: response.id
            },
            success: function(response){
              // alert('saved draft');
              // $("#contentId").val(response.id)
							$("#results").html("");
							$('table').empty();
            }
          })
				}
			})
    }

		function init() {
			gapi.client.setApiKey("AIzaSyC1qnx101nz-Yr-kCQgrL93cWXX0F5OFww");
			gapi.client.load("youtube", "v3", function() {
				// yt api is ready
			});
		}
	</script>
	<script src="https://apis.google.com/js/client.js?onload=init"></script>

	<script>
		@if ($group_has_content->count() > 0) 
			$("#section_show_group").show();
		@else
			$("#section_show_group").hide();
		@endif
		$("#group_student_id").change(function() {
			if ($(this).val() == 'group') {
				$("#section_show_group").show();
			} else {
				$("#section_show_group").hide();
			}
		})
	</script>

@endpush
