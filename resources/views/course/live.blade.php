@extends('layouts.app')

@section('title', 'Kelas Live')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3>Kelas Live</h3>
      <div class="row">
        @foreach($courses as $course)
          @if(is_liveCourse($course->id) == true)
            <div class="col-md-3">
              <div class="card">
                <span class="course-live">{!!is_liveCourse($course->id) == true ? '<img height="30" src="/img/live.png" />' : ''!!}</span>
                <a href="{{url('course/'. $course->slug)}}" class="zoom-img withripple">
                  <img src="{{asset_url(course_image_small($course->image))}}" alt="{{ $course->title }}" class="dq-image-big img-fluid">
                </a>
                <div class="card-block text-left">
                  <a href="{{url('course/'. $course->slug)}}" data-applink="{{url('course/'. $course->slug)}}">
                    <h4 class="color-primary dq-max-title">{{$course->title}}</h4>
                  </a>
                  <p>
                    <i class="color-dark"><a href="/user/{{ $course->user_slug }}" class="dq-max-subtitle">{{ $course->name }}</a></i>
                  </p>
                  <div class="mt-2 text-right">
                    <a href="{{url('course/'. $course->slug)}}" class="btn btn-raised btn-primary">
                      <i class="fa fa-money"></i>{{$course->price == 0 ? 'Free' : rupiah($course->price)}}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          @endif
          @endforeach
      </div>
    </div>
    </div>

    <!--Pagination Wrap Start-->
    <div class="text-center">
      {{$courses->links('pagination.default')}}
    </div>
    <!--Pagination Wrap End-->
</div>

@endsection

@push('script')
  <script type="text/javascript">
    $(".filterCategory").change(function(){
      window.location.assign('/category/'+$(this).val());
    })
    $('a[data-applink]').applink();
  </script>
@endpush
