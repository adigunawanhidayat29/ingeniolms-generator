@extends('layouts.app')

@section('title', 'Course Atendee ' . $Course->title)

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Daftar List <span>Peserta</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/preview/{{$Course->id}}">Pengajar</a></li>
          <li>Daftar Peserta</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<!-- <div class="card-md"> -->
			  <!-- <div class="panel-heading">
			    <h3 class="panel-title">Course Completion - {{$Course->title}}</h3>
			  </div> -->
			  <!-- <div class="card-block"> -->
					<h2 class="headline-sm no-m">Daftar Peserta - <span>{{$Course->title}}</span></h2>
					<table class="table table-bordered mt-2">
						<tr class="bg-primary">
							<th>No</th>
							<th>Nama</th>
							<th>Pilihan</th>
						</tr>
						@foreach($completions as $index => $completion)
							<tr class="bg-white">
								<td>{{$index + 1}}</td>
								<td>{{$completion['name']}}</td>
								<td>
                  <a class="btn btn-primary btn-raised btn-sm" href="/course/completion/detail/{{Request::segment(3)}}/{{$completion['user_id']}}">Lihat Progres</a>
                  <a onclick="return confirm('Yakin akan menghapus peserta ini?')" class="btn btn-danger btn-raised btn-sm" href="/course/completion/delete/{{Request::segment(3)}}/{{$completion['user_id']}}">Hapus</a>
                </td>
							</tr>
						@endforeach
					</table>
			  <!-- </div> -->
			<!-- </div> -->
		</div>
	</div>
@endsection
