@extends('layouts.app')

@section('title', $discussion->body)

@push('meta')
  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .upload-page{
      background: #f8f8f8;
      border: 1px solid #d1d1d1;
      border-top: none;
      padding: 10px 15px;
      padding-bottom: 5px;
    }

    .upload-page input{
      /* z-index: 999; */
      /* display: none; */
    }

    .hidden{
      display: none;
    }

    .komentar-second{

    }

    .komentar-first{
      border: 1px solid #d3cfcf;
      height: 70px;
      margin-bottom: 30px;
      margin-top: 10px;
      cursor: pointer;
      padding: 10px 20px;
      color: #c7c7c7;
    }
  </style>
  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload.css" />
  <link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui.css" />
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript
    ><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-noscript.css"
  /></noscript>
  <noscript
    ><link rel="stylesheet" href="/jquery-upload/css/jquery.fileupload-ui-noscript.css"
  /></noscript>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.discussion.breadcumb_home')</a></li>
          <li><a href="/course/dashboard">@lang('front.discussion.breadcumb_my_courses')</a></li>
          <li><a href="/course/learn/{{$course->slug}}">{{$course->title}}</a></li>
          <li>@lang('front.discussion.breadcumb_discussion')</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
        <a class="btn btn-default btn-raised" href="/course/learn/{{$course->slug}}"><i class="fa fa-chevron-left"></i>@lang('front.general.text_back')</a>
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-1">
                <img height="50" src="{{avatar($discussion->photo)}}" alt="{{$discussion->name}}">
              </div>
              <div class="col-md-11">
                <h3 class="panel-title">
                  <strong>{{$discussion->body}}</strong>
                  <br>
                  <i style="font-size:12px;color:#0a769e">{{$discussion->name}}</i> <i style="font-size:12px">{{$discussion->created_at}}</i>
                </h3>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="det-pertanyaan">
              {!!$discussion->description!!}
            </div>
          </div>
        </div>
        <h3 class="headline-sm fs-20 no-m"><span>@lang('front.discussion.give_comment')</span></h3>
        <div class="komentar-first">@lang('front.discussion.give_comment')</div>
        <div class="komentar-second hidden">
          <div class="form-group no-m">
            <form action="{{url('discussion/answer')}}" method="post">
              {{csrf_field()}}
              <input type="hidden" name="discussion_id" value="{{$discussion->id}}">
              <textarea name="body" id="answer" placeholder="Berikan Komentar Anda untuk membantu penanya menemukan jawaban terbaik." class="form-control" rows="8" cols="80"></textarea>

              <div class="upload-file-new" style="background: #f8f8f8;padding: 0px 10px;border: 1px solid #d1d1d1;">
                <input id="file_upload" name="file_upload" type="file" multiple="true">
                <div id="queue"></div>
              </div>

              <div class="form-group no-m">
                <input type="submit" class="btn btn-primary btn-raised" name="submit" value="@lang('front.discussion.submit_comment')">
                <input type="button" class="btn btn-default btn-raised" id="cancel-komen" name="submit" value="@lang('front.general.text_cancel')">
              </div>
            </form>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            @foreach($discussion_asnwers as $discussion_asnwer)
              <div class="col-md-1">
                <p>
                  <img src="{{avatar($discussion_asnwer->photo)}}" height="50" alt="{{$discussion_asnwer->name}}">
                </p>
              </div>
              <div class="col-md-11">
                <i style="font-size:12px;color:#0a769e">{{$discussion_asnwer->name}} <span><i style="font-size:12px;">{{$discussion_asnwer->created_at}}</i></span></i>
                <p>{!!$discussion_asnwer->body!!}</p>

              </div>
              @if($discussion_asnwer->file)
              <?php
                $files = json_decode($discussion_asnwer->file);
                usort($files, function ($a, $b) use ($files) {

                    // preg_match("/(\d+(?:-\d+)*)/", $a[0], $matches);
                    // $firstimage = $matches[1];
                    // preg_match("/(\d+(?:-\d+)*)/", $b[0], $matches);
                    // $lastimage = $matches[1];
                    $ext = explode('.',$a[0]);
                    $ext = strtolower($ext[count($ext)-1]);
                    $firstimage = $ext;
                    $ext = explode('.',$b[0]);
                    $ext = strtolower($ext[count($ext)-1]);
                    $lastimage = $ext;


                    return ($firstimage != 'png') ? -1 : 1;
                });
              ?>
              <p class="fs-14" style="color: #999;">Attachment</p>
                <div class="row">
                  @foreach($files as $file)
                    <?php
                        $ext = explode('.',$file[0]);
                        $ext = strtolower($ext[count($ext)-1]);
                        $filename = $file[1];
                        $img = asset_url('uploads/fileupload/discussion/'.$file[0]);
                        if($ext == 'pdf'){
                          $img = asset('img/pdf.png');
                        }elseif ($ext == 'xlsx') {
                          $img = asset('img/excel.png');
                        }elseif ($ext == 'docx') {
                          $img = asset('img/word.png');
                        }
                     ?>
                    @if($ext == 'png' || $ext == 'jpeg' || $ext == 'jpg')
                      <div class="col-lg-3">
                        <div class="img-attachment">
                          <img src="{{$img}}" style="width:100%;height: 90px;border-radius: 11px;margin-bottom: 10px;" alt="{{$filename}}" title="{{$filename}}">
                          <a class="groups-title" href="{{asset_url('uploads/fileUpload/discussion/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                        </div>
                      </div>
                    @else
                    <div class="col-lg-12">
                      <div class="img-attachment">
                        <img src="{{$img}}" style="width:50px;margin-right:20px;" alt="{{$filename}}" title="{{$filename}}">
                        <a class="groups-title" href="{{asset_url('uploads/fileUpload/discussion/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                      </div>
                    </div>
                    @endif
                  @endforeach
                </div>
              @endif
              <hr>
            @endforeach
          </div>
        </div>
    </div>
  </div>
@endsection

@push('script')
  {{-- CKEDITOR --}}

  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>

  <!-- The template to display files available for upload -->
  <script>
    $(".komentar-first").click(function(){
      $(".komentar-second").removeClass('hidden');
      $(".komentar-first").addClass('hidden');
    });

    $("#cancel-komen").click(function(){
      $(".komentar-first").removeClass('hidden');
      $(".komentar-second").addClass('hidden');
    });

    $("#add-file-jquery-upload").change(function(){
      $(".jQuery-upload-btn").removeClass('hidden');
    });

    $("#cancel-jquery-upload").click(function(){
      $(".jQuery-upload-btn").addClass('hidden');
    });

    function versandkosten() {
        var lengthTable = $('.template-upload').length;
        if(lengthTable == 0){
          $(".jQuery-upload-btn").addClass('hidden');
        }
    }

    var basketAmount = $('.table-files-upload');
    basketAmount.bind("DOMSubtreeModified", versandkosten);
  </script>
  <script src="{{asset('uploadify-master/sample/jquery.uploadifive.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
		<?php $timestamp = time();?>
		$(function() {
      var token = '{{ csrf_token() }}';
			$('#file_upload').uploadifive({
				'auto'             : true,
				'formData'         : {
									   'timestamp' : '<?php echo $timestamp;?>',
									   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
                     '_token': token
				                     },
       'method'            : 'POST',
				'queueID'          : 'queue',
				'uploadScript'     : '{{route("uploadify", "discussion")}}',
        'onUploadComplete' : function(file, data) { console.log(data); },
				'onCancel' : function(file) {
          var token = '{{ csrf_token() }}';
          $.ajax({
            url : "{{route('uploadify.cancel', 'update')}}",
            type : 'POST',
            data : {
              'data' : file.originName,
              '_token' : '{{csrf_token()}}'
            },
            success : function(result){
              console.log(result);
            }
          });
         },
       'buttonText' : `<img src="{{asset('img/folder.svg')}}" class="img-file"> <span class="add-file">{{Lang::get('front.instructor_group.action_add_file')}}</span>`
			});
		});

	</script>
  <style media="screen">
    .img-file{
      width: 20px;
    }

    .add-file{
      margin-left: 2px;
    }

    .uploadifive-button {
        float: left;
        margin-right: 10px;
        background: none;
        color: black;
        border: none;
        right: 12px;
        cursor: all-scroll;
    }

    .uploadifive-button {
      float: left;
      margin-right: 10px;
    }

    #queue {
        overflow: auto;
        padding: 0 3px 3px;
        width: 100%;
    }

    .uploadifive-button:hover {
        background: none !important;
    }
  </style>
@endpush
