@extends('layouts.app')

@section('title', 'Cart')

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Shopping <span>Cart</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>Shopping Cart</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3 class="headline-md headline-line">You Have {{Cart::count()}} of Your Shopping List</h3>
        </div>
        <div class="col-md-12">
          <div class="card-md">
            <div class="card-block">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($carts as $cart)
                    <tr>
                      <td width="20">
                        <p><strong>{{$cart->name}}</strong></p>
                        <p><img height="80" src="{{ $cart->options->type == 'program' ? $cart->options->image : $cart->options->image }}" alt="{{$cart->name}}"></p>
                      </td>
                      <td>{{rupiah($cart->price)}}</td>
                      <td>
                        <a onclick="return confirm('Are you sure want to remove this product?')" href="{{url('cart/remove/'.$cart->rowId)}}">Delete</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="1">&nbsp;</td>
                    <td>Subtotal</td>
                    <td>RM {{Cart::subtotal()}}</td>
                  </tr>
                  <tr>
                    @if(Cart::count() === 0)
                      <td colspan="3"><a class="btn btn-primary btn-lg btn-block btn-raised" href="{{url('/')}}">Back To Homepage</a></td>
                    @else
                      <td colspan="3">
                        @php
                        if($cart->options->affiliate){
                          $affiliate = $cart->options->affiliate;
                        }else{
                          $affiliate = '';
                        }
                        @endphp

                        <a class="btn btn-success btn-lg btn-block btn-raised" href="/cart/checkout/v2{{$affiliate}}">Checkout</a>
                        <a class="btn btn-primary btn-lg btn-block btn-raised" href="{{url('/courses')}}">Continue Shopping</a>

                      </td>
                    @endif
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
