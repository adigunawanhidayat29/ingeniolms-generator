@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
  <div class="container mt-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">{{Cart::count()}} product in a cart</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                @php $subtotal = 0 @endphp
                @foreach($carts as $cart)
                  @php $subtotal += $cart->price; @endphp
                  <tr>
                    <td width="20">
                      <p><strong>{{$cart->name}}</strong></p>
                      <p><img height="80" src="{{ $cart->options->type == 'program' ? $cart->options->image : $cart->options->image }}" alt="{{$cart->name}}"></p>
                    </td>
                    {{-- <td>{{rupiah($cart->price)}}</td> --}}
                    <td>{{rupiah($cart->price)}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if(Session::get('error'))
              <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            @if(Session::get('success'))
              <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            <form class="" action="/cart/promotion" method="post">
              {{csrf_field()}}
              <p>Utilize Levy</p>
              <input autocomplete="off" style="padding:5px;" type="text" name="discount_code" value="{{old('discount_code')}}" placeholder="Utilize Levy">
              <input type="submit" class="btn btn-info btn-raised" value="Check">
            </form>
          </div>
          <div class="col-md-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Payment</h3>
              </div>
              <div class="panel-body">
                <p>
                  @if(Session::get('promotion'))
                    Promo : {{Session::get('promotion')->name}} - {{Session::get('promotion')->discount . "%"}}
                    @php
                      $price_discount = ($subtotal * Session::get('promotion')->discount) / 100;
                    @endphp
                    <h3 style="margin: 0;">Total : <span style="font-weight: 700;">{{rupiah($subtotal - $price_discount)}}</span></h3>
                  @else
                    <h3 style="margin: 0;">Total : <span style="font-weight: 700;">RM {{Cart::subtotal()}}</span></h3>
                  @endif

                  @php
                    if(Request::get('aff')) {
                      $affiliate = '?aff='.Request::get('aff');
                    }else{
                      $affiliate = '';
                    }
                  @endphp
                </p>

                <div style="margin-bottom: 1rem;">
                  <p style="font-size: 16px; margin-bottom: 0;">Payment Method</p>
                  <img src="/img/starvisionit/pay_with_billplz_all.png" alt="" style="width: 300px;">
                </div>

                @if(Session::get('promotion'))
                  @if(Session::get('promotion')->discount != 100)
                    <div class="form-group" style="margin-top: 0;">
                      <a href="/pembayaran" class="btn btn-primary btn-raised">Proceed Payment</a>
                    </div>
                  @else
                    {{Form::open(['url' => 'transaction/buy'.$affiliate, 'method' => 'post'])}}
                      <div class="form-group" style="margin-top: 0;">
                        <input type="hidden" name="method" value="discount 100%">
                        {{-- <label for="method_transfer">Discount 100%</label> --}}
                      </div>

                      <div class="form-group" style="margin-top: 0;">
                        <input type="submit" name="submit" value="Dapatkan Gratis" class="btn btn-success btn-raised">
                      </div>
                    {{Form::close()}}
                  @endif
                @else
                  <div class="form-group" style="margin-top: 0;">
                    <a href="/pembayaran" class="btn btn-primary btn-raised">Proceed Payment</a>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    $("#information_transfer").show();
    $("#information_card").hide();

    $("input[name=method]").click(function(){
      if($(this).val() == 'transfer'){
        $("#information_transfer").show();
        $("#information_card").hide();
      }else{
        $("#information_card").show();
        $("#information_transfer").hide();
      }
    });
  </script>
@endpush
