@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
  <div class="container mt-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">{{Cart::count()}} Produk dalam keranjang </h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Produk</th>
                  <th>Harga</th>
                </tr>
              </thead>
              <tbody>
                @php $subtotal = 0 @endphp
                @foreach($carts as $cart)
                  @php $subtotal += $cart->price; @endphp
                  <tr>
                    <td width="20">
                      <p><strong>{{$cart->name}}</strong></p>
                      <p><img height="80" src="{{ $cart->options->type == 'program' ? asset_url($cart->options->image) : asset_url(course_image_small($cart->options->image)) }}" alt="{{$cart->name}}"></p>
                    </td>
                    <td>{{rupiah($cart->price)}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if(Session::get('error'))
              <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            @if(Session::get('success'))
              <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            <form class="" action="/cart/promotion" method="post">
              {{csrf_field()}}
              <p>Punya kode promo?</p>
              <input style="padding:5px;" type="text" name="discount_code" value="{{old('discount_code')}}" placeholder="Masukan kode promo">
              <input type="submit" class="btn btn-info btn-raised" value="Cek">
            </form>
          </div>
          <div class="col-md-8">

            @if(Session::get('promotion'))
              Promo : {{Session::get('promotion')->name}} - {{Session::get('promotion')->discount . "%"}}
              @php
                $price_discount = ($subtotal * Session::get('promotion')->discount) / 100;
              @endphp
              <h3>Total : <strong>{{rupiah($subtotal - $price_discount)}}</strong></h3>
            @else
              <h3>Total : <strong>Rp {{Cart::subtotal()}}</strong></h3>
            @endif

            @php
              if(Request::get('aff')) {
                $affiliate = '?aff='.Request::get('aff');
              }else{
                $affiliate = '';
              }
            @endphp

            {{Form::open(['url' => 'transaction/buy'.$affiliate, 'method' => 'post'])}}


                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Pilih Metode Pembayaran</h3>
                    </div>
                    <div class="panel-body">

                      @if(Session::get('promotion'))
                        @if(Session::get('promotion')->discount != 100)
                          <div class="form-group">
                            <input type="radio" name="method" id="method_transfer" value="transfer" checked>
                            <label for="method_transfer">Transfer Bank</label>
                          </div>

                          <div id="information_transfer">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                      <img height="30" src="http://bukaloker.com/wp-content/uploads/2017/12/Bank-Mandiri-Persero-Tbk.-1.png" alt="">
                                      <br><br>
                                      <p>No Rekening : 130-00-1331794-9</p>
                                      <p>Atas Nama : IDELEARNING</p>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="form-group">
                            <input type="radio" name="method" value="discount 100%" checked>
                            <label for="method_transfer">Discount 100%</label>
                          </div>
                        @endif

                      @else
                        <div class="form-group">
                          <input type="radio" name="method" id="method_transfer" value="transfer" checked>
                          <label for="method_transfer">Transfer Bank</label>
                        </div>

                        <div id="information_transfer">
                          <div class="panel panel-default">
                            <div class="panel-body">
                              <div class="row">
                                  <div class="col-md-4">
                                    <img height="30" src="http://bukaloker.com/wp-content/uploads/2017/12/Bank-Mandiri-Persero-Tbk.-1.png" alt="">
                                    <br><br>
                                    <p>No Rekening : 130-00-1331794-9</p>
                                    <p>Atas Nama : IDELEARNING</p>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @endif

                      @if(Session::get('promotion'))
                        @if(Session::get('promotion')->discount != 100)
                          <div class="form-group">
                            <input type="submit" name="submit" value="Lanjutkan Pembayaran" class="btn btn-success btn-raised">
                          </div>
                        @else
                          <div class="form-group">
                            <input type="submit" name="submit" value="Dapatkan Gratis" class="btn btn-success btn-raised">
                          </div>
                        @endif
                      @else
                        <div class="form-group">
                          <input type="submit" name="submit" value="Lanjutkan Pembayaran" class="btn btn-success btn-raised">
                        </div>
                      @endif

                    </div>
                  </div>

            {{Form::close()}}
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    $("#information_transfer").show();
    $("#information_card").hide();
    $("input[name=method]").click(function(){
      if($(this).val() == 'transfer'){
        $("#information_transfer").show(500);
        $("#information_card").hide(500);
      }else{
        $("#information_card").show(500);
        $("#information_transfer").hide(500);
      }
    });
  </script>
@endpush
