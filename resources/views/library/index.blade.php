@extends('layouts.app_nation')

@section('title', Lang::get('front.page_library.title'))

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
  <style>
    .table-cs .dataTables_length {
      font-size: 20px;
    }

    .table-cs input {
      width: 100%;
      font-size: 14px;
      border: 1px solid #ccc;
      border-radius: 5px;
      box-shadow: none;
      padding: 0.75rem 1.5rem;
      margin: 0;
    }

    .table-cs thead>tr>th {
      border-bottom: none;
      padding: 8px 10px;
    }

    .table-cs tbody>tr>td {
      background: white !important;
    }

    .table-cs .dataTables_paginate .paginate_button {
    }

    .table-cs .dataTables_paginate span a.paginate_button {
      border-radius: 3px;
      padding: 0.25rem 0.75rem;
    }

    .table-cs .dataTables_paginate span a.paginate_button.current {
      border: 0px;
      background: #4ca3d9;
      color: #fff !important;
    }

    .table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after {
			display: none !important
    }

    .table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child {
			width: 0.1px !important;
			padding: 0;
    }

    .table-cs .dataTables_info {
      display: none;
    }

		.table-cs .table td, .table-cs .table th {
			vertical-align: middle;
		}

    .dataTables_filter {
      /* margin-bottom: 15px; */
    }
	</style>

	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>

	<style>
		.modal .modal-dialog .modal-content .modal-body.library-modal-tab {
      display: flex;
      padding: 0;
    }

    .library-modal-tab .tab {
      background-color: #f1f1f1;
      width: 30%;
      height: auto;
    }

    .library-modal-tab .tab .spacer {
      background: #fcfcfc;
      height: 5px;
    }

    .library-modal-tab .tab button {
      display: block;
      background-color: inherit;
      color: #424242;
      padding: 1.5rem 2.5rem;
      width: 100%;
      border: none;
      outline: none;
      text-align: left;
      cursor: pointer;
      transition: 0.3s;
      font-size: 14px;
    }

    .library-modal-tab .tab button:hover {
      background-color: #ddd;
    }

    .library-modal-tab .tab button.active {
      background-color: #ccc;
    }

    .library-modal-tab .tabcontent {
      background: #fcfcfc;
      width: 70%;
      height: auto;
      padding: 2rem;
    }

    .library-modal-tab .tabcontent h3 {
      margin-top: 0;
      margin-bottom: 1rem;
    }

		.placeholder{
			background-color:white;
			height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}

		/* .library-direc{
			display: flex;
			padding: 20px;
			font-size: 19px;
    	font-family: cursive;
		}

		.library-direc span{
			color: #4ca3d9;
	    font-size: 25px;
	    margin-right: 20px;
		}

		.direc:hover{
			background:  #4ca3d9;
			color: white !important;
		}

		.direc:hover .library-direc{
			color: white !important;
		}

		.direc:hover .library-direc span{
			color: white !important;
		} */

		/* .nav-tabs-ver-container-content{
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.tab-content-share .tab{
			min-height: 180px;
			margin-bottom: 25px;
		}

		.tab-content-share li{
			width: 100%;
		}

		.tab-content-share li a.active{
			background: #ccc;
		}

		.tab-content-share .nav-tabs-ver-container-content{
			min-height: 180px;
		}

		.tab-content-share .nav-tabs-ver-container-content .tab-pane{
			background: white;
    	color: black;
		}

		.tab-content-share .nav-tabs-ver-container-content .card-block{
			padding: 0;
		}

		.tab-content-share li a{
			display: block;
	    background-color: inherit;
	    color: black;
	    padding: 22px 16px;
	    width: 100%;
	    border: none;
	    outline: none;
	    text-align: left;
	    cursor: pointer;
	    transition: 0.3s;
	    font-size: 17px;
		} */

		.dropdown-menu.dropdown-menu-primary li a i{
			padding: 5px;
			border-radius: 5px;
			width: 25px;
			text-align: center;
		}

		.active, .site-dashboard-panel__nav:hover {
			background-color: #7c7c7c;
			background: none;
			color: black;
		}
	</style>
	<style>
		.card.card-library {
			background: #f9f9f9;
			color: #424242;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
		}

		.card.card-library.card-library-hover:hover {
			background: #f3f3f3;
		}

		.modal .modal-dialog .modal-content .modal-header {
      border-bottom: 1px solid #eee;
    }

    .modal .modal-dialog .modal-content .modal-header .modal-title {
      width: 100%;
      padding: 2rem 3rem;
    }

    .modal .modal-dialog .modal-content .modal-header .close {
      line-height: 1.65;
      margin: 2rem 3rem;
		}

		.modal .modal-dialog .modal-content .modal-body .form-group {
			margin: 0;
		}

		.modal .modal-dialog .modal-content .modal-body .card-library {
			border-radius: 0;
			box-shadow: none;
		}
	</style>
@endpush

@section('content')
	<div class="bg-page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php $request = Request::all(); ?>
					@if(isset($request['directory']))
						<h2 class="headline-md no-m">@lang('front.page_library.title') &bull; {{$directory->where('id', $request['directory'])->first()->directory_name}}</h2>
					@else
						<h2 class="headline-md no-m">@lang('front.page_library.title')</h2>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li>@lang('front.page_library.title')</li>
					@if(isset($request['directory']))
						<li>{{$directory->where('id', $request['directory'])->first()->directory_name}}</li>
					@else
						<li>My Library</li>
					@endif
				</ul>
			</div>
		</div>
	</div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				@foreach($directory as $index => $item)
					<div class="col-sm-4">
						<a class="card card-library card-library-hover" href="{{$index == 0 ? url('/instructor/library') : '?directory='.$item->id}}">
							<div class="card-block">
								<div class="fs-20">
									<span class="fa fa-folder-open" aria-hidden="true"></span>
									{{$item->directory_name}}
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
			@if(!isset($request['directory']))
				<a href="#" data-toggle="modal" data-target="#ModalMenuContent" class="btn btn-primary btn-raised">@lang('front.page_library.add_new_content_title')</a>
				<a href="#" data-toggle="modal" data-target="#ModalSharedGroupt" class="btn btn-primary btn-raised">@lang('front.page_library.add_new_folder')</a>
				<a href="#" data-toggle="modal" data-target="#ModalAddBankSoal" class="btn btn-primary btn-raised">@lang('front.page_library.add_new_storage_content')</a>
			@endif
			<div class="card card-library">

				<?php
					$shared_cek = false;
					if(isset($request['directory'])){
						if($request['directory'] == '3'){
							$shared_cek = true;
						}
					}
				?>

				@if($shared_cek)
					<!-- Nav tabs -->
					<ul class="nav nav-tabs shadow-2dp" role="tablist">
						<li class="nav-item">
							<a class="nav-link withoutripple active" href="#sharedFile" aria-controls="sharedFile" role="tab" data-toggle="tab" aria-selected="false">
								<i class="fa fa-file"></i>
								<span class="d-none d-sm-inline">@lang('front.page_library.shared_file')</span>
							</a>
						</li>
						<li class="nav-item current">
							<a class="nav-link withoutripple" href="#sharedFolder" aria-controls="sharedFolder" role="tab" data-toggle="tab" aria-selected="true">
								<i class="fa fa-folder-open"></i>
								<span class="d-none d-sm-inline">@lang('front.page_library.shared_folder')</span>
							</a>
						</li>
					</ul>
					<a class="btn btn-white btn-raised d-none d-sm-inline" href="#" data-toggle="modal" data-target="#ModalAddCodet" style="position: absolute; right: 20px;">
						<i class="fa fa-plus"></i>
						<span class="d-none d-sm-inline">@lang('front.page_library.add_new_content_folder')</span>
					</a>
					<a class="btn-circle btn-circle-sm btn-circle-default btn-circle-raised d-sm-none d-inline" href="#" data-toggle="modal" data-target="#ModalAddCodet" style="position: absolute; top: 7.5px; right: 10px;">
						<i class="fa fa-plus"></i>
					</a>
					<div class="modal" id="ModalAddCodet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h3 class="modal-title" id="myModalLabel">@lang('front.page_library.add_new_content_folder')</h3>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
								</div>
								<div class="modal-body library-modal-tab">
									<div class="tab">
										<button class="tablinks" onclick="openCity(event, 'KodeFolder')"><i class="fa fa-folder"></i> @lang('front.page_library.folder_code')</button>
										<button class="tablinks" onclick="openCity(event, 'KodeKonten')" id="defaultOpen"><i class="fa fa-file-text"></i> @lang('front.page_library.content_code')</button>
									</div>

									<div id="KodeFolder" class="tabcontent">
										<h3>@lang('front.page_library.add_folder_code')</h3>
										<form action="{{url('/course/folder/library/add/share/')}}" method="post">
											<input type="hidden" name="_token" value="{{csrf_token()}}">
											<input type="text" name="shared_code" class="form-control" placeholder="@lang('front.page_library.folder_code')">
											<button type="submit" class="btn btn-primary btn-raised">Tambah</button>
										</form>
									</div>
									<div id="KodeKonten" class="tabcontent">
										<h3>@lang('front.page_library.add_content_code')</h3>
										<form action="{{url('/course/content/library/add/share/')}}" method="post">
											<input type="hidden" name="_token" value="{{csrf_token()}}">
											<input type="text" name="shared_code" class="form-control" placeholder="@lang('front.page_library.content_code')">
											<button type="submit" class="btn btn-primary btn-raised">@lang('front.page_library.add')</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-block">
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade active show" id="sharedFile">
								<div class="table-responsive table-cs">
									<table id="example-1" class="table table-striped d-table">
										<thead>
											<tr>
												<th style="min-width: 25vh;">@lang('front.page_library.coloum_content_name')</th>
												<th>@lang('front.page_library.coloum_created_at')</th>
												<th class="text-center">@lang('front.page_library.coloum_directory')</th>
												<th class="text-center">@lang('front.page_library.coloum_type')</th>
												<th class="text-center">@lang('front.page_library.coloum_code')</th>
												<th class="text-center">@lang('front.page_library.coloum_action')</th>
											</tr>
										</thead>
										<tbody>
											@foreach($directory as $index => $item)
												@foreach($item->library as $index => $library)
													@if($library->content)
														<tr>
															<td style="min-width: 25vh;"><i class="{{content_type_icon($library->content->type_content)}} mr-1"></i> {{$library->content->title}}</td>
															<td>{{$library->content->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<td class="text-center">{{$library->content->type_content}}</td>
															<td class="text-center color-primary">{{$library->shared_code}}</td>
															<td class="text-center">
																@isset($request['directory'])
																	@if($library->user_id == Auth::user()->id)
																	<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
																	<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																			<div class="modal-content">
																				<div class="modal-body">
																					<div class="d-flex align-items-center justify-content-between mb-2">
																						<h3 class="headline headline-sm m-0">@lang('front.page_library.modal_return_confirm_title')</h3>
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																							<span aria-hidden="true">
																								<i class="zmdi zmdi-close"></i>
																							</span>
																						</button>
																					</div>
																					<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																						<input type="hidden" name="_token" value="{{csrf_token()}}">
																						<div class="form-group text-center">
																							<p>@lang('front.page_library.modal_return_confirm_question')</p>
																							<p style="color:red"><i> @lang('front.page_library.modal_return_confirm_attention')</i></p>
																							<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">@lang('front.general.text_cancel')</button>
																							<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.page_library.text_return')</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																	@else
																		@if($library->shared->where('user_id', Auth::user()->id)->first())
																		<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																		<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																				<div class="modal-content">
																					<div class="modal-body">
																						<div class="d-flex align-items-center justify-content-between mb-2">
																							<h3 class="headline headline-sm m-0">@lang('front.page_library.modal_delete_confirm_title')</h3>
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">
																									<i class="zmdi zmdi-close"></i>
																								</span>
																							</button>
																						</div>
																						<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<div class="form-group text-center">
																								<p>@lang('front.page_library.modal_delete_confirm_title')</p>
																								<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">@lang('front.general.text_cancel')</button>
																								<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																							</div>
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																		@endif
																	@endif
																@endif
															</td>
														</tr>
													@elseif($library->quiz)
														<tr>
															<td style="min-width: 25vh;"><i class="fa fa-{{$library->quiz->quizz_type == 'quiz' ? 'star' : 'edit'}} mr-1"></i> {{$library->quiz->name}}</td>
															<td>{{$library->quiz->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<td class="text-center">quiz</td>
															<td class="text-center color-primary">{{$library->shared_code}}</td>
															<td class="text-center">
																@isset($request['directory'])
																	@if($library->user_id == Auth::user()->id)
																	<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
																	<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																			<div class="modal-content">
																				<div class="modal-body">
																					<div class="d-flex align-items-center justify-content-between mb-2">
																						<h3 class="headline headline-sm m-0">Kembalikan Kontent</h3>
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																							<span aria-hidden="true">
																								<i class="zmdi zmdi-close"></i>
																							</span>
																						</button>
																					</div>
																					<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																						<input type="hidden" name="_token" value="{{csrf_token()}}">
																						<div class="form-group text-center">
																							<p>Apa anda yakin mengembalikan kontent anda?</p>
																							<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																							<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																							<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																	@else
																		@if($library->shared->where('user_id', Auth::user()->id)->first())
																		<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																		<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																				<div class="modal-content">
																					<div class="modal-body">
																						<div class="d-flex align-items-center justify-content-between mb-2">
																							<h3 class="headline headline-sm m-0">Hapus Tugas</h3>
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">
																									<i class="zmdi zmdi-close"></i>
																								</span>
																							</button>
																						</div>
																						<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<div class="form-group text-center">
																								<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																								<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																								<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																							</div>
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																		@endif
																	@endif
																@endif
															</td>
														</tr>
													@elseif($library->assignment)
														<tr>
															<td style="min-width: 25vh;"><i class="fa fa-tasks mr-1"></i> {{$library->assignment->title}}</td>
															<td>{{$library->assignment->created_at}}</td>
															<td class="text-center">-</td>
															<td class="text-center">Tugas</td>
															<td class="text-center color-primary">{{$library->shared_code}}</td>
															<td class="text-center">
																@isset($request['directory'])
																	@if($library->user_id == Auth::user()->id)
																	<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
																	<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																		<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																			<div class="modal-content">
																				<div class="modal-body">
																					<div class="d-flex align-items-center justify-content-between mb-2">
																						<h3 class="headline headline-sm m-0">Kembalikan Kontent</h3>
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																							<span aria-hidden="true">
																								<i class="zmdi zmdi-close"></i>
																							</span>
																						</button>
																					</div>
																					<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																						<input type="hidden" name="_token" value="{{csrf_token()}}">
																						<div class="form-group text-center">
																							<p>Apa anda yakin mengembalikan kontent anda?</p>
																							<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																							<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																							<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																	@else
																		@if($library->shared->where('user_id', Auth::user()->id)->first())
																		<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																		<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																				<div class="modal-content">
																					<div class="modal-body">
																						<div class="d-flex align-items-center justify-content-between mb-2">
																							<h3 class="headline headline-sm m-0">Hapus Tugas</h3>
																							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">
																									<i class="zmdi zmdi-close"></i>
																								</span>
																							</button>
																						</div>
																						<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<div class="form-group text-center">
																								<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																								<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																								<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																							</div>
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																		@endif
																	@endif
																@endif
															</td>
														</tr>
													@endif
												@endforeach
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="sharedFolder">
								<div class="table-responsive table-cs">
									<table id="example2" class="table table-striped d-table">
										<thead>
											<tr>
												<th style="min-width: 25vh;">@lang('front.page_library.coloum_folder_name')</th>
												<th>@lang('front.page_library.coloum_created_at')</th>
												<th class="text-center">@lang('front.page_library.coloum_directory')</th>
												<th class="text-center">@lang('front.page_library.coloum_type')</th>
												<th class="text-center">@lang('front.page_library.coloum_code')</th>
												<th class="text-center">@lang('front.page_library.coloum_action')</th>
											</tr>
										</thead>
										<tbody>
											@foreach($group as $groups)
												<tr>
													<td style="min-width: 25vh;"><a href="#" data-toggle="modal" data-target="#ModalSharedGrouptDetail-{{$groups->id}}"><i class="fa fa-folder-open mr-1" style="color: #4ca3d9;"></i> {{$groups->group_name}}</a></td>
													<td>{{$groups->created_at}}</td>
													<td class="text-center">{{$item->directory_name}}</td>
													<td class="text-center">Folder Shared</td>
													<td class="text-center color-primary">{{$groups->group_code}}</td>
													<td class="text-center">
														@if(Auth::user()->id == $groups->user_id)
														<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollbackGroup-{{$groups->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
														<div class="modal" id="ModalRollbackGroup-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																<div class="modal-content">
																	<div class="modal-body">
																		<div class="d-flex align-items-center justify-content-between mb-2">
																			<h3 class="headline headline-sm m-0">Kembalikan Folder</h3>
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">
																					<i class="zmdi zmdi-close"></i>
																				</span>
																			</button>
																		</div>
																		<form class="text-left" action="{{url('/library/group/shared/rollback/'.$groups->id)}}" method="post">
																			<input type="hidden" name="_token" value="{{csrf_token()}}">
																			<div class="form-group text-center">
																				<p>Apa anda yakin mengembalikan folder anda?</p>
																				<p style="color:red"><i>* Bila folder dibagikan, maka semua yang berbagi dengan folder anda akan terhapus</i></p>
																				<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																				<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
														@else
														<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeleteGroup-{{$groups->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
														<div class="modal" id="ModalDeleteGroup-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																<div class="modal-content">
																	<div class="modal-body">
																		<div class="d-flex align-items-center justify-content-between mb-2">
																			<h3 class="headline headline-sm m-0">Hapus Folder</h3>
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">
																					<i class="zmdi zmdi-close"></i>
																				</span>
																			</button>
																		</div>
																		<form class="text-left" action="{{url('/library/group/shared/destroy/'.$groups->id)}}" method="post">
																			<input type="hidden" name="_token" value="{{csrf_token()}}">
																			<div class="form-group text-center">
																				<p>Apa anda yakin menghapus folder yang berbagi dengan anda?</p>
																				<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																				<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
														@endif
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				@else
					<div class="card-block">
						<div class="table-responsive table-cs">
							<table id="example" class="table table-striped d-table">
								<thead>
									<tr>
										<th style="min-width: 25vh;">@lang('front.page_library.coloum_title')</th>
										<th>@lang('front.page_library.coloum_created_at')</th>
										<th class="text-center">@lang('front.page_library.coloum_directory')</th>
										<th class="text-center">@lang('front.page_library.coloum_type')</th>
										@if(isset($request['directory']))
											@if($request['directory'] == '3')
												<th class="text-center">Code</th>
											@endif
										@endif
										<th class="text-center">@lang('front.page_library.coloum_action')</th>
									</tr>
								</thead>
								<tbody>
									@foreach($group as $groups)
										<tr style="color: #4ca3d9;">
											<td>
												@if($groups->question_bank == 1)
												<a href="{{url('library/bank/soal/show', $groups->id)}}"><i class="fa fa-folder mr-1"></i> {{$groups->group_name}}</a>
												@else
												<a href="#" data-toggle="modal" data-target="#ModalSharedGrouptDetail-{{$groups->id}}"><i class="fa fa-folder-open mr-1"></i> {{$groups->group_name}}</a>
												@endif
											</td>
											<td>{{$groups->created_at}}</td>
											<td class="text-center">
												@if($groups->user_id == Auth::user()->id)
													@if($groups->group_code == null && $groups->status == '0')
														My Library
													@elseif($groups->status == '1')
														Public Library
													@else
														Shared Library
													@endif
												@else
													Shared Library
												@endif
											</td>
											<td class="text-center">{{$groups->question_bank == 1 ? 'Bank Soal' : 'Folder'}}</td>
											<td>
												@isset($request['directory'])
													@if($groups->user_id == Auth::user()->id)
													<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalGroupRollback-{{$groups->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
													<div class="modal" id="ModalGroupRollback-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Kembalikan Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<form class="text-left" action="{{url('/library/group/shared/rollback/'.$groups->id)}}" method="post">
																		<input type="hidden" name="_token" value="{{csrf_token()}}">
																		<div class="form-group text-center">
																			<p>Apa anda yakin mengembalikan folder anda?</p>
																			<p style="color:red"><i>* Bila folder dibagikan, maka semua yang berbagi dengan folder anda akan terhapus</i></p>
																			<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																			<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
													@endif
												@else
													@if(Auth::user()->id == $groups->user_id)
													@if($groups->question_bank != 1)
													<a href="#" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-success" data-toggle="modal" data-target="#ModalSharedGrouptAdd-{{$groups->id}}" title="Tambah konten"><i class="fa fa-plus"></i></a>
													@endif
													<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" data-toggle="modal" data-target="#ModalSharedContentGroup-{{$groups->id}}" href="javascript:void(0)" data-toggle="modal" data-target="#ModalSharedContent-{{$groups->id}}" title="Share"><i class="fa fa-share-alt"></i></a>
													<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-info"  data-toggle="modal" data-target="#ModalSharedGrouptEdit-{{$groups->id}}" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>
													<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalDeleteGroup-{{$groups->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
													<div class="modal" id="ModalDeleteGroup-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Hapus Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<form class="text-left" action="{{url('/library/group/destroy/'.$groups->id)}}" method="post">
																		<input type="hidden" name="_token" value="{{csrf_token()}}">
																		<div class="form-group text-center">
																			<p>Apa anda yakin menghapus folder anda?</p>
																			<p style="color:red"><i>* Bila folder dibagikan, maka semua yang berbagi dengan folder anda akan terhapus</i></p>
																			<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																			<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
													@else
													<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeleteGroup-{{$groups->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
													<div class="modal" id="ModalDeleteGroup-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Hapus Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<form class="text-left" action="{{url('/library/group/shared/destroy/'.$groups->id)}}" method="post">
																		<input type="hidden" name="_token" value="{{csrf_token()}}">
																		<div class="form-group text-center">
																			<p>Apa anda yakin menghapus folder yang berbagi dengan anda?</p>
																			<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																			<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
													@endif
													@if($groups->question_bank != 1)
													<div class="modal" id="ModalSharedGrouptDetail-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Konten / Kuis Folder Anda</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<div class="table-responsive table-cs">
																		<table class="display example222" style="width:100%">
																			<thead>
																				<tr>
																					<th style="width:20px;"></th>
																					<th>Nama Konten</th>
																					<th>Tanggal Dibuat</th>
																					<th class="text-center">Tipe</th>
																					<th style="width:20px;"></th>
																				</tr>
																			</thead>
																			<tbody>
																				@foreach($groups->contents as $index => $item)
																					<?php $library = $item->library;?>
																					@if($library->content)
																						<tr>
																							<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
																							<td>{{$library->content->title}}</td>
																							<td>{{$library->content->created_at}}</td>
																							<td class="text-center">{{$library->content->type_content}}</td>
																							<td style="width:20px;" class="text-center"><i class="fa fa-trash text-danger" style="cursor:pointer"></i></td>
																						</tr>
																					@elseif($library->quiz)
																						<tr>
																							<td class="text-center"><i class="fa fa-{{$library->quiz->quizz_type == 'quiz' ? 'star' : 'edit'}}"></i></td>
																							<td>{{$library->quiz->name}}</td>
																							<td>{{$library->quiz->created_at}}</td>
																							<td class="text-center">quiz</td>
																							<td style="width:20px;" class="text-center"><i class="fa fa-trash text-danger" style="cursor:pointer"></i></td>
																						</tr>
																					@elseif($library->assignment)
																						<tr>
																							<td class="text-center"><i class="fa fa-tasks"></i></td>
																							<td>{{$library->assignment->title}}</td>
																							<td>{{$library->assignment->created_at}}</td>
																							<td class="text-center">Tugas</td>
																							<td style="width:20px;" class="text-center"><i class="fa fa-trash text-danger" style="cursor:pointer"></i></td>
																						</tr>
																					@endif
																				@endforeach
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="modal" id="ModalSharedGrouptAdd-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Tambah Konten Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<form class="text-left" action="{{url('/library/group/add/content/'.$groups->id)}}" method="post">
																		<input type="hidden" name="_token" value="{{csrf_token()}}">
																		<div class="form-group">
																			<div class="table-responsive table-cs">
																				<table class="table table-striped d-table edit-group">
																					<thead>
																						<tr>
																							<th style="width:20px;"></th>
																							<th style="width:20px;"></th>
																							<th>Nama Konten</th>
																							<th>Tanggal Dibuat</th>
																							<th class="text-center">Directory</th>
																							<th class="text-center">Tipe</th>
																						</tr>
																					</thead>
																					<tbody>
																						@foreach($directory as $index => $item)
																							@foreach($item->library as $index => $library)
																								@if($library->content)
																									<tr>
																										<td style="width:20px; padding: 0;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																										<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
																										<td>{{$library->content->title}}</td>
																										<td>{{$library->content->created_at}}</td>
																										<td class="text-center">{{$item->directory_name}}</td>
																										<td class="text-center">{{$library->content->type_content}}</td>
																									</tr>
																								@elseif($library->quiz)
																									<tr>
																										<td style="width:20px; padding: 0;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																										<td class="text-center"><i class="fa fa-{{$library->quiz->quizz_type == 'quiz' ? 'star' : 'edit'}}"></i></td>
																										<td>{{$library->quiz->name}}</td>
																										<td>{{$library->quiz->created_at}}</td>
																										<td class="text-center">{{$item->directory_name}}</td>
																										<td class="text-center">quiz</td>
																									</tr>
																								@elseif($library->assignment)
																									<tr>
																										<td style="width:20px; padding: 0;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																										<td class="text-center"><i class="fa fa-tasks"></i></td>
																										<td>{{$library->assignment->title}}</td>
																										<td>{{$library->assignment->created_at}}</td>
																										<td class="text-center">-</td>
																										<td class="text-center">Tugas</td>
																									</tr>
																								@endif
																							@endforeach
																						@endforeach
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<button type="submit" class="btn btn-primary btn-raised m-0">Tambah Konten</button>
																	</form>
																</div>
															</div>
														</div>
													</div>
													@endif

													<div class="modal" id="ModalSharedContent-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Berbagi Konten / Kuis</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<div class="tab-content-share">
																		<div class="tab">
																			<ul class="nav nav-tabs-ver" role="tablist">
																				<li class="nav-item">
																					<a class="nav-link active" href="#home-{{$groups->id}}" aria-controls="home-{{$groups->id}}" role="tab" data-toggle="tab" aria-selected="false">
																						<i class="fa fa-tags"></i> Public Library
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link" href="#profile-{{$groups->id}}" aria-controls="profile-{{$groups->id}}" role="tab" data-toggle="tab" aria-selected="false">
																						<i class="fa fa-users"></i> Shared Library
																					</a>
																				</li>
																			</ul>
																		</div>
																		<div class="nav-tabs-ver-container-content">
																			<div class="card-block">
																				<div class="tab-content">
																					<div role="tabpanel" class="tab-pane active" id="home-{{$groups->id}}">
																						<h4>Berbagi Kepada Publik</h4>
																						<p>Bagikan konten anda kepada publik</p>
																						<form action="{{url('library/shared/'.$groups->id.'/2')}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<button type="submit" class="btn btn-primary btn-raised">Bagikan Konten</button>
																						</form>
																					</div>
																					<div role="tabpanel" class="tab-pane" id="profile-{{$groups->id}}">
																						<h4>Berbagi Dengan Orang Tertentu</h4>
																						<p>Bagikan konten library anda terhadap orang orang tertentu</p>
																						<form action="{{url('library/shared/'.$groups->id.'/3')}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<button type="submit" class="btn btn-primary btn-raised">Bagikan Kontent</button>
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="modal" id="ModalSharedGrouptEdit-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between">
																		<h3 class="headline headline-sm m-0">Edit Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<form action="{{url('/library/group/update/'.$groups->id)}}" method="post">
																		<input type="hidden" name="_token" value="{{csrf_token()}}">
																		<div class="form-group">
																			<label class="control-label">Nama folder</label>
																			<input type="text" placeholder="Nama Folder Anda" name="group_name" value="{{$groups->group_name}}" class="form-control">
																		</div>
																		<button type="submit" class="btn btn-primary btn-raised m-0">Edit Folder</button>
																	</form>
																</div>
															</div>
														</div>
													</div>
													<div class="modal" id="ModalSharedContentGroup-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="d-flex align-items-center justify-content-between mb-2">
																		<h3 class="headline headline-sm m-0">Berbagi Folder</h3>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">
																				<i class="zmdi zmdi-close"></i>
																			</span>
																		</button>
																	</div>
																	<div class="tab-content-share">
																		<div class="tab">
																			<ul class="nav nav-tabs-ver" role="tablist">
																				<li class="nav-item">
																					<a class="nav-link active" href="#home-group-{{$groups->id}}" aria-controls="home-group-{{$groups->id}}" role="tab" data-toggle="tab" aria-selected="false">
																						<i class="fa fa-tags"></i> Public Library
																					</a>
																				</li>
																				<li class="nav-item">
																					<a class="nav-link" href="#profile-group-{{$groups->id}}" aria-controls="profile-group-{{$groups->id}}" role="tab" data-toggle="tab" aria-selected="false">
																						<i class="fa fa-users"></i> Shared Library
																					</a>
																				</li>
																			</ul>
																		</div>
																		<div class="nav-tabs-ver-container-content">
																			<div class="card-block">
																				<div class="tab-content">
																					<div role="tabpanel" class="tab-pane active" id="home-group-{{$groups->id}}">
																						<h4>Berbagi Kepada Publik</h4>
																						<p>Bagikan folder anda kepada publik</p>
																						<form action="{{url('library/shared/group/'.$groups->id.'/2')}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<button type="submit" class="btn btn-primary btn-raised">Bagikan Folder</button>
																						</form>
																					</div>
																					<div role="tabpanel" class="tab-pane" id="profile-group-{{$groups->id}}">
																						<h4>Berbagi Dengan Orang Tertentu</h4>
																						<p>Bagikan folder library anda terhadap orang orang tertentu</p>
																						<form action="{{url('library/shared/group/'.$groups->id.'/3')}}" method="post">
																							<input type="hidden" name="_token" value="{{csrf_token()}}">
																							<button type="submit" class="btn btn-primary btn-raised">Bagikan Folder</button>
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endif
											</td>
										</tr>
									@endforeach
									@foreach($directory as $index => $item)
										@foreach($item->library as $index => $library)
											@if($library->content)
												<tr>
													<td style="min-width: 25vh;">
														<i class="{{content_type_icon($library->content->type_content)}} mr-1"></i>
														@if($library->content->type_content == 'file')
														{!!$library->content->description!!}
														@else
														{!!$library->content->title!!}
														@endif
													</td>
													<td>{{$library->content->created_at}}</td>
													<td class="text-center">{{$item->directory_name}}</td>
													<!-- <td class="text-center">{{$library->user_id == Auth::user()->id ? $item->directory_name : 'Shared Library'}}</td> -->
													<td class="text-center">{{$library->content->type_content}}</td>
													<td>
														@isset($request['directory'])
															@if($library->user_id == Auth::user()->id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
															<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Kembalikan Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin mengembalikan kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
																@if(!$library->shared->where('user_id', Auth::user()->id)->first())
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-success" data-toggle="modal" data-target="#ModalAddPublicShared-{{$library->id}}" href="javascript:void(0)" title="Add"><i class="fa fa-plus"></i></a>
																<div class="modal" id="ModalAddPublicShared-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Tambah Library</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/public/'.$library->id.'/shared/')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menambahkan ke dalam library anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-success btn-raised m-0">Tambah</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@else
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Hapus Kontent</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@endif
															@endif
														@else
															@if(Auth::user()->id == $library->user_id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" href="javascript:void(0)" data-toggle="modal" data-target="#ModalSharedContent-{{$library->id}}" title="Share"><i class="fa fa-share-alt"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-info" href="{{url('library/course/content/update/'.$library->content->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Konten</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@endif
														@endif
													</td>
												</tr>
											@elseif($library->quiz)
												<tr>
													<td style="min-width: 25vh;"><i class="fa fa-{{$library->quiz->quizz_type == 'quiz' ? 'star' : 'edit'}} mr-1"></i> {{$library->quiz->name}}</td>
													<td>{{$library->quiz->created_at}}</td>
													<td class="text-center">{{$library->user_id == Auth::user()->id ? $item->directory_name : 'Shared Library'}}</td>
													<td class="text-center">quiz</td>
													<td>
														@isset($request['directory'])
															@if($library->user_id == Auth::user()->id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
															<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Kembalikan Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin mengembalikan kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
																@if(!$library->shared->where('user_id', Auth::user()->id)->first())
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-success" data-toggle="modal" data-target="#ModalAddPublicShared-{{$library->id}}" href="javascript:void(0)" title="Add"><i class="fa fa-plus"></i></a>
																<div class="modal" id="ModalAddPublicShared-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Tambah Library</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/public/'.$library->id.'/shared/')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menambahkan ke dalam library anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-success btn-raised m-0">Tambah</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@else
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Hapus Kontent</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@endif
															@endif
														@else
															@if(Auth::user()->id == $library->user_id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-warning" href="{{url('library/course/'.$library->quiz->quizz_type.'/manage/'.$library->quiz->id)}}" title="Manage"><i class="fa fa-list"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" href="javascript:void(0)" data-toggle="modal" data-target="#ModalSharedContent-{{$library->id}}" title="Share"><i class="fa fa-share-alt"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-info" href="{{url('library/course/quiz/update/'.$library->quiz->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Konten</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">>@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@endif
														@endif
													</td>
												</tr>
											@elseif($library->assignment)
												<tr>
													<td style="min-width: 25vh;"><i class="fa fa-tasks mr-1"></i> {{$library->assignment->title}}</td>
													<td>{{$library->assignment->created_at}}</td>
													<td class="text-center">{{$library->user_id == Auth::user()->id ? $item->directory_name : 'Shared Library'}}</td>
													<td class="text-center">Tugas</td>
													<td>
														@isset($request['directory'])
															@if($library->user_id == Auth::user()->id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalRollback-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-sign-out"></i></a>
															<div class="modal" id="ModalRollback-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Kembalikan Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/rollback/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin mengembalikan kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">Kembalikan</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
																@if(!$library->shared->where('user_id', Auth::user()->id)->first())
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-success" data-toggle="modal" data-target="#ModalAddPublicShared-{{$library->id}}" href="javascript:void(0)" title="Add"><i class="fa fa-plus"></i></a>
																<div class="modal" id="ModalAddPublicShared-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Tambah Library</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/public/'.$library->id.'/shared/')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menambahkan ke dalam library anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-success btn-raised m-0">Tambah</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@else
																<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
																<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<div class="d-flex align-items-center justify-content-between mb-2">
																					<h3 class="headline headline-sm m-0">Hapus Tugas</h3>
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																						<span aria-hidden="true">
																							<i class="zmdi zmdi-close"></i>
																						</span>
																					</button>
																				</div>
																				<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="form-group text-center">
																						<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																						<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																						<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
																@endif
															@endif
														@else
															@if(Auth::user()->id == $library->user_id)
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" href="javascript:void(0)" data-toggle="modal" data-target="#ModalSharedContent-{{$library->id}}" title="Share"><i class="fa fa-share-alt"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-info" href="{{url('library/course/assignment/update/'.$library->assignment->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger" data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Konten</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent anda?</p>
																					<p style="color:red"><i>* Bila kontent dibagikan, maka semua yang berbagi dengan kontent anda akan terhapus</i></p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@else
															<a class="btn-circle btn-circle-sm btn-circle-raised btn-circle-danger"  data-toggle="modal" data-target="#ModalDeletLibrary-{{$library->id}}" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>
															<div class="modal" id="ModalDeletLibrary-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div class="d-flex align-items-center justify-content-between mb-2">
																				<h3 class="headline headline-sm m-0">Hapus Kontent</h3>
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																			</div>
																			<form class="text-left" action="{{url('/library/course/content/shared/destroy/'.$library->id)}}" method="post">
																				<input type="hidden" name="_token" value="{{csrf_token()}}">
																				<div class="form-group text-center">
																					<p>Apa anda yakin menghapus kontent yang berbagi dengan anda?</p>
																					<button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">Cancel</button>
																					<button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
																				</div>
																			</form>
																		</div>
																	</div>
																</div>
															</div>
															@endif
														@endif
													</td>
												</tr>
											@endif
											<div class="modal" id="ModalSharedContent-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
													<div class="modal-content">
														<div class="modal-body">
															<div class="d-flex align-items-center justify-content-between mb-2">
																<h3 class="headline headline-sm m-0">Berbagi Konten / Kuis</h3>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="zmdi zmdi-close"></i>
																	</span>
																</button>
															</div>
															<div class="tab-content-share">
																<div class="tab">
																	<ul class="nav nav-tabs-ver" role="tablist">
																		<li class="nav-item">
																			<a class="nav-link active" href="#home-{{$library->id}}" aria-controls="home-{{$library->id}}" role="tab" data-toggle="tab" aria-selected="false">
																				<i class="fa fa-tags"></i> Public Library
																			</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link" href="#profile-{{$library->id}}" aria-controls="profile-{{$library->id}}" role="tab" data-toggle="tab" aria-selected="false">
																				<i class="fa fa-users"></i> Shared Library
																			</a>
																		</li>
																	</ul>
																</div>
																<div class="nav-tabs-ver-container-content">
																	<div class="card-block">
																		<div class="tab-content">
																			<div role="tabpanel" class="tab-pane active" id="home-{{$library->id}}">
																				<h4>Berbagi Kepada Publik</h4>
																				<p>Bagikan konten anda kepada publik</p>
																				<form action="{{url('library/shared/'.$library->id.'/2')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<button type="submit" class="btn btn-primary btn-raised">Bagikan Konten</button>
																				</form>
																			</div>
																			<div role="tabpanel" class="tab-pane" id="profile-{{$library->id}}">
																				<h4>Berbagi Dengan Orang Tertentu</h4>
																				<p>Bagikan konten library anda terhadap orang orang tertentu</p>
																				<form action="{{url('library/shared/'.$library->id.'/3')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<button type="submit" class="btn btn-primary btn-raised">Bagikan Kontent</button>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@endif
			</div>

			<div class="modal" id="ModalMenuContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="myModalLabel">@lang('front.page_library.add_new_content_title')</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
						</div>
						<div class="modal-body library-modal-tab">
							{{-- <div class="tab">
								<button class="tablinks" onclick="openCity(event, 'Label')"><i class="fa fa-tags"></i> Label</button>
								<button class="tablinks" onclick="openCity(event, 'Video')" id="defaultOpen"><i class="fa fa-video-camera"></i> Konten Video</button>
								<button class="tablinks" onclick="openCity(event, 'Teks')"><i class="fa fa-text-height"></i> Konten Teks</button>
								<button class="tablinks" onclick="openCity(event, 'File')"><i class="fa fa-file-o"></i> Konten File</button>
								<button class="tablinks" onclick="openCity(event, 'Kuis')"><i class="fa fa-question"></i> Kuis</button>
								<button class="tablinks" onclick="openCity(event, 'Tugas')"><i class="fa fa-tasks"></i> Tugas</button>
							</div> --}}

							<div class="tab">
								<button class="tablinks" onclick="openCity(event, 'Teks')"><i class="fa fa-text-height"></i> Teks</button>
								<button class="tablinks" onclick="openCity(event, 'Video')" id="defaultOpen"><i class="fa fa-video-camera"></i> Video</button>
								<button class="tablinks" onclick="openCity(event, 'File')"><i class="fa fa-file"></i> File</button>
								<div class="spacer"></div>
								<button class="tablinks" onclick="openCity(event, 'Tugas')"><i class="fa fa-tasks"></i> Tugas</button>
								<button class="tablinks" onclick="openCity(event, 'Survey')"><i class="fa fa-edit"></i> Survey</button>
								<button class="tablinks" onclick="openCity(event, 'Kuis')"><i class="fa fa-question"></i> Kuis</button>
								<div class="spacer"></div>
								<button class="tablinks" onclick="openCity(event, 'Label')"><i class="fa fa-tags"></i> Label</button>
							</div>

							<div id="Label" class="tabcontent">
								<h3>Tambah Label</h3>
								<p>Tambahkan label untuk informasi konten lebih jelas</p>
								<a href="/library/course/content/create/?content=label" class="btn btn-primary btn-raised">Tambah Label</a>
							</div>

							<div id="Video" class="tabcontent">
								<h3>Tambah Konten Video</h3>
								<p>Tambahkan konten video agar proses belajar menjadi lebih interaktif</p>
								<a href="/library/course/content/create/?content=video" class="btn btn-primary btn-raised">Upload Video</a>
								<a href="/library/course/content/create/?content=url&urlType=video" class="btn btn-primary btn-raised">Tambah URL Youtube</a>
							</div>

							<div id="Survey" class="tabcontent">
								<h3>Tambah Konten Survey</h3>
								<p>Tambahkan konten survey agar proses belajar menjadi lebih interaktif</p>
								<a href="/library/course/survey/create" class="btn btn-primary btn-raised">Tambah Survey</a>
							</div>

							<div id="Teks" class="tabcontent">
								<h3>Tambah Konten Teks</h3>
								<p>Anda bisa menambahkan konten dengan hanya teks saja. bisa digunakan seperti membuat pendahuluan dan lain-lain.</p>
								<a href="/library/course/content/create/?content=text" class="btn btn-primary btn-raised">Tambah Konten Teks</a>
							</div>

							<div id="File" class="tabcontent">
								<h3>Tambah Konten File</h3>
								<p>Anda bisa menambahkan konten file / berkas. bisa digunakan seperti membuat file pdf, power point dan lain-lain.</p>
								<a href="/library/course/content/create/?content=file" class="btn btn-primary btn-raised">Tambah Konten File</a>
								<a href="/library/course/content/create/?content=url&urlType=file" class="btn btn-primary btn-raised">Tambah Dari URL</a>
							</div>

							<div id="Kuis" class="tabcontent">
								<h3>Tambah Kuis</h3>
								<p>Tambahkan kuis untuk menguji peserta sejauhmana mereka belajar</p>
								<a href="/library/course/quiz/create" class="btn btn-primary btn-raised">Tambah Kuis</a>
							</div>

							<div id="Tugas" class="tabcontent">
								<h3>Tambah Tugas</h3>
								<p>Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.</p>
								<a href="/library/course/assignment/create/" class="btn btn-primary btn-raised">Tambah Tugas</a>
							</div>
							{{-- <a href="#" class="btn btn-default btn-raised">Batal</a> --}}
						</div>
					</div>
				</div>
			</div>

			<div class="modal" id="ModalSharedGroupt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="myModalLabel">@lang('front.page_library.add_new_folder')</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
						</div>
						<div class="modal-body">
							<form action="{{url('/library/group/create/')}}" method="post">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<div class="form-group">
									<label class="control-label">Nama folder</label>
									<input type="text" placeholder="Nama Folder Anda" name="group_name" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label">Pilih Konten / Kuis</label>
									<div class="card card-library">
										<div class="card-block">
											<div class="table-responsive table-cs">
												<table id="createDirectory" class="table table-striped d-table">
													<thead>
														<tr>
															<th style="width:20px;"></th>
															<th>Nama Konten</th>
															<th>Tanggal Dibuat</th>
															<th class="text-center">Directory</th>
															<th class="text-center">Tipe</th>
														</tr>
													</thead>
													<tbody>
														@foreach($directory_user as $index => $item)
															@foreach($item->library as $index => $library)
																@if($library->content)
																	<tr>
																		<td style="padding: 12px 0 !important;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																		<td><i class="{{content_type_icon($library->content->type_content)}}"></i> {{$library->content->title}}</td>
																		<td>{{$library->content->created_at}}</td>
																		<td class="text-center">{{$item->directory_name}}</td>
																		<td class="text-center">{{$library->content->type_content}}</td>
																	</tr>
																@elseif($library->quiz)
																	<tr>
																		<td style="padding: 12px 0 !important;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																		<td><i class="fa fa-{{$library->quiz->quizz_type == 'quiz' ? 'star' : 'edit'}}"></i> {{$library->quiz->name}}</td>
																		<td>{{$library->quiz->created_at}}</td>
																		<td class="text-center">{{$item->directory_name}}</td>
																		<td class="text-center">quiz</td>
																	</tr>
																@elseif($library->assignment)
																	<tr>
																		<td style="padding: 12px 0 !important;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></td>
																		<td><i class="fa fa-tasks"></i> {{$library->assignment->title}}</td>
																		<td>{{$library->assignment->created_at}}</td>
																		<td class="text-center">-</td>
																		<td class="text-center">Tugas</td>
																	</tr>
																@endif
															@endforeach
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<button type="submit" class="btn btn-primary btn-raised m-0">Buat Folder</button>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal" id="ModalAddBankSoal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="myModalLabel">@lang('front.page_library.add_new_storage_content')</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
						</div>
						<div class="modal-body">
							<form action="{{url('/library/bank/soal/create/')}}" method="post">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<div class="form-group">
									<label class="control-label">Nama Bank Soal</label>
									<input type="text" placeholder="Nama Bank Soal Anda" name="group_name" class="form-control">
								</div>
								<button type="submit" class="btn btn-primary btn-raised m-0">Buat Bank Soal</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
	<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="{{asset('js/sweetalert.min.js')}}"></script>
	<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script>
	// trigger modal add konten / quiz
		function openCity(evt, cityName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
					tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}

		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();

		// trigger modal add konten / quiz

		$(document).ready(function() {
			$('#example').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
			$('#example2').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
			$('#example22').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
			$('.example222').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
			$('#example-1').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
			$('#createDirectory').DataTable({
				"lengthChange": false,
				language: { search: '', searchPlaceholder: "Search ..." },
			});

			$('.edit-group').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
		});
	</script>
@endpush
