@extends('layouts.app')

@push('style')
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Membuat <span>Tugas</span></h2>
        </div>
      </div>
    </div>
  </div>
	<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Pengajar</a></li>
          <li><a href="/course/dashboard">Kelola Kelas</a></li>
        </ul>
      </div>
    </div>
  </div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method)) }}

										<div class="form-group no-m">
											<label for="title">Judul</label>
											<input placeholder="Masukan Judul" type="text" class="form-control" name="title" value="{{ $assignment->title }}" required>
										</div>

										<div class="form-group">
											<label for="title">Deskripsi / Penjelasan</label>
											<textarea name="description" id="description" required>{{ $assignment->description }}</textarea>
										</div>
										<div class="form-group">
											<label for="title">Tipe Tugas</label><br>
											<select class="form-control selectpicker" name="type" requred>
												<option {{$assignment->type == '0' ? 'selected' : ''}} value="0">Online Text</option>
												<option {{$assignment->type == '1' ? 'selected' : ''}} value="1">File Upload</option>
											</select>
										</div>
										<div class="form-group">
											<label for="Attempt">Kesempatan Menjawab</label>
											<input placeholder="Kesempatan Percobaan" type="number" class="form-control" name="attempt" value="{{ $assignment->attempt }}" required>
										</div>
										<div class="form-group">
											<label for="name">Tanggal Mulai</label>
											<input placeholder="Tanggal Mulai" autocomplete="off" type="text" class="form-control form_datetime" name="time_start" value="{{ $assignment->time_start }}" required>
										</div>
										<div class="form-group">
											<label for="name">Tanggal Akhir</label>
											<input placeholder="Tanggal Akhir" autocomplete="off" type="text" class="form-control form_datetime" name="time_end" value="{{ $assignment->time_end }}" required>
										</div>
										<div class="form-group">
											{{  Form::submit($button . " Tugas" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}
@endpush
