@extends('layouts.app')

@section('title')
	{{ 'Buat Kuis '}}
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
@endpush

@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Membuat <span>Kuis</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Kuis</a></li>
					<li><a href="/instructor/library">Library</a></li>
					<li>Membuat Kuis</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						{{-- <div class="box-header with-border">
							<h3 class="box-title">{{$button}} Kuis</h3>
						</div> --}}
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">
									{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

										<div class="form-group">
											<label for="name">Judul Kuis</label>
											<input placeholder="Judul Kuis" type="text" class="form-control" name="name" value="{{ $quiz->name }}" required>
										</div>

										<div class="form-group">
											<label for="title">Deskripsi / Penjelasan</label>
											<textarea name="description" id="description">{!! $quiz->description !!}</textarea>
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Tanggal Mulai</label>
													<input placeholder="Tanggal Mulai" type="text" autocomplete="off" class="form-control datePicker" name="date_start" value="{{ substr($quiz->time_start,0,10) }}" required>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Jam Mulai</label>
													<input placeholder="Jam Mulai" type="time" autocomplete="off" class="form-control" name="time_start" value="{{ substr($quiz->time_start,11,5) }}" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Tanggal Selesai</label>
													<input placeholder="Tanggal Selesai" type="text" autocomplete="off" class="form-control datePicker" name="date_end" value="{{ substr($quiz->time_end,0,10) }}" required>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="name">Jam Selesai</label>
													<input placeholder="Jam Selesai" type="time" autocomplete="off" class="form-control" name="time_end" value="{{ substr($quiz->time_end,11,5) }}" required>
												</div>
											</div>
										</div>


										<div class="form-group">
											<label for="name">Durasi (Menit)</label>
											<input placeholder="60" type="number" class="form-control" name="duration" value="{{ $quiz->duration }}" required>
										</div>
										<div class="form-group">
											{{  Form::submit($button . " Kuis" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('course/preview/') }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>

@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}

	{{-- datetime picker --}}
	<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript">
		$('.form_datetime').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
		});
	</script>
	{{-- datetime picker --}}

	<script type="text/javascript">
		$(".datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>
@endpush
