@extends('layouts.app')

@section('title')
	{{ 'Edit Pertanyaan'}}
@endsection

@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Edit <span>Pertanyaan</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
					<li><a href="#">Kuis</a></li>
					<li><a href="/instructor/library">Library</a></li>
					<li>Melakukan Update Pertanyaan</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Default box -->
					<div class="box">
						<div class="box-body">

							<center>
	              @if(Session::has('success'))
	                <div class="alert alert-success alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('success') !!}
	                </div>
	              @elseif(Session::has('error'))
	                <div class="alert alert-error alert-dismissible">
	                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                  {!! Session::get('error') !!}
	                </div>
	              @endif
	            </center>

							<div class="card-md">
							  <div class="card-block">

									{{ Form::open(array('url' => $action, 'method' => $method)) }}

										{{-- <div class="form-group">
											<label for="name">Tipe Kuis</label>
											<select class="form-control selectpicker" name="quiz_type_id" id="quiz_type_id" required>
												<option value="">Pilih Tipe</option>
												@foreach($quiz_types->where('id', '<=', 7) as $quiz_type)
													<option {{ $quiz_type->id == $quiz_type_id ? 'selected' : '' }} value="{{$quiz_type->id}}">{{$quiz_type->type}}</option>
												@endforeach()
											</select>
										</div> --}}
										<input type="hidden" name="quiz_type_id" value="{{$quiz_type_id}}">

										<div class="form-group">
											@if($quiz_type_id != '4')
												<label for="title">Pertanyaan</label>
											@endif
											<textarea name="question" id="question">{!! $question !!}</textarea>
										</div>

										@if($quiz_type_id != '4')
											<div class="form-group">
												<label for="title">Poin</label>
												<input type="number" name="weight" class="form-control" value="{{$weight}}">
											</div>
										@endif

										{{-- <div class="form-group">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('/library/course/quiz/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div> --}}

										@if($quiz_type_id == '1' || $quiz_type_id == '2')
											<div class="row">
												<div class="col-md-10">
													<h3 class="headline-xs">Pilihan Jawaban</h3>
												</div>
												<div class="col-md-2 text-center">
													<h3 class="headline-xs">Pilihan Benar ?</h3>
												</div>
											</div>
										@endif

										@if($quiz_type_id != '4')
											<div id="new_row_answer">
												@php $index = 0 @endphp
												@foreach($answers as $index => $answer)

													<div class="card-sm mb-2" id="row_answer{{$index}}">
														<div class="card-block bg-white">
															<div class="row">
																<div class="col-md-10">
																	<div class="form-group no-m">
																		<label class="control-label" for="inputDefault">Masukan pilihan</label>
																		<input autocomplete="off" type="text" name="old_answer[]" id="text_answer{{$index}}" value="{{$answer->answer}}" class="form-control">
																		<input type="hidden" name="old_answer_id[]" value="{{$answer->id}}">
																		@if($quiz_type_id == '3')
																			<input type="hidden" name="old_answer_ordering[]" value="{{$index}}">
																		@endif
																		<div id="text_answer_editor_place{{$index}}"></div>
																		<a href="#/" id="remove_row_answer" answer-id="{{$answer->id}}" data-id="{{$index}}">hapus</a>
																		<a href="#/" class="answer_editor" data-id="{{$index}}">gunakan editor</a>
																	</div>
																</div>
																<div class="col-md-2">
																	<br>
																	@if($quiz_type_id == '1' || $quiz_type_id == '2')
																		<div class="text-center">
									                    <label>
																				<input type="hidden" value="{{$answer->answer_correct}}" id="correctAnswertext{{$index}}" name="old_answer_correct[]">
								                        <input type="checkbox" name="" id="correctAnswerCheck" data-increment="{{$index}}" value="1" {{$answer->answer_correct == '1' ? 'checked' : ''}}>
									                    </label>
										                </div>
																	@endif
																</div>
															</div>
														</div>
													</div>
												@endforeach
												<input type="hidden" id="choice_increments" value="{{$index}}">
											</div>
											<button id="{{$quiz_type_id == '3' ? 'add_new_answer_ordering' : 'add_new_answer'}}" type="button" class="btn btn-raised">Tambah Pilihan</button>
											<br><br>
									@endif

										<div class="form-group">
											<input type="hidden" name="remove_answers" id="remove_answers" value="">
											{{  Form::hidden('id', $id) }}
											{{  Form::submit($button . " Pertanyaan" , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
											<a href="{{ url('/library/course/quiz/manage/'.$quiz->id) }}" class="btn btn-default btn-raised">Batal</a>
										</div>
									{{ Form::close() }}
							  </div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('question');
	</script>
	<script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
	{{-- CKEDITOR --}}

	{{-- add new answer --}}
	<script type="text/javascript">
		$("#add_new_answer").click(function(){
			var choice_increments = $("#choice_increments").val()
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
				'<div class="card-block bg-white">'+
				'<div class="row">'+
					'<div class="col-md-10">'+
						'<div class="form-group no-m">'+
							'<label class="control-label" for="inputDefault">Masukan pilihan</label>'+
							'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
							'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
							'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
							'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
						'</div>'+
					'</div>'+

					'<div class="col-md-2">'+
						'<br>'+
						'<div class="text-center">'+
								'<label>'+
									'<input type="hidden" value="0" id="correctAnswertext'+choice_increments+'" name="answer_correct[]">'+
									'<input type="checkbox" name="" id="correctAnswerCheck" data-increment="'+choice_increments+'" value="1">'+
								'</label>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<br>'
			);
		})
	</script>

	<script type="text/javascript">
		$(function(){
			$(document).on('change','#correctAnswerCheck',function(e){
				e.preventDefault();
				var data_increment = $(this).attr('data-increment');
				$("#correctAnswertext" + data_increment).val($(this).is(":checked") == true ? '1' : '0')
			});
		})
	</script>
	{{-- add new answer --}}

	{{-- add new answer ordering --}}
	<script type="text/javascript">
		$("#add_new_answer_ordering").click(function(){
			var choice_increments = parseInt($("#choice_increments").val()) + 1
			$("#choice_increments").val( parseInt($("#choice_increments").val()) + 1)

			$("#new_row_answer").append(
				'<div class="card-sm mb-2" id="row_answer'+choice_increments+'">'+
					'<div class="card-block bg-white">'+
						'<div class="form-group">'+
							'<label class="control-label" for="focusedInput1">Masukan pilihan</label>'+
							'<input autocomplete="off" type="text" name="answer[]" id="text_answer'+choice_increments+'" value="" class="form-control">'+
							'<div id="text_answer_editor_place'+choice_increments+'"></div>'+
							'<input type="hidden" name="answer_ordering[]" value="'+choice_increments+'">'+
							'<a href="#/" id="remove_row_answer" data-id="'+choice_increments+'">hapus</a>'+
							'<a href="#/" class="answer_editor" data-id="'+choice_increments+'"> gunakan editor</a>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<br>'
			);
		})
	</script>
	{{-- add new answer ordering --}}

	{{-- remove new answer --}}
	<script type="text/javascript">
		$(function(){
			var removed_answer = [];
			$(document).on('click','#remove_row_answer',function(e){
				e.preventDefault();
					var data_id = $(this).attr('data-id');
					var answer_id = $(this).attr('answer-id');
					removed_answer.push(answer_id)
					if(answer_id){
						$("#remove_answers").val(removed_answer)
					}
					$("#row_answer"+data_id).remove();
			});
		});
	</script>
	{{-- remove new answer --}}

	{{-- delete answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#delete_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var url_delete_answer =  "{{url('course/quiz/question/answer/delete/'.$quiz->id.'/')}}";
				$.ajax({
					url : url_delete_answer + '/' + data_id,
					type : "GET",
					success : function(){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- delete answer --}}

	{{-- edit answer --}}
	<script type="text/javascript">
		$(function(){
			$(document).on('click','#save_edit_answer',function(e){
				e.preventDefault();
				var data_id = $(this).attr('data-id');
				var token = '{{ csrf_token() }}';
				$.ajax({
					url : "{{url('course/quiz/question/answer/update_action')}}",
					type : "POST",
					data : {
						answer : $("#edit_text_answer"+data_id).val(),
						answer_correct : $("#edit_text_answer_correct"+data_id).val(),
						id : data_id,
						_token: token
					},
					success : function(result){
						location.reload();
					},
				});
			});
		});
	</script>
	{{-- edit answer --}}
@endpush
