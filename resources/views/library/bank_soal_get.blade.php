@extends('layouts.app_nation')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
  <style>
    .table-cs .dataTables_length {
      font-size: 20px;
    }

    .table-cs input {
      width: 100%;
      font-size: 14px;
      border: 1px solid #ccc;
      border-radius: 5px;
      box-shadow: none;
      padding: 0.75rem 1.5rem;
      margin: 0;
    }

    .table-cs thead>tr>th {
      border-bottom: none;
      padding: 8px 10px;
    }

    .table-cs tbody>tr>td {
      background: white !important;
    }

    .table-cs .dataTables_paginate .paginate_button {
    }

    .table-cs .dataTables_paginate span a.paginate_button {
      border-radius: 3px;
      padding: 0.25rem 0.75rem;
    }

    .table-cs .dataTables_paginate span a.paginate_button.current {
      border: 0px;
      background: #4ca3d9;
      color: #fff !important;
    }

    .table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after {
			display: none !important
    }

    .table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child {
			width: 0.1px !important;
			padding: 0;
    }

    .table-cs .dataTables_info {
      display: none;
    }

		.table-cs .table td, .table-cs .table th {
			vertical-align: middle;
		}

    .dataTables_filter {
      /* margin-bottom: 15px; */
    }
  </style>
	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>
	<style>
		/* Style the tab */
		.tab {
			float: left;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
			width: 30%;
			height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
			display: block;
			background-color: inherit;
			color: black;
			padding: 22px 16px;
			width: 100%;
			border: none;
			outline: none;
			text-align: left;
			cursor: pointer;
			transition: 0.3s;
			font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
			background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
			background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.placeholder{
			background-color:white;
			height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}

		/* .library-direc{
			display: flex;
			padding: 20px;
			font-size: 19px;
    	font-family: cursive;
		}

		.library-direc span{
			color: #4ca3d9;
	    font-size: 25px;
	    margin-right: 20px;
		}

		.direc:hover{
			background:  #4ca3d9;
			color: white !important;
		}

		.direc:hover .library-direc{
			color: white !important;
		}

		.direc:hover .library-direc span{
			color: white !important;
		} */

		.nav-tabs-ver-container-content{
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.tab-content-share .tab{
			min-height: 180px;
			margin-bottom: 25px;
		}

		.tab-content-share li{
			width: 100%;
		}

		.tab-content-share li a.active{
			background: #ccc;
		}

		.tab-content-share .nav-tabs-ver-container-content{
			min-height: 180px;
		}

		.tab-content-share .nav-tabs-ver-container-content .tab-pane{
			background: white;
    	color: black;
		}

		.tab-content-share .nav-tabs-ver-container-content .card-block{
			padding: 0;
		}

		.tab-content-share li a{
			display: block;
	    background-color: inherit;
	    color: black;
	    padding: 22px 16px;
	    width: 100%;
	    border: none;
	    outline: none;
	    text-align: left;
	    cursor: pointer;
	    transition: 0.3s;
	    font-size: 17px;
		}

		.dropdown-menu.dropdown-menu-primary li a i{
			padding: 5px;
			border-radius: 5px;
			width: 25px;
			text-align: center;
		}

		.active, .site-dashboard-panel__nav:hover {
			background-color: #7c7c7c;
			background: none;
			color: black;
		}
	</style>
	<style>
		.card.card-library {
			background: #f9f9f9;
			color: #424242;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
		}

		.card.card-library.card-library-hover:hover {
			background: #f3f3f3;
		}
	</style>
@endpush

@section('content')
	<div class="bg-page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					{{-- <h2 class="headline-md no-m">Libraries (Beta)</h2> --}}
						<h2 class="headline-md no-m">Bank Soal</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">Home</a></li>
          <li><a href="{{url('/instructor/library')}}">Library</a></li>
					<li>{{$bank_soal->group_name}}</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<form action="{{url('library/bank/soal/get', [$quiz->id, $bank_soal->id])}}" method="post">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="row">
				</div>
	      <div class="btn-group">
	        <button type="submit" class="btn btn btn-primary dropdown-toggle btn-raised">
	          Tambah Pertanyaan
	        </button>
	      </div>

				<div class="card card-library">
					<div class="card-block">
						<div class="table-responsive table-cs">
							<table id="example" class="table table-striped d-table">
								<thead>
									<tr>
	                  <th></th>
										<th style="min-width: 25vh;">Title</th>
										<th>Tipe</th>
										<th class="text-center">Bobot Nilai</th>
									</tr>
								</thead>
								<tbody>
	                @foreach($bank_soal->question as $index => $item)
	                <tr>
	                  <td style="    width: 20px;"><input type="checkbox" name="bank_question_id[]" value="{{$item->id}}"></td>
	                  <td>{{strip_tags($item->question)}}</td>
	                  <td>{{$item->type->type}}</td>
	                  <td class="text-center">{{$item->weight}}</td>
	                </tr>
	                @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@push('script')
	<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
	<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
	<script src="{{asset('js/sweetalert.min.js')}}"></script>
	<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script>

		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();

		// trigger modal add konten / quiz

		$(document).ready(function() {
			$('#example').DataTable({
				language: { search: '', searchPlaceholder: "Search ..." },
			});
		});
	</script>
@endpush
