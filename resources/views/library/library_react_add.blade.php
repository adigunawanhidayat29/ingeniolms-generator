@extends('layouts.app_nation')

@section('title', 'My Library')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">
	<style>
	.react-container{
		margin: 20px;
		margin-left: 40px;
		margin-right: 40px;
	}
	</style>
	<style media="screen">
		.editableform .form-group{
			margin: 0px !important;
			padding: 0px !important;
		}
		.editableform .form-group .form-control{
			height: 33px;
			padding: 0px;
			margin-top: -1.3rem;
			margin-bottom: 0px;
			line-height: auto;
		}
		.editable-input .form-control{
			color: white !important;
			width: auto;
			font-size: 2.4rem;
			font-weight: bold;
		}
		.editable-buttons{
			display: none;
		}
	</style>
	<style>
		/* Style the tab */
		.tab {
				float: left;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
				width: 30%;
				height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
				display: block;
				background-color: inherit;
				color: black;
				padding: 22px 16px;
				width: 100%;
				border: none;
				outline: none;
				text-align: left;
				cursor: pointer;
				transition: 0.3s;
				font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
				background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
				background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
				float: left;
				padding: 0px 12px;
				border: 1px solid #ccc;
				width: 70%;
				border-left: none;
				height: auto;
		}

		.placeholder{
				background-color:white;
				height:18px;
		}

		.move:hover{
			cursor: move;
		}

		.lh-3{
			line-height: 3;
		}
		.ContentSortable{
			border:0.2px solid #dfe6e9;
			min-height:60px;
			/* padding:10px; */
		}
		.img-circle{
			border-radius:100%;
			border: 3px solid #dcdde1;
			padding: 5px;
			width: 120px;
			height: 120px;
		}

		.togglebutton label .toggle{
			margin-left: 15px;
			margin-right: 15px;
		}

		.preview-group{
			display: inline-flex;
		}
			.preview-group .preview{
				text-align: center;
			}
			.preview-group .status{
				margin-right: 1rem;
			}

		.date-time span{
			display: inline-block;
			margin-bottom: 0;
			margin-right: 1rem;
		}

		@media (max-width: 768px){
			.preview-group{
				display: block;
			}
				.preview-group .duration{
					text-align: right;
				}
				.preview-group .preview{
					display: inline-flex;
					margin-right: 1rem;
				}
				.preview-group .status{
					text-align: right;
					margin-right: 0;
				}
				.togglebutton label .toggle{
					margin-left: 10px;
					margin-right: 0;
				}
			.date-time span{
				display: block;
				margin-bottom: 0.5rem;
				margin-right: 0;
			}
		}
	</style>
	<style media="screen">
		.eyeCheckbox > input{
				display: none;
			}
			.eyeCheckbox input[type=checkbox]{
				opacity:0;
				position: absolute;
			}
			.eyeCheckbox i{
				cursor: pointer;
			}
			.list-group{
			border: 0;
		}
		.list-group a.list-group-item{
			border: 1px solid #eee;
		}

		.library-direc{
			display: flex;
			padding: 20px;
			font-size: 19px;
    	font-family: cursive;
		}

		.library-direc span{
			color: #4ca3d9;
	    font-size: 25px;
	    margin-right: 20px;
		}

		.direc:hover{
			background:  #4ca3d9;
			color: white !important;
		}

		.direc:hover .library-direc{
			color: white !important;
		}

		.direc:hover .library-direc span{
			color: white !important;
		}

		.nav-tabs-ver-container-content{
			float: left;
			padding: 0px 12px;
			border: 1px solid #ccc;
			width: 70%;
			border-left: none;
			height: auto;
		}

		.tab-content-share .tab{
			min-height: 180px;
			margin-bottom: 25px;
		}

		.tab-content-share li{
			width: 100%;
		}

		.tab-content-share li a.active{
			background: #ccc;
		}

		.tab-content-share .nav-tabs-ver-container-content{
			min-height: 180px;
		}

		.tab-content-share .nav-tabs-ver-container-content .tab-pane{
			background: white;
    	color: black;
		}

		.tab-content-share .nav-tabs-ver-container-content .card-block{
			padding: 0;
		}

		.tab-content-share li a{
			display: block;
	    background-color: inherit;
	    color: black;
	    padding: 22px 16px;
	    width: 100%;
	    border: none;
	    outline: none;
	    text-align: left;
	    cursor: pointer;
	    transition: 0.3s;
	    font-size: 17px;
		}

		.dropdown-menu.dropdown-menu-primary li a i{
			padding: 5px;
			border-radius: 5px;
			width: 25px;
			text-align: center;
		}

		.active, .site-dashboard-panel__nav:hover {
			background-color: #7c7c7c;
	    background: none;
	    color: black;
		}
	</style>
@endpush

@section('content')
<div class="container">
	<h2></h2>
	<div class="row">
		@foreach($directory as $index => $item)
		<div class="col-sm-4">
			<div class="card direc">
					<a href="{{$index == 0 ? url('/course/content/library/add/'.$course_id.'/'.$section_id) : '?directory='.$item->id}}">
					<div class="library-direc">
						<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
						{{$item->directory_name}}
					</div>
				</a>
			</div>
		</div>
		@endforeach
	</div>
	<br>
	<div class="card" style="padding:20px;">
		<div class="title-table" style="margin-bottom: 15px;">
			<div class="left" style="float:left">
				<?php $request = Request::all(); ?>
				@if(isset($request['directory']))
				<h2 style="line-height: 0.5;"><a href="{{url('instructor/library')}}">Library</a> / {{$directory->where('id', $request['directory'])->first()->directory_name}}</h2>
				@else
				<h2 style="line-height: 0.5;">Your Library</h2>
				@endif
			</div>
		</div>
		<?php
			$shared_cek = false;
			if(isset($request['directory'])){
				if($request['directory'] == '3'){
					$shared_cek = true;
				}
			}
		 ?>
		@if($shared_cek)
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs  shadow-2dp" role="tablist">
	      <li class="nav-item">
	        <a class="nav-link withoutripple active" href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-selected="false">
	          <i class="fa fa-user"></i>
	          <span class="d-none d-sm-inline">Shared File</span>
	        </a>
	      </li>
	      <li class="nav-item current">
	        <a class="nav-link withoutripple" href="#settings" aria-controls="settings" role="tab" data-toggle="tab" aria-selected="true">
	          <i class="fa fa-users"></i>
	          <span class="d-none d-sm-inline">Shared Grup</span>
	        </a>
	      </li>
			</ul>
	    <div class="card-block">
	      <!-- Tab panes -->
	      <div class="tab-content">
	        <div role="tabpanel" class="tab-pane fade active show" id="home">
						<form action="{{url('course/content/library/add/share')}}" method="post">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary btn-raised" style="float: right ;margin-top: 0;">Submit</button>
							<input type="text" class="form-control" name="shared_code" placeholder="Masukan Kode Kontent Untuk Menambah Kontent Baru Yang Dibagikan" style="    width: 88%;float: left;">
						</form>
						<form action="{{url('course/content/library/add/'.$course_id.'/'.$section_id)}}" method="post">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary btn-raised">Tambah Kontent / Kuis</button>
							<br><br>
							<div class="table-responsive">
								<table id="example-1" class="display" style="width:100%">
										<thead>
												<tr>
														<th></th>
														<th style="width:20px;"></th>
														<th>Nama Kontent</th>
														<th>Tanggal Dibuat</th>
														<th class="text-center">Directory</th>
														<th class="text-center">Tipe</th>
														<th class="text-center">Code</th>
												</tr>
										</thead>
										<tbody>
											@foreach($share_user as $index => $item)
												<?php $library = $item->library; ?>
												@if($library->content)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->content->id}}"></td>
															<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
															<td>{{$library->content->title}}</td>
															<td>{{$library->content->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<th class="text-center">{{$library->content->type_content}}</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@elseif($library->quiz)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->quiz->id}}"></td>
															<td class="text-center"><i class="fa fa-star"></i></td>
															<td>{{$library->quiz->name}}</td>
															<td>{{$library->quiz->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<th class="text-center">quiz</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@elseif($library->assignment)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->assignment->id}}"></td>
															<td class="text-center"><i class="fa fa-tasks"></i></td>
															<td>{{$library->assignment->title}}</td>
															<td>{{$library->assignment->created_at}}</td>
															<td class="text-center">-</td>
															<th class="text-center">Tugas</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@endif
											@endforeach
											@foreach($directory as $index => $item)
												@foreach($item->library as $index => $library)
												@if($library->content)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->content->id}}"></td>
															<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
															<td>{{$library->content->title}}</td>
															<td>{{$library->content->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<th class="text-center">{{$library->content->type_content}}</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@elseif($library->quiz)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->quiz->id}}"></td>
															<td class="text-center"><i class="fa fa-star"></i></td>
															<td>{{$library->quiz->name}}</td>
															<td>{{$library->quiz->created_at}}</td>
															<td class="text-center">{{$item->directory_name}}</td>
															<th class="text-center">quiz</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@elseif($library->assignment)
													<tr>
															<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->assignment->id}}"></td>
															<td class="text-center"><i class="fa fa-tasks"></i></td>
															<td>{{$library->assignment->title}}</td>
															<td>{{$library->assignment->created_at}}</td>
															<td class="text-center">-</td>
															<th class="text-center">Tugas</th>
															<th class="text-center color-primary">{{$library->shared_code}}</th>
													</tr>
												@endif
												@endforeach
											@endforeach
										</tbody>
								</table>
							</div>
						</form>
	        </div>
	        <div role="tabpanel" class="tab-pane fade" id="settings">
						<form action="{{url('course/content/library/group/share')}}" method="post">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary btn-raised" style="float: right ;margin-top: 0;">Submit</button>
							<input type="text" class="form-control" name="shared_code" placeholder="Masukan Kode Grup Untuk Menambah Grup Baru Yang Dibagikan" style="    width: 88%;float: left;">
							<br>
							<br>
							<br>
						</form>
						<div class="table-responsive">
							<table id="example2" class="display" style="width:100%">
									<thead>
											<tr>
													<th style="width:20px;"></th>
													<th>Nama Grup</th>
													<th>Tanggal Dibuat</th>
													<th class="text-center">Directory</th>
													<th class="text-center">Tipe</th>
													<th class="text-center">Code</th>
											</tr>
									</thead>
									<tbody>
										@foreach($group_user as $item)
											<?php $groups = $item->group; ?>
											<tr>
													<td class="text-center"><a href="#" data-toggle="modal" data-target="#ModalSharedGrouptAdd-{{$item->id}}"><i class="fa fa-folder-open" style="font-size: 30px;color: #4ca3d9;"></i></a></td>
													<td>
														<a href="#" data-toggle="modal" data-target="#ModalSharedGrouptAdd-{{$item->id}}">{{$groups->group_name}}</a>
														<div class="modal" id="ModalSharedGrouptAdd-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="panel panel-default">
																			<div class="panel-body">
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																				<form action="{{url('course/content/library/add/'.$course_id.'/'.$section_id)}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="head-panel" style="margin-bottom:20px">
																						<h3 style="float: left;">Tambah Kontent Grup</h3>
																						<button type="submit" class="btn btn-primary btn-raised" style="float: right;z-index: 9999;"> Tambah Kontent</button>
																					</div>
																					<div class="table-responsive">
																						<table class="display example222" style="width:100%">
																								<thead>
																										<tr>
																												<th style="width:20px;"></th>
																												<th style="width:20px;"></th>
																												<th>Nama Kontent</th>
																												<th>Tanggal Dibuat</th>
																												<th class="text-center">Directory</th>
																												<th class="text-center">Tipe</th>
																										</tr>
																								</thead>
																								<tbody>
																									@foreach($group_user as $index => $item)
																										@foreach($item->group->contents as $index => $library)
																										<?php $library = $library->library; ?>
																											@if($library->content)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
																														<td>{{$library->content->title}}</td>
																														<td>{{$library->content->created_at}}</td>
																														<td class="text-center">{{$item->directory_name}}</td>
																														<th class="text-center">{{$library->content->type_content}}</th>
																												</tr>
																											@elseif($library->quiz)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="fa fa-star"></i></td>
																														<td>{{$library->quiz->name}}</td>
																														<td>{{$library->quiz->created_at}}</td>
																														<td class="text-center">{{$item->directory_name}}</td>
																														<th class="text-center">quiz</th>
																												</tr>
																											@elseif($library->assignment)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="fa fa-tasks"></i></td>
																														<td>{{$library->assignment->title}}</td>
																														<td>{{$library->assignment->created_at}}</td>
																														<td class="text-center">-</td>
																														<th class="text-center">Tugas</th>
																												</tr>
																											@endif
																										@endforeach
																									@endforeach
																								</tbody>
																						</table>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
															</div>
														</div>
													</td>
													<td>{{$groups->created_at}}</td>
													<td class="text-center">{{$item->directory_name}}</td>
													<th class="text-center">Group Shared</th>
													<th class="text-center color-primary">{{$groups->group_code}}</th>
											</tr>
										@endforeach
										@foreach($group as $groups)
											<tr>
													<td class="text-center"><a href="#" data-toggle="modal" data-target="#ModalSharedGrouptAdd-2-{{$groups->id}}"><i class="fa fa-folder-open" style="font-size: 30px;color: #4ca3d9;"></i></a></td>
													<td>
														<a href="#" data-toggle="modal" data-target="#ModalSharedGrouptAdd-2-{{$groups->id}}">{{$groups->group_name}}</a>
														<div class="modal" id="ModalSharedGrouptAdd-2-{{$groups->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
																	<div class="modal-content">
																		<div class="panel panel-default">
																			<div class="panel-body">
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">
																						<i class="zmdi zmdi-close"></i>
																					</span>
																				</button>
																				<form action="{{url('/library/group/create/')}}" method="post">
																					<input type="hidden" name="_token" value="{{csrf_token()}}">
																					<div class="head-panel" style="margin-bottom:20px">
																						<h3 style="float: left;">Tambah Kontent Grup</h3>
																						<button type="submit" class="btn btn-primary btn-raised" style="float: right;z-index: 9999;"> Tambah Kontent</button>
																					</div>
																					<div class="table-responsive">
																						<table id="example22" class="display" style="width:100%">
																								<thead>
																										<tr>
																												<th style="width:20px;"></th>
																												<th style="width:20px;"></th>
																												<th>Nama Kontent</th>
																												<th>Tanggal Dibuat</th>
																												<th class="text-center">Directory</th>
																												<th class="text-center">Tipe</th>
																										</tr>
																								</thead>
																								<tbody>
																									@foreach($directory_user as $index => $item)
																										@foreach($item->library as $index => $library)
																											@if($library->content)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
																														<td>{{$library->content->title}}</td>
																														<td>{{$library->content->created_at}}</td>
																														<td class="text-center">{{$item->directory_name}}</td>
																														<th class="text-center">{{$library->content->type_content}}</th>
																												</tr>
																											@elseif($library->quiz)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="fa fa-star"></i></td>
																														<td>{{$library->quiz->name}}</td>
																														<td>{{$library->quiz->created_at}}</td>
																														<td class="text-center">{{$item->directory_name}}</td>
																														<th class="text-center">quiz</th>
																												</tr>
																											@elseif($library->assignment)
																												<tr>
																														<th style="width:20px;" class="text-center"><input type="checkbox" name="library_id[]" value="{{$library->id}}"></th>
																														<td class="text-center"><i class="fa fa-tasks"></i></td>
																														<td>{{$library->assignment->title}}</td>
																														<td>{{$library->assignment->created_at}}</td>
																														<td class="text-center">-</td>
																														<th class="text-center">Tugas</th>
																												</tr>
																											@endif
																										@endforeach
																									@endforeach
																								</tbody>
																						</table>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
															</div>
														</div>
													</td>
													<td>{{$groups->created_at}}</td>
													<td class="text-center">{{$item->directory_name}}</td>
													<th class="text-center">Group Shared</th>
													<th class="text-center color-primary">{{$groups->group_code}}</th>
											</tr>
										@endforeach
									</tbody>
							</table>
						</div>
	        </div>
	      </div>
	    </div>
		@else
		<form action="{{url('course/content/library/add/'.$course_id.'/'.$section_id)}}" method="post">
			{{ csrf_field() }}
			<button type="submit" class="btn btn-primary btn-raised">Tambah Kontent / Kuis</button>
			<br><br>
			<table id="example" class="display" style="width:100%">
	        <thead>
	            <tr>
									<th></th>
	                <th style="width:20px;"></th>
	                <th>Nama Kontent</th>
	                <th>Tanggal Dibuat</th>
									<th class="text-center">Directory</th>
									<th class="text-center">Tipe</th>
	            </tr>
	        </thead>
	        <tbody>
						@foreach($directory as $index => $item)
							@foreach($item->library as $index => $library)
							@if($library->content)
		            <tr>
										<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->content->id}}"></td>
		                <td class="text-center"><i class="{{content_type_icon($library->content->type_content)}}"></i></td>
		                <td>{{$library->content->title}}</td>
		                <td>{{$library->content->created_at}}</td>
		                <td class="text-center">{{$item->directory_name}}</td>
										<th class="text-center">{{$library->content->type_content}}</th>
		            </tr>
							@elseif($library->quiz)
								<tr>
										<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->quiz->id}}"></td>
										<td class="text-center"><i class="fa fa-star"></i></td>
										<td>{{$library->quiz->name}}</td>
										<td>{{$library->quiz->created_at}}</td>
										<td class="text-center">{{$item->directory_name}}</td>
										<th class="text-center">quiz</th>
								</tr>
							@elseif($library->assignment)
								<tr>
										<td class="text-center"><input type="checkbox" name="content_id[]" value="{{$library->assignment->id}}"></td>
										<td class="text-center"><i class="fa fa-tasks"></i></td>
										<td>{{$library->assignment->title}}</td>
										<td>{{$library->assignment->created_at}}</td>
										<td class="text-center">-</td>
										<th class="text-center">Tugas</th>
								</tr>
							@endif
							<div class="modal" id="ModalSharedContent-{{$library->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
										<div class="modal-content">
											<div class="panel panel-default">
												<div class="panel-body">
													<h3>Berbagi Konten / Kuis</h3>
													<div class="tab-content-share">
														<div class="tab">
															<ul class="nav nav-tabs-ver" role="tablist">
																<li class="nav-item">
																	<a class="nav-link active" href="#home-{{$library->id}}" aria-controls="home-{{$library->id}}" role="tab" data-toggle="tab" aria-selected="false">
																		<i class="fa fa-tags"></i> Public Library</a>
																</li>
																<li class="nav-item">
																	<a class="nav-link" href="#profile-{{$library->id}}" aria-controls="profile-{{$library->id}}" role="tab" data-toggle="tab" aria-selected="false">
																		<i class="fa fa-users"></i> Shared Library</a>
																</li>
															</ul>
														</div>
														<div class="nav-tabs-ver-container-content">
															<div class="card-block">
																<div class="tab-content">
																	<div role="tabpanel" class="tab-pane active" id="home-{{$library->id}}">
																		<h3>Berbagi Kepada Public</h3>
																		<p>
																			Bagikan kontent anda kepada public
																		</p>

																		<form action="{{url('library/shared/'.$library->id.'/2')}}" method="post">
																			<input type="hidden" name="_token" value="{{csrf_token()}}">
																			<button type="submit" class="btn btn-primary btn-raised">Bagikan Kontent</button>
																		</form>
																		<br>
																	</div>
																	<div role="tabpanel" class="tab-pane" id="profile-{{$library->id}}">
																		<h3>Berbagi Dengan Kode / Grup</h3>
																		<p>
																			Bagikan kontent library anda terhadap orang orang tertentu
																		</p>
																		<form action="{{url('library/shared/'.$library->id.'/3')}}" method="post">
																			<input type="hidden" name="_token" value="{{csrf_token()}}">
																			<button type="submit" class="btn btn-primary btn-raised">Bagikan Dengan Kode</button>
																			<button type="button" class="btn btn-primary btn-raised">Bagikan Dengan Grup</a>
																		</form>

																		<br>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
								</div>
							</div>
							@endforeach
						@endforeach
	        </tbody>
	    </table>
		</form>
		@endif
	</div>
</div>

@endsection

@push('script')
		<script type="text/javascript" src="/js/bootstrap-editable.min.js"></script>
		<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
		<script src="{{asset('js/sweetalert.min.js')}}"></script>
		<script type="text/javascript" src="{{url('plupload/plupload.full.min.js')}}"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script>
		// trigger modal add konten / quiz
		  function openCity(evt, cityName) {
		    var i, tabcontent, tablinks;
		    tabcontent = document.getElementsByClassName("tabcontent");
		    for (i = 0; i < tabcontent.length; i++) {
		        tabcontent[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablinks");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(cityName).style.display = "block";
		    evt.currentTarget.className += " active";
		  }

		  // trigger modal add konten / quiz

			$(document).ready(function() {
					$('#example').DataTable();
					$('#example2').DataTable();
					$('.example222').DataTable();
			    $('#example-1').DataTable();
			});
		</script>
@endpush
