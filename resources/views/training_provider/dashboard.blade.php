@extends('training_provider.layouts.app')

@section('title', Lang::get('back.dashboard.title'))

@push('style')
  <style>
    a.info-box {
      text-decoration: none;
      cursor: pointer;
    }
  </style>
@endpush

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
        <h2>@lang('back.dashboard.header')</h2>
      </div>

      <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="/admin/users" class="info-box bg-pink hover-expand-effect">
            <div class="icon">
              <i class="material-icons">people_alt</i>
            </div>
            <div class="content">
              <div class="text">@lang('back.dashboard.total_user')</div>
              <div class="number count-to" data-from="0" data-to="{{$all_users_total}}" data-speed="15" data-fresh-interval="20">{{$all_users_total}}</div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="/admin/instructor" class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
              <i class="material-icons">account_circle</i>
            </div>
            <div class="content">
              <div class="text">@lang('back.dashboard.total_teacher')</div>
              <div class="number count-to" data-from="0" data-to="{{$all_instructors_total}}" data-speed="1000" data-fresh-interval="20">{{$all_instructors_total}}</div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="/admin/users" class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
              <i class="material-icons">school</i>
            </div>
            <div class="content">
              <div class="text">@lang('back.dashboard.total_participant')</div>
              <div class="number count-to" data-from="0" data-to="{{$all_students_total}}" data-speed="1000" data-fresh-interval="20">{{$all_students_total}}</div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="/admin/courses" class="info-box bg-orange hover-expand-effect">
            <div class="icon">
              <i class="material-icons">menu_book</i>
            </div>
            <div class="content">
              <div class="text">@lang('back.dashboard.total_course')</div>
              <div class="number count-to" data-from="0" data-to="{{$all_courses_total}}" data-speed="1000" data-fresh-interval="20">{{$all_courses_total}}</div>
            </div>
          </a>
        </div>
      </div>

      {{-- <div class="row">
        <div class="col-md-3">
          <div class="panel panel-success text-center">
            <div class="panel-heading">
              <h3 class="panel-title">Total User</h3>
            </div>
            <div class="panel-body table-responsive">
              <strong>
                {{$all_users_total}}
              </strong>
            </div>
            <a href="/users">
              <div class="panel-footer">
                Lihat
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-info text-center">
            <div class="panel-heading">
              <h3 class="panel-title">Total Pengajar</h3>
            </div>
            <div class="panel-body table-responsive">
              <strong>
                {{$all_instructors_total}}
              </strong>
            </div>
            <a href="/instructor">
              <div class="panel-footer">
                Lihat
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default text-center">
            <div class="panel-heading">
              <h3 class="panel-title">Total Pelajar</h3>
            </div>
            <div class="panel-body table-responsive">
              <strong>
                {{$all_students_total}}
              </strong>
            </div>
            <a href="/users">
              <div class="panel-footer">
                Lihat
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-primary text-center">
            <div class="panel-heading">
              <h3 class="panel-title">Total Kelas</h3>
            </div>
            <div class="panel-body table-responsive">
              <strong>
                {{$all_courses_total}}
              </strong>
            </div>
            <a href="/courses">
              <div class="panel-footer">
                Lihat
              </div>
            </a>
          </div>
        </div>
      </div> --}}

      <div class="row">
        <!-- @include('admin.sidebar') -->

        {{-- KELAS AKTIF --}}
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="block-header">
            <h2>@lang('back.dashboard.active_course')</h2>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_courses_total_daily)}} @lang('back.dashboard.total_active_course') <small>(@lang('back.dashboard.active_daily'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.course_title_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_courses_total_daily as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_courses_total_weekly)}} @lang('back.dashboard.total_active_course') <small>(@lang('back.dashboard.active_weekly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.course_title_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_courses_total_weekly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_courses_total_monthly)}} @lang('back.dashboard.total_active_course') <small>(@lang('back.dashboard.active_monthly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.course_title_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_courses_total_monthly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_courses_total_anual)}} @lang('back.dashboard.total_active_course') <small>(@lang('back.dashboard.active_annual'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.course_title_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_courses_total_anual as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- PENGAJAR AKTIF --}}
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="block-header">
            <h2>@lang('back.dashboard.active_teacher')</h2>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_teachers_total_daily)}} @lang('back.dashboard.total_active_teacher') <small>(@lang('back.dashboard.active_daily'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.teacher_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_teachers_total_daily as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_teachers_total_weekly)}} @lang('back.dashboard.total_active_teacher') <small>(@lang('back.dashboard.active_weekly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.teacher_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_teachers_total_weekly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_teachers_total_monthly)}} @lang('back.dashboard.total_active_teacher') <small>(@lang('back.dashboard.active_monthly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.teacher_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_teachers_total_monthly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_teachers_total_anual)}} @lang('back.dashboard.total_active_teacher') <small>(@lang('back.dashboard.active_annual'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.teacher_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_teachers_total_anual as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- PESERTA AKTIF --}}
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="block-header">
            <h2>@lang('back.dashboard.active_participant')</h2>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_students_total_daily)}} @lang('back.dashboard.total_active_participant') <small>(@lang('back.dashboard.active_daily'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.participant_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_students_total_daily as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_students_total_weekly)}} @lang('back.dashboard.total_active_participant') <small>(@lang('back.dashboard.active_weekly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.participant_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_students_total_weekly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_students_total_monthly)}} @lang('back.dashboard.total_active_participant') <small>(@lang('back.dashboard.active_monthly'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.participant_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_students_total_monthly as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="header">
              <h2>{{count($all_active_students_total_anual)}} @lang('back.dashboard.total_active_participant') <small>(@lang('back.dashboard.active_annual'))</small></h2>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>@lang('back.dashboard.participant_name_table')</th>
                      <th>@lang('back.dashboard.last_seen_table')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($all_active_students_total_anual as $index => $data)
                      <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        {{-- <div class="col-md-12">
          <div class="card">
            <div class="body">
              <div class="row">
                <div class="col-md-4">
                  <h3 class="text-center">Kelas Aktif</h3>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">
                                {{count($all_active_courses_total_daily)}} Total Kelas Aktif <small>Hari ini</small>
                              </h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveCoursesDaily')" class="panel-title">
                                <span id="iconToggleShowActiveCoursesDaily">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveCoursesDaily" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_courses_total_daily as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->title}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=courses">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_courses_total_weekly)}} Total Kelas Aktif <small>Perminggu</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveCoursesWeekly')" class="panel-title">
                                <span id="iconToggleShowActiveCoursesWeekly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveCoursesWeekly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_courses_total_weekly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->title}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=courses">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_courses_total_monthly)}} Total Kelas Aktif <small>Perbulan</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveCoursesMonthly')" class="panel-title">
                                <span id="iconToggleShowActiveCoursesMonthly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveCoursesMonthly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_courses_total_monthly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->title}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=courses">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_courses_total_anual)}} Total Kelas Aktif <small>Pertahun</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveCoursesAnual')" class="panel-title">
                                <span id="iconToggleShowActiveCoursesAnual">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveCoursesAnual" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_courses_total_anual as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->title}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=courses">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <h3 class="text-center">Pengajar Aktif</h3>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-success text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_teachers_total_daily)}} Total Pengajar Aktif <small>Perhari</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveTeachersDaily')" class="panel-title">
                                <span id="iconToggleShowActiveTeachersDaily">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveTeachersDaily" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_teachers_total_daily as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=teachers">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-success text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_teachers_total_weekly)}} Total Pengajar Aktif <small>Perminggu</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveTeachersWeekly')" class="panel-title">
                                <span id="iconToggleShowActiveTeachersWeekly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveTeachersWeekly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_teachers_total_weekly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                              </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=teachers">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-success text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_teachers_total_monthly)}} Total Pengajar Aktif <small>Perbulan</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveTeachersMonthly')" class="panel-title">
                                <span id="iconToggleShowActiveTeachersMonthly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveTeachersMonthly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir DIlihat</th>
                            </tr>
                            @foreach($all_active_teachers_total_monthly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                              </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=teachers">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-success text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_teachers_total_anual)}} Total Pengajar Aktif <small>Pertahun</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveTeachersAnual')" class="panel-title">
                                <span id="iconToggleShowActiveTeachersAnual">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveTeachersAnual" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_teachers_total_anual as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}  {!!$data->school != null ? '<i>dari <strong>' . $data->school . '</strong><i>' : ''!!}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=teachers">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <h3 class="text-center">Peserta Aktif</h3>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-warning text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_students_total_daily)}} Total Peserta Aktif <small>Perhari</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveStudentsDaily')" class="panel-title">
                                <span id="iconToggleShowActiveStudentsDaily">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveStudentsDaily" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_students_total_daily as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=students">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-warning text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_students_total_weekly)}} Total Peserta Aktif <small>Perminggu</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveStudentsWeekly')" class="panel-title">
                                <span id="iconToggleShowActiveStudentsWeekly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveStudentsWeekly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_students_total_weekly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=students">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-warning text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_students_total_monthly)}} Total Peserta Aktif <small>Perbulan</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveStudentsMonthly')" class="panel-title">
                                <span id="iconToggleShowActiveStudentsMonthly">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div id="columnActiveStudentsMonthly" class="panel-body table-responsive">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_students_total_monthly as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=students">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="panel panel-warning text-center">
                        <div class="panel-heading">
                          <div class="row">
                            <div class="col-md-10 text-right">
                              <h3 class="panel-title">{{count($all_active_students_total_anual)}} Total Peserta Aktif <small>Pertahun</small></h3>
                            </div>
                            <div class="col-md-2 text-right">
                              <button style="background: transparent; border:none;" onclick="toggleColumn('ActiveStudentsAnual')" class="panel-title">
                                <span id="iconToggleShowActiveStudentsAnual">-</span>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body table-responsive" id="columnActiveStudentsAnual">
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Terakhir Dilihat</th>
                            </tr>
                            @foreach($all_active_students_total_anual as $index => $data)
                            <tr>
                              <td>{{$index +1}}</td>
                              <td>{{$data->name}}</td>
                              <td>{{ $data->created_at == null ? '' : $data->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                          </table>
                        </div>
                        <a href="/dashboard?type=students">
                          <div class="panel-footer text-right">
                            <strong>
                              Lihat Detail
                            </strong>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
      </div>
    </div>
  </section>
@endsection

@push('script')
  <script>
    function toggleColumn(column) {
      $("#column"+column).toggle(500);

      if($("#column"+column).is(':visible')) {
        $("#iconToggleShow"+column).html('-')
      } else {
        $("#iconToggleShow"+column).html('+')
      }
    }
  </script>
@endpush
