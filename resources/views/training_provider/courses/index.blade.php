@extends('training_provider.layouts.app')

@section('title')

@push('style')  
  <link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
@endpush

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>List Courses Pending</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>Course Name</th>
                                            <th>User Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($id_users as $iduser)

                                            @php
                                                $courses = get_courses($iduser->user_id);
                                            @endphp

                                            @foreach($courses as $no => $course)
                                            <tr>
                                                <td>{{ $course->title }}</td>
                                                <td>{{ $course->name }}</td>
                                                <td><a class="btn btn-primary" href="/training_provider/courses/approve/{{$course->id}}">Approve</a></td>
                                            </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection
