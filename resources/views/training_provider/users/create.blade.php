@extends('training_provider.layouts.app')

@section('title')
    
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>List Users</h2>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        @php
                            if(Session::get('error')){
                                $errors = Session::get('error');
                            }
                        @endphp
                        <form action="/training_provider/users/store" method="post">
                        {{ csrf_field() }}
                            <!-- <input type="hidden" class="form-control" name="idtrainingprovider" value="" required> -->
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="name">Nama User</label>
                                    <!-- <input placeholder="Nama User" type="text" class="form-control" name="name" id="name" required> -->
                                    <select class="select2" data-placeholder="Select category" name="user_id" style="width: 100%;">
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection