@extends('layouts.app')

@section('title', Lang::get('front.page_manage_ceritificate.title') . " - " . $course->title)

@push('style')
<style media="screen">
    .link-grade {
        text-decoration: underline;
    }
</style>
<style>
    .nav-tabs-ver-container .nav-tabs-ver {
        padding: 0;
        margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
        display: none;
    }
</style>
<style>
    .card.card-groups {
        background: #f9f9f9;
        transition: all 0.3s;
        border: 1px solid #f5f5f5;
        border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
        position: absolute;
        background-color: #fff;
        color: #333;
        right: 1rem;
        top: 1rem;
        cursor: pointer;
        border: 1px solid #4ca3d9;
        z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
        padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
        position: absolute;
        top: 1rem;
        right: 2rem;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 15rem;
        padding: 0;
        margin: .125rem 0 0;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .15);
        border-radius: .25rem;
        box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
        display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
        display: block;
        background: #fff;
        width: 100%;
        font-size: 14px;
        color: #212529;
        padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
        background: #4ca3d9;
        color: #fff;
    }

    .card.card-groups .card-block {
        width: 100%;
    }

    .card.card-groups.dropdown .card-block {
        width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
        font-size: 16px;
        font-weight: 500;
        color: #4ca3d9;
        margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
        text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
        font-weight: 400;
        color: #666;
    }
</style>
@endpush

@section('content')
<div class="bg-page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="headline-md no-m">@lang('front.page_manage_ceritificate.title')
                    <span>{{ $course->title }}</span></h2>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/">@lang('front.page_manage_atendee.breadcumb_home')</a></li>
                <li><a href="/course/dashboard">@lang('front.page_manage_atendee.breadcumb_manage_courses')</a></li>
                <li><a href="/course/preview/{{Request::segment(3)}}">{{ $course->title }}</a></li>
                <li>@lang('front.page_manage_ceritificate.title')</li>
            </ul>
        </div>
    </div>
</div>
<div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-3 nav-tabs-ver-container">
                <img src="https://assets.ingeniolms.com//uploads/courses/default.jpg" alt="" class="img-fluid mb-2">
                <div class="card no-shadow">
                    <ul class="nav nav-tabs-ver" role="tablist">
                        <li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
                        <li class="nav-item"><a class="nav-link" href="/course/atendee/{{$course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
                        <li class="nav-item"><a class="nav-link" href="/course/attendance/{{$course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
                        <li class="nav-item"><a class="nav-link" href="/course/grades/{{$course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>
                        <li class="nav-item"><a class="nav-link active" href="/courses/certificate/{{$course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>
                        <li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
                        @if(isTeacher(Auth::user()->id) === true)
                            <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/list_badges"><i class="fa fa-shield"></i> Badges</a></li>
                            @if($course->level_up == 1)
                                <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/level_settings"><i class="fa fa-trophy"></i> Levels</a></li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="attendee">

                        <center>
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {!! Session::get('success') !!}
                            </div>
                            @elseif(Session::has('error'))
                            <div class="alert alert-error alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {!! Session::get('error') !!}
                            </div>
                            @endif

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </center>

                        <div class="d-flex align-items-center justify-content-between">
                            <h3 class="headline headline-sm mt-0 mb-0">
                                @lang('front.page_manage_ceritificate.setting_text')</h3>
                        </div>
                        <form action="/course/ceritificate/users/save/{{ $course->id }}" method="POST">
                            {{ csrf_field() }}
                            <div>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>@lang('front.page_manage_ceritificate.table_no')</th>
                                        <th>@lang('front.page_manage_ceritificate.table_name')</th>
                                        <th>#</th>
                                    </tr>
                                    @foreach ($course_users as $index => $course_user)
                                    @php $isUserHaveCeritificate =
                                    \DB::table('ceritificate_course_users')->where(['user_id' => $course_user->user_id,
                                    'course_id' => $course_user->course_id])->first() @endphp
                                    <tr>
                                        <td>{{ $index +1 }}</td>
                                        <td>{{ $course_user->name }}</td>
                                        <td><input {{ $isUserHaveCeritificate ? 'checked' : '' }} name="user_id[]"
                                                value={{ $course_user->user_id }} type="checkbox"></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary btn-raised">@lang('front.general.text_save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@push('script')
@endpush