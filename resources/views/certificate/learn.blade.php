@extends('layouts.app')

@section('title', 'Sertifikat Kelas')

@push('style')
<style media="screen">
    .link-grade {
        text-decoration: underline;
    }
</style>
<style>
    .nav-tabs-ver-container .nav-tabs-ver {
        padding: 0;
        margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
        display: none;
    }
</style>
<style>
    .card.card-groups {
        background: #f9f9f9;
        transition: all 0.3s;
        border: 1px solid #f5f5f5;
        border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
        position: absolute;
        background-color: #fff;
        color: #333;
        right: 1rem;
        top: 1rem;
        cursor: pointer;
        border: 1px solid #4ca3d9;
        z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
        padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
        position: absolute;
        top: 1rem;
        right: 2rem;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 15rem;
        padding: 0;
        margin: .125rem 0 0;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .15);
        border-radius: .25rem;
        box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
        display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
        display: block;
        background: #fff;
        width: 100%;
        font-size: 14px;
        color: #212529;
        padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
        background: #4ca3d9;
        color: #fff;
    }

    .card.card-groups .card-block {
        width: 100%;
    }

    .card.card-groups.dropdown .card-block {
        width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
        font-size: 16px;
        font-weight: 500;
        color: #4ca3d9;
        margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
        text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
        font-weight: 400;
        color: #666;
    }
</style>

@endpush

@section('content')
<div class="bg-page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="headline-md no-m">Sertifikat <span>Kelas</span></h2>
            </div>
        </div>
    </div>
</div>
<div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-3 nav-tabs-ver-container">
                <img src="https://assets.ingeniolms.com//uploads/courses/default.jpg" alt="" class="img-fluid mb-2">
                <div class="card no-shadow">
                    <ul class="nav nav-tabs-ver" role="tablist">
                        <li class="nav-item"><a class="nav-link" href="/course/atendee/#"><i class="fa fa-users"></i>
                                Daftar Peserta</a></li>
                        <li class="nav-item"><a class="nav-link" href="/course/grades/#"><i
                                    class="fa fa-address-book-o"></i> Grade Book</a></li>
                        <li class="nav-item"><a class="nav-link active" href="/course/certificate/199"><i
                                    class="fa fa-address-book-o"></i> Sertifikat</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="attendee">

                        @if(!$certificate)

                         <div class="alert alert-info">
                             Belum ada sertifikat
                         </div>

                        @else
                            <input type="button" class="btn btn-primary btn-raised" onclick="printDiv('printableArea')" value="Print Sertifikat" />
                            
                            <div class="" id="printableArea">
                                <svg width="600" height="600" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <image href="{{ $certificate->image }}"
                                        height="600" width="600" />
                                    <text style="font-size: 20px; text-align: center;" x="40%" y="{{ $certificate->y_coordinate }}">{{ Auth::user()->name }}</text>
                                </svg>
                            </div>

                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@push('script')

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endpush