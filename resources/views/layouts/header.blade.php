<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Indie+Flower:400|">
<link rel="stylesheet" href="{{asset('/css/preload.min.css')}}">
<link rel="stylesheet" href="{{asset('/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/plugins.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/style.dq-light-blue.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/dq-style.css')}}">
<link rel="stylesheet" href="{{asset('/css/dq-style-2.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/font-awesome/css/font-awesome.min.css')}}"/>
{{--<link rel="stylesheet" href="{{asset('/css/all.css')}}">--}}
<link rel="shotcut icon" href="{!! Setting('favicon')->value != '' ? asset_url(Setting('favicon')->value) : asset('/img/favicon.png') !!}">
<!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
<![endif]-->
<style>
  .dq-project-bg{
    background-image: url(/img/project-bg.jpg);
    background-size: cover;
    background-position: top center;
    background-repeat: no-repeat;
  }

  .course-live{
    position:absolute !important;
    z-index:999;
    margin:10px 0 0 10px;
  }

  .col-md-3 .card .card-img-top {
    height: 170px;
    object-fit: cover;
  }

  .sliderWrap.slick-dotted.slick-slider {
    margin-bottom: 0;
  }

  .sliderWrap .slick-slide {
    padding-left: 0;
    padding-right: 0;
  }

  .sliderWrap .slick-list {
    padding: 0;
  }

  .sliderWrap .slick-dots {
    bottom: 2rem;
  }

  .sliderWrap .slick-dots li button:before {
    font-size: 50px;
    color: #eee;
  }

  .sliderWrap .slick-dots li.slick-active button:before {
    color: #fff;
    opacity: 1;
  }

  .sliderWrap .image {
    position: relative;
  }

  .sliderWrap .slider-text,
  .sliderWrap .slider-overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: flex-start;
    justify-content: flex-end;
  }

  .sliderWrap .slider-overlay {
    background: #000;
    opacity: 0.3;
    z-index: 0;
  }

  .sliderWrap .slider-text {
    flex-direction: column;
    padding: 10rem;
    z-index: 1;
  }

  .sliderWrap .slider-text .slider-title {
    font-size: 36px;
    font-weight: 700;
    color: #fff;
    margin-bottom: 0;
    line-height: 1;
  }

  .sliderWrap .slider-text .slider-subtitle {
    font-size: 18px;
    font-weight: 300;
    color: #fff;
    margin-bottom: 0;
    line-height: 1.5;
  }

  .sliderWrap .slider-text .slider-link {
    background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    color: #fff;
    border-radius: 5px;
    padding: 1rem 3rem;
    margin-top: 1rem;
  }

  .sliderWrap .image img {
    width: 100%;
    height: 75vh;
    object-fit: cover;
  }

  .bg-header {
    @if(Setting('header_gradient')->value == 'default')
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
    @elseif(Setting('header_gradient')->value == 'custom')
      background: linear-gradient(<?= isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : '0' ?>deg, <?= isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : '#3276a1' ?> 100%);
    @else
      background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    @endif
  }

  .bg-header .image{
    width: 100% !important;
  }

  .bg-header .text .header-link .btn-header{
    background-color: #fff112 !important;
  }

  .bg-header .text .header-link .btn-header-link{
    color: #fff112 !important;
  }

  .bg-page-title {
    @if(Setting('header_gradient')->value == 'default')
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
    @elseif(Setting('header_gradient')->value == 'custom')
      background: linear-gradient(<?= isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : '0' ?>deg, <?= isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : '#3276a1' ?> 100%);
    @else
      background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    @endif
  }

  .bg-page-title-negative{
    background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
  }

  .bg-page-title h2 {
    color: {{ isset(Setting('banner_font_color')->value) ? Setting('banner_font_color')->value : '#ffffff' }};
  }

  .top-title-bg {
    @if(Setting('header_gradient')->value == 'default')
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
    @elseif(Setting('header_gradient')->value == 'custom')
      background: linear-gradient(<?= isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : '0' ?>deg, <?= isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : '#3276a1' ?> 100%);
    @else
      background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    @endif
  }

  .top-title-bg .row {
    align-items: center;
    height: 250px;
  }

  .top-title-bg .headline-md,
  .top-title-bg p,
  .top-title-bg span,
  .top-title-bg a {
    color: {{ isset(Setting('banner_font_color')->value) ? Setting('banner_font_color')->value : '#ffffff' }};
  }

  .top-title-bg a:hover {
    text-decoration: underline;
  }

  .top-title-bg .headline-md {
    font-size: 40px;
  }

  .top-title-bg .headline-md.truncate {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    text-overflow: ellipsis;
    height: 96px;
  }

  .top-title-bg .headline-md span {
    font-weight: 400;
  }

  .divider-gradient-negative {
    background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
  }

  .landingpage-accent {
    display: block;
    width: 100%;
    height: .5rem;
    @if(Setting('header_gradient')->value == 'default')
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
    @elseif(Setting('header_gradient')->value == 'custom')
      background: linear-gradient(<?= isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : '0' ?>deg, <?= isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : '#3276a1' ?> 100%);
    @else
      background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    @endif
  }

  .landingpage-wrap {
    background: <?= isset(Setting('color_primary')->value) && Setting('color_primary')->value != null ? Setting('color_primary')->value : '#2980b9' ?>; /* LANDING PAGE SECTION */
  }

  .feature-wrap {
    background: <?= isset(Setting('feature_background_color')->value) && Setting('feature_background_color')->value != null ? Setting('feature_background_color')->value : '#2980b9' ?>; /* LANDING PAGE SECTION */
    color: <?= isset(Setting('feature_text_color')->value) && Setting('feature_text_color')->value != null ? Setting('feature_text_color')->value : '#ffffff' ?>;
    padding-top: 2rem;
    padding-bottom: 2rem;
  }

  .feature-wrap .app-feature-image {
    width: 100%;
  }

  .feature-wrap .headline-md {
    color: <?= isset(Setting('feature_text_color')->value) && Setting('feature_text_color')->value != null ? Setting('feature_text_color')->value : '#ffffff' ?>;
  }

  .feature-wrap .headline-line:after {
    background: #FFDD0B;
    top: 75%;
  }
</style>

<style>
  .ms-navbar-white {
    background-color: {{ isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : 'white' }}; /* NAVBAR BACKGROUND */
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item>a,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item>a {
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' }}; /* FONT NAVBAR */
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item>a:hover,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item>a:hover {
    color: {{ isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : 'black' }}; /* FONT NAVBAR HOVER */
  }

  .nav-left .nav-item>a {
    display: inline-flex;
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' }};
    padding: 1rem 1.5rem;
    font-weight: 500;
  }

  .nav-left .nav-item>a:hover {
    display: inline-flex;
    color: {{ isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : 'black' }};
    padding: 1rem 1.5rem;
    font-weight: 500;
  }

  .bd-scroll .ms-navbar-white {
    background: {{ isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : 'white' }};
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item>a:before,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item>a:before {
    background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' }};
  }

  .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu,
  .ms-lead-navbar .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu {
    top: 74px;
  }

  /* .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a,
  .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:hover,
  .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:focus,
  .ms-lead-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a,
  .ms-lead-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:hover,
  .ms-lead-navbar .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:focus {
    background: transparent;
  } */

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown.show>a,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:hover,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:focus,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown.show>a,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:hover,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown.show>a:focus {
    color: {{ isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : 'black' }}; /* FONT NAVBAR HOVER */
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:before,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:before {
    background-color: {{ isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : 'white' }};
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item {
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' }};
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:hover,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:focus,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:hover,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu li .dropdown-item:focus {
    background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' }} !important; /* LINK NAVBAR BACKGROUND */
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right .ms-tab-menu-right-container li a:hover,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right .ms-tab-menu-right-container li a:focus,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right .ms-tab-menu-right-container li a:hover,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right .ms-tab-menu-right-container li a:focus {
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .ms-navbar.navbar-mode .navbar-collapse,
  .ms-lead-navbar.navbar-mode .navbar-collapse {
    margin-right: 0;
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left {
    background: {{ isset(Setting('navbar_background_color')->value) ? Setting('navbar_background_color')->value : '#4ca3d9' }};
  }

  .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu,
  .ms-lead-navbar .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu {
    min-width: 250px;
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-right {
    display: inline-block;
    min-width: 250px;
    max-height: 500px;
    overflow-y: auto;
  }

  .ms-navbar .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link.no-arrow:after {
    display: none;
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link {
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : '#ffffff' }};
  }

  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link:hover,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link:focus,
  .ms-navbar-white .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link.active,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link:hover,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link:focus,
  .ms-header-white+.ms-navbar-dark .navbar-collapse .navbar-nav .nav-item.dropdown .dropdown-menu .ms-tab-menu .ms-tab-menu-left .nav-item a.nav-link.active {
    background-color: <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' ?>;
    color: <?= isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : 'black' ?>; /* FONT NAVBAR HOVER */
  }

  .ms-navbar-white .btn-navbar-menu {
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' }}; /* FONT NAVBAR */
  }

  .navbar-nav {
    width: 100%;
  }

  .nav.nav-tabs {
    display: flex;
    background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    border-bottom: 1px solid <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
  }

  .nav.nav-tabs.nav-tabs-full {
    display: table;
  }

  .nav.nav-tabs li {
    display: table-cell;
    width: auto;
  }

  .nav.nav-tabs li a {
    color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?>; /* FONT HORIZONTAL TABS */
    opacity: 0.75;
  }

  .nav.nav-tabs li a.active {
    background-color: rgba(0, 0, 0, 0.1);
    color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?>; /* FONT HORIZONTAL TABS */
    opacity: 1;
  }

  .nav.nav-tabs .ms-tabs-indicator {
    background-color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : '#333' ?>;
  }

  .nav-tabs-ver-container .nav-tabs-ver li a.active {
    background-color: #eee;
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .nav-tabs-ver-container .nav-tabs-ver li a:hover {
    color: #222;
  }

  .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary {
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary li a {
    color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?>;
    opacity: 0.75;
  }

  .nav-tabs-ver-container .nav-tabs-ver.nav-tabs-ver-primary li a.active {
    color: <?= isset(Setting('tabs_font_color')->value) ? Setting('tabs_font_color')->value : 'black' ?>;
    opacity: 1;
  }

  .nav-item>a:hover:before {
    height: 0 !important;
  }

  .nav-item>a:hover,
  .navbar .navbar-nav>li>a:hover,
  .navbar .navbar-nav>li>a:focus {
    background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#4ca3d9' }}; /* LINK NAVBAR BACKGROUND */
  }

  .nav-item.nav-item-category {
    margin-left: 0;
    margin-right: auto;
  }

  .menu-profile-dropdown {
    color: {{ isset(Setting('navbar_font_color')->value) ? Setting('navbar_font_color')->value : 'black' }} !important; /* FONT NAVBAR */
  }

  .menu-profile-dropdown:hover {
    color: {{ isset(Setting('navbar_font_color_hover')->value) ? Setting('navbar_font_color_hover')->value : 'white' }} !important; /* FONT NAVBAR HOVER */
  }
</style>

<style>
  a,
  .dropdown-menu li a:hover {
    color: #333;
    transition: all ease .1s;
  }

  .color-primary {
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }} !important;
  }

  .checkbox input[type=checkbox]:checked+.checkbox-material .check,
  label.checkbox-inline input[type=checkbox]:checked+.checkbox-material .check {
    color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    border-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
  }

  .checkbox input[type=checkbox]:checked+.checkbox-material .check:before,
  label.checkbox-inline input[type=checkbox]:checked+.checkbox-material .check:before {
    color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
  }

  .checkbox input[type=checkbox]:checked+.checkbox-material:after,
  label.checkbox-inline input[type=checkbox]:checked+.checkbox-material:after {
    background-color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
  }

  .form-control,
  .form-group .form-control,
  .form-group.is-focused .form-control {
    background-image: linear-gradient({{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }}, {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }}),linear-gradient(#d2d2d2,#d2d2d2)
  }

  .form-group .select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none;
  }

  .form-group .select2-container--default .select2-selection--multiple,
  .form-group .select2-container--default .select2-selection--single {
    background-image: linear-gradient(#1abc9c, #1abc9c),linear-gradient(#d2d2d2,#d2d2d2);
    background-size: 0 2px,100% 1px;
    background-repeat: no-repeat;
    background-position: center bottom,center calc(100% - 1px);
    background-color: transparent;
    height: 38px;
    border: none;
    border-radius: 0px;
    padding: 7px 0;
    font-size: 16px;
    color: #bbb;
    line-height: 1.42857;
  }

  .form-group .select2-container--default .select2-selection--single .select2-selection__rendered,
  ::placeholder,
  ::-ms-input-placeholder {
    color: #bbb;
  }

  .btn-circle.btn-circle-raised.btn-circle-primary,
  .btn.btn-raised.btn-primary,
  .btn.btn-fab.btn-primary,
  .btn-group-raised .btn.btn-primary,
  .input-group-btn .btn.btn-raised.btn-primary,
  .input-group-btn .btn.btn-fab.btn-primary,
  .btn-group-raised .input-group-btn .btn.btn-primary {
    background: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .btn-circle.btn-circle-raised.btn-circle-primary:before,
  .btn.btn-raised:not(.btn-link):hover.btn-primary,
  .btn.btn-raised:not(.btn-link):focus.btn-primary,
  .btn.btn-raised:not(.btn-link).active.btn-primary,
  .btn.btn-raised:not(.btn-link):active.btn-primary,
  .btn-group-raised .btn:not(.btn-link):hover.btn-primary,
  .btn-group-raised .btn:not(.btn-link):focus.btn-primary,
  .btn-group-raised .btn:not(.btn-link).active.btn-primary,
  .btn-group-raised .btn:not(.btn-link):active.btn-primary,
  .input-group-btn .btn.btn-raised:not(.btn-link):hover.btn-primary,
  .input-group-btn .btn.btn-raised:not(.btn-link):focus.btn-primary,
  .input-group-btn .btn.btn-raised:not(.btn-link).active.btn-primary,
  .input-group-btn .btn.btn-raised:not(.btn-link):active.btn-primary,
  .btn-group-raised .input-group-btn .btn:not(.btn-link):hover.btn-primary,
  .btn-group-raised .input-group-btn .btn:not(.btn-link):focus.btn-primary,
  .btn-group-raised .input-group-btn .btn:not(.btn-link).active.btn-primary,
  .btn-group-raised .input-group-btn .btn:not(.btn-link):active.btn-primary {
    /* background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' }}; */
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .btn-group .dropdown-menu.dropdown-menu-primary {
    background: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .btn-group .dropdown-menu.dropdown-menu-primary li a:hover,
  .btn-group .dropdown-menu.dropdown-menu-primary li a:focus {
    background-color: {{ isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#039be5' }};
  }

  .btn-group .dropdown-menu li a:hover,
  .btn-group .dropdown-menu li a:focus {
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .btn-see-all {
    padding: 0.5rem 1rem;
    border: 2px solid {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    border-radius: 5px;
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    transition: 0.5s;
  }

  .btn-see-all:hover{
    color: #fff;
    border: 2px solid {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    background: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .btn,
  .input-group-btn .btn {
    border-radius: 5px;
  }

  .btn i.after-text {
    margin-left: 1rem;
    margin-right: -1rem;
  }

  .headline-xs,
  .headline-sm,
  .headline-md,
  .headline-lg,
  .headline-xlg {
    margin-top: 0;
    margin-bottom: 0;
  }

  .headline-xs.headline-line,
  .headline-sm.headline-line,
  .headline-md.headline-line,
  .headline-lg.headline-line,
  .headline-xlg.headline-line {
    margin-bottom: 30px;
  }

  .headline-line:after {
    @if(Setting('header_gradient')->value == 'default')
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#3276a1' ?> 50%);
    @elseif(Setting('header_gradient')->value == 'custom')
      background: linear-gradient(<?= isset(Setting('custom_gradient_rotate')->value) ? Setting('custom_gradient_rotate')->value : '0' ?>deg, <?= isset(Setting('custom_gradient_primary')->value) ? Setting('custom_gradient_primary')->value : '#4ca3d9' ?> 0%, <?= isset(Setting('custom_gradient_secondary')->value) ? Setting('custom_gradient_secondary')->value : '#3276a1' ?> 100%);
    @else
      background: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    @endif
  }

  .card-paper .headline-xs,
  .card-paper .headline-sm,
  .card-paper .headline-md,
  .card-paper .headline-lg,
  .card-paper .headline-xlg {
    margin-top: 1rem;
  }

  .card-paper{
    color: #333;
    transition: all ease 0.3s;
    border: 0;
    border-radius: 5px;
    box-shadow: 1px 1px 20px rgba(0, 0, 0, 0.1);
    margin-bottom: 1rem;
  }

  .card-paper .card-img-top {
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }

  .card-paper .card-body {
    padding: 2rem;
  }

  .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover,
  .card-md .md-dropdown .md-dropdown-group .md-dropdown-menu .md-dropdown-item:hover,
  .members-tab .members-dropdown-group .members-dropdown-menu .members-dropdown-item:hover,
  .libraries-tab .libraries-dropdown-group .libraries-dropdown-menu .libraries-dropdown-item:hover {
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }} !important;
  }

  .card.card-info {
    border-bottom: solid 3px <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    border-radius: 3px;
  }

  .modal-dialog {
    margin: 50px auto;
  }

  ul.breadcrumb li a {
    color: <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' ?>;
    text-decoration: none;
  }

  .list-group a,
  .list-group-item div.item-content {
    color: #424242;
  }

  .list-group a:hover,
  .list-group a.list-group-item:hover,
  .list-group a.list-group-item:focus {
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .pagination .page-item .page-link {
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: all ease .2s;
    color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .pagination .page-item.active .page-link:hover,
  .pagination .page-item.active .page-link:focus {
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.1);
  }

  .pagination .page-item:hover .page-link,
  .pagination .page-item.active .page-link {
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
    border-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }

  .badge.badge-primary {
    background-color: {{ isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#4ca3d9' }};
  }
</style>

<style>
  @media (max-width: 767px) {
    .top-title-bg{
      max-height: none;
      margin-bottom: 2rem;
    }

    .top-title-bg .headline-md {
      font-size: 36px;
    }

    .top-title-bg .headline-md.truncate {
      height: 90px;
    }
  }

  @media(max-width: 576px) {
      .nav.nav-tabs li a {
          padding: 1.5rem 1rem;
      }
  }
</style>

{{-- <script>
  (function(d, w, c) {
      w.ChatraID = 'GaXcDrehkLRnyByxF';
      var s = d.createElement('script');
      w[c] = w[c] || function() {
          (w[c].q = w[c].q || []).push(arguments);
      };
      s.async = true;
      s.src = 'https://call.chatra.io/chatra.js';
      if (d.head) d.head.appendChild(s);
  })(document, window, 'Chatra');
</script> --}}

{{-- we chat --}}
{{-- <script src="https://wchat.freshchat.com/js/widget.js"></script> --}}
{{-- we chat --}}

<!-- Facebook Pixel Code -->
{{-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '599240126949149');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=599240126949149&ev=PageView&noscript=1"
/></noscript> --}}
<!-- End Facebook Pixel Code -->
@stack('style')
