<style>
  .ms-slidebar .ms-slidebar-menu>li.divider {
    background: #e3e3e3;
    height: 1px;
    padding: 0 2rem;
    margin: 2rem 0;
  }
  .ms-slidebar .ms-slidebar-menu>li.title {
    font-size: 12px;
    font-weight: 700;
    color: #ccc;
    text-transform: uppercase;
    letter-spacing: 3px;
    padding: 0.75rem 2rem;
  }
  .ms-slidebar .ms-slidebar-menu>li.profile {
    background: #fff;
    display: flex;
    align-items: flex-end;
    font-size: 16px;
    font-weight: 700;
    text-transform: capitalize;
    color: #333;
    padding: 0.75rem 2rem;
  }
  .ms-slidebar .ms-slidebar-menu>li.profile img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 50%;
    margin-right: 1rem;
  }
  .ms-slidebar .ms-slidebar-menu>li>a,
  .ms-slidebar .ms-slidebar-menu>li>a:hover,
  .ms-slidebar .ms-slidebar-menu>li>a:focus {
    background: #fff;
    display: flex;
    align-items: center;
    font-size: 14px;
    color: #333;
    padding: 0.75rem 2rem;
  }
  .ms-slidebar .ms-slidebar-menu li.menu .submenu {
    display: none;
    background: #fafafa;
    padding-left: 0rem;
    box-shadow: inset 0px 5px 5px rgba(0, 0, 0, 0.05);
  }
  .ms-slidebar .ms-slidebar-menu li.menu .submenu a {
    background: transparent;
    font-size: 14px;
    color: #333;
  }
  .ms-slidebar .ms-slidebar-menu li.menu .submenu.active {
    display: block;
    -webkit-animation: fadeIn 0.3s ease-in;
    animation: fadeIn 0.3s ease-in;
    padding-left: 2rem;
  }

  @keyframes fadeIn {
    from {
        opacity: 0;
        padding-left: 0rem;
    }
    to {
        opacity: 1;
        padding-left: 2rem;
    }
  }
</style>

<div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
  <div class="sb-slidebar-container">
    <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
      <form class="form-mobile" action="/search">
        <input class="form-nav-search" type="text" value="{{Request::get('q')}}" name="q" placeholder="Cari kelas ...">
        <button class="icon-nav-search" type="submit"></button>
      </form>

      @if(Auth::check())
        <li class="title">Akun</li>
        <li class="profile">
          <img src="{{avatar(Auth::user()->photo)}}" alt="{{Auth::user()->name}}">
          {{Auth::user()->name}}
        </li>
        <li><a href="{{url('profile')}}">Profil Saya <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
        <li><a href="{{url('logout')}}">Keluar <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>

        <li class="divider"></li>
        <li class="title">Menu Lainnya</li>

        {{-- <li><a href="{{url('my-course')}}">Kelas Saya <i class="fa fa-angle-right ml-auto mr-0"></i></a></li> --}}
        <li><a href="{{url('course/dashboard')}}">Kelas <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>

        @if(isInstructor(Auth::user()->id) === true)
          {{-- <li class="menu">
            <a href="#">Kelola <i class="fa fa-angle-down ml-auto mr-0"></i></a>
            <ul class="submenu">
              <li><a onclick="location.href='{{url('course/dashboard')}}'" href="">Kelas</a></li>
              <li><a onclick="location.href='{{url('instructor/groups')}}'" href="">Groups</a></li>
              <li><a onclick="location.href='{{url('instructor/library')}}'" href="">Libraries (Beta)</a></li>
            </ul>
          </li> --}}

          <li><a href="{{url('instructor/library')}}">Library <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
          <li><a href="{{url('instructor/groups')}}">Grup <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
        @else
          @if( isset(Setting('vcon')->value) && Setting('vcon')->value == 'true')
            <li><a href="{{url('vcon')}}">Vcon</a></li>
          @endif
        @endif

        @if( isset(Setting('vcon')->value) && Setting('vcon')->value == 'true')
          <li><a href="{{url('vcon')}}">Vcon <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
        @endif
      @else
        <li><a href="{{url('login')}}">Masuk <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>

        @if(isset(Setting('register')->value) && Setting('register')->value == 'true')
          <li><a href="{{url('login')}}">Daftar <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
        @endif

        @if( isset(Setting('vcon')->value) && Setting('vcon')->value == 'true')
          <li><a href="{{url('vcon')}}">Vcon <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
        @endif
      @endif

      @if( isset(Setting('menu_teacher')->value) && Setting('menu_teacher')->value == 'true')
        <li><a href="{{url('pengajar')}}">Kontributor <i class="fa fa-angle-right ml-auto mr-0"></i></a></li>
      @endif

      <li class="divider"></li>
      <li class="title">Kategori</li>
      {{-- <li class="menu">
        <a href="#">Category 1 <i class="fa fa-angle-right ml-auto mr-0"></i></a>
        <ul class="submenu">
          <li>
            <a href="#">Sub Category 1</a>
          </li>
          <li>
            <a href="#">Sub Category 2</a>
          </li>
        </ul>
      </li>
      <li class="menu">
        <a href="#">Category 2 <i class="fa fa-angle-right ml-auto mr-0"></i></a>
        <ul class="submenu">
          <li>
            <a href="#">Sub Category 1</a>
          </li>
          <li>
            <a href="#">Sub Category 2</a>
          </li>
        </ul>
      </li> --}}

      @php $Categories = DB::table('categories')->where('parent_category', '0')->get(); @endphp
      @foreach($Categories as $index => $Category)
        <li class="menu">
          <a href="#">{{$Category->title}} <i class="fa fa-angle-down ml-auto mr-0"></i></a>
          <ul class="submenu">
            @php $ChildCategories = DB::table('categories')->where('parent_category', $Category->id)->get(); @endphp
            @foreach($ChildCategories as $data)
              <li>
                <a onclick="location.href='/category/{{$data->slug}}'" href="">{{$data->title}}</a>
                {{-- <a href="/category/{{$data->slug}}">{{$data->title}}</a> --}}
              </li>
            @endforeach
          </ul>
        </li>
      @endforeach
    </ul>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('.menu a').on("click", function(e){
      $(this).next('.submenu').toggleClass('active');
      e.stopPropagation();
      e.preventDefault();
    });
  });
</script>
