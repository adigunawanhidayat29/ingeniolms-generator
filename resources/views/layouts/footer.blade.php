<style>
  .sub-footer-section {
    background: <?= isset(Setting('footer_background_color')->value) && Setting('footer_background_color')->value != null ? Setting('footer_background_color')->value : '#3d4349' ?>;
    color: <?= isset(Setting('footer_text_color')->value) && Setting('footer_text_color')->value != null ? Setting('footer_text_color')->value : '#ffffff' ?>;
  }

  .sub-footer-section .headline {
    font-size: 16px;
    font-weight: 700;
    margin-bottom: 2rem;
  }

  .sub-footer-section .address {
    max-width: 95%;
    font-size: 14px;
    color: <?= isset(Setting('footer_text_color')->value) && Setting('footer_text_color')->value != null ? Setting('footer_text_color')->value : '#ffffff' ?>;
    margin-bottom: 0;
  }

  .sf-link-group {
    padding: 0;
  }

  .sf-link-group li {
    list-style: none;
    line-height: 2.5;
  }

  .sf-link-group li a {
    font-size: 14px;
    color: <?= isset(Setting('footer_text_color')->value) && Setting('footer_text_color')->value != null ? Setting('footer_text_color')->value : '#ffffff' ?>;
  }

  .sf-link-group li a:hover {
    font-size: 14px;
    color: <?= isset(Setting('footer_text_color')->value) && Setting('footer_text_color')->value != null ? Setting('footer_text_color')->value . 'c0' : '#ffffffc0' ?>;
  }

  .sf-link-group li a i {
    margin-right: 1rem;
  }

  .sf-apps-group {
    text-align: center;
  }

  .sf-mobile-apps {
    width: 50%;
    height: 150px;
    object-fit: cover;
    object-position: top;
    margin-bottom: 2rem;
  }

  .sf-mobile-apps-download {
    display: inline-block;
    margin-right: 0.5rem;
  }

  .sf-mobile-apps-download img {
    background: transparent;
    width: 150px;
    border-radius: 5px;
    transition: all ease 0.2s;
  }

  .sf-mobile-apps-download:hover {
    box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.2);
  }

  @media (max-width: 768px) {

    .sf-social-group,
    .sf-link-group {
      margin-bottom: 2rem;
    }

    .sf-apps-group {
      text-align: left;
    }

    .sf-apps-group .sf-mobile-apps {
      display: none;
    }

    .sf-apps-group .sf-mobile-apps-download img {
      width: 125px;
    }
  }
</style>

<div class="wrap sub-footer-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <p class="headline">{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</p>
        <p class="address">{!! isset(Setting('footer_address')->value) ? Setting('footer_address')->value : Lang::get('front.footer.address_not_set') !!}</p>
        <ul class="sf-link-group">
          <li style="{!! Setting('footer_email')->value != null ? '' : 'display: none;' !!}">
            <a href="mailto:{!! Setting('footer_email')->value !!}" target="_blank"><i class="zmdi zmdi-email"></i> {!!
              isset(Setting('footer_email')->value) ? Setting('footer_email')->value : Lang::get('front.footer.email_not_set') !!}</a>
          </li>
          <li style="{!! Setting('footer_phone')->value != null ? '' : 'display: none;' !!}">
            <a href="tel:{!! Setting('footer_phone')->value !!}" target="_blank"><i class="zmdi zmdi-phone"></i> {!!
              isset(Setting('footer_phone')->value) ? Setting('footer_phone')->value : Lang::get('front.footer.phone_not_set')
              !!}</a>
          </li>
        </ul>
        <div class="sf-social-group">
          @if( Setting('footer_instagram')->value != '#' )
          <a href="{{ Setting('footer_instagram')->value }}"
            class="btn-circle btn-circle-xs btn-circle-raised btn-instagram"><i class="zmdi zmdi-instagram"></i></a>
          @endif
          @if( Setting('footer_facebook')->value != '#' )
          <a href="{{ Setting('footer_facebook')->value }}"
            class="btn-circle btn-circle-xs btn-circle-raised btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
          @endif
          @if( Setting('footer_twitter')->value != '#' )
          <a href="{{ Setting('footer_twitter')->value }}"
            class="btn-circle btn-circle-xs btn-circle-raised btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
          @endif
          @if( Setting('footer_youtube')->value != '#' )
          <a href="{{ Setting('footer_youtube')->value }}"
            class="btn-circle btn-circle-xs btn-circle-raised btn-youtube"><i class="zmdi zmdi-youtube-play"></i></a>
          @endif
          @if( Setting('footer_linkedin')->value != '#' )
          <a href="{{ Setting('footer_linkedin')->value }}"
            class="btn-circle btn-circle-xs btn-circle-raised btn-linkedin"><i class="zmdi zmdi-linkedin"></i></a>
          @endif
        </div>
      </div>

      <div class="col-md-2">
        <p class="headline">@lang('front.footer.title_about_us')</p>
        <ul class="sf-link-group">
          <li>
            <a
              href="/term-condition">{{isset(Setting('tnc_title')->value) ? Setting('tnc_title')->value : Lang::get('front.footer.title_term_and_condition')}}</a>
          </li>
          <li>
            <a
              href="/privacy-policy">{{isset(Setting('privacy_policy_title')->value) ? Setting('privacy_policy_title')->value : Lang::get('front.footer.title_privacy_policy')}}</a>
          </li>
          <li>
            <a
              href="/contact-us">{{isset(Setting('contact_us_title')->value) ? Setting('contact_us_title')->value : Lang::get('front.footer.title_contact_us')}}</a>
          </li>
        </ul>
      </div>

      <div class="col-md-2">
        <p class="headline">@lang('front.footer.documentations')</p>
        <ul class="sf-link-group">
          <li>
            <a href="/lmsdoc" target="_blank">@lang('front.footer.documentations_admin')</a>
          </li>
          <li>
            <a href="/lmsdoc" target="_blank">@lang('front.footer.documentations_instructor')</a>
          </li>
          <li>
            <a href="/lmsdoc" target="_blank">@lang('front.footer.documentations_student')</a>
          </li>
        </ul>
      </div>

      @if( Setting('footer_app_link')->value == 'true' )
        <div class="col-md-4 sf-apps-group">
          <p class="headline">@lang('front.footer.download_apps')</p>
          <img class="sf-mobile-apps" src="{{asset('img/apps-compress.png')}}">
          <div>
            <a class="sf-mobile-apps-download" href="{{ Setting('app_store_link')->value }}" target="_blank">
              <img src="{{asset('img/app-store.png')}}">
            </a>
            <a class="sf-mobile-apps-download" href="{{ Setting('google_play_link')->value }}" target="_blank">
              <img src="{{asset('img/play-store.png')}}">
            </a>
          </div>
        </div>
      @endif
    </div>
  </div>
</div>

<div class="wrap pt-2 pb-2 bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        {{-- FOOTER FOR MOBILE VIEW --}}
        <div class="d-sm-none d-block text-center">
          &copy; {!! isset(Setting('copyright')->value) ? Setting('copyright')->value : 'PT. Dataquest Leverage
          Indonesia.' !!}

          <div>
            <span style="position: relative; font-size: 14px; color: #ccc; margin-top: 2rem;">@lang('front.footer.choose_language')
              <select class="ChangeLanguage" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; cursor: pointer; opacity: 0;">
                @foreach(\App\Models\Language::get() as $lang)
                  <option {{Session::get('locale') == $lang->alias ? 'selected' : ''}} value="{{$lang->alias}}">
                    {{$lang->title}}</option>
                @endforeach
              </select>
            </span>
          </div>
        </div>

        {{-- FOOTER FOR DESKTOP VIEW --}}
        <div class="d-none d-sm-flex align-items-center justify-content-between">
          &copy; {!! isset(Setting('copyright')->value) ? Setting('copyright')->value : 'PT. Dataquest Leverage
          Indonesia.' !!}

          <div>
            <span style="position: relative; font-size: 14px; color: #ccc;">@lang('front.footer.choose_language')
              <select class="ChangeLanguage" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; cursor: pointer; opacity: 0;">
                @foreach(\App\Models\Language::get() as $lang)
                  <option {{Session::get('locale') == $lang->alias ? 'selected' : ''}} value="{{$lang->alias}}">
                    {{$lang->title}}</option>
                @endforeach
              </select>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
