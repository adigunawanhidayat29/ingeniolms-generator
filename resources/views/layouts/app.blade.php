<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>@yield('title') - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url_base" content="{{ url('') }}">
    <meta name="host_id" content="{{empty(Auth::user()) ? '' : Auth::user()->id}}">
    <meta name="app-parents" content=".">
    @stack('meta')
    @include('layouts.header')

    @if(\Request::segment(1) == 'course' && \Request::segment(2) == 'learn')
      @php
        $checkAuthorCourse = DB::table('courses')->where('slug', \Request::segment(3))->first();
        $cekAuthor = explode(',', $checkAuthorCourse->id_author);
      @endphp

      @if(in_array(Auth::user()->id, $cekAuthor))
        <style>
          .fixed-top {
            top: 46px;
          }
        </style>
      @endif
    @endif

    @if(Auth::check())
      @if (isAdmin(Auth::user()->id))
        <style>
          .fixed-top {
            top: 46px;
          }
        </style>
      @endif
    @endif

    <style>
      .preview-alert {
        position: sticky;
        display: flex;
        align-items: center;
        justify-content: center;
        background: orange;
        text-align: center;
        padding: 1rem;
        top: 0;
        z-index: 1200;
      }

      .preview-alert p {
        margin-bottom: 0;
        color: #424242;
      }

      .preview-alert .preview-link {
        text-decoration: underline;
        color: #424242;
      }

      .preview-alert .preview-link:hover {
        color: #000;
      }

      .preview-alert .close-btn {
        position: absolute;
        font-size: 20px;
        color: #fff;
        right: 2rem;
      }

      .preview-alert .close-btn:hover {
        color: #cc3333;
      }

      .admin-alert {
        position: sticky;
        display: flex;
        align-items: center;
        justify-content: space-between;
        background: #424242;
        font-size: 14px;
        text-align: center;
        top: 0;
        z-index: 1200;
      }

      .admin-alert .notify {
        position: absolute;
        width: 100%;
        color: #fff;
        margin-bottom: 0;
        z-index: -1;
      }

      .admin-alert ul {
        display: flex;
        align-items: center;
        list-style: none;
        padding: 0;
        margin: 0;
      }

      .admin-alert ul li {
        position: relative;
      }

      .admin-alert ul li a{
        display: inline-block;
        color: #fff;
        padding: 1rem;
      }

      .admin-alert ul li a:hover {
        background: #666;
      }

      .admin-alert ul li ul {
        position: absolute;
        display: none;
        background: #424242;
        width: 200px;
        max-width: 200px;
        list-style: none;
        text-align: left;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      }

      .admin-alert ul li:hover ul {
        display: block;
      }

      .admin-alert ul li ul li a {
        display: block;
        color: #fff;
        padding: 0.5rem 1rem;
      }

      .admin-alert ul li ul li a:hover {
        background: #666;
      }

      @media (max-width: 420px) {
        .preview-alert p {
          font-size: 12px;
        }
      }
    </style>
  </head>
  <body>
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>

    @if(Auth::check())
      @if (isAdmin(Auth::user()->id))
        <div class="admin-alert">
          <ul>
            <li>
              <a href="/admin"><i class="zmdi zmdi-view-dashboard"></i> @lang('front.header_menu.dashboard')</a>
            </li>
            <li class="d-none d-sm-inline">
              <a href="#"><i class="zmdi zmdi-plus-circle"></i> @lang('front.header_menu.title_add_new')</a>
              <ul>
                <li>
                  <a href="/admin/course/create">@lang('front.header_menu.courses')</a>
                </li>
                <li>
                  <a href="/admin/categories/create">@lang('front.header_menu.categories')</a>
                </li>
                <li>
                  <a href="/admin/users/create">@lang('front.header_menu.student')</a>
                </li>
                <li>
                  <a href="/admin/instructor/create">@lang('front.header_menu.instructor')</a>
                </li>
                <li>
                  <a href="/admin/vcons/create">@lang('front.header_menu.vcon')</a>
                </li>
                <li>
                  <a href="/admin/ceritificate">@lang('front.header_menu.ceritificates')</a>
                </li>
              </ul>
            </li>
          </ul>

          <p class="notify d-none d-sm-inline">@lang('front.header_menu.text_login_as_admin')</p>

          <ul>
            <li>
              <a href="/admin/setting/site"><i class="zmdi zmdi-settings"></i> @lang('front.header_menu.settings')</a>
            </li>
          </ul>
        </div>
      @endif

      @if (isEmployeer(Auth::user()->id))
      <div class="admin-alert">
          <ul>
            <li>
              <a href="/employeer"><i class="zmdi zmdi-view-dashboard"></i>&nbsp; Dashboard Employeer</a>
            </li>
          </ul>

          <p class="notify d-none d-sm-inline">Anda Login Sebagai Employeer</p>

        </div>
      @endif

      @if (isTrainingProvider(Auth::user()->id))
      <div class="admin-alert">
          <ul>
            <li>
              <a href="/training_provider"><i class="zmdi zmdi-view-dashboard"></i>&nbsp; Dashboard Training Provider</a>
            </li>
          </ul>

          <p class="notify d-none d-sm-inline">Anda Login Sebagai Training Provider</p>

        </div>
      @endif
    @endif

    @if(\Request::segment(1) == 'course' && \Request::segment(2) == 'learn')
      @if(in_array(Auth::user()->id, $cekAuthor))
        <div class="preview-alert">
          <p class="d-none d-sm-inline">@lang('front.header_menu.text_on_preview_mode') <a href="/course/preview/{{ $checkAuthorCourse->id }}" class="preview-link">@lang('front.header_menu.text_back_to_instructor_page')</a></p>
          <p class="d-inline d-sm-none">@lang('front.header_menu.text_preview_mode') <a href="/course/preview/{{ $checkAuthorCourse->id }}" class="preview-link">@lang('front.header_menu.text_back_to_instructor_page')</a></p>
        </div>
      @endif
    @endif

    <div class="ms-site-container">
      @include('layouts.navigation')

      @if(Auth::check())
        @if(Auth::user()->is_active == '0' || Auth::user()->is_active == null)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              <strong>@lang('front.information.title')</strong> @lang('front.information.text_email_verify') <a style="text-decoration: underline" href="{{ url('user/resend/activation') }}">@lang('front.information.button_email_verify')</a>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
      @endif

      @yield('content')
      @include('layouts.footer')
      @include('layouts.script')
    </div>

    @include('layouts.sidebar')

    <script src="{{asset('toastr/toastr.min.js')}}"></script>
    <script src="{{asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('bootstrap-datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

    <script>
      @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}")
      @endif
      @if(Session::has('flash_message'))
        toastr.success("{{Session::get('flash_message')}}")
      @endif
    </script>

    <script>
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true
      });

      $('.datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss'
      });
    </script>
    @stack('script-preview')

  </body>
</html>
