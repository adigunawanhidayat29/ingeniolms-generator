<link rel="stylesheet" href="{{asset('css/ingenio-icons.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Lobster+Two:400,700|Orbitron:400,500,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Arizonia|Great+Vibes|Playball" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<link rel="shotcut icon" href="{{asset('favicon.png')}}">
<!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
<![endif]-->

<style>
  .dq-project-bg{
    background-image: url(/img/project-bg.jpg);
    background-size: cover;
    background-position: top center;
    background-repeat: no-repeat;
  }
  .course-live{
    position:absolute !important;
    z-index:999;
    margin:10px 0 0 10px;
  }
</style>

<script>
    // (function(d, w, c) {
    //     w.ChatraID = 'GaXcDrehkLRnyByxF';
    //     var s = d.createElement('script');
    //     w[c] = w[c] || function() {
    //         (w[c].q = w[c].q || []).push(arguments);
    //     };
    //     s.async = true;
    //     s.src = 'https://call.chatra.io/chatra.js';
    //     if (d.head) d.head.appendChild(s);
    // })(document, window, 'Chatra');
</script>

@stack('style')
