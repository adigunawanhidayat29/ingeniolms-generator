<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>@yield('title') - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</title>
    <meta name="description" content="Material Style Theme">

    <link rel="stylesheet" href="{{asset('/font-awesome/css/font-awesome.min.css')}}">

    <!-- Material Design fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="{{asset('/css/style.dq-light-blue.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/bootstrap-material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/app-static-style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/dq-style-2.css')}}">

    {{-- <link rel="shotcut icon" href="{!! isset(Setting('favicon')->value) ? Setting('favicon')->value : '/img/ingenio-logo.png' !!}"> --}}
    <link rel="shotcut icon" href="{!! Setting('favicon')->value != '' ? asset_url(Setting('favicon')->value) : asset('/img/favicon.png') !!}">
    @stack('style')

    @if(\Request::segment(2) == 'learn')
      @php
        $checkAuthorCourse = DB::table('courses')->where('slug', \Request::segment(4))->first();
      @endphp

      @if(Auth::user()->id == $checkAuthorCourse->id_author)
        <style>
          .fixed-top {
            top: 46px;
          }

          #sidebar .player-content-list {
            height: calc(100% - 135px);
          }
        </style>
      @endif
    @endif

    <style>
      .preview-alert {
        position: sticky;
        display: flex;
        align-items: center;
        justify-content: center;
        background: orange;
        text-align: center;
        padding: 12px;
        top: 0;
        z-index: 1200;
      }

      .preview-alert p {
        font-family: Roboto,sans-serif;
        font-size: 15px;
        color: #424242;
        margin-bottom: 0;
      }

      .preview-alert .preview-link {
        text-decoration: underline !important;
        color: #424242;
      }

      .preview-alert .preview-link:hover {
        color: #000;
      }

      .preview-alert .close-btn {
        position: absolute;
        font-size: 20px;
        color: #fff;
        right: 20px;
      }

      .preview-alert .close-btn:hover {
        color: #cc3333;
      }
    </style>
  </head>
  <body>

    @if(\Request::segment(2) == 'learn')
      @if(Auth::user()->id == $checkAuthorCourse->id_author)
        <div class="preview-alert">
          <p>@lang('front.header_menu.text_on_preview_mode') <a href="/course/preview/{{ $checkAuthorCourse->id }}" class="preview-link">@lang('front.header_menu.text_back_to_instructor_page')</a></p>
        </div>
      @endif
    @endif

    @yield('content')

    <script src="{{asset('js/plugins.min.js')}}"></script>
    <script src="{{asset('js/app.min.js')}}"></script>
    <script src="{{asset('js/portfolio.js')}}"></script>

    @stack('script')

  </body>
</html>
