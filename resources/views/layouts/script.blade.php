<script src="{{asset('js/all.js')}}"></script>
<script type="text/javascript">
  $(".ChangeLanguage").change(function(){
    $.ajax({
      url : "/change-language",
      method : "post",
      data : {'locale':$(this).val(), _token : '{{ csrf_token() }}'},
      success: function(result){
        location.reload();
      }
    })
  })
</script>

@if ($errors->has('email'))
  <script type="text/javascript">
    $("#ms-account-modal").modal('show')
  </script>
@endif

@stack('script')
