<!-- Modal -->
<div class="modal modal-default" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog animated zoomIn animated-3x" role="document">
    <div class="modal-content">
      <div class="modal-header d-block shadow-2dp no-pb">
        <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="zmdi zmdi-close"></i>
          </span>
        </button>
        <div class="modal-title text-center">
          <img src="/img/ingenio-logo.png" alt="Ingenio">
          <br><br>
          {{-- <span class="ms-logo ms-logo-white ms-logo-sm mr-1">M</span>
          <h3 class="no-m ms-site-title">Material
            <span>Style</span>
          </h3> --}}
        </div>
        <div class="modal-header-tabs">
          <ul class="nav nav-tabs nav-tabs-full nav-tabs-2 nav-tabs-primary" role="tablist">
            <li class="nav-item" role="presentation">
              <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link active withoutripple">
                <i class="zmdi zmdi-account"></i> Login</a>
            </li>
            <li class="nav-item" role="presentation">
              <a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="nav-link withoutripple">
                <i class="zmdi zmdi-account-add"></i> Register</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="modal-body">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
            @if ($errors->has('email'))
              <div class="alert alert-danger">
                <strong>Gagal, Email atau password salah</strong>
              </div>
            @endif
            <form autocomplete="off" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              <fieldset>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-account"></i>
                    </span>
                    <label class="control-label" for="ms-form-email">Email</label>
                    <input type="email" id="ms-form-email" name="email" class="form-control" value="{{ old('email') }}">
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-lock"></i>
                    </span>
                    <label class="control-label" for="ms-form-pass">Password</label>
                    <input type="password" name="password" id="ms-form-pass" class="form-control"> </div>
                </div>
                <div class="row mt-2">
                  <div class="col-md-6">
                    <div class="form-group no-mt">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remember"> Remember Me </label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-raised btn-primary pull-right">Login</button>
                  </div>
                </div>
              </fieldset>
            </form>
            <div class="text-center">
              <a class="btn btn-link text-right" href="{{ route('password.request') }}">
                Lupa Password Anda?
              </a>
            </div>
            <div class="text-center">
              <h3>Login with</h3>
              <a href="/auth/facebook" class="wave-effect-light btn btn-raised btn-facebook">
                <i class="zmdi zmdi-facebook"></i> Facebook</a>
              <a href="/auth/google" class="wave-effect-light btn btn-raised btn-google">
                <i class="zmdi zmdi-google"></i> Google</a>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="ms-register-tab">
            <form method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <fieldset>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-account"></i>
                    </span>
                    <label class="control-label" for="ms-form-user">Name</label>
                    <input type="text" id="ms-form-user" name="name" class="form-control" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                      <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-email"></i>
                    </span>
                    <label class="control-label" for="ms-form-email">Email</label>
                    <input type="email" name="email" id="ms-form-email" class="form-control" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                      <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-phone"></i>
                    </span>
                    <label class="control-label" for="ms-form-phone">Phone</label>
                    <input type="text" id="ms-form-phone" name="phone" class="form-control" value="{{ old('phone') }}">
                    @if ($errors->has('phone'))
                      <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-lock"></i>
                    </span>
                    <label class="control-label" for="ms-form-pass">Password</label>
                    <input type="password" name="password" id="ms-form-pass" class="form-control">
                    @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-lock"></i>
                    </span>
                    <label class="control-label" for="ms-form-pass">Re-type Password</label>
                    <input type="password" name="password_confirmation" id="ms-form-pass" class="form-control"> </div>
                </div>
                <div class="form-group no-mt">
                  <div class="checkbox">
                    <label>
                      <input required type="checkbox" name="agree"> Dengan ini saya menyetujui Syarat & Ketentuan dan Kebijakan Privasi yang ditetapkan. </label>
                  </div>
                </div>
                <button type="submit" class="btn btn-raised btn-block btn-primary">Register Sekarang</button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end of modal -->
<nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-primary" style="background:#1E479D;">
  <div class="container container-full">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">
        <img src="/img/ingenio-logo-white.png" alt="Ingenio" style="">
        <!-- <img src="/img/demo/logo-navbar.png" alt=""> -->
      </a>
    </div>
    <div class="collapse navbar-collapse" id="ms-navbar">
      <ul class="navbar-nav">
        <li class="nav-item"><a href="{{url('/')}}"><span class="fa fa-home"></span></a></li>
        <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">Kategori
            <i class="zmdi zmdi-chevron-down"></i>
          </a>
          <ul class="dropdown-menu">
            @php $Categories = DB::table('categories')->get(); @endphp
            @foreach($Categories as $Category)
              <li>
                <a class="dropdown-item" href="/category/{{$Category->slug}}" class="dropdown-link">{{$Category->title}}</a>
              </li>
            @endforeach
          </ul>
        </li>
        <li class="nav-item"><a href="{{url('courses')}}">Kelas</a></li>
        <li class="nav-item"><a href="{{url('courses-live')}}">Kelas Live</a></li>
        {{-- <li class="nav-item"><a href="{{url('projects')}}">Proyek</a></li> --}}
        @if(Auth::check())
          @if(isInstructor(Auth::user()->id) === true)
            <li class="nav-item dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pengajar <i class="zmdi zmdi-chevron-down"></i></a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{url('course/dashboard')}}">Kelola Kelas</a></li>
                @if(isManager(Auth::user()->id) === true)
                <li><a class="dropdown-item" href="{{url('landingpage/builder/open')}}">Site Builder (Beta)</a></li>
                @endif
                {{-- <li><a class="dropdown-item" href="{{url('course/archived')}}">Kelas Arsip</a></li> --}}
                <li><a class="dropdown-item" href="{{url('instructor/library')}}">Library (Beta)</a></li>
                {{-- <li><a class="dropdown-item" href="{{url('course/create')}}">@lang('app.create_course')</a></li> --}}
                <li><a class="dropdown-item" href="{{url('instructor/transactions')}}">Transaksi</a></li>
                <li><a class="dropdown-item" href="{{url('video-learning-studio/steps')}}">Video Learning Studio</a></li>
                {{-- <li><a class="dropdown-item" href="{{url('instructor/groups')}}">Buat Group Pengajar</a></li> --}}
                @if(isManager(Auth::user()->id) === true)
                  <li><a class="dropdown-item" href="{{url('program/dashboard')}}">Program</a></li>
                  <li><a class="dropdown-item" href="{{url('instructor/groups')}}">Buat Group Pengajar</a></li>
                @else
                  <li><a class="dropdown-item" href="{{url('become-a-manager')}}">Menjadi Manager</a></li>
                @endif
              </ul>
            </li>
          @else
            <li class="nav-item"><a href="{{url('instructor')}}">@lang('app.become_instructor')</a></li>
          @endif
          <li class="nav-item"><a href="{{url('cart')}}"><i class="fa fa-shopping-cart no-m"></i> {!! Cart::count() == 0 ? '' : '<span class="badge-pill badge-pill-danger">'.Cart::count().'</span>'!!}</a></li>
          <li class="nav-item"><a href="{{url('my-course')}}">Kelas Saya</a></li>
          <li class="nav-item"><a href="{{url('profile')}}"><img src="{{avatar(Auth::user()->photo)}}" style="height:25px;border-radius:100%;" alt="{{Auth::user()->name}}"></a></li>
          <li class="nav-item"><a href="{{url('logout')}}">@lang('app.logout')</a></li>
        @else
          <li class="nav-item"><a href="{{url('instructor')}}">@lang('app.become_instructor')</a></li>
          <li class="nav-item"><a href="{{url('cart')}}"><i class="fa fa-shopping-cart"></i>{!! Cart::count() == 0 ? '' : '<span class="badge-pill badge-pill-danger">'.Cart::count().'</span>'!!}</a></li>
          <li class="nav-item"><a href="{{url('register')}}" data-toggle="modal" data-target="#ms-account-modal"><i class="icon-lock"></i>@lang('app.login') &#38; @lang('app.register')</a></li>
        @endif
      </ul>
    </div>
    <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
      <i class="zmdi zmdi-menu"></i>
    </a>
  </div>
  <!-- container -->
</nav>
