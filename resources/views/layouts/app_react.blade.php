<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Ingenio - @yield('title')</title>
    <meta name="description" content="Online Training bersama praktisinya Untuk Karir & Passionmu">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url_base" content="{{ url('') }}">
    <meta name="host_id" content="{{empty(Auth::user()) ? '' : Auth::user()->id}}">
    <meta name="app-parents" content=".">

    @stack('meta')
    @include('layouts.header_react')
  </head>
  <body style="background-color:white;">
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>

    <div class="ms-site-container">
        @yield('content')
    </div>

  </body>
</html>
