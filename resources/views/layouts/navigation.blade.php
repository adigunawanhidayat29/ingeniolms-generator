<nav class="navbar navbar-expand-md navbar-static ms-navbar ms-navbar-white navbar-mode" style="margin-bottom:0;">
  <div class="container container-full">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">
        <img src="{!! Setting('icon')->value != '' ? asset_url(Setting('icon')->value) : asset('img/ingenio-logo.png') !!}" alt="{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}" style="width: auto; max-width: 100px; height: auto; max-height: 50px;">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="ms-navbar">
      <ul class="navbar-nav">
        <div class="navbar-left">

          @if( isset(Setting('format_menu')->value) && Setting('format_menu')->value == 'catalog')
            @if( isset(Setting('menu_teacher')->value) && Setting('menu_teacher')->value == 'true')
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="catalog">@lang('front.menu.categories')
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="{{url('courses')}}">@lang('front.menu.courses')</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="{{url('pengajar')}}">@lang('front.menu.contributor')</a>
                  </li>
                </ul>
              </li>
            @else
              <li class="nav-item">
                <a href="{{url('courses')}}">@lang('front.menu.courses')</a>
              </li>
            @endif
          @endif

          @if( isset(Setting('format_menu')->value) && Setting('format_menu')->value == 'category')
            @if( isset(Setting('menu_categories')->value) && Setting('menu_categories')->value == 'true')
              <li class="nav-item nav-item-category dropdown">
                <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="home">@lang('front.menu.categories')
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="ms-tab-menu">
                    <ul class="nav nav-tabs ms-tab-menu-left" role="tablist">
                      @php $Categories = DB::table('categories')->where('parent_category', '0')->get(); @endphp
                      @foreach($Categories as $index => $Category)
                        @php $kat = DB::table('categories')->where('parent_category', $Category->id)->get(); @endphp
                        <li class="nav-item">
                          <a class="nav-link {{ $kat->count() == 0 ? 'no-arrow' : '' }} {{$index == 0 ? 'active' : ''}}" onclick="window.location.href='/category/{{$Category->slug}}'" href="#tab-{{$Category->id}}" data-hover="tab" data-toggle="tab" role="tab">
                            {{$Category->title}}</a>
                        </li>
                      @endforeach
                    </ul>
                    <div class="tab-content ms-tab-menu-right" style="min-width: auto;">
                      @foreach($Categories as $index => $Category)
                        @php $kat = DB::table('categories')->where('parent_category', $Category->id)->get(); @endphp
                        <div class="tab-pane {{$index == 0 ? 'active' : ''}}" id="tab-{{$Category->id}}" role="tabpanel" style="{{ $kat->count() != 0 ? 'width: 250px;' : '' }}">
                          <ul class="ms-tab-menu-right-container">
                            @php $ChildCategories = DB::table('categories')->where('parent_category', $Category->id)->get(); @endphp
                            @foreach($ChildCategories as $data)
                              <li>
                                <a href="/category/{{$data->slug}}">{{$data->title}}</a>
                              </li>
                            @endforeach
                          </ul>
                        </div>
                      @endforeach
                    </div>
                  </li>
                </ul>
              </li>
            @endif

            @if( isset(Setting('menu_teacher')->value) && Setting('menu_teacher')->value == 'true')
              <li class="nav-item"><a href="{{url('pengajar')}}">@lang('front.menu.contributor')</a></li>
            @endif
          @endif

          <li class="nav-item nav-item-search">
            <form action="/search">
              <input class="form-nav-search" type="text" name="q" placeholder="@lang('front.menu.search')">
              <span class="icon-nav-search"></span>
            </form>
          </li>
        </div>

        @if( isset(Setting('vcon')->value) && Setting('vcon')->value == 'true')
          <li class="nav-item"><a href="{{url('vcon')}}">@lang('front.menu.vcon')</a></li>
        @endif

        @if(Auth::check())
          @if(isInstructor(Auth::user()->id) === true)
            {{-- <li class="nav-item dropdown">
              <a href="{{url('course/dashboard')}}" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="blog">
                Kelola
                <i class="zmdi zmdi-chevron-down"></i>
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="{{url('course/dashboard')}}">Kelas</a></li>
                <li><a class="dropdown-item" href="{{url('instructor/groups')}}">Groups</a></li>
                <li><a class="dropdown-item" href="{{url('instructor/library')}}">Libraries (Beta)</a></li>
              </ul>
            </li> --}}
            <li class="nav-item"><a href="{{url('instructor/groups')}}">@lang('front.menu.group')</a></li>
            <li class="nav-item"><a href="{{url('instructor/library')}}">@lang('front.menu.library')</a></li>
            <li class="nav-item"><a href="{{url('course/dashboard')}}">@lang('front.menu.courses')</a></li>
          @else
            <li class="nav-item"><a href="{{url('course/dashboard')}}">@lang('front.menu.courses')</a></li>
          @endif
          {{-- <li class="nav-item"><a href="{{url('my-course')}}">{{ isset(Setting('menu_mycourse')->value) ? Setting('menu_mycourse')->value : 'Kelas Saya' }}</a></li> --}}
          <li class="nav-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
            <ul class="dropdown-menu notify-drop">
              <div class="notify-drop-title">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">Notifications (<b>2</b>)</div>
                  <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu."><i class="fa fa-dot-circle-o"></i></a></div>
                </div>
              </div>
              <!-- end notify title -->
              <!-- notify content -->
              <div class="drop-content">
                @php
                  $notifications = get_notifications(Auth::user()->id);
                @endphp
                @foreach($notifications as $notification)
                <li>
                  <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                  <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">{{$notification->description}}</a><a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                  
                  <hr>
                  <p class="time">{{$notification->created_at}}</p>
                  </div>
                </li>
                @endforeach
              </div>
              <div class="notify-drop-footer text-center">
                <a href="">See all</a>
              </div>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="blog">
              <img src="{{avatar(Auth::user()->photo)}}" style="width: 25px; height: 25px; object-fit: cover; border-radius: 100%;" alt="{{Auth::user()->name}}">
              <i class="zmdi zmdi-chevron-down"></i>
            </a>
            {{-- <a href="#" class="dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{avatar(Auth::user()->photo)}}" style="height:25px;border-radius:100%;" alt="{{Auth::user()->name}}"></a> --}}
            <ul class="dropdown-menu">
              <li>
                <a class="dropdown-item d-flex align-items-center menu-profile-dropdown"><img src="{{avatar(Auth::user()->photo)}}" style="width: 15px; height: 15px; object-fit: cover; border-radius: 100%; margin-right: 0.75rem;" alt="{{Auth::user()->name}}"> {{ Auth::user()->name }}</a>
              </li>
              <li class="divider m-0"></li>
              <li>
                <a class="dropdown-item menu-profile-dropdown" href="{{url('profile')}}">@lang('front.menu.profile')</a>
              </li>
              <li>
                <a class="dropdown-item menu-profile-dropdown" href="{{url('logout')}}">@lang('front.menu.logout')</a>
              </li>
            </ul>
          </li>

        @else
          <li class="nav-item"><a href="{{url('login')}}">@lang('front.menu.login')</a></li>

          @if(isset(Setting('register')->value) && Setting('register')->value == 'true')
            <li class="nav-item"><a href="{{url('register')}}">@lang('front.menu.register')</a></li>
          @endif

        @endif
      </ul>
    </div>
    <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
      <i class="zmdi zmdi-menu"></i>
    </a>
  </div>
</nav>

@push('script')

  <script>
    $('#dropdown-toggle').click(function(){
      $('.dropdown-canvas#category-toggle').toggle(200);
    });

  </script>

@endpush
