<table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#1E479D;">
  <tr>
    <td style="background-color:#1E479D;">

      <table cellspacing="0" cellpadding="0" class="force-full-width">
        <tr>
          <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center; padding-top:30px;" class="mobile-spacing">
            {{$title}}
            <br/><br/>
          </td>
        </tr>

      </table>

    </td>
  </tr>
</table>
