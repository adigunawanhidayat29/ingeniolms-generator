@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header')
        @endcomponent
    @endslot

    {{-- Subheader --}}
    @isset($subheader)
        @slot('subheader')
            @component('mail::subheader', ['title' => $subheader])
            @endcomponent
        @endslot
    @endisset

    {{-- Body --}}
    {{ $slot }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')

        @endcomponent
    @endslot
@endcomponent
