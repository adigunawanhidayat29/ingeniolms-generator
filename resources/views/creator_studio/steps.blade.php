@extends('layouts.app')

@section('title', 'Video Learning Studio')

@section('content')
  <div class="container container-full">
    <!-- STEP 1 -->
    <div class="card" id="step1" style="display:visible;">
      <div class="card-block text-center">
        <img class="mt-3" src="/img/VL/ingenio.png" style="max-width: 100%;">
        <h1 class="text-dark fs-50">Selamat Datang di Video Learning Studio</h1>
        <h3>Anda akan dipandu untuk membuat video learning yang cepat dan mudah!</h3>
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip1" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext1" class="btn btn-raised btn-primary pull-right">Start</a>
      </div>
    </div>
    <!-- STEP 2 -->
    <div class="card" id="step2" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Klik "Mulai Merekam!"</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c1.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip2" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext2" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev1" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 3 -->
    <div class="card" id="step3" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Apabila belum pernah membuat video silakan download launcher dan ikuti langkah-langkah instalasi pada komputer atau laptop Anda</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c2.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip3" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext3" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev2" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 4 -->
    <div class="card" id="step4" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Apabila jendela kamera telah terbuka setting microphone untuk merekam suara</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c3.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip4" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext4" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev3" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 5 -->
    <div class="card" id="step5" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Disarankan menggunakan resolusi 360p atau 480p agar tidak memberatkan proses di komputer Anda</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c4.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip5" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext5" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev4" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 6 -->
    <div class="card" id="step6" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Siapkan materi yang akan Anda rekam (bisa berupa presentasi powerpoint atau menggunakan aplikasi paint untuk menulis seperti di whiteboard)</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c5.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip6" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext6" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev5" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 7 -->
    <div class="card" id="step7" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Jangan lupa menyimpan posisi materi yang akan direkam di dalam kamera (di dalam kotak garis putus-putus)</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c6.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip7" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext7" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev6" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 8 -->
    <div class="card" id="step8" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Apabila sudah siap klik "Record"</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c7.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip8" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext8" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev7" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 9 -->
    <div class="card" id="step9" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Rileks namun bergairahlah dalam menyampaikan materi Anda</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c8.jpg" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip9" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext9" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev8" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 10 -->
    <div class="card" id="step10" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Apabila sudah selesai klik tombol "pause"</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c9.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip10" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext10" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev9" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 11 -->
    <div class="card" id="step11" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Klik tombol "done"</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c10.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip11" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext11" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev10" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 12 -->
    <div class="card" id="step12" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Klik tombol "publish" <i class="fa fa-long-arrow-right"></i>"Save as Video File" , simpan di direktori yang Anda kehendaki</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c11.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip12" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext12" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev11" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 13 -->
    <div class="card" id="step13" style="display:none;">
      <div class="card-block text-center">
        <img class="mt-3" src="/img/VL/ingenio.png" style="max-width: 100%;">
        <h1 class="text-dark fs-50">Proses perekaman selesai</h1>
        <h3>Anda tinggal mengupload videonya di kursus/training Anda!</h3>
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip13" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext13" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev12" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 14 -->
    <div class="card" id="step14" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Klik menu "Instruktur" <i class="fa fa-long-arrow-right"></i>"Kursus"</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c12.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip14" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext14" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev13" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 15 -->
    <div class="card" id="step15" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Upload konten video sesuai bagian bab/chapter yang Anda kehendaki</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c13.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip15" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext15" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev14" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 16 -->
    <div class="card" id="step16" style="display:none;">
      <div class="card-header bg-white">
        <h1 class="text-dark">Selesai! Anda berhasil memproduksi konten video learning bersama Ingenio</h1>
      </div>
      <div class="card-block text-center">
        <img src="/img/VL/c14.png" style="max-width: 100%;">
      </div>
      <div class="card-footer bg-white">
        <a id="btnSkip16" class="btn btn-raised btn-danger pull-left">Skip</a>
        <a id="btnNext16" class="btn btn-raised btn-primary pull-right">Next</a>
        <a id="btnPrev15" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
    <!-- STEP 17 -->
    <div class="card" id="step17" style="display:none;">
      <div class="card-block text-center">
        <img src="/img/VL/ingenio.png" style="max-width: 100%;">
        <h1 class="text-dark fs-50">Selamat!</h1>
      </div>
      <div class="card-footer bg-white">
        <a id="btnFinish" class="btn btn-raised btn-primary pull-right">Finish</a>
        <a id="btnPrev16" class="btn btn-raised btn-default pull-right">Prev</a>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script src="/js/component-carousels.js"></script>
  <script>
    /*
    $('#btnNext').click(function(){
      $('.card').hide(this,'.card');
      $('.card').next('.card').show();
    });
    */
    $('#btnNext1').click(function(){
      $('#step2').show();
      $('#step1').hide();
    })

    $('#btnPrev1').click(function(){
      $('#step1').show();
      $('#step2').hide();
    })

    $('#btnNext2').click(function(){
      $('#step3').show();
      $('#step2').hide();
    })

    $('#btnPrev2').click(function(){
      $('#step2').show();
      $('#step3').hide();
    })

    $('#btnNext3').click(function(){
      $('#step4').show();
      $('#step3').hide();
    })

    $('#btnPrev3').click(function(){
      $('#step3').show();
      $('#step4').hide();
    })

    $('#btnNext4').click(function(){
      $('#step5').show();
      $('#step4').hide();
    })

    $('#btnPrev4').click(function(){
      $('#step4').show();
      $('#step5').hide();
    })

    $('#btnNext5').click(function(){
      $('#step6').show();
      $('#step5').hide();
    })

    $('#btnPrev5').click(function(){
      $('#step5').show();
      $('#step6').hide();
    })

    $('#btnNext6').click(function(){
      $('#step7').show();
      $('#step6').hide();
    })

    $('#btnPrev6').click(function(){
      $('#step6').show();
      $('#step7').hide();
    })

    $('#btnNext7').click(function(){
      $('#step8').show();
      $('#step7').hide();
    })

    $('#btnPrev7').click(function(){
      $('#step7').show();
      $('#step8').hide();
    })

    $('#btnNext8').click(function(){
      $('#step9').show();
      $('#step8').hide();
    })

    $('#btnPrev8').click(function(){
      $('#step8').show();
      $('#step9').hide();
    })

    $('#btnNext9').click(function(){
      $('#step10').show();
      $('#step9').hide();
    })

    $('#btnPrev9').click(function(){
      $('#step9').show();
      $('#step10').hide();
    })

    $('#btnNext10').click(function(){
      $('#step11').show();
      $('#step10').hide();
    })

    $('#btnPrev10').click(function(){
      $('#step10').show();
      $('#step11').hide();
    })

    $('#btnNext11').click(function(){
      $('#step12').show();
      $('#step11').hide();
    })

    $('#btnPrev11').click(function(){
      $('#step11').show();
      $('#step12').hide();
    })

    $('#btnNext12').click(function(){
      $('#step13').show();
      $('#step12').hide();
    })

    $('#btnPrev12').click(function(){
      $('#step12').show();
      $('#step13').hide();
    })

    $('#btnNext13').click(function(){
      $('#step14').show();
      $('#step13').hide();
    })

    $('#btnPrev13').click(function(){
      $('#step13').show();
      $('#step14').hide();
    })

    $('#btnNext14').click(function(){
      $('#step15').show();
      $('#step14').hide();
    })

    $('#btnPrev14').click(function(){
      $('#step14').show();
      $('#step15').hide();
    })

    $('#btnNext15').click(function(){
      $('#step16').show();
      $('#step15').hide();
    })

    $('#btnPrev15').click(function(){
      $('#step15').show();
      $('#step16').hide();
    })

    $('#btnNext16').click(function(){
      $('#step17').show();
      $('#step16').hide();
    })

    $('#btnPrev16').click(function(){
      $('#step16').show();
      $('#step17').hide();
    })

    $('#btnSkip1').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip2').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip3').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip4').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip5').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip6').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip7').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip8').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip9').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip10').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip11').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip12').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip13').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip14').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip15').click(function(){
      window.location = '/video-learning-studio';
    });
    $('#btnSkip16').click(function(){
      window.location = '/video-learning-studio';
    });

    $('#btnFinish').click(function(){
      window.location = '/video-learning-studio';
    });
  </script>
@endpush
