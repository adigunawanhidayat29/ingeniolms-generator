@extends('layouts.app')

@section('title', 'Video Learning Studio')

@section('content')
  <div class="container">
    <div class="text-center">
      <h3>Video Learning Studio</h3>
      <div style="height:400px;border:1px solid #bbb; padding:20px; line-height:100px">
        <br>
        <div class="start-screen-recording-big recording-style-blue"><div><div class="rec-dot"></div><span>Mulai Merekam</span></div></div><script src="//api.apowersoft.com/screen-recorder?lang=en" defer></script>
      </div>
    </div>
  </div>
  <br>
@endsection
