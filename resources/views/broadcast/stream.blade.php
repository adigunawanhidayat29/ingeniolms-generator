@extends('layouts.app')

@section('title', 'Streaming')

<style media="screen">
  video{
    height: 500px;
    width:100%;
  }

  iframe{
    width: 100%;
    height: 650px;
  }
</style>

@section('content')
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="container">
          <h3><span id="textStreaming">Persiapan</span> Live Streaming</h3>

          @if(isInstructor(Auth::user()->id) === true)
            <div id="localVideo" style="width:100%;height:500px;border: 1px solid #bbb"></div>
            <input class="btn btn-info btn-raised" type="button" id="btnTestingStreaming" onclick="testingTransition()" value="Siap Streaming?"/>
            <input class="btn btn-primary btn-raised" type="button" id="btnLiveStreaming" onclick="liveTransition()" value="Mulai Streaming Sekarang"/>
            <input class="btn btn-danger btn-raised" type="button" id="btnStopStreaming" onclick="completeTransition()" value="Stop Streaming"/>
            <p id="status"></p>
          @else
            {!!$CourseBroadcast->embed_html!!}
            <br>
          @endif
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Chat</h3>
        </div>
        <div class="panel-body" id="panel_chat" style="height:600px; overflow:auto;">
          <div id="MessageShow"></div>
        </div>
        <div class="panel-footer text-right">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" rows="2" cols="80" placeholder="Write something..." id="MessageBody"></textarea>
                <button id="MessageSend" class="btn btn-primary btn-raised">Send</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('script')
  <script src="/pusher/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('6fe45cd1b03b7a9d8def', {
      cluster: 'ap1',
      encrypted: true
    });

    var channel = pusher.subscribe('broadcast-chat-{{$CourseBroadcast->id}}');
    channel.bind('get-broadcast-chat', function(data) {
      // console.log(data);

      var avatar = data.user.photo;
      var avatarSplit = avatar.split('/')
      if(avatarSplit[1] == 'uploads'){
        avatar = "{{asset_url()}}" + avatar;
      }else{
        avatar = avatar
      }

      var isUserSender = data.user.id;
      var isUserLoggedIn = "{{Auth::user()->id}}";

      if(isUserSender == isUserLoggedIn){
        var column_user = '<div class="row">'+
          '<div class="col-md-10">'+
            '<strong>'+data.user.name+'</strong><br>'+
            data.course_broadcast_chats.body+
          '</div>'+
          '<div class="col-md-2">'+
            '<img class="img-responsive img-circle" style="height:30px" src="'+avatar+'">'+
          '</div>'+
        '</div>'+
        '<hr>';
      }else{
        var audio = new Audio('/sounds/notif.mp3');
        audio.play();
        var column_user = '<div class="row">'+
          '<div class="col-md-2">'+
            '<img class="img-responsive img-circle" style="height:30px" src="'+avatar+'">'+
          '</div>'+
          '<div class="col-md-10">'+
            '<strong>'+data.user.name+'</strong><br>'+
            data.course_broadcast_chats.body+
          '</div>'+
        '</div>'+
        '<hr>';
      }

      $("#MessageShow").append(column_user);
    });
  </script>

  <script type="text/javascript">

    function updateScroll(){
      var element = document.getElementById("panel_chat");
      element.scrollTop = element.scrollHeight;
    }
    updateScroll();

    function MessageShow(){
      $.ajax({
        url : "{{url('course/broadcast/message/get/' . $CourseBroadcast->id)}}",
        type : "GET",
        success : function(result){
          $("#MessageShow").html(result);
        },
      });
    }
    MessageShow();
    // updateScroll();

    function MessageSend(){
      var course_id = '{{$CourseBroadcast->course_id}}';
      var broadcast_id = '{{$CourseBroadcast->id}}';
      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('course/broadcast/message/save')}}",
        type : "POST",
        data : {
          body : $("#MessageBody").val(),
          course_id : course_id,
          broadcast_id : broadcast_id,
          _token: token
        },
        success : function(result){
          // MessageShow();
          // updateScroll();
          $("#MessageBody").val('');
        },
      });
    }

    $("#MessageSend").click(function(){
      MessageSend();
    });

    $("#MessageBody").keyup(function(e){
      var code = (e.keyCode ? e.keyCode : e.which);
      if(code == 13) { //Enter keycode
        MessageSend();
      }
    });
  </script>

  {{-- flashphoner --}}
  <script type="text/javascript" src="/flashphoner/flashphoner.js"></script>
  <script type="text/javascript">
  var SESSION_STATUS = Flashphoner.constants.SESSION_STATUS;
  var STREAM_STATUS = Flashphoner.constants.STREAM_STATUS;
  var localVideo;
  $("#btnTestingStreaming").show()
  $("#btnLiveStreaming").hide()
  $("#btnStopStreaming").hide()

  function init_page() {
    Flashphoner.init();
    localVideo = document.getElementById("localVideo");
  }
  init_page()

  function publishToYoutube(){
    var urlServer = "wss://159.65.10.176:8443";
      Flashphoner.createSession({urlServer: urlServer}).on(SESSION_STATUS.ESTABLISHED, function(session){
      //session connected, start streaming
      startStreaming(session);

    }).on(SESSION_STATUS.DISCONNECTED, function(){
      setStatus("Connection DISCONNECTED");
    }).on(SESSION_STATUS.FAILED, function(){
      setStatus("Connection FAILED");
    });
  }
  publishToYoutube()

  function startStreaming(session) {
     var streamName =  "{{$CourseBroadcast->stream_name}}";
     var rtmpUrl =  "{{$CourseBroadcast->stream_url}}";
     session.createStream({
         name: streamName,
         display: localVideo,
         cacheLocalResources: true,
         receiveVideo: false,
         receiveAudio: false,
         rtmpUrl: rtmpUrl
     }).on(STREAM_STATUS.PUBLISHING, function(publishStream){
         setStatus(STREAM_STATUS.PUBLISHING);
     }).on(STREAM_STATUS.UNPUBLISHED, function(){
         setStatus(STREAM_STATUS.UNPUBLISHED);
     }).on(STREAM_STATUS.FAILED, function(){
         setStatus(STREAM_STATUS.FAILED);
     }).publish();
  }

  function setStatus(status){
     document.getElementById("status").innerHTML = status;
  }

  function testingTransition(){
    $.ajax({
      url : 'https://www.googleapis.com/youtube/v3/liveBroadcasts/transition?access_token={{Session::get('access_token')['access_token']}}&broadcastStatus=testing&id={{$CourseBroadcast->youtube_id}}&part=id,snippet,contentDetails,status',
      type : "post",
      success : function(response){
        // console.log(response);
        setTimeout(function() {
          $("#textStreaming").html('Mulai')
          $("#btnTestingStreaming").hide()
          $("#btnLiveStreaming").show()
        }, 5000);
      }
    })
  }

  function liveTransition(){
    $.ajax({
      url : 'https://www.googleapis.com/youtube/v3/liveBroadcasts/transition?access_token={{Session::get('access_token')['access_token']}}&broadcastStatus=live&id={{$CourseBroadcast->youtube_id}}&part=id,snippet,contentDetails,status',
      type : "post",
      success : function(response){
        // console.log(response);
        setTimeout(function() {
          $("#textStreaming").html('Sedang')
          $("#btnLiveStreaming").hide()
          $("#btnStopStreaming").show()
        }, 5000);
      }
    })
  }

  function completeTransition(){
    $.ajax({
      url : 'https://www.googleapis.com/youtube/v3/liveBroadcasts/transition?access_token={{Session::get('access_token')['access_token']}}&broadcastStatus=complete&id={{$CourseBroadcast->youtube_id}}&part=id,snippet,contentDetails,status',
      type : "post",
      success : function(response){
        // console.log(response);

        setTimeout(function() {
          $("#textStreaming").html('Selesai')
          $("#btnStopStreaming").val('Selesai')
          $("#btnStopStreaming").prop('disabled', true);
          completeStreaming();
        }, 5000);
      }
    })
  }

  function completeStreaming(){
    $.ajax({
      url : '/youtube/streaming/complete',
      type : "post",
      body: {
        _token : "{{csrf_token()}}",
        id : {{$CourseBroadcast->id}}
      },
      success : function(response){
        console.log(response);
      }
    })
  }

  </script>
  {{-- flashphoner --}}

  <script type="text/javascript">
    // notifi reload page
    window.onbeforeunload = function() {
      @if(isInstructor(Auth::user()->id) === true)
        return "Streaming akan berhenti jika Anda keluar. yakin?";
      @else
        return "Streaming akan terlewat jika Anda keluar. Yakin?";
      @endif
    };
  </script>

@endpush
