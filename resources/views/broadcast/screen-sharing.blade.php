@extends('layouts.app')

@section('title', 'Screen Sharing')

@push('style')
  <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/cpinanjhpabalmhpbcedgcapebnomjmc">
@endpush

@section('content')
  <div class="container">
    <h1>Screen Sharing</h1>
    <p>Install Now</p>
    <h2>Capture</h2>
    <div id="localVideo" style="width: 320px; height: 240px"> </div>
    <h2>Preview</h2>
    <div id="remoteVideo" style="width: 320px; height: 240px"> </div>
    <p>Connect and share screen</p>
    <p id="status"> </p>

    <button id="installExtensionButton" onclick="installExtension()" type="button">Install Now</button>

    <button id="publishBtn" onclick="connectAndShareScreen()" type="button">Connect and share screen</button>
    <p id="status"> </p>
  </div>

@endsection

@push('script')
  <script type="text/javascript" src="/flashphoner/flashphoner.js"></script>
  <script type="text/javascript">
    var SESSION_STATUS = Flashphoner.constants.SESSION_STATUS;
    var STREAM_STATUS = Flashphoner.constants.STREAM_STATUS;
    var localVideo;
    var remoteVideo;
    var extensionId = "cpinanjhpabalmhpbcedgcapebnomjmc";

    function init_page() {
     //init api
     try {
         Flashphoner.init({screenSharingExtensionId: extensionId});
     } catch (e) {
         //can't init
         return;
     }

     var interval = setInterval(function () {
         chrome.runtime.sendMessage(extensionId, {type: "isInstalled"}, function (response) {
             if (response) {
                 document.getElementById("installExtensionButton").disabled = true;
                 clearInterval(interval);
                 localVideo = document.getElementById("localVideo");
                 remoteVideo = document.getElementById("remoteVideo");
             } else {
                 document.getElementById("installExtensionButton").disabled = false;
             }
         });
     }, 500);

    }

    function connectAndShareScreen() {
     var url = "wss://159.65.10.176:8443";
     console.log("Create new session with url " + url);
     Flashphoner.createSession({urlServer: url}).on(SESSION_STATUS.ESTABLISHED, function (session) {
         //session connected, start streaming
         startStreaming(session);
     }).on(SESSION_STATUS.DISCONNECTED, function () {
         setStatus(SESSION_STATUS.DISCONNECTED);
     }).on(SESSION_STATUS.FAILED, function () {
         setStatus(SESSION_STATUS.FAILED);
     });

    }

    function startStreaming(session) {
     var streamName = "test123";
     var constraints = {
         video: {
             width: 320,
             height: 240,
             frameRate: 10,
             type: "screen"
         }
     };
     session.createStream({
         name: streamName,
         display: localVideo,
         constraints: constraints
     }).on(STREAM_STATUS.PUBLISHING, function (publishStream) {
         setStatus(STREAM_STATUS.PUBLISHING);
         //play preview
         session.createStream({
             name: streamName,
             display: remoteVideo
         }).on(STREAM_STATUS.PLAYING, function (previewStream) {
             //enable stop button
         }).on(STREAM_STATUS.STOPPED, function () {
             publishStream.stop();
         }).on(STREAM_STATUS.FAILED, function () {
             //preview failed, stop publishStream
             if (publishStream.status() == STREAM_STATUS.PUBLISHING) {
                 setStatus(STREAM_STATUS.FAILED);
                 publishStream.stop();
             }
         }).play();
     }).on(STREAM_STATUS.UNPUBLISHED, function () {
         setStatus(STREAM_STATUS.UNPUBLISHED);
         //enable start button
     }).on(STREAM_STATUS.FAILED, function () {
         setStatus(STREAM_STATUS.FAILED);
     }).publish();
    }

    //show connection or local stream status
    function setStatus(status) {
     var statusField = document.getElementById("status");
     statusField.innerHTML = status;
    }

    //install extension
    function installExtension() {
     chrome.webstore.install();
    }
  </script>
@endpush
