@extends('layouts.app')

@section('title', 'Create Streaming')

@section('content')
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="container">
          <h3>Create Broadcast</h3>
          <form class="" action="/youtube/streaming" method="get">
            {{csrf_field()}}
            <input type="hidden" name="course_id" value="{{$course_id}}">
            <div class="form-group">
              <label for="">Title</label>
              <input type="text" name="title" class="form-control" id="" placeholder="">
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea name="description" rows="4" class="form-control" style="border:1px solid #bbb"></textarea>
            </div>
            {{-- <div class="form-group">
              <label for="">Category</label>
              <select class="form-control selectpicker" name="category">
              <option value="unlisted">Unlisted</option>
              <option value="public">Public</option>
              </select>
            </div> --}}
            <input class="btn btn-primary btn-raised" type="submit" value="Start Broadcast"/>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
