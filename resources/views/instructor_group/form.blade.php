@extends('layouts.app')
@section('title', $button . ' ' . Lang::get('front.instructor_group.title'))

@push('style')
  <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">{{$button}} @lang('front.instructor_group.title')</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.instructor_group.breadcumb_home')</a></li>
          <li><a href="/instructor/groups">@lang('front.instructor_group.breadcumb_group')</a></li>
          <li>{{$button}} @lang('front.instructor_group.breadcumb_group')</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="card card-groups no-shadow mb-0">
        <div class="card-block">
          <form class="" action="{{$action}}" method="post">
            {{csrf_field()}}
            <div class="form-group m-0">
              <label for="">@lang('front.instructor_group.create_title')</label>
              <input type="text" class="form-control" id="" required placeholder="" name="title" value="{{$title}}">
            </div>
            <div class="form-group">
              <label for="name">@lang('front.instructor_group.create_add_instructor')</label>
              <select class="form-control" name="user_id[]" id="user_id" multiple required>
                @foreach($authors as $author)
                  <option {{ in_array($author->id, explode(',', $user_id)) ? 'selected' : '' }} value="{{$author->id}}">{{$author->name}} - {{$author->email}}</option>
                @endforeach()
              </select>
            </div>
            <div class="form-group">
              <label for="">@lang('front.instructor_group.create_description') @lang('front.instructor_group.create_description_optional')</label>
              <input type="text" class="form-control" id="" placeholder="" name="description" value="{{$description}}">
            </div>
            <a href="/instructor/groups" class="btn btn-default btn-raised">@lang('front.general.text_cancel')</a>
            <button type="submit" class="btn btn-primary btn-raised">@lang('front.general.text_save')</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  {{-- SELECT2 --}}
  <script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
  <script type="text/javascript">
    $("#user_id").select2({
       placeholder: "{{Lang::get('front.instructor_group.create_choose_instructor')}}"
    });
  </script>
  {{-- SELECT2 --}}
@endpush
