@extends('layouts.app')

@section('title', $instructor_groups->title)

@push('style')
  <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/library.css')}}">
  <link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('uploadify-master/sample/uploadifive.css')}}">
  <style>
    .nav-tabs-ver-container .nav-tabs-ver {
      padding: 0;
      margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
      display: none;
    }
  </style>

  <style>
    .card.card-groups {
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .btn-trash {
      position: absolute;
      right: 3rem;
      top: 1rem;
      padding: 5px;
    }

    .card.card-groups .btn-trash i{
      margin: 4px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .members-tab,
    .libraries-tab {
      display: flex;
      position: relative;
      align-items: center;
      justify-content: space-between;
      padding: 1.5rem 0;
      border-bottom: 1px solid #eee;
    }

    .members-tab .members-identity-group,
    .libraries-tab .libraries-identity-group {
      width: 100%;
      display: flex;
      align-items: center;
      margin-right: 2rem;
    }

    .members-tab .members-identity-group img,
    .libraries-tab .libraries-identity-group img {
      width: 30px;
      height: 30px;
      object-fit: cover;
      margin-right: 0.5rem;
    }

    .members-tab .members-identity-group a,
    .libraries-tab .libraries-identity-group a {
      color: #4ca3d9;
      text-decoration: none;
    }

    .members-tab .members-identity-group a:hover,
    .libraries-tab .libraries-identity-group a:hover {
      text-decoration: underline;
    }

    .members-tab .members-dropdown-group,
    .libraries-tab .libraries-dropdown-group {
      background: linear-gradient(180deg, #f3f3f3, #ccc);
      border: 1px solid #999;
      padding: 0rem 1rem;
      cursor: pointer;
    }

    .members-tab .members-dropdown-group .members-dropdown-menu,
    .libraries-tab .libraries-dropdown-group .libraries-dropdown-menu {
      display: none;
      position: absolute;
      background: #fff;
      min-width: 15rem;
      font-size: 14px;
      text-align: left;
      list-style: none;
      color: #212529;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      padding: 0;
      margin: .125rem 0 0;
      top: 4rem;
      right: 0;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
      z-index: 99;
    }

    .members-tab .members-dropdown-group:hover .members-dropdown-menu,
    .libraries-tab .libraries-dropdown-group:hover .libraries-dropdown-menu {
      display: block;
    }

    .members-tab .members-dropdown-group .members-dropdown-menu .members-dropdown-item,
    .libraries-tab .libraries-dropdown-group .libraries-dropdown-menu .libraries-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .members-tab .members-dropdown-group .members-dropdown-menu .members-dropdown-item:hover,
    .libraries-tab .libraries-dropdown-group .libraries-dropdown-menu .libraries-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .nav.nav-tabs-scroll::-webkit-scrollbar {
      height: 5px;
    }

    .nav.nav-tabs-scroll::-webkit-scrollbar-track {
      background: #fff;
    }

    .nav.nav-tabs-scroll::-webkit-scrollbar-thumb {
      background: #eee;
    }

    .nav.nav-tabs-scroll::-webkit-scrollbar-thumb:hover {
      background: #e3e3e3;
    }

    .nav.nav-tabs-scroll {
      display: flex;
      flex-wrap: nowrap;
      white-space: nowrap;
      overflow: scroll hidden;
      border-bottom: 0px;
    }

    .nav.nav-tabs-scroll li a {
      display: inline-flex;
      align-items: center;
    }

    /* Style the tab */
		.tab {
				float: left;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
				width: 30%;
				height: auto;
		}

		/* Style the buttons inside the tab */
		.tab button {
				display: block;
				background-color: inherit;
				color: black;
				padding: 22px 16px;
				width: 100%;
				border: none;
				outline: none;
				text-align: left;
				cursor: pointer;
				transition: 0.3s;
				font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
				background-color: #ddd;
		}

		/* Create an active/current "tab button" class */
		.tab button.active {
				background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
				float: left;
				padding: 0px 12px;
				border: 1px solid #ccc;
				width: 70%;
				border-left: none;
				height: auto;
		}

    /* .fa-folder-open{
      color: #4ca3d9;
    } */

    .mr-l{
      margin-right: 8px;
    }

    /* .nav-item>a:hover {
        background-color: #4cd137 !important;
        color: white !important;
    } */
    .form-group input[type=file] {
      opacity: 1;
      position: static;
    }

    .uploadifive-button:hover {
        background-color: none;
    }
  </style>

@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">{{$instructor_groups->title}}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.instructor_group.breadcumb_home')</a></li>
          <li><a href="/instructor/groups">@lang('front.instructor_group.breadcumb_group')</a></li>
          <li>{{$instructor_groups->title}}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">

      <center>
        @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! Session::get('message') !!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-error alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! Session::get('error') !!}
            </div>
        @endif
      </center>

      <div class="row">
        <div class="col-md-3 nav-tabs-ver-container">
          <img src="https://assets.ingeniolms.com//uploads/courses/default.jpg" alt="" class="img-fluid mb-2">
          <div class="card no-shadow">
            <ul class="nav nav-tabs-ver" role="tablist">
              <li class="nav-item active"><a class="nav-link active" href="#update" aria-controls="update" role="tab" data-toggle="tab"><i class="fa fa-list"></i> @lang('front.instructor_group.sidemenu_update')</a></li>
              <li class="nav-item"><a class="nav-link" href="#discussions" aria-controls="discussions" role="tab" data-toggle="tab"><i class="fa fa-comments-o"></i> @lang('front.instructor_group.sidemenu_discussion')</a></li>
              <li class="nav-item"><a class="nav-link" href="#members" aria-controls="members" role="tab" data-toggle="tab"><i class="fa fa-group"></i> @lang('front.instructor_group.sidemenu_members')</a></li>
              {{-- <li class="nav-item"><a class="nav-link" href="#libraries" aria-controls="libraries" role="tab" data-toggle="tab"><i class="fa fa-cubes"></i> Libraries</a></li> --}}
              {{-- <li class="nav-item"><a class="nav-link" href="{{url('instructor/library')}}"><i class="fa fa-cubes"></i> Libraries</a></li> --}}
            </ul>
          </div>
          <div class="card card-success-inverse no-shadow">
            <div class="card-block">
              <p class="fs-14 fw-600 mb-0">@lang('front.instructor_group.access_code')</p>
              <p class="fs-20 fw-300 text-uppercase" style="letter-spacing: 2px;">{{$instructor_groups->group_code}}</p>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="tab-content">
            {{-- Updates --}}
            <div role="tabpanel" class="tab-pane active" id="update">
              <h4 class="headline-md no-m"><b>@lang('front.instructor_group.sidemenu_update')</b></h4>
              <div class="komentar-first">@lang('front.instructor_group.action_send_update')</div>
              <div class="form-group no-m komentar-second hidden">
                <form action="{{url('instructor/group/update/submit')}}" method="post">
                  {{csrf_field()}}
                  <input type="hidden" name="group_id" value="{{$instructor_groups->id}}">
                  <textarea name="body" id="answer" placeholder="Berikan Komentar Anda untuk membantu penanya menemukan jawaban terbaik." class="form-control" rows="8" cols="80"></textarea>

                  <div class="upload-file-new" style="background: #f8f8f8;padding: 0px 10px;border: 1px solid #d1d1d1;">
                    <input id="file_upload" name="file_upload" type="file" multiple="true">
                    <div id="queue"></div>
                  </div>

                  <div class="form-group no-m">
                    <input type="submit" class="btn btn-primary btn-raised" id="submit-komentar" name="submit" value="@lang('front.instructor_group.action_send_update')">
                  </div>
                </form>
              </div>
              @if($instructor_groups->group_update)
                @foreach($instructor_groups->group_update->where('level_1', null) as $answer)
                <div class="card card-groups dropdown" style="box-shadow: 0px 6px 10px -5px;margin-top:20px;">
                  @if($answer->user_id == Auth::user()->id)
                  <button class="btn btn-danger btn-raised btn-trash" onclick="$('#form-koment-delete-{{$answer->id}}').submit()"><i class="fa fa-trash"></i></button>
                  <form action="{{url('/instructor/group/update/delete/'.$answer->id)}}" id="form-koment-delete-{{$answer->id}}" method="post" style="display:none;">
                    {{csrf_field()}}
                  </form>
                  @endif
                  <div class="card-block" style="width: 100%;">
                    <img src="{{avatar($answer->user->photo)}}" style="width: 25px; height: 25px; object-fit: cover; border-radius: 100%;    margin-right: 10px;" alt="{{$answer->user->name}}">
                    <a class="groups-title" href="#">{{$answer->user->name}} <span class="fs-14" style="color: #999;">{{time_elapsed_string($answer->created_at)}}&nbsp;&middot;&nbsp;</span></a>
                    <hr style="margin-top:15px">
                    <p class="groups-description">{!!$answer->description!!}</p>
                    @if($answer->file)
                    <?php
                      $files = json_decode($answer->file);
                      usort($files, function ($a, $b) use ($files) {

                          // preg_match("/(\d+(?:-\d+)*)/", $a[0], $matches);
                          // $firstimage = $matches[1];
                          // preg_match("/(\d+(?:-\d+)*)/", $b[0], $matches);
                          // $lastimage = $matches[1];
                          $ext = explode('.',$a[0]);
                          $ext = strtolower($ext[count($ext)-1]);
                          $firstimage = $ext;
                          $ext = explode('.',$b[0]);
                          $ext = strtolower($ext[count($ext)-1]);
                          $lastimage = $ext;


                          return ($firstimage != 'png') ? -1 : 1;
                      });
                    ?>
                    <p class="fs-14" style="color: #999;">Attachment</p>
                      <div class="row">
                        @foreach($files as $file)
                          <?php
                              $ext = explode('.',$file[0]);
                              $ext = strtolower($ext[count($ext)-1]);
                              $filename = $file[1];
                              $img = asset_url('uploads/fileupload/update/'.$file[0]);
                              if($ext == 'pdf'){
                                $img = asset('img/pdf.png');
                              }elseif ($ext == 'xlsx') {
                                $img = asset('img/excel.png');
                              }elseif ($ext == 'docx') {
                                $img = asset('img/word.png');
                              }
                           ?>
                          @if($ext == 'png' || $ext == 'jpeg' || $ext == 'jpg')
                            <div class="col-lg-3">
                              <div class="img-attachment">
                                <img src="{{$img}}" style="width:100%;height: 90px;border-radius: 11px;margin-bottom: 10px;" alt="{{$filename}}" title="{{$filename}}">
                                <a class="groups-title" href="{{asset_url('uploads/fileUpload/update/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                              </div>
                            </div>
                          @else
                          <div class="col-lg-12">
                            <div class="img-attachment">
                              <img src="{{$img}}" style="width:50px;margin-right:20px;" alt="{{$filename}}" title="{{$filename}}">
                              <a class="groups-title" href="{{asset_url('uploads/fileUpload/update/'.$file[0])}}" download="{{$filename}}">{{$filename}}</a>
                            </div>
                          </div>
                          @endif
                        @endforeach
                      </div>
                    @endif
                    <div class="reply-comment">
                      <hr style="border-color:#bbb;margin-bottom:10px">
                      <button class="reply-btn btn btn-info btn-raised">Balas</button>
                      <div class="text-reply">
                        <form method="POST" action="{{url('instructor/group/update/submit')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <input type="hidden" name="group_id" value="{{$instructor_groups->id}}">
                          <input name="parents" type="hidden" value="{{$answer->id}}">
                          <textarea name="body" id="discussion-reply-{{$answer->id}}" class="form-control discussion-reply" required="required"></textarea>
                          <input class="btn btn-primary btn-raised" name="button" type="submit" value="Submit Komentar">
                        </form>
                      </div>
                      <div class="reply_1-page" style="margin-top:20px;">
                        @if($answer->reply_1)
                          @foreach($answer->reply_1 as $reply_1)
                          <div class="card card-groups dropdown" style="box-shadow: none;margin:0px;border: none;border-top: 1px solid #d1d1d1;border-radius: 0;">
                            @if($reply_1->user_id == Auth::user()->id)
                            <button class="btn btn-danger btn-raised btn-trash"  onclick="$('#form-koment-delete-{{$reply_1->id}}').submit()" style="right: 1rem;"><i class="fa fa-trash"></i></button>
                            <form action="{{url('/course/content/discussion/delete/'.$reply_1->id)}}" id="form-koment-delete-{{$reply_1->id}}" method="post" style="display:none;">
                              {{csrf_field()}}
                            </form>
                            @endif
                            <div class="card-block">
                              <img src="{{avatar($reply_1->user->photo)}}" style="width: 25px; height: 25px; object-fit: cover; border-radius: 100%;    margin-right: 10px;" alt="{{$reply_1->user->name}}">
                              <a class="groups-title" href="#">{{$reply_1->user->name}} <span class="fs-14" style="color: #999;">{{time_elapsed_string($reply_1->created_at)}}&nbsp;&middot;&nbsp;</span></a>
                              <br>
                              <p class="groups-description">{!!$reply_1->description!!}</p>
                            </div>
                          </div>
                          @endforeach
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              @endif
            </div>

            {{-- Discussions --}}
            <div role="tabpanel" class="tab-pane" id="discussions">
              <h3 class="headline headline-sm mt-0 mb-2">@lang('front.instructor_group.discussion_title')</h3>
              <a class="btn btn-sm btn-raised btn-primary mt-0" href="#" data-toggle="modal" data-target="#createDiscussion">@lang('front.instructor_group.discussion_add')</a>
              @foreach($group_discussions as $group_discussion)
              <div class="card card-groups dropdown">
                <div class="groups-dropdown">
                  <div class="groups-dropdown-group">
                    <i class="fa fa-ellipsis-v"></i>
                    <ul class="groups-dropdown-menu">
                      <li>
                        <a class="groups-dropdown-item" href="#" data-toggle="modal" data-target="#editDiscussionModal-{{$group_discussion->id}}">
                          <i class="fa fa-pencil"></i> @lang('front.general.text_edit')
                        </a>
                      </li>
                      <li>
                        <a class="groups-dropdown-item" href="#" data-toggle="modal" data-target="#deleteDiscussionModal-{{$group_discussion->id}}">
                          <i class="fa fa-trash"></i> @lang('front.general.text_delete')
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <a class="groups-title" href="/instructor/groups/discussion/{{$group_discussion->id}}">{{$group_discussion->title}}</a>
                  {{-- <p class="groups-description fs-14" style="color: #999;">1 Post&nbsp;&middot;&nbsp;@lang('front.instructor_group.discussion_created_by') John Doe&nbsp;&middot;&nbsp;28 Mei 2020</p> --}}
                </div>
              </div>
              <div class="modal" id="editDiscussionModal-{{$group_discussion->id}}" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
                <form action="{{url('instructor/groups/discussion/update/'.$group_discussion->id)}}" method="post">
                  {{csrf_field()}}
                  <div class="modal-dialog animated zoomIn animated-3x" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                          <div class="form-group">
                            <label class="control-label">@lang('front.instructor_group.discussion_create_title')</label>
                            <input type="text" class="form-control" name="judul_diskusi" value="{{$group_discussion->title}}">
                          </div>
                          <div class="form-group">
                            <label class="control-label">@lang('front.instructor_group.discussion_create_description')</label>
                            <textarea name="diskusi_description" id="" cols="30" rows="3" class="form-control">{!!$group_discussion->description!!}</textarea>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">@lang('front.general.text_close')</button>
                        <button type="submit" class="btn btn-raised btn-primary">@lang('front.general.text_save')</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal" id="deleteDiscussionModal-{{$group_discussion->id}}" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
                <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                  <div class="modal-content">
                    <div class="modal-body">
                      <div class="d-flex align-items-center justify-content-between mb-2">
                        <h3 class="headline headline-sm m-0">@lang('front.instructor_group.discussion_delete')</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">
                            <i class="zmdi zmdi-close"></i>
                          </span>
                        </button>
                      </div>
                      <form action="{{url('instructor/groups/discussion/delete/'.$group_discussion->id)}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group text-center">
                          <p>@lang('front.instructor_group.discussion_delete_confirm')</p>
                          <p style="color:red"><i>@lang('front.instructor_group.discussion_delete_confirm_detail')</i></p>
                          <button type="button" class="btn btn-default btn-raised m-0" data-dismiss="modal">@lang('front.general.text_cancel')</button>
                          <button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.general.text_delete')</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>

            {{-- Members --}}
            <div role="tabpanel" class="tab-pane" id="members">
              <h3 class="headline headline-sm mt-0 mb-2">@lang('front.instructor_group.members_title')</h3>
              @foreach($members as $item)
              <div class="members-tab">
                <div class="members-identity-group">
                  <img src="/img/inspiration.png" alt="">
                  <p class="mb-0">{{$item->name}}&nbsp;<i class="fa fa-star" style="color: #ffc107;"></i></p>
                </div>
                <div class="members-dropdown-group">
                  <i class="fa fa-cog"></i>
                  <ul class="members-dropdown-menu">
                    {{-- <li>
                      <a class="members-dropdown-item" href="#">
                        <i class="fa fa-user-times"></i> Remove admin
                      </a>
                    </li> --}}
                    <li>
                      <a onclick="return confirm('@lang('front.instructor_group.members_remove_confirm')')" class="members-dropdown-item" href="/instructor/groups/{{$instructor_groups->id}}/remove/user/{{$item->id}}">
                        <i class="fa fa-users"></i> @lang('front.instructor_group.members_remove')
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              @endforeach
            </div>

            {{-- Libraries --}}
            <div role="tabpanel" class="tab-pane" id="libraries">
              <h3 class="headline headline-sm mt-0 mb-2">Libraries</h3>
              @if(Auth::user()->id == $instructor_groups->created_by)
              <a class="btn btn-sm btn-raised btn-primary mt-0" href="#" data-toggle="modal" data-target="#addLibrariesContent">Tambah Konten</a>
              @endif
              <div id="librariesOption" class="btn-group mt-0">
                <button type="button" class="btn btn-sm btn-raised btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Opsi <i class="zmdi zmdi-chevron-down right"></i>
                </button>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#"><i class="mr-0 fa fa-copy"></i> Copy</a></li>
                  <li><a class="dropdown-item" href="#"><i class="mr-0 fa fa-cut"></i> Move</a></li>
                  <li><a class="dropdown-item" href="#"><i class="mr-0 fa fa-trash"></i> Delete</a></li>
                </ul>
              </div>
              @foreach($instructor_groups->library as $item)
                <div class="libraries-tab">
                  <div class="libraries-identity-group">
                    <div class="form-group mt-0 mr-1 pb-0" style="line-height: 0;">
                      <input id="librariesCheckbox" type="checkbox" name="">
                    </div>
                    @if($item->content)
                      <?php $data = dataContent($item->content) ?>
                      <a href="#" class="mb-0"><i class="{{content_type_icon($data->type_content)}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</a>
                    @else
                      <?php $data = $item->folder; ?>
                      <a href="#" class="mb-0"><i class="fa fa-folder-open mr-l mr-l"></i> {{$data->group_name}}</a>
                    @endif
                  </div>
                  <div class="libraries-dropdown-group">
                    <i class="fa fa-cog"></i>
                    <ul class="libraries-dropdown-menu">
                      <!-- <li>
                        <a class="libraries-dropdown-item" href="#">
                          <i class="fa fa-pencil"></i> Edit
                        </a>
                      </li>
                      <li>
                        <a class="libraries-dropdown-item" href="#">
                          <i class="fa fa-copy"></i> Copy
                        </a>
                      </li>
                      <li>
                        <a class="libraries-dropdown-item" href="#">
                          <i class="fa fa-cut"></i> Move
                        </a>
                      </li> -->
                      <li>
                        <a class="libraries-dropdown-item" href="#" onclick="deleteCourse($(this))">
                          <i class="fa fa-trash"></i> Delete
                          <form class="delete-kontent" action="{{url('instructor/library/group/'.$item->id)}}" method="post">
                            {{csrf_field()}}
                          </form>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="createDiscussion" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
    <form action="{{url('instructor/groups/detail/'.$id.'/discussion/store')}}" method="post">
      {{csrf_field()}}
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="modal-body">
              <div class="form-group">
                <label class="control-label">@lang('front.instructor_group.discussion_create_title')</label>
                <input type="text" class="form-control" name="judul_diskusi">
              </div>
              <div class="form-group">
                <label class="control-label">@lang('front.instructor_group.discussion_create_description')</label>
                <textarea name="diskusi_description" id="" cols="30" rows="3" class="form-control"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-raised btn-default" data-dismiss="modal">@lang('front.general.text_cancel')</button>
            <button type="submit" class="btn btn-raised btn-primary">@lang('front.general.text_save')</button>
          </div>
        </div>
      </div>
    </form>
  </div>

  <div class="modal" id="addLibrariesContent" tabindex="-1" role="dialog" aria-labelledby="addLibrariesContentLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="card nav-tabs-ver-container mb-0">
          <div class="row">
            <div class="col-lg-3">
              <ul class="nav nav-tabs-ver" role="tablist" style="height: 100%;">
                <li class="nav-item"><a class="nav-link" href="#" data-dismiss="modal" style="color: #cc3333; border-bottom: 2px solid #ddd;"><i class="zmdi zmdi-close"></i> Close</a></li>
                <li class="nav-item"><a class="nav-link active" href="#contentVideo" aria-controls="contentVideo" role="tab" data-toggle="tab"><i class="zmdi zmdi-collection-video"></i> Video</a></li>
                <li class="nav-item"><a class="nav-link" href="#contentText" aria-controls="contentText" role="tab" data-toggle="tab"><i class="zmdi zmdi-collection-text"></i> Teks</a></li>
                <li class="nav-item"><a class="nav-link" href="#contentFile" aria-controls="contentFile" role="tab" data-toggle="tab"><i class="zmdi zmdi-collection-item"></i> File</a></li>
                <li class="nav-item"><a class="nav-link" href="#contentQuiz" aria-controls="contentQuiz" role="tab" data-toggle="tab"><i class="zmdi zmdi-assignment-check"></i> Kuis</a></li>
                <li class="nav-item"><a class="nav-link" href="#contentAssignment" aria-controls="contentAssignment" role="tab" data-toggle="tab"><i class="zmdi zmdi-assignment-o"></i> Tugas</a></li>
                <li class="nav-item"><a class="nav-link" href="#contentLibrary" aria-controls="contentLibrary" role="tab" data-toggle="tab"><i class="zmdi zmdi-archive"></i> Impor library</a></li>
              </ul>
            </div>
            <div class="col-lg-9 nav-tabs-ver-container-content">
              <div class="card-block">
                <div class="tab-content">
                  {{-- CONTENT VIDEO --}}
                  <div role="tabpanel" class="tab-pane active" id="contentVideo">
                    <form action="#">
                      <div class="form-group mt-0">
                        <label class="control-label">Judul</label>
                        <input type="text" class="form-control" placeholder="Masukkan judul">
                      </div>
                      <div class="form-group btn-group d-block">
                        <label class="control-label">Pilih sumber video konten anda</label>
                        <button type="button" class="form-control selectpicker btn dropdown-toggle d-flex align-items-center justify-content-between" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 15px;">
                          Opsi sumber video <i class="zmdi zmdi-chevron-down right"></i>
                        </button>
                        <ul class="dropdown-menu" style="width: 100%; margin-top: 34px;">
                          <li><a id="videoUploadOption" class="dropdown-item" href="#">Unggah video</a></li>
                          <li><a id="youtubeUploadOption" class="dropdown-item" href="#">Gunakan sumber dari youtube</a></li>
                        </ul>
                      </div>
                      <div id="videoUpload" class="form-group">
                        <label class="control-label">Unggah video</label>
                        <input type="text" readonly="" class="form-control" placeholder="Browse...">
                        <input type="file" id="inputFile" multiple="">
                      </div>
                      <div id="youtubeUpload" class="form-group">
                        <label class="control-label">Gunakan sumber dari youtube</label>
                        <input type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=8p08aY-SiM8">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Deskripsi</label>
                        <textarea name="description" id="descriptionVideo"></textarea>
                      </div>
                      <button type="submit" class="btn btn-raised btn-primary">Simpan dan terbitkan</button>
                    </form>
                  </div>

                  {{-- CONTENT TEXT --}}
                  <div role="tabpanel" class="tab-pane" id="contentText">
                    <form action="#">
                      <div class="form-group mt-0">
                        <label class="control-label">Judul</label>
                        <input type="text" class="form-control" placeholder="Masukkan judul">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Deskripsi</label>
                        <textarea name="description" id="descriptionText"></textarea>
                      </div>
                      <button type="submit" class="btn btn-raised btn-primary">Simpan dan terbitkan</button>
                    </form>
                  </div>

                  {{-- CONTENT FILE --}}
                  <div role="tabpanel" class="tab-pane" id="contentFile">
                    <form action="#">
                      <div class="form-group mt-0">
                        <label class="control-label">Judul</label>
                        <input type="text" class="form-control" placeholder="Masukkan judul">
                      </div>
                      <div class="form-group btn-group d-block">
                        <label class="control-label">Pilih sumber file konten anda</label>
                        <button type="button" class="form-control selectpicker btn dropdown-toggle d-flex align-items-center justify-content-between" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 15px;">
                          Opsi sumber file <i class="zmdi zmdi-chevron-down right"></i>
                        </button>
                        <ul class="dropdown-menu" style="width: 100%; margin-top: 34px;">
                          <li><a id="fileUploadOption" class="dropdown-item" href="#">Unggah file</a></li>
                          <li><a id="externalUploadOption" class="dropdown-item" href="#">Gunakan sumber dari link</a></li>
                        </ul>
                      </div>
                      <div id="fileUpload" class="form-group">
                        <label class="control-label">Unggah file (.docx, .xlsx, .pdf, etc.)</label>
                        <input type="text" readonly="" class="form-control" placeholder="Browse...">
                        <input type="file" id="inputFile" multiple="">
                      </div>
                      <div id="externalUpload" class="form-group">
                        <label class="control-label">Gunakan sumber dari link</label>
                        <input type="text" class="form-control" placeholder="Masukkan URL">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Deskripsi</label>
                        <textarea name="description" id="descriptionFile"></textarea>
                      </div>
                      <button type="submit" class="btn btn-raised btn-primary">Simpan dan terbitkan</button>
                    </form>
                  </div>

                  {{-- CONTENT QUIZ --}}
                  <div role="tabpanel" class="tab-pane" id="contentQuiz">
                    <form action="#">
                      <div class="form-group mt-0">
                        <label class="control-label">Judul</label>
                        <input type="text" class="form-control" placeholder="Masukkan judul">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Atur waktu mulai</label>
                        <div class="d-flex">
                          <input id="datePicker" type="text" class="form-control mx-1" name="" value="2020-05-29" placeholder="Tanggal" autocomplete="off" required>
                          <input type="time" class="form-control timePicker mx-1" name="" value="08:00" placeholder="Jam" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Atur waktu selesai</label>
                        <div class="d-flex">
                          <input id="datePicker" type="text" class="form-control mx-1" name="" value="2020-05-29" placeholder="Tanggal" autocomplete="off" required>
                          <input type="time" class="form-control timePicker mx-1" name="" value="08:00" placeholder="Jam" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Durasi (menit)</label>
                        <input type="number" class="form-control" name="" placeholder="0">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Deskripsi</label>
                        <textarea name="description" id="descriptionQuiz"></textarea>
                      </div>
                      <button type="submit" class="btn btn-raised btn-primary">Simpan dan terbitkan</button>
                    </form>
                  </div>

                  {{-- CONTENT ASSIGNMENT --}}
                  <div role="tabpanel" class="tab-pane" id="contentAssignment">
                    <form action="#">
                      <div class="form-group mt-0">
                        <label class="control-label">Judul</label>
                        <input type="text" class="form-control" placeholder="Masukkan judul">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Atur waktu mulai</label>
                        <div class="d-flex">
                          <input id="datePicker" type="text" class="form-control mx-1" name="" value="2020-05-29" placeholder="Tanggal" autocomplete="off" required>
                          <input type="time" class="form-control timePicker mx-1" name="" value="08:00" placeholder="Jam" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Atur waktu selesai</label>
                        <div class="d-flex">
                          <input id="datePicker" type="text" class="form-control mx-1" name="" value="2020-05-29" placeholder="Tanggal" autocomplete="off" required>
                          <input type="time" class="form-control timePicker mx-1" name="" value="08:00" placeholder="Jam" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Kesempatan menjawab</label>
                        <input type="number" class="form-control" name="" placeholder="0">
                      </div>
                      <div class="form-group">
                        <label class="control-label">Tipe tugas</label>
                        <select class="form-control selectpicker" data-dropup-auto="false">
                          <option value="">Online text</option>
                          <option value="">Upload file</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Deskripsi</label>
                        <textarea name="description" id="descriptionAssignment"></textarea>
                      </div>
                      <button type="submit" class="btn btn-raised btn-primary">Simpan dan terbitkan</button>
                    </form>
                  </div>

                  {{-- CONTENT LIBRARY --}}
                  <div role="tabpanel" class="tab-pane" id="contentLibrary">
                    <div class="card mb-0 no-shadow">
                      <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" href="#library-tab" aria-controls="library-tab" role="tab" data-toggle="tab">
                            <i class="fa fa-cubes" style="margin:0"></i> <span class="d-none d-sm-inline">Library</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#group-tab"  aria-controls="group-tab" role="tab" data-toggle="tab">
                            <i class="fa fa-users" style="margin:0"></i> <span class="d-none d-sm-inline">Group</span>
                          </a>
                        </li>
                      </ul>
                      <div class="tab-content">
                        {{-- LIBRARY TAB --}}
                        <div role="tabpanel" class="tab-pane active" id="library-tab">
                          <ul class="nav nav-tabs nav-tabs-scroll nav-tabs-transparent indicator-primary" role="tablist" style="height:100%">
                            <li class="nav-item">
                              <a class="nav-link active" href="#my-library-tab" aria-controls="my-library-tab" role="tab" data-toggle="tab">
                                <i class="fa fa-cubes"></i> My Library</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#shared-library-tab" aria-controls="shared-library-tab" role="tab" data-toggle="tab">
                                <i class="fa fa-share-alt"></i> Shared Library</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#public-library-tab" aria-controls="profile3" role="tab" data-toggle="tab">
                                <i class="fa fa-folder-open"></i> Public Library</a>
                            </li>
                          </ul>
                          <div class="tab-content mt-2">
                            <div role="tabpanel" class="tab-pane active" id="my-library-tab">
                              <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                {{csrf_field()}}
                                <div class="table-responsive table-cs">
                                  <table id="example-1" class="display" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Nama</th>
                                        <th class="text-center">Tipe</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($folder->where('user_id', Auth::user()->id) as $item)
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                        <td>
                                          @if($item->question_bank == 0)
                                          <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                            <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          </a>
                                          <div class="collapse" id="collapseExample-{{$item->id}}">
                                            <div class="card card-body">
                                              <table>
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Tipe</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($item->contents as $item_folder)
                                                  <?php
                                                      $data = dataContent($item_folder->library);
                                                      $icon = content_type_icon($data->type_content);
                                                      $type = 'Quiz';
                                                      if($data->type_content){
                                                        $type = $data->type_content;
                                                      }else if($data->type == "0"){
                                                        $type = 'Tugas';
                                                        $icon = "fa fa-tasks";
                                                      }
                                                    ?>
                                                  <tr>
                                                    <td class="text-center"><input type="checkbox" name="content[]" value="{{$item_folder->library->id}}/content"></td>
                                                    <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                    <td class="text-center">{{$type}}</td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                          @else
                                          <i class="fa fa-folder mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          @endif
                                        </td>
                                        <td class="text-center">{{$item->question_bank == 0 ? 'Folder' : 'Bank Soal'}}</td>
                                      </tr>
                                      @endforeach
                                      @foreach($myLibrary[0]->library as $indexItem => $item)
                                      <?php
                                          $data = dataContent($item);
                                          $icon = content_type_icon($data->type_content);
                                          $type = 'Quiz';
                                          if($data->type_content){
                                            $type = $data->type_content;
                                          }else if($data->type == "0"){
                                            $type = 'Tugas';
                                            $icon = "fa fa-tasks";
                                          }
                                        ?>
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                        <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                        <td class="text-center">{{$type}}</td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
                                </div>
                                <button type="submit" class="btn btn-raised btn-primary">Impor</button>
                              </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="shared-library-tab">
                              <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                {{csrf_field()}}
                                <div class="table-responsive table-cs">
                                  <table id="example-2" class="display" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Nama</th>
                                        <th class="text-center">Tipe</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($folder->where('user_id', '!=', Auth::user()->id) as $item)
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                        <td>
                                          @if($item->question_bank == 0)
                                          <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                            <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          </a>
                                          <div class="collapse" id="collapseExample-{{$item->id}}">
                                            <div class="card card-body">
                                              <table>
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Tipe</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($item->contents as $item_folder)
                                                  <?php
                                                      $data = dataContent($item_folder->library);
                                                      $icon = content_type_icon($data->type_content);
                                                      $type = 'Quiz';
                                                      if($data->type_content){
                                                        $type = $data->type_content;
                                                      }else if($data->type == "0"){
                                                        $type = 'Tugas';
                                                        $icon = "fa fa-tasks";
                                                      }
                                                    ?>
                                                  <tr>
                                                    <td class="text-center"><input type="checkbox" name="content[]" value="{{$item_folder->library->id}}/content"></td>
                                                    <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                    <td class="text-center">{{$type}}</td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                          @else
                                          <i class="fa fa-folder mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          @endif
                                        </td>
                                        <td class="text-center">Folder</td>
                                      </tr>
                                      @endforeach
                                      @foreach($myLibrary[2]->library as $item)
                                      <?php
                                          $data = dataContent($item);
                                          $icon = content_type_icon($data->type_content);
                                          $type = 'Quiz';
                                          if($data->type_content){
                                            $type = $data->type_content;
                                          }else if($data->type == "0"){
                                            $type = 'Tugas';
                                            $icon = "fa fa-tasks";
                                          }
                                        ?>
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                        <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                        <td class="text-center">{{$type}}</td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
                                </div>
                                <button type="submit" class="btn btn-raised btn-primary">Impor</button>
                              </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="public-library-tab">
                              <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                {{csrf_field()}}
                                <div class="table-responsive table-cs">
                                  <table id="example-3" class="display" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Nama</th>
                                        <th class="text-center">Tipe</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($folder->where('status', '1') as $item)
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                        <td>
                                          @if($item->question_bank == 0)
                                          <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                            <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          </a>
                                          <div class="collapse" id="collapseExample-{{$item->id}}">
                                            <div class="card card-body">
                                              <table>
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Tipe</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($item->contents as $item_folder)
                                                  <?php
                                                      $data = dataContent($item_folder->library);
                                                      $icon = content_type_icon($data->type_content);
                                                      $type = 'Quiz';
                                                      if($data->type_content){
                                                        $type = $data->type_content;
                                                      }else if($data->type == "0"){
                                                        $type = 'Tugas';
                                                        $icon = "fa fa-tasks";
                                                      }
                                                    ?>
                                                  <tr>
                                                    <td class="text-center"><input type="checkbox" name="content[]" value="{{$item_folder->library->id}}/content"></td>
                                                    <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                    <td class="text-center">{{$type}}</td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                          @else
                                          <i class="fa fa-folder mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                          @endif
                                        </td>
                                        <td class="text-center">Folder</td>
                                      </tr>
                                      @endforeach

                                      @foreach($publicLibrary->library as $item)
                                      <?php
                                          $data = dataContent($item);
                                          $icon = content_type_icon($data->type_content);
                                          $type = 'Quiz';
                                          if($data->type_content){
                                            $type = $data->type_content;
                                          }else if($data->type == "0"){
                                            $type = 'Tugas';
                                            $icon = "fa fa-tasks";
                                          }
                                        ?>
                                      <tr>
                                        <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                        <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                        <td class="text-center">{{$type}}</td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
                                </div>
                                <button type="submit" class="btn btn-raised btn-primary">Impor</button>
                              </form>
                            </div>
                          </div>
                        </div>

                        {{-- GROUP TAB --}}
                        <div role="tabpanel" class="tab-pane" id="group-tab">
                          <ul class="nav nav-tabs nav-tabs-scroll nav-tabs-transparent indicator-primary" role="tablist">
                            @foreach($group as $index => $item)
                              <li class="nav-item">
                                <a class="nav-link {{$index == 0 ? 'active' : ''}}" href="#my-library-tab-{{$item->group_code}}" aria-controls="my-library-tab-{{$item->group_code}}" role="tab" data-toggle="tab">
                                  <i class="fa fa-group"></i> {{$item->title}}
                                </a>
                              </li>
                            @endforeach
                          </ul>
                          <div class="tab-content mt-2">
                            @foreach($group as $index => $item)
                              <div role="tabpanel" class="tab-pane {{$index == 0 ? 'active' : ''}}" id="my-library-tab-{{$item->group_code}}">
                                <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                  {{csrf_field()}}
                                  <div class="table-responsive table-cs">
                                    <table id="example-group-{{$item->group_code}}" class="display" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th></th>
                                          <th>Nama</th>
                                          <th class="text-center">Tipe</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($item->library as $content)
                                          @if($content->folder)
                                            <tr>
                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                              <td>
                                                @if($content->folder->question_bank == 0)
                                                <a class="btn btn-primary" style="padding: 0;" data-toggle="collapse" href="#collapseExample-Group-{{$content->folder->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-Group-{{$content->folder->id}}">
                                                  <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$content->folder->group_name}}
                                                </a>
                                                <div class="collapse" id="collapseExample-Group-{{$content->folder->id}}">
                                                  <div class="card card-body">
                                                    <table>
                                                      <thead>
                                                        <tr>
                                                          <th></th>
                                                          <th>Nama</th>
                                                          <th class="text-center">Tipe</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        @foreach($content->folder->contents as $item_folder)
                                                        <?php
                                                            $data = dataContent($item_folder->library);
                                                            $icon = content_type_icon($data->type_content);
                                                            $type = 'Quiz';
                                                            if($data->type_content){
                                                              $type = $data->type_content;
                                                            }else if($data->type == "0"){
                                                              $type = 'Tugas';
                                                              $icon = "fa fa-tasks";
                                                            }
                                                          ?>
                                                        <tr>
                                                          <td class="text-center"><input type="checkbox" name="content[]" value="{{$item_folder->library->id}}/content"></td>
                                                          <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                                          <td class="text-center">{{$type}}</td>
                                                        </tr>
                                                        @endforeach
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                                @else
                                                <i class="fa fa-folder mr-l" aria-hidden="true"></i> {{$content->folder->group_name}}
                                                @endif

                                              </td>
                                              <td class="text-center">Folder</td>
                                            </tr>
                                          @endif
                                        @endforeach
                                        @foreach($item->library as $content)
                                          @if($content->content)
                                            <?php
                                              $data = dataContent($content->content);
                                              $icon = content_type_icon($data->type_content);
                                              $type = 'Quiz';
                                              if($data->type_content){
                                                $type = $data->type_content;
                                              }else if($data->type == "0"){
                                                $type = 'Tugas';
                                                $icon = "fa fa-tasks";
                                              }
                                            ?>
                                            <tr>
                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$content->content->id}}/content"></td>
                                              <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                              <td class="text-center">{{$type}}</td>
                                            </tr>
                                          @endif
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                  <button type="submit" class="btn btn-raised btn-primary">Impor</button>
                                </form>
                              </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="addLibrariesContent" tabindex="-1" role="dialog" aria-labelledby="addLibrariesContentLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="panel panel-default">
          <div class="panel-body">
            <h3>Tambah Konten / Kuis</h3>
            <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'Label')"><i class="fa fa-tags"></i> Label</button>
              <button class="tablinks" onclick="openCity(event, 'Video')" id="defaultOpen"><i class="fa fa-video-camera"></i> Konten Video</button>
              <button class="tablinks" onclick="openCity(event, 'Teks')"><i class="fa fa-text-height"></i> Konten Teks</button>
              <button class="tablinks" onclick="openCity(event, 'File')"><i class="fa fa-file-o"></i> Konten File</button>
              <button class="tablinks" onclick="openCity(event, 'Kuis')"><i class="fa fa-question"></i> Kuis</button>
              <button class="tablinks" onclick="openCity(event, 'Tugas')"><i class="fa fa-tasks"></i> Tugas</button>
              <button class="tablinks" onclick="openCity(event, 'Library')"><i class="fa fa-archive"></i> Library</button>
            </div>

            <div id="Label" class="tabcontent">
              <h3>Tambah Label</h3>
              <p>
                Tambahkan label untuk informasi konten lebih jelas
              </p>

              <a href="/library/course/content/create/?content=label" class="btn btn-primary btn-raised">Tambah Label</a>

              <br>
            </div>

            <div id="Video" class="tabcontent">
              <h3>Tambah Konten Video</h3>
              <p>
                Tambahkan konten video agar proses belajar menjadi lebih interaktif
              </p>
              <a href="/library/course/content/create/?content=video" class="btn btn-primary btn-raised">Upload Video</a>
              <a href="/library/course/content/create/?content=url&urlType=video" class="btn btn-primary btn-raised">Tambah URL Youtube</a>

              <br>
            </div>

            <div id="Teks" class="tabcontent">
              <h3>Tambah Konten Teks</h3>
              <p>
                Anda bisa menambahkan konten dengan hanya teks saja. bisa digunakan seperti membuat pendahuluan dan lain-lain.
              </p>
              <a href="/library/course/content/create/?content=text" class="btn btn-primary btn-raised">Tambah Konten Teks</a>
              <br>
            </div>

            <div id="File" class="tabcontent">
              <h3>Tambah Konten File</h3>
              <p>
                Anda bisa menambahkan konten file / berkas. bisa digunakan seperti membuat file pdf, power point dan lain-lain.
              </p>
              <a href="/library/course/content/create/?content=file" class="btn btn-primary btn-raised">Tambah Konten File</a>
              <a href="/library/course/content/create/?content=url&urlType=file" class="btn btn-primary btn-raised">Tambah Dari URL</a>

              <br>
            </div>

            <div id="Kuis" class="tabcontent">
              <h3>Tambah Kuis</h3>
              <p>
                Tambahkan kuis untuk menguji peserta sejauhmana mereka belajar
              </p>
              <a href="/library/course/quiz/create" class="btn btn-primary btn-raised">Tambah Kuis</a>
              <br>
            </div>

            <div id="Tugas" class="tabcontent">
              <h3>Tambah Tugas</h3>
              <p>
                Anda dapat memberikan tugas kepada peserta untuk melakukan penilaian kualitas belajar siswa di kelas Anda.
              </p>
              <a href="/library/course/assignment/create/" class="btn btn-primary btn-raised">Tambah Tugas</a>
              <br>
            </div>

            <div id="Library" class="tabcontent">
              <h3>Import Library</h3>
              <p>
                Anda dapat melakukan import dari library anda.
              </p>
              <a href="#" data-toggle="modal" data-target="#importLibrary" class="btn btn-primary btn-raised">Import Library</a>
              <br>
            </div>
            <br><br><br><br> &nbsp;
            {{-- <a href="#" class="btn btn-default btn-raised">Batal</a> --}}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="importLibrary" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="nav-tabs-ver-container">
            <div class="row">
              <div class="col-lg-2" style="flex: 0 0 12.666667%; max-width: 12.666667%;">
                <ul class="nav nav-tabs-ver nav-tabs-ver-primary" role="tablist" style="height:100%">
                  <li class="nav-item">
                    <a class="nav-link active" style="text-align:center" href="#library-tab" aria-controls="home3" role="tab" data-toggle="tab">
                      <i class="fa fa-building" style="margin:0"></i><br>Library</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#group-tab" style="text-align:center"  aria-controls="profile3" role="tab" data-toggle="tab">
                      <i class="fa fa-users" style="margin:0"></i><br>Group</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-10 nav-tabs-ver-container-content" style="flex: 0 0 87.333333%;max-width: 87.333333%;padding-right: 0px;">
                <div class="card-block">
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="library-tab">
                      <div class="nav-tabs-ver-container">
                        <div class="row">
                          <div class="col-lg-3" style="flex: 0 0 30%; max-width: 30%; padding-left:0">
                            <ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%">
                              <li class="nav-item">
                                <a class="nav-link active" href="#my-library-tab" aria-controls="my-library-tab" role="tab" data-toggle="tab">
                                  <i class="fa fa-building"></i> My Library</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#shared-library-tab" aria-controls="shared-library-tab" role="tab" data-toggle="tab">
                                  <i class="fa fa-share-alt"></i> Shared Library</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#public-library-tab" aria-controls="profile3" role="tab" data-toggle="tab">
                                  <i class="fa fa-users"></i> Public Library</a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-9 nav-tabs-ver-container-content" style="flex: 0 0 70%; max-width: 70%;">
                            <div class="card-block">
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="my-library-tab">
                                  <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="table-responsive table-cs">
                    									<table id="example-1" class="display" style="width:100%">
                  											<thead>
                  													<tr>
                                                <th></th>
                  															<th>Nama</th>
                  															<th class="text-center">Tipe</th>
                  													</tr>
                  											</thead>
                  											<tbody>
                                          @foreach($folder->where('user_id', Auth::user()->id) as $item)
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                            <td>
                                              <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{$item->id}}">
                                                <i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}
                                              </a>
                                              <div class="collapse" id="collapseExample-{{$item->id}}">
                                                <div class="card card-body">
                                                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                                </div>
                                              </div>
                                            </td>
                                            <td class="text-center">Folder</td>
                                          </tr>
                                          @endforeach
                                          @foreach($myLibrary[0]->library as $indexItem => $item)
                                          <?php
                                              $data = dataContent($item);
                                              $icon = content_type_icon($data->type_content);
                                              $type = 'Quiz';
                                              if($data->type_content){
                                                $type = $data->type_content;
                                              }else if($data->type == "0"){
                                                $type = 'Tugas';
                                                $icon = "fa fa-tasks";
                                              }
                                           ?>
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                            <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                            <td class="text-center">{{$type}}</td>
                                          </tr>
                                          @endforeach
                  											</tbody>
                    									</table>
                    								</div>

                                    <div class="modal-footer" style="padding-right: 0;">
                                      <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                    </div>
                                  </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="shared-library-tab">
                                  <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="table-responsive table-cs">
                                      <table id="example-2" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Nama</th>
                                                <th class="text-center">Tipe</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($folder->where('user_id', '!=', Auth::user()->id) as $item)
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                            <td><i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}</td>
                                            <td class="text-center">Folder</td>
                                          </tr>
                                          @endforeach
                                          @foreach($myLibrary[2]->library as $item)
                                          <?php
                                              $data = dataContent($item);
                                              $icon = content_type_icon($data->type_content);
                                              $type = 'Quiz';
                                              if($data->type_content){
                                                $type = $data->type_content;
                                              }else if($data->type == "0"){
                                                $type = 'Tugas';
                                                $icon = "fa fa-tasks";
                                              }
                                           ?>
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                            <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                            <td class="text-center">{{$type}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>

                                    <div class="modal-footer" style="padding-right: 0;">
                                      <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                    </div>
                                  </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="public-library-tab">
                                  <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="table-responsive table-cs">
                                      <table id="example-3" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Nama</th>
                                                <th class="text-center">Tipe</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($folder->where('status', '1') as $item)
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                            <td><i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$item->group_name}}</td>
                                            <td class="text-center">Folder</td>
                                          </tr>
                                          @endforeach

                                          @foreach($publicLibrary->library as $item)
                                          <?php
                                              $data = dataContent($item);
                                              $icon = content_type_icon($data->type_content);
                                              $type = 'Quiz';
                                              if($data->type_content){
                                                $type = $data->type_content;
                                              }else if($data->type == "0"){
                                                $type = 'Tugas';
                                                $icon = "fa fa-tasks";
                                              }
                                           ?>
                                          <tr>
                                            <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/content"></td>
                                            <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                            <td class="text-center">{{$type}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>

                                    <div class="modal-footer" style="padding-right: 0;">
                                      <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="group-tab">
                      <div class="nav-tabs-ver-container">
                        <div class="row">
                          <div class="col-lg-3" style="    flex: 0 0 30%;max-width: 30%;padding-left:0">
                            <ul class="nav nav-tabs-ver nav-tabs-ver" role="tablist" style="height:100%">
                              @foreach($group as $index => $item)
                              <li class="nav-item">
                                <a class="nav-link {{$index == 0 ? 'active' : ''}}" href="#my-library-tab-{{$item->group_code}}" aria-controls="my-library-tab-{{$item->group_code}}" role="tab" data-toggle="tab">
                                  <i class="fa fa-building"></i> {{$item->title}}</a>
                              </li>
                              @endforeach
                            </ul>
                          </div>
                          <div class="col-lg-9 nav-tabs-ver-container-content" style="    flex: 0 0 70%;max-width: 70%;">
                            <div class="card-block">
                              <div class="tab-content">
                                @foreach($group as $index => $item)
                                  <div role="tabpanel" class="tab-pane {{$index == 0 ? 'active' : ''}}" id="my-library-tab-{{$item->group_code}}">
                                    <form action="{{url('instructor/groups/detail/'.$id.'/add/resource')}}" method="post">
                                      {{csrf_field()}}
                                      <div class="table-responsive table-cs">
                      									<table id="example-group-{{$item->group_code}}" class="display" style="width:100%">
                    											<thead>
                    													<tr>
                                                  <th></th>
                    															<th>Nama</th>
                    															<th class="text-center">Tipe</th>
                    													</tr>
                    											</thead>
                    											<tbody>
                                            @foreach($item->library as $content)
                                            @if($content->folder)
                                            <tr>
                                              <td class="text-center"><input type="checkbox" name="content[]" value="{{$item->id}}/folder"></td>
                                              <td><i class="fa fa-folder-open mr-l" aria-hidden="true"></i> {{$content->folder->group_name}}</td>
                                              <td class="text-center">Folder</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                            @foreach($item->library as $content)
                                            @if($content->content)
                                            <?php
                                                $data = dataContent($content->content);
                                                $icon = content_type_icon($data->type_content);
                                                $type = 'Quiz';
                                                if($data->type_content){
                                                  $type = $data->type_content;
                                                }else if($data->type == "0"){
                                                  $type = 'Tugas';
                                                  $icon = "fa fa-tasks";
                                                }
                                             ?>
                                             <tr>
                                               <td class="text-center"><input type="checkbox" name="content[]" value="{{$content->content->id}}/content"></td>
                                               <td><i class="{{$icon}} mr-l"></i> {{$data->title ? $data->title : $data->name}}</td>
                                               <td class="text-center">{{$type}}</td>
                                             </tr>
                                             @endif
                                            @endforeach
                    											</tbody>
                      									</table>
                      								</div>

                                      <div class="modal-footer" style="padding-right: 0;">
                                        <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-raised btn-primary">Tambah</button>
                                      </div>
                                    </form>
                                  </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
  <script src="{{asset('uploadify-master/sample/jquery.uploadifive.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
		<?php $timestamp = time();?>
		$(function() {
      var token = '{{ csrf_token() }}';
			$('#file_upload').uploadifive({
				'auto'             : true,
				'formData'         : {
									   'timestamp' : '<?php echo $timestamp;?>',
									   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
                     '_token': token
				                     },
       'method'            : 'POST',
				'queueID'          : 'queue',
				'uploadScript'     : '{{route("uploadify", "update")}}',
        'onUploadComplete' : function(file, data) { console.log(data); },
				'onCancel' : function(file) {
          var token = '{{ csrf_token() }}';
          $.ajax({
            url : "{{route('uploadify.cancel', 'update')}}",
            type : 'POST',
            data : {
              'data' : file.originName,
              '_token' : '{{csrf_token()}}'
            },
            success : function(result){
              console.log(result);
            }
          });
         },
       'buttonText' : `<img src="{{asset('img/folder.svg')}}" class="img-file"> <span class="add-file">{{Lang::get('front.instructor_group.action_add_file')}}</span>`
			});
		});

	</script>
  <style media="screen">
    .img-file{
      width: 20px;
    }

    .add-file{
      margin-left: 2px;
    }

    .uploadifive-button {
        float: left;
        margin-right: 10px;
        background: none;
        color: black;
        border: none;
        right: 12px;
        cursor: all-scroll;
    }

    .uploadifive-button {
      float: left;
      margin-right: 10px;
    }

    #queue {
        overflow: auto;
        padding: 0 3px 3px;
        width: 100%;
    }

    .uploadifive-button:hover {
        background: none !important;
    }
  </style>
  <script type="text/javascript">
  $(".komentar-first").click(function(){
    $(".komentar-second").removeClass('hidden');
    $(".komentar-first").addClass('hidden');
  });
  $(".text-reply").hide();
  $(".reply-btn").click(function(){
    $(".text-reply").hide();
    $(this).siblings(".text-reply").show();

  });
  </script>
  <style media="screen">
  .komentar-first{
    border: 1px solid #d3cfcf;
    height: 70px;
    margin-top: 10px;
    cursor: pointer;
    padding: 10px 20px;
    color: #c7c7c7;
  }

  .hidden{
    display: none;
  }
  </style>
  <script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('answer', options);

    $('.discussion-reply').each(function(e){
        CKEDITOR.replace( this.id, options);
    });
  </script>

  <!-- The Templates plugin is included to render the upload/download listings -->
  <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
  <!-- blueimp Gallery script -->
  <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

  <script>
    $('#librariesOption').hide()
    $('#librariesCheckbox').click(function() {
      $('#librariesOption').toggle()
    })

    $('#videoUpload').hide()
    $('#youtubeUpload').hide()
    $('#videoUploadOption').click(function() {
      $('#videoUpload').show()
      $('#youtubeUpload').hide()
    })
    $('#youtubeUploadOption').click(function() {
      $('#videoUpload').hide()
      $('#youtubeUpload').show()
    })

    $('#fileUpload').hide()
    $('#externalUpload').hide()
    $('#fileUploadOption').click(function() {
      $('#fileUpload').show()
      $('#externalUpload').hide()
    })
    $('#externalUploadOption').click(function() {
      $('#fileUpload').hide()
      $('#externalUpload').show()
    })

  // trigger modal add konten / quiz
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }

    $('#example-1').DataTable({
      language: { search: '', searchPlaceholder: "Search ..." },
    });
    $('#example-2').DataTable({
      language: { search: '', searchPlaceholder: "Search ..." },
    });
    $('#example-3').DataTable({
      language: { search: '', searchPlaceholder: "Search ..." },
    });
    @foreach($group as $index => $item)
      $('#example-group-{{$item->group_code}}').DataTable({
        language: { search: '', searchPlaceholder: "Search ..." },
      });
    @endforeach

    $("#example-1_wrapper .dataTables_length").html("My Library");
    $("#example-2_wrapper .dataTables_length").html("Shared Library");
    $("#example-3_wrapper .dataTables_length").html("Public Library");
    @foreach($group as $index => $item)
      $("#example-group-{{$item->group_code}}_wrapper .dataTables_length").html("{{$item->title}}");
    @endforeach
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function deleteCourse(data){
      data.children('form').submit();
    }
  </script>
  <script src="{{url('ckeditor/ckeditor.js')}}"></script>
  <script src="{{url('js/bootstrap-datetimepicker.js')}}"></script>
  <script type="text/javascript">
		CKEDITOR.replace('descriptionVideo');
		CKEDITOR.replace('descriptionText');
		CKEDITOR.replace('descriptionFile');
		CKEDITOR.replace('descriptionQuiz');
    CKEDITOR.replace('descriptionAssignment');

    $('.timePicker').datetimepicker({
			language:  'id',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1
    });

    $("#datePicker").datepicker({
			orientation:"bottom left",
			autoclose:!0,
			todayHighlight:!0,
			format: 'yyyy-mm-dd',
		})
	</script>
  <style>
    .table-cs .dataTables_length{
      /* background: #4ca3d9;
      color: white !important;
      padding: 0px 10px;
      border-radius: 10px; */
      font-size: 20px;
    }

    .table-cs input{
      /* box-shadow: 0px 5px 18px -10px;
      border-radius: 10px;
      border: 1px solid #00000014;
      padding-left: 15px; */
      font-size: 14px;
      border: 1px solid #ccc;
      border-radius: 5px;
      box-shadow: none;
      padding: 0.75rem 1.5rem;
      margin: 0;
    }

    .table-cs thead>tr>th{
      border-bottom: none;
      padding: 8px 10px;
    }

    .table-cs tbody>tr>td{
      background: white !important;
    }

    .table-cs tbody>tr>td{
      background: white !important;
    }

    .table-cs .dataTables_paginate .paginate_button{
    }

    .table-cs .dataTables_paginate span a.paginate_button
    {
      /* padding: 0;
      background: #4cd137;
      background: none;
      border-radius: 50px;
      border:none; */
      border-radius: 3px;
      padding: 0.25rem 0.75rem;
    }

    .table-cs .dataTables_paginate span a.paginate_button.current{
      /* background: #4cd137;
      color:white !important; */
      border: 0px;
      background: #4ca3d9;
      color: #fff !important;
    }

    .table-cs thead tr th:first-child.sorting_asc:after, #example-1.dataTable thead tr th:first-child.sorting_desc:after, #example-1.dataTable thead tr th:first-child.sorting:after{
        display: none !important
    }

    .table-cs thead tr th:first-child, #example-1.dataTable thead tr th:first-child{
        width: 0.1px !important;
        padding: 0;
    }

    .table-cs .dataTables_info{
      display: none;
    }

    .dataTables_filter{
      /* margin-bottom: 15px; */
    }
  </style>
@endpush
