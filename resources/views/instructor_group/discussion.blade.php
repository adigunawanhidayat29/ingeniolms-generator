@extends('layouts.app')

@section('title', 'Judul Grup')

@push('style')
  <style>
    .nav-tabs-ver-container .nav-tabs-ver {
      padding: 0;
      margin: 0;
    }

    .nav-tabs-ver-container .nav-tabs-ver:after {
      display: none;
    }
  </style>

  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .hidden{
      display: none;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/home">@lang('front.instructor_group.home')</a></li>
          <li><a href="/instructor/groups">@lang('front.instructor_group.groups')</a></li>
          <li><a href="/instructor/groups/detail/{{$discussion->group_id}}">@lang('front.instructor_group.group_title')</a></li>
          <li>@lang('front.instructor_group.sample_discussion')</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <!-- <div class="col-md-3 nav-tabs-ver-container">
          <a class="btn btn-block btn-raised btn-default no-shadow mt-0 mb-2" href="/instructor/groups/detail"><i class="zmdi zmdi-arrow-left"></i> Kembali</a></li>
          <div class="card card-success-inverse no-shadow">
            <div class="card-block">
              <p class="fs-14 fw-600 mb-0">Access Code</p>
              <p class="fs-20 fw-300 text-uppercase" style="letter-spacing: 2px;">qwert-yuiop</p>
            </div>
          </div>
        </div> -->
        <div class="col-md-12">
          <a class="btn btn-block btn-raised btn-default no-shadow mt-0 mb-2" href="{{url('/instructor/groups/detail/'.$discussion->group_id)}}"><i class="zmdi zmdi-arrow-left"></i> @lang('front.instructor_group.back')</a></li>
          <div class="mb-2">
            <h3 class="headline headline-sm mt-0">{{$discussion->title}}</h3>
            <p class="fs-14" style="color: #999;">@lang('front.instructor_group.created_by') {{$discussion->user->name}}&nbsp;&middot;&nbsp;{{date('d M Y',strtotime($discussion->created_at))}}</p>
            <p class="fs-16 mb-0">{{$discussion->description}}</p>
          </div>

          <div class="card card-groups">
            <div class="card-block">
              <div class="addAnswer">
                <form action="" method="post">
                  {{csrf_field()}}
                  <div class="form-group mt-0">
                    <label class="control-label">@lang('front.instructor_group.write_comment')</label>
                    <textarea name="body" cols="30" rows="1" class="form-control"></textarea>
                  </div>
                  <button type="submit" class="btn btn-raised btn-primary">@lang('front.instructor_group.send')</button>
                </form>
              </div>
              <div class="editAnswer hidden">
                <form method="post" id="form-edit">
                  {{csrf_field()}}
                  <div class="form-group mt-0">
                    <label class="control-label">@lang('front.instructor_group.write_comment')</label>
                    <textarea name="body" cols="30" rows="1" class="form-control" id="body-edit"></textarea>
                  </div>
                  <button type="submit" class="btn btn-raised btn-success">@lang('front.instructor_group.edit')</button>
                  <button type="button" class="btn btn-raised btn-danger" id="btn-cancel-edit">@lang('front.instructor_group.cancel')</button>
                </form>
              </div>
            </div>
          </div>

          @foreach($discussion_list as $item)
          <div class="card card-groups dropdown">
            <div class="groups-dropdown">
              <div class="groups-dropdown-group">
                <i class="fa fa-ellipsis-v"></i>
                <ul class="groups-dropdown-menu">
                  <li>
                    <a class="groups-dropdown-item" href="#" onclick="editAnswer({{$item->id}},'{{$item->body}}')">
                      <i class="fa fa-pencil"></i> @lang('front.instructor_group.edit')
                    </a>
                  </li>
                  <li>
                    <a class="groups-dropdown-item" href="#" data-toggle="modal" data-target="#deleteDiscussionDetailModal-{{$item->id}}">
                      <i class="fa fa-trash"></i> @lang('front.instructor_group.delete')
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-block">
              <a class="groups-title" href="#">{{$item->user->name}}</a>
              <p class="fs-14" style="color: #999;">{{date('d M Y H:m:i', strtotime($item->created_at))}}&nbsp;&middot;&nbsp;</p>
              <p class="groups-description">{{$item->body}}</p>
            </div>
          </div>
          <div class="modal" id="deleteDiscussionDetailModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="d-flex align-items-center justify-content-between mb-2">
                    <h3 class="headline headline-sm m-0">@lang('front.instructor_group.delete_comment')</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">
                        <i class="zmdi zmdi-close"></i>
                      </span>
                    </button>
                  </div>
                  <form action="{{url('instructor/groups/discussion/detail/delete/'.$item->id)}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group text-center">
                      <p>@lang('front.instructor_group.delete_comment_alert')</p>
                      <p style="color:red"><i>@lang('front.instructor_group.comment_alert_description')</i></p>
                      <button type="button" class="btn btn-primary btn-raised m-0" data-dismiss="modal">@lang('front.instructor_group.cancel')</button>
                      <button type="submit" class="btn btn-danger btn-raised m-0">@lang('front.instructor_group.delete')</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="createDiscussion" tabindex="-1" role="dialog" aria-labelledby="createDiscussionLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <form action="#">
            <div class="form-group">
              <label class="control-label">@lang('front.instructor_group.discussion_title')</label>
              <input type="text" class="form-control" name="" placeholder="@lang('front.instructor_group.add_title_placeholder')">
            </div>
            <div class="form-group">
              <label class="control-label">@lang('front.instructor_group.create_description')</label>
              <textarea name="" id="" cols="30" rows="3" class="form-control"></textarea>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">@lang('front.instructor_group.close')</button>
          <button type="button" class="btn btn-raised btn-primary">@lang('front.instructor_group.discussion_add')</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
<script type="text/javascript">
  function editAnswer(id, desc){
    $(".addAnswer").addClass('hidden');
    $(".editAnswer").removeClass('hidden');
    $("#body-edit").val(desc)
    $("#form-edit").attr('action', "{{url('instructor/groups/discussion/detail/update/')}}/"+id);
  }

  $("#btn-cancel-edit").click(function(){
    $(".addAnswer").removeClass('hidden');
    $(".editAnswer").addClass('hidden');
    $("#body-edit").val(null)
    $("#form-edit").attr('action', null);
  })
</script>
@endpush
