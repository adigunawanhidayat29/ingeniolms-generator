@extends('layouts.app')

@section('title', Lang::get('front.instructor_group.title'))

@push('style')
  <style>
    .card.card-groups {
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.instructor_group.title')</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.instructor_group.breadcumb_home')</a></li>
          <li>@lang('front.instructor_group.breadcumb_group')</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-light-inverse">
            <div class="card-block">
              <a class="btn btn-primary btn-raised" href="/instructor/groups/create">@lang('front.instructor_group.btn_create_group')</a>
              <a class="btn btn-default btn-raised" href="#" data-toggle="modal" data-target="#joinGroup">@lang('front.instructor_group.btn_join_group')</a>
            </div>
          </div>

          @foreach ($instructor_groups as $key => $instructor_group)
            <div class="card card-groups dropdown">
              <div class="groups-dropdown">
                <div class="groups-dropdown-group">
                  <i class="fa fa-ellipsis-v"></i>
                  <ul class="groups-dropdown-menu">
                    <li>
                      <a class="groups-dropdown-item" href="/instructor/groups/edit/{{$instructor_group->id}}">
                        <i class="fa fa-pencil"></i> @lang('front.general.text_edit')
                      </a>
                    </li>
                    <li>
                      <a class="groups-dropdown-item" href="#">
                        <i class="fa fa-trash"></i> @lang('front.general.text_delete')
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="card-block">
                <a class="groups-title" href="/instructor/groups/detail/{{$instructor_group->id}}">{{$instructor_group->title}}</a>
                <p class="groups-description">{{$instructor_group->description}}</p>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="joinGroup" tabindex="-1" role="dialog" aria-labelledby="joinGroupLabel">
    <form action="{{url('instructor/group/join')}}" method="post">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="modal-body">
              <div class="form-group">
                <label class="control-label">@lang('front.instructor_group.join_group_with_access_code')</label>
                <input type="text" class="form-control" name="group_code" placeholder="@lang('front.instructor_group.input_access_code')">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">@lang('front.general.text_close')</button>
            <button type="submit" class="btn btn-raised btn-primary">@lang('front.general.text_join')</button>
          </div>
        </div>
      </div>
    </form>
  </div>
@endsection
