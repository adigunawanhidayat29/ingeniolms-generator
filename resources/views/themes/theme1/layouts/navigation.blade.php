<!--Header Wrap Start-->
    <header class="gt_hdr2_wrap">
      <div class="gt_top2_wrap default_width">
        <div class="container">
          <div class="gt_top_element">
            <ul>
              <li><a href="#">Student</a></li>
              <li><a href="#">Faculty  Staff</a></li>
              <li><a href="#">Parents</a></li>
              <li><a href="#">Allmni</a></li>
            </ul>
          </div>
          <div class="gt_hdr_2_ui_element">
            <ul>
              <li><i class="icon-lock"></i><a data-target="#gt-login" href="remote.html" data-toggle="modal" href="#">Login &#38; Register</a></li>
              <li>
                  <div class="gt-user-login">
                      <div class="gt-media">
                          <figure><img alt="" src="/themes/theme1/assets/extra-images/popular-course-icon.png"></figure>
                      </div>
                      <a href="#">Alard William</a>
                      <ul>
                          <li><a href="#"><i class="fa fa-user"></i> About me</a></li>
                          <li><a href="#"><i class="fa fa-graduation-cap"></i> My Courses</a></li>
                          <li><a href="#"><i class="fa fa-heart"></i> Favorites</a></li>
                          <li><a href="#"><i class="fa fa-file"></i> Statement</a></li>
                          <li class="active"><a href="#"><i class="fa fa-gears"></i> Profile Setting</a></li>
                          <li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
                      </ul>
                  </div>
              </li>
            </ul>
          </div>
          <div class="gt_login_element">
            <p>Apply Program &#38; Degress  -   Find Funding</p>
          </div>
        </div>
      </div>
      <div class="gt_menu_bg default_width">
        <div class="container">
          <!--Logo Wrap Start-->
          <div class="gt_logo gt_logo_padding">
            <a href="/"><img height="70px" src="{!! isset(Setting('icon')->value) ? Setting('icon')->value : '/img/ingenio-logo.png' !!}" alt="{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}"></a>
          </div>
          <!--Logo Wrap End-->
          <!--Search Wrap Start-->
          <div class="gt_search_wrap">
            <span class="search-fld"><i class="fa fa-search"></i></span>
              <div class="search-wrapper-area">
                  <form class="search-area">
                      <input type="text" placeholder="Search Here"/>
                      <input type="submit" value="Go"/>
                  </form>
                  <span class="gt_search_remove_btn"><i class="fa fa-remove"></i></span>
              </div>
          </div>
          <!--Search Wrap End-->
          <!--Navigation Wrap Start-->
          <nav class="gt_navigation2">
            <button class="gt_mobile_menu">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <ul>
              <li class="active"><a href="javascript:avoid(0);">Home</a></li>
            </ul>
          </nav>
          <!--Navigation Wrap End-->
        </div>
      </div>
    </header>
    <!--Header Wrap End-->