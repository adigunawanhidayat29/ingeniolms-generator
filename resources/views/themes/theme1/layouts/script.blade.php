
<!--Jquery Library-->
<script src="/themes/theme1/assets/js/jquery.js"></script>
<!--Bootstrap core JavaScript-->
<script src="/themes/theme1/assets/js/bootstrap.min.js"></script>
<!--Flex Slider JavaScript-->
<script src="/themes/theme1/assets/js/jquery.flexslider-min.js"></script>
<!--Swiper Slider JavaScript-->
<script src="/themes/theme1/assets/js/swiper.jquery.min.js"></script>
<!--Owl Carousel JavaScript-->
<script src="/themes/theme1/assets/js/owl.carousel.js"></script>
<!--Chosen JavaScript-->
<script src="/themes/theme1/assets/js/chosen.jquery.min.js"></script>
<!--Chosen JavaScript-->
<script src="/themes/theme1/assets/js/waypoints-min.js"></script>
<!--Pretty Photo Javascript-->
<script src="/themes/theme1/assets/js/jquery.prettyPhoto.js"></script>
<!--Custom JavaScript-->
<script src="/themes/theme1/assets/js/custom.js"></script>

@stack('script')
