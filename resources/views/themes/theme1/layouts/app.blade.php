<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>@yield('title') - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url_base" content="{{ url('') }}">
    <meta name="host_id" content="{{empty(Auth::user()) ? '' : Auth::user()->id}}">
    <meta name="app-parents" content=".">
    @stack('meta')
    {{-- @include('layouts.header') --}}

    <!-- Animation CSS -->
    <link href="/themes/theme1/assets/css/animate.css" rel="stylesheet">
    <!-- Chosen CSS -->
    <link href="/themes/theme1/assets/css/chosen.min.css" rel="stylesheet">
    <!-- Chosen CSS -->
    <link href="/themes/theme1/assets/css/login-register.css" rel="stylesheet">
    <!-- Swiper Slider CSS -->
    <link href="/themes/theme1/assets/css/flexslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="/themes/theme1/assets/css/prettyPhoto.css" rel="stylesheet">
    <!-- Swiper Slider CSS -->
    <link href="/themes/theme1/assets/css/swiper.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="/themes/theme1/assets/css/style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="/themes/theme1/assets/css/color-2.css" rel="stylesheet">
    <!-- Typography StyleSheet CSS -->
    <link href="/themes/theme1/assets/css/typography-02.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="/themes/theme1/assets/css/responsive.css" rel="stylesheet">

    <style media="screen">
      .gt_exp_hvr_des{position: absolute;bottom: -100px;left: 0px;width: 100%;height: auto;background-color: #ffffff;text-align: center;padding: 52px 20px 20px;opacity: 0;visibility: visible;}
      .gt_exp_hvr_des,.gt_expl_course_wrap:hover .gt_exp_hvr_des{opacity: 1;bottom: 0px;visibility: visible;}
    </style>
    
  </head>
  <body>

    <div class="gt_wrapper">
      @include('themes/theme1/layouts.navigation')
      @yield('content')
      @include('themes/theme1/layouts.footer')
      @include('themes/theme1/layouts.script')
    </div>
    

    {{-- @include('layouts.sidebar') --}}

  </body>
</html>
