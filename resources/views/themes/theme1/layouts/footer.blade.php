<!--Footer Wrap Start-->
    <footer>
      <!--Newletter Wrap Start-->
      <div class="gt_newsletter02_bg default_width">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="gt_newslettr_hdg default_width">
                <i class="icon-tool"></i>
                <h6>Our</h6>
                <h3>Newsletter</h3>
              </div>
            </div>
            <div class="col-md-8">
              <div class="gt_newsletter_form default_width">
                <form>
                  <input type="text" class="p_sub" placeholder="Enter Your E-Mail Address">
                  <button><i class="fa fa-send"></i></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Newletter Wrap End-->

      <!--Footer List Wrap Start-->
      <div class="gt_footer2_bg default_width">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="gt_foo_logo_wrap default_width">
                <a href="#"><img src="/themes/theme1/assets/images/footer-logo.png" alt=""></a>
                  <p>Lorem ipsum dolor sit amet, consectet ur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                  <span><a href="#">Read More</a></span>
                  <ul class="gt_scl_icon">
                    <li><a class="bg_fb" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="bg_twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="bg_gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="bg_linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="bg_behance" href="#"><i class="fa fa-behance"></i></a></li>
                  </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="gt_foo2_link gt_widget2_hdg">
                  <h5>Quick links</h5>
                  <ul>
                      <li><a href="#">Home</a></li>
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Latest Blog</a></li>
                      <li><a href="#">Courses</a></li>
                      <li><a href="#">Admmision</a></li>
                      <li><a href="#">Event</a></li>
                      <li><a href="#">Contact Us</a></li>
                      <li><a href="#">Become a Teacher</a></li>
                      <li><a href="#">Support Forums</a></li>
                  </ul>
                  <ul>
                      <li><a href="#">Support Forums</a></li>
                      <li><a href="#">FAQ &#38; Help Center</a></li>
                      <li><a href="#">Register an account</a></li>
                      <li><a href="#">Login with account</a></li>
                      <li><a href="#">Careers</a></li>
                  </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="gt_widget2_hdg">
                <h5>Contact Us</h5>
                <p>Lorem ipsum dolor sit amet, consectet ur adipiscing elit,</p>
                <ul class="gt_team1_contact_info">
                  <li><i class="fa fa-map-marker"></i>14350 60th St North Clearwater FL. 33760 </li>
                  <li><i class="fa fa-phone"></i>1-677-124-44227 </li>
                  <li><i class="fa fa-envelope"></i> <a href="#">info@kidscenter.com</a> </li>
                  <li><i class="fa fa-phone"></i>Mon- Fri 08:00 am to 05:00 pm</li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!--Footer List Wrap End-->

      <!--Copyright Wrap End-->
      <div class="gt_copyright_wrap copyright2_bg">
        <div class="container">
            <div class="gt_copyright_des">
                <p>Copyright © <a href="#">Edu-skill </a> 2016. All rights reserved.</p>
                <span>Created by: <a href="#">2goodtheme.com</a></span>
              </div>
          </div>
      </div>
      <!--Copyright Wrap Start-->
    </footer>
    <!--Footer Wrap End-->

    <!--Back to Top Wrap Start-->
    <div class="back-to-top">
      <a href="#home"><i class="fa fa-angle-up"></i></a>
    </div>
    <!--Back to Top Wrap Start-->