@extends('themes.theme1.layouts.app')

@section('title', 'Beranda')

@section('content')
  <!--gt Wrapper Start-->

    <!--Banner Wrap Start-->
    <div class="gt_banner default_width">
      <div class="swiper-container" id="swiper-container">
         <ul class="swiper-wrapper">
            @foreach($sliders as $index => $data)
              <li class="swiper-slide">
                  <img src="{{$data->image}}" alt="">
                  <div class="gt_banner_text gt_slide_1">
                    <h3>What would you like to learn</h3>
                    <h2>Complete Education Solution</h2>
                    <p>Aenean commodo ligal geate dolor. Aenan massa. Loren ipsum dolor sit amet,color dolor sit amet</p>
                    <a href="#">see More<i class="fa fa-arrow-right"></i></a>
                  </div>
              </li>
            @endforeach
         </ul>
       </div>
      <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
      <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>
    </div>
    <!--Banner Wrap End-->

    <!--Main Content Wrap Start-->
    <div class="gt_content_wrap">
      <!--Search Bar Wrap Start-->
      <div class="container">
        <div class="gt_search02_wrap default_width">
          <div class="col-md-7 col-sm-6 no_padding">
            <div class="gt_cat_search">
               <input class="ph_news" type="text" placeholder="Search Courses">
             </div>
          </div>
          <div class="col-md-3 col-sm-4 no_padding">
            <div class="gt_cat_search">
               <select class="find_course_search">
                 <option>All Categories</option>
                 <option>2</option>
                 <option>3</option>
                 <option>4</option>
               </select>
             </div>
          </div>
          <div class="col-md-2 col-sm-2 no_padding">
            <div class="gt_cat_search">
               <input class="ph_news" type="submit" value="Search Now">
             </div>
          </div>
        </div>
      </div>
      <!--Search Bar Wrap End-->

      <!--Services Wrap Start-->
      <section>
        <div class="container">

          <div class="row">
            <div class="col-md-6">
              <div class="get_started_video">
                  <img src="/themes/theme1/assets/extra-images/welcome-eduskill.png" alt="">
                  <div class="get_video_icon">
                      <a data-rel="prettyPhoto" href="https://www.youtube.com/watch?v=Z_Qb-BOAVac"><i class="fa fa-play"></i></a>
                      <span>Watch The Video</span>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="get_started_content_wrap ct_blog_detail_des_list">
                  <h3>Description of University</h3>
                  <p>We are home to 1,500 students (aged 12 to 16) and 100 expert faculty and staff community representing over 40 different nations. We are proud of our international and multi-cultural ethos, the way our community collaborates to make a difference. Our world-renowned curriculum is built on the best.</p>
                  <p>Global and US standards.We are home to 1,500 students (aged 12 to 16) and 100 expert faculty and staff.Community representing over 40 different nations. We are proud of our international.</p>
                  <ul>
                    <li>We are home to 1,500 students and 100 experts.</li>
                      <li>Community representing over 40 of different nations.</li>
                      <li>We are proud of our international.</li>
                      <li>We are home to 1,500.</li>
                      <li>Community representing over 40 different nations.</li>
                  </ul>
              </div>
            </div>
          </div>

        </div>
      </section>
      <!--Services Wrap End-->

      <!--Why Choose Us 02 Wrap Start-->
      <section class="gt_choose02_wrap">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="gt_choose2_slider_wrap flexslider" id="gt_choose2_slider_wrap">
                <ul class="slides">
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-01.jpg" alt="">
                    <div class="get_video_icon">
                      <a data-rel="prettyPhoto" href="https://www.youtube.com/watch?v=Z_Qb-BOAVac"><i class="fa fa-play"></i></a>
                      <span>Watch The Video</span>
                    </div>
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-02.jpg" alt="">
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-03.jpg" alt="">
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-04.jpg" alt="">
                  </li>
                </ul>
              </div>
              <div class="gt_choose_slider2_thumb flexslider" id="gt_choose2_slider_thumb">
                <ul class="slides">
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-01.jpg" alt="">
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-02.jpg" alt="">
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-03.jpg" alt="">
                  </li>
                  <li>
                    <img src="/themes/theme1/assets/extra-images/choose-04.jpg" alt="">
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-md-6">
              <!--Heading 02 Wrap Start-->
              <div class="gt_hdg_1 default_width align_left">
                <h3>WHY CHOOSE US?</h3>
                <p>Aenean commodo ligal geate dolor. Aenan massa. Loren ipsum dolor sit amet,color<br>tetuer adiois elit, aliquam eget nibh etibra</p>
                <span class="gt_hdg02_span"></span>
              </div>
              <!--Heading 02 Wrap End-->
              <div class="gt_choose2_list_wrap default_width">
                <ul>
                  <li>
                    <div class="gt_choose2_icon">
                      <i class="icon-medal"></i>
                    </div>
                    <div class="gt_chooose_2_list_des">
                      <h6><a href="#">Quality Education</a></h6>
                      <p>Diet and health, human osteology,paleopathology  epidemiology, human evolution, disease ecology human adaptation, </p>
                    </div>
                  </li>
                  <li>
                    <div class="gt_choose2_icon">
                      <i class="icon-teacher-pointing-blackboard"></i>
                    </div>
                    <div class="gt_chooose_2_list_des">
                      <h6><a href="#">Expert  Teacher</a></h6>
                      <p>Diet and health, human osteology,paleopathology  epidemiology, human evolution, disease ecology human adaptation, </p>
                    </div>
                  </li>
                  <li>
                    <div class="gt_choose2_icon">
                      <i class="icon-open-book"></i>
                    </div>
                    <div class="gt_chooose_2_list_des">
                      <h6><a href="#">Book Library &#38; store</a></h6>
                      <p>Diet and health, human osteology,paleopathology  epidemiology, human evolution, disease ecology human adaptation, </p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Why Choose Us 02 Wrap End-->

      <!--Explore the Courses 02 Wrap Start-->
      <section class="gt_exp_course_bg">
        <div class="container">
          <!--Heading 02 Wrap Start-->
          <div class="gt_hdg_1 default_width">
            <h3>Explore all the COURSES</h3>
            <p>Aenean commodo ligal geate dolor. Aenan massa. Loren ipsum dolor sit amet,color<br>tetuer adiois elit, aliquam eget nibh etibra</p>
            <span class="gt_hdg02_span"></span>
          </div>
          <!--Heading 02 Wrap End-->

          <!--Explore Courses List Wrap Start-->
          <div class="row">
            @foreach($courses as $course)
              {{-- <div class="col-md-4 col-sm-6">
                <div class="gt_expl_course_wrap exp_course1 default_width">
                  <figure>
                    <img src="{{asset_url(course_image_medium($course->image))}}" alt="">
                    <figcaption class="gt_expl_course_tag exp_course1">Technology</figcaption>
                  </figure>
                  <div class="gt_exp_hvr_des">
                    <span><i class="icon-people"></i></span>
                    <h6>{{$course->title}}</h6>
                    <p>{!!str_limit($course->course_description, 100)!!}</p>
                    <div class="gt_exp_hvr_des_lnk">
                      <p><span>$65</span>Per Month</p>
                      <a href="{{url('course/'. $course->slug)}}">See More</a>
                    </div>
                  </div>
                </div>
              </div> --}}
              <div class="col-md-4 col-sm-6">
                <div class="default_width" style="border:1px solid #bbb;margin:10px 0px;">
                  <figure>
                    <img src="{{asset_url(course_image_small($course->image))}}" style="height:400px!important" alt="">
                    {{-- <figcaption class="gt_expl_course_tag exp_course1">Technology</figcaption> --}}
                  </figure>
                  <div class="gt_exp_hvr_des">
                    <h6>{{$course->title}}</h6>
                    <p>{!!str_limit($course->course_description, 100)!!}</p>
                    <div class="gt_exp_hvr_des_lnk">
                      <p>{{$course->price == 0 ? 'Gratis' : $course->price}}</p>
                      <a href="{{url('course/'. $course->slug)}}">See More</a>
                    </div>
                  </div>
                </div>
              </div>

            @endforeach
            
          </div>
          <!--Explore Courses List Wrap End-->

          <!--See More Button Wrap Start-->
          <div class="gt_see_more_btn default_width">
            <a href="/courses">See More <i class="fa fa-arrow-circle-right"></i></a>
          </div>
          <!--See More Button Wrap End-->

        </div>
      </section>
      <!--Explore the Courses 02 Wrap End-->

      <!--Testimonial 02 Wrap Start-->
      <section class="gt_testimonial02_bg">
        <div class="container">
          <!--Heading 02 Wrap Start-->
          <div class="gt_hdg_1 default_width white_color">
            <h3>What Student say about us</h3>
            <p>Aenean commodo ligal geate dolor. Aenan massa. Loren ipsum dolor sit amet,color<br>tetuer adiois elit, aliquam eget nibh etibra</p>
            <span class="gt_white gt_hdg02_span"></span>
          </div>
          <!--Heading 02 Wrap End-->

          <!--Testimonial List Wrap Start-->
          <div class="gt_testimonial_slider" id="gt_testimonial_slider">
            <div class="item">
              <div class="gt_testimonial2_wrap default_width">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <ul class="gt_rating_star">
                  <li>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                  </li>
                </ul>
                <div class="gt_testimonial_img">
                  <figure>
                      <img src="/themes/theme1/assets/extra-images/testimonial-01.jpg" alt="">
                  </figure>
                  <div class="gt_testimonial_des">
                      <h5><a href="#">Jenishah Smith Deo</a></h5>
                      <span>Student at campus</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="gt_testimonial2_wrap default_width">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <ul class="gt_rating_star">
                  <li>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                  </li>
                </ul>
                <div class="gt_testimonial_img">
                  <figure>
                      <img src="/themes/theme1/assets/extra-images/testimonial-02.jpg" alt="">
                  </figure>
                  <div class="gt_testimonial_des">
                      <h5><a href="#">Jenishah Smith Deo</a></h5>
                      <span>Student at campus</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="gt_testimonial2_wrap default_width">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <ul class="gt_rating_star">
                  <li>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                  </li>
                </ul>
                <div class="gt_testimonial_img">
                  <figure>
                      <img src="/themes/theme1/assets/extra-images/testimonial-03.jpg" alt="">
                  </figure>
                  <div class="gt_testimonial_des">
                      <h5><a href="#">Jenishah Smith Deo</a></h5>
                      <span>Student at campus</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="gt_testimonial2_wrap default_width">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <ul class="gt_rating_star">
                  <li>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                  </li>
                </ul>
                <div class="gt_testimonial_img">
                  <figure>
                      <img src="/themes/theme1/assets/extra-images/testimonial-04.jpg" alt="">
                  </figure>
                  <div class="gt_testimonial_des">
                      <h5><a href="#">Jenishah Smith Deo</a></h5>
                      <span>Student at campus</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="gt_testimonial2_wrap default_width">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <ul class="gt_rating_star">
                  <li>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                    <a href="#"><i class="fa fa-star"></i></a>
                  </li>
                </ul>
                <div class="gt_testimonial_img">
                  <figure>
                      <img src="/themes/theme1/assets/extra-images/testimonial-01.jpg" alt="">
                  </figure>
                  <div class="gt_testimonial_des">
                      <h5><a href="#">Jenishah Smith Deo</a></h5>
                      <span>Student at campus</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--Testimonial List Wrap End-->
        </div>
      </section>
      <!--Testimonial 02 Wrap End-->

      <!--Latest News and Events Wrap Start-->
      <section>
        <div class="container">
          <!--Heading 02 Wrap Start-->
          <div class="gt_hdg_1 default_width">
            <h3>Latest News &#38; event</h3>
            <p>Aenean commodo ligal geate dolor. Aenan massa. Loren ipsum dolor sit amet,color<br>tetuer adiois elit, aliquam eget nibh etibra</p>
            <span class="gt_hdg02_span"></span>
          </div>
          <!--Heading 02 Wrap End-->

          <!-- Event List Wrap Start -->
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="gt_latest_news_wrap default_width">
                <figure>
                    <img src="/themes/theme1/assets/extra-images/courses-01.jpg" alt="">
                    <figcaption class="gt_news_figcaption">
                        <ul>
                            <li><a href="#"><i class="fa fa-search"></i></a></li>
                            <li><a href="assets/extra-images/gallery-01.jpg" data-rel="prettyPhoto"><i class="fa fa-expand"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <div class="gt_news_des_wrap default_width">
                    <ul class="gt_blog_meta">
                      <li><i class="fa fa-user"></i> By <a href="#">john Deo</a></li>
                      <li><i class="fa fa-calendar"></i>Dec 24 2016</li>
                      <li><i class="fa fa-comments"></i><a href="#">04</a></li>
                    </ul>
                    <h6><a href="#">Aenean commodo ligal geatedolor. Aenan</a></h6>
                    <p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsumnibh id elit. </p>
                    <span>+</span>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="gt_latest_news_wrap default_width">
                <figure>
                    <img src="/themes/theme1/assets/extra-images/courses-02.jpg" alt="">
                    <figcaption class="gt_news_figcaption">
                        <ul>
                            <li><a href="#"><i class="fa fa-search"></i></a></li>
                            <li><a href="assets/extra-images/gallery-01.jpg" data-rel="prettyPhoto"><i class="fa fa-expand"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <div class="gt_news_des_wrap default_width">
                    <ul class="gt_blog_meta">
                      <li><i class="fa fa-user"></i> By <a href="#">john Deo</a></li>
                      <li><i class="fa fa-calendar"></i>Dec 24 2016</li>
                      <li><i class="fa fa-comments"></i><a href="#">04</a></li>
                    </ul>
                    <h6><a href="#">Aenean commodo ligal geatedolor. Aenan</a></h6>
                    <p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsumnibh id elit. </p>
                    <span>+</span>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="gt_latest_news_wrap default_width">
                <figure>
                    <img src="/themes/theme1/assets/extra-images/courses-03.jpg" alt="">
                    <figcaption class="gt_news_figcaption">
                        <ul>
                            <li><a href="#"><i class="fa fa-search"></i></a></li>
                            <li><a href="assets/extra-images/gallery-01.jpg" data-rel="prettyPhoto"><i class="fa fa-expand"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <div class="gt_news_des_wrap default_width">
                    <ul class="gt_blog_meta">
                      <li><i class="fa fa-user"></i> By <a href="#">john Deo</a></li>
                      <li><i class="fa fa-calendar"></i>Dec 24 2016</li>
                      <li><i class="fa fa-comments"></i><a href="#">04</a></li>
                    </ul>
                    <h6><a href="#">Aenean commodo ligal geatedolor. Aenan</a></h6>
                    <p>Lorem Ipsum proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsumnibh id elit. </p>
                    <span>+</span>
                </div>
              </div>
            </div>
          </div>
          <!-- Event List Wrap End -->

        </div>
      </section>
      <!--Latest News and Events Wrap End-->
    </div>
    <!--Main Content Wrap End-->

  <!--gt Wrapper End-->

@endsection