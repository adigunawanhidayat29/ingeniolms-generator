<div id="vueAppFooter">
  <template>
    <footer class="ms-footer">
      <div class="container" v-if="!loading">
        <p>Copyright &copy; @{{settings.footer.value}}</p>
      </div>
    </footer>
    <div class="btn-back-top">
      <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
        <i class="zmdi zmdi-long-arrow-up"></i>
      </a>
    </div>
  
    <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
      <div class="sb-slidebar-container">
        <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
  
          <li class="card" role="tab" id="sch1">
            <a href="http://live.unpad.ac.id/a/">
              Beranda
            </a>
          </li>
          <li class="nav-item">
            <a href="http://live.unpad.ac.id/a/tentang-kami/">TENTANG KAMI</a>
          </li>
          <li class="nav-item">
            <a href="http://live.unpad.ac.id/a/pjj/">PJJ</a>
          </li>
          <li class="nav-item">
            <a href="http://live.unpad.ac.id/a/reguler/">Reguler</a>
          </li>
          <li class="nav-item">
            <a href="http://live.unpad.ac.id/portal2/berita/">BERITA</a>
          </li>
          <li class="nav-item">
            <a href="https://unpad.kuliahdaring.id/login">LOGIN PJJ</a>
          </li>
          <li class="nav-item">
            <a href="http://live.unpad.ac.id/login/login.php">LOGIN REGULER</a>
          </li>
          <li><a href="https://unpad.kuliahdaring.id/login"><i class="fa fa-arrow-right"></i>Login</a></li>
          <li><a href="https://unpad.kuliahdaring.id/register"><i class="fa fa-user"></i>Daftar</a></li>
        </ul>
      </div>
    </div>
  </template>
</div>

<script defer>
    const vueAppFooter = new Vue({
      el: '#vueAppFooter',
      data () {
        return {
          settings: {},
          loading: false
        }
      },
      mounted () {
          this.getSetting()
      },
      methods: {
        getSetting () {
          this.loading = true;
          axios({
            method: 'get',
            url: '/api/v3/settings',
            json: true
          }).then((response) => {
            // console.log(response.data.data)
            this.settings = response.data.data
            this.loading = false;
          }).catch((error) => {
              console.log(error);
          });
        }
      },
    })
</script>