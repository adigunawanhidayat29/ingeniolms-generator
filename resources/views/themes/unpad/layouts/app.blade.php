<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport"
    content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="theme-color" content="#333">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') - {{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</title>
  <meta name="description" content="Online Course">
  <meta name="csrf-token" content="dXlQdr5UvT6itJSApCr83ZfxpGizkUdcwKp919YL">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css"
    href="/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
  <!-- REVOLUTION STYLE SHEETS -->
  <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/settings.css">
  <!-- REVOLUTION LAYERS STYLES -->
  <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/layers.css">
  <!-- REVOLUTION NAVIGATION STYLES -->
  <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/navigation.css">
  <link rel="stylesheet" href="/css/preload.min.css">
  <link rel="stylesheet" href="/css/plugins.min.css">
  <!-- TYPEWRITER ADDON -->
  <script type="text/javascript" src="/plugins/revolution/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution-addons/typewriter/css/typewriter.css">
  <link rel="stylesheet" href="http://unpad.kuliahdaring.id/css/style.light-blue-500.min.css">
  <link rel="stylesheet" href="http://unpad.kuliahdaring.id/css/dq-style.css">
  <link rel="shotcut icon" href="{!! isset(Setting('icon')->value) ? Setting('icon')->value : '/img/ingenio-logo.png' !!}">
  <!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->

  <style>
    .dq-project-bg {
      background-image: url('http://unpad.kuliahdaring.id/assets/img/project-bg.jpg');
      background-size: cover;
      background-position: top center;
      background-repeat: no-repeat;
    }
  </style>


  <style>
    .dq-project-bg {
      background-image: url('http://unpad.kuliahdaring.id/img/project-bg.jpg');
      background-size: cover;
      background-position: top center;
      background-repeat: no-repeat;
    }

    .dq-play-video {
      width: 100%;
      background: transparent;
      -webkit-transition: 0.5s ease;
      -webkit-opacity: 0.6;
    }

    .dq-play-video:hover {
      background: #000;
      -webkit-opacity: 1;
    }

    .dq-play-video i {
      width: 50px;
      height: 50px;
      border: 3px solid #fff;
      border-radius: 100%;
      position: absolute;
      text-align: center;
      line-height: 45px;
      font-size: 20px;
      color: white;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      padding: inherit;
      background: #000;
      opacity: 0.7;
      box-shadow: 0 0 5px #333;
    }
  </style>

  <link rel="stylesheet" href="/bootstrap-tour/css/bootstrap-tour-standalone.min.css">
  <style>
    .dq-project-bg {
      background-image: url('http://unpad.kuliahdaring.id/img/project-bg.jpg');
      background-size: cover;
      background-position: top center;
      background-repeat: no-repeat;
    }

    .dq-degree-bg {
      background: url('http://unpad.kuliahdaring.id/img/unpad/perpustakaan1.jpg') no-repeat center;
      background-size: cover;
      width: 100%;
      height: 400px;
      position: relative;
      overflow: hidden;
    }

    .dq-overlay {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: rgba(0, 0, 0, 0.7);
    }

    @media screen and (max-width:768px) {
      .banner-responsive {
        width: 100%;
      }

      .img-responsive {
        display: block;
      }
    }
  </style>

  @stack('style')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
  <script src="https://vuejs.org/js/vue.min.js"></script>
</head>
<body>
  <div id="ms-preload" class="ms-preload">
    <div id="status">
      <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
      </div>
    </div>
  </div>

  <div class="ms-site-container">
    @include('themes/unpad/layouts.navigation')
    @yield('content')
    @include('themes/unpad/layouts.footer')
  </div>

  @include('themes/unpad/layouts.script')

  @stack('vue')

</body>
</html>