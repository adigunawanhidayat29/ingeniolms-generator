<div id="vueAppNav">
  <template>
    <!-- Modal -->
    <div class="modal modal-default" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
          <div class="modal-header d-block shadow-2dp no-pb">
            <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                <i class="zmdi zmdi-close"></i>
              </span>
            </button>
            <div class="modal-title text-center">
              <img :src="settings.icon.value" alt="{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}">
              <br><br>
            </div>
          </div>
          <div class="modal-body">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
                <h3>Silakan Masuk</h3>
                <form autocomplete="off" method="POST" action="https://unpad.kuliahdaring.id/login">
                  <input type="hidden" name="_token" value="dXlQdr5UvT6itJSApCr83ZfxpGizkUdcwKp919YL">
                  <fieldset>
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-email">Email</label>
                        <input type="email" id="ms-form-email" name="email" class="form-control" value="">
                      </div>
                    </div>
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock"></i>
                        </span>
                        <label class="control-label" for="ms-form-pass">Password</label>
                        <input type="password" name="password" id="ms-form-pass" class="form-control"> </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-6">
                        <div class="form-group no-mt">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" name="remember"> Remember Me </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <button type="submit" class="btn btn-raised btn-primary pull-right">Login</button>
                      </div>
                    </div>
                  </fieldset>
                </form>
                <div class="text-center">
                  <h3>Or Login with</h3>
                  <a href="http://unpad.kuliahdaring.id/auth/facebook" class="wave-effect-light btn btn-raised btn-facebook">
                    <i class="zmdi zmdi-facebook"></i> Facebook</a>
                  <a href="http://unpad.kuliahdaring.id/auth/google" class="wave-effect-light btn btn-raised btn-google">
                    <i class="zmdi zmdi-google"></i> Google</a>
                  <a href="http://unpad.kuliahdaring.id/auth/paus" class="wave-effect-light btn btn-raised btn-warning btn-block btn-lg">Paus ID</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <header class="ms-header ms-header-default">
      <!--ms-header-dark-->
      <div class="container container-full">
        <div class="ms-title">
          <a href="/">     
            <img style="height:60px" :src="settings.icon.value" :alt="settings.icon.title">
            <br>
            <br>
          </a>
        </div>
        <div class="header-right">
          <ul class="nav" style="margin-top:40px">

            <li class="nav-item"><a href="http://unpad.kuliahdaring.id/page/tentang-kami"
                class="nav-link ms-toggle-left animated zoomInDown animation-delay-10">Tentang Kami</a></li>

            <li class="nav-item">
              <a href="#" data-toggle="modal" data-target="#ms-account-modal"
                class="nav-link animated zoomInDown animation-delay-10">Masuk</a>
            </li>

            <li class="nav-item">
              <a href="https://unpad.kuliahdaring.id/register" class="nav-link animated zoomInDown animation-delay-10">Daftar</a>
            </li>
          </ul>
        </div>
      </div>
    </header>

    <nav class="navbar navbar-expand-md navbar-static ms-navbar ms-navbar-dark" style="background:#1AC2E5;width:100%">
      <div class="container container-full">
        <div class="navbar-header">
          <a class="navbar-brand" href="http://live.unpad.ac.id/">
            <img src="{!! isset(Setting('icon')->value) ? Setting('icon')->value : '/img/ingenio-logo.png' !!}" alt="{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}" width="50px" height="50px" style="">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="ms-navbar">
          <ul class="navbar-nav" v-for="menu in menus">
            <li class="nav-item">
              <a style="text-transform: capitalize;" :href="menu.url">@{{menu.name}}</a>
            </li>
          </ul>
        </div>
        <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
          <i class="zmdi zmdi-menu"></i>
        </a>
      </div>
      <!-- container -->
    </nav>
  </template>
</div>
  
  <script defer>
    const vueAppNav = new Vue({
      el: '#vueAppNav',
      data () {
        return {
          settings: [],
          menus: []
        }
      },
      mounted () {
          this.getSetting()
          this.getMenu()
      },
      methods: {
        getSetting () {
          axios({
            method: 'get',
            url: '/api/v3/settings',
            json: true
          }).then((response) => {
            // console.log(response.data.data)
            this.settings = response.data.data
          })
        },
        getMenu () {
          axios({
            method: 'get',
            url: '/api/v3/menus',
            json: true
          }).then((response) => {
            // console.log(response.data.data)
            this.menus = response.data.data
          })
        }
      },
    })
</script>