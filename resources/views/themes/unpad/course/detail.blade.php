@extends('themes.unpad.layouts.app')

@section('title', $course->title)

@push('meta')
<meta name="description" content="{{$course->subtitle}}. {{strip_tags($course->description)}}" />
<meta name="keywords" content="elearning {{$course->title}}, ecourse {{$course->title}}, training {{$course->title}}, kelas {{$course->title}}" />

<meta property="og:type" content="article"/>
<meta property="og:title" content="{{$course->title}}"/>
<meta property="og:description" content="{{strip_tags($course->description)}}"/>
<meta property="og:image" content="{{asset_url(course_image_medium($course->image))}}"/>
<meta property="og:url" content="{{url('course/' . $course->slug )}}"/>
<meta property="og:site_name" content="Ingenio"/>
<meta property="article:publisher" content="https://www.facebook.com/ingeniocoid/"/>
<meta property="og:image:width" content="640"/>
<meta property="og:image:height" content="424"/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{$course->title}}">
<meta name="twitter:description" content="{{strip_tags($course->description)}}">
<meta name="twitter:image" content="{{asset_url(course_image_medium($course->image))}}">
<meta name="twitter:site" content="@ingeniocoid">
@endpush

@push('style')
  <style media="screen">
    .ms-navbar{
      margin-bottom: 0;
    }
    .no-radius{
      border-radius: 0;
    }
    .top-title-bg{
      background: #192a56;
      max-height: 350px;
      padding: 10px;
    }
    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
    }
    .dq-description-max{
      overflow: hidden;
      min-height: 20px;
      max-height: 300px;
      display: -webkit-box!important;
      -webkit-box-orient: vertical;
      white-space: normal;
      margin:0;
    }
    .mt-20{
      margin-top: 20rem;
    }
    .detailSide{
      position: absolute;
      width: 380px;
      left: 38%;
      top: 10%;
      transform: translate(100%, 0%);
    }
    @media (max-width:1366px){
      .detailSide{
        position: absolute;
        width: 380px;
        left: 36%;
        top: 15%;
        transform: translate(100%, 0%);
      }
    }
    @media (max-width:768px){
      .top-title-bg{
        background: #ffffff;
        max-height: none;
        padding: 0 0 6rem 0;
        margin-bottom:2rem;
      }
        .top-title-bg .container .row .col-md-8{
          background: #192a56 !important;
        }
      .center{
        text-align: center !important;
      }
      .mt-20{
        margin-top: auto !important;
      }
    }
  </style>

  <style media="screen">
    .play-preview{
      color: #dcdde1;
      font-size:95px;
      padding: 10px;
    }
    .play-preview:hover{
      color: #f5f6fa;
      font-size:100px;
    }

    .btn-preview{
      position: absolute;
      width: 80%;
      height: auto;
      text-align: center;
      z-index: 30;
    }
    /*
      .btn-preview .frame{
        width: 120px;
        height: 120px;
        background: #00000073;
        padding: auto;
        opacity: 1;
        border: 0;
        border-radius: 100%;
        line-height: 11;
        transition: 0.2s ease;
        z-index: 29
      }
      .btn-preview .frame:hover{
        width: 150px;
        height: 150px;
      }
      */
      .btn-preview i{
        background: #0000009c;
        width: 100px;
        height: 100px;
        padding: 25px;
        border-radius: 100%;
        opacity: 1;
        margin-top: 10%;
        font-size: 50px;
        transition: 0.1s;
        z-index: 28;
      }
      .btn-preview i:hover{
        font-size: 60px;
        width: 110px;
        height: 110px;
      }

      #desktop{
        display: block;
      }
      #mobile{
        display: none;
      }
      @media (max-width:767px){
        #desktop{
          display: none;
        }
        #mobile{
          display: block;
        }
        .detailSide{
          display: none;
        }

        .col-md-6 p{
          margin: 0;
        }
      }
  </style>
  <style media="screen">
    .course-detail span{
      font-family: Roboto,sans-serif !important;
      font-size: 15px !important;
    }
    .list-group{
      border: 0;
    }
    .list-group li.list-group-item{
      border: 1px solid #eee;
    }
  </style>

  <link href="/videojs/video-js.css" rel="stylesheet">
  <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
  <link href="/videojs/quality-selector.css" rel="stylesheet">
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">

@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="#">Semua Kategori</a></li>
          <li>{{$course->category}}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="top-title-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-8" style="padding-top:5%;padding-bottom:5%;">
          <h1 style="color:white"><strong>{{$course->title}}</strong></h1>
          <p style="color:white; font-size:18px;">{{$course->subtitle}}</p>
          <span class="">
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <span style="color:white;padding: 0 7px;">5.0 (1 penilaian)</span>
          </span>
          <span style="color:white; padding: 0 7px;">{{$course->category}}</span>
          <span style="color:white; padding: 0 7px;">{{$course->level}}</span>
          <br>
          <span style="color:white;">Pengajar <a style="color:white;" href="/user/{{$course->author_slug}}"><strong>{{$course->author}}</strong></a></span>
          {{-- <li><strong>Pengajar:</strong> {{$course->author}}</li> --}}
          {{-- <li><strong>Kategori:</strong> {{$course->category}}</li>
          <li><strong>Level:</strong> {{$course->level}}</li> --}}
        </div>
        <div class="col-md-4">
          <br>
          <div class="card-md">
            <div class="card-block">
              @if($content_previews)
                <a id="previewContent" data-id="{{$content_previews[0]['id']}}" href="#/">
                  <div class="btn-preview">
                    <i class="fa fa-play color-white"></i>
                  </div>
                  <img src="{{asset_url(course_image_small($course->image))}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
                </a>
              @else
                <img src="{{asset_url(course_image_small($course->image))}}" class="img-fluid" alt="{{ $course->title }}" style="width:100%;"/>
              @endif
              @if($course->archive != '1')
                <h3 class="text-center color-dark" style="font-family:sans-serif;font-weight:600;">{{$course->price == 0 ? 'Gratis' : "Harga: " . rupiah($course->price)}}</h3>
                @php
                  if(Request::get('aff')) {
                    $affiliate = '?aff='.Request::get('aff');
                  }else{
                    $affiliate = '';
                  }
                @endphp
                @if($course->price === 0)
                  {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                    <input type="hidden" name="id" value="{{$course->id}}">
                    <input type="hidden" name="slug" value="{{$course->slug}}">
                    <button type="submit" class="btn btn-success btn-raised btn-block">Enroll Sekarang</button>
                  {{Form::close()}}
                @else
                  @if($CourseOnCart === false)
                    {{Form::open(['url' => 'cart/buy/'.$course->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-success btn-raised btn-block">Beli</button>
                    {{Form::close()}}
                  @endif
                  @if($CourseOnCart === false)
                    {{Form::open(['url' => 'cart/add/'.$course->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-primary btn-raised btn-block ">Tambahkan keranjang</button>
                    {{Form::close()}}
                  @else
                    <a href="{{url('cart')}}" class="btn btn-primary btn-raised btn-block">Lihat Keranjang</a>
                  @endif
                  @if($course->password)
                    <a href="#" data-target="{{Auth::check() ? '#CourseCheckPassword' : '#ms-account-modal'}}" data-toggle="modal" class="btn btn-info btn-raised btn-block">Enroll Dengan Password</a>
                  @endif
                @endif
              @endif
              <div class="color-dark">
                <b>Termasuk:</b>
                <ul>
                  <li>Total Durasi video <span id="video_durations"></span> </li>
                  <li>Akses seumur hidup</li>
                  <li>Akses di seluler</li>
                </ul>
              </div>
              <hr>
              <div class="text-center">
                <p>Bagikan:</p>
                <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('course/' . $course->slug )}}" rel="nofollow" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
                <a target="_blank" href="https://twitter.com/intent/tweet?text={{$course->title}};url={{url('course/' . $course->slug )}};via=ingeniocoid" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
                @if(Auth::check())
                  @if(isAffiliate(Auth::user()->id) != false)
                    <a href="#/" title="Promote Page" class="btn-circle btn-google btn-copy" data-clipboard-text="{{url('course/' . $course->slug ) . isAffiliate(Auth::user()->id)->url}}"><i class="fa fa-bookmark"></i></a>
                  @endif
                @endif
              </div>
            </div>
          </div>

          <div id="desktop">
            <div class="row">
              <div class="col-md-12">
                <h2 class="headline-md headline-line">Tentang <span>Pengajar</span></h2>
              </div>
            </div>
            <div class="card-md">
              <div class="card-block text-center">
                <a href="/user/{{$course->author_slug}}">
                  <img src="{{avatar($course->photo)}}" class="dq-frame-round-md">
                </a>
                <a href="/user/{{$course->author_slug}}">
                  <h3 style="font-weight:900;">{{$course->author}}</h3>
                </a>
                <div class="row text-left">
                  <div class="col-md-6 center">
                    <p><i class="fa fa-star-o"></i> {{$rate}} Rating</p>
                    <p><i class="fa fa-comments-o"></i> {{$count}} Reviews</p>
                  </div>
                  <div class="col-md-6 center">
                    <p><i class="fa fa-users"></i> {{$total_students}} Students</p>
                    <p><i class="fa fa-play-o"></i> {{$total_courses}} Courses</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- CONTENT -->
  <div class="wrap pt-2 pb-2 bg-white mb-2 color-dark" style="min-height: 750px;">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <!-- DESKRIPSI -->
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>Deskripsi</span></h2>
            </div>
          </div>
          <div>
            <div id="description" class="course-detail">
              <p>{!! $course->description !!}</p>
            </div>
            <a class="btn-more" id="more-deskripsi">+ Selengkapnya</a>
          </div>

          <!-- TUJUAN -->
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line"><span>Tujuan</span></h2>
            </div>
          </div>
          <div>
            <div id="target" class="course-detail">
              <p>{!! $course->goal !!}</p>
            </div>
            <a class="btn-more" id="more-tujuan">+ Selengkapnya</a>
          </div>

          <!-- MATERI -->
          <div class="ms-collapse" id="accordion" role="tabpanel" aria-multiselectable="true">
            <div class="row">
              <div class="col-md-12">
                <h2 class="headline-md headline-line">Materi untuk <span>kelas ini</span></h2>
              </div>
            </div>
            <?php $i=1; $video_durations = 0 ?>
            <div class="panel-group" id="accordion fade in active">
              @foreach($sections as $section)
               <div class="panel card-panel">
                 <a data-toggle="collapse" data-parent="#accordion" href="#content{{$i}}">
                   <div class="panel-heading" style="background:#f9f9f9">
                     <h4 class="panel-title">
                        <span style="color:#474545; font-size:13px">Topik {{$i}}:</span>
                        <br/><br/>
                        <strong style="color:#2e2e2e">{{ $section['title'] }}</strong>
                     </h4>
                   </div>
                 </a>
                 <div id="content{{$i}}" class="panel-collapse collapse {{$i == 1 ? 'show' : ''}}">
                   <div class="panel-body" style="background:white;">
                    <ul class="list-group">
                      @foreach($section['section_contents'] as $content)
                        @if($content->preview == '1') <a id="previewContent" data-id="{{$content->id}}" href="#/"> @endif
                          @if($content->type_content == 'label')
                            <div class="label-dq-content mb-1 mt-1">
                              {!!$content->description!!}                                   
                            </div>
                          @else
                            <li class="list-group-item">
                              <p class="color-dark">
                                <i class="{{content_type_icon($content->type_content)}}"></i> {{$content->title}}
                                <div class="ml-auto">
                                  @if($content->type_content == 'video')
                                    <span class="text-right">{{ $content->video_duration >= 3600 ? gmdate("H:i:s", $content->video_duration) : gmdate("i:s", $content->video_duration)}}</span> &nbsp;
                                    @php $video_durations += $content->video_duration @endphp
                                  @endif
                                  @if($content->preview == '0')
                                    <i style="color:#bbb" class="fa fa-lock"></i>
                                  @else
                                    <i style="color:#bbb" class="fa fa-unlock"></i>
                                  @endif
                                </div>
                              </p>
                            </li>
                          @endif
                        @if($content->preview == '1') </a> @endif
                      @endforeach
                      @foreach($section['section_quizzes'] as $quiz)
                        <li class="list-group-item">
                          <p class="color-dark">
                            <i class="fa fa-star"></i> {{$quiz->name}}
                            <div class="ml-auto">
                              <i style="color:#bbb" class="fa fa-lock"></i>
                            </div>
                          </p>
                        </li>
                      @endforeach
                    </ul>
                   </div>
                 </div>
               </div>
              <?php $i++; ?>
              @endforeach
            </div>
          </div>

        </div>

        <div id="mobile" class="col-md-4 color-dark text-center mt-20">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line">Tentang <span>Pengajar</span></h2>
            </div>
          </div>
          <div class="card-md">
            <div class="card-block">
              <a href="/user/{{$course->author_slug}}">
                <img width="120" height="120" src="{{avatar($course->photo)}}" class="dq-frame-round-md">
              </a>

              <a href="/user/{{$course->author_slug}}">
                <h3 style="font-weight:900;">{{$course->author}}</h3>
              </a>
              <div class="row text-left">
                <div class="col-md-6 center">
                  <p><i class="fa fa-star-o"></i> {{$rate}} Rating</p>
                  <p><i class="fa fa-comments-o"></i> {{$count}} Reviews</p>
                </div>
                <div class="col-md-6 center">
                  <p><i class="fa fa-users"></i> {{$total_students}} Students</p>
                  <p><i class="fa fa-play-o"></i> {{$total_courses}} Courses</p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal" id="CourseCheckPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id="CoursePassword" placeholder="Masukan Password">
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button id="CheckPassword" type="button" class="btn  btn-primary">Enroll</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->

  <!-- preview konten -->
  <div class="modal" id="modalPreviewContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
          <div class="modal-content" style="background:#2f3640">
              <div class="modal-header">
                  <h3 class="modal-title" id="previewTitle" style="color: white;"></h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div id="previewKonten" style="min-height:300px; color:white !important"></div>
                <div class="">
                  <br>
                  <p style="color: white; font-size:16px">Gratis sampel video:</p>
                  <ul class="list-group" style="list-style:none">
                    @foreach ($content_previews as $key => $content_preview)
                      <a id="previewContent" data-id="{{$content_preview['id']}}" href="#/">
                        <li class="list-group-item" style="list-style:none">
                          <p class="color-dark">
                            <i class="{{content_type_icon($content_preview['type_content'])}}"></i> {{$content_preview['title']}}
                          </p>
                        </li>
                      </a>
                    @endforeach
                  </ul>
                </div>
              </div>
          </div>
      </div>
  </div>
  <!-- preview konten -->
@endsection

@push('script')
  <script src="/videojs/ie8/videojs-ie8.min.js"></script>
  <script src="/videojs/video.js"></script>
  <script src="/videojs/videojs-markers.min.js"></script>
  <script src="/videojs/silvermine-videojs-quality-selector.min.js"></script>
  <script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
  <script src="{{asset('clipboard/clipboard.min.js')}}"></script>

	<script type="text/javascript">
		var clipboard = new ClipboardJS('.btn-copy');
		clipboard.on('success', function(e) {
	    swal("Berhasil!", "Silakan bagikan link ini untuk referal Anda: " + e.text, "success");
	    e.clearSelection();
		});
	</script>

  <script type="text/javascript">
    $("#video_durations").html('{{$video_durations >= 3600 ? gmdate("H", $video_durations) . ' jam' : gmdate("i", $video_durations) . ' menit'}}')
  </script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/course/check-password/{{$course->id}}',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          if(response === 'true'){
            $.ajax({
              url : '/course/enroll',
              type : 'get',
              data : {
                id : {{$course->id}},
                _token : '{{csrf_token()}}',
              },
              success : function(data){
                window.location.assign('/course/learn/{{$course->slug}}')
              }
            })
          }else{
            alert('Password salah');
          }
        }
      })
    })
  </script>
  <script type="text/javascript">
    if($('#description').height() > 300){
      $('#description').addClass('dq-description-max');
      $('#more-deskripsi').addClass('show');
    }
    if($('#target').height() > 300){
      $('#target').addClass('dq-description-max');
      $('#more-tujuan').addClass('show');
    }

    $('#more-deskripsi').click(function(){
      $('#description').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-deskripsi').show();
    });
    $('#more-tujuan').click(function(){
      $('#target').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-tujuan').show();
    });
  </script>

  <script type="text/javascript">
    $(function(){
      $(document).on('click', '#previewContent', function(){
        var content_id = $(this).attr('data-id');
        // alert(content_id);

        $.ajax({
          url : '/course/content',
          type : 'post',
          data : {
            'id' : content_id,
            '_token' : '{{csrf_token()}}'
          },
          beforeSend: function(){
            $("#modalPreviewContent").modal('show');
            $("#previewKonten").html('Mohon tunggu...');
          },
          success : function(result){
            // console.log(result);
            $("#previewTitle").html(result.title);

            if(result.type_content == 'video'){
              $("#previewKonten").html('<video id="my-video" class="video-js vjs-default-skin vjs-16-9 vjs-big-play-centered" controls="true" autoplay="true" preload="auto" width="100%" height="500"><source src="'+result.content+'" type="video/mp4" selected="true" label="360p"></video>');
              videojs('my-video', {playbackRates: [0.5, 1, 1.5, 2],controlBar: {fullscreenToggle: true},});
              var player = videojs('my-video');
              player.on('contextmenu', function(e) {
                  e.preventDefault();
              });

            }else if(result.type_content == 'file'){
              $("#previewKonten").html('<iframe width="100%" height="500px" src="'+result.content+'/preview"></iframe><div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>');
            }else{
              $("#previewKonten").html(result.content)
            }
          },
          error: function(){
            $("#previewKonten").html('Mohon maaf. Terjadi kesalahan');
          }
        });

        if(videojs('my-video')){
          videojs('my-video').dispose();
        }

      })
    })
  </script>

  <script type="text/javascript">
    $('#modalPreviewContent').on('hidden.bs.modal', function () {
      $("#previewKonten").html('');
      videojs('my-video').dispose();
    })
  </script>

@endpush
