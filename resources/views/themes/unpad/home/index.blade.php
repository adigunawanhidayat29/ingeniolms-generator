@extends('themes.unpad.layouts.app')

@section('title', 'Beranda')

@push('style')
    <style>
      /* FADE IN */
      .fade-enter-active {
        transition: opacity 1s;
      }
      .fade-enter {
        opacity: 0;
      }

      /* GO TO NEXT SLIDE */
      .slide-next-enter-active,
      .slide-next-leave-active {
        transition: transform 0.5s ease-in-out;
      }
      .slide-next-enter {
        transform: translate(100%);
      }
      .slide-next-leave-to {
        transform: translate(-100%);
      }

      /* GO TO PREVIOUS SLIDE */
      .slide-prev-enter-active,
      .slide-prev-leave-active {
        transition: transform 0.5s ease-in-out;
      }
      .slide-prev-enter {
        transform: translate(-100%);
      }
      .slide-prev-leave-to {
        transform: translate(100%);
      }

      /* SLIDES CLASSES */

      .blue {
        background: #4a69bd;
      }

      .red {
        background: #e55039;
      }

      .yellow {
        background: #f6b93b;
      }

      /* SLIDER STYLES */
      
      #slider {
        width: 100%;
        height: 600px;
        /* position: relative; */
      }

      .slide {
        width: 100%;
        height: 600px;
        position: absolute;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
      }

      .btn-slider {
        z-index: 10;
        cursor: pointer;
        border: 3px solid #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 70px;
        height: 70px;
        position: absolute;
        top: calc(50% - 35px);
        left: 1%;
        transition: transform 0.3s ease-in-out;
        user-select: none;
      }

      .btn-slider-next {
        left: auto;
        right: 1%;
      }

      .btn-slider:hover {
        transform: scale(1.1);
      }

    </style>
@endpush

@section('content')

{{-- <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper ms-hero-rev fullscreen-container"
    data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
      <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
          <ul>
          <!-- SLIDE  -->
          <li data-index="rs-0" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
              data-thumb="/plugins/revolution//images/citybg-100x50.jpg" data-rotate="0" data-saveperformance="off"
              data-title="Home" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
              data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="http://assets.unpad.kuliahdaring.id//uploads/sliders/home2646.jpg" alt="Home"
              data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"
              data-no-retina>
              <!-- LAYERS -->
              <div class="tp-caption tp-resizeme banner-responsive" id="slide-0-layer-0"
              data-x="['right','center','center','center']" data-hoffset="['0','0','0','0']"
              data-y="['middle','middle','middle','middle']" data-voffset="0" data-fontsize="['130','130','100','80']"
              data-lineheight="['130','130','100','80']" data-width="none" data-height="none" data-whitespace="nowrap"
              data-type="text" data-responsive_offset="on"
              data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
              data-textAlign="['right','right','right','right']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"
              data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
              style="z-index: 13; white-space: nowrap; font-size: 130px; line-height: 130px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-width:0px;letter-spacing:-7px;">
              <img src="/img/unpad/asset-11.png" class="img-responsive">
              </div>
      
      
          </li>
          <!-- SLIDE  -->
          <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
              data-thumb="/plugins/revolution//images/citybg-100x50.jpg" data-rotate="0" data-saveperformance="off"
              data-title="Home 2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
              data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="http://assets.unpad.kuliahdaring.id//uploads/sliders/home-2725.jpg" alt="Home 2"
              data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"
              data-no-retina>
              <!-- LAYERS -->
              <div class="tp-caption tp-resizeme banner-responsive" id="slide-1-layer-1"
              data-x="['right','center','center','center']" data-hoffset="['0','0','0','0']"
              data-y="['middle','middle','middle','middle']" data-voffset="0" data-fontsize="['130','130','100','80']"
              data-lineheight="['130','130','100','80']" data-width="none" data-height="none" data-whitespace="nowrap"
              data-type="text" data-responsive_offset="on"
              data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
              data-textAlign="['right','right','right','right']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"
              data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
              style="z-index: 13; white-space: nowrap; font-size: 130px; line-height: 130px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Arial, Helvetica, sans-serif;background-color:rgba(0, 0, 0, 0);border-width:0px;letter-spacing:-7px;">
              <img src="/img/unpad/asset-11.png" class="img-responsive">
              </div>
      
      
          </li>
          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
      </div>
    </div> --}}

<div id="vueAppHome">
  <template>
    
    <div id="slider">
      <transition-group tag="div" :name="transitionName" class="slides-group">
        <div v-if="show" :key="current" class="slide" >
          <p><img style="width:100% !important;" :src="slides[current].image"></p>
        </div>
      </transition-group>
      <div class="btn-slider btn-slider-prev" aria-label="Previous slide" @click="slide(-1)">
        &#10094;
      </div>
      <div class="btn-slider btn-slider-next" aria-label="Next slide" @click="slide(1)">
        &#10095
      </div>
    </div>

    <div class="wrap" style="margin-top:-50px;padding:15px;background:#f9f9f9;">
      <div class="container">
          <p class="text-center color-dark mt-6" style="font-family:sans-serif;font-size:20px;line-height:30px;">
          live.unpad.ac.id merupakan portal layanan pembelajaran secara virtual melalui Learning Management System yang
          dirancang untuk kebutuhan pembelajaran secara reguler untuk mahasiswa reguler dan mahasiswa Program Studi di Luar
          Kampus Utama (PSDKU) Universitas Padjadjaran.
          </p>
          <p class="text-center mt-2">
          <a href="/login" id="TourRegister" class="btn btn-xlg btn-raised btn-primary">Daftar Sekarang</a>
          </p>
      </div>
    </div>
  
    <!-- ms-hero ms-hero-black -->
    <div class="container-full dq-degree-bg" id="TourDegree">
      <div class="dq-overlay">
          <div class="container">
          <div class="row">
              <div class="col-md-6 color-white" style="margin-top:75px;">
              <h3>Degree</h3>
              <h1 class="text-uppercase dq-title-caption">Inovasi Regional</h1>
              <p style="font-size:20px;">Dapatkan informasi perkuliahan S2 program studi inovasi regional Universitas
                  Padjadjaran.</p>
              <p><a href="/degree/inovasi-regional" class="btn btn-xlg btn-raised btn-primary">Klik di sini</a></p>
              </div>
              <div class="col-md-6 color-white" style="margin-top:75px;">
              <h3>Degree</h3>
              <h1 class="text-uppercase dq-title-caption">S2 Kebijakan Publik</h1>
              <p style="font-size:20px;">Dapatkan informasi perkuliahan S2 program studi kebijakan publik Universitas
                  Padjadjaran.</p>
              <p><a href="/degree/s2-kebijakan-publik" class="btn btn-xlg btn-raised btn-primary">Klik di sini</a></p>
              </div>
          </div>
          </div>
      </div>
    </div>
  
    <div class="wrap" v-show="settings.courses.value == 'true'">
      <h1 class="text-center text-uppercase dq-title-caption">Kelas</h1>
      <div class="container mt-1">
          <div class="row">
            <div class="col-md-12 text-center mb-3">
                <p style="font-size:20px;">Pembelajaran online menawarkan cara baru untuk mengeksplorasi mata pelajaran yang
                Anda minati. Temukan minat Anda dengan melihat-lihat kategori kursus online kami:</p>
            </div>
            <div class="col-md-4" v-for="course in courses">
                <div class="card" style="box-shadow:none">
                  <div class="ms-carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                      <a :href="'/course/'+course.slug">
                          <div class="carousel-item active">
                          <span class="dq-play-video">
                              <i class="fa fa-play"></i>
                              <img class="d-block img-fluid" :src="course.image" style="height:200px;width:100%;">
                              <div class="carousel-caption" style="height:30%;padding:0;">
                              <h3>@{{course.title}}</h3>
                              </div>
                          </div>
                          </span>
                      </div>
                      <div class="card-block">
                      <b class="color-dark" style="font-size:20px">@{{course.title}}</b>
                      <p style="color:#ccc;">@{{course.user.name}}</p>
                      </div>
                      </a>
                  </div>
                </div>
            </div>
          </div>
      </div>
    </div>
  
    <!--container -->
    <div id="TourLearn" class="wrap" style="background-image: radial-gradient(farthest-corner at 50% 50%, #f1f1f1 50%, #ddd 75%, #bbb 100%);">
      <h1 class="text-center text-uppercase dq-title-caption" style="color:#757575;">Mengapa Live Unpad?</h1>
      <br>
      <div class="container mb-2">
          <div class="row">
          <div class="text-center col-md-4">
              <i class="fa fa-cube" style="font-size:100px"></i>
              <h3><b>Belajar apapun</b></h3>
              <p>Pilih dari ratusan kursus online gratis: dari Bahasa & Budaya hingga Bisnis & Manajemen; Sains & Teknologi
              untuk Kesehatan & Psikologi.</p>
          </div>
          <div class="text-center col-md-4">
              <i class="fa fa-comments" style="font-size:100px"></i>
              <h3><b>Belajar Bersama</b></h3>
              <p>Bergabunglah dengan kursus online dan temui pelajar lain dari seluruh dunia. Belajar semudah dan alami
              seperti mengobrol dengan teman-teman.</p>
          </div>
          <div class="text-center col-md-4">
              <i class="fa fa-graduation-cap" style="font-size:100px"></i>
              <h3><b>Belajar dengan ahlinya</b></h3>
              <p>Temui pendidik dari universitas terkemuka dan institusi budaya, yang akan berbagi pengalaman mereka melalui
              video, artikel, kuis dan diskusi.</p>
          </div>
          </div>
      </div>
    </div>
  </template>
</div>  
@endsection

@push('vue')
    <script defer>
      const vueAppHome = new Vue({
        el: '#vueAppHome',
        data () {
          return {
            courses: [],
            settings: {},
            current: 0,
            direction: 1,
            transitionName: "fade",
            show: false,
            slides: []
          }
        },
        mounted () {
            this.getHome()
            this.getSetting()
            this.show = true
        },
        methods: {
          getHome () {
            axios({
              method: 'get',
              url: '/api/v3/home',
              json: true
            }).then((response) => {
              // console.log(response.data.data.sliders)
              this.courses = response.data.data.courses.data
              this.slides = response.data.data.sliders
            })
          },
          getSetting () {
            axios({
              method: 'get',
              url: '/api/v3/settings',
              json: true
            }).then((response) => {
              // console.log(response.data.data)
              this.settings = response.data.data
            })
          },
          slide (dir) {
            this.direction = dir;
            dir === 1
              ? (this.transitionName = "slide-next")
              : (this.transitionName = "slide-prev");
            var len = this.slides.length;
            // console.log(len)
            this.current = (this.current + dir % len + len) % len;
            // console.log(this.current)
          }
        },
      })
  </script>
@endpush