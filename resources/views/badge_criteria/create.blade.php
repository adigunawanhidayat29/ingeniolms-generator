@extends('layouts.app')

@section('badge content', 'Create Badge')

@section('content')


<div>
  @if(!empty($badge))
    <h3>{{$badge->name}}</h3>
  @endif
</div>

<form method="POST" action="criteria_assign" enctype="multipart/form-data">
{{ csrf_field() }}
    <div id="content-forms">
      <div class="form-group">
          <label>Criteria</label>
          <select class="select2" data-placeholder="Select category" name="criteria_id" style="width: 100%;">
          @php
              $criterias = get_criteria();
          @endphp

          @foreach($criterias as $criteria)
              <option value="{{$criteria->id}}">{{$criteria->name}}</option>
          @endforeach
          </select>
      </div>
      @if(!empty($badge))
        <input type="hidden" name="badge_id" value="{{$badge->id}}">
        <input type="hidden" name="course_id" value="{{$badge->course_id}}">
      @endif
    </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

@endsection