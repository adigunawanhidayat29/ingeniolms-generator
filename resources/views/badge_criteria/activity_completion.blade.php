@extends('layouts.app')

@section('title', 'Activity Completion')

@push('style')
  <style>
    select.form-control:not([size]):not([multiple]) {
      height: auto;
      cursor: pointer;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Activity <span>Completion</span></h2>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/course/dashboard">Manage Class</a></li>
          <li><a href="/course/preview/5">*Class Name*</a></li>
          <li><a href="/5/list_badges">*Badge Name*</a></li>
          <li>Activity Completion</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="card-md">
            <div class="card-block">
              <form method="POST" action="/activity_completion/store" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="content-forms">
                  <div class="form-group" style="margin: 0;">
                    {{-- <label>Content</label> --}}
                    <select class="form-control" data-placeholder="Select category" name="idcontent[]" style="width: 100%;">
                      <option>- Select Content -</option>
                      @foreach($contents as $get)
                        <option value="{{$get->id}}">{{$get->title}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <input type="hidden" name="badge_id" value="{{$badge_id}}">
                <input type="hidden" name="criteria_id" value="{{$criteria_id}}">

                <button type="button" class="btn btn-raised btn-primary" id="add_btn">Add content</button>

                <hr style="margin: 2rem 0;">

                <div class="form-group">
                  <label class="radio-inline"><input type="radio" name="complete_type" value="any" required>ANY</label>
                  <label class="radio-inline"><input type="radio" name="complete_type" value="all">ALL</label>
                </div>

                <button type="submit" class="btn btn-raised btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $("#add_btn").on('click', function(){
        var html = '';
        html += '<div class="form-group" style="margin: 0;">';
        html += '<select class="form-control" data-placeholder="Select category" name="idcontent[]" style="width: 100%;">';
        html += '<option>- Select Content -</option>';
        html += '@foreach($contents as $get) <option value="{{$get->id}}">{{$get->title}}</option> @endforeach';
        html += '</select>'
        html += '</div>';
        $('#content-forms').append(html);
      })
    });
  </script>
@endpush