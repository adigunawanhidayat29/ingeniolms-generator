@extends('layouts.app')

@section('title', 'Video')

@push('style')
  <link rel="stylesheet" href="/sweetalert2/sweetalert2.min.css">  
  
@endpush

@push('meta')
  <meta name="description" content="Online Training bersama praktisinya Untuk Karir & Passionmu">
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <h2 class="headline-md no-m">Video <span>{{ Setting('title')->value }}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>Video</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        @foreach($videos as $video)
          <div class="col-md-4">
            <iframe width="350" height="197" src="{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection