@component('mail::message')

  @slot('subheader')
    Pengajar Ingenio
  @endslot

  <center>
  <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
    <tr>
      <td style="text-align:left;">
        <br>
        <strong>Hallo {{$Instructor->name}},</strong><br>
        <p>Terimakasih sudah mendaftar menjadi pengajar di Ingenio. Mohon untuk menunggu proses verifikasi oleh admin selama kurang lebih 2 x 24 jam.</p>
      </td>
    </tr>
  </table>
  </center>
@endcomponent
