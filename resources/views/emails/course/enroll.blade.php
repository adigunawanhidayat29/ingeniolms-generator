@component('mail::message')
# Hello {{$name}}

Thank for enrolled course <strong>{{$course}}</strong>.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
