@extends('layouts.app')

@section('title', 'Aktivasi Berhasil')

@section('content')
  <div class="container">
    <div class="alert alert-success">
      Akun anda berhasil aktif, silakan login untuk mulai menikmati fitur kami
      <a href="/login">Login</a>
    </div>
  </div>
@endsection
