
@extends('layouts.app')

@section('title')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.profile.breadcrumb_home')</a></li>
          <li>@lang('front.profile.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
  </div>

    <div class="container">
    <h2>{{$badge->badge_name}}</h2>
    <img src="/storage/badge_content/{{$badge->badge_image}}">
    <p></p>
    </div>
@endsection