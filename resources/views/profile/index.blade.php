@extends('layouts.app')

@section('title', $User->name)

@push('style')
  <style>
    @media (max-width: 420px) {
      .headline.headline-md {
        font-size: 24px;
        margin-top: 0;
      }
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.profile.breadcrumb_home')</a></li>
          <li>@lang('front.profile.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container">
    @if(Session::get('success'))
      <div class="alert alert-success">{{Session::get('success')}}</div>
    @elseif(Session::get('message'))
      <div class="alert alert-danger">{{Session::get('message')}}</div>
    @endif
    <div class="card nav-tabs-ver-container">
      <div class="row">
        <div class="col-lg-3">
          <ul class="nav nav-tabs-ver nav-tabs-ver-primary d-none d-sm-flex" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#change-profile" aria-controls="change-profile" role="tab" data-toggle="tab"><i class="zmdi zmdi-accounts-list-alt"></i><span class="d-none d-sm-inline"> @lang('front.profile.general_tab')</a></li>
            <li class="nav-item"><a class="nav-link" href="#my-badges" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="zmdi zmdi-account-circle"></i><span class="d-none d-sm-inline">My Badges</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-password" aria-controls="change-password" role="tab" data-toggle="tab"><i class="zmdi zmdi-lock"></i><span class="d-none d-sm-inline"> @lang('front.profile.password_tab')</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-avatar" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="zmdi zmdi-account-circle"></i><span class="d-none d-sm-inline"> @lang('front.profile.avatar_tab')</a></li>
            @if(isInstructor(Auth::user()->id))
              <li class="nav-item"><a class="nav-link" href="#change-bankAccount" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="fa fa-money"></i><span class="d-none d-sm-inline"> @lang('front.profile.bank_account_tab')</a></li>
            @endif
          </ul>
          <ul class="nav nav-tabs d-flex d-sm-none" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#change-profile" aria-controls="change-profile" role="tab" data-toggle="tab"><i class="zmdi zmdi-accounts-list-alt"></i><span class="d-none d-sm-inline"> @lang('front.profile.general_tab')</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-password" aria-controls="change-password" role="tab" data-toggle="tab"><i class="zmdi zmdi-lock"></i><span class="d-none d-sm-inline"> @lang('front.profile.password_tab')</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-avatar" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="zmdi zmdi-account-circle"></i><span class="d-none d-sm-inline"> @lang('front.profile.avatar_tab')</a></li>
            @if(isInstructor(Auth::user()->id))
              <li class="nav-item"><a class="nav-link" href="#change-bankAccount" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="fa fa-money"></i><span class="d-none d-sm-inline"> @lang('front.profile.bank_account_tab')</a></li>
            @endif
          </ul>
        </div>
        <div class="col-lg-9 nav-tabs-ver-container-content">
          <div class="card-block">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="change-profile">
                <h3 class="headline headline-md">@lang('front.profile.header_general')</h3>
                <div class="divider divider-dark"></div>
                {{Form::open(['url' => '/profile/update','method' => 'post'])}}
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.name_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.name_label')" name="name" value="{{$User->name}}">
                  </div>
                  @if($user_provider > 0)
                  <div class="form-group">
                    <label class="control-label" for="name">Training Provider</label>
                    <input type="email" class="form-control" value="{{$training_provider->name}}" disabled>
                  </div>
                  @else
                  <div class="form-group">
                    <label class="control-label" for="name">Training Provider</label>
                    <input type="email" class="form-control" value="" disabled>
                  </div>
                  @endif
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.email_label')</label>
                    <input type="email" class="form-control" id="" placeholder="@lang('front.profile.email_label')" name="email" value="{{$User->email}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.phone_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.phone_label')" name="phone" value="{{$User->phone}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.description_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.description_label')" name="short_description" value="{{$User->short_description}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.facebook_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.facebook_label')" name="facebook" value="{{$User->facebook}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.instagram_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.instagram_label')" name="instagram" value="{{$User->instagram}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.twitter_label')</label>
                    <input type="text" class="form-control" id="" placeholder="@lang('front.profile.twitter_label')" name="twitter" value="{{$User->twitter}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.language_label')</label>
                    {{-- <input type="text" class="form-control" id="" placeholder="@lang('front.profile.language_label')" name="language" value="{{$User->language}}"> --}}
                    <br>
                    <select name="language" class="form-control selectpicker">
                      @foreach(\App\Models\Language::get() as $lang)
                        <option {{$lang->alias == $User->language ? 'selected' : ''}} value="{{$lang->alias}}">
                          {{$lang->title}}</option>
                      @endforeach
                    </select>
                  </div>
                  <input type="submit" name="submit" value="@lang('front.profile.submit_button')" class="btn btn-primary btn-raised">
                {{Form::close()}}
              </div>

              <div role="tabpanel" class="tab-pane" id="change-password">
                <h3 class="headline headline-md">@lang('front.profile.header_password')</h3>
                <div class="divider divider-dark"></div>
                {{Form::open(['url' => '/profile/change-password','method' => 'post'])}}
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.password_label')</label>
                    <input type="password" class="form-control" id="" name="password" placeholder="@lang('front.profile.password_label')" value="" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.confirm_password_label')</label>
                    <input type="password" class="form-control" id="" name="password-confirm" placeholder="@lang('front.profile.confirm_password_label')" value="" required>
                  </div>
                  <input type="submit" name="submit" value="@lang('front.profile.submit_button')" class="btn btn-primary btn-raised">
                {{Form::close()}}
              </div>

              <div role="tabpanel" class="tab-pane" id="change-avatar">
                <h3 class="headline headline-md">@lang('front.profile.header_avatar')</h3>
                <div class="divider divider-dark"></div>
                {{Form::open(['url' => '/profile/change-avatar','method' => 'post', 'files' => true])}}
                  <div class="form-group">
                    <label class="control-label" for="name">@lang('front.profile.avatar_label')</label>
                    <br>
                    <img src="{{avatar($User->photo)}}" alt="{{$User->name}}" class="img-thumbnail" style="height:100px;">
                    <br><br>
                    <input type="file" class="form-control" id="" name="image" placeholder="@lang('front.profile.avatar_label')">
                  </div>
                  <input type="submit" name="submit" value="@lang('front.profile.submit_button')" class="btn btn-primary btn-raised">
                {{Form::close()}}
              </div>

              @if(isInstructor(Auth::user()->id))
                <div role="tabpanel" class="tab-pane" id="change-bankAccount">
                  <h3 class="headline headline-md">@lang('front.profile.header_bank_account')</h3>
                  <div class="divider divider-dark"></div>
                  {{Form::open(['url' => '/instructor/banks/update', 'method' => 'post'])}}
                    <div class="form-group">
                      <label class="control-label" for="name">@lang('front.profile.bank_name_label')</label>
                      <input type="text" class="form-control" id="" placeholder="@lang('front.profile.bank_name_label')" name="bank_name" value="{{$bank_name}}">
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="name">@lang('front.profile.account_number_label')</label>
                      <input type="text" class="form-control" id="" placeholder="@lang('front.profile.account_number_label')" name="account_number" value="{{$account_number}}">
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="name">@lang('front.profile.account_name_label')</label>
                      <input type="text" class="form-control" id="" placeholder="@lang('front.profile.account_name_label')" name="account_name" value="{{$account_name}}">
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="name">@lang('front.profile.bank_branch_label')</label>
                      <input type="text" class="form-control" id="" placeholder="@lang('front.profile.bank_branch_label')" name="bank_branch" value="{{$bank_branch}}">
                    </div>
                    <input type="submit" name="submit" value="@lang('front.profile.submit_button')" class="btn btn-primary btn-raised">
                  {{Form::close()}}
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
<script>
  $(function () {
      $('#table-badges').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      });
  });
</script>

@endpush