@extends('layouts.app')

@section('title', 'Aktivasi Berhasil')

@section('content')
  <div class="container">
    <div class="alert alert-success">
      Kami telah mengirim link ke email Anda untuk aktivasi.
      <a style="text-decoration: underline" href="/">Ok, Lanjutkan Belajar</a>
    </div>
  </div>
@endsection
