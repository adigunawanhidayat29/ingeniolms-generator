@extends('layouts.mapp')

@section('title', $User->name)

@section('content')
  <div class="container">
    <div class="card nav-tabs-ver-container">
      <div class="row">
        <div class="col-lg-3">
          <ul class="nav nav-tabs-ver nav-tabs-ver-primary" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#change-profile" aria-controls="change-profile" role="tab" data-toggle="tab"><i class="zmdi zmdi-account"></i> Change Profile</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-password" aria-controls="change-password" role="tab" data-toggle="tab"><i class="zmdi zmdi-lock"></i> Change Password</a></li>
            <li class="nav-item"><a class="nav-link" href="#change-avatar" aria-controls="change-avatar" role="tab" data-toggle="tab"><i class="zmdi zmdi-account"></i> Change Avatar</a></li>
          </ul>
        </div>
        <div class="col-lg-9 nav-tabs-ver-container-content">
          <div class="card-block">
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="change-profile">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Your Profile</h3>
                  </div>
                  <div class="panel-body">
                    {{Form::open(['url' => '/m/profile/update/'.$User->id,'method' => 'post'])}}
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" class="form-control" id="" placeholder="Name" name="name" value="{{$User->name}}">
                    </div>
                    <div class="form-group">
                      <label for="name">Email</label>
                      <input type="email" class="form-control" id="" placeholder="Email" name="email" value="{{$User->email}}">
                    </div>
                    <input type="submit" name="submit" value="Save Profile" class="btn btn-primary btn-raised">
                    {{Form::close()}}
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="change-password">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Change Password</h3>
                  </div>
                  <div class="panel-body">
                    {{Form::open(['url' => '/m/profile/update/password/'.$User->id,'method' => 'post'])}}
                    <div class="form-group">
                      <label for="name">Password</label>
                      <input type="password" class="form-control" id="" name="password" placeholder="Password" value="" required>
                    </div>
                    <div class="form-group">
                      <label for="name">Password Confirmation</label>
                      <input type="password" class="form-control" id="" name="password-confirm" placeholder="Confirm Password" value="" required>
                    </div>
                    <input type="submit" name="submit" value="Save Profile" class="btn btn-primary btn-raised">
                    {{Form::close()}}
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="change-avatar">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Change Avatar</h3>
                  </div>
                  <div class="panel-body">
                    {{Form::open(['url' => '/m/profile/update/avatar/'.$User->id,'method' => 'post', 'files' => true])}}
                    <div class="form-group">
                      <label for="name">Photo</label>
                      <br>
                      <img src="{{avatar($User->photo)}}" alt="{{$User->name}}" class="img-thumbnail" style="height:100px;">
                      <br><br>
                      <input type="file" class="form-control" id="" name="image" placeholder="Photo">
                    </div>
                    <input type="submit" name="submit" value="Save Profile" class="btn btn-primary btn-raised">
                    {{Form::close()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
