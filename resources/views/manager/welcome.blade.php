@extends('layouts.app')

@section('title')
	{{ 'Menjadi Manager' }}
@endsection

@section('content')
	<div class="container">
		<h1>Selamat datang menjadi Manager</h1>
    <p>Untuk menjadi manager kelas Anda harus mengajukan diri terlebih dahulu untuk kami review</p>
    <p>Nikmati fitur yang tersedia setelah Anda menjadi manager kelas di Ingenio</p>

    <p>Tunggu apalagi?</p>
    <a class="btn btn-primary btn-raised" href="/become-a-manager/join">Ajukan Sekarang</a>
	</div>
@endsection

@push('script')

@endpush
