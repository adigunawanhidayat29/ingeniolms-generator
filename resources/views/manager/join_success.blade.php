@extends('layouts.app')

@section('title')
	{{ 'Menjadi Manager' }}
@endsection

@section('content')
	<div class="container">
		<h1>Satu Langkah Lagi</h1>
    <p>Terimakasih sudah mengajukan diri sebagai manager di ingenio. Kami akan melakukan review terlebih dahulu selama kurang lebih 2x24 jam.</p>
		<p>Jika Anda terpilih, Anda akan mendapatkan pemberitahuan dari kami melalui email Anda.</p>
		<h3>-<i>Keep Learning</i>-</h3>		
	</div>
@endsection

@push('script')

@endpush
