@extends('layouts.app')

@section('title', 'Quiz Completions ')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info alert-light alert-dismissible" role="alert">
				Search Data Report berdasarkan Course, Content, Participant dan Quiz!
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse">
						<h4 class="panel-title">
							Report Course Completion
						</h4>
					</a>
				</div>
				<div id="collapse" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Course</label><br>
							<div class="col-md-10">
								<select class="form-control selectpicker" name="model" id="course" required>
									<option value="">Choose Course</option>
									@foreach($kursus as $data)
									<option value="{{$data->id}}">{{$data->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="card card-warning">
							<div class="card-header">
								<h3 class="card-title" id="course-title"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Progress</th>
										<th>Setting</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="com-name"></td>
										<td class="com-percentage"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
						<h4 class="panel-title">
							Report Content Completion
						</h4>
					</a>
				</div>
				<div id="collapse1" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Content</label><br>
							<div class="col-md-6">
								<select class="form-control selectpicker" name="model" id="content" required>
									<option value="">Choose Content</option>
									@foreach($contents as $data)
									<option value="{{$data->id}}">{{$data->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<iframe style="width:100%; height:400px; align:center" src="" id="content-url" allowfullscreen="true" ></iframe>
							</br>
						</div>
						<div class="card card-success">
							<div class="card-header">
								<h3 class="card-title" id="content-title"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Content</th>
										<th>Finish</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="progress-name"></td>
										<td class="progress-content"></td>
										<td class="progress-finish"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
						<h4 class="panel-title">
							Report Learn Student 
						</h4>
					</a>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Participant</label><br>
							<div class="col-md-6">
								<select class="form-control selectpicker" name="model" id="user" required>
									<option value="">Choose Participant</option>
									@foreach($users as $data)
									<option value="{{$data->id}}">{{$data->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="card card-success">
							<div class="card-header">
								<h3 class="card-title" id="user-name"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Content</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="progress-name1"></td>
										<td class="progress-title"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
						<h4 class="panel-title">
							Report Quiz Participant
						</h4>
					</a>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Quiz</label><br>
							<div class="col-md-6">
								<select class="form-control selectpicker" name="model" id="kuis" required>
									<option value="">Choose Quiz</option>
									@foreach($kuis as $data)
									<option value="{{$data->id}}">{{$data->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="card card-success">
							<div class="card-header">
								<h3 class="card-title" id="kuis-name"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								<thead>
									<tr>
										<th>Name</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="kuis-name"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</div>
@endsection

@push('script')
<script type="text/javascript">

$("#course").change(function(){
	$.ajax({
		url : "/report/get-content/"+$(this).val(),
		method : "get",
		success: function(result){
			$("#course-title").html(result.course.title);
			
			$(".com-name").html("");
			$(".com-percentage").html("" );
				
			$.map(result.completions, function( val, i ) {
				$(".com-name").append(val.name + "<br>");
				$(".com-percentage").append(val.percentage + " %" + "<br>");
			});
		}
	})
});

$("#content").change(function(){
	$.ajax({
		url : "/report/get-content/"+$(this).val(),
		method : "get",
		success: function(result){
			
			$("#content-title").html(result.content.title);
			$("#content-url").attr("src", result.content.full_path_file + "/preview");
			
			$(".progress-name").html("");
			$(".progress-content").html("");
			$(".progress-finish").html("");
			
			$.map(result.progresses, function( val, i ) {
				$(".progress-name").append(val.name + "<br>");
				$(".progress-content").append(val.title + "<br>");
				$(".progress-finish").append(val.updated_at + "<br>");
			});
		}
	})
});

$("#user").change(function(){
	$.ajax({
		url : "/report/get-content/"+$(this).val(),
		method : "get",
		success: function(result){
			$("#user-name").html(result.user.name);
			
			$(".progress-name1").html("");
			$(".progress-title").html("");
				
			$.map(result.userprogress, function( val, i ) {
				$(".progress-name1").append(val.name + "<br>");
				$(".progress-title").append(val.title + "<br>");
			});
		}
	})
});

$("#kuis").change(function(){
	$.ajax({
		url : "/report/get-content/"+$(this).val(),
		method : "get",
		success: function(result){
			$("#kuis-name").html(result.kuis.name);
			
			$(".kuis-name").htmld("");
			
			$.map(result.kuisp, function( val, i ) {
				$(".kuis-name").append(val.name + "<br>");
			});
		}
	})
});
</script>
@endpush
