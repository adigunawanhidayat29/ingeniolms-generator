@extends('layouts.app')

@section('title', 'Quiz Summary')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title"><i class="zmdi zmdi-graduation-cap"></i> Summary</h3>
				</div>
				<table class="table table-no-border table-striped">
					<thead>
						<tr>
							<th>Name</th>
							@foreach($quiz as $data)
							<th><a href="/report/summary/detail/{{$data->id}}">{{$data->name}}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach($name as $data)
						<tr>
							<td>{{$data->name}}</td>
							@foreach($quizparticipant as $qp)
								@if($qp->quiz_id == $data->quiz_id)
									<td>{{ number_format($qp->grade, 2, '.', '')}}</td>
								@endif
							@endforeach
						</tr>
						@endforeach
						
						<!--
						@foreach($quizparticipant as $qp)
						<td>
							@if($qp->quiz_id == $data->quiz_id)
								{{ number_format($qp->grade, 2, '.', '')}}
							@endif
						</td>
						@endforeach
						-->
						<!--
						@foreach($quizparticipant as $data)
						<tr>
							<td>{{$data->name}}</td>
							@foreach($quiz as $kuis)
							<td>{{$kuis->id == $data->quiz_id ? number_format($data->grade, 2, '.', ''):""}}</td>
							@endforeach
						</tr>
						@endforeach
						-->
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
</div>

@endsection