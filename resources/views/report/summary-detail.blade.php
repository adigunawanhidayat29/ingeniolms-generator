@extends('layouts.app')

@section('title', 'Quiz Summary')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title"><i class="zmdi zmdi-graduation-cap"></i> {{$quiz->name}}</h3>
				</div>
				<table class="table table-no-border table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Start On</th>
							<th>Completed</th>
							<th>Grade</th>
						</tr>
					</thead>
					<tbody>
						@foreach($quizparticipant as $data)
						<tr>
							<td>{{$data->name}}</td>
							<td>{{$data->time_start}}</td>
							<td>{{$data->time_end}}</td>
							<td>{{$data->grade}}</td>
						</tr>	
						@endforeach
					</tbody>
				</table>
			</div>
			<a href="/report/summary/1" class="btn btn-raised btn-info">Back</a>
		</div>
	</div>
</div>
@endsection