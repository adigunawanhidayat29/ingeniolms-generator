@extends('layouts.app')

@section('title', 'All Projects')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        @foreach($CourseProjectUsers as $CourseProjectUser)
          <div class="col-md-4">
            <div class="card">
              <a href="{{url('project/'. $CourseProjectUser->slug)}}" class="zoom-img withripple">
                <img src="{{asset_url(course_image_small($CourseProjectUser->image))}}" alt="{{ $CourseProjectUser->title }}" class="dq-image-big img-fluid">
              </a>
              <div class="card-block text-left">
                <a href="{{url('project/'. $CourseProjectUser->slug)}}">
                  <h4 class="color-primary dq-max-title">{{$CourseProjectUser->title}}</h4>
                </a>
                <p>
                  <i class="color-dark">{{ $CourseProjectUser->name }}</i>
                </p>
              </div>
            </div>
          </div>
          @endforeach
      </div>
    </div>
    </div>

    <!--Pagination Wrap Start-->
    <div class="text-center">
      {{$CourseProjectUsers->links('pagination.default')}}
    </div>
    <!--Pagination Wrap End-->
</div>

@endsection

@push('script')

@endpush
