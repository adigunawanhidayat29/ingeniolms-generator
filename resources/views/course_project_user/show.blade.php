@extends('layouts.app')

@section('title')
	{{ $CourseProjectUser->title }}
@endsection

@section('content')
  <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-8">
						<h1>{{$CourseProjectUser->title}}</h1>
					</div>
					<div class="col-md-4" class="text-right" style="line-height:90px;">
						<span>{{$CourseProjectUserLikes}} <i class="fa fa-thumbs-o-up"></i> Like</span>
						&nbsp;&nbsp;
						<span>{{count($CourseProjectUserComments)}} <i class="fa fa-comments"></i> Comment</span>
					</div>
				</div>
					<div class="thumbnail-container">
						<img width="100%" class="img-fluid" src="{{asset_url($CourseProjectUser->image)}}" alt="{{$CourseProjectUser->title}}">
					</div>
				<p>
					{!!$CourseProjectUser->description!!}
				</p>

				<div class="text-center">
					<form class="" action="/course/project/like/{{ $CourseProjectUser->id }}" method="post">
						{{csrf_field()}}
						<input type="submit" class="btn btn-primary btn-raised" value="Like">
					</form>
				</div>

				<h3>Comments</h3>
				<hr>

				<div class="row">
					<div class="col-md-1">
						<img class="img-fluid" src="{{avatar(Auth::user()->photo)}}" style="border-radius:100%;" alt="{{Auth::user()->name}}">
					</div>
					<div class="col-md-11">
						<form class="" action="/course/project/comment/{{ $CourseProjectUser->id }}" method="post">
							{{csrf_field()}}
							<div class="form-group">
							  <textarea placeholder="Write your comment..." style="border:1px solid #bbb; height:100px; padding:10px" name="comment" class="form-control"></textarea>
								<input type="submit" class="btn btn-primary btn-raised" value="Post">
							</div>
						</form>
					</div>
				</div>

				<hr>

				@foreach($CourseProjectUserComments as $CourseProjectUserComment)
					<div class="dq-padding-comment">
						<span>
							<div class="row">
								<img src="{{avatar($CourseProjectUserComment->photo)}}" class="dq-image-user dq-ml">
								<div class="col-md-8">
									<div>
										<span>
											{{$CourseProjectUserComment->name}}
										</span>
									</div>
									<div>
										<span>
											{{$CourseProjectUserComment->comment}}
										</span>
									</div>
								</div>
							</div>
						</span>
					</div>
					<hr>
				@endforeach

			</div>
		</div>
	</div>
@endsection
