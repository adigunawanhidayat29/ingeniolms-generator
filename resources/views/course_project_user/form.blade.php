@extends('layouts.app')

@section('title')
	Submit Project {{$CourseProject->title}}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Submit Project '{{$CourseProject->title}}'</h3>
					</div>
					<div class="box-body">
						{{ Form::open(array('url' => $action, 'method' => 'post', 'files' => true)) }}

							<div class="form-group">
								<label for="">Title</label>
								<input placeholder="Title" type="text" class="form-control" name="title" value="{{$title}}" required>
							</div>

							<div class="form-group">
								<label for="title">Description</label>
								<textarea name="description" id="description">{!!$description!!}</textarea>
							</div>

							<div class="form-group">
								@if($image != "")
									<img src="{{asset_url($image)}}" alt="">
									<br>
								@endif
								<label for="title">Browse Image</label>
								<input placeholder="Image" type="file" class="form-control" name="image">
							</div>

							<div class="form-group">
								{{  Form::submit('Submit' , array('class' => 'btn btn-primary btn-raised')) }}
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
	</script>
	{{-- CKEDITOR --}}
@endpush
