<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Konten Video Manager</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/videojs/video-js.css" rel="stylesheet">
    <link href="/videojs/videojs.markers.min.css" rel="stylesheet">
    <link rel="shotcut icon" href="/favicon.png">
    <style media="screen">
      .vjs-play-progress{
        background: red !important;
        height: 5px;
      }
      .vjs-big-play-button {
        font-size: 3em;
        line-height: 2em !important;
        height: 2em !important;
        width: 3em !important;
        display: block;
        position: relative;
        top: 50% !important;
        left: 50% !important;
        padding: 0;
        cursor: pointer;
        opacity: 1;
        border: 0.06666em solid #fff;
        background-color: red !important;
        -webkit-border-radius: 0.3em;
        -moz-border-radius: 0.3em;
        border-radius: 0.3em;
        -webkit-transition: all 0.4s;
        -moz-transition: all 0.4s;
        -ms-transition: all 0.4s;
        -o-transition: all 0.4s;
        transition: all 0.4s;
      }
      .video-js .vjs-time-control{
        flex: none;
        font-size: 1em;
        line-height: 5.2em;
        min-width: 2em;
        width: auto;
        padding-left: 1em;
        padding-right: 1em;
      }
      .vjs-volume-bar .vjs-slider-horizontal{
        width: 5em;
        height: 1.3em;
      }
      .video-js .vjs-volume-bar{
        margin: 2.35em 0.45em;
        margin-top: 2.35em;
        margin-right: 0.45em;
        margin-bottom: 2.35em;
        margin-left: 0.45em;
      }
      .vjs-playback-rate .vjs-playback-rate-value{
        pointer-events: none;
        font-size: 1.5em;
        line-height: 3.3em;
        text-align: center;
      }
      .vjs-menu-button-popup .vjs-menu .vjs-menu-content{
        background-color: rgba(43, 51, 63, 0.7);
        position: absolute;
        width: 100%;
        bottom: 3.5em;
        max-height: 15em;
      }
      .video-js .vjs-control-bar{
        width: 100%;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        height: 5.0em;
        background-color: rgba(43, 51, 63, 0.7);
      }
      .vjs-button > .vjs-icon-placeholder::before {
        font-size: 1.8em;
        line-height: 2.8em;
      }
      .modal-dialog {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
      }

      .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
      }
    </style>

        {{-- <script src="https://apis.google.com/js/api.js"></script>
        <script>

          function start() {
            // Initializes the client with the API key and the Translate API.
            gapi.client.init({
              'apiKey': 'AIzaSyDl4fmCktf6Q1JDuzHsfRqLnq8xZ0jRzco',
              'discoveryDocs': ['https://www.googleapis.com/drive/v2/files?fields=id,name'],
              'scope': 'https://www.googleapis.com/auth/drive',
              // 'clientId': '621552637539-o2tsbqeeigrknc1cqppi37r93v2vioiv.apps.googleusercontent.com',
            }).then(function() {
              // Executes an API request, and returns a Promise.
              // The method name `language.translations.list` comes from the API discovery.
              return gapi.client.drive.files.get({
                'fileId': '15wKj4q1hYFlPLXOc6_jkFoEVHwOFQjgD'
              });

              // return gapi.client.request({
              //   'path': '/drive/v2/files',
              //   'method': 'GET',
              //   'params' : {
              //     'fileId' : '15wKj4q1hYFlPLXOc6_jkFoEVHwOFQjgD',
              //   }
              // });

            }).then(function(response) {
              console.log(response);
            })
          };

          // Loads the JavaScript client library and invokes `start` afterwards.
          gapi.load('client', start);

        </script> --}}

        {{-- <script type="text/javascript">
              function handleClientLoad() {
                // Loads the client library and the auth2 library together for efficiency.
                // Loading the auth2 library is optional here since `gapi.client.init` function will load
                // it if not already loaded. Loading it upfront can save one network request.
                gapi.load('client:auth2', initClient);
              }

              function initClient() {
                // Initialize the client with API key and People API, and initialize OAuth with an
                // OAuth 2.0 client ID and scopes (space delimited string) to request access.
                gapi.client.init({
                    apiKey: 'AIzaSyDl4fmCktf6Q1JDuzHsfRqLnq8xZ0jRzco',
                    discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
                    clientId: '621552637539-o2tsbqeeigrknc1cqppi37r93v2vioiv.apps.googleusercontent.com',
                    scope: 'profile',
                    scope: 'https://www.googleapis.com/auth/drive',
                }).then(function () {
                  // Listen for sign-in state changes.
                  gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                  // Handle the initial sign-in state.
                  updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                });
              }

              function updateSigninStatus(isSignedIn) {
                // When signin status changes, this function is called.
                // If the signin status is changed to signedIn, we make an API call.
                if (isSignedIn) {
                  makeApiCall();
                }
              }

              function handleSignInClick(event) {
                // Ideally the button should only show up after gapi.client.init finishes, so that this
                // handler won't be called before OAuth is initialized.
                gapi.auth2.getAuthInstance().signIn();
              }

              function handleSignOutClick(event) {
                gapi.auth2.getAuthInstance().signOut();
              }

              function makeApiCall() {
                // Make an API call to the People API, and print the user's given name.
                gapi.client.people.people.get({
                  'resourceName': 'people/me',
                  'requestMask.includeField': 'person.names'
                }).then(function(response) {
                  console.log('Hello, ' + response.result.names[0].givenName);
                  console.log(gapi.auth.getToken().access_token);
                }, function(reason) {
                  console.log('Error: ' + reason.result.error.message);
                });
              }

            </script>
            <script async defer src="https://apis.google.com/js/api.js"
              onload="this.onload=function(){};handleClientLoad()"
              onreadystatechange="if (this.readyState === 'complete') this.onload()">
            </script> --}}

  </head>
  <body>
    {{-- <button id="signin-button" onclick="handleSignInClick()">Sign In</button>
    <button id="signout-button" onclick="handleSignOutClick()">Sign Out</button> --}}
    <nav class="navbar navbar-default" role="navigation" style="background:#1E479D; border-radius:0">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a style="color:white" class="navbar-brand" href="/course/preview/{{$course->id}}"><i class="glyphicon glyphicon-chevron-left"></i> Kembali</a>
        </div>
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container">

      {{-- <div class="container-fluid dq-video-frame">
        <div class="dq-video embed-responsive embed-responsive-16by9">
          <video
            id="my-video"
            class="video-js embed-responsive-item vjs-big-play-centered"
            controls="true"
            preload="auto"
            autoplay="true"
            width="100%"
            height="100%">
            <source src="{{server_assets() . $content->full_path_original}}" type='video/mp4'>
          </video>
        </div>
      </div> --}}

      {{-- KONTEN YOUTUBE --}}
      @if($content->type_content == 'url-video')
      <div class="container-fluid dq-video-frame">
        <div class="dq-video embed-responsive embed-responsive-16by9">
        <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
        <video
          id="my-video"
          class="video-js embed-responsive-item vjs-big-play-centered"
          controls="true"
          preload="auto"
          autoplay="false"
          width="100%"
          height="387px"
          style="width: 100%; height: 387px;"
          data-setup='{ "techOrder": ["youtube", "html5"], "sources": [{ "type": "video/youtube", "src": "{{$content->full_path_file}}"}] }'
        ></video>
      </div>
      </div>
    @endif
    {{-- KONTEN YOUTUBE --}}

    {{-- KONTEN VIDEO --}}
    @if($content->type_content == 'video')
      <div class="container-fluid dq-video-frame">
        <div class="dq-video embed-responsive embed-responsive-16by9">
        <h2 class="headline-md mt-0 mb-1" style="width: calc(100% - 50px);"><span>{{ $content->title }}</span></h2>
        <video
          id="my-video"
          class="video-js embed-responsive-item vjs-big-play-centered"
          controls="true"
          preload="auto"
          autoplay="true"
          width="100%"
          style="width: 100% !important; height: 500px;"
          height="100%">
          <source src="{{asset_url($content->path_file)}}" type='video/mp4'>
        </video>
      </div>
    </div>
    @endif
    {{-- KONTEN VIDEO --}}

      <br>
      <button class="btn btn-default" type="button" id="btnAdd">Buat Kuis</button>
      <br><br>
      <div class="panel panel-default">
        <div class="panel-body">
          <table class="table table-bordered">
            <tr>
              <th>Pertanyaan</th>
              <th>Pada detik</th>
              <th>Pilihan</th>
            </tr>
            @foreach($content_video_quizes as  $data)
              <tr>
                <td>{{$data->question}}</td>
                <td>{{$data->seconds}}</td>
                <td>
                  <a onclick="return confirm('Yakin?')" href="/course/content/video/delete/{{$data->id}}">Hapus</a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>

      <div class="modal fade" id="modalAddKuis" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="">Input Kuis Dalam detik ke <span id="video-secon">0</span></h4>
            </div>
            <form class="" action="/content-video-quiz/store" method="post">
              {{csrf_field()}}
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Pertanyaan</label>
                  <input type="text" name="question" class="form-control" id="" placeholder="Pertanyaan" required>
                  <input type="hidden" name="seconds" id="question_seconds">
                  <input type="hidden" name="content_id" value="{{Request::segment(5)}}">
                </div>
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="form-group">
                        <label for="">Jawaban</label>
                        <input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]" required>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="">Deskripsi Jawaban</label>
                          <input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">
                        </div>
                      </div>
                      <div class="form-group">
                        <select class="form-control" name="is_correct[]" style="width:15%">
                          <option value="1">Benar</option>
                          <option value="0">Salah</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="form-group">
                        <label for="">Jawaban</label>
                        <input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]">
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="">Deskripsi Jawaban</label>
                          <input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">
                        </div>
                      </div>
                      <div class="form-group">
                        <select class="form-control" name="is_correct[]" style="width:15%">
                          <option value="1">Benar</option>
                          <option value="0">Salah</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div id="place_new_answer"></div>
                </div>
                <button type="button" id="add_new_answer" class="btn btn-default">Tambah Jawaban</button>
              </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Simpan">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      @foreach($content_video_quizes as  $data)
        <div class="modal fade" id="content_video_quizes{{$data->id}}" role="dialog" aria-labelledby="" style="z-index:9999999999999999999 !important; position:absolute !important;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">{{$data->question}}</h3>
                  </div>
                  <div class="panel-body">
                    <div id="alertCheckAnswer{{$data->id}}"></div>
                    @foreach($data->content_video_quiz_answers as $content_video_quiz_answer)
                      <div class="radio">
                        <label><input type="radio" name="content_video_quiz_answer_{{$data->id}}" value="{{$content_video_quiz_answer->id}}">{{$content_video_quiz_answer->answer}}</label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" id="btnCheckAnswer" data-id="{{$data->id}}" class="btn btn-primary">Periksa Jawaban</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Lanjutkan</button>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>

    <!-- If you'd like to support IE8 -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/videojs/ie8/videojs-ie8.min.js"></script>
    <script src="/videojs/video.js"></script>
    <script src="/videojs/Youtube.min.js"></script>
    <script src="/videojs/videojs-markers.min.js"></script>
    <script type="text/javascript">
      // =========== VIDEO PROPERTY =======
      videojs('my-video', {
        playbackRates: [0.5, 1, 1.5, 2],
        controlBar: {
          fullscreenToggle: true
        }
      });
      // =========== VIDEO PROPERTY =======


      // =========== EVENT ON SECOND =======
      var player = videojs('my-video');

      // markers
      player.markers({
        markerStyle: {
           'width':'3px',
           'background-color': 'yellow'
        },
        markers: [
          @foreach($content_video_quizes as  $data)
           {time: '{{$data->seconds}}.1', text: "{{$data->question}}"},
          @endforeach
        ]
      });
      // markers

        @foreach($content_video_quizes as  $data)
          var timeCheck{{$data->id}} = function() {
            if (player.currentTime() > {{$data->seconds}}) {
              player.off('timeupdate', timeCheck{{$data->id}});
              player.trigger('played{{$data->seconds}}Second');
            }
          };
          player.on('played{{$data->seconds}}Second', function(){
            player.pause();
            $("#content_video_quizes{{$data->id}}").modal({backdrop: 'static', keyboard: false, show:'true'});
            console.log(player.currentTime()); // get current time
          });
          player.on('timeupdate', timeCheck{{$data->id}});
        @endforeach

      // =========== EVENT ON SECOND =======

      $("#btnAdd").click(function(){
        // console.log(player.currentTime()); // get current time
        $("#video-secon").html(Math.floor(player.currentTime()))
        $("#question_seconds").val(Math.floor(player.currentTime()))
        player.pause();
        $("#modalAddKuis").modal('show');
      })
    </script>

    {{-- add new section --}}
    <script type="text/javascript">
      $("#add_new_answer").click(function(){
        $("#place_new_answer").append(
          '<div class="panel panel-default">'+
            '<div class="panel-body">'+
              '<div class="form-group">'+
                '<label for="">Jawaban</label>'+
                '<input type="text" class="form-control" id="" placeholder="Jawaban" name="answer[]">'+
              '</div>'+
              '<div class="col-md-12">'+
                '<div class="form-group">'+
                  '<label for="">Deskripsi Jawaban</label>'+
                  '<input type="text" class="form-control" id="" placeholder="Deskripsi Jawaban" name="answer_description[]">'+
                '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<select class="form-control" name="is_correct[]" style="width:15%">'+
                  '<option value="1">Benar</option>'+
                  '<option value="0">Salah</option>'+
                '</select>'+
              '</div>'+
            '</div>'+
          '</div>'
        );
      });
    </script>
    {{-- add new section --}}

    {{-- check answer --}}
    <script type="text/javascript">
      $(function(){
        $(document).on('click','#btnCheckAnswer',function(e){
          e.preventDefault();
            var data_id = $(this).attr('data-id');
            var content_video_quiz_answer_id = $("input[name*='content_video_quiz_answer_"+data_id+"']:checked").val()

            $.ajax({
              url: '/content-video-quiz/check-answer',
              type: 'post',
              data: {
                _token: '{{csrf_token()}}',
                content_video_quiz_answer_id: content_video_quiz_answer_id
              },
              success: function(result){
                if(result.is_correct == '1'){
                  $("#alertCheckAnswer"+data_id).removeClass('alert alert-danger').addClass('alert alert-success');
                  $("#alertCheckAnswer"+data_id).html(
                    '<strong>Betul</strong>'+
                    '<p>'+result.answer_description+'</p>'
                  );
                }else{
                  $("#alertCheckAnswer"+data_id).removeClass('alert alert-success').addClass('alert alert-danger');
                  $("#alertCheckAnswer"+data_id).html(
                    '<strong>Salah</strong>'+
                    '<p>'+result.answer_description+'</p>'
                  );
                }
              }
            })
        });
      });

    </script>
    {{-- check answer --}}

    {{-- <script type="text/javascript">
      $.ajax({
        url: 'https://www.googleapis.com/drive/v2/files/15wKj4q1hYFlPLXOc6_jkFoEVHwOFQjgD',
        type: 'get',
        headers: {
          'Authorization':'Bearer ya29.GlyqBT761w2tIotcxnQ8IpEOKQ4biIa1zNKboiih5xZVtFDHZYOpr9VGQwv374AJDnKkL9h6BvdrqKW3_Sz2bVATQUyzr_xd-sZXdaw9HRAqMTYiMPnNslLmYKGJcA',
        },
        success: function(result){
          // console.log(result);
          console.log(result.downloadUrl);
          $("#videoSource").attr('src', 'https://doc-08-00-docs.googleusercontent.com/docs/securesc/mo2o1s20j9idif6fkrq35qhcqedbar39/n4o28jhg42nj48ogongg6tfap5r0r8hv/1524794400000/08619370197821206898/17567607825987252969/15wKj4q1hYFlPLXOc6_jkFoEVHwOFQjgD?h=05282844538638547335&access_token=ya29.GlyqBT761w2tIotcxnQ8IpEOKQ4biIa1zNKboiih5xZVtFDHZYOpr9VGQwv374AJDnKkL9h6BvdrqKW3_Sz2bVATQUyzr_xd-sZXdaw9HRAqMTYiMPnNslLmYKGJcA')
        }
      })
    </script> --}}

  </body>
</html>
