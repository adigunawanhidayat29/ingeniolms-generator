@extends('baper.template')
@section('title', 'Register - Tes Baper')

@push('style')
  <style media="screen">
    .mw-500{
      max-width: 500px;
    }
    .card-register{
      background: #fff;
      width: 100%;
      border-radius: 5px;
      box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.05);
    }
      .card-register .card-header{
        background-color: #ffffff !important;
        padding: 1.25rem 4rem;
        margin-bottom: 0;
        background-color: rgba(0,0,0,.03);
        border-bottom: 1px solid rgba(0,0,0,.05);
      }
        .card-register .card-header .card-title{
          margin: 0.5rem 0;
        }
      .card-register .card-block{
        padding: 1rem 1rem 3rem 1rem;
      }
    .form-register{
      padding: 0 3rem;
    }
  </style>
@endpush

@section('content')
  <div class="wrap" style="background-color: #f8f9fa; min-height: 100vh; padding: 10vh 0;">
    <div class="container">

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
      <div class="mx-auto mw-500">
        <div class="card-register">
          <div class="card-header">
            <h3 class="card-title">Register BAPER PLUS <sub class="fw-600 fs-14" style="bottom:0;">TEST</sub></h3>
          </div>
          <div class="card-block">
            <form class="form-register" action="" method="post">
              {{csrf_field()}}
              <div class="form-group">
                {{--<label for="">Nama</label>--}}
                <input type="text" name="name" class="form-control" id="" placeholder="Nama" value="{{old('name')}}">
              </div>
              <div class="form-group">
                {{--<label for="">Email</label>--}}
                <input type="email" name="email" class="form-control" id="" placeholder="Email" value="{{old('email')}}">
              </div>
              <div class="form-group">
                {{--<label for="">No Telepon</label>--}}
                <input type="number" name="phone" class="form-control" id="" placeholder="No. Telepon" value="{{old('phone')}}">
              </div>
              <div class="form-group">
                {{--<label for="">Password</label>--}}
                <input type="password" name="password" class="form-control" id="" placeholder="Password">
              </div>
              <div class="form-group">
                {{--<label for="">Konfirmasi Password</label>--}}
                <input type="password" name="password_confirmation" class="form-control" id="" placeholder="Konfirmasi Password">
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-lg btn-block btn-primary btn-raised" value="Register">
              </div>
            </form>
            <div class="text-center">
              <span>Sudah punya akun? </span><a href="/tesbaper/login">Login disini</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')

@endpush
