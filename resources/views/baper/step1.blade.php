@extends('baper.template')
@section('title', 'Tes Baper - Step 1')

@push('style')

    <style type="text/css">
      a.disabled {
        text-decoration: none;
        color: black;
        cursor: default;
      }
      .goto-link p{
        color: #333;
        margin-left: 0.5rem;
        margin-bottom: 0;
        overflow: hidden;
        min-height: 20px;
        display: -webkit-inline-box !important;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        white-space: normal;
        line-height: 1.5rem;
        width: 90%;
      }

      /* REPLACE */
      .pagination{
        display: block;
        font-size: 12px;
        margin: 0;
      }
      .pagination .page-item .page-link{
        padding: 8px;
      }
      .page-item:first-child .page-link, .page-item:last-child .page-link{
        padding: .5rem .75rem;
      }

      .checkbox label, .radio label, label{
        color: #333;
      }

      .form-control, .form-group .form-control {
        border: 1px solid #e3e3e3;
        background-image: none;
        background-size: 0 2px,100% 1px;
        background-repeat: no-repeat;
        background-position: center bottom,center calc(100% - 1px);
        background-color: #f3f3f3;
        transition: background 0s ease-out;
        float: none;
        box-shadow: none;
        border-radius: 2px;
        padding: 0 1rem;
      }
        .form-control:focus, .form-group .form-control:focus{
          border: 1px solid #f3f3f3;
        }
      /* REPLACE */

      .m-0{
        margin: 0 !important;
      }

      .baper-header{
        padding: 1rem;
      }

      .card-baper{
        border-radius: 0 0 5px 5px;
        background: #f9f9f9;
        transition: all 0.3s;
        border: 1px solid #f5f5f5;
        margin-bottom: 2rem;
        margin-bottom: 20px;
      }
        .card-baper .card-header{
          padding: 1rem;
          background: #f6f6f6;
          border-bottom: 1px solid #ececec;
          margin-bottom: 0;
          display: flex;
        }
          .card-baper .card-header .card-title{
            margin: 0;
            font-size: 16px;
            font-weight: 500;
            color: #333;
            display: -webkit-inline-box;
          }
            .card-baper .card-header .card-title p{
              margin-left: 0.5rem;
              margin-bottom: 0;
              width: 96%;
              line-height: 1.5;
              font-weight: 400;
            }
        .card-baper .card-block{
          padding: 1rem;
        }
      .card-framestep{
        border-radius: 0 0 5px 5px;
        background: #ffffff;
        transition: all 0.3s;
        border: 1px solid #f5f5f5;
        margin-bottom: 2rem;
        margin-bottom: 20px;
      }
        .card-framestep .card-header{
          padding: 1rem;
          background: #f6f6f6;
          border-bottom: 1px solid #ececec;
          margin-bottom: 0;
          display: flex;
        }
          .card-framestep .card-header .card-title{
            margin: 0;
            font-size: 16px;
            font-weight: 400;
            color: #333;
            display: -webkit-inline-box;
          }
            .card-framestep .card-header .card-title p{
              margin-left: 0.5rem;
              margin-bottom: 0;
              width: 96%;
              line-height: 1.5;
              font-weight: 400;
            }
        .card-framestep .card-block{
          padding: 1rem;
        }
        .card-framestep .card-footer{
          padding: 0.5rem 1rem;
          background: #f6f6f6;
          border-top: 1px solid #ececec;
          margin-bottom: 0;
          display: block;
        }

      .baper-list-test{
        display: flex;
        width: 100%;
        height: auto;
      }
        .baper-list-test .number{
          margin: 0 1rem 0 0;
          text-align: center;
        }
          .baper-list-test .number .number-class{
            background: #f3f3f3;
            color: #333;
            padding: 10px;
            margin: 7px 0;
            border-radius: 5px;
            box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
          }
        .baper-list-test .question{
          width: 100%;
          text-align: left;
        }
          .baper-list-test .question .question-class{
            list-style: none;
            cursor: move;
            background: #f3f3f3;
            color: #333;
            padding: 10px;
            margin: 7px 0;
            border-radius: 5px;
            box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
          }
    </style>
@endpush

@section('content')
  <div class="wrap pt-2 pb-2 bg-white" style="min-height: 100vh">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="card-framestep">
            <div class="card-header">
              <p class="card-title">Step 1</p>
            </div>
            <form id="myform">
            <div class="card-block no-p">
              @foreach($quiz_page_breaks as $quiz_page_break)
              <div class="col-md-12">
                <div class="baper-header">
                  <h2 class="headline-md"><span>{{$quiz_page_break->title}}</span></h2>
                  <p>{{$quiz_page_break->description}}</p>
                </div>
              </div>
              <div class="col-md-12">
                <div id="question_content">
                  @php $i =1; @endphp
                  @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
                    <div class="card-baper" id="question_{{$quiz_question->id}}">
                      <div class="card-header">
                        <div class="card-title">
                          <span class="badge">{{$i++}}</span> {!!$quiz_question->question!!}
                        </div>
                      </div>
                      <div class="card-block">
                        @if($quiz_question->quiz_type_id == '6')
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input required onkeyup="save_session_answer(this)" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="text" class="form-control">
                              </div>
                            </div>
                          </div>
                        @endif
                      </div>
                    </div>
                  @endforeach

                  <div class="card-baper" id="question_BaperCode">
                    <div class="card-header">
                      <div class="card-title">
                        <span class="badge">5</span> Kode Batch (Abaikan jika Anda tidak mendapatkannya dari koordinator tes)
                      </div>
                    </div>
                    <div class="card-block">

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input onkeyup="save_session_answer(this)" name="BaperCode" id="BaperCode" type="text" class="form-control">
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
              @endforeach
            </div>
            </form>
            <div class="card-footer">
              <div class="text-right">
                <a href="#/" onclick="nextPage()" class="btn btn-primary btn-raised">Lanjutkan</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    $('#sidebarCall').click(function(){
      $('.sidebar').toggleClass('show');
      // $(this).hide();
    });
    $('#sidebarCallClose').click(function(){
      $('.sidebar').toggleClass('show');
      // $('#sidebarCall').show();
    });
  </script>

  {{-- <script src="/js/plugins.min.js"></script> --}}
  {{-- <script src="/js/app.min.js"></script> --}}
  {{-- <script src="/js/portfolio.js"></script> --}}
  {{-- <script src="/js/jquery.js"></script> --}}
  <script src="/js/bootstrap.min.js"></script>

  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  {{-- finish quiz --}}
  <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($all_quiz_page_breaks as $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
          @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]:checked').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
            // check if answer is null
            if(answer_checked == null){
              answer_checked = 0;
            }else{
              answer_checked = answer_checked;
            }
            // check if answer is null
          @elseif($quiz_question->quiz_type_id == '3')
            var answer_checked = $('input[name=answer_ordering{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'questionOrdering'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '5')
            var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '6')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @endif

          // answers.push(answer_checked);
          answers.push(session_value);
          questions.push('{{$quiz_question->id}}');
          question_type.push('{{$quiz_question->quiz_type_id}}');
      @endforeach
    @endforeach

      // console.log(answers);

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('tesbaper/finish/'.$quiz->id)}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          _token: token
        },
        success : function(result){
          //remove session answer
          @foreach($all_quiz_page_breaks as $quiz_page_break)
            @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
              localStorage.removeItem('answer{{$quiz_question->id}}');
              localStorage.removeItem('questionOrdering{{$quiz_question->id}}');
            @endforeach
          @endforeach
          //remove session answer

          window.location.assign('{{url('tesbaper/result/'.$quiz->id)}}');
        },
      });
    }

    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
  @foreach($all_quiz_page_breaks as $quiz_page_break)
    @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
      var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');

      @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}][value='"+session_value+"']").attr("checked","checked");
        }
      @elseif($quiz_question->quiz_type_id == '3')
        if(session_value !== null){
          $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '5')
        if(session_value !== null){
          $("textarea[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @elseif($quiz_question->quiz_type_id == '6')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @endif

      var BaperCode = get_session_answer('BaperCode');
      // only code baper
      $("input[name=BaperCode]").val(BaperCode);

    @endforeach
  @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}

  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script src="/jquery-ui/jquery.ui.touch-punch.min.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
          $('.answerNumber').each(function (i) {
              var humanNum = i + 1;
              $(this).html(humanNum + '');
          });
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}

  {{-- <script type="text/javascript">
    function nextPage(){

      var next = false;

      @foreach($quiz_page_breaks as $index => $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
          @if($quiz_question->quiz_type_id == 6)

            if(!get_session_answer('{{'answer'.$quiz_question->id}}')){
              alert('{{$quiz_page_break->title . ' | ' . strip_tags($quiz_question->question) }} harus di isi')
              next = false;
            }else{
              next = true;
            }

          @endif
        @endforeach
      @endforeach

      //console.log(next);

      if(next === true){
         window.location.assign('{{url('/tesbaper/step-2')}}');
      }


    }
  </script> --}}

  <script type="text/javascript" src="/jquery-validate/jquery.validate.min.js"></script>

  <script type="text/javascript">
    var validator = $('#myform').validate({
      errorClass: "bg-danger",
      errorElement: "div",
    });

    function nextPage(){

      if($("#myform").valid()){   // test for validity
           // do stuff if form is valid
           window.location.assign('{{url('/tesbaper/step-2')}}');
       } else {
           // do stuff if form is not valid
           validator.focusInvalid();
       }

    }

    // localStorage.clear();
  </script>
@endpush
