<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Baperplus - @yield('title')</title>
    <meta name="description" content="Material Style Theme">

    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">

    <!-- Material Design fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap Material Design -->
    <!-- <link rel="stylesheet" href="/css/bootstrap-material-design.min.css"> -->
    <link rel="stylesheet" href="/css/app-static-style.css">
    <link rel="stylesheet" href="/css/preload.min.css">
    <link rel="stylesheet" href="/css/plugins.min.css">
    <link rel="stylesheet" href="/css/style.dq-light-blue.min.css">
    <link rel="stylesheet" href="/css/dq-style-2.css">
    <link rel="shorcut icon" href="https://baperplus.ingenio.co.id/images/uploads/1/5bf27105a72e4_logo.jpg?0.7607144102014562">
    @stack('style')

  </head>
  <body>

    <div id="page" class="page">

      <div id="non-printable" class="block empty yummy top-left" style="box-shadow: 0 1px 25px rgba(0, 0, 0, 0.1); padding-top: 0px;padding-bottom: 0px;background-color: rgb(255, 255, 255);background-image: none;">
  			<div class="overly" style="background-color: transparent;">
  			</div>
  			<div class="container">
  				<div data-component="grid" class="row">
  					<div class="col-md-12" style="padding: 10px;">
  						<div data-container="true">
  							<div data-component="image" id="4BSgj">
                  <a href="#">
                    <img src="https://baperplus.ingenio.co.id/images/uploads/1/5bf27105a72e4_logo.jpg?0.7607144102014562" class="" style="width: 50px; height: 50px; border-color: rgba(0, 0, 0, 0); border-style: none; border-width: 1px; border-radius: 5px;" alt="image" />
                  </a>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  			<!-- /.container -->
  		</div>
      @yield('content')
    </div>

    <script src="/js/plugins.min.js"></script>
    <script src="/js/app.min.js"></script>
    <script src="/js/portfolio.js"></script>

    @stack('script')

  </body>
</html>
