@extends('baper.template')
@section('title', 'Laporan Tes Baper')

@push('style')
  <style media="screen">
  *{
  font-family: Roboto,sans-serif;
  }

  .report{
  background: #fff;
  width: 100%;
  padding: 10rem;
  }
  .report-header{
  position: relative;
  /* background: #ff0; */
  background: #00b050;
  padding: 1rem 2rem;
  text-align: center;
  }
  .report-header h2{
  color: #fff;
  font-size: 14px;
  font-weight: 600;
  margin: 0;
  }
  .report-title{
  position: relative;
  width: 100%;
  margin: 3rem 0;
  }
  .report-title img{
  position: absolute;
  width: 100px;
  right: 0;
  }
  .report-title p{
  font-size: 14px;
  }
  .report-title h2{
  margin: 0 0 1rem 0;
  font-weight: 600;
  }
  .report-body{

  }
  .report-body .participant{
  margin: 3rem 0;
  text-align: center;
  }
  .report-title .participant p{
  font-size: 14px;
  }
  .report-body .participant h2{
  margin: 0 0 1rem 0;
  font-size: 5rem;
  font-weight: 400;
  border-bottom: 2px solid #f3f3f3;
  width: 400px;
  margin-left: auto;
  margin-right: auto;
  }
  .report-body .cluster{
  margin: 3rem 0;
  }
  .cluster-table{
  width: 100%;
  }
  .cluster-table tr th, .cluster-table tr td{
  padding: 1rem 3rem;
  border: 1px solid #e9ecef;
  }
  </style>
  <style media="screen">

  .report{
  background: #fff;
  width: 100%;
  padding: 2rem;
  }
  .report-header{
  position: relative;
  /* background: #ff0; */
  background: #00b050;
  padding: 1rem 2rem;
  margin: 0;
  text-align: center;
  }
  .report-header h2{
  color: #fff;
  font-size: 14px;
  font-weight: 600;
  margin: 0;
  }
  .report-title{
  position: relative;
  width: 100%;
  margin: 3rem 0;
  }
  .report-title img{
  position: absolute;
  width: 100px;
  right: 0;
  }
  .report-title p{
  font-size: 14px;
  }
  .report-title h2{
  /* margin: 0 0 1rem 0; */
  margin: 0;
  font-weight: 600;
  }
  .report-body{

  }
  .report-body .grade{
  margin: 5rem 0;
  text-align: center;
  }
    .report-body .grade h3{
    margin: 0;
    }
  .report-body .participant{
  /* margin: 3rem 0; */
  margin: 0;
  text-align: center;
  }
  .report-title .participant p{
  font-size: 14px;
  }
  .report-body .participant h2{
  margin: 0 0 1rem 0;
  font-size: 5rem;
  font-weight: 400;
  border-bottom: 2px solid #f3f3f3;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  }
  .report-body .cluster{
  margin: 1rem 0;
  }
  .cluster-table{
  width: 100%;
  }
  .cluster-table tr th, .cluster-table tr td{
  padding: 0.5rem;
  border: 1px solid #e9ecef;
  font-size: 14px;
  }

  </style>

  <style type="text/css">

    #printable { display: none; }

    @media print
    {
        #non-printable { display: none; }
        #printable { display: block; width: 100%}
    }
    </style>

@endpush

@section('content')
  <div class="wrap pt-2 pb-2 mt-2 mb-2">
    <div class="container">
      <div class="row" id="non-printable">
        <div class="col-md-12">
          <a id="ignorePDF" target="_blank" href="/tesbaper/download/report/{{Request::segment(3)}}/{{Request::segment(4)}}" class="btn btn-danger btn-raised">Download Laporan PDF</a>
          <a id="ignorePDF" href="#/" onclick="print()" class="btn btn-info btn-raised">Print Laporan</a>

          <!-- REPORT -->
          <div class="report">
            <div class="report-header">
              <h2>BAPER PLUS</h2>
            </div>
            <div class="report-title">
              <img src="/baper-assets/logo.jpg" alt="baperplus">
              <p>Learning Consultant</p>
              <h2>BEFA Institute</h2>
              <p>Better Education For All</p>
            </div>
            <div class="report-body">
              <div class="participant">
                <p>diberikan kepada:</p>
                <h3>{{$biodata[0]['data']}}</h3>
                <p>Berasal dari sekolah {{$biodata[1]['data']}}</p>
                <p>No HP Orangtua {{$biodata[2]['data']}}</p>
                <p>Tanggal Lahir {{$biodata[3]['data']}}</p>
              </div>

              <div class="cluster">
                <table class="cluster-table" style="width:100%">
                  <tr class="active" style="background:#f3f3f3">
                    <th>Rekomendasi Jurusan</th>
                    <th>Urutan Cluster Kamu</th>
                  </tr>
                  <tr>
                    <td>
                      @foreach($answers_majors as $index => $value)
                        {{$index + 1}}. {{$value['major_name']}} <br>
                      @endforeach
                    </td>
                    <td>
                      @foreach($result_clusters as $index => $value)
                        {{-- @if($index < 4) --}}
                          {{$index + 1}}. {{$value['cluster']}} <br>
                        {{-- @endif --}}
                      @endforeach
                    </td>
                  </tr>
                </table>
              </div>

              <div class="grade">
                <h3>Tipe Kecerdasan</h3>
                <div class="cluster">
                  <div id="container"></div>
                  <div class="containerImage"></div>
                </div>
              </div>

            </div>
          </div>
          <!-- END REPORT -->


        </div>
      </div>

      <div class="row">
        <div id="printable">
          <div class="report">
            {{-- <div class="report-header">
              <h2>BAPER PLUS</h2>
            </div>
            <div class="report-title">
              <img src="/baper-assets/logo.jpg" alt="baperplus">
              <p>Learning Consultant</p>
              <h2>BEFA Institute</h2>
              <p>Better Education For All</p>
            </div> --}}
            <div class="report-body">
              <div class="participant">
                <p>diberikan kepada:</p>
                <h3>{{$biodata[0]['data']}}</h3>
                <p>Berasal dari sekolah {{$biodata[1]['data']}}</p>
              </div>

              <div class="cluster">
                <table class="cluster-table" style="width:100% !important">
                  <tr class="active" style="background:#f3f3f3">
                    <th>Rekomendasi Jurusan</th>
                    <th>Urutan Cluster Kamu</th>
                  </tr>
                  <tr>
                    <td>
                      @foreach($answers_majors as $index => $value)
                        {{$index + 1}}. {{$value['major_name']}} <br>
                      @endforeach
                    </td>
                    <td>
                      @foreach($result_clusters as $index => $value)
                        {{-- @if($index < 4) --}}
                          {{$index + 1}}. {{$value['cluster']}} <br>
                        {{-- @endif --}}
                      @endforeach
                    </td>
                  </tr>
                </table>
              </div>

              <div class="grade">
                <h3>Tipe Kecerdasan</h3>
                <div class="cluster">
                  <div class="containerImagePrint"></div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@push('script')

<script src="/highcharts/code/highcharts.js"></script>
<script src="/highcharts/code/modules/data.js"></script>
<script src="/highcharts/code/modules/drilldown.js"></script>
<script src="/highcharts/code/modules/exporting.js"></script>

<script type="text/javascript">
  EXPORT_WIDTH = 1000;

  function save_chart(chart) {
    var render_width = EXPORT_WIDTH;
    var render_height = render_width * chart.chartHeight / chart.chartWidth

    // Get the cart's SVG code
    var svg = chart.getSVG({
        exporting: {
            sourceWidth: chart.chartWidth,
            sourceHeight: chart.chartHeight
        }
    });

    // Create a canvas
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
    document.body.appendChild(canvas);
    $(".containerImage").html(canvas);

    // Create an image and draw the SVG onto the canvas
    var image = new Image;
    image.onload = function() {
        canvas.getContext('2d').drawImage(this, 0, 0, render_width, render_height);
    };
    image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  }

  function save_chartPrint(chart) {
    var render_width = EXPORT_WIDTH;
    var render_height = render_width * chart.chartHeight / chart.chartWidth

    // Get the cart's SVG code
    var svg = chart.getSVG({
        exporting: {
            sourceWidth: chart.chartWidth,
            sourceHeight: chart.chartHeight
        }
    });

    // Create a canvas
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
    document.body.appendChild(canvas);
    $(".containerImagePrint").html(canvas);

    // Create an image and draw the SVG onto the canvas
    var image = new Image;
    image.onload = function() {
        canvas.getContext('2d').drawImage(this, 0, 0, render_width, render_height);
    };
    image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  }
</script>

<script type="text/javascript">
// Create the chart
Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Grafik dan Tipe Kecerdasan'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: ''
    }

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  "series": [
    {
      "name": "Multiple Intellegency",
      "colorByPoint": true,
      "data": [
        @foreach($result_likerts as $result_likert)
          {
            "name": "{{$result_likert['multiple_intellegency_name']}}",
            "y": {{$result_likert['total']}},
          },
        @endforeach
      ]
    }
  ],
});

save_chart($('#container').highcharts());
save_chartPrint($('#container').highcharts());
$('#container').hide();
</script>


<script>
function preint() {
    window.print();
}
</script>
@endpush
