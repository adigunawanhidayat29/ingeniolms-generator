@extends('baper.template')
@section('title', 'Tes Baper')

@push('style')
  <link href="https://baperplus.ingenio.co.id/bundles/Professional_skeleton.css" rel="stylesheet">
@endpush

@section('content')


		<!-- /.item -->
		<div class="block empty yummy top-center" style="position: relative; padding-top: 0px; padding-bottom: 100px; background-color: rgb(255, 255, 255); background-image: none;">
			<div class="overly" style="background-color: transparent;">
			</div>
			<div class="container">
				<div data-component="grid" class="row">
					<div class="col-md-6">
						<div data-container="true">
							<div data-component="image">
								<img src="https://pagebuilder.ingenio.co.id/images/uploads/26/5bff759d07c26_333-transparent.png?0.924836483835678" class="" style="border-radius: 0px; border-color: rgb(48, 53, 62); border-style: none; border-width: 1px;" alt="image" />
							</div>
						</div>
					</div>
					<div class="col-md-6 text-center">
						<div data-container="true" style="margin: 25% 0">
							<div data-component="heading" data-content="true">
								<h1 class="text-left" style="font-size: 36px; color: rgb(255, 255, 255); margin: 20px 0px 15px;"></h1>
								<h1 class="text-center" style="font-size: 36px; color: rgb(87, 101, 116); margin: 20px 0px 0px;"><span style="font-family: Arial, Helvetica, sans-serif;">MASIH GALAU PILIH JURUSAN KULIAH ?</span></h1>
							</div>
							<div data-content="true" data-component="text" id="IXm5t">
								<p class="lead text-center" style="font-size: 22.4px; color: rgb(87, 101, 116); margin: 0px 0px 15px;"><span style="font-family: Arial, Helvetica, sans-serif;">Awas jangan asal pilih, menyesal lho... </span></p>
							</div>
							<div data-component="grid" class="text-center" data-nointent="true">
                <a href="/tesbaper/start/{{$baper_quiz->quiz_id}}" class="btn btn-primary btn-raised">Mulai Kuis Sekarang</a>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
@endsection

@push('script')
  <script type="text/javascript" src="bundles/Professional_skeleton.bundle.js"></script>
@endpush
