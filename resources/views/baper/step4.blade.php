@extends('baper.template')
@section('title', 'Tes Baper - Step 4')

@push('style')
  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
    .goto-link p{
      color: #333;
      margin-left: 0.5rem;
      margin-bottom: 0;
      overflow: hidden;
      min-height: 20px;
      display: -webkit-inline-box !important;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 1;
      white-space: normal;
      line-height: 1.5rem;
      width: 90%;
    }

    /* REPLACE */
    .pagination{
      display: block;
      font-size: 12px;
      margin: 0;
    }
    .pagination .page-item .page-link{
      padding: 8px;
    }
    .page-item:first-child .page-link, .page-item:last-child .page-link{
      padding: .5rem .75rem;
    }

    .checkbox label, .radio label, label{
      color: #333;
    }
    /* REPLACE */

    .m-0{
      margin: 0 !important;
    }

    .baper-header{
      padding: 1rem;
    }

    .card-baper{
      border-radius: 0 0 5px 5px;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }
      .card-baper .card-header{
        padding: 1rem;
        background: #f6f6f6;
        border-bottom: 1px solid #ececec;
        margin-bottom: 0;
        display: flex;
      }
        .card-baper .card-header .card-title{
          margin: 0;
          font-size: 16px;
          font-weight: 500;
          color: #333;
          display: -webkit-inline-box;
        }
          .card-baper .card-header .card-title p{
            margin-left: 0.5rem;
            margin-bottom: 0;
            width: 96%;
            line-height: 1.5;
            font-weight: 400;
          }
      .card-baper .card-block{
        padding: 1rem;
      }
    .card-framestep{
      border-radius: 0 0 5px 5px;
      background: #ffffff;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }
      .card-framestep .card-header{
        padding: 1rem;
        background: #f6f6f6;
        border-bottom: 1px solid #ececec;
        margin-bottom: 0;
        display: flex;
      }
        .card-framestep .card-header .card-title{
          margin: 0;
          font-size: 16px;
          font-weight: 400;
          color: #333;
          display: -webkit-inline-box;
        }
          .card-framestep .card-header .card-title p{
            margin-left: 0.5rem;
            margin-bottom: 0;
            width: 96%;
            line-height: 1.5;
            font-weight: 400;
          }
      .card-framestep .card-block{
        padding: 1rem;
      }
      .card-framestep .card-footer{
        padding: 0.5rem 1rem;
        background: #f6f6f6;
        border-top: 1px solid #ececec;
        margin-bottom: 0;
        display: block;
      }

    .baper-list-test{
      display: flex;
      width: 100%;
      height: auto;
    }
      .baper-list-test .number{
        margin: 0 1rem 0 0;
        text-align: center;
      }
        .baper-list-test .number .number-class{
          background: #f3f3f3;
          color: #333;
          padding: 10px;
          margin: 7px 0;
          border-radius: 5px;
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        }
      .baper-list-test .question{
        width: 100%;
        text-align: left;
      }
        .baper-list-test .question .question-class{
          list-style: none;
          cursor: move;
          background: #f3f3f3;
          color: #333;
          padding: 10px;
          margin: 7px 0;
          border-radius: 5px;
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        }
  </style>
@endpush

@section('content')
  <div class="wrap pt-2 pb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="card-framestep">
            <div class="card-header">
              <p class="card-title">Step 4</p>
            </div>
            <form id="myform">


            <div class="card-block no-p">
              <div class="col-md-12">
                {{-- <div class="baper-header">
                  <h2 class="headline-md"><span>{{$quiz_page_break->title}}</span></h2>
                  <p>{{$quiz_page_break->description}}</p>
                </div> --}}

                <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-8" role="tablist">
                  @foreach($quiz_page_breaks as $index => $quiz_page_break)
                    <li class="nav-item">
                      <a class="fw-600 nav-link withoutripple {{$index == '0' ? 'active' : ''}}" href="#clusters{{$index}}" aria-controls="{{$index}}" role="tab" data-toggle="tab" style="padding: 1.5rem 0;">
                        {{-- <span class="d-none d-sm-inline">{{$quiz_page_break->title}}</span> --}}
                        <span class="d-sm-none d-inline">{{$index + 1}}</span> <!-- Added -->
                        <span class="d-none d-sm-inline">Bag {{$index + 1}}</span>
                      </a>
                    </li>
                  @endforeach
                </ul>

              </div>


              <div class="col-md-12">
                <div id="question_content">
                  <div class="card-block">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        @php $buttoni = 1 @endphp
                        @foreach($quiz_page_breaks as $index => $quiz_page_break)
                          <div role="tabpanel" class="tab-pane fade {{$index == '0' ? 'show active' : ''}} " id="clusters{{$index}}">
                            <h2 class="headline-md"><span>{{$quiz_page_break->title}}</span></h2>
                            <p>{{$quiz_page_break->description}}</p>

                            <div id="question_content">
                              @php $i =1; @endphp
                              @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
                                <div class="card-baper" id="question_{{$quiz_question->id}}">
                                  <div class="card-header">
                                    <div class="card-title">
                                      <span class="badge">{{$i++}}</span> {!!$quiz_question->question!!}
                                    </div>
                                  </div>
                                  <div class="card-block bg_question_{{$quiz_question->id}}">
                                    @if($quiz_question->quiz_type_id == '7')
                                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                                        <div class="radio radio-primary form-inline mr-2">
                                          <label class="pl-5" style="color:#19191a" title="{{$question_answer->answer == '1' ? 'sangat tidak setuju' : ($question_answer->answer == '2' ? 'tidak setuju' : ($question_answer->answer == '3' ? 'netral' : ($question_answer->answer == '4' ? 'setuju' : 'sangat setuju')))}}">
                                            <input required id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">
                                            {!! $question_answer->answer !!}
                                          </label>
                                        </div>
                                      @endforeach
                                    @endif
                                  </div>

                                </div>

                              @endforeach
                            </div>
                            <div class="card-footer">
                              <div class="pull-left">
                                {{-- <a href="/tesbaper/step-1" class="btn btn-primary btn-raised">Sebelumnya</a> --}}
                              </div>
                              <div class="pull-right mt-2">
                                @if($buttoni > 1 && $buttoni <= 8)
                                  <button type="button" class="btn btn-default prev-step btn-raised">Sebelumnya</button>
                                @endif

                                @if($buttoni >= 1 && $buttoni <= 7)
                                  <button type="button" class="btn btn-primary next-step btn-raised">Berikutnya</button>
                                @endif

                                @if($buttoni == 8)
                                  <a href="#/" onclick="nextPage()" class="btn btn-primary btn-raised nextPage">Simpan dan Lanjutkan</a>
                                @endif
                              </div>
                            </div>

                          </div>



                        @php $buttoni++ @endphp
                        @endforeach
                      </div>
                  </div>

                </div>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection


@push('script')
  <script type="text/javascript">
    $('#sidebarCall').click(function(){
      $('.sidebar').toggleClass('show');
      // $(this).hide();
    });
    $('#sidebarCallClose').click(function(){
      $('.sidebar').toggleClass('show');
      // $('#sidebarCall').show();
    });
  </script>

  {{-- <script src="/js/plugins.min.js"></script> --}}
  {{-- <script src="/js/app.min.js"></script> --}}
  {{-- <script src="/js/portfolio.js"></script> --}}
  {{-- <script src="/js/jquery.js"></script> --}}
  <script src="/js/bootstrap.min.js"></script>

  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  {{-- finish quiz --}}
  <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($all_quiz_page_breaks as $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
          @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]:checked').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
            // check if answer is null
            if(answer_checked == null){
              answer_checked = 0;
            }else{
              answer_checked = answer_checked;
            }
            // check if answer is null
          @elseif($quiz_question->quiz_type_id == '3')
            var answer_checked = $('input[name=answer_ordering{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'questionOrdering'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '5')
            var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '6')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @endif

          // answers.push(answer_checked);
          answers.push(session_value);
          questions.push('{{$quiz_question->id}}');
          question_type.push('{{$quiz_question->quiz_type_id}}');
      @endforeach
    @endforeach

      // console.log(answers);

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('tesbaper/finish/'.$quiz->id)}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          BaperCode : get_session_answer('BaperCode'),
          _token: token
        },
        beforeSend: function(){

          swal({
            title: "Sedang memproses",
            text: "Jawaban Anda sedang kami proses",
            icon: "info",
            showCancelButton: false,
            showConfirmButton: false
          })

          $(".nextPage").prop('disabled', true);
          $(".nextPage").html('Memproses Hasil...')
        },
        success : function(result){
          //remove session answer
          @foreach($all_quiz_page_breaks as $quiz_page_break)
            @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
              localStorage.removeItem('answer{{$quiz_question->id}}');
              localStorage.removeItem('questionOrdering{{$quiz_question->id}}');
            @endforeach
          @endforeach
          localStorage.removeItem('BaperCode');
          //remove session answer

          window.location.assign('{{url('tesbaper/result/'.$quiz->id)}}');
        },
      });
    }

    $("#finish_quiz").click(function(){

        swal({
          title: "Apakah Anda Yakin?",
          text: "Jawaban Anda akan kami proses!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSave) => {
          if (willSave) {
            quiz_finish();
          } else {
            swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
          }
        });

    })

    function readyFinish(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    }

  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
  @foreach($all_quiz_page_breaks as $quiz_page_break)
    @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
      var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');

      @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}][value='"+session_value+"']").attr("checked","checked");
        }
      @elseif($quiz_question->quiz_type_id == '3')
        if(session_value !== null){
          $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '5')
        if(session_value !== null){
          $("textarea[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @elseif($quiz_question->quiz_type_id == '6')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @endif
    @endforeach
  @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}

  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script src="/jquery-ui/jquery.ui.touch-punch.min.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
          $('.answerNumber').each(function (i) {
              var humanNum = i + 1;
              $(this).html(humanNum + '');
          });
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}

  {{-- <script type="text/javascript">
    function nextPage(){

      var next = false;

      @foreach($quiz_page_breaks as $index => $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
          @if($quiz_question->quiz_type_id == 7)

            if(!get_session_answer('{{'answer'.$quiz_question->id}}')){

              // $([document.documentElement, document.body]).animate({
              //     scrollTop: $("#question_{{$quiz_question->id}}").offset().top
              // }, 200);

              $(".bg_question_{{$quiz_question->id}}").addClass("bg-danger").delay(5000).queue(function(){
                  $(this).removeClass("bg-danger").dequeue();
              });

              next = false;
            }else{
              next = true;
            }

          @endif
        @endforeach
      @endforeach

      //console.log(next);

      if(next === true){
         readyFinish()
      }else{
        $('.nav-tabs > .nav-item > .active').parent().next('li').find('a').trigger('click');
      }

    }
  </script> --}}

  <script type="text/javascript" src="/jquery-validate/jquery.validate.min.js"></script>

  <script type="text/javascript">
    var validator = $('#myform').validate({
      errorClass: "bg-danger",
      errorElement: "div",
    });

    function nextPage(){

      if($("#myform").valid()){   // test for validity
           // do stuff if form is valid
           readyFinish()
       } else {
           // do stuff if form is not valid
           validator.focusInvalid();
       }

    }

    // localStorage.clear();
  </script>

  <script type="text/javascript">
      $(document).ready(function () {
      //Initialize tooltips
      // $('.nav-tabs > li a[title]').tooltip();

      //Wizard
      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

          var $target = $(e.target);

          if ($target.parent().hasClass('disabled')) {
              return false;
          }
      });

      $(".next-step").click(function (e) {

        if($("#myform").valid()){   // test for validity
             // do stuff if form is valid
             var $active = $('.nav-tabs > .nav-item > .active');
             // console.log($active);
             $active.parent().next('li').find('a').removeClass('disabled');
             nextTab($active);
         } else {
             // do stuff if form is not valid
             validator.focusInvalid();
         }

      });

      $(".prev-step").click(function (e) {

          var $active = $('.nav-tabs > .nav-item > .active');
          prevTab($active);

      });
    });

    function nextTab(elem) {
      $(elem).parent().next('li').find('a').trigger('click');
    }
    function prevTab(elem) {
      $(elem).parent().prev('li').find('a').trigger('click');
    }
</script>
@endpush
