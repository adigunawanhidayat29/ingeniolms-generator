@extends('baper.template')
@section('title', 'Laporan Tes Baper')

@push('style')
  <style media="screen">
  @media all {
  .page-break { display: none; }
  }

  *{
  font-family: Roboto,sans-serif;
  }

  .report{
  background: #fff;
  width: 100%;
  padding: 10rem;
  }
  .report-header{
  position: relative;
  /* background: #ff0; */
  background: #00b050;
  padding: 1rem 2rem;
  text-align: center;
  }
  .report-header h2{
  color: #fff;
  font-size: 14px;
  font-weight: 600;
  margin: 0;
  }
  .report-title{
  position: relative;
  width: 100%;
  margin: 3rem 0;
  }
  .report-title img{
  position: absolute;
  width: 100px;
  right: 0;
  }
  .report-title p{
  font-size: 14px;
  }
  .report-title h2{
  margin: 0 0 1rem 0;
  font-weight: 600;
  }
  .report-body{

  }
  .report-body .participant{
  margin: 3rem 0;
  text-align: center;
  }
  .report-title .participant p{
  font-size: 14px;
  }
  .report-body .participant h2{
  margin: 0 0 1rem 0;
  font-size: 5rem;
  font-weight: 400;
  border-bottom: 2px solid #f3f3f3;
  width: 400px;
  margin-left: auto;
  margin-right: auto;
  }
  .report-body .cluster{
  margin: 3rem 0;
  }
  .cluster-table{
  width: 100%;
  }
  .cluster-table tr th, .cluster-table tr td{
  padding: 1rem 3rem;
  border: 1px solid #e9ecef;
  }
  </style>
  <style media="screen">

  .report{
  background: #fff;
  width: 100%;
  padding: 2rem;
  }
  .report-header{
  position: relative;
  /* background: #ff0; */
  background: #00b050;
  padding: 1rem 2rem;
  margin: 0;
  text-align: center;
  }
  .report-header h2{
  color: #fff;
  font-size: 14px;
  font-weight: 600;
  margin: 0;
  }
  .report-title{
  position: relative;
  width: 100%;
  margin: 3rem 0;
  }
  .report-title img{
  position: absolute;
  width: 100px;
  right: 0;
  }
  .report-title p{
  font-size: 14px;
  }
  .report-title h2{
  /* margin: 0 0 1rem 0; */
  margin: 0;
  font-weight: 600;
  }
  .report-body{

  }
  .report-body .grade{
  margin: 5rem 0;
  text-align: center;
  }
    .report-body .grade h3{
    margin: 0;
    }
  .report-body .participant{
  /* margin: 3rem 0; */
  margin: 0;
  text-align: center;
  }
  .report-title .participant p{
  font-size: 14px;
  }
  .report-body .participant h2{
  margin: 0 0 1rem 0;
  font-size: 5rem;
  font-weight: 400;
  border-bottom: 2px solid #f3f3f3;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  }
  .report-body .cluster{
  margin: 1rem 0;
  }
  .cluster-table{
  width: 100%;
  }
  .cluster-table tr th, .cluster-table tr td{
  padding: 0.5rem;
  border: 1px solid #e9ecef;
  font-size: 14px;
  }

  </style>

  <style type="text/css">

    #printable { display: none; }

    @media print
    {
        #non-printable { display: none; }
        #printable { display: block; width: 100%}
        .page-break { display: block; page-break-before: always; }
    }
    </style>

@endpush

@section('content')

  <div class="container" id="non-printable">
    {{-- <a id="ignorePDF" target="_blank" href="/tesbaper/download/report/{{Request::segment(3)}}/{{Request::segment(4)}}" class="btn btn-danger btn-raised">Download Laporan PDF</a> --}}
    <a id="ignorePDF" href="#/" onclick="print()" class="btn btn-info btn-raised">Print Laporan</a>
  </div>


  @foreach($BaperQuizCodes as $index_baperCode => $BaperQuizCode)

    @php

    $QuizParticipants = \App\QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
      ->where('quiz_participants.quiz_id', 128)
      ->where('quiz_participants.user_id', $BaperQuizCode->user_id)
      ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
      ->join('quiz_participants','quiz_participants.id','=','quiz_participant_answers.quiz_participant_id')
      ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
      ->join('baper_quizzes','baper_quizzes.quiz_id','=','quiz_questions.quiz_id')
      ->get();

    $Quiz = \App\Quiz::find($QuizParticipants->first()->quiz_id);

    // === get data clusters
    $answers_clusters = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '3'){

          $sum_number = 0;
          DB::statement(DB::raw('set @rownum=0'));
          $question_answers = DB::table('quiz_question_answers')
            ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
            ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
            ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
            ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
            ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
            ->get();
            foreach($question_answers as $index => $question_answer){
              array_push($answers_clusters, [
                'total' => $index + 1,
                'cluster' => $question_answer->cluster_name,
                'cluster_id' => $question_answer->cluster_id,
              ]);
            }
      }
    }
    // dd($total_clusters);
    // === get data clusters

    //===  grouping by cluster id
    $group_clusters = array();
    foreach($answers_clusters as $key => $item){
      $group_clusters[$item['cluster_id']][] = $item;
    }
    // dd($group_clusters);
    //===  grouping by cluster id

    // === totaling answer cluster
    $result_clusters = [];
    for($i=1; $i<=14; $i++){
      $total = 0;
      for($j=0; $j<=2; $j++){
        $total += $group_clusters[$i][$j]['total'];
      }
      array_push($result_clusters, [
        'cluster_id' => $group_clusters[$i][0]['cluster_id'],
        'cluster' => $group_clusters[$i][0]['cluster'],
        'total' => $total,
      ]);
      // var_dump($arr[$i]);
    }
    // === totaling answer cluster

    //=== sort by total asc
    usort($result_clusters, function($first_value, $second_value) {
        return $first_value['total'] <=> $second_value['total'];
    });
    // dd($result_clusters);
    //=== sort by total asc

    $answers_majors = [];
    foreach($result_clusters as $index => $value){
      if($index < 4){
        foreach($QuizParticipants as $index => $quiz_participant){
          if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2'){
            $major = DB::table('baper_cluster_major_answers')
              ->select('baper_cluster_major_answers.id', 'baper_cluster_majors.major_name')
              ->join('baper_cluster_majors', 'baper_cluster_majors.id', '=', 'baper_cluster_major_answers.cluster_major_id')
              ->where('baper_cluster_majors.cluster_id', $value['cluster_id'])
              ->where('quiz_answer_id', $quiz_participant->quiz_question_answer_id)
              ->first();
              if($major){
                array_push($answers_majors, ['major_name' => $major->major_name]);
                // var_dump($major->major_name);
              }
          }
        }
      }
    }
    // dd($answers_majors);

    $answers_likert = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '7'){
        $multiple_intellegencies = DB::table('baper_multiple_intellegencies')->select('baper_multiple_intellegencies.*')
          ->join('baper_multiple_intellegency_questions', 'baper_multiple_intellegency_questions.multiple_intellegency_id', '=', 'baper_multiple_intellegencies.id')
          ->where('baper_multiple_intellegency_questions.question_id', $quiz_participant->quiz_question_id)
          ->groupBy('baper_multiple_intellegencies.id')
          ->first();
        if($multiple_intellegencies){
          array_push($answers_likert, [
            'multiple_intellegency_id' => $multiple_intellegencies->id,
            'multiple_intellegency_name' => $multiple_intellegencies->name,
            'answer' => $quiz_participant->answer,
            'answer_id' => $quiz_participant->quiz_question_answer_id,
            'question_id' => $quiz_participant->quiz_question_id
          ]);
          // var_dump($major->major_name);
        }
      }
    }
    // dd($answers_likert);
    // $sum_answers = 0;
    $group_likerts = array();
    foreach($answers_likert as $data){
      $group_likerts[$data['multiple_intellegency_id']][] = $data;
    }
    // dd($group_likerts);
    $result_likerts = [];
    for($i=1; $i<=8; $i++){
      // dd( $group_likerts[$i]);
      $total = 0;
      for($j=0; $j<=9; $j++){
        $total += $group_likerts[$i][$j]['answer'];
      }
      array_push($result_likerts, [
        'multiple_intellegency_id' => $group_likerts[$i][0]['multiple_intellegency_id'],
        'multiple_intellegency_name' => $group_likerts[$i][0]['multiple_intellegency_name'],
        'total' => $total,
      ]);
    }
    // dd($result_likerts);

    // get biodata
    $biodata = [];
    foreach($QuizParticipants as $index => $quiz_participant){
      if($quiz_participant->quiz_type_id == '6'){
        array_push($biodata, [
          'data' => $quiz_participant->answer_short_answer
        ]);
      }
    }
    // get biodata

    @endphp
    <div class="wrap pt-2 pb-2 mt-2 mb-2">
      <div class="container">
        <div class="row" id="non-printable">
          <div class="col-md-12">

            <!-- REPORT -->
            <div class="report">
              <div class="report-header">
                <h2>BAPER PLUS</h2>
              </div>
              <div class="report-title">
                <img src="/baper-assets/logo.jpg" alt="baperplus">
                <p>Learning Consultant</p>
                <h2>BEFA Institute</h2>
                <p>Better Education For All</p>
              </div>
              <div class="report-body">
                <div class="participant">
                  <p>diberikan kepada:</p>
                  <h3>{{$biodata[0]['data']}}</h3>
                  <p>Berasal dari sekolah {{$biodata[1]['data']}}</p>
                </div>

                <div class="cluster">
                  <table class="cluster-table" style="width:100%">
                    <tr class="active" style="background:#f3f3f3">
                      <th>Rekomendasi Jurusan</th>
                      <th>Urutan Cluster Kamu</th>
                    </tr>
                    <tr>
                      <td>
                        @foreach($answers_majors as $index => $value)
                          {{$index + 1}}. {{$value['major_name']}} <br>
                        @endforeach
                      </td>
                      <td>
                        @foreach($result_clusters as $index => $value)
                          {{-- @if($index < 4) --}}
                            {{$index + 1}}. {{$value['cluster']}} <br>
                          {{-- @endif --}}
                        @endforeach
                      </td>
                    </tr>
                  </table>
                </div>

                <div class="grade">
                  <h3>Tipe Kecerdasan</h3>
                  <div class="cluster">
                    <div id="container{{$index_baperCode}}"></div>
                    <div class="containerImage{{$index_baperCode}}"></div>
                  </div>
                </div>

              </div>
            </div>
            <!-- END REPORT -->


          </div>
        </div>


        <div class="row">
          <div id="printable">
            <div class="report">
              {{-- <div class="report-header">
                <h2>BAPER PLUS</h2>
              </div>
              <div class="report-title">
                <img src="/baper-assets/logo.jpg" alt="baperplus">
                <p>Learning Consultant</p>
                <h2>BEFA Institute</h2>
                <p>Better Education For All</p>
              </div> --}}
              <div class="report-body">
                <div class="participant">
                  <p>diberikan kepada:</p>
                  <h3>{{$biodata[0]['data']}}</h3>
                  <p>Berasal dari sekolah {{$biodata[1]['data']}}</p>
                </div>

                <div class="cluster">
                  <table class="cluster-table" style="width:100% !important">
                    <tr class="active" style="background:#f3f3f3">
                      <th>Rekomendasi Jurusan</th>
                      <th>Urutan Cluster Kamu</th>
                    </tr>
                    <tr>
                      <td>
                        @foreach($answers_majors as $index => $value)
                          {{$index + 1}}. {{$value['major_name']}} <br>
                        @endforeach
                      </td>
                      <td>
                        @foreach($result_clusters as $index => $value)
                          {{-- @if($index < 4) --}}
                            {{$index + 1}}. {{$value['cluster']}} <br>
                          {{-- @endif --}}
                        @endforeach
                      </td>
                    </tr>
                  </table>
                </div>

                <div class="grade">
                  <h3>Tipe Kecerdasan</h3>
                  <div class="cluster">
                    <div class="containerImagePrint{{$index_baperCode}}"></div>
                  </div>
                </div>

              </div>
            </div>
            <div class="page-break"></div>
          </div>

        </div>

      </div>
    </div>
  @endforeach
@endsection

@push('script')

<script src="/highcharts/code/highcharts.js"></script>
<script src="/highcharts/code/modules/data.js"></script>
<script src="/highcharts/code/modules/drilldown.js"></script>
<script src="/highcharts/code/modules/exporting.js"></script>

@foreach($BaperQuizCodes as $index_baperCode => $BaperQuizCode)

  @php

  $QuizParticipants = \App\QuizParticipantAnswer::select('quiz_participant_answers.*','quiz_questions.*','quiz_question_answers.*')
    ->where('quiz_participants.quiz_id', 128)
    ->where('quiz_participants.user_id', $BaperQuizCode->user_id)
    ->join('quiz_question_answers','quiz_question_answers.id','=','quiz_participant_answers.quiz_question_answer_id','left')
    ->join('quiz_participants','quiz_participants.id','=','quiz_participant_answers.quiz_participant_id')
    ->join('quiz_questions','quiz_questions.id','=','quiz_participant_answers.quiz_question_id')
    ->join('baper_quizzes','baper_quizzes.quiz_id','=','quiz_questions.quiz_id')
    ->get();

  $Quiz = \App\Quiz::find($QuizParticipants->first()->quiz_id);

  // === get data clusters
  $answers_clusters = [];
  foreach($QuizParticipants as $index => $quiz_participant){
    if($quiz_participant->quiz_type_id == '3'){

        $sum_number = 0;
        DB::statement(DB::raw('set @rownum=0'));
        $question_answers = DB::table('quiz_question_answers')
          ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
          ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
          ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
          ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
          ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
          ->get();
          foreach($question_answers as $index => $question_answer){
            array_push($answers_clusters, [
              'total' => $index + 1,
              'cluster' => $question_answer->cluster_name,
              'cluster_id' => $question_answer->cluster_id,
            ]);
          }
    }
  }
  // dd($total_clusters);
  // === get data clusters

  //===  grouping by cluster id
  $group_clusters = array();
  foreach($answers_clusters as $key => $item){
    $group_clusters[$item['cluster_id']][] = $item;
  }
  // dd($group_clusters);
  //===  grouping by cluster id

  // === totaling answer cluster
  $result_clusters = [];
  for($i=1; $i<=14; $i++){
    $total = 0;
    for($j=0; $j<=2; $j++){
      $total += $group_clusters[$i][$j]['total'];
    }
    array_push($result_clusters, [
      'cluster_id' => $group_clusters[$i][0]['cluster_id'],
      'cluster' => $group_clusters[$i][0]['cluster'],
      'total' => $total,
    ]);
    // var_dump($arr[$i]);
  }
  // === totaling answer cluster

  //=== sort by total asc
  usort($result_clusters, function($first_value, $second_value) {
      return $first_value['total'] <=> $second_value['total'];
  });
  // dd($result_clusters);
  //=== sort by total asc

  $answers_majors = [];
  foreach($result_clusters as $index => $value){
    if($index < 4){
      foreach($QuizParticipants as $index => $quiz_participant){
        if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2'){
          $major = DB::table('baper_cluster_major_answers')
            ->select('baper_cluster_major_answers.id', 'baper_cluster_majors.major_name')
            ->join('baper_cluster_majors', 'baper_cluster_majors.id', '=', 'baper_cluster_major_answers.cluster_major_id')
            ->where('baper_cluster_majors.cluster_id', $value['cluster_id'])
            ->where('quiz_answer_id', $quiz_participant->quiz_question_answer_id)
            ->first();
            if($major){
              array_push($answers_majors, ['major_name' => $major->major_name]);
              // var_dump($major->major_name);
            }
        }
      }
    }
  }
  // dd($answers_majors);

  $answers_likert = [];
  foreach($QuizParticipants as $index => $quiz_participant){
    if($quiz_participant->quiz_type_id == '7'){
      $multiple_intellegencies = DB::table('baper_multiple_intellegencies')->select('baper_multiple_intellegencies.*')
        ->join('baper_multiple_intellegency_questions', 'baper_multiple_intellegency_questions.multiple_intellegency_id', '=', 'baper_multiple_intellegencies.id')
        ->where('baper_multiple_intellegency_questions.question_id', $quiz_participant->quiz_question_id)
        ->groupBy('baper_multiple_intellegencies.id')
        ->first();
      if($multiple_intellegencies){
        array_push($answers_likert, [
          'multiple_intellegency_id' => $multiple_intellegencies->id,
          'multiple_intellegency_name' => $multiple_intellegencies->name,
          'answer' => $quiz_participant->answer,
          'answer_id' => $quiz_participant->quiz_question_answer_id,
          'question_id' => $quiz_participant->quiz_question_id
        ]);
        // var_dump($major->major_name);
      }
    }
  }
  // dd($answers_likert);
  // $sum_answers = 0;
  $group_likerts = array();
  foreach($answers_likert as $data){
    $group_likerts[$data['multiple_intellegency_id']][] = $data;
  }
  // dd($group_likerts);
  $result_likerts = [];
  for($i=1; $i<=8; $i++){
    // dd( $group_likerts[$i]);
    $total = 0;
    for($j=0; $j<=9; $j++){
      $total += $group_likerts[$i][$j]['answer'];
    }
    array_push($result_likerts, [
      'multiple_intellegency_id' => $group_likerts[$i][0]['multiple_intellegency_id'],
      'multiple_intellegency_name' => $group_likerts[$i][0]['multiple_intellegency_name'],
      'total' => $total,
    ]);
  }
  // dd($result_likerts);

  // get biodata
  $biodata = [];
  foreach($QuizParticipants as $index => $quiz_participant){
    if($quiz_participant->quiz_type_id == '6'){
      array_push($biodata, [
        'data' => $quiz_participant->answer_short_answer
      ]);
    }
  }
  // get biodata

  @endphp

<script type="text/javascript">
  EXPORT_WIDTH = 1000;

  function save_chart(chart) {
    var render_width = EXPORT_WIDTH;
    var render_height = render_width * chart.chartHeight / chart.chartWidth

    // Get the cart's SVG code
    var svg = chart.getSVG({
        exporting: {
            sourceWidth: chart.chartWidth,
            sourceHeight: chart.chartHeight
        }
    });

    // Create a canvas
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
    document.body.appendChild(canvas);
    $(".containerImage{{$index_baperCode}}").html(canvas);

    // Create an image and draw the SVG onto the canvas
    var image = new Image;
    image.onload = function() {
        canvas.getContext('2d').drawImage(this, 0, 0, render_width, render_height);
    };
    image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  }

  function save_chartPrint(chart) {
    var render_width = EXPORT_WIDTH;
    var render_height = render_width * chart.chartHeight / chart.chartWidth

    // Get the cart's SVG code
    var svg = chart.getSVG({
        exporting: {
            sourceWidth: chart.chartWidth,
            sourceHeight: chart.chartHeight
        }
    });

    // Create a canvas
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
    document.body.appendChild(canvas);
    $(".containerImagePrint{{$index_baperCode}}").html(canvas);

    // Create an image and draw the SVG onto the canvas
    var image = new Image;
    image.onload = function() {
        canvas.getContext('2d').drawImage(this, 0, 0, render_width, render_height);
    };
    image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  }
</script>

<script type="text/javascript">
// Create the chart
Highcharts.chart('container{{$index_baperCode}}', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Grafik dan Tipe Kecerdasan'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: ''
    }

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
  },

  "series": [
    {
      "name": "Multiple Intellegency",
      "colorByPoint": true,
      "data": [
        @foreach($result_likerts as $result_likert)
          {
            "name": "{{$result_likert['multiple_intellegency_name']}}",
            "y": {{$result_likert['total']}},
          },
        @endforeach
      ]
    }
  ],
});

save_chart($('#container{{$index_baperCode}}').highcharts());
save_chartPrint($('#container{{$index_baperCode}}').highcharts());
$('#container{{$index_baperCode}}').hide();
</script>

@endforeach

<script>
function preint() {
    window.print();
}
</script>
@endpush
