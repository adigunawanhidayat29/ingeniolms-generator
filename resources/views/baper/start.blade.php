@extends('baper.template')

@section('title', $quiz->name)

@push('style')

  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
    .goto-link p{
      color: #333;
      margin-left: 0.5rem;
      margin-bottom: 0;
      overflow: hidden;
      min-height: 20px;
      display: -webkit-inline-box !important;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 1;
      white-space: normal;
      line-height: 1.5rem;
      width: 90%;
    }

    /* REPLACE */
    .pagination{
      display: block;
      font-size: 12px;
      margin: 0;
    }
    .pagination .page-item .page-link{
      padding: 8px;
    }
    .page-item:first-child .page-link, .page-item:last-child .page-link{
      padding: .5rem .75rem;
    }

    .checkbox label, .radio label, label{
      color: #333;
    }

    .answerNumber{
      margin-right: 1rem;
    }
    /* REPLACE */

    .m-0{
      margin: 0 !important;
    }

    .baper-header{
      padding: 1rem;
    }

    .card-baper{
      border-radius: 0 0 5px 5px;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      margin-bottom: 2rem;
      margin-bottom: 20px;
    }
      .card-baper .card-header{
        padding: 1rem;
        background: #f6f6f6;
        border-bottom: 1px solid #ececec;
        margin-bottom: 0;
        display: flex;
      }
        .card-baper .card-header .card-title{
          margin: 0;
          font-size: 16px;
          font-weight: 500;
          color: #333;
          display: -webkit-inline-box;
        }
          .card-baper .card-header .card-title p{
            margin-left: 0.5rem;
            margin-bottom: 0;
            width: 96%;
            line-height: 1.5;
            font-weight: 400;
          }
      .card-baper .card-block{
        padding: 1rem;
      }

    .baper-list-test{
      display: flex;
      width: 100%;
      height: auto;
    }
      .baper-list-test .number{
        margin: 0 1rem 0 0;
        text-align: center;
      }
        .baper-list-test .number .number-class{
          background: #f3f3f3;
          color: #333;
          padding: 10px;
          margin: 7px 0;
          border-radius: 5px;
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        }
      .baper-list-test .question{
        width: 100%;
        text-align: left;
      }
        .baper-list-test .question .question-class{
          list-style: none;
          cursor: move;
          background: #f3f3f3;
          color: #333;
          padding: 10px;
          margin: 7px 0;
          border-radius: 5px;
          box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        }
  </style>
  <style media="screen">
    .sidebar-overlay{
      background: rgba(0, 0, 0, 0.3);
      position: fixed;
      display: block;
      width: 100%;
      height: 100%;
      top: 0;
      padding: 0;
      z-index: 99;
    }
    .sidebar{
      position: fixed;
      display: block;
      background: #fff;
      width: 15%;
      height: 100%;
      top: 0;
      right: -20%;
      padding: 0;
      list-style: none;
      box-shadow: -2px 0 10px rgba(0, 0, 0, 0.3);
      transition: all ease 0.75s;
      z-index: 999;
    }
      .sidebar.show{
        right: 0;
      }
      .sidebar .sidebar-header{
        position: relative;
        top: 0;
        left: 0;
        background: #4ca3d9;
        width: 100%;
        height: auto;
        /* padding: 1rem; */
        padding: 2rem;
        margin: 0 0 1rem 0;
        text-align: center;
      }
        .sidebar .sidebar-header *{
          color: #fff;
          font-size: 16px;
          margin: 0;
        }
        .sidebar .sidebar-header>.btn-close{
          background: #4697ca;
          position: absolute;
          top: 0;
          left: 0;
          /* line-height: 1.45;
          padding: 0.5rem 1rem; */
          padding: 1.65rem;
          font-size: 20px;
          color: #fff !important;
          cursor: pointer;
        }
      .sidebar .sidebar-menu{
        width: 100%;
      }
        .sidebar .sidebar-menu .sidebar-menu-item{
          display: block;
          width: 100%;
          margin: 0 0 1rem 0;
          padding: 1rem;
        }
        .sidebar .sidebar-menu a{
          display: block;
          padding: 0.5rem 1rem;
          font-size: 12px;
        }
      .sidebar .sidebar-submenu{
        padding: 0;
      }
      .sidebar .sidebar-submenu .dropdown{
        width: 100%;
        list-style: none;
        display: none;
      }
        .sidebar .sidebar-submenu .dropdown a{
          background: #f3f3f3;
          display: block;
          padding: 0.5rem 0.5rem 0.5rem 2.5rem;
          font-size: 16px;
        }
      .sidebar .sidebar-menu a:hover{
        background: #f3f3f3;
      }
      .sidebar .sidebar-submenu a:hover{
        background: #ececec;
      }
      .btn-call{
        position: fixed;
        top: 30%;
        right: -2%;
        display: block;
        background: #4697ca;
        color: #fff !important;
        padding: 1.65rem;
        border-radius: 5px 5px 0 0;
        font-size: 20px;
        cursor: pointer;
        transform: rotate(-90deg);
        transition: all ease 0.3s;
      }
        .btn-call:before{
          display: block;
          content: "Go to page";
        }
        .btn-call i{
          display: none;
        }
        .btn-call:hover{
          box-shadow: -2px -2px 5px rgba(0, 0, 0, 0.3);
        }

      @media (max-width: 1366px){
        .sidebar{
          width: 25%;
          right: -30%;
        }

        .btn-call{
          position: fixed;
          top: 30%;
          right: -2.5%;
          display: block;
          background: #4697ca;
          color: #fff !important;
          padding: 1.65rem;
          border-radius: 5px 5px 0 0;
          font-size: 20px;
          cursor: pointer;
          transform: rotate(-90deg);
          transition: all ease 0.3s;
        }
          .btn-call:before{
            display: block;
            content: "Go to page";
          }
          .btn-call i{
            display: none;
          }
          .btn-call:hover{
            box-shadow: -2px -2px 5px rgba(0, 0, 0, 0.3);
          }
      }

      @media (max-width: 480px){
        .sidebar{
          width: 85%;
          right: -90%;
        }

        .btn-call{
          position: fixed;
          top: 5%;
          right: 0%;
          display: block;
          background: #4697ca;
          color: #fff !important;
          padding: 1.65rem;
          border-radius: 5px 0 0 5px;
          font-size: 20px;
          cursor: pointer;
          transform: rotate(0deg);
        }
          .btn-call:before{
            display: none;
            content: "Go to page";
          }
          .btn-call i{
            display: block;
          }
      }
  </style>
@endpush

@section('content')
  <div class="wrap pt-2 pb-2 bg-white" style="min-height:100vh">
    <div class="container">
      <div class="row">
          @foreach($quiz_page_breaks as $quiz_page_break)
          <div class="col-md-12">
            <div class="baper-header">
              <h3 class="headline-md">{{$quiz_page_break->title}}</h3>
              <p>{{$quiz_page_break->description}}</p>
            </div>
          </div>
          <div class="col-md-12">
            <div id="question_content">
              @php $i =1; @endphp
              @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
                <div class="card-baper" id="question_{{$quiz_question->id}}">
                  <div class="card-header">
                    <div class="card-title">
                      <span class="badge">{{$i++}}</span> {!!$quiz_question->question!!}
                    </div>
                  </div>
                  <div class="card-block">
                    @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2')
                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                        <div class="radio radio-primary">
                          <label>
                            <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}"> {!!$question_answer->answer!!}
                          </label>
                        </div>
                      @endforeach
                    @elseif($quiz_question->quiz_type_id == '7')
                      @foreach($quiz_question->quiz_question_answers as $question_answer)
                        <div class="radio radio-primary form-inline mr-2">
                          <label class="pl-5" style="color:#19191a" title="{{$question_answer->answer == '1' ? 'sangat tidak setuju' : ($question_answer->answer == '2' ? 'tidak setuju' : ($question_answer->answer == '3' ? 'netral' : ($question_answer->answer == '4' ? 'setuju' : 'sangat setuju')))}}">
                            <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">
                            {!! $question_answer->answer !!}
                          </label>
                        </div>
                      @endforeach
                    @elseif($quiz_question->quiz_type_id == '3')

                        <div class="baper-list-test">
                          <div class="question">
                            <div class="SectionSortable" question-id="{{$quiz_question->id}}">
                              @foreach($quiz_question->quiz_question_answers as $index => $question_answer)
                                <li class="question-class" data-id="{{$question_answer->id}}">
                                  <p class="m-0"> <span class="answerNumber badge-pill badge-pill-square badge-pill-primary">{{$index+1}}</span> <i class="move fa fa-arrows-v"></i> {!!$question_answer->answer!!} ({{$index+1}})</p>
                                </li>
                              @endforeach
                              <input type="hidden" name="answer_ordering{{$quiz_question->id}}" value="" id="questionOrdering{{$quiz_question->id}}">
                            </div>
                          </div>
                        </div>


                    @elseif($quiz_question->quiz_type_id == '5')
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <textarea onkeyup="save_session_answer(this)" style="border:1px solid #bbb; height:100px; padding:7px" name="answer{{$quiz_question->id}}" id="answer_id_{{$question_answer->id}}" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>
                    @elseif($quiz_question->quiz_type_id == '6')
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input onkeyup="save_session_answer(this)" style="border:1px solid #bbb" name="answer{{$quiz_question->id}}" id="answer_id_{{$quiz_question->id}}" type="text" class="form-control">
                          </div>
                        </div>
                      </div>
                    @endif
                  </div>
                </div>
              @endforeach
            </div>
          @endforeach

          <!--Pagination Wrap Start-->
          {{-- <div class="text-center">
            {{$quiz_page_breaks->links('pagination.default')}}
          </div> --}}
          <!--Pagination Wrap End-->
          <nav aria-label="Page navigation">
            <ul class="pager d-flex justify-content-between">
              <li class="page-item">
                <a class="page-link" href="javascript:void(0)"><span aria-hidden="true">«</span> Previous</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="javascript:void(0)">Next <span aria-hidden="true">»</span></a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <a id="sidebarCall" class="btn-call">
      <i class="fa fa-angle-left"></i>
    </a>
    <div class="sidebar">
      {{-- <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Waktu Anda</h3>
        </div>
        <div class="card-block">
          <div id="timer"></div>
        </div>
      </div> --}}
      <div class="sidebar-header">
        <a id="sidebarCallClose" class="btn-close">
          <i class="fa fa-times"></i>
        </a>
        <h3 class="headline-md">Go to Page</h3>
      </div>
      <div class="sidebar-menu">
        <div class="sidebar-menu-item">
          {{$quiz_page_breaks->links('pagination.default')}}
        </div>
        <br><br>
        <div class="sidebar-menu-item">
          <button id="finish_quiz" type="button" class="btn btn-primary btn-block btn-raised">Finish Quiz</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    $('#sidebarCall').click(function(){
      $('.sidebar').toggleClass('show');
      // $(this).hide();
    });
    $('#sidebarCallClose').click(function(){
      $('.sidebar').toggleClass('show');
      // $('#sidebarCall').show();
    });
  </script>

  {{-- <script src="/js/plugins.min.js"></script> --}}
  {{-- <script src="/js/app.min.js"></script> --}}
  {{-- <script src="/js/portfolio.js"></script> --}}
  {{-- <script src="/js/jquery.js"></script> --}}
  <script src="/js/bootstrap.min.js"></script>

  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  {{-- finish quiz --}}
  <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      var question_type = [];

      @foreach($all_quiz_page_breaks as $quiz_page_break)
        @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
          @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]:checked').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
            // check if answer is null
            if(answer_checked == null){
              answer_checked = 0;
            }else{
              answer_checked = answer_checked;
            }
            // check if answer is null
          @elseif($quiz_question->quiz_type_id == '3')
            var answer_checked = $('input[name=answer_ordering{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'questionOrdering'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '5')
            var answer_checked = $('textarea[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @elseif($quiz_question->quiz_type_id == '6')
            var answer_checked = $('input[name=answer{{$quiz_question->id}}]').val();
            var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');
          @endif

          // answers.push(answer_checked);
          answers.push(session_value);
          questions.push('{{$quiz_question->id}}');
          question_type.push('{{$quiz_question->quiz_type_id}}');
      @endforeach
    @endforeach

      // console.log(answers);

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('tesbaper/finish/'.$quiz->id)}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          question_type : question_type,
          _token: token
        },
        success : function(result){
          //remove session answer
          @foreach($all_quiz_page_breaks as $quiz_page_break)
            @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
              localStorage.removeItem('answer{{$quiz_question->id}}');
              localStorage.removeItem('questionOrdering{{$quiz_question->id}}');
            @endforeach
          @endforeach
          //remove session answer

          window.location.assign('{{url('tesbaper/result/'.$quiz->id)}}');
        },
      });
    }

    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
  @foreach($all_quiz_page_breaks as $quiz_page_break)
    @foreach($quiz_page_break->quiz_questions->where('quiz_id', $quiz->id) as $quiz_question)
      var session_value = get_session_answer('{{'answer'.$quiz_question->id}}');

      @if($quiz_question->quiz_type_id == '1' || $quiz_question->quiz_type_id == '2' || $quiz_question->quiz_type_id == '7')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}][value='"+session_value+"']").attr("checked","checked");
        }
      @elseif($quiz_question->quiz_type_id == '3')
        if(session_value !== null){
          $("#questionOrdering{{$quiz_question->id}}").val(get_session_answer("questionOrdering{{$quiz_question->id}}"));
        }
      @elseif($quiz_question->quiz_type_id == '5')
        if(session_value !== null){
          $("textarea[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @elseif($quiz_question->quiz_type_id == '6')
        if(session_value !== null){
          $("input[name=answer{{$quiz_question->id}}]").val(session_value);
        }
      @endif
    @endforeach
  @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}

  {{-- sortable --}}
  <script src="/jquery-ui/jquery-ui.js"></script>
  <script src="/jquery-ui/jquery.ui.touch-punch.min.js"></script>
  <script>
    $( function() {
      $( ".SectionSortable" ).sortable({
        axis: 'y',
        helper: 'clone',
        out: function(event, ui) {
          var itemOrder = $(this).sortable('toArray', { attribute: 'data-id' });
          var question_id = $(this).attr('question-id');
          localStorage.setItem("questionOrdering"+question_id, itemOrder)
          $("#questionOrdering"+question_id).val(itemOrder);
          // console.log(itemOrder)
          $('.answerNumber').each(function (i) {
              var humanNum = i + 1;
              $(this).html(humanNum + '');
          });
        },
      });
      $( ".SectionSortable" ).disableSelection();
    });
  </script>
  {{-- sortable --}}

  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
