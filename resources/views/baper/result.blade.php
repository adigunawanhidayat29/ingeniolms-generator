@extends('baper.template')

@section('title', $quiz->name)

@push('style')
  <style media="screen">
    .ms-collapse .card .card-header .card-title a:after{
      display: none;
    }
    .card-quiz .card-header .card-title p{
      margin-left: 0.5rem;
      margin-bottom: 0;
      width: 96%;
      line-height: 1.5;
      font-weight: 400;
    }

    .baper-collapse{
      position: relative;
      width: 100%;
      display: -webkit-box !important;
    }
  </style>
  <style media="screen">
    @media (max-width: 480px){
      .card-quiz .card-header .card-title p{
        margin-left: 0.5rem;
        margin-bottom: 0;
        width: 88%;
        line-height: 1.5;
        font-weight: 400;
      }
    }
  </style>
@endpush

@section('content')
  <div class="wrap pt-2 pb-2 mt-2 mb-2 bg-white">
    <div class="container">
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card-quiz">
          <div class="card-header d-flex">
            <h3 class="headline-md no-m" style="align-self: center">Hasil {{$quiz->name}}:</h3>
            <span class="ml-auto mr-0">
              <a class="btn btn-success btn-raised" href="/tesbaper/report/{{Auth::user()->id}}/{{$quiz->id}}">Lihat Hasil</a>
            </span>
          </div>
          <div class="card-block">
            @foreach($quiz_participants as $index => $quiz_participant)
              <div class="ms-collapse" id="accordion{{$index+1}}" role="tablist" aria-multiselectable="true">
                <div class="card card-default">
                  <div class="card-header bg-white" role="tab" id="heading{{$index+1}}">
                    <h4 class="card-title">
                      <a class="collapsed withripple baper-collapse" role="button" data-toggle="collapse" data-parent="#accordion{{$index+1}}" href="#baper{{$index+1}}" aria-expanded="false" aria-controls="baper{{$index+1}}">
                        <span class="badge">{{$index+1}}</span>
                        &nbsp;
                        {!!$quiz_participant->question!!}
                      </a>
                    </h4>
                  </div>
                  <div id="baper{{$index+1}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="heading{{$index+1}}">
                    <div class="card-block">
                      <h3 class="headline-xs mt-0"><span>Jawaban:</span></h3>
                      @if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '7')
                        {!!$quiz_participant->answer!!}
                        {{--{!!$quiz_participant->answer_correct == '1' ? '<i class="fa fa-check color-success"></i>' : '<i class="fa fa-times color-danger"></i>'!!}--}}
                      @elseif($quiz_participant->quiz_type_id == '3')
                        @php
                          $question_answers = DB::table('quiz_question_answers')
                          ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
                          ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
                          ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
                          ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
                          ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
                          ->get();
                        @endphp
                        @foreach($question_answers as $index => $question_answer)
                          {{$index +1}}. {{$question_answer->answer}} <strong>{{$question_answer->cluster_name}}</strong> <br>
                        @endforeach
                      @elseif($quiz_participant->quiz_type_id == '5')
                        {!!$quiz_participant->answer_essay!!}
                      @elseif($quiz_participant->quiz_type_id == '6')
                        {!!$quiz_participant->answer_short_answer!!}
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
@endsection

@push('script')
  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
