@extends('baper.template')
@section('title', 'Login - Tes Baper')

@push('style')
  <style media="screen">
    .mw-500{
      max-width: 500px;
    }
    .card-login{
      background: #fff;
      width: 100%;
      border-radius: 5px;
      box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.05);
    }
      .card-login .card-header{
        background-color: #ffffff !important;
        padding: 1.25rem 4rem;
        margin-bottom: 0;
        background-color: rgba(0,0,0,.03);
        border-bottom: 1px solid rgba(0,0,0,.05);
      }
        .card-login .card-header .card-title{
          margin: 0.5rem 0;
        }
      .card-login .card-block{
        padding: 1rem 1rem 3rem 1rem;
      }
    .form-login{
      padding: 0 3rem;
    }
  </style>
@endpush

@section('content')
  <div class="wrap" style="background-color: #f8f9fa; min-height: 100vh; padding: 10vh 0;">
    <div class="container">

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
      <div class="mx-auto mw-500">
        <div class="card-login">
          <div class="card-header">
            <h3 class="card-title">Login BAPER PLUS <sub class="fw-600 fs-14" style="bottom:0;">TEST</sub></h3>
          </div>
          <div class="card-block" style="padding-bottom: 3rem;">
            <form class="form-login" action="" method="post">
              {{csrf_field()}}
              <div class="form-group">
                {{--<label for="">Email</label>--}}
                <input type="email" name="email" class="form-control" id="" placeholder="Email" value="{{old('email')}}">
              </div>
              <div class="form-group">
                {{--<label for="">Password</label>--}}
                <input type="password" name="password" class="form-control" id="" placeholder="Password">
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-lg btn-block btn-primary btn-raised" value="Login">
              </div>
            </form>
            <div class="text-center">
              <span>Belum punya akun? </span><a href="/tesbaper/register">Register disini</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')

@endpush
