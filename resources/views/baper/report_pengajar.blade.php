@extends('baper.template')
@section('title', 'Laporan Tes Baper')
@push('style')
  <link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <style media="screen">
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			background: #ccc;
			color: #333!important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:active {
			background: none;
			color: black!important;
		}
		select.form-control:not([size]):not([multiple]){
			height: auto !important;
		}
	</style>
@endpush
@section('content')
  <div class="container">
    <div class="row mt-2">
      <div class="col-md-12">
        <h1>Hasil Tesbaper</h1>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Lihat Dengan Kode</h3>
          </div>
          <div class="panel-body">
            <form class="form-inline" action="/tesbaper/pengajar/report/128/filter" method="get">
              <div class="form-group">
                <input type="text" class="form-control" id="" placeholder="Masukan kode" name="code">
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-raised btn-primary" value="Lihat">
              </div>
            </form>
          </div>
        </div>
        <table class="table table-striped table-bordered" style="width:100%" id="data">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Kode Tesbaper</th>
              <th>Tanggal Mulai</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script src="{{url('js/jquery.dataTables.min.js')}}"></script>
	<script src="{{url('js/dataTables.bootstrap.min.js')}}"></script>
	<script type="text/javascript">
	  $(function(){
			var asset_url = '{{asset_url()}}';
	    $($("#data").DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: '{{ url("tesbaper/pengajar/report/serverside/".Request::segment(4)) }}',
	      columns: [
					{ data: 'name', name: 'name' },
					{ data: 'code', name: 'code' },
					{ data: 'created_at', name: 'created_at' },
	        { data: 'action', 'searchable': false, 'orderable':false }
	      ],
        "order": [[ 2, "desc" ]],
				"oLanguage": {
					"sLengthMenu": "Tampilkan _MENU_",
					"sSearch": "Cari: ",
		      "oPaginate": {
						"sPrevious": "<",
						"sNext": ">",
					}
		    },
	    }).table().container()).removeClass( 'form-inline' );
	  });
  </script>
@endpush
