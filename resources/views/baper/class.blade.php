@extends('baper.template')
@section('title', 'Kelas - Tes Baper')

@push('style')

@endpush

@section('content')
  <div class="wrap pt-2 pb-2 bg-white" style="min-height: 500px">
    <div class="container">
      <h1>Kelas Tes Baper</h1>
      <p>Halo, {{Auth::user()->name}} <a href="/tesbaper/logout">(logout)</a></p>
      
    </div>
  </div>
@endsection

@push('script')

@endpush
