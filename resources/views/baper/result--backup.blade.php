@extends('layouts.app')

@section('title', $quiz->name)

@section('content')

  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card-quiz">
          <div class="card-header">
            <h3 class="card-title">Hasil {{$quiz->name}}:</h3>
            <div class="text-right">
              <a class="btn btn-success btn-raised" href="/tesbaper/report/{{Auth::user()->id}}/{{$quiz->id}}">Lihat Hasil</a>
            </div>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table quiz-result">
                @foreach($quiz_participants as $index => $quiz_participant)
                  <tr class="">
                    <td>
                      <span class="badge">{{$index+1}}</span>
                    </td>
                    <td>
                      {!!$quiz_participant->question!!}
                    </td>
                    <td>
                      <div class="radio radio-primary">
                        @if($quiz_participant->quiz_type_id == '1' || $quiz_participant->quiz_type_id == '2' || $quiz_participant->quiz_type_id == '7')
                          {!!$quiz_participant->answer!!}
                          {{--{!!$quiz_participant->answer_correct == '1' ? '<i class="fa fa-check color-success"></i>' : '<i class="fa fa-times color-danger"></i>'!!}--}}
                        @elseif($quiz_participant->quiz_type_id == '3')
                          @php
                            $question_answers = DB::table('quiz_question_answers')
                            ->select(DB::raw('@rownum := @rownum + 1 AS number'), 'quiz_question_answers.*', 'baper_clusters.name as cluster_name', 'baper_clusters.id as cluster_id')
                            ->whereIn('quiz_question_answers.id', explode(',', $quiz_participant->quiz_question_answer_id))
                            ->join('baper_cluster_answers', 'baper_cluster_answers.quiz_answer_id', 'quiz_question_answers.id')
                            ->join('baper_clusters', 'baper_clusters.id', 'baper_cluster_answers.cluster_id')
                            ->orderByRaw(DB::raw("FIELD(quiz_question_answers.id, $quiz_participant->quiz_question_answer_id)"))
                            ->get();
                          @endphp
                          @foreach($question_answers as $index => $question_answer)
                            {{$index +1}}. {{$question_answer->answer}} <strong>{{$question_answer->cluster_name}}</strong> <br>
                          @endforeach
                        @elseif($quiz_participant->quiz_type_id == '5')
                          {!!$quiz_participant->answer_essay!!}
                        @elseif($quiz_participant->quiz_type_id == '6')
                          {!!$quiz_participant->answer_short_answer!!}
                        @endif
                      </div>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  {{-- wirisjs --}}
  <script type="text/javascript">
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = "WIRISplugins.js?viewer=image";
  document.head.appendChild(js);
  </script>
  <script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
  </script>
  {{-- wirisjs --}}
@endpush
