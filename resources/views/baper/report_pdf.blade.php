<html>
<head>
<meta charset="utf-8">
<title>Laporan</title>
<style media="screen">
*{
font-family: Roboto,sans-serif;
}

.report{
background: #fff;
width: 100%;
padding: 10rem;
}
.report-header{
position: relative;
/* background: #ff0; */
background: #00b050;
padding: 1rem 2rem;
text-align: center;
}
.report-header h2{
color: #fff;
font-size: 14px;
font-weight: 600;
margin: 0;
}
.report-title{
position: relative;
width: 100%;
margin: 3rem 0;
}
.report-title img{
position: absolute;
width: 100px;
right: 0;
}
.report-title p{
font-size: 14px;
}
.report-title h2{
margin: 0 0 1rem 0;
font-weight: 600;
}
.report-body{

}
.report-body .participant{
margin: 3rem 0;
text-align: center;
}
.report-title .participant p{
font-size: 14px;
}
.report-body .participant h2{
margin: 0 0 1rem 0;
font-size: 5rem;
font-weight: 400;
border-bottom: 2px solid #f3f3f3;
width: 400px;
margin-left: auto;
margin-right: auto;
}
.report-body .cluster{
margin: 3rem 0;
}
.cluster-table{
width: 100%;
}
.cluster-table tr th, .cluster-table tr td{
padding: 1rem 3rem;
border: 1px solid #e9ecef;
}
</style>
<style media="screen">

.report{
background: #fff;
width: 100%;
padding: 2rem;
}
.report-header{
position: relative;
/* background: #ff0; */
background: #00b050;
padding: 1rem 2rem;
margin: 0;
text-align: center;
}
.report-header h2{
color: #fff;
font-size: 14px;
font-weight: 600;
margin: 0;
}
.report-title{
position: relative;
width: 100%;
margin: 3rem 0;
}
.report-title img{
position: absolute;
width: 100px;
right: 0;
}
.report-title p{
font-size: 14px;
}
.report-title h2{
/* margin: 0 0 1rem 0; */
margin: 0;
font-weight: 600;
}
.report-body{

}
.report-body .grade{
margin: 5rem 0;
text-align: center;
}
  .report-body .grade h3{
  margin: 0;
  }
.report-body .participant{
/* margin: 3rem 0; */
margin: 0;
text-align: center;
}
.report-title .participant p{
font-size: 14px;
}
.report-body .participant h2{
margin: 0 0 1rem 0;
font-size: 5rem;
font-weight: 400;
border-bottom: 2px solid #f3f3f3;
width: 100%;
margin-left: auto;
margin-right: auto;
}
.report-body .cluster{
margin: 1rem 0;
}
.cluster-table{
width: 100%;
}
.cluster-table tr th, .cluster-table tr td{
padding: 0.5rem;
border: 1px solid #e9ecef;
font-size: 14px;
}

</style>
</head>
<body>
  <div class="wrap pt-2 pb-2 mt-2 mb-2">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- REPORT -->
          <div class="report">
            <div class="report-header">
              <h2>BAPER PLUS</h2>
            </div>
            <div class="report-title">
              <img src="baper-assets/logo.jpg" alt="baperplus">
              <p>Learning Consultant</p>
              <h2>BEFA Institute</h2>
              <p>Better Education For All</p>
            </div>
            <div class="report-body">
              <div class="participant">
                <p>diberikan kepada:</p>
                <h3>{{$biodata[0]['data']}}</h3>
                <p>Berasal dari sekolah {{$biodata[1]['data']}}</p>
              </div>

              <div class="cluster">
                <table class="cluster-table">
                  <tr class="active" style="background:#f3f3f3">
                    <th>Rekomendasi Jurusan</th>
                    <th>Urutan Cluster Kamu</th>
                  </tr>
                  <tr>
                    <td>
                      @foreach($answers_majors as $index => $value)
                        {{$index + 1}}. {{$value['major_name']}} <br>
                      @endforeach
                    </td>
                    <td>
                      @foreach($result_clusters as $index => $value)
                        {{-- @if($index < 4) --}}
                          {{$index + 1}}. {{$value['cluster']}} <br>
                        {{-- @endif --}}
                      @endforeach
                    </td>
                  </tr>
                </table>
              </div>

              <div class="grade">
                <h3>Tipe Kecerdasan</h3>
                <div class="cluster">
                  <table class="cluster-table">
                    <tr class="active" style="background:#f3f3f3">
                      @foreach($result_likerts as $result_likert)
                      <th>
                        {{$result_likert['multiple_intellegency_name']}}
                      </th>
                      @endforeach
                    </tr>

                    <tr>
                      @foreach($result_likerts as $result_likert)
                      <td>
                        {{$result_likert['total']}}
                      </td>
                      @endforeach
                    </tr>
                  </table>
                </div>
              </div>

            </div>
          </div>
          <!-- END REPORT -->

        </div>
      </div>
    </div>
  </div>


</body>
</html>
