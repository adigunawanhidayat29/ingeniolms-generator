@extends('employeer.layouts.app')

@section('title')

@push('style')  
  <link rel="stylesheet" href="/duallistbox/bootstrap-duallistbox.min.css">
@endpush

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>List Users</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div style="margin-bottom: 2rem;">
                                <a href="{{ url('/employeer/users/create') }}" class="btn btn-primary btn-sm" title="Add New Cohort">Add User</a>
                                <!-- <a class="btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#addCohortUser"><i class="material-icons">add</i><span>@lang('back.cohort.add_user_button')</span></a> -->
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $no => $item)
                                            <tr>
                                                <td>{{ $no+1 }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->username }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection
