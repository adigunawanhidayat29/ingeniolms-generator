@extends('layouts.app')

@section('title', 'Edit Report')

@push('style')
    <style>
        .form-group {
            margin: 0;
        }

        .form-group label.control-label {
            margin: 0;
        }
    </style>
@endpush

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="headline-md no-m">@lang('front.level_up.edit') <span>@lang('front.level_up.report')</span></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">@lang('front.level_up.home')</a></li>
                    <li><a href="/course/dashboard">@lang('front.level_up.manage_class')</a></li>
                    <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
                    <li>@lang('front.level_up.edit_report')</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap pt-2 pb-2 mb-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-md">
                        <div class="card-block">
                            <form action="/{{$course_user->course_id}}/{{$course_user->user_id}}/update_reports" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label" for="level">@lang('front.level_up.level')</label>
                                    <input type="text" class="form-control" id="level" name="level" value="{{$course_user->level}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="points">@lang('front.level_up.total_points')</label>
                                    <input type="number" class="form-control" id="points" name="points" value="{{$course_user->level_point}}" placeholder="@lang('front.level_up.enter_points')">
                                </div>
                                <button type="submit" class="btn btn-raised btn-primary">@lang('front.level_up.save_changes')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection