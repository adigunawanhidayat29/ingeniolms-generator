@extends('layouts.app')

@section('title', 'Overview Level')

@push('style')
    <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .nav-tabs-ver-container .nav-tabs-ver {
            padding: 0;
            margin: 0;
        }

        .nav-tabs-ver-container .nav-tabs-ver:after {
            display: none;
        }

        .col-md-3 .card .card-img-top {
            height: 137px;
        }

        .card {
            border: 1px solid #ececec;
        }

        .form-group {
            margin: 0;
        }

        .form-group.label-floating {
            margin: 8px 0 0;
        }

        .form-group label.control-label {
            margin: 0;
        }

        .form-group.label-floating label.control-label, 
        .form-group.label-placeholder label.control-label {
            top: 8px;
        }

        .form-group.label-static label.control-label, 
        .form-group.label-floating.is-focused label.control-label, 
        .form-group.label-floating:not(.is-empty) label.control-label {
            top: -8px;
        }

        .form-group .bootstrap-select.btn-group, 
        .form-horizontal .bootstrap-select.btn-group, 
        .form-inline .bootstrap-select.btn-group {
            margin-top: 0;
        }

        select.form-control:not([size]):not([multiple]) {
            height: auto;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="headline-md no-m">@lang('front.level_up.level') <span>@lang('front.level_up.overview')</span></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">@lang('front.level_up.home')</a></li>
                    <li><a href="/course/dashboard">@lang('front.level_up.manage_class')</a></li>
                    <li><a href="/course/preview/{{$course->id}}">{{$course->title}}</a></li>
                    <li>@lang('front.level_up.overview')</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap pt-2 pb-2 mb-2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-3 nav-tabs-ver-container">
					<img src="{{$course->image}}" alt="{{ $course->title }}" class="img-fluid mb-2">
					<div class="card no-shadow">
						<ul class="nav nav-tabs-ver" role="tablist">
							<li class="nav-item"><a class="nav-link color-danger" href="/course/preview/{{$course->id}}"><i class="fa fa-chevron-circle-left"></i> @lang('front.page_manage_atendee.sidebar_back')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/atendee/{{$course->id}}"><i class="fa fa-users"></i> @lang('front.page_manage_atendee.sidebar_atendee')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/attendance/{{$course->id}}"><i class="fa fa-check-square-o"></i> @lang('front.page_manage_atendee.sidebar_attendance')</a></li>
							<li class="nav-item"><a class="nav-link" href="/course/grades/{{$course->id}}"><i class="fa fa-address-book-o"></i> @lang('front.page_manage_atendee.sidebar_gradebook')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/certificate/{{$course->id}}"><i class="fa fa-certificate"></i> @lang('front.page_manage_atendee.sidebar_ceritificate')</a></li>              
							<li class="nav-item"><a class="nav-link" href="/courses/access-content/{{$course->id}}"><i class="fa fa-list-ul"></i> @lang('front.page_manage_atendee.sidebar_content_access')</a></li>
							@if(isTeacher(Auth::user()->id) === true)
                                <li class="nav-item"><a class="nav-link" href="/{{$course->id}}/list_badges"><i class="fa fa-shield"></i> Badges</a></li>
                                @if($course->level_up == 1)
                                    <li class="nav-item"><a class="nav-link active" href="/{{$course->id}}/level_settings"><i class="fa fa-trophy"></i> Levels</a></li>
                                @endif
                            @endif
						</ul>
					</div>
				</div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active">
                            <div class="card-md">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#info">@lang('front.level_up.info')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ladder">@lang('front.level_up.ladder')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#report">@lang('front.level_up.report')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#log">@lang('front.level_up.log')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#levels">@lang('front.level_up.level')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#rules">@lang('front.level_up.rules')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#visuals">@lang('front.level_up.visuals')</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings">@lang('front.level_up.settings_level')</a></li>
                                </ul>

                                <div class="card-block">
                                    <div class="tab-content">
                                        {{-- TAB INFO --}}
                                        <div id="info" class="tab-pane active">
                                            <div class="row">
                                                @foreach($course_leveling as $data_level)
                                                    <div class="col-md-4">
                                                        <div class="card no-shadow">
                                                            <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course->levelup_img)}}" alt="Card image">
                                                            <div class="card-body">
                                                                <p style="font-size: 16px; margin-bottom: 0;"><span style="display: block; font-size: 12px; font-weight: 700; color: #999; line-height: 1.2;">@lang('front.level_up.points_required'):</span> {{$data_level->points_required}}</p>
                                                                {{-- <h4 class="card-title text-center">{{$data_level->points_required}}</h4> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        {{-- TAB LADDER --}}
                                        <div id="ladder" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="table-ladder" class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('front.level_up.rank')</th>
                                                                <th>@lang('front.level_up.level')</th>
                                                                <th>@lang('front.level_up.participant')</th>
                                                                <th>@lang('front.level_up.total_points')</th>
                                                                <th>@lang('front.level_up.progress')</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            @foreach($course_user as $no => $data_user)
                                                                <tr>
                                                                    <td>{{$no+1}}</td>
                                                                    <td>{{$data_user->level}}</td>
                                                                    <td>{{$data_user->user_name}}</td>
                                                                    <td>{{$data_user->level_point}}</td>

                                                                    @php
                                                                        $get_current_level = get_current_level($data_user->course_id, $data_user->level);
                                                                        $get_next_level = get_next_level($data_user->course_id, $data_user->level);

                                                                        $current_point_level = ($data_user->level_point - $get_current_level->points_required);

                                                                        if($get_current_level->level == $get_next_level->level)
                                                                        {
                                                                            $points_togo = 0;

                                                                            $points_difference = 0;

                                                                            $exp_percentage = 100;
                                                                        }

                                                                        else

                                                                        {
                                                                            $points_difference = $get_next_level->points_required - $get_current_level->points_required;

                                                                            $exp_percentage = number_format($current_point_level / $points_difference * 100, 2);

                                                                            $points_togo = ($get_next_level->points_required - $data_user->level_point);
                                                                        }
                                                                    @endphp

                                                                    <td>
                                                                        <div class="progress" style="background-color: #fff; margin-bottom: 0;">
                                                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{$exp_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$exp_percentage}}%">
                                                                                {{$exp_percentage}}%
                                                                            </div>
                                                                        </div>
                                                                        <p>{{$points_togo}}xp to go</p>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB REPORT --}}
                                        <div id="report" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="table-report" class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('front.level_up.name')</th>
                                                                <th>@lang('front.level_up.level')</th>
                                                                <th>@lang('front.level_up.total_points')</th>
                                                                <th>@lang('front.level_up.progress')</th>
                                                                <th>@lang('front.level_up.action')</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            @foreach($course_user as $data_user)
                                                                <tr>
                                                                    <td>{{$data_user->user_name}}</td>
                                                                    <td>{{$data_user->level}}</td>
                                                                    <td>{{$data_user->level_point}}</td>
                                                                    <td>
                                                                        @php
                                                                            $get_current_level = get_current_level($data_user->course_id, $data_user->level);
                                                                            $get_next_level = get_next_level($data_user->course_id, $data_user->level);

                                                                            $current_point_level = ($data_user->level_point - $get_current_level->points_required);

                                                                            if($get_current_level->level == $get_next_level->level)
                                                                            {
                                                                                $points_togo = 0;

                                                                                $points_difference = 0;

                                                                                $exp_percentage = 100;
                                                                            }

                                                                            else

                                                                            {
                                                                                $points_difference = $get_next_level->points_required - $get_current_level->points_required;

                                                                                $exp_percentage = number_format($current_point_level / $points_difference * 100, 2);

                                                                                $points_togo = ($get_next_level->points_required - $data_user->level_point);
                                                                            }
                                                                        @endphp

                                                                        <div class="progress" style="background-color: #fff; margin-bottom: 0;">
                                                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{$exp_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$exp_percentage}}%">
                                                                                {{$exp_percentage}}%
                                                                            </div>
                                                                        </div>
                                                                        <p>{{$points_togo}}xp to go</p>
                                                                    </td>
                                                                    <td>
                                                                        <a href="/{{$data_user->course_id}}/{{$data_user->user_id}}/edit_reports" class="btn btn-raised btn-sm btn-primary m-0">@lang('front.level_up.edit')</a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB LOG --}}
                                        <div id="log" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="table-log" class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('front.level_up.event_time')</th>
                                                                <th>@lang('front.level_up.name')</th>
                                                                <th>@lang('front.level_up.reward')</th>
                                                                <th>@lang('front.level_up.event_name')</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            @foreach($users_leveling_logs as $log_data)
                                                                <tr>
                                                                    <td>{{$log_data->log_created_at}}</td>
                                                                    <td>{{$log_data->user_name}}</td>
                                                                    <td>{{$log_data->log_reward}}</td>
                                                                    
                                                                    @php
                                                                        $contents = get_contents_rule($log_data->leveling_rules_id);
                                                                    @endphp

                                                                    <td>
                                                                        <ul>
                                                                            @foreach($contents as $content)
                                                                                <li>{{$content->content_name}}</li>

                                                                                @php
                                                                                    $criterias = get_rules_criteria($content->rules_criteria_id);
                                                                                @endphp

                                                                                <p>-{{$criterias->name}}</p>
                                                                            @endforeach
                                                                        </ul>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB LEVEL --}}
                                        <div id="levels" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="/{{$course_id}}/update_levels" method="POST">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <input type="number" value="{{count($course_leveling)}}" min="2" id="add_card">
                                                        </div>
                                                        <div class="row" id="cards_level">
                                                            @foreach($course_leveling as $no => $data_level)
                                                                <div class="col-md-4 card-remove{{$no+1}}">
                                                                    <div class="card border no-shadow">
                                                                        <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course->levelup_img)}}" alt="Card image">
                                                                        <div class="card-body">
                                                                            <p>{{$no+1}}</p>
                                                                            <div class="form-group label-floating">
                                                                                <label class="control-label">@lang('front.level_up.level_name')</label>
                                                                                <input class="form-control" type="text" name="level_name[]" value="{{$data_level->level_name}}">
                                                                            </div>

                                                                            <div class="form-group label-floating">
                                                                                <label class="control-label">@lang('front.level_up.points_required')</label>
                                                                                <input class="form-control" type="number" name="points_required[]" value="{{$data_level->points_required}}">
                                                                            </div>

                                                                            <div class="form-group label-floating">
                                                                                <label class="control-label">@lang('front.level_up.description')</label>
                                                                                <input class="form-control" type="text" name="description[]" value="{{$data_level->level_description}}">
                                                                            </div>
                                                                            <input type="hidden" name="level[]" value="{{$no+1}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <button class="btn btn-raised btn-primary" type="submit">@lang('front.level_up.save_changes')</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB RULE --}}
                                        <div id="rules" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="/update_rules" method="post">
                                                        {{ csrf_field() }}
                                                        <div id="card-rule">
                                                            <div class="card no-shadow">
                                                                <div class="card-body">
                                                                    <div id="content-forms0">
                                                                        <div class="form-group">
                                                                            <label class="control-label">@lang('front.level_up.complete_type')</label>
                                                                            <select class="form-control" name="rule[0][complete_type]">
                                                                                <option value="any">@lang('front.level_up.any')</option>
                                                                                <option value="all">@lang('front.level_up.all')</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">@lang('front.level_up.points_required')</label>
                                                                            <input type="number" class="form-control" name="rule[0][points_earned]">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">@lang('front.level_up.contents')</label>
                                                                            <select class="form-control" name="rule[0][content_id][]">
                                                                                @foreach($course_content as $content)
                                                                                    <option value="{{$content->content_id}}">{{$content->content_title}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">@lang('front.level_up.complete_criteria')</label>
                                                                            <select class="form-control" name="rule[0][rules_criteria_id][]">
                                                                                @foreach($rules_criteria as $criteria)
                                                                                    <option value="{{$criteria->id}}">{{$criteria->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <button type="button" class="btn btn-raised btn-sm btn-primary add_content" value="0">@lang('front.level_up.add_content')</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a class="btn btn-raised btn-default" id="add_rule" style="cursor: pointer;">@lang('front.level_up.add_rule')</a>
                                                        <button type="submit" class="btn btn-raised btn-primary">@lang('front.level_up.save_changes')</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB VISUAL --}}
                                        <div id="visuals" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="/{{$course_id}}/update_visuals" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            {{-- <label class="control-label">@lang('front.level_up.upload_image')</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="customFile" name="img">
                                                                <label class="custom-file-label" for="customFile">@lang('front.level_up.choose_file')</label>
                                                            </div> --}}

                                                            <label class="control-label">@lang('front.level_up.upload_image')</label>
                                                            <input type="file" name="img">
                                                            <div class="input-group">
                                                                <input type="text" readonly class="form-control" placeholder="@lang('front.level_up.select_image')">
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-raised btn-primary" type="submit">@lang('front.level_up.save_changes')</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- TAB SETTING --}}
                                        <div id="settings" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="/{{$course_id}}/update_settings" method="post">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="control-label">@lang('front.level_up.points_gain')</label>
                                                            <div>
                                                                <input type="checkbox" name="notification" {{$course->points_gain_status == '0' ? '' : 'checked'}}>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">@lang('front.level_up.notifications')</label>
                                                            <div>
                                                                <input type="checkbox" name="notification" {{$course->level_notif_status == '0' ? '' : 'checked'}}>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-raised btn-primary">@lang('front.level_up.save_changes')</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script src="{{url('/js/bootstrap-switch.min.js')}}"></script> <!-- switchjs -->
<script src="{{url('ckeditor/ckeditor.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="/jquery-ui/jquery-ui.js"></script>
<!-- INTERNAL  FILE UPLOADES JS -->
<script src="{{asset('/fileuploads/js/fileupload.js')}}"></script>
<script src="{{asset('/fileuploads/js/file-upload.js')}}"></script>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#card-rule").on('click', '.add_content', function(){

        let value = $(this).attr('value');
        // console.log(value);
        let html = `<div class="form-group">
                        <label class="control-label">@lang('front.level_up.contents')</label>
                        <select class="form-control" name="rule[${value}][content_id][]">
                            @foreach($course_content as $content)
                                <option value="{{$content->content_id}}">{{$content->content_title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">@lang('front.level_up.complete_criteria')</label>
                        <select class="form-control" name="rule[${value}][rules_criteria_id][]">
                            @foreach($rules_criteria as $criteria)
                                <option value="{{$criteria->id}}">{{$criteria->name}}</option>
                            @endforeach
                        </select>
                    </div>`;
        $('#content-forms' + value).append(html);
    })
  });

  $(document).ready(function(){
    let i = 1;
    $("#add_rule").on('click', function(){
      let html = `<div class="card no-shadow">
                    <div class="card-body">
                        <div id="content-forms${i}">
                            <div class="form-group">
                                <label class="control-label">@lang('front.level_up.complete_type')</label>
                                <select class="form-control" name="rule[${i}][complete_type]">
                                    <option value="any">@lang('front.level_up.any')</option>
                                    <option value="all">@lang('front.level_up.all')</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">@lang('front.level_up.points_required')</label>
                                <input type="number" class="form-control" name="rule[${i}][points_earned]">
                            </div>

                            <div class="form-group">
                                <label class="control-label">@lang('front.level_up.contents')</label>
                                <select class="form-control" name="rule[${i}][content_id][]">
                                    @foreach($course_content as $content)
                                        <option value="{{$content->content_id}}">{{$content->content_title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">@lang('front.level_up.complete_criteria')</label>
                                <select class="form-control" name="rule[${i}][rules_criteria_id][]">
                                    @foreach($rules_criteria as $criteria)
                                        <option value="{{$criteria->id}}">{{$criteria->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <button type="button" class="btn btn-raised btn-sm btn-primary add_content" value="${i}">@lang('front.level_up.add_content')</button>
                    </div>
                </div>`;
            $('#card-rule').append(html);
            i++;
        })
    });
</script>

<script>
    $(document).ready(function(){
        
        let num_default = parseInt($("#add_card").val());
        let current_points = {{$data_level->points_required}};

        $("#add_card").keypress(function (evt) {
            evt.preventDefault();
        });

        $("#add_card").on('change', function(){

            let num = $("#add_card").val();
            let calculate = num - num_default;

            if(calculate > 0)
            {
                let points_required = current_points + 100;

                let card = `
                <div class="col-md-4 card-remove${num}">
                    <div class="card no-shadow">
                        <img class="card-img-top" src="{{asset('storage/levelup_img/'. $course->levelup_img)}}" alt="@lang('front.level_up.image')">
                        <div class="card-body">
                            <p>${num}</p>
                            <div class="form-group label-floating">
                                <label class="control-label">@lang('front.level_up.level_name')</label>
                                <input class="form-control" type="text" name="level_name[]" value="">
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">@lang('front.level_up.points_required')</label>
                                <input class="form-control" type="number" name="points_required[]" value="${points_required}">
                            </div>

                            <div class="form-group label-floating">
                                <label class="control-label">@lang('front.level_up.description')</label>
                                <input class="form-control" type="text" name="description[]" value="">
                            </div>
                            <input type="hidden" name="level[]" value="${num}">
                        </div>
                    </div>
                </div>`;

                $('#cards_level').append(card);

                num_default = num;
                current_points = points_required;
            }

            else
            {
                $('.card-remove' + num_default).remove();

                num_default = num;
                current_points = current_points - 100;
            }
        
        });
    });
</script>

<script>
    $(function () {
        $('#table-ladder').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $(function () {
        $('#table-log').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

    $("[name=notification]").bootstrapSwitch();
</script>
@endpush