@extends('layouts.app')

@section('title', Lang::get('front.login.title'))

@section('content')

  @php
    $login_withSelected = [];
    if(isset(Setting('login_with')->value)) {
      $login_withSelected = explode(',', Setting('login_with')->value);
    }
  @endphp

  @php
    $registrationSelected = [];
    if(isset(Setting('registration')->value)) {
      $registrationSelected = explode(',', Setting('registration')->value);
    }
  @endphp

  {{-- @if(Session::has('status'))
      <div class="alert alert-success">
          {{ Session::get('status') }}
      </div>
  @endif
  @if(Session::has('warning'))
      <div class="alert alert-warning">
          {{ Session::get('warning') }}
      </div>
  @endif

  <div class="container mt-5" style="margin-top:120px !important">
    <div class="row justify-content-md-center">
      <div class="col-lg-6">
        <div class="card card-hero card-default animated fadeInUp animation-delay-7">
          <div class="card-block">
            <h1 class="text-center">Login</h1>
            <form class="form-horizontal" method="POST" action="/login">
              {{csrf_field()}}
              <fieldset>
                <div class="form-group row">
                  <label for="inputEmail" class="col-md-3 control-label">Email / Username</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email / Username"> </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-md-3 control-label">Password</label>
                  <div class="col-md-9">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password"> </div>
                </div>
              </fieldset>
              <button class="btn btn-raised btn-primary btn-block">Login

              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div> --}}


  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.login.title_page') <span>@lang('front.login.title')</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.login.breadcumb_home')</a></li>
          <li>@lang('front.login.breadcumb_login')</li>
        </ul>
      </div>
    </div>
  </div>

  <section>
    <div class="wrap pt-2 pb-2 mb-2 bg-white">
      <div class="container">

        @if(Session::has('status'))
            <div class="alert alert-success">
                {{ Session::get('status') }}
            </div>
        @endif
        @if(Session::has('warning'))
            <div class="alert alert-warning">
                {{ Session::get('warning') }}
            </div>
        @endif

        <div class="row">
          <div class="col-md-12">
            <h3 class="headline-sm text-center">@lang('front.login.lets_start')</h3>
            <div class="w-300 mx-auto">

              @if( isset(Setting('registration')->value) && in_array("self", $registrationSelected))
                <div class="text-center">
                  <p>@lang('front.login.login_with')</p>
                  @if( isset(Setting('login_with')->value) && in_array("facebook", $login_withSelected))
                    <a href="/auth/facebook" class="btn-social facebook">
                      Facebook</a>
                  @endif
                  @if( isset(Setting('login_with')->value) && in_array("google", $login_withSelected))
                    <a href="/auth/google" class="btn-social google">
                      Google</a>
                    </a>
                  @endif
                </div>
              @endif

            <div class="text-center">
              <p>Login as:</p>
            </div>

            <div class="row text-center">
              <div class="col-md-12">
                <button id="button_trainee" class="btn btn-default" style="font-size: 12px; background-color: blue;" onClick="buttonTrainee()">Trainee</button>
              </div>
              <div class="col-md-12">
                <a id="button_trainer" href="https://trainers.browntech.my/login" class="btn btn-default" style="font-size: 12px;" onClick="buttonTrainer()">Trainer</a>
              </div>
              <div class="col-md-12">
                <button id="button_employer" class="btn btn-default" style="font-size: 12px;" onClick="buttonEmployer()">Employer</button>
              </div>
              <div class="col-md-12">
                <button id="button_training_provider" class="btn btn-default" style="font-size: 12px;" onClick="buttonTrainingProvider()">Training Provider</button>
              </div>
            </div>

              @if( isset(Setting('login_with')->value) && in_array("email", $login_withSelected))
                {{-- <h4 class="text-center">Masuk dengan email</h4>
                <div class="form-group label-floating is-empty formGroupEmail">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-email"></i>
                    </span>
                    <label for="inputEmail" class="control-label">Email</label>
                    <input type="email" class="form-control" name="checkEmail" id="inputEmail" value="{{old('email')}}" required autofocus>
                  </div>
                </div>

                <form method="POST" action="{{ route('login') }}">

                  {{ csrf_field() }}

                  @if($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                  @endif

                  <fieldset>
                    <div class="section_password">
                      <div class="form-group no-m no-p label-floating is-empty">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                          </span>
                          <span id="loginEmail"></span>
                          <input type="hidden" class="form-control" id="hiddenEmail" name="email" value="{{ old('email') }}">
                        </div>
                      </div>
                      <div class="form-group label-floating is-empty">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                          <label for="inputEmail" class="control-label">Password</label>
                          <input type="password" class="form-control" id="inputPassword" name="password" required>
                          @if ($errors->has('password'))
                            <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div>
                      <div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="fs-14">Ingat Saya.</span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-raised btn-primary" onclick="checkEmail()" id="checkEmailBtn" type="button">Mulai</button>
                    <button class="btn btn-raised btn-primary" type="submit" id="submitLogin">Masuk</button>
                  </fieldset>
                </form> --}}

                <div class="justify-content-md-center mt-2">
                  <form class="" method="POST" action="/login">
                    {{csrf_field()}}
                    <fieldset>
                      {{-- <div class="form-group">
                        <label for="inputEmail" class="control-label">Email atau Username</label>
                        <div class="">
                          <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Isi email disini..."> </div>
                      </div> --}}

                      {{-- <div class="form-group m-0">
                        <label for="inputPassword" class="control-label">Kata Sandi</label>
                        <div class="">
                          <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Isi kata sandi disini..."> </div>
                      </div> --}}
                      
                      <div class="form-group label-floating is-empty">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                          </span>
                          <label for="inputEmail" class="control-label">Email or Username</label>
                          <input type="text" autocomplete="off" class="form-control" name="email" id="inputEmail" value="" required="" autofocus="">
                        </div>
                      </div>
                      <div class="form-group label-floating is-empty">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                          <label for="inputPassword" class="control-label">Password</label>
                          <input type="password" autocomplete="off" class="form-control" name="password" id="inputPassword" value="" required="" autofocus="">
                        </div>
                      </div>
                    </fieldset>
                    <button class="btn btn-raised btn-primary btn-block">@lang('front.login.btn_login')</button>
                  </form>
                </div>
              @endif
            </div>
            @if( isset(Setting('registration')->value) && in_array("self", $registrationSelected))
              <div class="text-center">
                <a class="btn btn-link" href="/password/reset" id="resetPassword">
                  @lang('front.login.forgot_password')
                </a>
              </div>

              <div class="text-center">
                <a class="btn btn-link" href="/register">
                  @lang('front.login.not_have_account') <span style="text-decoration: underline">@lang('front.login.register_now')</span>
                </a>
              </div>
            @endif
          </div>
        </div>
      </div>

    </div>
  </section>



@endsection

@push('script')
  <script type="text/javascript">
    $(".section_password").hide()
    $("#submitLogin").hide()

    $('#inputEmail').keyup(function(e){
      // const email = $(this).val();
        if(e.keyCode == 13){
          if( !validateEmail($(this).val())) {
            alert('failed email')
          }else{
            checkEmail();
            {{--$('#resetPassword').attr('href','/password/reset?email='+email)--}}
          }
        }
    });

    function validateEmail($email) {
      var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return emailReg.test( $email );
    }

    function checkEmail(){
      var email = $("#inputEmail").val();
      if ( email == '' ){
        $('#myAlert').fadeIn(600);
        setTimeout(function(){ $('#myAlert').fadeOut(600) }, 3000);
      }else{
        $.ajax({
          url: '/auth/check/email',
          type: 'post',
          data: {
            email: email,
            _token: "{{csrf_token()}}"
          },
          beforeSend: function(){},
          success: function(response){
            if(response.status == 200){
              $(".section_password").show();
              $("#submitLogin").show();
              $("#checkEmailBtn").hide();
              $(".formGroupEmail").hide();
              $("#inputPassword").focus();
              $("#loginEmail").html(email);
              $("#hiddenEmail").val(email);
            // }else if(response.status !== null){
              // $('#myAlert').fadeIn(600);
              // setTimeout(function(){ $('#myAlert').fadeOut(600) }, 3000);
            }else{
              window.location.assign('/register')
            }
          },
        })
      }
    }
  </script>

  <script>
    // simpan session user status [pengajar / pelajar]
    var url_string = window.location.href;
    var url = new URL(url_string);
    var user = url.searchParams.get("user");
    sessionStorage.setItem("user", `${user}`);
  </script>

  <script>
    function buttonTrainee(){
      document.getElementById("button_trainee").style.cssText = "background-color: blue;";
      document.getElementById("button_trainer").style.cssText = "background-color: white;";
      document.getElementById("button_employer").style.cssText = "background-color: white;";
      document.getElementById("button_training_provider").style.cssText = "background-color: white;";
    }

    function buttonTrainer(){
      document.getElementById("button_trainee").style.cssText = "background-color: white;";
      document.getElementById("button_trainer").style.cssText = "background-color: blue;";
      document.getElementById("button_employer").style.cssText = "background-color: white;";
      document.getElementById("button_training_provider").style.cssText = "background-color: white;";
    }

    function buttonEmployer(){
      document.getElementById("button_trainee").style.cssText = "background-color: white;";
      document.getElementById("button_trainer").style.cssText = "background-color: white;";
      document.getElementById("button_employer").style.cssText = "background-color: blue;";
      document.getElementById("button_training_provider").style.cssText = "background-color: white;";
    }

    function buttonTrainingProvider(){
      document.getElementById("button_trainee").style.cssText = "background-color: white;";
      document.getElementById("button_trainer").style.cssText = "background-color: white;";
      document.getElementById("button_employer").style.cssText = "background-color: white;";
      document.getElementById("button_training_provider").style.cssText = "background-color: blue;";
    }
  </script>
@endpush
