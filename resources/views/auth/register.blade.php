@extends('layouts.app')

@section('title', Lang::get('front.register.title'))

@section('content')
  {{-- <section>
    <div class="wrap pt-2 pb-2 mb-2 bg-white">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="headline-sm text-center">Daftar</h3>
            <div class="w-400 mx-auto">
              <div class="text-center">
                <p>Lengkapi data diri Anda sekarang juga</p>
                <div class="divider divider-gradient-negative"></div>
              </div>
                @if(Request::get('instructor'))
                  <div class="alert alert-info">Jika email Anda sudah terdaftar. silahkan <a href="#" data-toggle="modal" data-target="#ms-account-modal"><strong>masuk</strong></a> terlebih dahulu untuk memudahkan proses menjadi member</div>
                @endif
                <form method="POST" action="{{ route('register') }}">
                  {{ csrf_field() }}
                  <fieldset>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-email"></i>
                        </span>
                        <label for="inputUser" class="control-label">Email</label>
                        <input type="email" class="form-control" id="inputUser" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                          <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label for="inputUser" class="control-label">Nama</label>
                        <input type="text" class="form-control" id="inputUser" name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                          <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock-outline"></i>
                        </span>
                        <label for="inputPassword" class="control-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword" name="password" required>
                        @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock-outline"></i>
                        </span>
                        <label for="inputPassword2" class="control-label">Konfirmasi Password</label>
                        <input type="password" class="form-control" id="inputPassword2" name="password_confirmation" required>
                      </div>
                    </div>

                    <div class="form-group no-mt">
                      Dengan ini saya menyetujui <a href="/term-condition">Syarat & Ketentuan</a> dan <a href="/privacy-policy">Kebijakan Privasi</a> yang ditetapkan.
                    </div>
                    <button class="btn btn-raised btn-primary" type="submit" name="button">Daftar Sekarang</button>

                  </fieldset>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> --}}

  @php
    $login_withSelected = [];
    if(isset(Setting('login_with')->value)) {
      $login_withSelected = explode(',', Setting('login_with')->value);
    }
  @endphp

  @php
    $registrationSelected = [];
    if(isset(Setting('registration')->value)) {
      $registrationSelected = explode(',', Setting('registration')->value);
    }
  @endphp

  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.register.title_page') <span>@lang('front.register.title')</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.register.breadcumb_home')</a></li>
          <li>@lang('front.register.breadcumb_register')</li>
        </ul>
      </div>
    </div>
  </div>

  <section>
    <div class="wrap pt-2 pb-2 mb-2 bg-white">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="headline-sm text-center">@lang('front.register.title')</h3>
            <div class="w-400 mx-auto">

              @if( isset(Setting('registration')->value) && in_array("self", $registrationSelected))
                <div class="text-center">
                  <p>@lang('front.register.register_with')</p>
                  @if( isset(Setting('login_with')->value) && in_array("facebook", $login_withSelected))
                    <a href="/auth/facebook" class="btn-social facebook">
                      Facebook</a>
                  @endif
                  @if( isset(Setting('login_with')->value) && in_array("google", $login_withSelected))
                    <a href="/auth/google" class="btn-social google">
                      Google</a>
                    </a>
                  @endif
                </div>
              @endif

              <div class="text-center">
                <p>@lang('front.register.complete_biodata')</p>
                <div class="divider divider-gradient-negative"></div>
              </div>

                <form method="POST" action="{{ route('register') }}">
                  {{ csrf_field() }}
                  <input type="hidden" id="instructor" name="instructor" value="{{Request::get('instructor')}}">
                  <fieldset>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-email"></i>
                        </span>
                        <label for="inputUser" class="control-label">@lang('front.register.input_email')</label>
                        <input type="email" autocomplete="off" class="form-control" id="inputUser" name="email" value="{{\Session::get('checkEmail')}}" required autofocus>
                        @if ($errors->has('email'))
                          <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label for="inputUser" class="control-label">@lang('front.register.input_name')</label>
                        <input type="text" autocomplete="off" class="form-control" id="inputUser" name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                          <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock-outline"></i>
                        </span>
                        <label for="inputPassword" class="control-label">@lang('front.register.input_password')</label>
                        <input type="password" autocomplete="off" class="form-control" id="inputPassword" name="password" required>
                        @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock-outline"></i>
                        </span>
                        <label for="inputPassword2" class="control-label">@lang('front.register.input_confirm_password')</label>
                        <input type="password" autocomplete="off" class="form-control" id="inputPassword2" name="password_confirmation" required>
                      </div>
                    </div>

                    <div class="form-group no-mt">
                      @lang('front.register.agreement1') <a href="/term-condition">@lang('front.register.agreement2')</a> @lang('front.register.agreement3') <a href="/privacy-policy">@lang('front.register.agreement4')</a>@lang('front.register.agreement5')
                    </div>
                    <button class="btn btn-raised btn-primary" type="submit" name="button">@lang('front.register.btn_register')</button>

                  </fieldset>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('script')

  @if( isset(Setting('registration')->value) && !in_array("self", $registrationSelected))
    <script>
      window.location = '/login';
    </script>
  @endif
  
@endpush