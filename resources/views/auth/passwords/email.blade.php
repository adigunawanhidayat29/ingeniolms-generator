@extends('layouts.app')

@section('title', Lang::get('front.authentication.forgot_password'))

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-6" style="float:none;margin:auto;">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('front.authentication.forgot_password')</div>

                <div class="panel-body" style="background: white">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! Session::get('warning') !!}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="/password/reset">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">@lang('front.authentication.field_email')</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary btn-raised">
                                    @lang('front.authentication.forgot_password_send')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
