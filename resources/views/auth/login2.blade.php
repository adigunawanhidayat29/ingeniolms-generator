@extends('layouts.app')

@section('title','Login')

@section('content')
  <div class="bg-page-title-negative">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Halaman <span>Masuk</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>Halaman Masuk</li>
        </ul>
      </div>
    </div>
  </div>

  <section>
    <div class="wrap pt-2 pb-2 mb-2 bg-white">
      <div class="container">
        
        @if(Session::has('status'))
            <div class="alert alert-success">
                {{ Session::get('status') }}
            </div>
        @endif
        @if(Session::has('warning'))
            <div class="alert alert-warning">
                {{ Session::get('warning') }}
            </div>
        @endif

        <div class="row">
          <div class="col-md-12">
            <h3 class="headline-sm text-center">Mari mulai petualangan belajarmu!</h3>
            <div class="w-300 mx-auto">
              {{-- <div class="text-center">
                <p>Masuk dengan akun google / facebook</p>
                <a href="/auth/facebook" class="btn-social facebook">
                  Facebook</a>
                <a href="/auth/google" class="btn-social google">
                  Google</a>                
              </div>
              <h4 class="text-center">Atau gunakan email</h4>--}}
              <div class="form-group label-floating is-empty formGroupEmail">
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="zmdi zmdi-email"></i>
                  </span>
                  <label for="inputEmail" class="control-label">Email / Username</label>
                  <input type="email" class="form-control" name="checkEmail" id="inputEmail" value="{{old('email')}}" required autofocus>
                  @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              @if (Request::get('type')=='sso')
              <form method="POST" action="{{ route('login') }}?type=sso&api_key={{Request::get('api_key')}}&redirect={{Request::get('redirect')}}">
              @else
              <form method="POST" action="{{ route('login') }}">
              @endif
                {{ csrf_field() }}
                <fieldset>
                  <div class="section_password">
                    <div class="form-group no-m no-p label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-email"></i>
                        </span>
                        <span id="loginEmail"></span>
                        <input type="hidden" class="form-control" id="hiddenEmail" name="email" value="{{ old('email') }}">
                      </div>
                    </div>
                    <div class="form-group label-floating is-empty">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock"></i>
                        </span>
                        <label for="inputEmail" class="control-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword" name="password" required>
                        @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                    <div>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                          <span class="fs-14">Ingat Saya.</span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <button class="btn btn-raised btn-primary" onclick="checkEmail()" id="checkEmailBtn" type="button">Mulai</button>
                  <button class="btn btn-raised btn-primary" type="submit" id="submitLogin">Masuk</button>
                </fieldset>
              </form>
              <div class="text-center section_password">
                <a class="btn btn-link text-center" href="{{ route('password.request') }}">
                  Lupa Password?
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

@endsection

@push('script')
  <script type="text/javascript">
    $(".section_password").hide()
    $("#submitLogin").hide()

    $('#inputEmail').keyup(function(e){
        if(e.keyCode == 13){
          if( !validateEmail($(this).val())) {
            alert('failed email')
          }else{
            checkEmail()
          }
        }
    });

    function validateEmail($email) {
      var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return emailReg.test( $email );
    }

    function checkEmail(){
      var email = $("#inputEmail").val();
      if ( email == '' ){
        $('#myAlert').fadeIn(600);
        setTimeout(function(){ $('#myAlert').fadeOut(600) }, 3000);
      }else{
        $.ajax({
          url: '/auth/check/email',
          type: 'post',
          data: {
            email: email,
            _token: "{{csrf_token()}}"
          },
          beforeSend: function(){},
          success: function(response){
            if(response.status == 200){
              $(".section_password").show();
              $("#submitLogin").show();
              $("#checkEmailBtn").hide();
              $(".formGroupEmail").hide();
              $("#inputPassword").focus();
              $("#loginEmail").html(email);
              $("#hiddenEmail").val(email);
            // }else if(response.status !== null){
              // $('#myAlert').fadeIn(600);
              // setTimeout(function(){ $('#myAlert').fadeOut(600) }, 3000);
            }else{
              window.location.assign('/register')
            }
          },
        })
      }
    }
  </script>
@endpush
