@extends('layouts.app')

@section('title')
{{ '500'}}
@endsection

@section('content')
	<div class="container">

		<div class="bg-full-page bg-primary back-fixed" style="z-index:99999">
			<div class="mw-500 absolute-center">
				<div class="card animated zoomInUp animation-delay-7 color-primary withripple">
					<div class="card-block">
						<div class="text-center color-dark">
							<h1 class="color-primary text-big">@lang('front.500.header')</h1>
							<h2>@lang('front.500.description')</h2>
							<p class="lead lead-sm">@lang('front.500.note') <br> @lang('front.500.second_note').</p>
							<a href="/" class="btn btn-primary btn-raised">
								<i class="zmdi zmdi-home"></i> @lang('front.500.back')</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection