@extends('layouts.app')

@section('title')
	Program Tambah Modul
@endsection

@push('style')
	<link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
@endpush

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Program Tambah Course</h3>
					</div>
					<div class="box-body">
						<center>

              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp

						{{ Form::open(array('url' => '/program/module/store/'.Request::segment(4), 'method' => 'post')) }}

							<div class="form-group">
								<label for="">Tambah Modules</label>
								<select class="form-control" name="course_id[]" id="course_id" multiple required>
									@foreach($courses as $course)
										<option value="{{$course->id}}">{{$course->title}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<a href="{{ url('program/dashboard') }}" class="btn btn-default btn-raised">Kembali</a>
								{{  Form::submit('Simpan' , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
							</div>
						{{ Form::close() }}

						<h3>Course Dalam Program Ini</h3>
						<table class="table table-bordered">
							<tr>
								<th>Course</th>
								<th>Pilihan</th>
							</tr>
							@foreach($ProgramCourses as $ProgramCourse)
								<tr>
									<td>{{$ProgramCourse->title}}</td>
									<td><a class="btn btn-danger btn-raised btn-sm" onclick="return confirm('Are you sure?')" href="/program/module/delete/{{$ProgramCourse->id}}">Hapus</a></td>
								</tr>
							@endforeach
						</table>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- SELECT2 --}}
	<script type="text/javascript" src="{{url('select2/js/select2.min.js')}}"></script>
	<script type="text/javascript">
	  $("#course_id").select2();
	</script>
	{{-- SELECT2 --}}
@endpush
