@extends('layouts.app')

@section('title')
	{{ 'Program - ' . $programs->title }}
@endsection

@push('style')
  <style media="screen">
    .ms-navbar{
      margin-bottom: 0;
    }

    .no-radius{
      border-radius: 0;
    }

    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
    }

    .dq-description-max{
      overflow: hidden;
      min-height: 20px;
      max-height: 300px;
      display: -webkit-box!important;
      -webkit-box-orient: vertical;
      white-space: normal;
      margin:0;
    }

    .mt-20{
      margin-top: 20rem;
    }

    .detailSide{
      position: absolute;
      width: 380px;
      left: 38%;
      top: 10%;
      transform: translate(100%, 0%);
    }

    @media (max-width:1366px){
      .detailSide{
        position: absolute;
        width: 380px;
        left: 36%;
        top: 15%;
        transform: translate(100%, 0%);
      }
    }

    @media (max-width:768px){
      .center{
        text-align: center !important;
      }

      .mt-20{
        margin-top: auto !important;
      }
    }
  </style>

  <style media="screen">
    .play-preview{
      color: #dcdde1;
      font-size:95px;
      padding: 10px;
    }

    .play-preview:hover{
      color: #f5f6fa;
      font-size:100px;
    }

    .btn-preview{
      position: absolute;
      width: 80%;
      height: auto;
      text-align: center;
      z-index: 30;
    }
    
    .btn-preview i{
      background: #0000009c;
      width: 100px;
      height: 100px;
      padding: 25px;
      border-radius: 100%;
      opacity: 1;
      margin-top: 10%;
      font-size: 50px;
      transition: 0.1s;
      z-index: 28;
    }

    .btn-preview i:hover{
      font-size: 60px;
      width: 110px;
      height: 110px;
    }

    #desktop{
      display: block;
    }

    #mobile{
      display: none;
    }

    @media (max-width:767px){
      #desktop{
        display: none;
      }

      #mobile{
        display: block;
      }

      .detailSide{
        display: none;
      }

      .col-md-6 p{
        margin: 0;
      }
    }

    .card-md.set-desktop {
      display: block;
    }

    .card-md.set-mobile {
      display: none;
    }

    @media (max-width: 767px) {
      .card-md.set-desktop {
        display: none;
      }

      .card-md.set-mobile {
        display: block;
        width: 100%;
        background: #fff;
        box-shadow: 0px -5px 20px rgba(0, 0, 0, 0.1);
        position: fixed;
        left: 0;
        bottom: 0;
        z-index: 999;
      }
    }
  </style>

  <style>
    .card.card-groups {
      display: flex;
      align-items: center;
      flex-direction: row;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .groups-image {
      width: 200px;
      max-width: 200px;
      border-radius: 5px 0 0 5px;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .card.card-groups.with-more-button .card-block {
      position: relative;
      width: calc(100% - 200px);
    }

    .card.card-groups.with-more-button .card-block .groups-more-description {
      position: absolute; 
      width: calc(100% - 4rem); 
      font-size: 20px; 
      text-align: center; 
      color: #333;
      cursor: pointer;
    }

    @media (max-width: 767px) {
      .card.card-groups .groups-image {
        display: none;
      }
      
      .card.card-groups .card-block .groups-description {
        width: 100%;
      }

      .card.card-groups.with-more-button .card-block {
        width: 100%;
      }

      .card.card-groups.with-more-button .card-block .groups-more-description {
        position: relative;
        display: block;
        width: 100%;
        margin-bottom: -1.5rem;
      }
    }
  </style>

  <style>
    .progress.progress-custom {
      height: 5px;
    }

    .progress-info {
      display: flex; 
      align-items: flex-start; 
      justify-content: space-between;
    }

    .progress-info .progress-percentage {
      font-size: 12px; 
      color: #424242; 
      line-height: 1;
    }

    .progress-info .progress-status {
      display: inline-block; 
      text-align: center; 
      color: #03a9f4; 
      border: 2px solid #03a9f4; 
      padding: 0.5rem 1rem;
    }
  </style>
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="{{ url('/') }}">Beranda</a></li>
          <li><a href="{{ url('programs') }}">Semua Program</a></li>
          <li>{{$programs->title}}</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="top-title-bg">
    <div class="container" style="position: relative;">
      <div class="row">
        <div class="col-md-8">
          <h2 class="headline-md text-white mt-0"><span>{{$programs->title}}</span></h2>
					{{-- <div class="text-justify" style="color: white!important;">{!! $programs->description !!}</div> --}}
          <span class="">
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <i class="zmdi zmdi-star color-warning"></i>
            <span style="color:white;padding: 0 7px;">5.0 (1 penilaian)</span>
          </span>
          <br>
        </div>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 bg-white mb-2 color-dark">
    <div class="container" style="position: relative;">
      <div class="row">
        <div class="col-md-8">
          {{-- DESKRIPSI --}}
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line">Deskripsi <span>program</span></h2>
            </div>
          </div>
          <div>
            <div id="description" class="course-detail">
              <p>{!!$programs->description!!}</p>
            </div>
            <a class="btn-more" id="more-deskripsi">+ Selengkapnya</a>
          </div>

          {{-- MATERI PROGRAM --}}
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-md headline-line">Materi untuk <span>program ini</span></h2>
            </div>
          </div>
          <div class="row">
            @foreach($programs_courses as $course)
              <div class="col-md-12">
                <div class="card card-groups with-more-button">
                  <img class="groups-image" src="{{asset_url($course->image)}}" alt="{{ $course->title }}">
                  <div class="card-block">
                    <span class="groups-title">{{$course->title}}</span>
                    <div id="desc{{ $course->id }}" class="groups-description fs-14" style="color: #999;">{!!$course->description!!}</div>
                    <a id="btn-desc{{ $course->id }}" class="groups-more-description"><i class="zmdi zmdi-chevron-down"></i></a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
        <div class="col-md-4">
          <div style="position: sticky; top: 100px; margin-top: -250px;">
            <div class="card-md set-desktop">
              <img src="{{$programs->image}}" class="img-fluid" alt="{{ $programs->title }}" style="width:100%;"/>
              <div class="card-block">
                <h3 class="text-center color-dark mt-0 mb-1" style="font-family:sans-serif;font-weight:600;">{{$programs->price == 0 ? 'Gratis' : "Harga: " . rupiah($programs->price)}}</h3>
                @php
                  if(Request::get('aff')) {
                    $affiliate = '?aff='.Request::get('aff');
                  }else{
                    $affiliate = '';
                  }
                @endphp
                @if($programs->price === 0)
                  {{Form::open(['url' => 'program/enroll/'.$programs->id.'/'.$affiliate, 'method' => 'post'])}}
                    <input type="hidden" name="id" value="{{$programs->id}}">
                    <input type="hidden" name="slug" value="{{$programs->slug}}">
                    <button type="submit" class="btn btn-success btn-raised btn-block no-shadow no-radius">Enroll Sekarang</button>
                  {{Form::close()}}
                @else
                  @if($ProgramOnCart === false)
                    {{Form::open(['url' => 'cart/buy/program/'.$programs->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-success btn-raised btn-block no-shadow no-radius">Beli</button>
                    {{Form::close()}}
                  @endif
                  @if($ProgramOnCart === false)
                    {{Form::open(['url' => 'cart/add/program/'.$programs->id . $affiliate, 'method' => 'post'])}}
                      <button type="submit" name="addCart" class="btn btn-primary btn-raised btn-block no-shadow no-radius">Tambahkan keranjang</button>
                    {{Form::close()}}
                  @else
                    <a href="{{url('cart')}}" class="btn btn-primary btn-raised btn-block no-shadow no-radius">Lihat Keranjang</a>
                  @endif
                  @if($programs->password)
                    <a href="#" data-target="{{Auth::check() ? '#CourseCheckPassword' : '#ms-account-modal'}}" data-toggle="modal" class="btn btn-info btn-raised btn-block no-shadow no-radius">Enroll Dengan Password</a>
                  @endif
                @endif
                <div class="color-dark">
                  <b>Termasuk:</b>
                  <ul>
                    <li>Total Durasi video <span id="video_durations"></span> </li>
                    <li>Akses seumur hidup</li>
                    <li>Akses di seluler</li>
                  </ul>
                </div>
                <hr>
                <div class="text-center">
                  <p>Bagikan:</p>
                  <a target="_blank" href="https://www.facebook.com/sharer.php?u={{url('programs/' . $programs->slug )}}" rel="nofollow" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
                  <a target="_blank" href="https://twitter.com/intent/tweet?text={{$programs->title}};url={{url('programs/' . $programs->slug )}};via=ingeniocoid" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
                  {{-- <a href="#" class="btn-circle btn-google"><i class="zmdi zmdi-google-plus"></i></a> --}}
                </div>
              </div>
            </div>
          </div>

          <div class="card-md set-mobile">
            <div class="card-body d-flex align-items-center">
              <p class="text-center color-dark" style="font-family:sans-serif; font-size: 24px; font-weight: 600; margin: 0;">Gratis</p>
              <div class="d-flex ml-auto">
                <a class="btn-circle btn-circle-raised btn-circle-info mx-1" href="#" title="Detail Kelas" data-toggle="modal" data-target="#infoModal"><i class="fa fa-info"></i></a>
                {{Form::open(['url' => 'course/enroll'.$affiliate, 'method' => 'get'])}}
                  <input type="hidden" name="id" value="">
                  <input type="hidden" name="slug" value="">
                  <button type="submit" class="btn-circle btn-circle-raised btn-circle-success mx-1" title="Enroll Sekarang"><i class="fa fa-arrow-right"></i></button>
                {{Form::close()}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal" id="CourseCheckPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title color-primary" id="myModalLabel">Masukan Password</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id="CoursePassword" placeholder="Masukan Password">
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button id="CheckPassword" type="button" class="btn  btn-primary">Enroll</button>
              </div>
          </div>
      </div>
  </div>
  <!-- Modal -->

@endsection

@push('script')
  @foreach($programs_courses as $course)
    <script>
      $('#btn-desc{{ $course->id }}').hide();

      if($('#desc{{ $course->id }}').height() > 30) {
        $('#desc{{ $course->id }}').addClass('text-truncate');
        if($('.text-truncate')) {
          $('#btn-desc{{ $course->id }}').show();
        }
      }

      $('#btn-desc{{ $course->id }}').click(function(){
        $('#desc{{ $course->id }}').removeClass('text-truncate');
        $('.groups-image').css('align-self', 'flex-start');
        $(this).hide();
      });
    </script>
  @endforeach

  <script type="text/javascript">
    if($('.headline-md').height() > 48){
      $('.headline-md').addClass('truncate');
    }
    if($('#description').height() > 300){
      $('#description').addClass('dq-description-max');
      $('#more-deskripsi').addClass('show');
    }
    if($('#target').height() > 300){
      $('#target').addClass('dq-description-max');
      $('#more-tujuan').addClass('show');
    }

    $('#more-deskripsi').click(function(){
      $('#description').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-deskripsi').show();
    });
    $('#more-tujuan').click(function(){
      $('#target').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-tujuan').show();
    });
  </script>

  <script type="text/javascript">
    $("#CheckPassword").click(function(){
      var password = $("#CoursePassword").val();
      $.ajax({
        url : '/program/check-password/{{$programs->id}}',
        type : 'post',
        data : {
          password : password,
          _token : '{{csrf_token()}}',
        },
        success : function(response){
          console.log(response)
          if(response === 'true'){
            $.ajax({
              url : '/program/enroll-ajax/{{$programs->id}}',
              type : 'post',
              data : {
                _token : '{{csrf_token()}}',
              },
              success : function(data){
                window.location.assign('/program/learn/{{$programs->slug}}')
              }
            })
          }else{
            alert('Password salah');
          }
        }
      })
    })
  </script>
@endpush
