@extends('layouts.app')

@section('title')
	{{ 'Buat Program' }}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Program Create</h3>
					</div>
					<div class="box-body">
						@php
							if(Session::get('error')){
								$errors = Session::get('error');
							}
						@endphp
						{{ Form::open(array('url' => $action, 'method' => $method, 'files' => true)) }}

							<div class="form-group">
								<label for="">Judul</label>
								<input placeholder="Masukan Judul" type="text" class="form-control" name="title" value="{{ $title }}" required>
							</div>

							<div class="form-group">
								<label for="">Harga</label>
								<input placeholder="Masukan Harga" type="number" class="form-control" name="price" value="{{ $price }}" required>
							</div>

							<div class="form-group">
								<label for="">Gambar</label>
								@if(Request::segment(2) == 'edit')
									<br>
									<img src="{{asset_url($image)}}" height="80">
									<br>
								@endif
								<input placeholder="Pilih Gambar" type="file" class="form-control" name="image">
							</div>

							{{-- <div class="form-group">
								<label for="">Password</label>
								<input placeholder="Password" type="text" class="form-control" name="password" value="{{ $password }}">
							</div> --}}

							<div class="form-group">
								<label for="title">Deskripsi / Penjelasan</label>
								<textarea name="description" id="description">{!! $description !!}</textarea>
							</div>

							{{-- <div class="form-group">
								<label for="">Video Introduction</label>
								<input placeholder="Video Introduction" type="file" class="form-control" name="introduction">
							</div>

							<div class="form-group">
								<label for="title">Introduction Description</label>
								<textarea name="introduction_description" id="introduction_description">{!! $introduction_description !!}</textarea>
							</div> --}}

							{{-- <div class="form-group">
								<label for="title">Keunggulan</label>
								<textarea name="benefits" id="benefits">{!! $introduction_description !!}</textarea>
							</div> --}}

							{{-- <div class="form-group">
								<label for="title">Developer Team</label>
								<textarea name="developer_team" id="developer_team">{!! $developer_team !!}</textarea>
							</div> --}}

							{{-- <div class="form-group">
								<label for="title">Precondition</label>
								<textarea name="precondition" id="precondition">{!! $precondition !!}</textarea>
							</div> --}}

							<div class="form-group">
								{{  Form::hidden('id', $id) }}
								{{  Form::submit($button , array('class' => 'btn btn-primary btn-raised', 'name' => 'button')) }}
								<a href="{{ url('program') }}" class="btn btn-default btn-raised">Kembali</a>
							</div>
						{{ Form::close() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')
	{{-- CKEDITOR --}}
	<script src="{{url('ckeditor/ckeditor.js')}}"></script>
	<script type="text/javascript">
		CKEDITOR.replace('description');
		// CKEDITOR.replace('benefits');
		// CKEDITOR.replace('developer_team');
		// CKEDITOR.replace('precondition');
	</script>
	{{-- CKEDITOR --}}
@endpush
