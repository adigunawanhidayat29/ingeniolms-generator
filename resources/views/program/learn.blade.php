@extends('layouts.app')

@section('title', 'Learn Program ' . $programs->title)

@push('style')
  <style media="screen">
    .ms-navbar{
      margin-bottom: 0;
    }

    .no-radius{
      border-radius: 0;
    }

    .top-title-bg{
      background: #192a56;
      max-height: 350px;
      padding: 10px;
    }

    .top-title-bg .headline-md {
      font-size: 40px;      
    }

    .top-title-bg .headline-md span{
      font-weight: 400;     
    }

    .img-circle{
      border-radius:100%;
      border: 3px solid #dcdde1;
      padding: 5px;
    }

    .dq-description-max{
      overflow: hidden;
      min-height: 20px;
      max-height: 300px;
      display: -webkit-box!important;
      -webkit-box-orient: vertical;
      white-space: normal;
      margin:0;
    }

    .mt-20{
      margin-top: 20rem;
    }

    .detailSide{
      position: absolute;
      width: 380px;
      left: 38%;
      top: 10%;
      transform: translate(100%, 0%);
    }

    @media (max-width:1366px){
      .detailSide{
        position: absolute;
        width: 380px;
        left: 36%;
        top: 15%;
        transform: translate(100%, 0%);
      }
    }

    @media (max-width:768px){
      .top-title-bg{
        background: #ffffff;
        max-height: none;
        padding: 0 0 6rem 0;
        margin-bottom:2rem;
      }

      .top-title-bg .headline-md {
        font-size: 36px;      
      }

      .top-title-bg .container .row .col-md-8{
        {{--background: #192a56 !important;--}}
        background: transparent;
      }

      .center{
        text-align: center !important;
      }

      .mt-20{
        margin-top: auto !important;
      }
    }
  </style>

  <style media="screen">
    .play-preview{
      color: #dcdde1;
      font-size:95px;
      padding: 10px;
    }

    .play-preview:hover{
      color: #f5f6fa;
      font-size:100px;
    }

    .btn-preview{
      position: absolute;
      width: 80%;
      height: auto;
      text-align: center;
      z-index: 30;
    }
    
    .btn-preview i{
      background: #0000009c;
      width: 100px;
      height: 100px;
      padding: 25px;
      border-radius: 100%;
      opacity: 1;
      margin-top: 10%;
      font-size: 50px;
      transition: 0.1s;
      z-index: 28;
    }

    .btn-preview i:hover{
      font-size: 60px;
      width: 110px;
      height: 110px;
    }

    #desktop{
      display: block;
    }

    #mobile{
      display: none;
    }

    @media (max-width:767px){
      #desktop{
        display: none;
      }

      #mobile{
        display: block;
      }

      .detailSide{
        display: none;
      }

      .col-md-6 p{
        margin: 0;
      }
    }

    .card-md.set-desktop {
      display: block;
    }

    .card-md.set-mobile {
      display: none;
    }

    @media (max-width: 767px) {
      .card-md.set-desktop {
        display: none;
      }

      .card-md.set-mobile {
        display: block;
        width: 100%;
        background: #fff;
        box-shadow: 0px -5px 20px rgba(0, 0, 0, 0.1);
        position: fixed;
        left: 0;
        bottom: 0;
        z-index: 999;
      }
    }
  </style>

  <style>
    .top-title-bg {
      background: linear-gradient(90deg, <?= isset(Setting('color_primary')->value) ? Setting('color_primary')->value : '#E7597C' ?> 0%, <?= isset(Setting('color_secondary')->value) ? Setting('color_secondary')->value : '#c9395d' ?> 50%);
      max-height: 350px;
      padding: 10px;
    }

    .card.card-groups {
      display: flex;
      align-items: center;
      flex-direction: row;
      background: #f9f9f9;
      transition: all 0.3s;
      border: 1px solid #f5f5f5;
      border-radius: 5px;
    }

    .card.card-groups .groups-dropdown {
      position: absolute;
      background-color: #fff;
      color: #333;
      right: 1rem;
      top: 1rem;
      cursor: pointer;
      border: 1px solid #4ca3d9;
      z-index: 99;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group {
      padding: 0.5rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu {
      position: absolute;
      top: 1rem;
      right: 2rem;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 15rem;
      padding: 0;
      margin: .125rem 0 0;
      font-size: 1rem;
      color: #212529;
      text-align: left;
      list-style: none;
      background-color: #fff;
      border: 1px solid rgba(0,0,0,.15);
      border-radius: .25rem;
      box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group:hover .groups-dropdown-menu {
      display: block;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item {
      display: block;
      background: #fff;
      width: 100%;
      font-size: 14px;
      color: #212529;
      padding: 0.75rem 1.5rem;
    }

    .card.card-groups .groups-dropdown .groups-dropdown-group .groups-dropdown-menu .groups-dropdown-item:hover {
      background: #4ca3d9;
      color: #fff;
    }

    .card.card-groups .groups-image {
      width: 200px;
      max-width: 200px;
      border-radius: 5px 0 0 5px;
    }

    .card.card-groups .card-block {
      width: 100%;
    }

    .card.card-groups.dropdown .card-block {
      width: calc(100% - 50px);
    }

    .card.card-groups .card-block .groups-title {
      font-size: 16px;
      font-weight: 500;
      color: #4ca3d9;
      margin-bottom: 0;
    }

    .card.card-groups .card-block .groups-title:hover {
      text-decoration: underline;
    }

    .card.card-groups .card-block .groups-description {
      font-weight: 400;
      color: #666;
    }

    .card.card-groups.with-more-button .card-block {
      position: relative;
      width: calc(100% - 200px);
    }

    .card.card-groups.with-more-button .card-block .groups-more-description {
      position: absolute; 
      width: calc(100% - 4rem); 
      font-size: 20px; 
      text-align: center; 
      color: #333;
      cursor: pointer;
    }

    @media (max-width: 767px) {
      .card.card-groups .groups-image {
        display: none;
      }
      
      .card.card-groups .card-block .groups-description {
        width: 100%;
      }

      .card.card-groups.with-more-button .card-block {
        width: 100%;
      }

      .card.card-groups.with-more-button .card-block .groups-more-description {
        position: relative;
        display: block;
        width: 100%;
        margin-bottom: -1.5rem;
      }
    }
  </style>

  <style>
    .progress.progress-custom {
      height: 5px;
    }

    .progress-info {
      display: flex; 
      align-items: flex-start; 
      justify-content: space-between;
    }

    .progress-info .progress-percentage {
      font-size: 12px; 
      color: #424242; 
      line-height: 1;
    }

    .progress-info .progress-status {
      display: inline-block; 
      text-align: center; 
      color: #03a9f4; 
      border: 2px solid #03a9f4; 
      padding: 0.5rem 1rem;
    }
  </style>
@endpush

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li><a href="{{ url('programs') }}">Semua Program</a></li>
        <li>{{$programs->title}}</li>
      </ul>
    </div>
  </div>
</div>
<div class="top-title-bg">
  <div class="container" style="position: relative;">
    <div class="row align-items-center">
      <div class="col-md-4">
        <img src="{{$programs->image}}" class="img-fluid" alt="{{ $programs->title }}" style="width:100%;"/>
      </div>
      <div class="col-md-8">
        <h2 class="headline-md text-white mt-0"><span>{{$programs->title}}</span></h2>
        {{-- <div class="text-justify" style="color: white!important;">{!! $programs->description !!}</div> --}}
        <span class="">
          <i class="zmdi zmdi-star color-warning"></i>
          <i class="zmdi zmdi-star color-warning"></i>
          <i class="zmdi zmdi-star color-warning"></i>
          <i class="zmdi zmdi-star color-warning"></i>
          <i class="zmdi zmdi-star color-warning"></i>
          <span style="color:white;padding: 0 7px;">5.0 (1 penilaian)</span>
        </span>
        <br>
      </div>
    </div>
  </div>
</div>

<div class="wrap pt-2 pb-2 bg-white mb-2 color-dark">
  <div class="container">
    {{-- DESKRIPSI --}}
    <div class="row">
      <div class="col-md-12">
        <h2 class="headline-md headline-line">Deskripsi <span>program</span></h2>
      </div>
      <div class="col-md-12">
        <div id="description" class="course-detail">
          <p>{!!$programs->description!!}</p>
        </div>
        <a class="btn-more" id="more-deskripsi">+ Selengkapnya</a>
      </div>
    </div>

    {{-- MATERI PROGRAM --}}
    <div class="row">
      <div class="col-md-12">
        <h2 class="headline-md headline-line">Materi untuk <span>program ini</span></h2>
      </div>
      @foreach($courses as $index => $course)
        @php
          $num_contents = count($course->contents);
          // echo $num_contents;
          //count percentage
          $num_progress = 0;
          $num_progress = count(DB::table('progresses')->where(['course_id'=>$course->id, 'user_id' => Auth::user()->id, 'status' => '1'])->get());
          $percentage = 0;
          $percentage = $num_progress == 0 ? 100 : $num_contents / $num_progress;
          $percentage = $percentage == 0 ? 1 : $percentage ;
          $percentage = 100 / $percentage;
          //count percentage
        @endphp
      
        <div class="col-md-12">
          <div class="card card-groups">
            <img class="groups-image" src="{{$course->image}}" alt="{{ $course->title }}">
            <div class="card-block">
              <a class="groups-title" href="/course/learn/{{$course->slug}}">{{$course->title}}</a>
              <div class="progress progress-custom mt-1 mb-1">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ceil($percentage >= 100 ? 100 : $percentage)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ceil($percentage >= 100 ? 100 : $percentage)}}%;"></div>
              </div>
              <div class="progress-info">
                <div class="progress-percentage">
                  {{ceil($percentage >= 100 ? 100 : $percentage)}}% selesai
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

@endsection

@push('script')
  <script type="text/javascript">
    if($('#description').height() > 300){
      $('#description').addClass('dq-description-max');
      $('#more-deskripsi').addClass('show');
    }
    if($('#target').height() > 300){
      $('#target').addClass('dq-description-max');
      $('#more-tujuan').addClass('show');
    }

    $('#more-deskripsi').click(function(){
      $('#description').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-deskripsi').show();
    });
    $('#more-tujuan').click(function(){
      $('#target').removeClass('dq-description-max');
      $(this).hide();
      $('#hide-tujuan').show();
    });
  </script>
@endpush
