@extends('layouts.app')

@section('title')
	{{ 'Program' }}
@endsection


@section('content')
	<div class="container">
		<h1>Program</h1>
		<div class="row">
			@foreach($programs as $index => $program)
				<div class="col-md-3">
					<div class="card">
						<a href="{{url('program/'. $program->slug)}}" class="zoom-img withripple">
							<img src="{{$program->image}}" alt="{{ $program->title }}" class="dq-image-big img-fluid">
						</a>
						<div class="card-block text-left">
							<a href="{{url('program/'. $program->slug)}}" data-applink="{{url('programs/'. $program->slug)}}">
								<h4 class="color-primary dq-max-title">{{$program->title}}</h4>
							</a>
							<p>
								<i class="color-dark"><a href="/user/{{ $program->user_slug }}" class="dq-max-subtitle">{{ $program->name }}</a></i>
							</p>
							<div class="mt-2 text-right">
								<a href="{{url('programs/'. $program->slug)}}" class="btn btn-raised btn-primary">
									<i class="fa fa-money"></i>{{$program->price == 0 ? 'Free' : rupiah($program->price)}}
								</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection

@push('script')

@endpush
