@extends('layouts.app')

@section('title')
	{{ 'Program Dashboard' }}
@endsection


@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Program List</h3>
            <a href="{{url('program/create')}}" class="btn btn-primary btn-raised">Buat Program</a>
					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="card">
              <table class="table table-no-border table-striped">
								<thead>
                  <tr>
										<th>#</th>
                    <th>Judul</th>
                    <th>Harga</th>
                    <th>Deskripsi</th>
                    <th>Gambar</th>
                    <th>Pilihan</th>
                  </tr>
								</thead>
								<tbody>
									@foreach($Programs as $index => $Program)
										<tr>
											<td>{{$index + 1}}</td>
											<td>{{$Program->title}}</td>
											<td>{{rupiah($Program->price)}}</td>
											<td>{!!$Program->description!!}</td>
											<td><img height="80" src="{{asset_url($Program->image)}}" alt="{{$Program->title}}"></td>
											<td>
												<div class="btn-group">
												    <button type="button" class="btn btn btn-default dropdown-toggle btn-raised" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											        Pilihan <i class="zmdi zmdi-chevron-down right"></i>
												    </button>
												    <ul class="dropdown-menu dropdown-menu-default">
															<li><a class="dropdown-item" href="/program/module/add/{{$Program->id}}">Tambah Modul</a></li>
													    <li><a class="dropdown-item" href="/program/edit/{{$Program->id}}">Edit</a></li>
													    <li><a class="dropdown-item" onclick="return confirm('Are you sure?')" href="/program/delete/{{$Program->id}}">Delete</a></li>
												    </ul>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
              </table>
            </div>
						{{$Programs->render()}}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@push('script')

@endpush
