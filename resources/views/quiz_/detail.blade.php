@extends('layouts.app-static')

@section('title', $quiz->name)

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <br>
        <h3>{{$quiz->name}}</h3>
        <a class="btn btn-primary btn-raised" href="{{url('quiz/'.$quiz->id.'/start')}}">Start Quiz</a>
        <br><br>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Description</h3>
          </div>
          <div class="panel-body">
            {!! $quiz->description !!}
          </div>
        </div>

      </div>
      <div class="col-md-4">

      </div>
    </div>
  </div>

@endsection
