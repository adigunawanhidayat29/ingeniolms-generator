@extends('layouts.app-static')

@section('title', $quiz->name)

@push('style')
  <style type="text/css">
    a.disabled {
      text-decoration: none;
      color: black;
      cursor: default;
    }
  </style>
@endpush

@section('content')
  <div class="container-fluid">
    <br>
    <div class="row">
      <div class="col-md-8">
        <div id="question_content">
          @php $i =1; @endphp
          @foreach($quiz_questions_answers as $quiz_question_answer)
            <div class="panel panel-default" id="question_{{$quiz_question_answer['id']}}">
              <div class="panel-heading">
                <h3 class="panel-title"><span class="badge">{{$i++}}</span> {!!$quiz_question_answer['question']!!}</h3>
              </div>
              <div class="panel-body">
                @php
                  $char = 'A'; $char <= 'Z';
                @endphp
                @foreach($quiz_question_answer['question_answers'] as $question_answer)
                  <div class="radio radio-primary">
                    <label style="color:#19191a">
                      <input id="answer_id_{{$question_answer->id}}" type="radio" onclick="save_session_answer(this)" name="answer{{$question_answer->quiz_question_id}}" value="{{$question_answer->id}}">{{$char++}}. {{$question_answer->answer}}
                    </label>
                  </div>
                @endforeach
              </div>
            </div>
          @endforeach
        </div>

        {{-- <ul class="pagination" id="question_content-pagination">
          <li><a id="question_content-previous" href="#">&laquo; Previous</a></li>
          <li><a id="question_content-next" href="#">Next &raquo;</a></li>
        </ul> --}}

      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Waktu Anda</h3>
          </div>
          <div class="panel-body">
            <div id="timer"></div>
          </div>
        </div>
        <div class="">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              Go to Question
            </a>
            @php $i =1; @endphp
            @foreach($quiz_questions_answers as $quiz_question_answer)
              <a href="#question_{{$quiz_question_answer['id']}}" class="list-group-item"><span class="badge">{{$i++}}</span> {!!$quiz_question_answer['question']!!}</a>
            @endforeach
          </div>
          <button id="finish_quiz" type="button" class="btn btn-primary btn-lg btn-block btn-raised">Finish Quiz</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
  <script src="{{asset('js/jquery.paginate.min.js')}}"></script> --}}
  <script src="{{asset('js/sweetalert.min.js')}}"></script>

  {{-- question pagination --}}
  {{-- <script type="text/javascript">
    $('#question_content').paginate({itemsPerPage: 5});
  </script> --}}
  {{-- question pagination --}}

  {{-- finish quiz --}}
  <script type="text/javascript">
    function quiz_finish(){
      var answers = [];
      var questions = [];
      @foreach($quiz_questions_answers as $quiz_question_answer)
        var answer_checked = $('input[name=answer{{$quiz_question_answer['id']}}]:checked').val();
        // check if answer is null
        if(answer_checked == null){
          answer_checked = 0;
        }else{
          answer_checked = answer_checked;
        }
        // check if answer is null
        answers.push(answer_checked);
        questions.push('{{$quiz_question_answer['id']}}');
      @endforeach

      var token = '{{ csrf_token() }}';
      $.ajax({
        url : "{{url('quiz/'.$quiz->id.'/finish')}}",
        type : "POST",
        data : {
          answers : answers,
          questions : questions,
          _token: token
        },
        success : function(result){
          //remove session answer
          @foreach($quiz_questions_answers as $quiz_question_answer)
            localStorage.removeItem('answer{{$quiz_question_answer['id']}}');
          @endforeach
          //remove session answer

          window.location.assign('{{url('quiz/'.$quiz->id.'/result')}}');
        },
      });
    }
    $("#finish_quiz").click(function(){
      swal({
        title: "Apakah Anda Yakin?",
        text: "Jawaban Anda akan kami proses!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willSave) => {
        if (willSave) {
          quiz_finish();
        } else {
          swal("Dibatalkan, Silakan untuk melanjutkan pekerjaan Anda");
        }
      });
    })
  </script>
  {{-- finish quiz --}}

  {{-- save session answer --}}
  <script type="text/javascript">
    @foreach($quiz_questions_answers as $quiz_question_answer)
      @foreach($quiz_question_answer['question_answers'] as $question_answer)
        var session_value = get_session_answer('{{'answer'.$quiz_question_answer['id']}}');
        if(session_value !== null){
          $("input[name=answer{{$quiz_question_answer['id']}}][value='"+session_value+"']").attr("checked","checked");
        }
      @endforeach
    @endforeach

    function save_session_answer(e){
      var id = e.name;  // get the sender's id to save it .
      var val = e.value; // get the value.
      localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
    }

    //get the saved value function - return the value of "v" from localStorage.
    function get_session_answer  (v){
      if (localStorage.getItem(v) === null) {
        return "";// You can change this to your defualt value.
      }
      return localStorage.getItem(v);
    }
  </script>
  {{-- save session answer --}}

  {{-- timer --}}
  <script>
    // Set the date we're counting down to
    var countDownDate = new Date("{{$quiz->time_end}}").getTime();
    // Update the count down every 1 second
    var x = setInterval(function() {
        // Get todays date and time
        var now = new Date().getTime();
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Output the result in an element with id="demo"
        $("#timer").html(hours + " Jam, " + minutes + " Menit, " + seconds + " Detik Lagi")
        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            $("#timer").html("Waktu Habis");
            quiz_finish();
        }
    }, 1000);
  </script>
  {{-- timer --}}
@endpush
