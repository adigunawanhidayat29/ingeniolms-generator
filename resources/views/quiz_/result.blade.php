@extends('layouts.app-static')

@section('title', $quiz->name)

@section('content')

  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{$quiz->name}} Result</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-hover">
                @foreach($quiz_participants as $quiz_participant)
                  <tr>
                    <td>
                      <strong>{!!$quiz_participant->question!!}</strong>
                      <div class="radio radio-primary">
                        {!!$quiz_participant->answer!!}
                        {!!$quiz_participant->answer_correct == '1' ? '<i class="fa fa-check color-success"></i>' : '<i class="fa fa-times color-danger"></i>'!!}
                      </div>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Your Score</h3>
          </div>
          <div class="panel-body">
            <!-- <p>total question : {{$count_question}}</p>
            <p>answer correct : {{$count_answer_correct}}</p>
            <p>answer incorrect : {{$count_answer_incorrect}}</p>
            <hr> -->
            <p>Your Score <span class="badge">{{$score}}</span></p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
