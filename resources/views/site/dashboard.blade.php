@extends('layouts.app')

@section('title', 'Site Creator | Dashboard')

@push('style')
	<link href="{{ asset('/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('select2/css/select2.min.css')}}">
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	
@endpush

@section('content')
	<div id="root"></div>
	<!-- <script src="js/app.js"></script> -->
	<script type="text/javascript" src="{{url('js/helper.js')}}"></script>
	<script type="text/javascript" src="{{url('js/global.134340.min.js')}}"></script>
@endsection