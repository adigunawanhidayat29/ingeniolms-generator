@extends('layouts.app_react')

@section('title', 'Site Creator | Builder')

@push('style')
	<link href="{{url('css/library.main.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('css/ingenio-icons.css')}}">

	{{-- Set Style --}}
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster+Two:400,700|Orbitron:400,500,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Arizonia|Great+Vibes|Playball" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <style>
        .ign-card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 100%;
            border-radius: 5px;
            background-color: white;
        }

        .ign-card:hover{
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .ign-card > img {
            border-radius: 5px 5px 0 0;
        }

        .ign-card__container {
            padding: 2px 16px;
        }

    </style>
@endpush

@section('content')
	<div id="root"></div>
	<!-- <script src="js/app.js"></script> -->
	<script type="text/javascript" src="{{url('js/helper.js')}}"></script>
	<script type="text/javascript" src="{{url('js/global.134340.min.js')}}"></script> 
	
@endsection