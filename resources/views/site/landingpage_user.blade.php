<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{$title}}</title>
        <meta name="description" content="{{$description}}">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

        <style>
            body {
                font-family: '{{$font}}';
                padding: 0;
                margin: 0;
                background-color: white;
                overflow-x: hidden;
            }
            .ign-card {
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                width: 100%;
                border-radius: 5px;
                background-color: white;
            }

            .ign-card:hover{
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            }

            .ign-card > img {
                border-radius: 5px 5px 0 0;
            }

            .ign-card__container {
                padding: 2px 16px;
            }
        </style>


        <!-- Custom fonts for this template -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
        
        <link href="https://fonts.googleapis.com/css?family=Lobster+Two:400,700|Orbitron:400,500,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia|Great+Vibes|Playball" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        {!!$html!!}
    </body>
</html>

