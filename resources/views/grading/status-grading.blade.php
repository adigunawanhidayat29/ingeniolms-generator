@extends('layouts.app')

@section('title', 'Quiz Completions ')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-royal alert-light alert-dismissible" role="alert">
				Search data grading berdasarkan Kuis dan Tugas!
			</div>
			<div class="panel panel-warning">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
						<h4 class="panel-title">
							Grading Quiz
						</h4>
					</a>
				</div>
				<div id="collapse1" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Quiz</label><br>
							<div class="col-md-6">
								<select class="form-control selectpicker" name="model" id="kuis" required>
									<option value="">Choose Quiz</option>
									@foreach($quiz as $data)
									<option value="{{$data->id}}">{{$data->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="card card-info">
							<div class="card-header">
								<h3 class="card-title" id="kuis-name"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Start On</th>
										<th>Completed</th>
										<th>Grade</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="user-name"></td>
										<td class="start-on"></td>
										<td class="updated-at"></td>
										<td class="grade"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-warning">
				<div class="panel-heading">
					<a style="color:black;" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
						<h4 class="panel-title">
							Grading Assignment
						</h4>
					</a>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body" style="background:white">
						<div class="form-group">
							<label for="price">Assignment</label><br>
							<select class="form-control selectpicker" name="model" id="assignment" required>
								<option value="">Choose Assignment</option>
								@foreach($assignment as $data)
								<option value="{{$data->id}}">{{$data->title}}</option>
								@endforeach
							</select>
						</div>
						<div class="card card-info">
							<div class="card-header">
								<h3 class="card-title" id="ass-name"><i class="zmdi zmdi-graduation-cap"></i> </h3>
							</div>
							<table class="table table-no-border table-striped">
								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$("#kuis").change(function(){
		// alert($(this).val())
		$.ajax({
			url : "/grading/get-quiz/"+$(this).val(),
			method : "get",
			success: function(result){
				// console.log(result.a)
				$("#kuis-name").html(result.kuis.name);
				$.map(result.a, function( val, i ) {
					$(".user-name").append(val.name + "<br>");
					$(".start-on").append(val.time_start + "<br>");
					$(".updated-at").append(val.time_end + "<br>");
					$(".grade").append(val.grade + "<br>");
				});
			}
		})
	});
	$("#assignment").change(function(){
		// alert($(this).val())
		$.ajax({
			url : "/grading/get-quiz/"+$(this).val(),
			method : "get",
			success: function(result){
				$("#ass-name").html(result.ass.title);
			}
		})
	});
</script>
@endpush