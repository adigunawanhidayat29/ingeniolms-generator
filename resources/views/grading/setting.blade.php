@extends('layouts.app')

@section('title', 'Pengaturan Grades')

@push('style')
  <style media="screen">
    .link-grade{
      text-decoration: underline;
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Nilai <span>Peserta</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/course/dashboard">Pengajar</a></li>
          <li><a href="/course/preview/{{Request::segment(4)}}">Progress Peserta</a></li>
          <li><a href="/course/grades/{{Request::segment(4)}}">Buku Nilai</a></li>
          <li>Pengaturan</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="wrap pt-2 pb-2 mb-2 bg-white">
		<div class="container">
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Pengaturan</h3>
			  </div>
			  <div class="panel-body" style="background:white">
          <form class="" action="/course/grades/setting/save/{{Request::segment(4)}}" method="post">
            {{csrf_field()}}
            @foreach($quizzes as $quiz)
              @php
                $grade_setting_quiz = \DB::table('grade_settings')->where(['table_name' => 'quizzes', 'table_id' => $quiz->id])->first();
              @endphp
              <div class="form-group">
                <label for="">{{$quiz->name}}</label>
                <input type="text" class="form-control" id="" name="percentage[]" placeholder="persentase {{$quiz->name}}" value="{{isset($grade_setting_quiz) ? $grade_setting_quiz->percentage : '0'}}">
                <input type="hidden" name="table_id[]" value="{{$quiz->id}}">
                <input type="hidden" name="table_name[]" value="quizzes">
              </div>
            @endforeach

            @foreach($assignments as $assignment)
              @php
                $grade_setting_assignment = \DB::table('grade_settings')->where(['table_name' => 'assignments', 'table_id' => $assignment->id])->first();
              @endphp
              <div class="form-group">
                <label for="">{{$assignment->title}}</label>
                <input type="text" class="form-control" id="" name="percentage[]" placeholder="persentase {{$assignment->title}}" value="{{isset($grade_setting_assignment) ? $grade_setting_assignment->percentage : '0'}}">
                <input type="hidden" name="table_id[]" value="{{$assignment->id}}">
                <input type="hidden" name="table_name[]" value="assignments">
              </div>
            @endforeach

            <input type="submit" name="" class="btn btn-primary btn-raised" value="Simpan">
          </form>

			  </div>
			</div>
		</div>
	</div>

@endsection
