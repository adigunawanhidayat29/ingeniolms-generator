@extends('layouts.app')

@section('title', 'Register Affiliate')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    @if(!Auth::check())
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      {{-- <h3>Affiliate Register / Login</h3> --}}
      <div class="row">
        {{-- <div class="col-md-8">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Affiliate Register</h3>
            </div>
            <div class="panel-body">
              <form class="" action="/affiliate/join" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Name" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{old('phone')}}" required>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}" required>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="bank" placeholder="Bank" value="{{old('bank')}}" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="bank_account_number" placeholder="Nomor Rekening Bank" value="{{old('bank_account_number')}}" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="bank_account_name" placeholder="Bank Atas Nama" value="{{old('bank_account_name')}}" required>
                </div>
                <input type="submit" class="btn btn-primary btn-raised" value="Register">
              </form>
            </div>
          </div>
        </div> --}}
        <div class="col-md-8">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Apa Itu Affiliate Marketing?</h3>
            </div>
            <div class="panel-body">
              <p>
                Secara sederhana affiliate adalah makelar atau perantara, memasarkan produk seseorang untuk mendapatkan keuntungan. Lebih jelasnya proses affiliate marketing digambarkan pada bagan dibawah ini.
              </p>
            </div>
          </div>
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Bagaimana caranya?</h3>
            </div>
            <div class="panel-body">
              <p>
                Cukup dengan mendaftar akun Anda bisa langsung menjadi affiliate.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Affiliate Login</h3>
            </div>
            <div class="panel-body">
              <form class="" action="/affiliate/login" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  <input type="email" class="form-control" name="email" value="" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <input type="submit" class="btn btn-primary btn-raised" value="Login">
              </form>
            </div>
          </div>
        </div>
      </div>
    @else
      @if(!Session::get('message'))
        <p>Lanjutkan untuk menjadi Affiliate?</p>
        <form class="" action="/affiliate/join/user" method="post">
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary btn-raised">Lanjutkan</button>
        </form>
      @endif
    @endif
  </div>
@endsection
