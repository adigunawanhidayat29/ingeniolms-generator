@extends('layouts.app')

@section('title', 'Affiliate Balance')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-3">
        @include('affiliate.navigation')
      </div>
      <div class="col-md-9">
        <button type="button" class="btn btn-primary btn-raised">Saldo: {{rupiah($balance)}}</button>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Affiliate Balance</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>No</td>
                      <td>Tanggal</td>
                      <td>Informasi</td>
                      <td>Debit</td>
                      <td>Kredit</td>
                    </tr>
                    @foreach($affiliate_balances as $index => $affiliate_balance)
                      <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$affiliate_balance->created_at}}</td>
                        <td>{{$affiliate_balance->information}}</td>
                        <td>{{rupiah($affiliate_balance->debit)}}</td>
                        <td>{{rupiah($affiliate_balance->credit)}}</td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                {{$affiliate_balances->render()}}
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
