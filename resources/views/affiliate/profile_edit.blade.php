@extends('layouts.app')

@section('title', 'Edit Profile Affiliate')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Edit Profile Affiliate</h3>
          </div>
          <div class="panel-body">
            <form action="/affiliate/profile/update" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              {{-- <div class="form-group">
                <img src="{{asset_url($affiliate_user->banner)}}"><br>
                <label for="">Banner</label>
                <input type="text" readonly="" class="form-control" placeholder="Browse...">
                <input type="file" name="banner">
              </div> --}}
              <div class="form-group">
                <label for="">Nama Bank</label>
                <input type="text" class="form-control" name="bank" value="{{$affiliate_user->bank}}" placeholder="Nama Bank">
              </div>
              <div class="form-group">
                <label for="">Atas Nama</label>
                <input type="text" class="form-control" name="bank_account_name" value="{{$affiliate_user->bank_account_name}}" placeholder="Atas Nama">
              </div>
              <div class="form-group">
                <label for="">Nomor Rekening</label>
                <input type="text" class="form-control" name="bank_account_number" value="{{$affiliate_user->bank_account_number}}" placeholder="Nomor Rekening">
              </div>
              <input type="submit" class="btn btn-primary btn-raised" value="Update Profile">
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection
