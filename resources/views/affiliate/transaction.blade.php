@extends('layouts.app')

@section('title', 'Affiliate Transaction')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-3">
        @include('affiliate.navigation')
      </div>
      <div class="col-md-9">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Affiliate Transaction</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>No</td>
                      <td>Invoice</td>
                      <td>Customer</td>
                      <td>Telepon</td>
                      <td>Produk</td>
                      <td>Total</td>
                      <td>Komisi</td>
                      <td>Status</td>
                    </tr>
                    @foreach($affiliate_transactions as $index => $affiliate_transaction)
                      <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$affiliate_transaction->invoice}}</td>
                        <td>{{$affiliate_transaction->name}}</td>
                        <td>{{$affiliate_transaction->phone}}</td>
                        <td>
                          @php
                          $TransactionDetails = DB::table('transactions_details')->where('invoice', $affiliate_transaction->invoice);
                          if($TransactionDetails->first()->course_id != NULL){
                            $TransactionDetails->join('courses','courses.id','=','transactions_details.course_id');
                          }else{
                            $TransactionDetails->join('programs','programs.id','=','transactions_details.program_id');
                          }
                          @endphp
                          <ul>
                            @foreach($TransactionDetails->get() as $TransactionDetail)
                              <li>{{$TransactionDetail->title}}</li>
                            @endforeach
                          </ul>
                        </td>
                        <td>{{rupiah($affiliate_transaction->subtotal)}}</td>
                        <td>{{rupiah($affiliate_transaction->commission)}}</td>
                        <td>{!!$affiliate_transaction->status == '1' ? '<span class="text-success">Success</span>' : '<span class="text-warning">Pending</span>'!!}</td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                {{$affiliate_transactions->render()}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
