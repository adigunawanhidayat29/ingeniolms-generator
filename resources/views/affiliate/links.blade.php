@extends('layouts.app')

@section('title', 'Affiliate Penarikan')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-3">
        @include('affiliate.navigation')
      </div>
      <div class="col-md-9">
        <a data-target="#modalWithdrawalRequest" data-toggle="modal" class="btn btn-primary btn-raised" href="#">Buat Permintaan</a>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Sematkan Link</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>No</td>
                      <td>Course</td>
                      <td>Link</td>
                      <td>Pilihan</td>
                    </tr>
                    @foreach($courses as $index => $data)
                      <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$data->title}}</td>
                        <td>
                          <span id="textURL{{$data->id}}">{{url('course/'.$data->slug. $affiliate_user->url)}}</span>
                        </td>
                        <td>
                          <button onclick="copyToClipboard('#textURL{{$data->id}}')" class="btn btn-primary btn-raised btn-sm">Copy</button>
                        </td>
                      </tr>
                    @endforeach
                  </table>
                </div>
                <div class="text-center">
            			{{$courses->links('pagination.default')}}
            		</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modalWithdrawalRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title color-primary" id="myModalLabel">Permintaan Penarikan</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
        </div>
        <div class="modal-body">
          <form class="" action="/affiliate/withdrawal/request" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <input type="number" min="20000" class="form-control" name="amount" placeholder="Jumlah">
            </div>
            <input type="submit" value="Kirim" class="btn btn-primary btn-raised">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function copyToClipboard(element){
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
      alert('copied');
    }
  </script>
@endpush
