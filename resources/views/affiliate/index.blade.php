@extends('layouts.app')

@section('title', 'Affiliate Dashboard')

@section('content')
  <div class="container">
    @if(Session::get('message'))
      <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
    <div class="row">
      <div class="col-md-3">
        @include('affiliate.navigation')
      </div>
      <div class="col-md-9">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Dashboard - Hallo, {{Auth::user()->name}}</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <br>
                <div class="card card-warning-inverse">
                  <div class="card-block text-center">
                    <span class="ms-icon ms-icon-white ms-icon-circle ms-icon-xxlg mb-4"><i class="fa fa-money color-warning"></i></span>
                    <h4 class="color-light">{{rupiah($balance)}}</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <h3>Aturan Terkini</h3>
                <table class="table table-bordered">
                  <tr>
                    <td>Komisi</td>
                    <td>{{rupiah($affiliate_setting->commission)}}</td>
                  </tr>
                  <tr>
                    <td>Diskon</td>
                    <td>{{$affiliate_setting->discount}} %</td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai</td>
                    <td>{{$affiliate_setting->start_date}}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Berakhir</td>
                    <td>{{$affiliate_setting->end_date}}</td>
                  </tr>
                </table>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <h3>Profile Anda</h3>
                <table class="table">
                  <tr>
                    <td>Kode Referal</td>
                    <td>{{$affiliate_user->url}}</td>
                  </tr>
                  <tr>
                    <td>Kode Diskon</td>
                    <td>
                      {{$affiliate_user->code}}
                      {{-- <a style="font-size:12px" href="#" data-toggle="modal" data-target="#modalEditCode">Edit Code</a> --}}
                    </td>
                  </tr>
                  {{-- <tr>
                    <td>Sematkan</td>
                    <td>
                      <span id="textURL">
                        {{'<a href="'.url('program/'.$program->slug.'/'.$affiliate_user->url).'"><img src="'.asset_url($affiliate_setting->banner).'" height="100" /></a>'}}
                      </span>
                      <br>
                      <button type="button" onclick="copyToClipboard('#textURL')" class="btn btn-primary btn-raised btn-sm"> Copy </button>
                    </td>
                  </tr> --}}
                  {{-- <tr>
                    <td>Banner</td>
                    <td><img src="{{asset_url($affiliate_user->banner)}}"></td>
                  </tr> --}}
                  <tr>
                    <td>Nama Bank</td>
                    <td>{{$affiliate_user->bank}}</td>
                  </tr>
                  <tr>
                    <td>Bank Atas Nama</td>
                    <td>{{$affiliate_user->bank_account_name}}</td>
                  </tr>
                  <tr>
                    <td>Nomor Rekening Bank</td>
                    <td>{{$affiliate_user->bank_account_number}}</td>
                  </tr>
                </table>
                <a href="/affiliate/profile/edit" class="btn btn-warning btn-raised">Edit Profile</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modalEditCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title color-primary" id="myModalLabel">Edit Code Affiliate</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
        </div>
        <div class="modal-body">
          <form class="" action="/affiliate/profile/edit-code" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <input type="text" class="form-control" name="code" value="{{$affiliate_user->code}}" placeholder="Code Affiliate">
            </div>
            <input type="submit" value="Simpan" class="btn btn-primary btn-raised">
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script>
    function copyToClipboard(element){
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
      alert('copied');
    }
  </script>
@endpush
