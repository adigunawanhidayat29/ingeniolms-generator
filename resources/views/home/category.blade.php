@extends('layouts.app')

@section('title', $Category->title)

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">@lang('front.detail_category.header') <span>"{{$Category->title}}"</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.detail_category.breadcrumb_home')</a></li>
          <li><a href="/courses">@lang('front.detail_category.breadcrumb_first')</a></li>
          @if($ParentCategory)
            <li><a href="/category/{{$ParentCategory->slug}}">{{$ParentCategory->title}}</a></li>
          @endif
          <li>{{$Category->title}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <button id="filterButton" class="btn btn-raised btn-primary" type="button" name="button" data-toggle="modal" data-target="#filter" style="display:none;">Filter</button>
        </div>
        {{-- <div id="mobile" class="col-md-3">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-sm headline-line">Filter <span>berdasarkan:</span></h2>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Harga:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Gratis
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Berbayar
                </div>
                <div class="form-group no-m">
                  <input type="submit" class="btn btn-primary btn-raised" value="Filter">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Format:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Video
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Live
                </div>
                <div class="form-group no-m">
                  <input type="submit" class="btn btn-primary btn-raised" value="Filter">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Durasi:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> @lang('front.detail_category.lifetime')
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> @lang('front.detail_category.scheduled')
                </div>
                <div class="form-group no-m">
                  <input type="submit" class="btn btn-primary btn-raised" value="Filter">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
              @php $Categories = DB::table('categories')->get(); @endphp
              @foreach($Categories as $Category)
                <a href="/category/{{$Category->slug}}">{{$Category->title}}</a><br>
              @endforeach
            </div>
          </div>
        </div> --}}
        <div class="col-md-12">
          {{-- JIKA MEMILIKI SUBCATEGORI --}}
          @if($categoriesWithCourses->children)
            @foreach($categoriesWithCourses->children as $data)
              <div class="row">
                <div class="col-md-12">
                  <h3>{{$data->title}}</h3>
                  <div class="row">
                    @foreach($data->courses as $course)
                      <div class="col-md-3 mb-2 {{$course->public == '1' ? 'public-course' : 'private-course'}}">
                        <div class="card-sm">
                          <a href="{{url('course/'. $course->slug)}}">
                            <img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
                          </a>
                          <a href="{{url('course/'. $course->slug)}}">
                            <div class="card-block text-left">
                              <span class="badge badge-primary">@lang('front.detail_category.lifetime')</span>
                              <span class="text-right" style="background: orange; color: white; padding: 4px; font-weight: bold; border-radius: 32px; font-size: 12px">
                                {{$course->price == 0 ? '' : $course->price}}
                              </span>
                              <h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
                              <p>
                                  <i class="fa fa-star color-warning"></i>
                                  <i class="fa fa-star color-warning"></i>
                                  <i class="fa fa-star color-warning"></i>
                                  <i class="fa fa-star color-warning"></i>
                                  <i class="fa fa-star color-warning"></i>
                                </p>
                              <p class="section-body-xs">
                                <span class="dq-max-subtitle">{{ $course->name }}</span>
                              </p>
                              <div class="m-0">
                                <button class="btn btn-raised btn-primary btn-block">@lang('front.detail_category.detail_button')</button>
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            @endforeach
            <hr>
          @endif
          {{-- JIKA MEMILIKI SUBCATEGORI --}}

          <h3>{{$Category->title}}</h3>
          
          <div class="row">
              @foreach($categoriesWithCourses->courses as $course)
              @php
                $video_duration = 0;
                foreach($course->contents()->where('contents.status', '1')->get() as $course_content){
                  $video_duration += $course_content->video_duration;
                }
              @endphp

              {{-- <div class="col-md-3">
                <a href="{{url('course/'. $course->slug)}}">
                  <div class="card-list">
                    <img src="{{$course->image}}" alt="{{ $course->title }}" class="img-fluid">
                    <div class="card-block description text-left">
                      <h3 class="headline-xs no-m">{{$course->title}}</h3>
                      @if(is_liveCourse($course->id) == true) <span class="badge badge-danger">Live</span> @endif
                      @if($course->public == '0') <span class="badge badge-warning">Tertutup</span> @endif
                      @if($course->model == 'batch') <span class="badge badge-success">@lang('front.detail_category.scheduled')</span> @endif
                      @if($course->model == 'subscription') <span class="badge badge-primary">@lang('front.detail_category.lifetime')</span> @endif

                      <div class="option-mobile text-right">
                        <p>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                        </p>
                        {{$course->price == 0 ? 'Rp. 0' : rupiah($course->price)}}
                      </div>

                      <span class="d-0">&nbsp; | &nbsp;<span>
                      <span class="section-body-xs d-0">
                        <span><i class="fa fa-book"></i> {{$course->contents()->where('contents.status', '1')->get()->count()}} konten</span>
                        &nbsp;&nbsp;
                        <span><i class="fa fa-clock-o"></i> {{floor($video_duration / 60)}} menit</span>
                      </span>
                      <p class="section-body-xs color-dark">
                        <i class="fa fa-user"></i> {{ $course->name }}
                      </p>

                      <p class="section-body-xs d-0 color-dark" style="font-size:14px;">{{$course->subtitle}}</p>
                    </div>
                    <div class="option d-0 text-right">
                      <p>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                      </p>
                      {{$course->price == 0 ? 'Rp. 0' : rupiah($course->price)}}
                    </div>
                  </div>
                </a>
              </div> --}}

              <div class="col-md-3 mb-2 {{$course->public == '1' ? 'public-course' : 'private-course'}}">
                <div class="card-sm">
                  <a href="{{url('course/'. $course->slug)}}">
                    <img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
                  </a>
                  <a href="{{url('course/'. $course->slug)}}">
                    <div class="card-block text-left">
                      <span class="badge badge-primary">@lang('front.detail_category.lifetime')</span>
                      {{-- <span class="text-right" style="background: orange; color: white; padding: 4px; font-weight: bold; border-radius: 32px; font-size: 12px">
                        {{$course->price == 0 ? 'Rp.0 (Gratis)' : $course->price}}
                      </span> --}}
                      <h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
                      <p>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                      </p>
                      <p class="section-body-xs">
                        <span class="dq-max-subtitle">{{ $course->name }}</span>
                      </p>
                      <div class="m-0">
                        <button class="btn btn-raised btn-primary btn-block">@lang('front.detail_category.detail_button')</button>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>

      <!--Pagination Wrap Start-->
      <div class="text-center">
        {{$courses->links('pagination.default')}}
      </div>
      <!--Pagination Wrap End-->
    </div>
  </div>

  <div class="modal" id="filter" tabindex="-1" role="dialog" aria-labelledby="filterLabel" style="z-index:99999;">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body p-1">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-sm headline-line">Filter <span>berdasarkan:</span></h2>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Harga:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Gratis
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Berbayar
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Format:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Video
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Live
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Durasi:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Lifetime
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Terjadwal
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
              @php $Categories = DB::table('categories')->get(); @endphp
              @foreach($Categories as $Category)
                <a href="/category/{{$Category->slug}}">{{$Category->title}}</a><br>
              @endforeach
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Apply</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">
    if($(document).width() < 1024){
      $('#mobile').hide();
      $('#filterButton').show();
    }
  </script>
@endpush
