@extends('layouts.app')

@section('content')
	<iframe width="480" height="270" src="//www.youtube.com/embed/Ks-_Mh1QhMc" frameborder="0" gesture="media" allowfullscreen></iframe>
	{{-- <div class="card">
				    <div data-type="youtube" data-video-id="Ks-_Mh1QhMc"></div>
				</div> --}}
	{{-- <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">

					</div>

					<div class="box-body">

            <center>
              @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('success') !!}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {!! Session::get('error') !!}
                </div>
              @endif
            </center>

            <div class="table-responsive">
							<div class="panel panel-primary">
						    <div class="panel-heading">Course Participant : {{$courses->title}}</div>
						    <div class="panel-body">
									<a class="btn btn-default" href="{{url('courses/show/'. $courses->id)}}">Back</a>
									<br><br>
									<table class="table table-hover">
		                <tr>
											<th>No</th>
		                	<th>Name</th>
		                </tr>
										@php $no = 1 @endphp
										@foreach($course_users as $course_user)
											<tr>
												<td>{{$no++}}</td>
												<td>{{$course_user->name}}</td>
											</tr>
										@endforeach
		              </table>
								</div>
						  </div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div> --}}
@endsection
