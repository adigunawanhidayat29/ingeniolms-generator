@extends('layouts.app')

@section('title', isset(Setting('contact_us_title')->value) ? Setting('contact_us_title')->value : 'Hubungi Kami')

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>{{isset(Setting('contact_us_title')->value) ? Setting('contact_us_title')->value : 'Hubungi Kami'}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>{{isset(Setting('contact_us_title')->value) ? Setting('contact_us_title')->value : 'Hubungi Kami'}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {!!isset(Setting('contact_us_value')->value) ? Setting('contact_us_value')->value : 'data belum ada'!!}
        </div>
      </div>
    </div>
  </div>
@endsection
