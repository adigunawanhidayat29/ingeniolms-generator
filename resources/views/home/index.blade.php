@extends('layouts.app')

@section('title', Lang::get('front.page_home.title'))

@push('style')
<link rel="stylesheet" href="{{asset('sweetalert2/sweetalert2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}" />
<style>
  .tagline-image {
    width: 100%;
  }

  .tagline-text {
    font-size: 50px;
    line-height: 1;
    text-shadow: 0px 5px 15px rgba(0, 0, 0, 0.25);
  }

  .tagline-text span {
    font-weight: 600;
  }

  .tagline-text.white {
    color: #ffffff;
  }

  .tagline-text.yellow {
    color: #fff112;
  }

  /* .download-section {
      margin-top: 5rem;
      padding-top: 2rem;
      padding-bottom: 0rem;
    } */

  /* .download-section .headline-line:after {
      top: 75%;
    } */

  /* .mobile-download {
      overflow: hidden;
      height: 300px;
      position: relative;
      margin-top: -30%;
    } */

  /* .mobile-apps-download{
			display: inline-block;
			margin-right: 0.5rem;
		} */

  /* .mobile-apps-download img{
			background: transparent;
			width: 150px;
			border-radius: 5px;
			transition: all ease 0.2s;
		} */

  /* .mobile-apps-download:hover{
			box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.2);
		} */

  @media(max-width: 1024px) {
    .tagline-text {
      font-size: 40px;
    }
  }

  @media(width: 768px) {
    .tagline-text {
      font-size: 30px;
    }
  }

  @media(max-width: 767px) {
    .tagline-text {
      font-size: 30px;
      text-align: center;
    }
  }

  @media(max-width: 420px) {
    .tagline-text {
      font-size: 30px;
      text-align: center;
    }
  }
</style>

<style>
  .btn-see-all:before {
    content: "@lang('front.page_home.more_button')";
  }

  .wrap-headline-group {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 2rem;
  }

  .video-wrap {
    background: #fff;
  }

  .video-wrap iframe {
    width: 350px;
    height: 197px;
  }

  @media (max-width: 768px) {
    .btn-see-all:before {
      content: "\f105";
    }

    .category-wrap .container,
    .available-wrap .container,
    .program-wrap .container,
    .degree-wrap .container,
    .video-wrap .container {
      padding-left: 35px;
      padding-right: 35px;
    }

    .video-wrap iframe {
      width: 100%;
      height: 172px;
      margin-bottom: 1rem;
    }

    .wrap-headline-group .btn-see-all {
      position: relative;
      top: 0;
      left: 0;
    }
  }
</style>
@endpush

@push('meta')
<meta name="description" content="Online Training bersama praktisinya Untuk Karir & Passionmu">
@endpush

@section('content')

@if( isset(Setting('banner')->value) && Setting('banner')->value == 'true' )
@if( isset(Setting('banner_style')->value) && Setting('banner_style')->value == 'default' )
<div class="bg-header">
  <div class="container">
    <div class="row">
      <div class="col-md-7 text color-white">
        {{-- <h3 class="color-white">Platform e-learning</h3> --}}
        {{-- <p class="w-500">Temukan dan ikuti kelas terbaik untuk melejitkan bakat dan minatmu</p> --}}
        <h2><span>{{ isset(Setting('title')->value) ? Setting('title')->value : 'Ingeniolms App' }}</span></h2>
        <p class="w-500">
          {{ isset(Setting('tagline')->value) ? Setting('tagline')->value : 'Tagline site belum diatur' }}</p>
        <div class="header-link mt-2">
          <a class="btn-header" href="/courses">Cari Kelas</a>
          <span>atau</span>
          <a class="btn-header-link" href="#" data-toggle="modal" data-target="#myModal">Punya kode kelas?</a>
        </div>
      </div>
      <div class="col-md-5 image">
        <div class="headerFade">
          @foreach($banners as $banner)
          <div class="image">
            <img src="{{asset_url($banner->image)}}" alt="{{$banner->name}}">
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@else
{{-- <div id="carousel-example-generic" class="ms-carousel carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    @if( count($sliders) == 0 )
      <div class="carousel-item active" style="height:500px !important">
        <img class="d-block img-fluid" style="width:100% !important;" src="{{asset('img/banner.jpg')}}">
        <div class="carousel-caption">
          <h3>Slide One</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget nulla felis. Sed leo dolor, fermentum ac metus sit amet, laoreet scelerisque lectus. Quisque dolor est, scelerisque eget orci ac, vehicula luctus ante. Nulla turpis sem, lobortis aliquet tristique eget, lacinia ut urna. Donec consectetur sodales lectus in cursus. Nullam ligula lorem, euismod vitae mattis non, posuere et leo. Maecenas feugiat leo varius turpis ullamcorper, in bibendum odio sagittis. Curabitur malesuada risus vel dui cursus rhoncus.</p>
        </div>
      </div>
      <div class="carousel-item" style="height:500px !important">
        <img class="d-block img-fluid" style="width:100% !important;" src="{{asset('img/banner1.jpg')}}">
        <div class="carousel-caption">
          <h3>Slide Two</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget nulla felis. Sed leo dolor, fermentum ac metus sit amet, laoreet scelerisque lectus. Quisque dolor est, scelerisque eget orci ac, vehicula luctus ante. Nulla turpis sem, lobortis aliquet tristique eget, lacinia ut urna. Donec consectetur sodales lectus in cursus. Nullam ligula lorem, euismod vitae mattis non, posuere et leo. Maecenas feugiat leo varius turpis ullamcorper, in bibendum odio sagittis. Curabitur malesuada risus vel dui cursus rhoncus.</p>
        </div>
      </div>
    @else
      @foreach($sliders as $index => $data)
        <div class="carousel-item {{ $index == 0 ? 'active' : ''}}" style="height:500px !important">
          <img class="d-block img-fluid" style="width:100% !important;" src="{{$data->image}}" alt="{{$data->title}}">
          <div class="carousel-caption">
            <h3>{{$data->title}}</h3>
            <p>{{$data->description}}</p>
          </div>
        </div>
      @endforeach
    @endif
  </div>

<!-- Controls -->
<a href="#carousel-example-generic"
  class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control-prev" role="button"
  data-slide="prev"><i class="zmdi zmdi-chevron-left"></i></a>
<a href="#carousel-example-generic"
  class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control-next" role="button"
  data-slide="next"><i class="zmdi zmdi-chevron-right"></i></a>
</div> --}}

<div class="sliderWrap">
  @if( count($sliders) == 0 )
  <div class="image">
    <div class="slider-text">
      <p class="slider-title">LOREM IPSUM</p>
      <p class="slider-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
        velit</p>
      <a href="#" class="slider-link">LINK TO</a>
    </div>
    <div class="slider-overlay"></div>
    <img src="{{asset('img/banner.jpg')}}" alt="">
  </div>
  <div class="image">
    <div class="slider-text">
      <p class="slider-title">LOREM IPSUM TWO</p>
      <p class="slider-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
        velit</p>
      <a href="#" class="slider-link">LINK TO</a>
    </div>
    <div class="slider-overlay"></div>
    <img src="{{asset('img/banner1.jpg')}}" alt="">
  </div>
  @else
  @foreach($sliders as $index => $data)
  <div class="image">
    <div class="slider-text"
      style="align-items: {{ $data->text_alignment }}; justify-content: {{ $data->text_position }};">
      <p class="slider-title"
        style="font-size: {{ $data->header_font_size != null ? $data->header_font_size : '36px' }}; font-weight: {{ $data->header_font_weight != null ? $data->header_font_weight : '700' }}; color: {{ $data->header_font_color != null ? $data->header_font_color : '#ffffff' }}">
        {{$data->header}}</p>
      <p class="slider-subtitle"
        style="font-size: {{ $data->subheader_font_size != null ? $data->subheader_font_size : '18px' }}; font-weight: {{ $data->subheader_font_weight != null ? $data->subheader_font_weight : '300' }}; color: {{ $data->subheader_font_color != null ? $data->subheader_font_color : '#ffffff' }}">
        {{$data->subheader}}</p>
      <a href="{{$data->link_button}}"
        class="slider-link {{ $data->link_button == null || $data->text_button == null ? 'd-none' : '' }}">{{$data->text_button}}</a>
    </div>
    <div class="slider-overlay"
      style="opacity: {{ $data->overlay_transparention != null ? $data->overlay_transparention : '#000000' }};"></div>
    <img src="{{asset_url($data->image)}}" alt="{{$data->header}}">
  </div>
  @endforeach
  @endif
</div>
@endif
@else
<div class="landingpage-accent"></div>
@endif

<center>
  @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {!! Session::get('success') !!}
      </div>
  @elseif(Session::has('warning'))
  <div class="alert alert-warning alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {!! Session::get('warning') !!}
  </div>
  @elseif(Session::has('error'))
      <div class="alert alert-error alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {!! Session::get('error') !!}
      </div>
  @endif
</center>

@php
$mdColumn = 12 / isset(Setting('catalog_column')->value) ? Setting('catalog_column')->value : '3';
@endphp

@if( isset(Setting('categories')->value) && Setting('categories')->value == 'true')
<!-- KATEGORI -->
<div class="wrap category-wrap pt-4 pb-4">
  <div class="container">
    {{-- <div class="row">
          <div class="col-md-9">
            <h2 class="headline-md mb-3">{{ isset(Setting('menu_category')->value) ? Setting('menu_category')->value : 'Kategori' }}
    </h2>
  </div>
  <div class="col-md-3">
    <a class="btn-see-all pull-right mt-3" href="/categories"></a>
  </div>
</div> --}}

<div class="wrap-headline-group">
  <h2 class="headline-md">
    {{-- <span>{{ isset(Setting('menu_category')->value) ? Setting('menu_category')->value : Lang::get('front.page_home.title_category') }}</span> --}}
    <span>@lang('front.page_home.title_category')</span>
  </h2>
  <a class="btn-see-all" href="/categories"></a>
</div>
<div class="row">
  @foreach($categories as $data)
  <div class="col-md-{{12 / $mdColumn}} text-center">
    <div class="card card-paper">
      <img class="card-img-top" src="{{$data->icon}}" alt="">
      <div class="card-body">
        <h3 class="headline-sm text-truncate fs-16"><span>{{$data->title}}</span></h3>
        <p class="text-truncate" style="color: #aaa;">{!!$data->description!!}</p>
        <a class="btn btn-raised btn-primary"
          href="{{url('category/'. $data->slug)}}">@lang('front.page_home.detail_button') <i
            class="zmdi zmdi-arrow-right after-text"></i></a>
      </div>
    </div>
  </div>
  @endforeach
</div>
</div>
</div>
<!-- KATEGORI -->
@endif

@if( isset(Setting('courses')->value) && Setting('courses')->value == 'true')
<!-- KELAS TERSEDIA -->
<div class="wrap available-wrap pt-4 pb-4" style="background: #f9f9f9;">
  <div class="container">
    {{-- <div class="row">
            <div class="col-md-9">
              <h2 class="headline-md">Kelas Tersedia</h2>
            </div>
            <div class="col-md-3">
              <a class="btn-see-all pull-right mt-3" href="/courses"></a>
            </div>
          </div> --}}

    <div class="wrap-headline-group">
      <h2 class="headline-md"><span>@lang('front.page_home.title_available_courses')</span></h2>
      <a class="btn-see-all" href="/courses"></a>
    </div>
    <div class="row mt-5">

      @foreach($courses as $course)
      <div class="col-md-{{12 / $mdColumn}} text-center">
        <div class="card card-paper">
          <img class="card-img-top" src="{{$course->image}}" alt="">
          <div class="card-body">
            <h3 class="headline-sm text-truncate fs-16"><span>{{$course->title}}</span></h3>
            <p class="text-truncate" style="color: #aaa;">{!!$course->subtitle!!}</p>
            <a class="btn btn-raised btn-primary"
              href="{{url('course/'. $course->slug)}}">@lang('front.page_home.detail_button') <i
                class="zmdi zmdi-arrow-right after-text"></i></a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
<!-- KELAS TERSEDIA -->
@endif

<!-- PROGRAM -->
@if( isset(Setting('program')->value) && Setting('program')->value == 'true')
<div class="wrap program-wrap pt-4 pb-4">
  <div class="container">
    <div class="wrap-headline-group">
      <h2 class="headline-md"><span>@lang('front.page_home.title_available_program')</span></h2>
      <a class="btn-see-all" href="/programs"></a>
    </div>
    <div class="row">
      @foreach($programs as $program)
      <div class="col-md-3 text-center">
        <div class="card card-paper">
          <img class="card-img-top" src="{{$program->image}}" alt="">
          <div class="card-body">
            <h3 class="headline-sm fs-16"><span>{{$program->title}}</span></h3>
            <p>{!!str_limit($program->description, 100)!!}</p>
            <a class="btn btn-raised btn-primary"
              href="{{url('program/'. $program->slug)}}">@lang('front.page_home.detail_button') <i
                class="zmdi zmdi-arrow-right after-text"></i></a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif
<!-- PROGRAM -->

<!-- DEGREE -->
@if( isset(Setting('degree')->value) && Setting('degree')->value == 'true')
<div class="wrap degree-wrap pt-4 pb-4">
  <div class="container">
    <div class="wrap-headline-group">
      <h2 class="headline-md"><span>@lang('front.page_home.title_available_degree')</span></h2>
      <a class="btn-see-all" href="/degrees"></a>
    </div>
    <div class="row">
      @foreach($degrees as $degree)
      <div class="col-md-3 text-center">
        <div class="card card-paper">
          <img class="card-img-top" src="{{$degree->image}}" alt="">
          <div class="card-body">
            <h3 class="headline-sm fs-16"><span>{{$degree->title}}</span></h3>
            <p>{!!str_limit($degree->description, 100)!!}</p>
            <a class="btn btn-raised btn-primary"
              href="{{url('degree/'. $degree->slug)}}">@lang('front.page_home.title_available_degree') <i
                class="zmdi zmdi-arrow-right after-text"></i></a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif
<!-- DEGREE -->

{{-- <div class="wrap landingpage-wrap pt-4 pb-4 color-white">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <img class="tagline-image" src="{{asset('img/web-tagline.png')}}" alt="">
          </div>
          <div class="col-md-6">
            <p class="tagline-text white">Liberating Knowledge</p>
            <p class="tagline-text yellow"><span>Ilmu Untuk Semua</span></p>
            </div>
          </div>
        </div>
      </div>
    </div> --}}

<div class="wrap feature-wrap pt-4 pb-4">
  <div class="container">
    <div class="row" style="align-items: center;">
      <div class="col-md-5">
        <img
          src="{{Setting('features_thumbnail')->value != '' ? asset_url(Setting('features_thumbnail')->value) : asset('/img/playground.png')}}"
          class="app-feature-image">
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col-md-12">
            <h2 class="headline-md headline-line mt-0">@lang('front.page_home.our_fitur_title_fitur')
              <span>@lang('front.page_home.our_fitur_title_our')</span></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            @foreach($features as $feature)
            <div style="margin-bottom: 1rem;">
              <p style="font-size: 16px; font-weight: 600; margin: 0;"><i class="{{$feature->icon}}"
                  style="font-size: 20px; {{$feature->icon != null ? 'margin-right: .5rem' : ''}};"></i>
                {{$feature->title}}</p>
              <p style="font-size: 14px; margin: 0;">{{$feature->description}}</p>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if( isset(Setting('video')->value) && Setting('video')->value == 'true')
@if(count($videos) > 0)
<!-- VIDEO -->
<div class="wrap video-wrap pt-4 pb-4">
  <div class="container">
    <div class="wrap-headline-group">
      <h2 class="headline-md"><span>@lang('front.page_home.title_video')</span></h2>
      <a class="btn-see-all" href="/video"></a>
    </div>
    <div class="row">
      @foreach($videos as $video)
      <div class="col-md-4">
        <iframe src="{{$video->url}}" frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif
@endif
</div>

<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog animated zoomIn animated-3x" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title color-dark" id="myModalLabel">Kode Kelas</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
              class="zmdi zmdi-close"></i></span></button>
      </div>
      <div class="modal-body">
        <form id="CheckPassword" class="form-group no-m" action="" method="post">
          <input id="CoursePassword" class="form-control" type="text" name="" value=""
            placeholder="Masukkan Kode Kelas">
          <input class="btn btn-raised btn-primary pull-right" type="submit" name="" value="Masuk">
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="play-video" tabindex="-1" role="dialog">
  <div class="w-1000 modal-dialog animated zoomIn animated-3x" role="document">
    <div class="modal-content">
      <div data-type="youtube" data-video-id="yjXPeKXQ_TA"></div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal" id="information" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog animated zoomIn animated-3x" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h3 class="modal-title color-dark" id="myModalLabel">Informasi Terbaru</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
              class="zmdi zmdi-close"></i></span></button>
      </div>
      <div class="modal-body">
        <a href="http://bit.ly/karyalomba" target="_blank">
          <img src="{{asset('img/kreasi-kelas-online.jpeg')}}" width="100%" class="img-responsive" alt="kreasi-kelas-online">
        </a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('script')

{{-- <script type="text/javascript">
    $("#information").modal('show')
  </script> --}}

<!-- <script type="text/javascript">
    var owl = $('.owl-carousel');

    owl.owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:4,
              nav:true,
              loop:false
          }
      },
      navText : ['<i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-2x fa-angle-right" aria-hidden="true"></i>'],
      animateIn: false,
      animateOut: '',
    });
  </script> -->
<script type="text/javascript" src="{{asset('slick/slick.min.js')}}"></script>
<script type="text/javascript">
  $('.sliderWrap').slick({
    dots: true,
    infinite: true,
    speed: 600,
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    cssEase: 'linear'
  });
  $('.headerFade').slick({
    dots: false,
    infinite: true,
    speed: 900,
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    cssEase: 'linear'
  });
  $('.responsips').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3.15,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2.65,
          slidesToScroll: 1,
          infinite: false,
          dots: false
        }
      },
      {
        breakpoint: 995,
        settings: {
          slidesToShow: 2.63,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2.25,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2.2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  $('.responsip').slick({
    dots: false,
    infinite: false,
    speed: 600,
    slidesToShow: 4.3,
    slidesToScroll: 3,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2.25,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2.2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $('.lazy').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 4,
    slidesToScroll: 1,
  });
</script>

<script src="{{asset('sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/promise-polyfill.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function () {
    // Add smooth scrolling to all links
    $("a").on('click', function (event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 600, function () {

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });

  $("#CheckPassword").submit(function (e) {
    e.preventDefault();

    var password = $("#CoursePassword").val();
    $.ajax({
      url: '/course/join-private-course',
      type: 'post',
      data: {
        password: password,
        _token: '{{csrf_token()}}',
      },
      success: function (response) {
        console.log(response);
        if (response.code == 200) {
          $.ajax({
            url: '/course/enroll',
            type: 'get',
            data: {
              id: response.data.id,
            },
            success: function (data) {
              $("#myModal").modal('hide');
              swal({
                type: 'success',
                title: 'Berhasil...',
                html: response.message,
              }).then(function () {
                window.location.assign('/course/learn/' + response.data.slug)
              });
            }
          })
        } else {
          $("#myModal").modal('hide');
          swal({
            type: 'error',
            title: 'Gagal!',
            html: response.message,
          })
        }
      },
      error: function () {
        $("#ms-account-modal").modal('show')
      }
    })
  })
</script>

<script type="text/javascript">
  var DEBUG = false;
  if (!DEBUG) {
    if (!window.console) window.console = {};
    var methods = ["log", "debug", "warn", "info"];
    for (var i = 0; i < methods.length; i++) {
      console[methods[i]] = function () {};
    }
  }
</script>
@endpush
