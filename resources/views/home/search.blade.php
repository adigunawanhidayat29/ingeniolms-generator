@extends('layouts.app')

@section('title', 'Hasil pencarian ' . '"'.Request::get('q').'"')

@push('style')
    <style>
      .flip-card {
        background-color: transparent;
        width: 300px;
        min-height: 350px;
        perspective: 1000px;
        padding: 10px;
      }
  
      .flip-card-inner {
        position: relative;
        width: 100%;
        height: 100%;
        text-align: center;
        transition: transform 0.6s;
        transform-style: preserve-3d;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        border-radius: 25px;
      }
  
      .flip-card:hover .flip-card-inner {
        transform: rotateY(180deg);
      }
  
      .flip-card-front, .flip-card-back {
        position: absolute;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        padding: 2rem;
        border-radius: 25px;
      }
  
      .flip-card-front {
        background-color: white;
        color: black;
      }
  
      .flip-card-front img {
        width: 175px;
        height: 175px;
        object-fit: cover;
        border-radius: 50%;
      }
      
      .flip-card-back {
        background: linear-gradient(0deg, #333 0%, #999 100%);
        color: white;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        transform: rotateY(180deg);
      }
  
      .flip-card-front .flip-content-frame {
        margin-top: 2rem;
      }
  
      .flip-card-back .flip-content-frame .name,
      .flip-card-front .flip-content-frame .name {
        font-size: 16px;
        font-weight: 600;
        margin: 0;
      }
  
      .flip-card-back .flip-content-frame .tagline,
      .flip-card-front .flip-content-frame .tagline {
        font-size: 14px;
        margin: 0;
      }
  
      .flip-card-back .flip-content-frame .description,
      .flip-card-front .flip-content-frame .description {
        font-size: 12px;
        margin: 0;
      }
  
      .flip-card-back .flip-content-frame .divider {
        width: 75%;
        color: #eee;
        margin: 1rem auto;
      }
  
      .flip-card-back .flip-content-frame .button {
        font-size: 14px;
        color: #fff;
        transition: 0.2s ease;
      }
  
      .flip-card-back .flip-content-frame .button:hover {
        border-bottom: 2px solid #fff;
      }
    </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m">Hasil pencarian <span>"{{Request::get('q')}}"</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>Pencarian</li>
          <li>{{Request::get('q')}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <button id="filterButton" class="btn btn-raised btn-primary" type="button" name="button" data-toggle="modal" data-target="#filter" style="display:none;">Filter</button>
        </div>
        {{-- <div id="mobile" class="col-md-3">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-sm headline-line">Filter <span>berdasarkan:</span></h2>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Harga:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Gratis
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Berbayar
                </div>
                <div class="form-group no-m">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Format:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Video
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Live
                </div>
                <div class="form-group no-m">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Durasi:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Lifetime
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Terjadwal
                </div>
                <div class="form-group no-m">
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
              @php $Categories = DB::table('categories')->get(); @endphp
              @foreach($Categories as $Category)
                <a href="/category/{{$Category->slug}}">{{$Category->title}}</a><br>
              @endforeach
            </div>
          </div>
        </div> --}}
        <div class="col-md-12">
          
          @if(count($courses) > 0)

          <h3>Kelas</h3>
          <div class="row">
            @foreach($courses as $course)
              @php
                $video_duration = 0;
                foreach($course->contents()->where('contents.status', '1')->get() as $course_content){
                  $video_duration += $course_content->video_duration;
                }
              @endphp

              <div class="col-md-3 mb-2 {{$course->public == '1' ? 'public-course' : 'private-course'}}">
                <div class="card-sm">
                  <a href="{{url('course/'. $course->slug)}}">
                    <img src="{{$course->image}}" alt="{{ $course->title }}" class="dq-image img-fluid">
                  </a>
                  <a href="{{url('course/'. $course->slug)}}">
                    <div class="card-block text-left">
                      <span class="badge badge-primary">Lifetime</span>
                      <span class="text-right" style="background: orange; color: white; padding: 4px; font-weight: bold; border-radius: 32px; font-size: 12px">
                        {{$course->price == 0 ? 'Rp.0 (Gratis)' : $course->price}}
                      </span>
                      <h4 class="headline-xs dq-max-title">{{$course->title}}</h4>
                      <p>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                        </p>
                      <p class="section-body-xs">
                        <span class="dq-max-subtitle">{{ $course->name }}</span>
                      </p>
                      <div class="m-0">
                        <button class="btn btn-raised btn-primary btn-block">Lihat</button>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              {{-- <div class="col-md-12">
                <a href="{{url('course/'. $course->slug)}}">
                  <div class="card-list">

                    <img src="{{$course->image}}" alt="{{ $course->title }}">
                    <div class="card-block description text-left">
                      <h3 class="headline-xs no-m">{{$course->title}}</h3>
                      @if(is_liveCourse($course->id) == true) <span class="badge badge-danger">Live</span> @endif
                      @if($course->public == '0') <span class="badge badge-warning">Tertutup</span> @endif
                      @if($course->model == 'batch') <span class="badge badge-success">Terjadwal</span> @endif
                      @if($course->model == 'subscription') <span class="badge badge-primary">Lifetime</span> @endif

                      <div class="option-mobile text-right">
                        <p>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star color-warning"></i>
                          <i class="fa fa-star-o"></i>
                        </p>
                        {{$course->price == 0 ? 'Gratis' : rupiah($course->price)}}
                      </div>

                      <span class="d-0">&nbsp; | &nbsp;<span>
                      <span class="section-body-xs d-0">
                        <span><i class="fa fa-book"></i> {{$course->contents()->where('contents.status', '1')->get()->count()}} konten</span>
                        &nbsp;&nbsp;
                        <span><i class="fa fa-clock-o"></i> {{floor($video_duration / 60)}} menit</span>
                      </span>
                      <p class="section-body-xs color-dark">
                        <i class="fa fa-user"></i> {{ $course->name }}
                      </p>

                      <p class="section-body-xs d-0 color-dark" style="font-size:14px;">{{$course->subtitle}}</p>
                    </div>
                    <div class="option d-0 text-right">
                      <p>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star color-warning"></i>
                        <i class="fa fa-star-o"></i>
                      </p>
                      {{$course->price == 0 ? 'Gratis' : rupiah($course->price)}}
                    </div>
                  </div>
                </a>
              </div> --}}
            @endforeach
          </div>
          @endif

          @if(count($users) > 0)
            <h3>Pengajar</h3>
            <div class="row">
              @foreach($users as $data)
              {{-- <a href="{{url('user/'. $data->slug)}}">
                <div class="col-md-3 text-center">
                  <div class="card card-paper">
                    <img class="card-img-top" src="{{$data->photo}}" alt="{{$data->name}}">
                    <div class="card-body">
                      <h3 class="headline-sm fs-16"><span>{{$data->name}}</span></h3>
                      <a class="btn btn-raised btn-primary" href="{{url('user/'. $data->slug)}}">Lihat Pengajar</a>
                    </div>
                  </div>
                </div>
              </a> --}}

              {{-- <div class="col-md-3 text-center flip-card">
                <div class="card card-paper flip-card-inner">
                  <div class="flip-card-front">
                    <img src="{{$data->photo}}" alt="{{$data->name}}" class="card-img-top">
                    <div class="card-body">
                      <h3 class="headline-sm fs-16"><span>{{$data->name}}</span></h3>
                      <h3 class="headline-sm fs-16"><span>{{$data->tagline}}</span></h3>
                      <a class="btn btn-raised btn-primary btn-sm" href="{{url('user/'. $data->slug)}}">Lihat Pengajar</a>
                    </div>
                  </div>
                  <div class="flip-card-back">
                    <p>{{$data->short_description}}</p>
                    <a class="btn btn-raised btn-primary btn-sm" href="{{url('user/'. $data->slug)}}">Lihat Detail</a>
                  </div>
                </div>
              </div> --}}

              <div class="col-md-3 text-center flip-card">
                <div class="flip-card-inner">
                  <div class="flip-card-front">
                    <img src="{{$data->photo}}" alt="{{$data->name}}">
                    <div class="flip-content-frame">
                      <p class="name">{{$data->name}}</p>
                      <p class="tagline">{{$data->tagline}}</p>
                    </div>
                  </div>
                  <div class="flip-card-back">
                    <div class="flip-content-frame">
                      <p class="name">{{$data->name}}</p>
                      <p class="description">{{$data->short_description}}</p>
                      <hr class="divider">
                      <a class="button" href="{{url('user/'. $data->slug)}}">Lihat Detail</a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="filter" tabindex="-1" role="dialog" aria-labelledby="filterLabel" style="z-index:99999;">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-body p-1">
          <div class="row">
            <div class="col-md-12">
              <h2 class="headline-sm headline-line">Filter <span>berdasarkan:</span></h2>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Harga:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Gratis
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Berbayar
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Format:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Video
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Live
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Durasi:</span></h3>
              <form action="/search" method="GET">
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Lifetime
                </div>
                <div class="form-group no-m">
                  <input type="checkbox" name="" value=""> Terjadwal
                </div>
                <div class="form-group no-m">
                  {{-- <input type="submit" class="btn btn-primary btn-raised" value="Filter"> --}}
                </div>
              </form>
            </div>
          </div>
          <div class="card-category">
            <div class="card-block">
              <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
              @php $Categories = DB::table('categories')->get(); @endphp
              @foreach($Categories as $Category)
                <a href="/category/{{$Category->slug}}">{{$Category->title}}</a><br>
              @endforeach
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Apply</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('script')
  <script type="text/javascript">
    if($(document).width() < 1024){
      $('#mobile').hide();
      $('#filterButton').show();
    }
  </script>
@endpush
