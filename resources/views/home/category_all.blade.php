@extends('layouts.app')

@section('title', Lang::get('front.all_category.title'))

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <h2 class="headline-md no-m">@lang('front.all_category.header')</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.all_category.breadcrumb_home')</a></li>
          <li>@lang('front.all_category.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
  </div>

  <!-- KELAS PRODI -->
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        @foreach($categories as $data)
          {{-- <a href="{{url('category/'. $data->slug)}}">
            <div class="col-md-4 text-center">
              <div class="card card-paper">
                <img class="card-img-top" src="{{$data->icon}}" alt="">
                <div class="card-body">
                  <h3 class="headline-sm fs-16"><span>{{$data->title}}</span></h3>
                  <p style="color: #bbb">{!!str_limit($data->description, 100)!!}</p>
                  <a class="btn btn-raised btn-primary" href="{{url('category/'. $data->slug)}}">Lihat Detail</a>
                </div>
              </div>
            </div>
          </a> --}}

          <div class="col-md-3 text-center">
            <div class="card card-paper">
              <img class="card-img-top" src="{{$data->icon}}" alt="">
              <div class="card-body">
                <h3 class="headline-sm text-truncate fs-16"><span>{{$data->title}}</span></h3>
                <p class="text-truncate" style="color: #aaa;">{!!$data->description, 100!!}</p>
                <a class="btn btn-raised btn-primary" href="{{url('category/'. $data->slug)}}">@lang('front.all_category.detail_button') <i class="zmdi zmdi-arrow-right after-text"></i></a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <!-- KELAS PRODI -->
@endsection