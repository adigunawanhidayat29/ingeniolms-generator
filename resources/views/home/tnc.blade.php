@extends('layouts.app')

@section('title', isset(Setting('tnc_title')->value) ? Setting('tnc_title')->value : 'Syarat & Ketentuan')

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>{{isset(Setting('tnc_title')->value) ? Setting('tnc_title')->value : 'Syarat & Ketentuan'}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>{{isset(Setting('tnc_title')->value) ? Setting('tnc_title')->value : 'Syarat & Ketentuan'}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {!!isset(Setting('tnc_value')->value) ? Setting('tnc_value')->value : 'data belum ada'!!}
        </div>
      </div>
    </div>
  </div>
@endsection
