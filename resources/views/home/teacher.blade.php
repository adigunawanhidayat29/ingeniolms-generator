@extends('layouts.app')

@section('title', Lang::get('front.contributor.title'))

@push('style')
  <style>
    .btn-see-all:before {
      content: "@lang('front.contributor.more_button')";
    }

    .hero.btn-see-all {
      border: 2px solid #fff;
      color: #fff;
    }

    .flip-card {
      background-color: transparent;
      width: 300px;
      min-height: 350px;
      perspective: 1000px;
      padding: 10px;
    }

    .flip-card-inner {
      position: relative;
      width: 100%;
      height: 100%;
      text-align: center;
      transition: transform 0.6s;
      transform-style: preserve-3d;
      border: 2px solid #eee;
      border-radius: 25px;
    }

    .flip-card:hover .flip-card-inner {
      transform: rotateY(180deg);
    }

    .flip-card-front, .flip-card-back {
      position: absolute;
      width: 100%;
      height: 100%;
      -webkit-backface-visibility: hidden;
      backface-visibility: hidden;
      padding: 2rem;
      border-radius: 25px;
    }

    .flip-card-front {
      /* background-color: white; */
      background-color: #f5f5f5;
      color: black;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
    }

    .flip-card-front img {
      width: 175px;
      height: 175px;
      object-fit: cover;
      border-radius: 50%;
    }
    
    .flip-card-back {
      background: linear-gradient(0deg, #333 0%, #999 100%);
      color: white;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      transform: rotateY(180deg);
    }

    .flip-card-front .flip-content-frame {
      margin-top: 2rem;
    }

    .flip-card-back .flip-content-frame .name,
    .flip-card-front .flip-content-frame .name {
      font-size: 16px;
      font-weight: 600;
      margin: 0;
    }

    .flip-card-back .flip-content-frame .tagline,
    .flip-card-front .flip-content-frame .tagline {
      font-size: 14px;
      margin: 0;
    }

    .flip-card-back .flip-content-frame .description,
    .flip-card-back .flip-content-frame .description *,
    .flip-card-front .flip-content-frame .description,
    .flip-card-front .flip-content-frame .description * {
      font-size: 12px;
      margin: 0;
    }

    .flip-card-back .flip-content-frame .divider {
      width: 75%;
      color: #eee;
      margin: 1rem auto;
    }

    .flip-card-back .flip-content-frame .button {
      font-size: 14px;
      color: #fff;
      transition: 0.2s ease;
    }

    .flip-card-back .flip-content-frame .button:hover {
      border-bottom: 2px solid #fff;
    }

    @media (max-width: 768px) {
      .btn-see-all:before {
        content: "\f105";
      }
      
      .btn-see-all {
        position: relative;
        top: inherit;
        left: inherit;
      }
    }

    @media (max-width: 420px) {
      .btn-see-all:before {
        content: "\f105";
      }
      
      .flip-card {
        width: 100%;
        min-height: 250px;
      }

      .flip-card-front, .flip-card-back {
        width: 100%;
        height: 100%;
      }

      .flip-card-front img {
        width: 125px;
        height: 125px;
      }
    }
  </style>
@endpush

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>@lang('front.contributor.header')</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">@lang('front.contributor.breadcrumb_home')</a></li>
          <li>@lang('front.contributor.breadcrumb_destination')</li>
        </ul>
      </div>
    </div>
  </div>

  <!-- KELAS PRODI -->
  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      @if(\Request::get('type') != 'community')
        <div class="row">
          <div class="col-md-12">
            <div class="d-flex align-items-center justify-content-between mb-1">
              <h2 class="headline headline-md m-0">@lang('front.contributor.all_contributor')</h2>
              @if(\Request::get('type') != 'all')
                <a class="btn-see-all" href="/pengajar?type=all"></a>
              @endif
            </div>
          </div>
        </div>

        <div class="row mb-3">
          @foreach($users as $data)
            <div class="col-md-3 text-center flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="{{$data->photo}}" alt="{{$data->name}}">
                  <div class="flip-content-frame">
                    <p class="name">{{$data->name}}</p>
                    <p class="tagline">{{ $data->tagline }}</p>
                  </div>
                </div>
                <div class="flip-card-back">
                  <div class="flip-content-frame">
                    <p class="name">{{$data->name}}</p>
                    <div class="description">{!! $data->short_description !!}</div>
                    <hr class="divider">
                    <a class="button" href="{{url('user/'. $data->slug)}}">@lang('front.contributor.detail_button')</a>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>

        @if(\Request::get('type') == 'all')
          <!--Pagination Wrap Start-->
          <div class="text-center">
            {{$users->appends(request()->except('page'))->links('pagination.default')}}
          </div>
          <!--Pagination Wrap End-->
        @endif
      @endif
      
      @if(\Request::get('type') != 'all')
        @if(count($communities) > 0)
          @foreach($communities as $data)
            <div class="row">
              <div class="col-md-12">
                <div class="d-flex align-items-center justify-content-between mb-1">
                  <h2 class="headline headline-md m-0">{{ $data->name }}</h2>
                  @if(\Request::get('type') != 'community')
                    <a class="btn-see-all" href="/pengajar?type=community&id={{ $data->id }}"></a>
                  @endif
                </div>
              </div>
            </div>

            @if(empty(\Request::get('id')))
              <div class="row mb-3">
                @foreach($data->instructors as $data)
                  <div class="col-md-3 text-center flip-card">
                    <div class="flip-card-inner">
                      <div class="flip-card-front">
                        <img src="{{photo_url($data->photo)}}" alt="{{$data->name}}">
                        <div class="flip-content-frame">
                          <p class="name">{{$data->name}}</p>
                          <p class="tagline">{{ $data->tagline }}</p>
                          {{-- <p class="name">{{$data->name}}</p>
                          <p class="tagline">{!! $data->short_description !!}</p> --}}
                        </div>
                      </div>
                      <div class="flip-card-back">
                        <div class="flip-content-frame">
                          <p class="name">{{$data->name}}</p>
                          <div class="description">{!! $data->short_description !!}</div>
                          {{-- <p class="name">{{$data->name}}</p>
                          <p class="description">{!!$data->description!!}</p> --}}
                          <hr class="divider">
                          <a class="button" href="{{url('user/'. $data->slug)}}">@lang('front.contributor.detail_button')</a>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            @else
              <div class="row">
                @foreach($instructors as $data)
                  <div class="col-md-3 text-center flip-card">
                    <div class="flip-card-inner">
                      <div class="flip-card-front">
                        <img src="{{photo_url($data->photo)}}" alt="{{$data->name}}">
                        <div class="flip-content-frame">
                          <p class="name">{{$data->name}}</p>
                          <p class="tagline">{{ $data->tagline }}</p>
                          {{-- <p class="name">{{$data->name}}</p>
                          <p class="tagline">{!! $data->short_description !!}</p> --}}
                        </div>
                      </div>
                      <div class="flip-card-back">
                        <div class="flip-content-frame">
                          <p class="name">{{$data->name}}</p>
                          <div class="description">{!! $data->short_description !!}</div>
                          {{-- <p class="name">{{$data->name}}</p>
                          <p class="description">{!!$data->description!!}</p> --}}
                          <hr class="divider">
                          <a class="button" href="{{url('user/'. $data->slug)}}">@lang('front.contributor.detail_button')</a>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="text-center">
                {{$instructors->appends(request()->except('page'))->links('pagination.default')}}
              </div>
            @endif
          @endforeach
        @endif
      @endif    
    </div>
  </div>
  <!-- KELAS PRODI -->
@endsection