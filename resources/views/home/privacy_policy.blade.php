@extends('layouts.app')

@section('title', isset(Setting('privacy_policy_title')->value) ? Setting('privacy_policy_title')->value : 'Kebijakan Privasi')

@section('content')
  <div class="bg-page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="headline-md no-m"><span>{{isset(Setting('privacy_policy_title')->value) ? Setting('privacy_policy_title')->value : 'Kebijakan Privasi'}}</span></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li>{{isset(Setting('privacy_policy_title')->value) ? Setting('privacy_policy_title')->value : 'Kebijakan Privasi'}}</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="wrap pt-2 pb-2 mb-2 bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {!!isset(Setting('privacy_policy_value')->value) ? Setting('privacy_policy_value')->value : 'data belum ada'!!}
        </div>
      </div>
    </div>
  </div>
@endsection
