<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\{{modelName}};

class {{modelName}}Controller extends Controller
{
    public function index(){        
        return view('admin.{{modelName}}.index');
    }

    public function serverside(Request $request)
    {
        if($request->ajax()){
            $data = {{modelName}}::select('*')->get();
            return Datatables::of($data)      
            ->addColumn('action', function ($data)
            {
                return '                
                    <a href="'. url(\Request::route()->getPrefix() .'/{{modelNamePlural}}/edit/'.$data->id) .'" class="btn btn-warning btn-sm">Edit</a>
                    <a href="'. url(\Request::route()->getPrefix() .'/{{modelNamePlural}}/delete/'.$data->id) .'" class="btn btn-danger btn-sm">Hapus</a>
                ';
            })->make(true);
        }else{
            exit("Not an AJAX request");
        }
    }

    public function create(){
        $data = [
            'action' => \Request::route()->getPrefix() .'/{{modelNamePlural}}/store',
            'method' => 'post',
            'button' => 'Create',
            {{column_array_old}}
        ];
        return view('admin.{{modelName}}.form', $data);
    }

    public function store(Request $request){
        ${{modelName}} = new {{modelName}};
        {{column_array_store}}
        ${{modelName}}->save();

        \Session::flash('success', 'Create record success');
        return redirect(\Request::route()->getPrefix() . '/{{modelNamePlural}}');
    }

    public function edit($id){
        ${{modelName}} = {{modelName}}::where('id', $id)->first();
        $data = [
            'action' => \Request::route()->getPrefix() .'/{{modelNamePlural}}/update',
            'method' => 'post',
            'button' => 'Update',
            {{column_array_old_value}}
        ];
        return view('admin.{{modelName}}.form', $data);
    }

    public function update(Request $request)
    {
        ${{modelName}} = {{modelName}}::where('id', $request->id)->first();        
        {{column_array_store}}
        ${{modelName}}->save();
        \Session::flash('success', 'Update record success');        

        return redirect(\Request::route()->getPrefix() . '/{{modelNamePlural}}');
    }

    public function delete(Request $request, $id)
    {
        ${{modelName}} = {{modelName}}::where('id', $id);
        if(${{modelName}}){
            ${{modelName}}->delete();
            \Session::flash('success', 'Delete record success');
        }else{
            \Session::flash('error', 'Delete record failed');
        }

        return redirect(\Request::route()->getPrefix() . '/{{modelNamePlural}}');
    }
}