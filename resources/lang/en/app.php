<?php

return [
  'home' => 'Beranda',
  'login' => 'Login',
  'register' => 'Register',
  'become_instructor' => 'Become An Instructor',
  'have_a_question' => 'Have A Question?',
  'about' => 'About',
  'contact' => 'Contact',
  'course' => 'Course',
  'create_course' => 'Create Course',
  'my_course' => 'My Course',
  'logout' => 'Logout',
  'search' => 'Search Here',
  'connect_us' => 'STAY TUNED WITH US',
  'subscribe_word' => 'Get our updates educational materials, new courses and more!',  
];
