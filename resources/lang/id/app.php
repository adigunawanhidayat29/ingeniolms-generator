<?php

return [
  'home' => 'Beranda',
  'login' => 'Masuk',
  'register' => 'Daftar',
  'become_instructor' => 'Menjadi Pengajar',
  'have_a_question' => 'Ada Pertanyaan?',
  'about' => 'Tentang Kami',
  'contact' => 'Kontak',
  'course' => 'Kursus',
  'create_course' => 'Tambah Kursus',
  'my_course' => 'Kursus Saya',
  'logout' => 'Keluar',
  'search' => 'Cari disini',
  'connect_us' => 'Tetap Bersama Kami',
  'subscribe_word' => 'Dapatkan update terbaru dari Kami.',
];
